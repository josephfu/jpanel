// global variable //
var gCurrMode = "EMPTY";
var gSpreadMax = 5;
var gSpreadMin = 5;
var gSpreadMid = 5;
var gByMacro = 1;
var gByManual = 7;
var gByFunction = 8;
var gSeqNum = 0;
var gActNo = 1;
var gSessID = 0;
var gLastMmoNum = 0;
var gMinSecToSec = 1000;
var gRandomNum = 0;
var gLeaderID = 0;
var gLeaderIP = 0;
var gLeaderOn = 0;
var gIsLeader = 0;
var gModeCfmAct = 0;
var gRowUpdFeq = 0;
var gRowRepOrdDly = 0;
var gRowOrdType;
var gRunning = 2;
var gGettingSessID = 0;
var gMaxProfit = 0;
var gGotParameter = 0;
var gCallingLeader = 0;
var gRepliedLeaderOn = 0;
var gChgingMode = 0;
var gAskingSessID = 0;
var gGuaSlowFedCount = 0;
var gGuaMarketDepthCount = 0;
var gInited = false;
var gGotNarrowMASpread = false;
var gCountingTickVolume = false;
var gTickCount = 0;
var gRowOrdMethod = "MA";
var gRowOrdSpeed = 60;
var gDefSymTs = 0.1;
var gOrdType = gDefOrdType;
var gOrdTIF = gDefOrdTIF;

var gHostName = window.location.hostname;
var gIP;
var gTtlFillAry = [];
var gMacOnAry = [];
var gSessIDAry = [];
var gSymSprdAry = [];
var gOnOffAry = [];
var gModeAry = [];
var gRowSeqAry = [];
var gQuoteAry = [];
var gRawMAQuoteAry = [];
var gMAQuoteAry = [];
var gTableMsgAry = [];
var gBgCountTimer;
var gDelayTurnOnTimer;
var gAutoFlatTimer;
var gReadTotalFillTimer;
var gReplyLeaderTimer;
var gSendSessIDTimer;
var gLeaderOnTimer;
var gGuaSlowFedTimer;
var gNoTradeTimer;
var gPauseMarcoTimer;
var gClearQuoteTimer;
var gFlatPosTimer;
var gGuardianTimer;
var gReloadTimer;
var gClearQuoteStatusTimer;
var gPanelShortInterval = false;
var gPanelLongInterval = false;
var gGetLeaderIDInterval = false;
var gCheckParameterInterval = false;
var gGetIPInterval = false;
var gFlatPosInterval = false;
var gReloadInterval = false;
var gWaitCMRInterval = false;
var gLastFillSide = "";
var gSymOpt_MREX = "";
var gSymOpt_PATS = "";
var gSymOpt_PATX = "";
var gSymOpt_HKE = "";
var gEcnList = [ "MREX" , "PATS" , "PATX" , "HKE" ];
var gGotCMR = false;
var gGotWrongQuote = false;
var gIconObj = { doubleUp: "\u23EB" ,
				up: "\u2B06" ,
				down: "\u2B07" ,
				doubleDown: "\u23EC" ,
				minus: "\u2796" ,
				doubleExcMark: "\u203C" ,
				warnning: "\u26A0" ,
				};
var gFAIconObj = { doubleUp: "&#xf102;" ,
				up: "&#xf062;" ,
				down: "&#xf063;" ,
				doubleDown: "&#xf103;" ,
				minus: "&#xf068;" ,
				doubleExcMark: "&#xf06a;" ,
				warnning: "&#xf071;" ,
				};

var gStatusObj = { on: false ,
				pause: false ,
				bidOn: false ,
				askOn: false ,
				bidAuto: false ,
				askAuto: false ,
				reloaded: false ,
				disconnected: false ,
				bidPause: false,
				askPause: false,
				} ;