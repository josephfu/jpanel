function jpanelAlgo( gInitObj , gDir ){
	//// get init object from GFA ////
	var gInitUserID = gInitObj.u;
	var gInitOrdSubscription = gInitObj.oc;
	var gInitSymItem = gInitObj.fsi;
	var gInitOpenOrdCount = gInitObj.ooc;
	var gInitAccPnl = gInitObj.tot;
	var gInitNetPos = gInitObj.onp;
	var gInitMktPx = gInitObj.mp;
	var gInitBidAry = gInitObj.cba;
	var gInitAskAry = gInitObj.caa;
	var gInitNotice = gInitObj.sg;
	var gInitDataStore = gInitObj.ds;

	gSymOpt_MREX = gSym_CME;
	gSymOpt_PATS = gSym_CME;
	gSymOpt_PATX = gSym_CME;
	gSymOpt_HKE = gSym_HKE;

	if ( gHostName == "192.168.1.187" ){ // for m6 testing server setting
		editParmAry( gLocalCfgAry , "ShowConsole" , "on" , true );
		gDefECN = "MREX";
		gTelegramTargetAry = [];
		gTelegramTargetAry[ 0 ]	= { userID : [ gInitUserID ] , target: [ "joseph" ] };
	}

//////////////////////// vue object //////////////

	var gRowHtml = $( ".defMacRow" ).html();

	var vuePanl = new Vue( {
		el: '.confPanlSec' ,
		data: {
			vData: gConfPanelAry ,
		} ,
	} );

	var vueGuard = new Vue( {
		el: '.confGuarSec' ,
		data: {
			vData: gConfGuardAry ,
		} ,
	} );

	var vueFiveSecVolTable = new Vue( {
		el: ".fiveSecVolTableDiv" ,
		data: {
			volMsgAry: gTableMsgAry ,
		} ,
		methods: {
			getPxDirectionClass: function ( obj ) {
				var tdClass = "";

				if ( obj.pxDirection == 1 ){
					tdClass = "tableTdUp";
				}else if ( obj.pxDirection == -1 ){
					tdClass = "tableTdDown";
				}else if ( obj.pxDirection == 0 ){
					tdClass = "tableTdFlat";
				}

				return tdClass;
			} ,
			getVolClass: function ( obj ) {
				var tdClass = "";
				var vol = obj.vol;

				if ( obj.pxDirection == 1 ){
					tdClass = "tableTdUp";
				}else if ( obj.pxDirection == -1 ){
					tdClass = "tableTdDown";
				}else if ( obj.pxDirection == 0 ){
					tdClass = "tableTdFlat";
				}

				if ( vol >= 500 && vol < 1000 ){
					tdClass += " tableLvl1Td";
				}else if ( vol >= 1000 && vol < 2000 ){
					tdClass += " tableLvl2Td";
				}else if ( vol >= 2000 ){
					tdClass += " tableLvl3Td";
				}

				return tdClass;
			} ,
			getPipChgClass: function ( obj ) {
				var tdClass = "";

				if ( obj.pxDirection == 1 ){
					tdClass = "tableTdUp";
				}else if ( obj.pxDirection == -1 ){
					tdClass = "tableTdDown";
				}else if ( obj.pxDirection == 0 ){
					tdClass = "tableTdFlat";
				}

				var chg = Math.abs( obj.pipChg );

				if ( chg >= 3 && chg < 6 ){
					tdClass += " tableLvl1Td";
				}else if ( chg >= 6 && chg < 10 ){
					tdClass += " tableLvl2Td";;
				}else if ( chg >= 10 ){
					tdClass += " tableLvl3Td";
				}
				return tdClass;
			} ,
			getFillDirectionClass: function ( obj ) {
				var tdClass = "";

				if ( obj.bidVol < 10 && obj.askVol < 10 ){
					return tdClass;
				}
				if ( obj.ratio < 0.49 ){
					tdClass = "tableTdUp";
					if ( obj.ratio < 0.15 ){
						tdClass += " tableLvl1Td";
					}
				}else if ( obj.ratio > 0.51 ){
					tdClass = "tableTdDown";
					if ( obj.ratio > 0.85 ){
						tdClass += " tableLvl1Td";
					}
				}else{
					tdClass = "tableTdFlat";
				}
				return tdClass;
			} ,
			getFillRatioClass: function ( obj ) {
				var tdClass = "";

				if ( obj.bidVol < 10 && obj.askVol < 10 ){
					return tdClass;
				}
				if ( obj.ratio < 0.35 ){
					tdClass = "tableTdUp";
					if ( obj.ratio < 0.15 ){
						tdClass += " tableLvl1Td";
					}
				}else if ( obj.ratio > 0.65 ){
					tdClass = "tableTdDown";
					if ( obj.ratio > 0.85 ){
						tdClass += " tableLvl1Td";
					}
				}else{
					tdClass = "tableTdFlat";
				}
				return tdClass;
			} ,
		} ,
	} );

	var vueTickVolTable = new Vue( {
		el: ".tickVolTableDiv" ,
		data: {
			volMsgAry: gTableMsgAry ,
		} ,
		methods: {
			getPxDirectionClass: function ( obj ) {
				var tdClass = "";

				if ( obj.pxDirection == 1 ){
					tdClass = "tableTdUp";
				}else if ( obj.pxDirection == -1 ){
					tdClass = "tableTdDown";
				}else if ( obj.pxDirection == 0 ){
					tdClass = "tableTdFlat";
				}

				return tdClass;
			} ,
			getVolClass: function ( obj ) {
				var tdClass = "";
				var vol = obj.vol;

				if ( obj.pxDirection == 1 ){
					tdClass = "tableTdUp";
				}else if ( obj.pxDirection == -1 ){
					tdClass = "tableTdDown";
				}else if ( obj.pxDirection == 0 ){
					tdClass = "tableTdFlat";
				}

				if ( vol >= 500 && vol < 1000 ){
					tdClass += " tableLvl1Td";
				}else if ( vol >= 1000 && vol < 2000 ){
					tdClass += " tableLvl2Td";
				}else if ( vol >= 2000 ){
					tdClass += " tableLvl3Td";
				}

				return tdClass;
			} ,
			getPipChgClass: function ( obj ) {
				var tdClass = "";

				if ( obj.pxDirection == 1 ){
					tdClass = "tableTdUp";
				}else if ( obj.pxDirection == -1 ){
					tdClass = "tableTdDown";
				}else if ( obj.pxDirection == 0 ){
					tdClass = "tableTdFlat";
				}

				var chg = Math.abs( obj.pipChg );

				if ( chg >= 3 && chg < 6 ){
					tdClass += " tableLvl1Td";
				}else if ( chg >= 6 && chg < 10 ){
					tdClass += " tableLvl2Td";;
				}else if ( chg >= 10 ){
					tdClass += " tableLvl3Td";
				}
				return tdClass;
			} ,
			getFillDirectionClass: function ( obj ) {
				var tdClass = "";

				if ( obj.bidVol < 10 && obj.askVol < 10 ){
					return tdClass;
				}
				if ( obj.ratio < 0.49 ){
					tdClass = "tableTdUp";
					if ( obj.ratio < 0.15 ){
						tdClass += " tableLvl1Td";
					}
				}else if ( obj.ratio > 0.51 ){
					tdClass = "tableTdDown";
					if ( obj.ratio > 0.85 ){
						tdClass += " tableLvl1Td";
					}
				}else{
					tdClass = "tableTdFlat";
				}
				return tdClass;
			} ,
			getFillRatioClass: function ( obj ) {
				var tdClass = "";

				if ( obj.bidVol < 10 && obj.askVol < 10 ){
					return tdClass;
				}
				if ( obj.ratio < 0.35 ){
					tdClass = "tableTdUp";
					if ( obj.ratio < 0.15 ){
						tdClass += " tableLvl1Td";
					}
				}else if ( obj.ratio > 0.65 ){
					tdClass = "tableTdDown";
					if ( obj.ratio > 0.85 ){
						tdClass += " tableLvl1Td";
					}
				}else{
					tdClass = "tableTdFlat";
				}
				return tdClass;
			} ,
		} ,
	} );

	vueOrderCountTable = new Vue( {
		el: ".orderCountTableDiv" ,

		data: {
			orderCountAry: [] ,
		} ,

		methods: {

		} ,
	} );

////////////////////////// initAlgo ////////////////////

	init();

/////////////////////////////// function /////////////////////////

	function init(){
		ee.addListener( "MC_CMR" , readMacro ); // run readMacro when received "CMR" msg
		ee.addListener( "MMO_O_CH_DIS" , GFA_disconnected ); // run GFA_disconnected when GFA is disconnected
		ee.addListener( "MMO_M_M_M_D_E" , readPriceQuote );
		ee.addListener( "e_m_k_c" , readTradeQuote ); // gfa intrinsic

		$.when( getClientIP() ).then( function (){ // wait until client got IP address
			getLocalStore();
			getMacro();
		} );
	}

	function initPanel( cmrAry ){ // start panel
		// e.g. CMR 302 GET_MACRO SESSID 198 SEQ 192.168.0.110::48
		var seq = cmrAry[ 6 ];

		var ip = seq.split( "::" )[ 0 ];
		var num = parseInt( seq.split( "::" )[ 1 ] );

		if ( ip != gIP || num != gRandomNum ){ // check who are getting macro
			return;
		}

		setTimeout( function (){
			loadMacroSwtich();
			showSymSpread();
			applyRowSeq();
			updatePnLStopLable();
			getAllParameter();
			setPanelButton();
			subscribePanelSymbol();
			getMsgData();

			$.when( electLeader() ).then( function (){
				$.when( checkIsLeader() ).then( function (){
					onPanelTimer();

					if ( gIsLeader == 0 ){
						return;
					}

					setStopAll();
					setFuseStopAll();
				} );
			} );
		} , 500 );
	}

	function setPanelButton(){
		if ( !gInited ){
			gInited = true;
		}else{
			return;
		}

		$( ".confPanlSec" ).on( "mouseup" , ".confPanlCfmDiv" , uiConfimPanlCfg );
		$( ".confPanlSec" ).on( "mouseup" , ".confReldDiv" , uiClickReloadAll );
		$( ".confPanlSec" ).on( "mouseup" , ".confReldAllDiv" , uiClickReloadEveryOne );
		$( ".confGuarSec" ).on( "mouseup" , ".confGuarCfmDiv" , uiConfimGuarCfg );
		$( ".confModeSec" ).on( "click" , ".confModeSaveDiv" , uiClickSaveMode );
		$( ".confModeSec" ).on( "click" , ".confModeNewDiv" , uiClickNewMode );
		$( ".confModeSec" ).on( "click" , ".confModeRenameDiv" , uiClickRenameMode );
		$( ".confModeSec" ).on( "click" , ".confModeDeleteDiv" , uiClickDeleteMode );
		$( ".confModeSec" ).on( "click" , ".confModeDeleteAllDiv" , uiClickDeleteAllMode );
		$( ".confModeSec" ).on( "click" , ".confModeCfmDiv" , uiClickModeCfmBtn );
		$( ".confModeSec" ).on( "click" , ".confModeCnlDiv" , uiClickModeCnlBtn );
		$( ".confModeSec" ).on( "click" , ".confModeLi" , uiClickUseMode );
		$( ".confErrorSec" ).on( "click" , ".confErrorCnlBtnDiv" , uiClickCloseErrorDiv );
		$( ".cfmSec" ).on( "click" , ".cfmOnCnlBtn" , hideCfmSec );
		$( ".cfmSec" ).on( "click" , ".cfmOnCfmBtn" , uiClickCfmOnPanel );
		$( ".titleSec" ).on( "mouseup" , ".panlDelAllDiv" , uiClickDeleteAllMacro );
		$( ".titleSec" ).on( "mouseup" , ".panlCfgDiv" , uiClickCfgPanelBtn );
		$( ".titleSec" ).on( "mouseup" , ".panlGuarDiv" , uiClickCfgGuarBtn );
		$( ".titleSec" ).on( "mouseup" , ".panlSwLbl" , uiClickPanlSwitch );
		$( ".titleSec" ).on( "mouseup" , ".panlSortAscBtn" , function (){ uiClickSortRow( 1 ); } );
		$( ".titleSec" ).on( "mouseup" , ".panlSortDescBtn" , function (){ uiClickSortRow( 2 ); } );
		$( ".titleSec" ).on( "dblclick" , ".panlMaxProfitDiv" , renewMaxProfit );
		$( ".confGuarSec" ).on( "change" , ".confOffChk" , uiClickCfgChkOffBox );
		$( ".confGuarSec" ).on( "change" , ".confPauseChk" , uiClickCfgChkPauseBox );
		$( ".confGuarSec" ).on( "change" , ".confFlatAllChk" , uiClickCfgChkFlatAllBox );
		$( ".panlSec" ).on( "mouseup" , ".confRowCfmDiv" , uiConfimRowCfg );
		$( ".panlSec" ).on( "mouseup" , ".rowCfgDiv" , uiClickCfgRowBtn );
		$( ".panlSec" ).on( "mouseup" , ".macSwLbl" , uiClickMacroSwitch );
		$( ".panlSec" ).on( "mouseup" , ".ifdSwLbl" , uiClickIfdSwitch );
		$( ".panlSec" ).on( "mousedown" , hideCfgSec );
		$( ".panlSec" ).on( "mousedown" , ".titleSec" , hideCfgRowSec );
		$( ".panlSec" ).on( "mousedown" , ".rowSec" , hideCfgRowSec );
		$( ".panlSec" ).on( "mouseup" , ".panlAddDiv" , uiClickAddRowBtn );
		$( ".panlSec" ).on( "mouseup" , ".rowDelDiv" , uiClickDelRowBtn );
		$( ".panlSec" ).on( "change" , ".txtIpt" , uiInputTxtValue );
		$( ".panlSec" ).on( "mouseup" , ".txtIpt" , showSlider );
		$( ".panlSec" ).on( "mouseup" , ".ctrlCvr" , showMacOnWarn );
		$( ".panlSec" ).on( "mouseup" , ".rowInfCvr" , showRowOnWarn );
		$( ".panlSec" ).on( "change" , ".ecnSlt" , uiChgRowEcn );
		$( ".panlSec" ).on( "change" , ".symSlt" , uiChgRowSym );
		$( ".panlSec" ).on( "mouseup" , ".rowSprdDiv" , uiClickCfgSpread );
		$( ".panlSec" ).on( "click" , ".sprdBtn" , uiClickChgSpread );
		$( ".panlSec" ).on( "mouseup" , ".confSymSprdCfmDiv" , uiConfimSymSprdCfg );
		$( ".panlSec" ).on( "mouseup" , ".panlModeDiv" , uiClickPanelMode );
		$( ".panlSec" ).on( "mouseup" , function ( evt ){ hideSlider( evt ); } );
		$( ".titleSec" ).on( "mouseup" , ".panlSaveDiv" , updateAllMacro );
		$( ".titleSec" ).on( "mouseup" , ".panlFiveSecVolTableDiv" , uiClickShowFiveSecVolTable );
		$( ".titleSec" ).on( "mouseup" , ".panlTickVolTableDiv" , uiClickShowTickVolTable );
		$( ".titleSec" ).on( "mouseup" , ".panlOrderCountTableDiv" , uiClickShowOrderCountTable );
		$( ".confSec" ).on( "mouseup" , ".closeSecBtn" , uiClickCloseSecDiv );
		$( ".panlSec" ).sortable( { // drag row
			items: ".macRow" ,
			handle: ".rowSortDiv" ,
			axis: "y" ,
			helper: "clone" ,
			containment: ".jPanelDiv" ,
			update: function ( event , ui ) {
				saveRowSeq();
			} ,
		} );
		$( ".confSec " ).resizable( {
			handles: "n , s" ,
			maxHeight: 1000 ,
			minHeight: 400 ,
		} );
		$( ".confSec " ).draggable( { // drag config section
			cancel: ".confModeListUl , .confTxtDiv , .iptDiv , .txtIpt" ,
		} );

		$( document ).keydown( function ( e ){ ctrlKeyDown( e ) } );
		$( document ).keyup( function ( e ){ ctrlKeyUp( e ) } );
		$( document ).keydown( function ( e ){ shiftKeyDown( e ) } );
		$( document ).keyup( function ( e ){ shiftKeyUp( e ) } );
	}

//////////// start , stop , set , delete macro function ///////////

	function getMacro(){
		var sessID = "";
		if ( gSessID != 0 ){
			sessID = gSessID + " ";
		}

		gRandomNum = getRandomNumber();

		var cmd = "CMD " + gInitUserID + " GET_MACRO " + sessID + "SEQ " + gIP + "::" + gRandomNum + "::" + gVersion;
		pushCMD( cmd );
		showLog( "getMacro" , cmd );
		//e.g. CMD 302 GET_MACRO SEQ 192.168.0.110::64::1a
	}

	function readMacro( cmrMsg ){ // handle "CMR" message
		// showLog( "readMacro" , cmrMsg ); // show all cmr msg
		gGotCMR = true;
		var cmrLine = cmrMsg.split( "\n" );

		for ( var i = 0 ; i < cmrLine.length ; i ++ ){
			var cmrAry = cmrLine[ i ].split( " " );
			var msgType = cmrAry[ 0 ];
			var userID = cmrAry[ 1 ];
			var cmrType = cmrAry[ 2 ];
			if ( cmrAry.length < 4 || msgType != "CMR" || userID != gInitUserID || gStatusObj.reloaded || gStatusObj.disconnected ){ // ignore if msg too short , not cmr , not for this user or user was requested to reload.
				return;
			}

			if ( cmrAry[ 3 ].slice( 0 , 7 ) != "algoLp_" ){ // hide algoLp_ msg on con sole log
				showLog( "readMacro" , cmrLine[ i ] );
			}

			if ( cmrType == "MACRO" ){
				var macName = cmrAry[ 3 ];
				var macParm = cmrAry[ 5 ];
				if ( macName.slice( 0 , 7 ) == "algoMac" ){
					try{
						updateRowInfo( cmrAry );
					}catch ( e ){
						showParmErrorNotice( 1 );
						showLog( "catch error" , e );
					}
				}else if ( macName == "_ON_MACRO" ){
					readOnMacroList( cmrAry );
				}else if ( macName == "_LEADER_ID" ){
					readLeaderID( cmrAry );
				}else if ( macName == "_SYM_SPREAD" ){
					readSymSpread( cmrAry );
				}else if ( macName.slice( 0 , 5 ) == "_MODE" ){
					readMode( cmrAry );
				}else if ( macName == "_CURRENT_MODE" ){
					gCurrMode = macParm;
					showCurrentMode( macParm );
				}else if ( macName == "_ROW_SEQ" ){
					readRowSeq( cmrAry );
				}else if ( macName == "_MAX_PROFIT" ){
					readMaxProfit( cmrAry );
				}else if ( macName.split( "_" )[ 1 ] == "conf" ){
					var name = macName.split( "_" )[ 2 ];
					var property = macName.split( "_" )[ 3 ];

					if ( property == "on" || property == "off" || property == "pause" || property == "flat" || property == "flatAll" || property == "notice" ){
						macParm = Boolean( parseInt( macParm ) );
					}

					if ( property == "parm" ){
						macParm = parseFloat( macParm );
					}

					editParmAry( gConfPanelAry , name , property , macParm );

					if ( name == "OrderMethod" && property == "parm" ){
						$( ".confRowOrderMethodDiv" ).each( function (){
							$( this ).find( ".txtIpt" ).val( macParm );
						} );
					}
				}else if ( macName.split( "_" )[ 1 ] == "guard" ){
					var name = macName.split( "_" )[ 2 ];
					var property = macName.split( "_" )[ 3 ];

					if ( property == "on" || property == "off" || property == "pause" || property == "flat" || property == "flatAll" || property == "notice" ){
						macParm = Boolean( parseInt( macParm ) );
					}

					if ( property == "parm" ){
						macParm = parseFloat( macParm );
					}

					editParmAry( gConfGuardAry , name , property , macParm );

					if ( name == "NoTrade" && ( property == "on" || property == "parm" ) ){
						noTradeGuardian();
					}else if ( name == "LossFromTop" ){
						showLossFromTop();
					}else if ( name == "FuseStopQty" && property == "parm" ){
						$( ".stopAllQtySn" ).text( macParm );
					}
				}

			}else if ( cmrType == "MACRO_DELETE" ){
				var macName = cmrAry[ 3 ];
				var macParm = cmrAry[ 5 ];
				if ( macName.slice( 0 , 7 ) == "algoMac" ){
					deleteRow( cmrAry );
				}else if ( macName.slice( 0 , 5 ) == "_MODE" ){
					readDeleteMode( cmrAry );
				}

			}else if ( cmrType == "DO" ){
				cmrAction = cmrAry[ 3 ];
				if ( cmrAction == "_STOPALL" || cmrAction == "_STOPALL2" || cmrAction == "_FUSESTOPALL" ){
					turnnedOffPanel();
				}else if ( cmrAction.slice( 0 , 7 ) == "algoMac" || cmrAction.slice( 0 , 7 ) == "algoCnl" || cmrAction.slice( 0 , 6 ) == "algoLp" ){
					triggerMacro( cmrAry );
				}

			}else if ( cmrType == "ALGO-ON" || cmrType == "ALGO-OFF" ){
				triggerMacro( cmrAry );

			}else if ( cmrType == "MSG" ){
				var msgTopic = cmrAry[ 3 ];

				if ( msgTopic == "MMALGO" ){
					readMsg( cmrAry );
				}else if ( msgTopic == "ALGO" ){
					readPlacedPx( cmrAry );
				}else if ( msgTopic == "SEND_SESSID" ){
					collectSessID( cmrAry );
				}else if ( msgTopic == "GET_SESSID" ){
					sendSessID();
				}else if ( msgTopic == "CALL_LEADER" ){
					replyLeaderID( cmrAry );
				}else if ( msgTopic == "LEADER_ON" ){
					readLeaderOnMsg( cmrAry );
				}else if ( msgTopic == "MMNOTICE" ){
					readNotice( cmrAry );
				}else if ( msgTopic == "MULTIPLE_LEADER" ){
					getNewSessID( cmrAry );
				}else if ( msgTopic == "TOASTR_MSG" ){
					readToastrMsg( cmrAry );
				}else if ( msgTopic == "SOUND_MSG" ){
					readSoundMsg( cmrAry );
				}else if ( msgTopic == "LOG" ){
					var logTopic = cmrAry[ 4 ];
					if ( logTopic == "a_tableMsg" ){
						readTableMsg( cmrAry , 0 );
					}
				}

			}else if ( cmrType == "TOTAL_FILL" ){
				readTotalFill( cmrAry );

			}else if ( cmrType == "GET_MACRO" ){
				getSessID( cmrAry );
				initPanel( cmrAry );

			}else if ( cmrType == "ALGO-RUNNING" ){
				readAlgoRunning( cmrAry );

			}else if ( cmrType == "GET" ){
				readParameter( cmrAry );
			}
		}
	}

	function buildMacro( numStr , side , sym , ecn , qty , pips , updFeq , plcOrdDly , repOrdDly , mCase , spread , ordMethod , ordSpeed , ordType , symTs ){
		if ( ! validateParameter( "numStr" , numStr , numStr ) ||
			! validateParameter( "side" , side , side ) ||
			! validateParameter( "sym" , sym , sym ) ||
			! validateParameter( "ecn" , ecn , ecn ) ||
			! validateParameter( "qty" , qty , qty ) ||
			! validateParameter( "pips" , pips , pips ) ||
			! validateParameter( "updFeq" , updFeq , updFeq ) ||
			! validateParameter( "plcOrdDly" , plcOrdDly , plcOrdDly ) ||
			! validateParameter( "repOrdDly" , repOrdDly , repOrdDly ) ||
			! validateParameter( "mCase" , mCase , mCase ) ||
			! validateParameter( "spread" , spread , spread ) ||
			! validateParameter( "ordMethod" , ordMethod , ordMethod ) ||
			! validateParameter( "ordSpeed" , ordSpeed , ordSpeed ) ||
			! validateParameter( "ordType" , ordType , ordType ) ||
			! validateParameter( "symTs" , symTs , symTs ) ){

			throw new Error( "Parameter is NaN or undefined." ); // avoid sending "NaN" to server
		}

		var macName = "algoMac_" + numStr + "_" + side;
		var cnlName = "algoCnl_" + numStr + "_" + side;
		var lpName = "algoLp_" + numStr + "_" + side;
		var pxName = "algoPx_" + numStr + "_" + side;

		if ( side == "BUY" ){
			var sideNum = 1;
			var bidask = "BID"

			if ( ordType == gLOType ){
				var ordSide = "BUY";
			}else if ( ordType == gSOType ){
				var ordSide = "SELL";
			}
			var flatSide = "SELL";
			var flatSign = "-";

		}else if ( side == "SELL" ){
			var sideNum = 2;
			var bidask = "ASK"

			if ( ordType == gLOType ){
				var ordSide = "SELL";
			}else if ( ordType == gSOType ){
				var ordSide = "BUY";
			}
			var flatSide = "BUY";
			var flatSign = "+";
		}

		var flatPx = gFuseStopPxBuffer * symTs;

		var chkSide = "IF_" + bidask + "_CHECK"; // check whether bid px is checked
		var saveSide = "SAVE" + bidask; // check whether bid px is checked

		var bidaskSuffix = "";
		if ( ordMethod != "Normal" ){
			bidaskSuffix = "_" + ordMethod + "_" + ordSpeed;
			//bidaskSuffix = "_SLOW";
		}

		var bidOrAsk = bidask + bidaskSuffix;
		// e.g. "BID_SLOW_10" or "ASK_MA_20"

		qty = Math.abs( qty );
		if ( mCase == 11 ){
			qty = 1; // only allow user to set qty 1 if it is "stop all and buy/sell" case
		}

		pips = pips + spread;

		if ( pips >= 0 ){
			var sign = "+";
		}else{
			var sign = "-";
		}
		pips = Math.abs( pips );

		var slip = 10;
		//var poi = gInitUserID + numStr + sideNum;
		var poi = numStr + sideNum;
		var ordMac = "DEFAULT SYM " + sym + " O " + ecn + " " + ordType + " " + gOrdTIF + " " + ordSide + " " + sym + " QTY " + qty;
		if ( ordType == gLOType ){
			var ordPrefix = "AT";
			var ordTypeMac = ordPrefix + " " + bidOrAsk + " " + sign + " " + pips;
		}else if ( ordType == gSOType ){
			var ordPrefix = "TRIG";
			var ordTypeMac = ordPrefix + " " + bidOrAsk + " " + sign + " " + pips + " SLIP " + slip + " TS " + symTs + " DIP 1";
		}

		var fuseStopQty = readParmAry( gConfGuardAry , "FuseStopQty" ).parm;

		var macStartMac = "CMD " + gInitUserID + " MACRO " + macName + " NAME " + macName + " DEFAULT MODE " + mCase;
		var macCnlAlgo = "CANCEL_ALGO " + macName;
		var macCnlOrd = "O " + ecn + " CO DAY CANCEL 0 POI " + poi;
		var macSendOrd = ordMac + " " + ordTypeMac;
		var macSavePOI = "POI " + poi;
		var macSavePx = "SAVE " + pxName + " " + ordPrefix;
		var macUpdMac = "DELAY 0 " + updFeq + " { DO " + lpName + " SEQ " + gByMacro + " }";
		var macDoMac = "DO " + macName + " SEQ " + gByMacro;
		var macStopAll = "DO _STOPALL SEQ " + gByMacro;
		var macStopMsg = "MSG " + gInitUserID + " MMALGO { Hit ALGO Fuse. Stopped All Algo. }";
		var macStopNotice = "MSG " + gInitUserID + " MMNOTICE { ALGOFUSE Hit ALGO Fuse. Stopped All Algo. }";

		var macFuseStopAll = "DO _FUSESTOPALL SEQ " + gByMacro;
		var macFlat = "O " + ecn + " " + gLOType + " " + gOrdTIF + " " + flatSide + " " + sym + " QTY " + fuseStopQty + " AT " + pxName + " " + flatSign + " " + flatPx + " SEQ 1";

		// if ( mCase == 1 ){ // Stop on Partial Fill
			// var macro = macStartMac + " " + macCnlAlgo +
				// " | " + macCnlOrd +
				// " | " + macSendOrd + " " + macSavePOI + " " + macSavePx +
				// " | " + macUpdMac +
				// " | IF_DONE 1 " + ecn + " LAST_TRADE_OID 1 { " + macCnlAlgo + " | " + macCnlOrd + " }" +
				// " | IF_DONE 1 " + ecn + " LAST_TRADE_OID 2 { " + macCnlAlgo + " }" +
				// " | IF_DONE 1 " + ecn + " LAST_TRADE_OID 4 { NAME " + macName + " DELAY 0 0 { " + macDoMac + " } }" +
				// " | IF_DONE 1 " + ecn + " LAST_TRADE_OID 8 { " + macCnlAlgo + " }";

		// }else if ( mCase == 2 ){ //stop on Complete Fill only
			// var macro = macStartMac + " " + macCnlAlgo +
				// " | " + macCnlOrd +
				// " | " + macSendOrd + " " + macSavePOI + " " + macSavePx +
				// " | " + macUpdMac +
				// " | IF_DONE 1 " + ecn + " LAST_TRADE_OID 1 { " + macCnlAlgo + " | " + macCnlOrd + " | DELAY 0 " + repOrdDly + " { " + macDoMac + " } }" +
				// " | IF_DONE 1 " + ecn + " LAST_TRADE_OID 4 { NAME " + macName + " DELAY 0 0 { " + macDoMac + " } }" +
				// " | IF_DONE 1 " + ecn + " LAST_TRADE_OID 8 { " + macCnlAlgo + " }";

		if ( mCase == 9 ){ //Non-Stop
			var macro = macStartMac + " " + macCnlAlgo +
				" | " + macCnlOrd +
				" | " + macSendOrd + " " + macSavePOI + " " + macSavePx +
				" | " + macUpdMac +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 1 { " + macCnlOrd + " }" +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 2 { NAME " + macName + " DELAY 0 " + repOrdDly + " { " + macDoMac + " } }" +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 4 { NAME " + macName + " DELAY 0 " + plcOrdDly + " { " + macDoMac + " } }" +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 8 { " + macCnlAlgo + " }";

		}else if ( mCase == 10 ){ // Stop All ALGO
			var macro = macStartMac + " " + macCnlAlgo +
				" | " + macCnlOrd +
				" | " + macSendOrd + " " + macSavePOI + " " + macSavePx +
				" | " + macUpdMac +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 1 { " + macStopAll + " }" +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 2 { " + macStopAll + " | " + macStopMsg + " | " + macStopNotice + " }" +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 4 { NAME " + macName + " DELAY 0 0 { " + macDoMac + " } }" +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 8 { " + macCnlAlgo + " }";

		}else if ( mCase == 11 ){ // Stop All ALGO and BUY/SELL
			var macro = macStartMac + " " + macCnlAlgo +
				" | " + macCnlOrd +
				" | " + macSendOrd + " " + macSavePOI + " " + macSavePx +
				" | " + macUpdMac +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 2 { " + macFuseStopAll + " | " + macFlat + " | " + macStopMsg + " | " + macStopNotice + " }" +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 4 { NAME " + macName + " DELAY 0 0 { " + macDoMac + " } }" +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 8 { " + macCnlAlgo + " }";

		}else if ( mCase == 12 ){ // Stop All ALGO and BUY/SELL
			var macro = macStartMac + " " + macCnlAlgo +
				" | " + macCnlOrd +
				" | " + macSendOrd + " " + macSavePOI + " " + macSavePx +
				" | " + macUpdMac +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 1 { " + macCnlAlgo + " | " + macCnlOrd + " }" +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 2 { " + macCnlAlgo + " }" +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 4 { NAME " + macName + " DELAY 0 0 { " + macDoMac + " } }" +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 8 { " + macCnlAlgo + " }";

		}else{
			return;
		}

		var cmdMac = macro; // algo macro
		var cmdMacIn = "CMD " + gInitUserID + " MACRO_IN KEYR ECN SYM GUI_BID GUI_ASK"; // macro in
		var cmdCnl = "CMD " + gInitUserID + " MACRO " + cnlName + " " + macCnlAlgo +
						" | " + macCnlOrd +
						" | DELAY 0 1000 { " + macCnlOrd + " }"; // stop algo
		var cmdLp = "CMD " + gInitUserID + " MACRO " + lpName + " NAME " + lpName +
						" | DEFAULT SYM " + sym + " O " + ecn + " " + ordType + " " + gOrdTIF + " " + ordTypeMac +
						" | " + "IF { NE AT " + pxName + " } " +
						"{ " + macCnlOrd + " } " +
						"{ NAME " + macName + " DELAY 0 " + updFeq + " { DO " + lpName + " SEQ " + gByMacro + " } }";

		var cmd = { cmdMac: cmdMac , cmdMacIn: cmdMacIn , cmdCnl: cmdCnl , cmdLp: cmdLp };
		return cmd;
	}

	function setMacro( macName , parm ){
		if ( jQuery.type( parm ) == "array" ){
			var parmStr = "{ ";
			for ( var i = 0 ; i < parm.length ; i ++ ) {
				if ( i > 0 ){
					parmStr += " ";
				}
				parmStr += parm[ i ];
				if ( ! validateParameter( macName , parm[ i ] , parm[ i ] ) ){
					throw new Error( "Parameter is NaN or undefined." );
				}
			}
			parmStr = parmStr + " }";
		}else{
			var parmStr = parm;

			if ( ! validateParameter( macName , parm , parm ) ){
				throw new Error( "Parameter is NaN or undefined." );
			}
		}

		getSeqNum( gByFunction );
		var cmd = "CMD " + gInitUserID + " MACRO " + macName + " NAME " + parmStr;
		//var cmd = "CMD " + gInitUserID + " MACRO " + macName + " NAME " + parmStr + " SEQ " + gSeqNum;

		pushCMD( cmd );
		showLog( "setMacro" , cmd );
	}

	function updateMacro( rowID , side ){
		var numStr = rowID.substr( rowID.length - 2 );

		var ecn = $( rowID ).find( ".ecnDiv" ).find( ".current" ).html();
		ecn = ecn.replace( / /g , "" );

		var sym = $( rowID ).find( ".symDiv" ).find( ".current" ).html();
		sym = sym.replace( / /g , "" );

		var symTs = getSymItem( ecn , sym ).ts;
		var symLs = getSymItem( ecn , sym ).ls;

		var symSpread = getSymSpread( ecn , sym );

		if ( ! validateParameter( "symTs" , symTs , symTs ) || ! validateParameter( "symLs" , symLs , symLs ) || ! validateParameter( "symSpread" , symSpread , symSpread ) ){
			return;
		}

		if ( side == "BUY" ){
			var bidOrAsk = ".bidSec";
			var spread = symSpread[ 0 ];
		}else{
			var bidOrAsk = ".askSec";
			var spread = symSpread[ 1 ];
		}

		var qty = parseInt( $( rowID ).find( bidOrAsk ).find( ".ctrlQtyDiv" ).find( ".txtIpt" ).val() );
		var pips = parseInt( $( rowID ).find( bidOrAsk ).find( ".ctrlPipsDiv" ).find( ".txtIpt" ).val() );

		var sPips = pips * symTs;
		var sQty = qty * symLs;
		var sSpread = spread * symTs;

		var mCase = $( rowID ).find( bidOrAsk ).find( ".ifdSwChked" ).find( ".ifdSwIpt" ).attr( "value" );
		var repOrdDly = $( rowID ).find( ".confSec" ).find( ".confRowReplaceDelayDiv" ).find( ".txtIpt" ).val() * gMinSecToSec;
		var updFeq = $( rowID ).find( ".confSec" ).find( ".confRowUpdateFeqDiv" ).find( ".txtIpt" ).val() * gMinSecToSec;
		var plcOrdDly = gDefPlcOrdDly * gMinSecToSec;

		var ordMethod = $( rowID ).find( ".confSec" ).find( ".confRowOrderMethodDiv" ).find( ".confSeltDiv" ).find( ".current" ).html();
		ordMethod = ordMethod.replace( / /g , "" );

		var ordSpeed = $( rowID ).find( ".confSec" ).find( ".confRowOrderMethodDiv" ).find( ".txtIpt" ).val();

		var ordType = $( rowID ).find( ".confSec" ).find( ".confRowOrdTypeDiv" ).find( ".current" ).html();
		ordType = ordType.replace( / /g , "" );

		var cmd = buildMacro( numStr , side , sym , ecn , sQty , sPips , updFeq , plcOrdDly , repOrdDly , mCase , sSpread , ordMethod , ordSpeed , ordType , symTs );

		getSeqNum( gByFunction );

		pushCMD( cmd.cmdMac );
		pushCMD( cmd.cmdMacIn );
		pushCMD( cmd.cmdCnl );
		pushCMD( cmd.cmdLp );

		showLog( "updateMacro" , cmd.cmdMac );
		showLog( "updateMacro" , cmd.cmdCnl );
		showLog( "updateMacro" , cmd.cmdLp );
	}

	function updateAllMacro(){
		$( ".macRow" ).each( function (){
			var rowID = "#" + $( this ).attr( 'id' );
			updateMacro( rowID , "BUY" );
			updateMacro( rowID , "SELL" )
		} );
	}

	function restartAllMacro(){
		$( ".macRow" ).each( function (){
			var rowID = "#" + $( this ).attr( 'id' );
			restartMacro( rowID , "BUY" );
			restartMacro( rowID , "SELL" )
		} );
	}

	function startMacro( macName ){
		getSeqNum( gByManual );
		var cmd = "CMD " + gInitUserID + " DO " + macName + " SEQ " + gSeqNum;

		pushCMD( cmd );
		showLog( "startMacro" , cmd );
	}

	function stopMacro( cnlName ){
		getSeqNum( gByManual );
		var cmd = "CMD " + gInitUserID + " DO " + cnlName + " SEQ " + gSeqNum;

		pushCMD( cmd );
		showLog( "stopMacro" , cmd );
	}

	function restartMacro( rowID , side ){
		if ( side == "BUY" ){
			var bidOrAsk = ".bidSec";
		}else{
			var bidOrAsk = ".askSec";
		}

		var swDiv = $( rowID ).find( bidOrAsk ).find( ".macSw" );
		if ( !$( swDiv ).find( ".swOnLbl" ).hasClass( "swMacOn" ) ){
			return;
		}

		var numStr = rowID.substr( rowID.length - 2 );
		var macName = "algoMac_" + numStr + "_" + side;
		var cnlName = "algoCnl_" + numStr + "_" + side;

		stopMacro( cnlName );
		setTimeout( function (){
			startMacro( macName );
		} , 100 );
	}

	function startAllMacro(){
		for ( var i = 0 ; i < gMacOnAry.length ; i ++ ){
			var num = gMacOnAry[ i ].split( "_" )[ 1 ];
			var side = gMacOnAry[ i ].split( "_" )[ 2 ];
			var rowID = "#macRow_" + num;

			if ( $( rowID ).length == 0 ){
				continue;
			}

			if ( side == "BUY" ){
				var bidOrAsk = ".bidSec";
			}else{
				var bidOrAsk = ".askSec";
			}

			var macName = "algoMac_" + num + "_" + side;
			startMacro( macName );
		}
	}

	function deleteMacro( name ){
		//var cmd = "CMD " + gInitUserID + " MACRO_DELETE " + name + " SEQ " + gSeqNum;
		var cmd = "CMD " + gInitUserID + " MACRO_DELETE " + name;

		pushCMD( cmd );
		showLog( "deleteMacro" , cmd );
	}

	function triggerMacro( cmrAry ){
		var cmrType = cmrAry[ 2 ];
		var cmrAction = cmrAry[ 3 ];
		var numStr = cmrAction.split( "_" )[ 1 ];
		var side = cmrAction.split( "_" )[ 2 ];
		var rowID = "#macRow_" + numStr;

		if ( side == "BUY" ){
			var secSide = ".bidSec";
		}else if ( side == "SELL" ){
			var secSide = ".askSec";
		}

		if ( cmrType == "DO" ){
			if ( cmrAction.slice( 0 , 7 ) == "algoMac" ){
				trunOnMacroSwitch( rowID , secSide );
				flashMacroLight( rowID , secSide );
			}else if ( cmrAction.slice( 0 , 7 ) == "algoCnl" ){
				trunOffMacroSwtich( rowID , secSide );
			}else if ( cmrAction.slice( 0 , 6 ) == "algoLp" ){
				trunOnMacroSwitch( rowID , secSide );
				flashMacroLight( rowID , secSide );
			}

		}else if ( cmrType == "ALGO-ON" ){
			trunOnMacroSwitch( rowID , secSide );

		}else if ( cmrType == "ALGO-OFF" ){
			trunOffMacroSwtich( rowID , secSide );
		}

		showBgAlgoCount();
	}

	function uiClickPanlSwitch(){
		var swVal = $( this ).find( ".swIpt" ).val();
		var click = false;

		if ( swVal == "Off" || swVal == "Flat" ){
			click = true;

			turnOffPanel();
			triggerOffRowGuardian();
		}

		if ( swVal == "Flat" ){
			var squarePct = 100;
			var cutSlip = 10;

			clearTimeout( gAutoFlatTimer ); // clear previous timer

			//var flatQty = readParmAry( gConfGuardAry , "AutoFlatQty" ).parm;
			var flatQty = 100;

			gAutoFlatTimer = setTimeout( function (){
				squarePosition( flatQty , gDefAutoFlatSlip );
			} , 1000 ); // wait 1 second to ensure _STOPALL is completed.

		}else if ( swVal == "On" ){
			if ( $( this ).hasClass( "panlSwChked" ) ){
				return;
			}
			if ( !onAlgoGuardian() ){
				return; // check whether has open order or not
			}

			click = true;
			turnOnPanel();
			restartAllGuardianIfTouch();
		}

		if ( click ){
			sendResetTriggered();
			setParameter( "_aStatus_pause" , 0 );
		}
	}

	function turnOnPanel(){ // turn on algo
		try{
			updateAllMacro();
			setParameter( "_aStatus_on" , 1 );

			startAllMacro();

			var msg = "Trun On Algo. Seq No.: " + gSeqNum + ".";
			sendTabMsg( msg );

			autoCheckChangeMode(); // check mode

		}catch ( e ){ // show error when turnning on panel button
			showParmErrorNotice( 2 );
			showLog( "catch error" , e );
		}
	}

	function turnOffPanel(){
		startStopAll( gByManual );

		setParameter( "_aStatus_on" , 0 );

		gStatusObj.on = false;

		var msg = "Trun Off Algo. Seq No.: " + gSeqNum + ".";
		sendTabMsg( msg );
	}

	function turnnedOffPanel(){
		clearTimeout( gDelayTurnOnTimer );
		trunOffAllMacroSwitch();
	}

	function uiClickCfmOnPanel(){ // confirm trun on panel even some algo are still runnning
		hideCfmSec();
		turnOnPanel();
	}

	function trunPanelSwitch( parm ){ // switch on or off the Big on off switch
		var panlSwBg = $( ".panlSwDiv" ).find( ".panlSwBg" );
		var swOnLbl = $( ".panlSwDiv" ).find( ".swOnLbl" );
		var swOffLbl = $( ".panlSwDiv" ).find( ".swOffLbl" )

		$( panlSwBg ).stop( true , true );
		if ( parm ){
			if ( $( swOnLbl ).hasClass( "panlSwChked" ) ){
				return;
			}
			$( panlSwBg ).css( { "left":"-240px" , "width":"33.33%" } );
			$( swOnLbl ).addClass( "panlSwChked" , 300 );
			$( swOffLbl ).removeClass( "panlSwChked" , 300 );

			showSymSpread(); // show symbol spread
			onPanelTimer();

		}else{
			if ( $( swOffLbl ).hasClass( "panlSwChked" ) ){
				return;
			}
			$( panlSwBg ).css( { "left":"-160px" , "width":"66.66%" } );
			$( swOnLbl ).removeClass( "panlSwChked" , 300 );
			$( swOffLbl ).addClass( "panlSwChked" , 300 );
		}
	}

	function pauseAlgo( rMsg ){
		startStopAll( gByFunction );
		setParameter( "_aStatus_pause" , 1 );
		var msg = "Pause Algo. " + rMsg;
		sendTabMsg( msg );
	}

	function resumeAlgo(){
		setParameter( "_aStatus_on" , 1 );
		setParameter( "_aStatus_pause" , 0 );
		autoCheckChangeMode();
		startAllMacro();

		var msg = "Resume Algo. ";
		sendTabMsg( msg );
	}

	function trunOffAllMacroSwitch(){
		if ( gStatusObj.on ){
			setParameter( "_aStatus_on" , 0 );
		}

		$( ".infPxDiv" ).find( ".infDiv" ).find( ".txtIpt" ).val( 0 );
		$( ".panlStopNtcSec " ).fadeOut();
		//hideBgAlgoCount();

		var rowID;
		$( ".macRow" ).each( function (){
			rowID = "#" + $( this ).attr( "id" );
			trunOffMacroSwtich( rowID , ".bidSec" );
			trunOffMacroSwtich( rowID , ".askSec" );
		} );
	}

	function uiClickMacroSwitch(){ // click on or off button for a macro by user
		var macSwitch = this;

		if ( gStatusObj.on && $( macSwitch ).hasClass( "swMacOn" ) ){
			return;
		}else if ( !gStatusObj.on && $( macSwitch ).hasClass( "swChked" ) ){
			return;
		}

		if ( $( macSwitch ).closest( ".sideSec" ).hasClass( "bidSec" ) ){
			var side = "BUY";
		}else{
			var side = "SELL";
		}

		var swVal = $( macSwitch ).find( ".swIpt" ).val();
		var rowID = "#" + $( macSwitch ).closest( ".macRow" ).attr( "id" );
		var numStr = rowID.substr( rowID.length - 2 );
		var macName = "algoMac_" + numStr + "_" + side;
		var cnlName = "algoCnl_" + numStr + "_" + side;

		if ( !gStatusObj.on ){ // check whether the big on off button is on or not
			if ( swVal == "On" ){
				addOnMacroList( macName ); // store on off information to a list
			}else{
				removeOnMacroList( macName );
			}

		}else{
			if ( swVal == "On" ){
				try{
					updateMacro( rowID , "BUY" ); // update both bid side and ask side macros before turn macro on
					updateMacro( rowID , "SELL" );
					addOnMacroList( macName );
					startMacro( macName );
				}catch ( e ){
					showLog( "catch error" , e );
				}
			}else if ( swVal == "Off" ){
				removeOnMacroList( macName );
				stopMacro( cnlName );
			}
		}
	}

	function trunOnMacroSwitch( rowID , side ){
		var swDiv = $( rowID ).find( side ).find( ".macSw" );
		if ( $( swDiv ).find( ".swOnLbl" ).hasClass( "swMacOn" ) ){
			return;
		}

		$( swDiv ).find( ".swOnLbl" ).addClass( "swMacOn" );
		$( swDiv ).siblings( ".macSec" ).find( ".ctrlCvr" ).show();
		$( rowID ).find( ".rowInfCvr" ).show();
	}

	function trunOffMacroSwtich( rowID , side ){
		var swDiv = $( rowID ).find( side ).find( ".macSw" );
		if ( !$( swDiv ).find( ".swOnLbl" ).hasClass( "swMacOn" ) ){
			return;
		}
		$( swDiv ).find( ".swOnLbl" ).removeClass( "swMacOn" );
		$( swDiv ).siblings( ".macSec" ).find( ".ctrlCvr" ).hide();
		$( rowID ).find( ".rowInfCvr" ).hide();
		$( rowID ).find( side ).find( ".infPxDiv" ).find( ".txtIpt" ).attr( "value" , 0 );
	}

	function addOnMacroList( macName ){ // save macro on off information to a macro
		var newMac = 1;
		for ( var i = 0 ; i < gMacOnAry.length ; i ++ ){
			if ( gMacOnAry[ i ] == macName ){
				newMac = 0;
				break;
			}
		}
		if ( newMac == 1 ){
			gMacOnAry.push( macName );
		}

		setMacro( "_ON_MACRO" , gMacOnAry );

		// e.g. CMD 302 MACRO _ON_MACRO NAME { algoMac_02_SELL algoMac_01_SELL }
	}

	function removeOnMacroList( macName ){
		var index = gMacOnAry.indexOf( macName );
		if ( index == -1 ) {
			return;
		}

		gMacOnAry.splice( index , 1 );

		setMacro( "_ON_MACRO" , gMacOnAry );
	}

	function readOnMacroList( cmrAry ){ // read macro on off information and store it to a array
		// e.g. CMR 302 MACRO _ON_MACRO NAME { algoMac_02_SELL algoMac_01_SELL }
		gMacOnAry = [];
		//for ( var i = 6 ; i < cmrAry.length - 3 ; i ++ ){
		for ( var i = 6 ; i < cmrAry.length - 1 ; i ++ ){
			var macName = cmrAry[ i ];
			gMacOnAry.push( macName );
		}

		loadMacroSwtich();
	}

	function loadMacroSwtich(){ // turn on macro if it was on
		$( ".macSw" ).each( function (){
			$( this ).find( ".swOnLbl" ).removeClass( "swChked" );
			$( this ).find( ".swOffLbl" ).addClass( "swChked" );
		} );

		for ( var i = 0 ; i < gMacOnAry.length ; i ++ ){
			var num = gMacOnAry[ i ].split( "_" )[ 1 ];
			var side = gMacOnAry[ i ].split( "_" )[ 2 ];
			var rowID = "#macRow_" + num;
			if ( side == "BUY" ){
				var bidOrAsk = ".bidSec";
			}else{
				var bidOrAsk = ".askSec";
			}
			$( rowID ).find( bidOrAsk ).find( ".macSw" ).find( ".swOffLbl" ).removeClass( "swChked" );
			$( rowID ).find( bidOrAsk ).find( ".macSw" ).find( ".swOnLbl" ).addClass( "swChked" );
		}
	}

	function setStopAll(){ // to cancel all orders and cancel all running algos
		var cnl = "O MREX CO DAY CANCEL ALL" + gLOType + " | O PATS CO DAY CANCEL ALL" + gLOType + " | O PATX CO DAY CANCEL ALL" + gLOType + " | O HKE CO DAY CANCEL ALL" + gLOType + " | O MREX CO DAY CANCEL ALL" + gSOType + " | O PATS CO DAY CANCEL ALL" + gSOType + " | O PATX CO DAY CANCEL ALL" + gSOType + "| O HKE CO DAY CANCEL ALL" + gSOType;

		var cmd = "CMD " + gInitUserID + " MACRO _STOPALL NAME _STOPALL CANCEL_ALGO ALL | " + cnl +
					" | DELAY 0 1000 { " + cnl + " }";

		if ( gInitUserID == 528 || gInitUserID == 529 ){
			cmd = "CMD " + gInitUserID + " MACRO _STOPALL NAME _STOPALL CANCEL_ALGO ALL"; // dont cancel dicky's orders when turn off algo
		}

		pushCMD( cmd );
		showLog( "setStopAll" , cmd );

		// e.g. CMD 302 MACRO _STOPALL NAME _STOPALL CANCEL_ALGO ALL | O MREX CO DAY CANCEL ALLLO2 | O PATS CO DAY CANCEL ALLLO2 | O HKE CO DAY CANCEL ALLLO2 | O MREX CO DAY CANCEL ALLSO2 | O PATS CO DAY CANCEL ALLSO2 | O HKE CO DAY CANCEL ALLSO2 | MSG 302 MMALGO { Stopped All Algo. } | DELAY 0 1000 { O MREX CO DAY CANCEL ALLLO2 | O PATS CO DAY CANCEL ALLLO2 | O HKE CO DAY CANCEL ALLLO2 | O MREX CO DAY CANCEL ALLSO2 | O PATS CO DAY CANCEL ALLSO2 | O HKE CO DAY CANCEL ALLSO2 }
	}

	function setFuseStopAll(){
		var cnl = "O MREX CO DAY CANCEL ALL" + gLOType + " | O PATS CO DAY CANCEL ALL" + gLOType + " | O PATX CO DAY CANCEL ALL" + gLOType + " | O HKE CO DAY CANCEL ALL" + gLOType + " | O MREX CO DAY CANCEL ALL" + gSOType + " | O PATS CO DAY CANCEL ALL" + gSOType + " | O PATX CO DAY CANCEL ALL" + gLOType + " | O HKE CO DAY CANCEL ALL" + gSOType;

		var cmd = "CMD " + gInitUserID + " MACRO _FUSESTOPALL NAME _FUSESTOPALL CANCEL_ALGO ALL | " + cnl;

		pushCMD( cmd );
		showLog( "setFuseStopAll" , cmd );
	}

	function startStopAll( type ){ // call STOPALL macro
		$( ".panlStopNtcSec" ).stop( true , true );
		$( ".panlStopNtcSec" ).fadeIn();

		getSeqNum( type );

		var cmd = "CMD " + gInitUserID + " DO _STOPALL" + " SEQ " + gSeqNum;

		pushCMD( cmd );
		showLog( "startStopAll" , cmd );
	}

	function setExtraStopAll(){ // a extra stop for handling MREX problem
		var cnl = "";
		var i = 0;

		$( ".macRow" ).each( function (){
			var rowID = "#" + $( this ).attr( 'id' );
			var numStr = rowID.substr( rowID.length - 2 );
			var ecn = $( rowID ).find( ".ecnDiv" ).find( ".current" ).html();
			ecn = ecn.replace( / /g , "" );
			var poi = gInitUserID + numStr;

			if ( i > 0 ){
				cnl += " | ";
			}
			var ord = "O " + ecn + " CO DAY CANCEL 0 POI " + poi;
			cnl += ord + "1" + " | " + ord + "2";

			i ++;
		} );

		var cmd = "CMD " + gInitUserID + " MACRO _STOPALL2 NAME _STOPALL2 CANCEL_ALGO ALL | " + cnl + " | MSG " + gInitUserID + " MMALGO { Stopped All Algo. } | DELAY 0 1000 { " + cnl + " }";

		pushCMD( cmd );
		showLog( "setExtraStopAll" , cmd );
	}

	function startExtraStopAll( type ){ // call extra STOPALL macro
		$( ".panlStopNtcSec" ).stop( true , true );
		$( ".panlStopNtcSec" ).fadeIn();

		getSeqNum( type );
		//var cmd = "CMD " + gInitUserID + " DO _STOPALL2" + " SEQ " + gSeqNum;
		var cmd = "CMD " + gInitUserID + " DO _STOPALL2";

		pushCMD( cmd );
		showLog( "startExtraStopAll" , cmd );
	}

	function stopExtraMacro(){
		if ( !gStatusObj.on ) { // check the big on off switch is on or not
			return;
		}

		$( ".swMacOn" ).each( function (){
			var swOnLbl = $( this );

			var rowID = "#" + $( swOnLbl ).closest( ".macRow" ).attr( "id" );
			var numStr = rowID.substr( rowID.length - 2 );

			if ( $( swOnLbl ).closest( ".sideSec" ).hasClass( "bidSec" ) ){
				var side = "BUY";
			}else{
				var side = "SELL";
			}

			var macName = "algoMac_" + numStr + "_" + side;

			if ( gMacOnAry.indexOf( macName ) == -1 ){
				var cnlName = "algoCnl_" + numStr + "_" + side;
				stopMacro( cnlName );
			}
		} );
	}

	function turnOnExtraMacro( type ){ // turn on extra macro when change mode
		if ( !gStatusObj.on ) { // check the big on off switch is on or not
			return;
		}

		var fillSide = gLastFillSide;
		var delayed = false;

		onMacro( type ); // if filled bid > turn on ask side macro first

		if ( !delayed ){
			return;
		}

		gDelayTurnOnTimer = setTimeout( function (){
			fillSide = "";
			onMacro( type ); // after delay , turn on another side macro
		} , gDelayTurnOn );

		function onMacro( type ){
			for ( var i = 0 ; i < gMacOnAry.length ; i ++ ){
				var macName = gMacOnAry[ i ];
				var numStr = macName.split( "_" )[ 1 ];
				var side = macName.split( "_" )[ 2 ];
				var rowID = "#macRow_" + numStr;

				if ( side == "BUY" ){
					var bidOrAsk = ".bidSec";
				}else{
					var bidOrAsk = ".askSec";
				}

				var swOnLbl = $( rowID ).find( bidOrAsk ).find( ".macSw" ).find( ".swOnLbl" );

				if ( $( swOnLbl ).hasClass( "swMacOn" ) ){
					continue;
				}

				if ( type = "Auto" && fillSide == side ){
					delayed = true;
					continue;
				}
				startMacro( macName );
			}
		}
	}

	function uiClickAddRowBtn(){ // add a macro row by user
		var rowNum = gLastMmoNum + 1;
		var pips = gDefPips * gDefSymTs;

		if ( rowNum < 10 ){
			var rowNumStr = "0" + rowNum;
		}else{
			var rowNumStr = rowNum;
		}

		var plcOrdDly = gDefPlcOrdDly * 1000;
		var updFeq = readParmAry( gConfPanelAry , "UpdateFeq" ).parm * 1000;
		var repOrdDly = readParmAry( gConfPanelAry , "ReplaceDelay" ).parm * 1000;
		var ecn = gDefECN;
		var sym = gDefSym;
		var qty = gDefQty;
		var mCase = gDefCase;
		var spread = 0;
		var ordMethod = readParmAry( gConfPanelAry , "OrderMethod" ).select;
		var ordSpeed = readParmAry( gConfPanelAry , "OrderMethod" ).parm;
		var ordType = gDefOrdType;
		var symTs = gDefSymTs;

		for ( var i = 0 ; i < 2 ; i ++ ){
			if ( i == 0 ){
				var side = "BUY";
				var sidePips = pips * -1;
			}else{
				var side = "SELL";
				var sidePips = pips;
			}

			var cmd = buildMacro( rowNumStr , side , sym , ecn , qty , sidePips , updFeq , plcOrdDly , repOrdDly , mCase , spread , ordMethod , ordSpeed , ordType , symTs );

			pushCMD( cmd.cmdMac ); // create two new macros ( buy and sell )
			pushCMD( cmd.cmdMacIn );
			pushCMD( cmd.cmdCnl );
			pushCMD( cmd.cmdLp );

			showLog( "uiClickAddRowBtn" , cmd.cmdMac );
			showLog( "uiClickAddRowBtn" , cmd.cmdCnl );
			showLog( "uiClickAddRowBtn" , cmd.cmdLp );
		}

		var rowID = "macRow_" + rowNumStr;
		addRowSeq( rowID );
	}

	function addRow( numStr ){ // add a new row if it is not exist
		if ( parseInt( numStr ) > gLastMmoNum ){
			gLastMmoNum = parseInt( numStr );
		}

		var rowHtml = "<div class = 'macRow''>" + gRowHtml + "</div>";
		$( ".panlSec" ).append( rowHtml );

		var macRow = $( ".panlSec" ).find( ".macRow:last" );
		$( macRow ).attr( 'id' , "macRow_" + numStr );
		setSldr( ".ctrlPipsDiv" , gDefPipsMin , gDefPipsMax , gDefPips ); // create drag slider for controls
		setSldr( ".ctrlQtyDiv" , gDefQtyMin , gDefQtyMax , gDefQty );

		$( macRow ).find( ".sldrBg" ).slider( { // set the slider
			orientation: "vertical" ,
			range: "min" ,
			create: function (){
				var val = $( this ).slider( "value" );
				$( this ).find( ".sldrBg" ).text( val );
			} ,
			slide: function ( event , ui ){
				var bidSign = "";
				var askSign = "";
				if ( $( this ).closest( ".ctrlDiv" ).hasClass( "ctrlPipsDiv" ) ){
					var ctrl = ".ctrlPipsDiv";
					bidSign = "-";
					askSign = "+";
				}else if ( $( this ).closest( ".ctrlDiv" ).hasClass( "ctrlQtyDiv" ) ){
					var ctrl = ".ctrlQtyDiv";
				}

				var rowID = "#" + $( this ).closest( ".macRow" ).attr( "id" );
				slideSldr( ".bidSec" , bidSign );
				slideSldr( ".askSec" , askSign );

				function slideSldr( side , sign ){
					var swDiv = $( rowID ).find( side ).find( ".macSw" );
					if ( $( swDiv ).find( ".swOnLbl" ).hasClass( "swMacOn" ) ){
						return;
					}
					$( rowID ).find( side ).find( ctrl ).find( ".txtIpt" ).val( sign + ui.value );
					$( rowID ).find( side ).find( ctrl ).find( ".sldrHd" ).text( sign + ui.value );
				}
			} ,
		} );

		var symECN = eval( "gSymOpt_" + gDefECN );
		var symList = "";

		$.each( symECN , function ( value ) {
			symList += "<option value = " + value + "> "+ value + "</option>";
		} );

		$( macRow ).find( ".symSlt" ).append( symList );
		$( macRow ).find( ".ecnSlt" ).val( gDefECN );
		$( macRow ).find( ".symSlt" ).val( gDefSym );
		$( macRow ).find( ".ecnSlt" ).foundationCustomForms(); // call foundationCustomForms to apply the change after select form was changed
		$( macRow ).find( ".symSlt" ).foundationCustomForms();

		$( macRow ).find( ".sprdSlidr" ).dragslider( { // create symbol spread slider
			range: true ,
			rangeDrag: true ,
			min: - ( gSpreadMax * 2 ) ,
			max: gSpreadMax * 2 ,
			values: [ -gSpreadMid , gSpreadMid ] ,

			slide: function ( event , ui ){
				var sldr = $( this );
				var bid = ui.values[ 0 ];
				var ask = ui.values[ 1 ];
				var rowID = "#" + $( sldr ).closest( ".macRow" ).attr( "id" );

				var bidAskVal = getMinMaxBidAskPips( rowID );
				var maxBid = bidAskVal.maxBid;
				var minAsk = bidAskVal.minAsk;

				var newBid = bid + gSpreadMid;
				var newAsk = ask - gSpreadMid;

				if ( ( newBid + maxBid ) > 0 ){
					return false;
				}
				if ( ( newAsk + minAsk ) < 0 ){
					return false;
				}

				$( sldr ).closest( ".confSymSprdDiv" ).find( ".bidSprdDiv" ).find( ".txtIpt" ).val( newBid );
				$( sldr ).closest( ".confSymSprdDiv" ).find( ".askSprdDiv" ).find( ".txtIpt" ).val( newAsk );
			}
		} );

		$( macRow ).find( ".stopAllQtySn" ).text( readParmAry( gConfGuardAry , "FuseStopQty" ).parm );
		$( macRow ).show();

		function setSldr( ctrlClass , minVal , MaxVal , Val ){
			$( macRow ).find( ctrlClass ).find( ".sldrBg" ).slider( {
				min: minVal ,
				max: MaxVal ,
				value: Val ,
			} );
			var bidSign = "";
			var askSign = "";

			if ( ctrlClass == ".ctrlPipsDiv" ){
				bidSign = "-";
				askSign = "+";
			}
			$( macRow ).find( ".bidSec" ).find( ctrlClass ).find( ".txtIpt" ).val( bidSign + Val );
			$( macRow ).find( ".askSec" ).find( ctrlClass ).find( ".txtIpt" ).val( askSign + Val );
			$( macRow ).find( ".bidSec" ).find( ctrlClass ).find( ".sldrHd" ).text( bidSign + Val );
			$( macRow ).find( ".askSec" ).find( ctrlClass ).find( ".sldrHd" ).text( askSign + Val );
		}
	}

	function updateRowInfo( cmrAry ){ // hande algo macro message
		// e.g. CMD 302 MACRO algoMac_01_BUY NAME algoMac_01_BUY DEFAULT MODE 9 CANCEL_ALGO algoMac_01_BUY | O MREX CO DAY CANCEL 0 POI 302011 SAVEBID algoPx_01_BUY | DEFAULT SYM CME/CMX_GLD/DEC17 O MREX LO2 DAY BUY CME/CMX_GLD/DEC17 QTY 2 AT BID - 0.1 POI 302011 | DELAY 0 3000 { DO algoLp_01_BUY SEQ 0 } | IF_DONE 1 MREX LAST_TRADE_OID 1 { O MREX CO DAY CANCEL 0 POI 302011 } | IF_DONE 1 MREX LAST_TRADE_OID 2 { NAME algoMac_01_BUY DELAY 0 6000 { DO algoMac_01_BUY SEQ 0 } } | IF_DONE 1 MREX LAST_TRADE_OID 4 { NAME algoMac_01_BUY DELAY 0 0 { DO algoMac_01_BUY SEQ 0 } } | IF_DONE 1 MREX LAST_TRADE_OID 8 { CANCEL_ALGO algoMac_01_BUY }
		var macName = cmrAry[ 3 ];
		var numStr = macName.split( "_" )[ 1 ];
		var side = macName.split( "_" )[ 2 ];
		var mCase = parseFloat( cmrAry[ 8 ] );
		var repOrdDly = readParmAry( gConfPanelAry , "ReplaceDelay" ).parm;

		var ecn = "";
		var ordType = "";
		var ordTIF = "";
		var ordSide = "";
		var sym = "";
		var qty = "";
		var ordMethod = "";
		var sign = "";
		var pips = "";
		var slip = "";
		var symTs = "";
		var updFeq = "";

		if ( mCase == 1 ){ // Stop on Partial Fill
			ecn = cmrAry[ 25 ];
			ordType = cmrAry[ 26 ];
			ordTIF = cmrAry[ 27 ];
			ordSide = cmrAry[ 28 ];
			sym = cmrAry[ 29 ];
			qty = parseFloat( cmrAry[ 31 ] );
			ordMethod = cmrAry[ 33 ];
			sign = cmrAry[ 34 ];
			pips = parseFloat( cmrAry[ 35 ] );
			if ( ordType == gLOType ){
				updFeq = parseFloat( cmrAry[ 44 ]/1000 );
			}else if ( ordType == gSOType ){
				slip = parseFloat( cmrAry[ 37 ] );
				symTs = parseFloat( cmrAry[ 39 ] );
				updFeq = parseFloat( cmrAry[ 50 ]/1000 );
			}

		}else if ( mCase == 2 ){ // stop on Complete Fill only
			ecn = cmrAry[ 25 ];
			ordType = cmrAry[ 26 ];
			ordTIF = cmrAry[ 27 ];
			ordSide = cmrAry[ 28 ];
			sym = cmrAry[ 29 ];
			qty = parseFloat( cmrAry[ 31 ] );
			ordMethod = cmrAry[ 33 ];
			sign = cmrAry[ 34 ];
			pips = parseFloat( cmrAry[ 35 ] );
			if ( ordType == gLOType ){
				updFeq = parseFloat( cmrAry[ 44 ]/1000 );
				repOrdDly = parseFloat( cmrAry[ 72 ]/1000 );
			}else if ( ordType == gSOType ){
				slip = parseFloat( cmrAry[ 37 ] );
				symTs = parseFloat( cmrAry[ 39 ] );
				updFeq = parseFloat( cmrAry[ 50 ]/1000 );
				repOrdDly = parseFloat( cmrAry[ 78 ]/1000 );
			}

		}else if ( mCase == 9 ){ // Non-Stop
			ecn = cmrAry[ 25 ];
			ordType = cmrAry[ 26 ];
			ordTIF = cmrAry[ 27 ];
			ordSide = cmrAry[ 28 ];
			sym = cmrAry[ 29 ];
			qty = parseFloat( cmrAry[ 31 ] );
			ordMethod = cmrAry[ 33 ];
			sign = cmrAry[ 34 ];
			pips = parseFloat( cmrAry[ 35 ] );
			if ( ordType == gLOType ){
				updFeq = parseFloat( cmrAry[ 44 ]/1000 );
				repOrdDly = parseFloat( cmrAry[ 78 ]/1000 );
			}else if ( ordType == gSOType ){
				slip = parseFloat( cmrAry[ 37 ] );
				symTs = parseFloat( cmrAry[ 39 ] );
				updFeq = parseFloat( cmrAry[ 50 ]/1000 );
				repOrdDly = parseFloat( cmrAry[ 84 ]/1000 );
			}

		}else if ( mCase == 10 ){ // Stop All ALGO
			ecn = cmrAry[ 25 ];
			ordType = cmrAry[ 26 ];
			ordTIF = cmrAry[ 27 ];
			ordSide = cmrAry[ 28 ];
			sym = cmrAry[ 29 ];
			qty = parseFloat( cmrAry[ 31 ] );
			ordMethod = cmrAry[ 33 ];
			sign = cmrAry[ 34 ];
			pips = parseFloat( cmrAry[ 35 ] );
			if ( ordType == gLOType ){
				updFeq = parseFloat( cmrAry[ 44 ]/1000 );
			}else if ( ordType == gSOType ){
				slip = parseFloat( cmrAry[ 37 ] );
				symTs = parseFloat( cmrAry[ 39 ] );
				updFeq = parseFloat( cmrAry[ 50 ]/1000 );
			}

		}else if ( mCase == 11 ){ // Stop All and flat
			ecn = cmrAry[ 25 ];
			ordType = cmrAry[ 26 ];
			ordTIF = cmrAry[ 27 ];
			ordSide = cmrAry[ 28 ];
			sym = cmrAry[ 29 ];
			qty = parseFloat( cmrAry[ 31 ] );
			ordMethod = cmrAry[ 33 ];
			sign = cmrAry[ 34 ];
			pips = parseFloat( cmrAry[ 35 ] );
			if ( ordType == gLOType ){
				updFeq = parseFloat( cmrAry[ 44 ]/1000 );
			}else if ( ordType == gSOType ){
				slip = parseFloat( cmrAry[ 37 ] );
				symTs = parseFloat( cmrAry[ 39 ] );
				updFeq = parseFloat( cmrAry[ 50 ]/1000 );
			}

		}else if ( mCase == 12 ){ // Guardian Algo
			ecn = cmrAry[ 25 ];
			ordType = cmrAry[ 26 ];
			ordTIF = cmrAry[ 27 ];
			ordSide = cmrAry[ 28 ];
			sym = cmrAry[ 29 ];
			qty = parseFloat( cmrAry[ 31 ] );
			ordMethod = cmrAry[ 33 ];
			sign = cmrAry[ 34 ];
			pips = parseFloat( cmrAry[ 35 ] );
			if ( ordType == gLOType ){
				updFeq = parseFloat( cmrAry[ 44 ]/1000 );
			}else if ( ordType == gSOType ){
				slip = parseFloat( cmrAry[ 37 ] );
				symTs = parseFloat( cmrAry[ 39 ] );
				updFeq = parseFloat( cmrAry[ 50 ]/1000 );
			}

		}else{
			return;
		}

		if ( ! validateParameter( "mCase" , mCase , mCase ) ||
			! validateParameter( "ecn" , ecn , ecn ) ||
			! validateParameter( "ordType" , ordType , ordType ) ||
			! validateParameter( "ordTIF" , ordTIF , ordTIF ) ||
			! validateParameter( "ordSide" , ordSide , ordSide ) ||
			! validateParameter( "sym" , sym , sym ) ||
			! validateParameter( "qty" , qty , qty ) ||
			! validateParameter( "ordMethod" , ordMethod , ordMethod ) ||
			! validateParameter( "sign" , sign , sign ) ||
			! validateParameter( "pips" , pips , pips ) ||
			! validateParameter( "updFeq" , updFeq , updFeq ) ||
			! validateParameter( "slip" , slip , slip ) ||
			! validateParameter( "symTs" , symTs , symTs ) ||
			! validateParameter( "updFeq" , updFeq , updFeq ) ){

			throw new Error( "Got NaN or undefined value when reading macro CMR" );
		}

		var rowID = "#macRow_" + numStr;
		var symTs = getSymItem( ecn , sym ).ts;
		var symLs = getSymItem( ecn , sym ).ls;
		var symSpread = getSymSpread( ecn , sym );

		if ( side == "BUY" ){
			var bidOrAsk = ".bidSec";
			var spread = symSpread[ 0 ];
		}else{
			var bidOrAsk = ".askSec";
			var spread = symSpread[ 1 ];
		}

		pips = Math.round( parseFloat( sign + pips ) / symTs );
		var sprdPips = pips - spread; // calulate actually pips
		var qty = Math.round( qty / symLs );

		if ( $( rowID ).length == 0 ){ // add a row if the row is not exist
			addRow( numStr );
			showSymSpread();
		}

		setSldr( ".ctrlPipsDiv" , sprdPips , pips ); // set slider
		setSldr( ".ctrlQtyDiv" , qty , qty );

		var oEcn = $( rowID ).find( ".ecnDiv" ).find( ".current" ).html();
		oEcn = oEcn.replace( / /g , "" ); // read current ecn

		if ( ecn != oEcn ){ // check whether ecn setting is changed or not
			$( rowID ).find( ".ecnSlt" ).val( ecn );
			$( rowID ).find( ".ecnSlt" ).trigger( "change" );
			$( rowID ).find( ".symSlt" ).empty();
			var symECN = eval( "gSymOpt_" + ecn );
			var symList = "";
			$.each( symECN , function ( value ) {
				symList += "<option value = " + value + "> "+ value + "</option>";
			} );
			$( rowID ).find( ".symSlt" ).append( symList ); // update symbol list after changing ecn setting
		}

		var oSym = $( rowID ).find( ".symDiv" ).find( ".current" ).html(); // read current symbol
		oSym = oSym.replace( / /g , "" );

		if ( sym != oSym ){ // check whether symbol setting is changed or not
			$( rowID ).find( ".symSlt" ).val( sym );
			$( rowID ).find( ".symSlt" ).trigger( "change" );
		}

		var oCase = parseInt( $( rowID ).find( bidOrAsk ).find( ".ifdSwChked" ).find( ".ifdSwIpt" ).val() ); // read macro if done action

		if ( mCase != oCase ){
			var ifdSwLbl = $( rowID ).find( bidOrAsk ).find( ".ifdSwDiv" ).find( ".ifdSwLbl" );
			var ifdSwBg = $( rowID ).find( bidOrAsk ).find( ".ifdSwBg" );
			$( ifdSwLbl ).removeClass( "ifdSwChked" );

			// if ( mCase == 1 ){ // Stop on Partial Fill
				// $( ifdSwLbl ).eq( 0 ).addClass( "ifdSwChked" );
				// $( ifdSwBg ).css( "top" , "-109px" );
			// }else if ( mCase == 2 ){ // stop on Complete Fill only
				// $( ifdSwLbl ).eq( 1 ).addClass( "ifdSwChked" );
				// $( ifdSwBg ).css( "top" , "-81px" );
			if ( mCase == 9 ){ // Non-Stop
				$( ifdSwLbl ).eq( 0 ).addClass( "ifdSwChked" );
				$( ifdSwBg ).css( "top" , "-122px" );
			}else if ( mCase == 10 ){ // Stop All ALGO
				$( ifdSwLbl ).eq( 1 ).addClass( "ifdSwChked" );
				$( ifdSwBg ).css( "top" , "-95px" );
			}else if ( mCase == 11 ){ // Stop All ALGO
				$( ifdSwLbl ).eq( 2 ).addClass( "ifdSwChked" );
				$( ifdSwBg ).css( "top" , "-68px" );
			}else if ( mCase == 12 ){ // Stop All ALGO
				$( ifdSwLbl ).eq( 3 ).addClass( "ifdSwChked" );
				$( ifdSwBg ).css( "top" , "-41px" );
			}
		}

		var oUpdFeq = parseFloat( $( rowID ).find( ".confRowUpdateFeqDiv" ).find( ".txtIpt" ).val() ); // read current macro update fequency

		if ( updFeq != oUpdFeq ){
			$( rowID ).find( ".confRowUpdateFeqDiv" ).find( ".txtIpt" ).val( updFeq ); // update update fequency if it is changed
		}

		var oRepOrdDly = parseFloat( $( rowID ).find( ".confRowReplaceDelayDiv" ).find( ".txtIpt" ).val() ); // read current replace order delay

		if ( repOrdDly != oRepOrdDly ){ // update replace order delay if it is changed
			if ( mCase == 2 || mCase == 9 ){
				$( rowID ).find( ".confRowReplaceDelayDiv" ).find( ".txtIpt" ).val( repOrdDly );
			}else if ( oRepOrdDly == 0 ){
				$( rowID ).find( ".confRowReplaceDelayDiv" ).find( ".txtIpt" ).val( repOrdDly );
			}
		}

		var ordMethodDiv = $( rowID ).find( ".confRowOrderMethodDiv" );
		var orderMethod = readParmAry( gConfPanelAry , "OrderMethod" ).parm;

		ordMethodAry = ordMethod.split( "_" );
		if ( ordMethodAry.length == 3 ){ // e.g. BID_SLOW_10 or ASK_MA_20
			var ordMethod = ordMethodAry[ 1 ]; // SLOW or MA
			var ordSpeed = ordMethodAry[ 2 ]; // e.g. 10 or 30 seconds

			$( ordMethodDiv ).find( ".txtIpt" ).val( ordSpeed );
			$( ordMethodDiv ).find( ".confSlet" ).val( ordMethod );
			$( ordMethodDiv ).find( ".confSlet" ).trigger( "change" ); // call trigger change to apply the select form change
		}else{ // e.g. BID or ASK
			$( ordMethodDiv ).find( ".txtIpt" ).val( orderMethod );
			$( ordMethodDiv ).find( ".confSlet" ).val( "Normal" );
			$( ordMethodDiv ).find( ".confSlet" ).trigger( "change" );
		}

		var confSlet = $( rowID ).find( ".confRowOrdTypeDiv" ).find( ".confSlet" ); // read order type ( LO or SO )
		$( confSlet ).val( ordType );
		$( confSlet ).trigger( "change" );

		$( rowID ).find( ".rowInfoID" ).text( rowID );

		function setSldr( ctrlClass , sldrVal , ctrlVal ){ // set slider
			$( rowID ).find( bidOrAsk ).find( ctrlClass ).find( ".ctrlVal" ).val( ctrlVal );
			if ( ctrlClass == ".ctrlPipsDiv" && sldrVal > 0 ){
				sldrVal = "+" + sldrVal;
			}
			var oSldrVal = Math.abs( $( rowID ).find( bidOrAsk ).find( ctrlClass ).find( ".sldrHd" ).text() );
			if ( sldrVal == oSldrVal ){
				return;
			}
			$( rowID ).find( bidOrAsk ).find( ctrlClass ).find( ".sldrHd" ).text( sldrVal );
			$( rowID ).find( bidOrAsk ).find( ctrlClass ).find( ".txtIpt" ).val( sldrVal );
		}

		///////////// temp setting ////////

		$( ordMethodDiv ).find( ".txtIpt" ).val( orderMethod );
		////////////////////////////////////
	}

	function uiClickDelRowBtn(){ // delete a row by user
		var runningMac = 0;
		var delRowBtn = this;

		$( delRowBtn ).closest( ".macRow" ).find( ".sideSec" ).each( function (){
			var swOnLbl = $( this ).find( ".macSw" ).find( ".swOnLbl" );
			if ( !$( swOnLbl ).hasClass( "swMacOn" ) ){
				return;
			}
			$( swOnLbl ).stop( true , true );
			$( swOnLbl ).addClass( "swOnNotice" , 300 );
			$( swOnLbl ).delay( 300 ).removeClass( "swOnNotice" , 300 );
			runningMac = 1;
			return false;
		} );

		if ( runningMac != 0 ){
			return
		}

		var rowID = $( delRowBtn ).closest( ".macRow" ).attr( "id" );
		var numStr = rowID.substr( rowID.length - 2 );
		var buyMacName = "algoMac_" + numStr + "_BUY";
		var buyCnlName = "algoCnl_" + numStr + "_BUY";
		var buyLpName = "algoLp_" + numStr + "_BUY";
		var buyPxName = "algoPx_" + numStr + "_BUY";

		var sellMacName = "algoMac_" + numStr + "_SELL";
		var sellCnlName = "algoCnl_" + numStr + "_SELL";
		var sellLpName = "algoLp_" + numStr + "_SELL";
		var sellPxName = "algoPx_" + numStr + "_SELL";

		getSeqNum( gByManual );

		deleteMacro( buyMacName ); // delete bid side macro
		deleteMacro( buyCnlName );
		deleteMacro( buyLpName );
		deleteMacro( buyPxName );
		deleteMacro( sellMacName ); // delete ask side macro
		deleteMacro( sellCnlName );
		deleteMacro( sellLpName );
		deleteMacro( sellPxName );

		removeOnMacroList( buyMacName ); // remove macro from on macro list
		removeOnMacroList( sellMacName );

		removeRowSeq( rowID ); // remove macro from macro sequence list
	}

	function deleteRow( cmrAry ){ // delete the row from UI
		// e.g. CMR 302 MACRO_DELETE algoMac_07_BUY
		var numStr = cmrAry[ 3 ].split( "_" )[ 1 ];
		var rowID = "#macRow_" + numStr;

		$( rowID ).fadeOut( 300 , function (){
			$( this ).remove();
		} );
	}

	function uiClickDeleteAllMacro(){ // delete all macro by user
		uiClickDeleteAllMode();
		startStopAll( gByManual );

		getSeqNum( gByManual );
		//var cmd = "CMD " + gInitUserID + " MACRO_DELETE_ALL" + " SEQ " + gSeqNum;
		var cmd = "CMD " + gInitUserID + " MACRO_DELETE_ALL";

		pushCMD( cmd );

		showLog( "uiClickDeleteAllMacro" , cmd );

		$( ".macRow" ).remove(); // delete all rows
		clearGlobalVal(); // clear all variables from browser

		setTimeout( function (){
			getMacro(); // get macro again to ensure everything are deleted
		} , 500 );
	}

	/////////////// panel timer function ////////////

	function onPanelTimer(){ // check open order , pnl , netPos and mode every second
		resetShortInterval( 1 );
		resetLongInterval( 1 );
		noTradeGuardian();
	}

	function shortPanelInterval(){
		lossFromTopGuardian();
		openOrderGuardian();
		netPosGuardian();
		maxLossGuardian();
		autoCheckChangeMode();
		saveMaxProfit();
		getDailyOrderCount();
	}

	function longPanelInterval(){
		checkIsLeader();
		//updateAlgoStatus();
	}

	function resetShortInterval( run ){
		clearInterval( gPanelShortInterval ); // stop panel timer

		var interval = 1000;
		var delay = Math.random() * 1000; // 0 - 1 second
		setTimeout( function (){
			clearInterval( gPanelShortInterval );
			gPanelShortInterval = false;

			if ( gPanelShortInterval ){
				return;
			}

			if ( run == 1 ){
				shortPanelInterval();
			}

			gPanelShortInterval = setInterval( shortPanelInterval , interval ); // 1000 = 1 seconds

		} , delay );
	}

	function resetLongInterval( run ){
		clearInterval( gPanelLongInterval );

		var interval = 60000;
		var delay = 3000 + Math.random() * 5000; // 3 - 8 second
		setTimeout( function (){
			clearInterval( gPanelLongInterval );
			gPanelLongInterval = false;

			if ( gPanelLongInterval ){
				return;
			}

			if ( run == 1 ){
				longPanelInterval();
			}

			gPanelLongInterval = setInterval( longPanelInterval , interval );
		} , delay );
	}

	/////////////// algo guardian function ////////////

	function triggerGuardian( type , reason ){
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){
			return;
		}

		var cfg = readParmAry( gConfGuardAry , type );

		var pauseTime = readParmAry( gConfGuardAry , "PauseTime" ).parm;
		var resumeVol = readParmAry( gConfGuardAry , "NmlSixtySecVol" ).parm;
		var algoOffFlat = readParmAry( gConfGuardAry , "AlgoOffFlat" ).on;
		var inFavorFlat = readParmAry( gConfGuardAry , "inFavorFlat" ).on;
		var flatQty = readParmAry( gConfGuardAry , "AutoFlatQty" ).parm;

		var doOff = guardianOff();
		var doPause = guardianPause();
		var doFlatAll = guardianFlatAll();
		var doFlat = guardianFlat();
		var doResume = guardianResume();
		var doNotice = guardianNotice();

		if ( doOff || doPause || doFlat || doFlatAll || doResume || doNotice ){
			editParmAry( gConfGuardAry , type , "triggered" , true );

			$.when( checkIsLeader() ).then( function (){
				if ( gIsLeader == 0 ){
					return;
				}
				sendMsg( gInitUserID , "MMNOTICE" , "GUARD_" + type , "" );

				if ( doNotice ){
					var msg = "Triggered " + cfg.title + " Guardian. " + reason;
					var toastrTopic = "AlgoGuardian:";

					if ( doOff || doFlat || doFlatAll ){
						var toastrType = "error";
						var soundType = "alert";
						msg = msg + gIconObj.doubleExcMark + gIconObj.doubleExcMark + gIconObj.doubleExcMark + gIconObj.doubleExcMark + gIconObj.doubleExcMark;
					}else{
						var toastrType = "warning";
						var soundType = "notice";
					}

					if ( type != "MaxFiveSecVol" ){
						sendTabMsg( msg );
						sendToastrMsg( toastrType , toastrTopic , msg );
						sendSoundMsg( soundType );
					}
				}

				// var isDealer = gDealerUser.indexOf( gInitUserID );
				// if ( isDealer >= 0 && cfg.gpAlert ){
					// sendAlertMsg( msg );
				// }

				if ( type == "LossFromTop" ){
					renewMaxProfit();
				}

				if ( doOff ){
					startStopAll( gByFunction );
				}

				if ( doPause ){
					clearTimeout( gPauseMarcoTimer );

					if ( cfg.autoResume ){
						var msg = "Resume Algo After " + pauseTime + " Second. ";
					}else if ( type == "MaxSixtySecVol" ){
						var msg = "Resume Algo When 60 Second Volume Drop to " + resumeVol + "Lots. "
					}else{
						var msg = "";
					}

					pauseAlgo( msg );
				}

				if ( doFlat || doFlatAll ){
					if ( doFlat ){
						var flatType = "flat";
					}else{
						var flatType = "flatAll";
						flatQty = 100;
					}
					clearTimeout( gFlatPosTimer );

					var delay = 0;
					var	paused = false;

					if ( !doOff && !doPause && algoOffFlat && type != "LossFromTop" && type != "MaxFiveSecVol" && type != "WideMASpread" ){ // LossFromTop will reset max profit
						editParmAry( gConfGuardAry , type , flatType , false );

						var parmName = "_guard_" + type + "_" + flatType;
						setMacro( parmName , 0 );
					}

					if ( !doOff && !doPause && gStatusObj.on ){
						pauseAlgo( "" );

						delay = 500;
						paused = true;

					}else{
						delay = 500;
					}

					gFlatPosTimer = setTimeout( function (){ // wait 0.9 second to make sure leader is also triggered this guardian
						squarePosition( flatQty , gDefAutoFlatSlip );

						if ( paused ){
							setTimeout( function (){
								resumeAlgo();
							} , delay );
						}
					} , delay );
				}
			} );
		}
		if ( doResume ){
			clearTimeout( gPauseMarcoTimer );

			if ( type == "NmlSixtySecVol" ){
				var resumeReason = "60 Second Volume Drop to " + resumeVol + ". ";
				var pauseDelay = 0;
			}else{
				var resumeReason = "Passed " + pauseTime + " Second. ";
				var pauseDelay = pauseTime * 1000;
			}

			if ( cfg.autoResume ){
				var resumeReason = "Passed " + pauseTime + " Second. ";
				var pauseDelay = pauseTime * 1000;
			}else if ( type == "NmlSixtySecVol" ){
				var resumeReason = "60 Second Volume Drop to " + resumeVol + ". ";
				var pauseDelay = 0;
			}else{
				var resumeReason = "";
				var pauseDelay = 0;
			}

			gPauseMarcoTimer = setTimeout( function (){
				if ( gStatusObj.on ){
					return;
				}

				if ( !gStatusObj.pause ){
					return;
				}

				$.when( checkIsLeader() ).then( function (){
					if ( gIsLeader == 0 ){
						return;
					}

					resumeAlgo();

					var msg = resumeReason + "Resume Algo by " + cfg.title + " Guardian. ";

					var toastrType = "success";
					var toastrTopic = "AlgoStatus:";
					var soundType = "notice";

					sendTabMsg( msg );
					sendToastrMsg( toastrType , toastrTopic , msg );
					sendSoundMsg( soundType );
				} );
			} , pauseDelay );
		}

		function guardianOff(){
			if ( !cfg.off ){
				return false;
			}

			if ( !gStatusObj.on ){
				return false;
			}

			return true;
		}

		function guardianPause(){
			if ( cfg.off ){
				return false;
			}

			if ( !cfg.pause ){
				return false;
			}

			if ( !gStatusObj.on ){ // // check the big on off switch is on or not again
				return false;
			}

			if ( gStatusObj.pause ){
				return false;
			}

			return true;
		}

		function guardianFlat(){
			if ( cfg.flatAll ){
				return false;
			}
			if ( !cfg.flat ){
				return false;
			}
			if ( flatQty == 0 ){
				return false;
			}

			var ecn = getEcnSym().ecn;
			var sym = getEcnSym().sym;
			var netPos = getSymNetPos( ecn , sym ).netPos;
			if ( netPos == 0 || ! $.isNumeric( netPos ) ){
				return false;
			}

			if ( type == "MaxFiveSecVol" && inFavorFlat ){
				var pxIcon = reason.split( " " )[ 47 ].slice( 0 , 1 );
				if ( pxIcon == gIconObj.up && netPos > 0 ){
					return;
				}
				if ( pxIcon == gIconObj.down && netPos < 0 ){
					return;
				}
			}

			if ( !algoOffFlat && !doOff && !doPause && !gStatusObj.on ){
				return false;
			}

			return true;
		}

		function guardianResume(){
			if ( doPause && cfg.autoResume ){
				return true;
			}else if ( cfg.on ){
				return true;
			}else{
				return false;
			}
		}

		function guardianNotice(){
			if ( !cfg.notice ){
				return false;
			}
			if ( !gStatusObj.on && !doPause && !doOff && !doFlat && !doFlatAll && !doResume ){
				return false;
			}
			return true;
		}

		function guardianFlatAll(){
			if ( !cfg.flatAll ){
				return false;
			}

			if ( flatQty == 0 ){
				return false;
			}

			var ecn = getEcnSym().ecn;
			var sym = getEcnSym().sym;
			var netPos = getSymNetPos( ecn , sym ).netPos;

			if ( netPos == 0 || ! $.isNumeric( netPos ) ){
				return false;
			}

			if ( !algoOffFlat && !doOff && !doPause && !gStatusObj.on ){
				return false;
			}

			return true;
		}
	}

	function openOrderGuardian(){ // check open order number
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){ // check if the panel is reloaded/disconnected or not
			return;
		}

		var type = "OpenOrder";
		var cfg = readParmAry( gConfGuardAry , type );

		if ( !cfg.pause && !cfg.off && !cfg.flat && !cfg.flatAll && !cfg.notice ){
			return;
		}

		var openOrder = getOpenOrderNum(); // get open order number

		if ( cfg.triggered ){
			if ( openOrder <= 2 ){
				editParmAry( gConfGuardAry , type , "triggered" , false );
			}
			return;
		}

		var algoNum = getRunningAlgoNum(); // get running algo number
		var limit = algoNum + gDefBufferOpenOrd; // default buffer = 3

		if ( openOrder <= limit ){ // check whether open order number is too much or not
			return;
		}

		var reason = "Exceed Open Order Limit. Number of Algo: " + algoNum + ". Number of Open Orders: " + openOrder + ". "; // send a stop all notice if it is a leader

		triggerGuardian( type , reason );
	}

	function maxLossGuardian(){ // guard maximum loss
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){ // check if the panel is reloaded/disconnected or not
			return;
		}

		var type = "MaxLoss";
		var cfg = readParmAry( gConfGuardAry , type );

		if ( !cfg.pause && !cfg.off && !cfg.flat && !cfg.flatAll && !cfg.notice ){
			return;
		}

		var pnl = getAccPnL(); // get current PnL

		if ( cfg.triggered ){
			if ( pnl > cfg.parm ){
				editParmAry( gConfGuardAry , type , "triggered" , false );
			}
			return;
		}

		if ( pnl >= cfg.parm ){
			return;
		}

		var reason = "Exceed Maximum Loss Limit. Current PnL: " + pnl + ". Maximum Loss Limit: " + cfg.parm + ". ";

		triggerGuardian( type , reason );
	}

	function lossFromTopGuardian(){ // check account daily loss from top. e.g. 1000 > 4000 > 2000 = loss from top 2000
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){ // check if the panel is reloaded/disconnected or not
			return;
		}

		var type = "LossFromTop";
		var cfg = readParmAry( gConfGuardAry , type );

		if ( !cfg.pause && !cfg.off && !cfg.flat && !cfg.flatAll && !cfg.notice ){
			return;
		}

		var pnl = getAccPnL(); // get current PnL
		var lossFromTop = ( pnl - gMaxProfit ).toFixed();

		if ( cfg.triggered ){
			if ( lossFromTop >= cfg.parm ){
				editParmAry( gConfGuardAry , type , "triggered" , false );
			}
			return;
		}

		if ( lossFromTop >= cfg.parm ){
			return;
		}

		var reason = "Exceed Maximum Loss From Top Limit. Loss From Top: " + lossFromTop + ". Maximum Loss From Top Limit: " + cfg.parm + ". ";

		triggerGuardian( type , reason );
	}

	function filledQtyGuardian( cmrAry ){ // guard filled quantity
		// e.g. CMR 300 TOTAL_FILL MREX CME/CMX_GLD/AUG17 15 46 SELL 1 1246.7 OPX 1246.4
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){ // check if the panel is reloaded/disconnected or not
			return;
		}

		var type = "FilledQty";
		var cfg = readParmAry( gConfGuardAry , type );

		if ( !cfg.pause && !cfg.off && !cfg.flat && !cfg.flatAll && !cfg.notice ){
			return;
		}

		var rEcn = cmrAry[ 3 ];
		var rSym = cmrAry[ 4 ];

		var maxQty = 0;

		$( ".infQtyDiv" ).each( function (){
			var infQtyDiv = $( this );

			var ecn = $( infQtyDiv ).closest( ".rowSec" ).find( ".rowInfSec" ).find( ".ecnDiv " ).find( ".current" ).html();
			ecn = ecn.replace( / /g , "" );

			var sym = $( infQtyDiv ).closest( ".rowSec" ).find( ".rowInfSec" ).find( ".symDiv " ).find( ".current" ).html();
			sym = sym.replace( / /g , "" ); // get row symbol

			if ( rEcn != ecn ){ // check the symbol is the same or not
				return;
			}

			if ( rSym != sym ){ // check the symbol is the same or not
				return;
			}

			var qty = Math.abs( $( infQtyDiv ).find( ".txtIpt" ).val() ); // read number of filled quantity for this macro

			if ( qty > maxQty ){
				maxQty = qty;
			}
		} );

		if ( cfg.triggered ){
			if ( maxQty <= cfg.parm ){
				editParmAry( gConfGuardAry , type , "triggered" , false );
			}
			return;
		}

		if ( maxQty <= cfg.parm ){
			return;
		}

		if ( $( infQtyDiv ).closest( ".sideSec" ).hasClass( "bidSec" ) ){
			var side = "BUY";
		}else{
			var side = "SELL";
		}

		var reason = "Exceed Fill Quantity Limit On " + ecn + " " + sym + ". Filled " + side + " Quantity: " + maxQty + " . Fill Quantity Limit: " + cfg.parm + ". ";

		triggerGuardian( type , reason );
	}

	function netPosGuardian(){ // guard net position
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){ // check if the panel is reloaded/disconnected or not
			return;
		}

		var type = "NetPos";
		var cfg = readParmAry( gConfGuardAry , type );

		if ( !cfg.pause && !cfg.off && !cfg.flat && !cfg.flatAll && !cfg.notice ){
			return;
		}

		var ecn = getEcnSym().ecn;
		var sym = getEcnSym().sym;
		var netPos = getSymNetPos( ecn , sym ).netPos;

		if ( netPos == 0 || ! $.isNumeric( netPos ) ){
			return;
		}

		if ( cfg.triggered ){
			if ( Math.abs( netPos ) <= cfg.parm ){
				editParmAry( gConfGuardAry , type , "triggered" , false );
			}
			return;
		}

		if ( Math.abs( netPos ) <= cfg.parm ){
			return;
		}

		var reason = "Exceed Net Position Limit On " + ecn + " " + sym + ". Net Position: " + netPos + " . Net Position Limit: " + cfg.parm + ". ";

		triggerGuardian( type , reason );
	}

	function slowFedGuardian( cmrAry ){ // guard slow feed
		// e.g. CMR 300 TOTAL_FILL MREX CME/CMX_GLD/AUG17 15 46 SELL 1 1246.7 OPX 1246.4
		// e.g. CMR 302 TOTAL_FILL MREX CME/CMX_GLD/DEC17 13 9 BUY 1 1268.5 OPX UNKNOWN
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){ // check if the panel is reloaded/disconnected or not
			return;
		}

		var type = "SlowFeed";
		var cfg = readParmAry( gConfGuardAry , type );

		if ( !cfg.pause && !cfg.off && !cfg.flat && !cfg.flatAll && !cfg.notice ){
			return;
		}

		if ( cfg.triggered ){
			return;
		}

		if ( cmrAry.length < 11 ){
			return;
		}
		if ( cmrAry[ 11 ] == "UNKNOWN" || cmrAry[ 11 ] == "NIL" ){
			return; // manual hit
		}

		if ( cmrAry[ 13 ] == "algoFlat" ){
			return; // cut loss order
		}

		var ecn = cmrAry[ 3 ];
		var sym = cmrAry[ 4 ];
		var side = cmrAry[ 7 ];
		var fillPx = parseFloat( cmrAry[ 9 ] );
		var ordPx = parseFloat( cmrAry[ 11 ] );
		var dip = parseFloat( getSymItem( ecn , sym ).dip );

		var ts = parseFloat( getSymItem( ecn , sym ).ts ); // get tick size

		if ( isNaN( fillPx ) || isNaN( ordPx ) || isNaN( ts ) || isNaN( dip ) ){
			var msg = "Error On Reading Filled Price Msg. filled px : " + fillPx + " . order px : " + ordPx + " . tick size : " + ts + " . Decimal Places : " + dip;
			sendLogMsg( msg );
			return;
		}

		fillPx = fillPx.toFixed( dip );
		ordPx = ordPx.toFixed( dip );

		if ( side == "BUY" ){
			if ( fillPx >= ordPx ){
				return;
			}
		}else{
			if ( fillPx <= ordPx ){
				return;
			}
		}

		if ( gGuaSlowFedCount == 0 ) {
			gGuaSlowFedCount += 1;

			clearTimeout( gGuaSlowFedTimer );
			gGuaSlowFedTimer = setTimeout( function (){
				gGuaSlowFedCount = 0;
				editParmAry( gConfGuardAry , type , "triggered" , false );
			} , 60000 );

			return;
		}

		var reason = "Slow Price Feed. Better Filled " + side + " " + ecn + " " + sym + " Order Twice in One Minute. Last Order Price: " + ordPx + ". Last Filled Price: " + fillPx + ". ";

		triggerGuardian( type , reason );
	}

	function twiceFillGuardian( msg ){
		// e.g. BUY Twice in 1 minute , Same Algo FullyFilled > 1
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){ // check if the panel is reloaded/disconnected or not
			return;
		}

		var type = "FilledTwice";
		var cfg = readParmAry( gConfGuardAry , type );

		if ( !cfg.pause && !cfg.off && !cfg.flat && !cfg.flatAll && !cfg.notice ){
			return;
		}

		var side = msg.split( " " )[ 0 ];
		if ( side != "BUY" && side != "SELL" ){
			return;
		}

		var reason = "Filled " + side + " Order Twice Within One Minute. ";

		triggerGuardian( type , reason );
	}

	function sixtySecVolGuardian( msg ){
		//e.g. Volume Update: CME/CMX_GLD/DEC17 2003
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){ // check if the panel is reloaded/disconnected or not
			return;
		}

		var type = "MaxSixtySecVol";
		var cfg = readParmAry( gConfGuardAry , type );

		if ( !cfg.pause && !cfg.off && !cfg.flat && !cfg.flatAll && !cfg.notice ){
			return;
		}

		var msgAry = msg.split( " " );

		if ( msgAry.length < 4 ){
			return;
		}

		var sym = msgAry[ 2 ];
		var vol = msgAry[ 3 ];

		var rEcn = getEcnSym().ecn;
		var rSym = getEcnSym().sym;

		if ( sym != rSym ){
			return;
		}

		if ( cfg.triggered ){
			return;
		}

		if ( vol < cfg.parm ){
			return
		}

		var reason = "Exceed Sixty Second Volume Limit On " + sym + ". Sixty Second Volume: " + vol + "Lots. Volume Limit: " + cfg.parm + "Lots. ";

		triggerGuardian( type , reason );
	}

	function resumeVolumeGuardian( msg ){
		if ( gStatusObj.on || !gStatusObj.pause ) { // check the big on off switch is on or not
			return;
		}

		if ( gStatusObj.reloaded || gStatusObj.disconnected ){ // check if the panel is reloaded/disconnected or not
			return;
		}
		var type = "NmlSixtySecVol";
		var cfg = readParmAry( gConfGuardAry , type );

		var triggerType = "MaxSixtySecVol";
		var triggered = readParmAry( gConfGuardAry , triggerType ).triggered;

		if ( !cfg.on ){
			return;
		}

		if ( !triggered ){
			return;
		}

		var msgAry = msg.split( " " );

		if ( msgAry.length < 4 ){
			return;
		}

		var sym = msgAry[ 2 ];
		var vol = msgAry[ 3 ];

		var rEcn = getEcnSym().ecn;
		var rSym = getEcnSym().sym;

		if ( sym != rSym ){
			return;
		}

		if ( vol > cfg.parm ){
			return;
		}

		editParmAry( gConfGuardAry , type , triggerType , false );

		var reason = "Sixty Second Volume Dropped to " + vol + "Lots On " + sym + ". ";

		triggerGuardian( type , reason );
	}

	function fiveSecVolGuardian(){
		var type = "MaxFiveSecVol";
		var cfg = readParmAry( gConfGuardAry , type );
		var obj = gMAQuoteAry[ 0 ];
		var preObj = gMAQuoteAry[ gFiveSecondTick - 1 ];

		if ( typeof obj == "undefined" ) {
			return;
		}
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){ // check if the panel is reloaded/disconnected or not
			return;
		}
		if ( !cfg.pause && !cfg.off && !cfg.flat && !cfg.flatAll && !cfg.notice ){
			return;
		}
		if ( gMAQuoteAry.length < gFiveSecondTick ){
			return;
		}

		if ( gMAQuoteAry[ 0 ].totalVol == 0 || gMAQuoteAry[ gFiveSecondTick - 1 ].totalVol == 0 ){
			return;
		}

		var sec5Vol = gMAQuoteAry[ 0 ].totalVol - gMAQuoteAry[ gFiveSecondTick - 1 ].totalVol;

		if ( cfg.triggered ){
			if ( sec5Vol < ( cfg.parm / 2 ) ){
				editParmAry( gConfGuardAry , type , "triggered" , false );
			}
			return;
		}

		if ( sec5Vol < cfg.parm ){
			return
		}

		var pxIcon = gIconObj.minus;
		var tradeIcon = gIconObj.minus;
		var warnningIcon = "";

		var mid = parseFloat( ( ( obj.bid + obj.ask ) / 2 ).toFixed( obj.dip + 1 ) );
		var preMid = parseFloat( ( ( preObj.bid + preObj.ask ) / 2 ).toFixed( preObj.dip + 1 ) );

		if ( mid > preMid ){
			pxIcon = gIconObj.up;
		}else if ( mid < preMid ){
			pxIcon = gIconObj.down;
		}

		var sec5BidVol = 0;
		var sec5AskVol = 0;

		for ( var i = 0 ; i < ( gFiveSecondTick - 1 ) ; i++ ){
			sec5BidVol += gMAQuoteAry[ i ].rlBidVol;
			sec5AskVol += gMAQuoteAry[ i ].rlAskVol;
		}

		if ( sec5BidVol < sec5AskVol ){
			tradeIcon = gIconObj.up;
		}else if ( sec5BidVol > sec5AskVol ){
			tradeIcon = gIconObj.down;
		}

		if ( ( pxIcon == gIconObj.up && tradeIcon == gIconObj.down ) || ( pxIcon == gIconObj.down && tradeIcon == gIconObj.up ) ){
			warnningIcon = gIconObj.warnning;
		}

		var reason = "Exceed Five Second Volume Limit On " + obj.ecn + " " + obj.sym + ". Five Second Volume: " + sec5Vol + "Lots. Volume Limit: " + cfg.parm + "Lots. Bid Px: " + obj.bid + ". Ask Px: " + obj.ask + ". Previous Bid: " + preObj.bid + ". Previous Ask: " + preObj.ask + ". Filled on Bid: " + sec5BidVol + ". Filled on Ask: " + sec5AskVol + ". Px Direction: " + pxIcon + pxIcon + ". Trade Direction: " + tradeIcon + tradeIcon + ". " + warnningIcon + warnningIcon;

		triggerGuardian( type , reason );

		sendTableMsg( "sec5Vol" , mid , preMid , sec5Vol , sec5BidVol , sec5AskVol );

		checkTickVolume( cfg.parm );
	}

	function wideMASpreadGuardian(){
		var type = "WideMASpread";
		var cfg = readParmAry( gConfGuardAry , type );
		var obj = gMAQuoteAry[ 0 ];
		var preObj = gMAQuoteAry[ gFiveSecondTick - 1 ];

		if ( typeof obj == "undefined" ) {
			return;
		}
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){ // check if the panel is reloaded/disconnected or not
			return;
		}
		if ( !cfg.pause && !cfg.off && !cfg.flat && !cfg.flatAll && !cfg.notice ){
			return;
		}
		if ( gMAQuoteAry.length < gFiveSecondTick ){
			return;
		}

		if ( cfg.triggered ){
			if ( obj.maSpreadPip < ( cfg.parm / 2 ) ){
				editParmAry( gConfGuardAry , type , "triggered" , false );
			}
			return;
		}

		if ( obj.maSpreadPip < cfg.parm ){
			return;
		}

		if ( gGotWrongQuote ){
			return;
		}

		var sec5Vol = gMAQuoteAry[ 0 ].totalVol - gMAQuoteAry[ gFiveSecondTick - 1 ].totalVol;
		var sec5BidVol = 0;
		var sec5AskVol = 0;
		var sec5RtsBidVol = 0;
		var sec5RtsAskVol = 0;

		for ( var i = 0 ; i < ( gFiveSecondTick - 1 ) ; i++ ){
			sec5BidVol += gMAQuoteAry[ i ].bidVol;
			sec5AskVol += gMAQuoteAry[ i ].askVol;
			sec5RtsBidVol += gMAQuoteAry[ i ].rtsBidVol;
			sec5RtsAskVol += gMAQuoteAry[ i ].rtsAskVol;
		}

		var mid = ( ( obj.bid + obj.ask ) / 2 ).toFixed( obj.dip + 1 );
		var preMid = ( ( preObj.bid + preObj.ask ) / 2 ).toFixed( preObj.dip + 1 );

		var pxIcon = gIconObj.minus;
		var tradeIcon = gIconObj.minus;
		var warnningIcon = "";

		if ( mid > preMid ){
			pxIcon = gIconObj.up;
		}else if ( mid < preMid ){
			pxIcon = gIconObj.down;
		}

		if ( sec5BidVol < sec5AskVol ){
			tradeIcon = gIconObj.up;
		}else if ( sec5BidVol > sec5AskVol ){
			tradeIcon = gIconObj.down;
		}

		if ( ( pxIcon == gIconObj.up && tradeIcon == gIconObj.down ) || ( pxIcon == gIconObj.down && tradeIcon == gIconObj.up ) ){
			warnningIcon = gIconObj.warnning;
		}

		var reason = "Exceed MA Spread Limit on " + obj.ecn + " " + obj.sym + ". MA Spread: " + obj.maSpreadPip + ". MA Spread Limit: " + cfg.parm + ". MA Bid Px: " + obj.maBid + ". MA Ask Px: " + obj.maAsk + ". Bid Px: " + obj.bid + ". Ask Px: " + obj.ask + ". Previous Bid: " + preObj.bid + ". Previous Ask: " + preObj.ask + ". Filled on Bid: " + sec5BidVol + ". Filled on Ask: " + sec5AskVol + ". Filled on Reuters Bid: " + sec5RtsBidVol + ". Filled on Reuters Ask: " + sec5RtsAskVol + ". Px Direction: " + pxIcon + pxIcon + ". Trade Direction: " + tradeIcon + tradeIcon + ". " + warnningIcon + warnningIcon;

		triggerGuardian( type , reason );
	}

	function noTradeGuardian(){
		clearTimeout( gNoTradeTimer );

		if ( !gStatusObj.on ) { // check the big on off switch is on or not
			return;
		}
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){ // check if the panel is reloaded/disconnected or not
			return;
		}

		var type = "NoTrade";
		var cfg = readParmAry( gConfGuardAry , type );

		if ( !cfg.on ){ // check the guard maximum loss from top function is turnned on or not
			return;
		}

		var noTradeTimer = cfg.parm * 1000;

		gNoTradeTimer = setTimeout( function (){
			if ( !gStatusObj.on ) { // check the big on off switch is on or not
				return;
			}

			var ecn = getEcnSym().ecn;
			var sym = getEcnSym().sym;
			var netPos = getSymNetPos( ecn , sym ).netPos;

			if ( netPos == 0 || ! $.isNumeric( netPos ) ){
				return;
			}

			if ( netPos > 0 ){
				var side = "SELL";
			}else{
				var side = "BUY";
			}

			triggerOnRowGuardian( ecn , sym , side );

		} , noTradeTimer );
	}

	function onAlgoGuardian(){ // check if there is any open order or running algo before trunning on panel
		if ( gStatusObj.on ) { // check the big on off switch is on or not
			return true;
		}

		if ( gStatusObj.reloaded || gStatusObj.disconnected ){ // check if the panel is reloaded/disconnected or not
			return true;
		}

		var type = "OpenOrder";
		var cfg = readParmAry( gConfGuardAry , type );

		if ( !cfg.off ){
			return true;
		}

		var orderNum = getOpenOrderNum(); // get open order number

		if ( orderNum == 0 ){
			return true;
		}

		$( ".cfmCvr" ).show();
		$( ".cfmOnAlgoDiv" ).show();
		return false;
	}

	function hideOnAlgoGuardian(){
		$( ".cfmCvr" ).hide();
		$( ".cfmOnAlgoDiv" ).hide();
	}

	function showGuardianNotice( type ){ // show a notice if guardian is triggered.
		var cfg = readParmAry( gConfGuardAry , type );

		if ( !cfg.showWarn ){
			return;
		}

		$( ".panlGuarDiv" ).addClass( "fontColorNotice" );
		$( "#jpanel_msg" ).find( ".badgeSp" ).addClass( "bgColorNotice" );

		var div = $( ".conf" + type + "Div" );
		$( div ).find( ".txtIpt" ).addClass( "confWarnIpt" , 300 );
		$( div ).addClass( "confGuardWarn" , 300 );

		if ( $( ".confGuarSec" ).is( ":visible" ) ){
			return;
		}

		showGuardianPanel();
	}

	function autoFlatPosition( type ){ // auto square position
		var type = "AutoFlatQty";
		var cfg = readParmAry( gConfGuardAry , type );

		if ( cfg.parm == 0 ){ // do nothing if auto cut 0 percent
			return;
		}

		squarePosition( cfg.parm , gDefAutoFlatSlip );
	}

	function squarePosition( pct , slip ){
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){ // check if the panel is reloaded/disconnected or not
			return;
		}

		clearInterval( gFlatPosInterval ); // clear previous timer
		gFlatPosInterval = false;

		var type = "MaxAutoFlatQty";
		var cfg = readParmAry( gConfGuardAry , type );

		var interval = 200;
		var wait = 2000;
		var orderNum = 0;

		var done = false;

		flatPos();
		gFlatPosInterval = setInterval( flatPos , interval );

		function flatPos(){
			if ( gStatusObj.on ){
				orderNum = getOpenOrderNum();
			}

			if ( wait <= 0 ){
				clearInterval( gFlatPosInterval );
				gFlatPosInterval = false;

				var msg = "Exist Outstanding Open Order , Open Order No. : " + orderNum + ". Auto Flat is Aborted.";
				sendTabMsg( msg );

				return;
			}

			if ( orderNum > 1 ){
				wait -= interval;
				return;
			}

			if ( done ){
				return;
			}

			done = true;

			clearInterval( gFlatPosInterval );
			gFlatPosInterval = false;

			var ecn = getEcnSym().ecn;
			var sym = getEcnSym().sym;

			var netPos = getSymNetPos( ecn , sym ).netPos;
			var dip = parseFloat( getSymItem( ecn , sym ).dip ); // get symbol decimal places
			var ts = parseFloat( getSymItem( ecn , sym ).ts );

			if ( netPos == 0 || ! $.isNumeric( netPos ) ){
				return;
			}

			var qty = Math.round( netPos * ( pct / 100 ) ) * -1; // square qty
			if ( qty == 0 ){
				return;
			}

			var bidPx = getSymBidAskPx( ecn , sym )[ 0 ];
			var askPx = getSymBidAskPx( ecn , sym )[ 1 ];

			if ( bidPx == 0 || askPx == 0 ){
				var msg = "Error on getting " + sym + " Bid Ask Price. Fail to send cut loss order.";
				sendLogMsg( msg );

				return;
			}

			if ( isNaN( dip ) ){
				var msg = "Error On Getting Decimal Places : " + dip + ". Fail to send cut loss order.";
				sendLogMsg( msg );

				return;
			}

			var msg = "Squaring Position. ecn : " + ecn + " .sym : " + sym + " .netPos : " + netPos + ". cut qty : " + qty;
			sendLogMsg( msg );

			if ( pct > 0 ){
				var px = getCutLossPx( ecn , sym , netPos , qty , bidPx , askPx );

			}else{
				if ( qty > 0 ){
					var px = askPx + ( slip * ts );
				}else{
					var px = bidPx - ( slip * ts );
				}
			}

			px = px.toFixed( dip );

			if ( qty > 0 ){
				var side = "Buy";
			}else{
				var side = "Sell";
			}

			if ( pct > 0 ){
				var type = "Flat";
			}else{
				var type = "Overweight";
			}

			if ( Math.abs( qty ) > cfg.parm ){
				if ( qty > 0 ){
					qty = cfg.parm;
				}else{
					qty = cfg.parm * -1;
				}

				var cutMsg = ". ( Maximum Auto " + type + " " + cfg.parm + " Lots. )";

			}else{
				var cutMsg = ". ( " + type + " " + Math.abs( pct ) + "% of Current Net Position. )";
			}

			sendSquarePositionOrder( ecn , sym , qty , px );

			var msg = "Auto " + type + " Position. Placed " + side + " " + Math.abs( qty ) + " Lots " + ecn + " " + sym + " Order at " + px + cutMsg;
			sendTabMsg( msg );
		}
	}

	function sendSquarePositionOrder( ecn , sym , qty , px ){
		var poi = gInitUserID.toString() + "999"; // e.g. 301999
		var type = "LO2";
		var tif = "DAY";

		if ( qty < 0 ){
			var side = "SELL";
		}else if ( qty > 0 ){
			var side = "BUY";
		}else{
			return;
		}

		var cmd = "CMD " + gInitUserID + " O " + ecn + " " + type + " " + tif + " " + side + " " + sym + " QTY " + Math.abs( qty ) + " AT " + px + " POI " + poi + " NAME algoFlat";

		pushCMD( cmd );
		showLog( "sendSquarePositionOrder" , cmd );
	}


	function triggerOnRowGuardian( ecn , sym , side ){
		if ( !gStatusObj.on ){ // // check the big on off switch is on or not again
			return;
		}

		var type = "NoTrade";
		var cfg = readParmAry( gConfGuardAry , type );

		$.when( checkIsLeader() ).then( function (){ // check itself is leader
			if ( gIsLeader == 0 ){
				return;
			}

			onMacro();
		} );

		function onMacro(){
			$( ".macRow" ).each( function (){
				var row = $( this );
				var id = $( this ).attr( "id" );
				var numStr = id.split( "_" )[ 1 ].toString();

				var rEcn = $( row ).find( ".ecnDiv" ).find( ".current" ).html();
				rEcn = rEcn.replace( / /g , "" );
				var rSym = $( row ).find( ".symDiv" ).find( ".current" ).html();
				rSym = rSym.replace( / /g , "" );

				if ( rEcn != ecn ){
					return;
				}
				if ( rSym != sym ){
					return;
				}

				$( row ).find( ".sideSec" ).each( function (){
					var ifdSwVal = $( this ).find( ".ifdSwChked" ).find( ".ifdSwIpt" ).attr( "value" );
					var pips = Math.abs( $( this ).find( ".ctrlPipsDiv" ).find( ".txtIpt" ).val() );

					if ( ifdSwVal != 12 ){
						return;
					}

					if ( $( this ).find( ".swOnLbl" ).hasClass( "swChked" ) ){
						return;
					}

					if ( $( this ).hasClass( "bidSec" ) ){
						var rSide = "BUY";
						var order = "Bid -" + pips + " Pips";
					}else{
						var rSide = "SELL";
						var order = "Ask +" + pips + " Pips";
					}

					if ( rSide != side ){
						return;
					}

					var macName = "algoMac_" + numStr + "_" + side;
					addOnMacroList( macName );
					startMacro( macName );

					var msg = "No Trade in " + cfg.parm + " Second. Turned On " + order + " " + ecn + " " + sym + " Order.";
					sendTabMsg( msg );
				} );
			} );
		}
	}

	function triggerOffRowGuardian(){
		$( ".macRow" ).each( function (){
			var row = $( this );
			var id = $( this ).attr( "id" );
			var numStr = id.split( "_" )[ 1 ].toString();
			var rEcn = $( row ).find( ".ecnDiv" ).find( ".current" ).html();
			rEcn = rEcn.replace( / /g , "" );
			var rSym = $( row ).find( ".symDiv" ).find( ".current" ).html();
			rSym = rSym.replace( / /g , "" );

			$( row ).find( ".sideSec" ).each( function (){
				var ifdSwVal = $( this ).find( ".ifdSwChked" ).find( ".ifdSwIpt" ).attr( "value" );
				var pips = Math.abs( $( this ).find( ".ctrlPipsDiv" ).find( ".txtIpt" ).val() );

				if ( ifdSwVal != 12 ){
					return;
				}

				if ( !$( this ).find( ".swOnLbl" ).hasClass( "swChked" ) ){
					return;
				}

				if ( $( this ).hasClass( "bidSec" ) ){
					var rSide = "BUY";
					var order = "Bid -" + pips + " Pips";
				}else{
					var rSide = "SELL";
					var order = "Ask +" + pips + " Pips";
				}

				var macName = "algoMac_" + numStr + "_" + rSide;
				var cnlName = "algoCnl_" + numStr + "_" + rSide;

				removeOnMacroList( macName );
				stopMacro( cnlName );

				if ( gStatusObj.on ){
					var msg = "Order is Filled. Turned Off " + order + " " + rEcn + " " + rSym + " Order.";
					sendTabMsg( msg );
				}
			} );
		} );
	}

	///////////////// config panel function //////////////

	function uiClickCfgPanelBtn(){ // config panel setting
		if ( $( ".confPanlSec" ).is( ":visible" ) ){
			return;
		}

		for ( i = 0 ; i < gConfPanelAry.length ; i++ ){
			var obj = gConfPanelAry[ i ];
			var div = ".conf" + obj.name + "Div";

			if ( !obj.showCfg ){
				continue;
			}

			if ( obj.showSelect ){
				$( div ).find( ".confSlet" ).val( obj.select );
				$( div ).find( ".confSlet" ).trigger( "change" );
				$( div ).find( ".confSlet" ).foundationCustomForms();
			}

			if ( obj.showParm ){
				$( div ).find( ".txtIpt" ).attr( "value" , obj.parm );
			}

			if ( obj.showOn ){
				if ( obj.local ){
					var on = readParmAry( gLocalCfgAry , obj.name ).on;
				}else{
					var on = obj.on;
				}
				$( div ).find( ".confChk" ).attr( "checked" , on );
			}

			$( div ).find( "*" ).prop( "disabled" , obj.disable );
		}

		$( ".confPanlSec" ).fadeIn();
	}

	function uiConfimPanlCfg(){ // confirm panel config
		var updated = false;
		var msg = "";

		for ( j = 0 ; j < gConfPanelAry.length ; j++ ){
			var obj = gConfPanelAry[ j ];
			var div = ".conf" + obj.name + "Div";
			var rowDiv = ".confRow" + obj.name + "Div";

			if ( !obj.showCfg ){
				continue;
			}

			if ( obj.showParm ){
				var chkParm = checkIptVal( div , obj.parmMax , obj.parmMin ); // input number validation

				if ( !chkParm ){
					return;
				}

				if ( obj.parm != chkParm ){ // check need to update the macro or not
					if ( obj.name == "UpdateFeq" || obj.name == "ReplaceDelay" || obj.name == "OrderMethod" ){
						updated = true;

						$( ".confRow" + obj.name + "Div" ).each( function (){
							$( this ).find( ".txtIpt" ).val( chkParm );
						} );
					}

					var parmName = "_conf_" + obj.name + "_parm";

					setMacro( parmName , chkParm );

					msg += obj.title + " Parameter Changed to " + chkParm + obj.unit + ". ";
				}
			}

			if ( obj.showOn ){
				var chk = $( div ).find( ".confChk" ).is( ":checked" );

				var chkNum = chk ? 1 : 0;
				var chkMsg = chk ? "Enabled" : "Disabled";

				if ( obj.local ){
					editParmAry( gLocalCfgAry , obj.name , "on" , chk );
					store.set( "j" + obj.name , chk );

				}else{
					if ( obj.on != chk ){
						var parmName = "_conf_" + obj.name + "_on";

						setMacro( parmName , chkNum );

						msg += chkMsg + " '" + obj.title + "' Function. ";
					}
				}
			}

			if ( obj.showSelect ){
				var select = $( div ).find( ".current" ).html();
				select = select.replace( / /g , "" );

				if ( obj.select != select ){
					$( rowDiv ).each( function (){
						$( this ).find( ".confSlet" ).val( select );
						$( this ).find( ".confSlet" ).trigger( "change" );
					} );

					var parmName = "_conf_" + obj.name + "_select";

					setMacro( parmName , select );

					msg += obj.title + " Changed to '" + select + "'. ";
				}
			}
		}

		if ( updated ){ // update and restart macro to apply the change
			updateAllMacro();
			restartAllMacro();
		}

		if ( msg != "" ){ // tell every one panel conf is changed.
			getSeqNum( gByFunction );
			msg += "Seq No.: " + gSeqNum + ".";

			var tabMsg = "Updated Algo Parameter : " + msg;
			sendTabMsg( tabMsg );
		}

		hideCfgSec();
	}

	function uiClickCfgGuarBtn(){ // config algo guardian setting
		if ( $( ".panlGuarDiv" ).hasClass( "fontColorNotice" ) ){
			$( ".panlGuarDiv" ).removeClass( "fontColorNotice" );
		}

		if ( $( ".confGuarSec" ).is( ":visible" ) ){
			return;
		}

		showGuardianPanel();
	}

	function showGuardianPanel(){
		for ( i = 0 ; i < gConfGuardAry.length ; i++ ){
			var obj = gConfGuardAry[ i ];
			var div = ".conf" + obj.name + "Div";

			if ( !obj.showCfg ){
				continue;
			}

			if ( obj.showParm ){
				$( div ).find( ".txtIpt" ).attr( "value" , obj.parm );
			}

			if ( obj.showOn ){
				$( div ).find( ".confOnChk" ).attr( "checked" , obj.on );
			}

			if ( obj.showOff ){
				$( div ).find( ".confOffChk" ).attr( "checked" , obj.off );

				if ( obj.off ){
					$( div ).find( ".confPauseChk" ).addClass( "disableCtrl" , 200 );
					$( div ).find( ".confPauseChk" ).prop( "disabled" , true );

					if ( obj.name == "MaxSixtySecVol" ) {
						$( ".confNmlSixtySecVolDiv" ).find( "*" ).addClass( "disableCtrl" , 200 );
					}

				}else{
					$( div ).find( ".confPauseChk" ).removeClass( "disableCtrl" , 200 );
					$( div ).find( ".confPauseChk" ).prop( "disabled" , false );

					if ( obj.name == "MaxSixtySecVol" ) {
						$( ".confNmlSixtySecVolDiv" ).find( "*" ).removeClass( "disableCtrl" , 200 );
					}
				}
			}

			if ( obj.showPause ){
				$( div ).find( ".confPauseChk" ).attr( "checked" , obj.pause );
			}

			if ( obj.showFlat ){
				$( div ).find( ".confFlatChk" ).attr( "checked" , obj.flat );
			}

			if ( obj.showFlatAll ){
				$( div ).find( ".confFlatAllChk" ).attr( "checked" , obj.flatAll );

				if ( obj.flatAll ){
					$( div ).find( ".confFlatChk" ).addClass( "disableCtrl" , 200 );
					$( div ).find( ".confFlatChk" ).prop( "disabled" , true );
				}else{
					$( div ).find( ".confFlatChk" ).removeClass( "disableCtrl" , 200 );
					$( div ).find( ".confFlatChk" ).prop( "disabled" , false );
				}
			}

			if ( obj.showNotice ){
				$( div ).find( ".confNoticeChk" ).attr( "checked" , obj.notice );
			}
		}

		$( ".confGuarSec" ).fadeIn();
	}

	function uiConfimGuarCfg(){ // confirm guardian setting by user
		var msg = "";

		for ( j = 0 ; j < gConfGuardAry.length ; j++ ){
			var obj = gConfGuardAry[ j ];
			var div = ".conf" + obj.name + "Div";
			var chkChg = false;
			var parmChg = false;

			if ( !obj.showCfg ){
				continue;
			}

			if ( obj.showParm ){
				var chkParm = checkIptVal( div , obj.parmMax , obj.parmMin ); // input number validation
				if ( chkParm === false ){
					return;
				}

				if ( obj.parm != chkParm ){ // check need to update the macro or not
					parmChg = true;
					obj.parm = chkParm;

					if ( obj.name == "MaxSixtySecVol" || obj.name == "NmlSixtySecVol" ){
						chkParm = Math.round( chkParm / 500 ) * 500;
					}else if ( obj.name == "LossFromTop" ){
						renewMaxProfit();
					}else if ( obj.ifTouch ){
						if ( chkParm != 0 ){
							buildIfTouchMarco( obj.name , chkParm );
						}else{
							stopIfTouchMarco( obj.name );
						}
					}

					var parmName = "_guard_" + obj.name + "_parm";

					setMacro( parmName , chkParm );
					msg += obj.title + " Changed to " + chkParm + obj.unit + ". ";
				}
			}

			if ( obj.showOn ){
				var chk = $( div ).find( ".confOnChk" ).is( ":checked" );

				var chkNum = chk ? 1 : 0;
				var chkMsg = chk ? "Enabled" : "Disabled";

				if ( obj.on != chk ){
					obj.on = chk;

					var parmName = "_guard_" + obj.name + "_on";

					setMacro( parmName , chkNum );
					msg += chkMsg + " '" + obj.title + "' On Function. ";
				}
			}

			if ( obj.showOff ){
				var chk = $( div ).find( ".confOffChk" ).is( ":checked" );

				var chkNum = chk ? 1 : 0;
				var chkMsg = chk ? "Enable" : "Disabled";

				if ( obj.off != chk ){
					chkChg = true;
					obj.off = chk;

					if ( obj.name == "LossFromTop" && chk ){
						renewMaxProfit();
					}else if ( chk ){

					}

					var parmName = "_guard_" + obj.name + "_off";

					setMacro( parmName , chkNum );
					msg += chkMsg + " '" + obj.title + "' Off Function. ";
				}
			}

			if ( obj.showPause ){
				var chk = $( div ).find( ".confPauseChk" ).is( ":checked" );

				var chkNum = chk ? 1 : 0;
				var chkMsg = chk ? "Enable" : "Disabled";

				if ( obj.pause != chk ){
					chkChg = true;
					obj.pause = chk;

					if ( obj.name == "LossFromTop" && chk ){
						renewMaxProfit();
					}

					var parmName = "_guard_" + obj.name + "_pause";

					setMacro( parmName , chkNum );
					msg += chkMsg + " '" + obj.title + "' Pause Function. ";
				}
			}

			if ( obj.showFlat ){
				var chk = $( div ).find( ".confFlatChk" ).is( ":checked" );

				var chkNum = chk ? 1 : 0;
				var chkMsg = chk ? "Enable" : "Disabled";

				if ( obj.flat != chk ){
					chkChg = true;
					obj.flat == chk;

					if ( obj.name == "LossFromTop" && chk ){
						renewMaxProfit();
					}

					var parmName = "_guard_" + obj.name + "_flat";

					setMacro( parmName , chkNum );
					msg += chkMsg + " '" + obj.title + "' Flat Function. ";
				}
			}

			if ( obj.showFlatAll ){
				var chk = $( div ).find( ".confFlatAllChk" ).is( ":checked" );

				var chkNum = chk ? 1 : 0;
				var chkMsg = chk ? "Enable" : "Disabled";

				if ( obj.flatAll != chk ){
					chkChg = true;
					obj.flatAll == chk;

					if ( obj.name == "LossFromTop" && chk ){
						renewMaxProfit();
					}

					var parmName = "_guard_" + obj.name + "_flatAll";

					setMacro( parmName , chkNum );
					msg += chkMsg + " '" + obj.title + "' Flat All Function. ";
				}
			}

			if ( obj.showNotice ){
				var chk = $( div ).find( ".confNoticeChk" ).is( ":checked" );

				var chkNum = chk ? 1 : 0;
				var chkMsg = chk ? "Enable" : "Disabled";

				if ( obj.notice != chk ){
					chkChg = true;
					obj.notice == chk;

					if ( obj.name == "LossFromTop" && chk ){
						renewMaxProfit();
					}

					var parmName = "_guard_" + obj.name + "_notice";

					setMacro( parmName , chkNum );
					msg += chkMsg + " '" + obj.title + "' Notice Function. ";
				}
			}

			if ( obj.ifTouch && ( chkChg || parmChg ) && obj.parm != 0 ){
				if ( obj.off || obj.pause || obj.flat || obj.flatAll || obj.notice ){
					stopIfTouchMarco( obj.name );
					runIfTouchMarco( "Near" , obj.name );
				}else{
					stopIfTouchMarco( obj.name );
				}
			}
		}

		if ( msg != "" ){ // tell every one panel conf is changed.
			getSeqNum( gByFunction );
			msg += "Seq No.: " + gSeqNum + ".";

			var tabMsg = "Updated Guardian Parameter : " + msg;
			sendTabMsg( tabMsg );
		}

		$( ".confDiv" ).removeClass( "confGuardWarn" );
		$( ".panlBtnDiv" ).removeClass( "fontColorNotice" );
		$( ".txtIpt" ).removeClass( "confWarnIpt" );

		hideCfgSec();
	}

	function uiClickCfgRowBtn(){ // config single row setting
		var confRowSec = $( this ).closest( ".macRow" ).find( ".confRowSec" );
		var confRowReplaceDelayDiv = $( confRowSec ).find( ".confRowReplaceDelayDiv" );
		var confRowUpdateFeqDiv = $( confRowSec ).find( ".confRowUpdateFeqDiv" );
		var confRowOrderMethodDiv = $( confRowSec ).find( ".confRowOrderMethodDiv" );
		var confRowOrdTypeDivDiv = $( confRowSec ).find( ".confRowOrdTypeDiv" );

		if ( $( confRowSec ).is( ":visible" ) ){
			return;
		}

		gRowRepOrdDly = $( confRowReplaceDelayDiv ).find( ".txtIpt" ).val(); // store current row setting to a global variable
		gRowUpdFeq = $( confRowUpdateFeqDiv ).find( ".txtIpt" ).val();
		gRowOrdSpeed = $( confRowOrderMethodDiv ).find( ".txtIpt" ).val();
		gRowOrdMethod = $( confRowOrderMethodDiv ).find( ".current" ).html();
		gRowOrdMethod = gRowOrdMethod.replace( / /g , "" );

		var ordType = $( confRowSec ).find( ".confRowOrdTypeDiv" ).find( ".current" ).html();
		gRowOrdType = ordType.replace( / /g , "" );

		$( confRowSec ).fadeIn();
	}

	function uiConfimRowCfg(){
		var confRowSec = $( this ).closest( ".macRow" ).find( ".confRowSec" );

		var updFeqDiv = $( confRowSec ).find( ".confRowUpdateFeqDiv" );
		var chkUpdFeq = checkIptVal( updFeqDiv , readParmAry( gConfPanelAry , "UpdateFeq" ).parmMax , readParmAry( gConfPanelAry , "UpdateFeq" ).parmMin ); // input validation
		if ( !chkUpdFeq ){
			return;
		}

		var repOrdDlyDiv = $( confRowSec ).find( ".confRowReplaceDelayDiv" );
		var chkRepOrdDly = checkIptVal( repOrdDlyDiv , readParmAry( gConfPanelAry , "ReplaceDelay" ).parmMax , readParmAry( gConfPanelAry , "ReplaceDelay" ).parmMin );
		if ( !chkRepOrdDly ){
			return;
		}

		var ordMethodDiv = $( confRowSec ).find( ".confRowOrderMethodDiv" );
		var chkOrdMethod = checkIptVal( ordMethodDiv , readParmAry( gConfPanelAry , "OrderMethod" ).parmMax , readParmAry( gConfPanelAry , "OrderMethod" ).parmMin );
		if ( !chkOrdMethod ){
			return;
		}

		var ordMethod = $( ordMethodDiv ).find( ".current" ).html();
		ordMethod = ordMethod.replace( / /g , "" );

		var ordType = $( confRowSec ).find( ".confRowOrdTypeDiv" ).find( ".current" ).html();
		ordType = ordType.replace( / /g , "" );

		if ( gRowRepOrdDly == chkRepOrdDly && gRowUpdFeq == chkUpdFeq && gRowOrdSpeed == chkOrdMethod && gRowOrdMethod == ordMethod && gRowOrdType == ordType ){ // check need to update the macro or not
			hideCfgRowSec();
			return;
		}

		var rowID = "#" + $( this ).closest( ".macRow" ).attr( 'id' );

		updateMacro( rowID , "BUY" ); // update and restart macro to apply the change
		restartMacro( rowID , "BUY" );
		updateMacro( rowID , "SELL" );
		restartMacro( rowID , "SELL" );

		hideCfgRowSec();
	}

	function hideCfgSec(){
		$( ".confPanlSec" ).fadeOut();
		$( ".confGuarSec" ).fadeOut();
		$( ".confModeSec" ).fadeOut();
	}

	function hideCfgRowSec(){
		$( ".confRowSec" ).fadeOut();
		$( ".confSymSprdDiv" ).fadeOut();
	}

	function hideCfmSec(){
		$( ".cfmCvr" ).fadeOut();
		$( ".cfmSec" ).fadeOut();
	}

	///////////////// set price spread function ///////////////

	function uiClickCfgSpread(){ // config each symbol spread
		var macRow = $( this ).closest( ".macRow" );
		var confSymSprdDiv = $( macRow ).find( ".confSymSprdDiv" );
		if ( $( confSymSprdDiv ).is( ":visible" ) ){
			return;
		}

		var ecn = $( macRow ).find( ".ecnDiv" ).find( ".current" ).html();
		ecn = ecn.replace( / /g , "" );

		var sym = $( macRow ).find( ".symDiv" ).find( ".current" ).html();
		sym = sym.replace( / /g , "" );

		var bid = parseInt( $( confSymSprdDiv ).find( ".bidSprdDiv" ).find( ".txtIpt" ).val() );
		var ask = parseInt( $( confSymSprdDiv ).find( ".askSprdDiv" ).find( ".txtIpt" ).val() );

		symSpread = getSymSpread( ecn , sym );
		mBid = symSpread[ 0 ];
		mAsk = symSpread[ 1 ];

		if ( bid != mBid ){
			$( confSymSprdDiv ).find( ".bidSprdDiv" ).find( ".txtIpt" ).val( mBid );
			$( confSymSprdDiv ).find( ".sprdSlidr" ).dragslider( "values" , 0 , mBid - gSpreadMid );
		}

		if ( ask != mAsk ){
			$( confSymSprdDiv ).find( ".askSprdDiv" ).find( ".txtIpt" ).val( mAsk );
			$( confSymSprdDiv ).find( ".sprdSlidr" ).dragslider( "values" , 1 , mAsk + gSpreadMid );
		}
		$( confSymSprdDiv ).fadeIn();
	}

	function getMinMaxBidAskPips( rowID ){ // get the minimum and maximum pip for the symbol
		var ecn = $( rowID ).find( ".ecnDiv" ).find( ".current" ).html();
		ecn = ecn.replace( / /g , "" );
		var sym = $( rowID ).find( ".symDiv" ).find( ".current" ).html();
		sym = sym.replace( / /g , "" );

		var maxBid = -1000;
		var minAsk = 1000;

		$( ".macRow" ).each( function (){
			var row = $( this );
			var rEcn = $( row ).find( ".ecnDiv" ).find( ".current" ).html();
			rEcn = rEcn.replace( / /g , "" );

			var rEcn = $( row ).find( ".symDiv" ).find( ".current" ).html();
			rEcn = rEcn.replace( / /g , "" );

			if ( rEcn != ecn ){
				return;
			}
			if ( rSym != sym ){
				return;
			}

			bid = parseInt( $( row ).find( ".bidSec" ).find( ".ctrlPipsDiv" ).find( ".txtIpt" ).val() );
			ask = parseInt( $( row ).find( ".askSec" ).find( ".ctrlPipsDiv" ).find( ".txtIpt" ).val() );

			if ( bid > maxBid ){
				maxBid = bid;
			}
			if ( ask < minAsk ){
				minAsk = ask;
			}
		} );

		var bidAskVal = { maxBid: maxBid , minAsk: minAsk , };
		return bidAskVal;
	}

	function uiClickChgSpread(){ // click changing symbol spread button
		var sprdBtn = $( this );
		var rowID = "#" + $( sprdBtn ).closest( ".macRow" ).attr( "id" );

		var bidAskVal = getMinMaxBidAskPips( rowID );
		var maxBid = bidAskVal.maxBid;
		var minAsk = bidAskVal.minAsk;

		var confSymSprdDiv = $( rowID ).closest( ".macRow" ).find( ".confSymSprdDiv" );
		var bid = parseInt( $( confSymSprdDiv ).find( ".bidSprdDiv" ).find( ".txtIpt" ).val() );
		var ask = parseInt( $( confSymSprdDiv ).find( ".askSprdDiv" ).find( ".txtIpt" ).val() );

		if ( $( sprdBtn ).hasClass( "addSprdBtn" ) ){
			var newBid = bid - 1;
			var newAsk = ask + 1;
		}else if ( $( sprdBtn ).hasClass( "minusSprdBtn" ) ){
			var newBid = bid + 1;
			var newAsk = ask - 1;
		}else if ( $( sprdBtn ).hasClass( "bidShfitBtn" ) ){
			var newBid = bid - 1;
			var newAsk = ask - 1;
		}else if ( $( sprdBtn ).hasClass( "askShfitBtn" ) ){
			var newBid = bid + 1;
			var newAsk = ask + 1;
		}else if ( $( sprdBtn ).hasClass( "resetSprdBtn" ) ){
			var newBid = 0;
			var newAsk = 0;
		}

		if ( newBid < -gSpreadMax || ( newBid + maxBid ) > 0 ){ // validate the new bid and ask
			return;
		}
		if ( newAsk > gSpreadMax || ( newAsk + minAsk ) < 0 ){
			return;
		}

		$( confSymSprdDiv ).find( ".bidSprdDiv" ).find( ".txtIpt" ).val( newBid );
		$( confSymSprdDiv ).find( ".askSprdDiv" ).find( ".txtIpt" ).val( newAsk );
		$( confSymSprdDiv ).find( ".sprdSlidr" ).dragslider( "values" , 0 , newBid - gSpreadMid );
		$( confSymSprdDiv ).find( ".sprdSlidr" ).dragslider( "values" , 1 , newAsk + gSpreadMid );
	}

	function uiConfimSymSprdCfg(){ // confirm changing symbol spread
		var macRow = $( this ).closest( ".macRow" );
		var rowID = "#" + $( macRow ).attr( 'id' );
		var confSymSprdDiv = $( macRow ).find( ".confSymSprdDiv" );

		var ecn = $( macRow ).find( ".ecnDiv" ).find( ".current" ).html();
		ecn = ecn.replace( / /g , "" );

		var sym = $( macRow ).find( ".symDiv" ).find( ".current" ).html();
		sym = sym.replace( / /g , "" );

		var bid = parseInt( $( confSymSprdDiv ).find( ".bidSprdDiv" ).find( ".txtIpt" ).val() );
		var ask = parseInt( $( confSymSprdDiv ).find( ".askSprdDiv" ).find( ".txtIpt" ).val() );

		if ( ! $.isNumeric( bid ) || ! $.isNumeric( ask ) ){ // input validation
			return;
		}

		var setParm = [];
		if ( bid == 0 && ask == 0 ){
			setParm.push( ecn , sym , bid , ask );
			setMacro( "_SYM_SPREAD" , setParm );
		}

		var newSym = 1;
		for ( var i = 0 ; i < gSymSprdAry.length ; i ++ ){
			var mEcn = gSymSprdAry[ i ][ 0 ];
			var mSym = gSymSprdAry[ i ][ 1 ];
			var mBid = gSymSprdAry[ i ][ 2 ];
			var mAsk = gSymSprdAry[ i ][ 3 ];

			if ( mEcn != ecn ){
				continue;
			}
			if ( mSym != sym ){
				continue;
			}

			newSym = 0;
			if ( mBid != 0 || mAsk != 0 ){
				gSymSprdAry[ i ] = [ ecn , sym , bid , ask ];
			}
			break;
		}

		if ( newSym == 1 ){
			gSymSprdAry[ gSymSprdAry.length ] = [ ecn , sym , bid , ask ];
		}

		setParm = [];

		for ( var i = 0 ; i < gSymSprdAry.length ; i ++ ){
			var mEcn = gSymSprdAry[ i ][ 0 ];
			var mSym = gSymSprdAry[ i ][ 1 ];
			var mBid = gSymSprdAry[ i ][ 2 ];
			var mAsk = gSymSprdAry[ i ][ 3 ];
			if ( mBid != 0 || mAsk != 0 ){
				setParm.push( mEcn , mSym , mBid , mAsk );
			}
		}

		setMacro( "_SYM_SPREAD" , setParm ); // save symbol spread information to a macro
		// e.g. CMD 302 MACRO _SYM_SPREAD NAME { MREX CME/CMX_GLD/DEC17 -1 0 }

		$( ".macRow" ).each( function (){
			var macRow = $( this );

			var rEcn = $( macRow ).find( ".ecnDiv" ).find( ".current" ).html();
			rEcn = rEcn.replace( / /g , "" );

			var rSym = $( macRow ).find( ".symDiv" ).find( ".current" ).html();
			rSym = rSym.replace( / /g , "" );

			if ( rEcn != ecn ){
				return;
			}
			if ( rSym != sym ){
				return;
			}

			var rowID = "#" + $( macRow ).attr( 'id' );

			updateMacro( rowID , "BUY" ); // update and restart macro to apply the change
			restartMacro( rowID , "BUY" );
			updateMacro( rowID , "SELL" );
			restartMacro( rowID , "SELL" );
		} );

		hideCfgRowSec();
	}

	$.widget( "ui.dragslider" , $.ui.slider , { // create symbol spread slider

		options: $.extend( { } , $.ui.slider.prototype.options , { rangeDrag:false } ) ,

		_create: function (){
			$.ui.slider.prototype._create.apply( this , arguments );
			this._rangeCapture = false;
		} ,

		_mouseCapture: function ( event ) {
			var o = this.options;

			if ( o.disabled ) {
				return false;
			}

			if ( event.target == this.range.get( 0 ) && o.rangeDrag == true && o.range == true ) {
				this._rangeCapture = true;
				this._rangeStart = null;
			} else {
				this._rangeCapture = false;
			}

			$.ui.slider.prototype._mouseCapture.apply( this , arguments );

			if ( this._rangeCapture == true ) {
				this.handles.removeClass( "ui-state-active" ).blur();
			}

			return true;
		} ,

		_mouseStop: function ( event ) {
			this._rangeStart = null;
			return $.ui.slider.prototype._mouseStop.apply( this , arguments );
		} ,

		_slide: function ( event , index , newVal ) {
			if ( !this._rangeCapture ) {
				return $.ui.slider.prototype._slide.apply( this , arguments );
			}

			if ( this._rangeStart == null ) {
				this._rangeStart = newVal;
			}

			var oldValLeft = this.options.values[ 0 ] ,
			oldValRight = this.options.values[ 1 ] ,
			slideDist = newVal - this._rangeStart ,
			newValueLeft = oldValLeft + slideDist ,
			newValueRight = oldValRight + slideDist ,
			allowed;

			if ( this.options.values && this.options.values.length ) {
				if ( newValueRight > this._valueMax() && slideDist > 0 ) {
					slideDist -= ( newValueRight-this._valueMax() );
					newValueLeft = oldValLeft + slideDist;
					newValueRight = oldValRight + slideDist;
				}

				if ( newValueLeft < this._valueMin() ) {
					slideDist += ( this._valueMin()-newValueLeft );
					newValueLeft = oldValLeft + slideDist;
					newValueRight = oldValRight + slideDist;
				}

				if ( slideDist != 0 ) {
					newValues = this.values();
					newValues[ 0 ] = newValueLeft;
					newValues[ 1 ] = newValueRight;

					allowed = this._trigger( "slide" , event , {
						handle: this.handles[ index ] ,
						value: slideDist ,
						values: newValues
					} );

					if ( allowed !== false ) {
						this.values( 0 , newValueLeft , true );
						this.values( 1 , newValueRight , true );
					}
					this._rangeStart = newVal;
				}
			}
			$( this ).closest( ".confSymSprdDiv" ).find( ".bidSprdDiv" ).find( ".txtIpt" ).val( newValueLeft + gSpreadMid );
			$( this ).closest( ".confSymSprdDiv" ).find( ".askSprdDiv" ).find( ".txtIpt" ).val( newValueRight - gSpreadMid );
		} ,
	} );

	function readSymSpread( cmrAry ){ // handle symbol spread CMR
		// e.g. CMR 302 MACRO _SYM_SPREAD NAME { MREX CME/CMX_GLD/DEC17 -1 1 }
		gSymSprdAry = []; // clear old information

		//for ( var i = 6 ; i < cmrAry.length - 3 ; i = i + 4 ){ // use this if cmd added seq number
		for ( var i = 6 ; i < cmrAry.length - 1 ; i = i + 4 ){
			var ecn = cmrAry[ i ];
			var sym = cmrAry[ i + 1 ];
			var bid = parseInt( cmrAry[ i + 2 ] );
			var ask = parseInt( cmrAry[ i + 3 ] );

			var newSym = 1;

			for ( var j = 0 ; j < gSymSprdAry.length ; j ++ ){
				var mEcn = gSymSprdAry[ j ][ 0 ];
				var mSym = gSymSprdAry[ j ][ 1 ];

				if ( mEcn != ecn ){
					continue;
				}
				if ( mSym != sym ){
					continue;
				}

				newSym = 0;
				gSymSprdAry[ j ] = [ ecn , sym , bid , ask ];
				break;
			}

			if ( newSym == 1 ){
				gSymSprdAry[ gSymSprdAry.length ] = [ ecn , sym , bid , ask ]; // save spread information to an array
			}
		}

		showSymSpread(); // show the spread information
	}

	function getSymSpread( ecn , sym ){ // get current spread for specify product
		var symSpread = [ 0 , 0 ];

		for ( var j = 0 ; j< gSymSprdAry.length ; j ++ ){
			var mEcn = gSymSprdAry[ j ][ 0 ];
			var mSym = gSymSprdAry[ j ][ 1 ];
			var mBid = gSymSprdAry[ j ][ 2 ];
			var mAsk = gSymSprdAry[ j ][ 3 ];

			if ( mEcn != ecn ){
				continue;
			}
			if ( mSym != sym ){
				continue;
			}
			symSpread = [ mBid , mAsk ];
			break;
		}

		return symSpread;
	}

	function showSymSpread(){ // show spread on row
		$( ".infSprdDiv" ).find( ".txtIpt" ).val( 0 );

		for ( var i = 0 ; i < gSymSprdAry.length ; i ++ ){
			var ecn = gSymSprdAry[ i ][ 0 ];
			var sym = gSymSprdAry[ i ][ 1 ];
			var bidSp = parseFloat( gSymSprdAry[ i ][ 2 ] );
			var askSp = parseFloat( gSymSprdAry[ i ][ 3 ] );

			$( ".macRow" ).each( function (){
				var rowID = $( this );

				var mEcn = $( rowID ).find( ".ecnDiv" ).find( ".current" ).html();
				mEcn = mEcn.replace( / /g , "" );

				var mSym = $( rowID ).find( ".symDiv" ).find( ".current" ).html();
				mSym = mSym.replace( / /g , "" );

				if ( mEcn != ecn ){
					return;
				}
				if ( mSym != sym ){
					return;
				}
				setSp( rowID , ".bidSec" , bidSp );
				setSldr( rowID , ".bidSec" , bidSp );
				setSp( rowID , ".askSec" , askSp );
				setSldr( rowID , ".askSec" , askSp );
			} );
		}
		function setSp( rowID , bidOrAsk , spread ){
			var oSpread = parseFloat( $( rowID ).find( bidOrAsk ).find( ".infSprdDiv" ).find( ".txtIpt" ).val() );
			if ( oSpread == spread ){
				return;
			}
			if ( spread > 0 ){
				spread = "+" + spread;
			}
			$( rowID ).find( bidOrAsk ).find( ".infSprdDiv" ).find( ".txtIpt" ).val( spread );
		}
		function setSldr( rowID , bidOrAsk , spread ){
			var oPx = parseFloat( $( rowID ).find( bidOrAsk ).find( ".ctrlPipsDiv" ).find( ".ctrlVal" ).val() );
			var sldrVal = oPx - spread;
			var oSldrVal = parseFloat( $( rowID ).find( bidOrAsk ).find( ".ctrlPipsDiv" ).find( ".sldrHd" ).text() );

			if ( sldrVal == oSldrVal ){
				return;
			}
			if ( sldrVal > 0 ){
				sldrVal = "+" + sldrVal;
			}
			$( rowID ).find( bidOrAsk ).find( ".ctrlPipsDiv" ).find( ".sldrHd" ).text( sldrVal );
			$( rowID ).find( bidOrAsk ).find( ".ctrlPipsDiv" ).find( ".txtIpt" ).val( sldrVal );
		}
	}

	/////////////////// set , change mode function //////////////////

	function readMode( cmrAry ){ // read mode list
		// e.g. CMR 302 MACRO _MODE_M01_MODE01 NAME { algoMac_02_SELL algoMac_01_SELL }
		var ary = [];
		var id = cmrAry[ 3 ].replace( "_MODE_" , "" );

		ary.push( id );
		//for ( var i = 6 ; i < cmrAry.length - 3 ; i ++ ){ // use this if cmd added seq number
		for ( var i = 6 ; i < cmrAry.length - 1 ; i ++ ){
			var onMac = cmrAry[ i ];
			ary.push( onMac );
		}

		var newID = 1;
		for ( var i = 0 ; i < gModeAry.length ; i ++ ){
			if ( gModeAry[ i ][ 0 ] != id ){
				continue;
			}

			gModeAry[ i ] = ary;
			newID = 0;
			break;
		}

		if ( newID == 1 ){
			gModeAry.push( ary );
		}
		showModeLi( id ); // show mode to a LI
	}

	function readDeleteMode( cmrAry ){ // read delete mode message
		// e.g. CMR 302 MACRO_DELETE _MODE_M01_MODE01
		var id = cmrAry[ 3 ].replace( "_MODE_" , "" );

		for ( var i = 0 ; i < gModeAry.length ; i ++ ){
			if ( gModeAry[ i ][ 0 ] != id ){
				continue;
			}

			gModeAry.splice( i , 1 );
			break;
		}
		removeModeLi( id );
	}

	function uiClickPanelMode(){ // show mode panel
		$( ".confModeLi" ).remove(); // remove all old mode li
		showModeList(); // add all exist mode to li
		$( ".confModeSec" ).show();
	}

	function uiClickNewMode(){ // add a new mode by user
		var num = getModeNum() + 1;
		if ( num < 10 ){
			num = "0" + num;
		}
		var pos = num;
		var name = "MODE" + num;
		var id = "M" + pos + "_" + name;
		saveMode( id );
		setCurrentMode( id ); // set current mode to new mode
	}

	function setCurrentMode( id ){ // set current mode
		var name = "_CURRENT_MODE";
		setMacro( name , id );
	}

	function uiClickSaveMode(){ // replace mode
		if ( $( ".currentMode" ).length == 0 ){
			return;
		}

		gModeCfmAct = 1;
		var name = $( ".currentMode" ).html();
		var msg = "Confirm to replace " + name + "?";

		$( ".confModeTxtSn" ).html( msg );
		$( ".confModeTxtDiv" ).show();
		$( ".confModeIptDiv" ).hide();
		$( ".confModeSecCvr" ).show();
		$( ".confModeCfmSec" ).show();
	}

	function uiClickRenameMode(){ // rename mode
		if ( $( ".currentMode" ).length == 0 ){
			return;
		}

		gModeCfmAct = 3;
		var name = $( ".currentMode" ).html();

		$( ".confModeCfmSec" ).find( ".txtIpt" ).val( name );
		$( ".confModeTxtDiv" ).hide();
		$( ".confModeIptDiv" ).show();
		$( ".confModeSecCvr" ).show();
		$( ".confModeCfmSec" ).show();
		$( ".confModeCfmSec" ).show();
		$( ".confModeCfmSec" ).find( ".txtIpt" ).focus();
	}

	function uiClickDeleteMode(){ // delete mode
		if ( $( ".currentMode" ).length == 0 ){
			return;
		}

		gModeCfmAct = 4;
		var name = $( ".currentMode" ).html();
		var msg = "Confirm to delete " + name + "?";

		$( ".confModeTxtSn" ).html( msg );
		$( ".confModeTxtDiv" ).show();
		$( ".confModeIptDiv" ).hide();
		$( ".confModeSecCvr" ).show();
		$( ".confModeCfmSec" ).show();
	}

	function uiClickDeleteAllMode(){
		for ( var i = 0 ; i < gModeAry.length ; i ++ ){
			var id = gModeAry[ i ][ 0 ];
			deleteMode( id );
		}

		setCurrentMode( "EMPTY" ); // clear current mode setting
	}

	function uiClickModeCfmBtn(){ // confirm to replace/rename/delete mode
		var id = $( ".currentMode" ).attr( "id" );

		if ( gModeCfmAct == 1 ){ // replace mode
			saveMode( id );
		}else if ( gModeCfmAct == 3 ){ // rename mode
			var name = $( ".confModeCfmSec" ).find( ".txtIpt" ).val();
			if ( ! validateModeName( name ) ){
				return;
			}
			name = validateModeName( name );
			chgModeName( id , name );
		}else if ( gModeCfmAct == 4 ){ // delete mode
			deleteMode( id );
			setCurrentMode( "EMPTY" );
		}

		$( ".confModeSecCvr" ).hide();
		$( ".confModeCfmSec" ).hide();
	}

	function uiClickModeCnlBtn(){ // cancel to replace/rename/delete mode
		$( ".confModeSecCvr" ).hide();
		$( ".confModeCfmSec" ).hide();
	}

	function uiClickUseMode(){ // use mode
		var id = $( this ).attr( "id" );
		changeMode( id , "Manu" , 0 )
	}

	function showModeList(){
		gModeAry.sort();
		for ( var i = 0 ; i < gModeAry.length ; i ++ ){
			var id = gModeAry[ i ][ 0 ];
			showModeLi( id );
		}

		showCurrentMode( gCurrMode );
	}

	function showModeLi( id ) {
		if ( $( "#" + id ).length > 0 ){
			return;
		}
		var pos = id.split( "_" )[ 0 ];
		var name = id.split( "_" )[ 1 ];
		var str = name.substr( 0 , 1 ).toUpperCase();
		var className = "";

		if ( str == "X" ){ // add background color if mode name is start from "X" or "Y" or "Z"
			className = "confModeX";
		}else if ( str == "Y" ){
			className = "confModeY";
		}else if ( str == "Z" ){
			className = "confModeZ";
		}else if ( str == "W" ){
			className = "confModeW";
		}else if ( str == "S" ){
			className = "confModeS";
		}

		$( ".confModeListUl" ).append( "<li class = 'confModeLi " + className + "' id = '" + pos + "_" + name + "' >" + name + "</li>" );
	}

	function useMode( id ){
		var name = id.split( "_" )[ 1 ];
		showCurrentMode( id ); // change current mode to this mode

		var update = 0;

		for ( var i = 0 ; i < gModeAry.length ; i ++ ){
			if ( gModeAry[ i ][ 0 ] != id ){
				continue;
			}

			gMacOnAry = [];

			for ( var j = 1 ; j < gModeAry[ i ].length ; j ++ ){
				var macName = gModeAry[ i ][ j ];
				var numStr = macName.split( "_" )[ 1 ];
				var rowID = "#macRow_" + numStr;

				if ( $( rowID ).length == 0 ){
					update = 1;
					continue;
				}

				gMacOnAry.push( gModeAry[ i ][ j ] );
			}
			break;
		}

		setMacro( "_ON_MACRO" , gMacOnAry );

		if ( update == 1 ){ // check if mode content is changed. ( some row are deleted. )
			var mode = "_MODE_" + id;
			setMacro( mode , gMacOnAry );
		}

		loadMacroSwtich(); // turn on macro
	}

	function removeModeLi( id ) {
		$( "#" + id ).remove();
	}

	function saveMode( id ){
		var name = "_MODE_" + id;

		setMacro( name , gMacOnAry );

		updateAllMacro(); // save all macro setting before saving mode to ensure macro setting is correct
	}

	function deleteMode( id ){
		var name = "_MODE_" + id;

		getSeqNum( gByManual );
		deleteMacro( name );
	}

	function clearMode( id ){
		getSeqNum( gByManual );
		//var cmd = "CMD " + gInitUserID + " BASE_CMD { CLEAR_BASE " + id + " }" + " SEQ " + gSeqNum;
		var cmd = "CMD " + gInitUserID + " BASE_CMD { CLEAR_BASE " + id + " }";

		pushCMD( cmd );
		showLog( "clearMode" , cmd );
	}

	function chgModeName( id , name ){
		var pos = id.split( "_" )[ 0 ];
		pos = pos.replace( "M" , "" );
		deleteMode( id );

		var newID = "M" + pos + "_" + name;
		saveMode( newID );
		setCurrentMode( newID );

		setTimeout( function (){
			$( ".confModeLi" ).remove();
			showModeList();
		} , 100 );
	}

	function validateModeName( input ){ // validate mode name ( not allow just to input @ # ! etc )
		if ( input.length == "" ){
			return false;
		}
		input = input.replace( / /g , "-" );
		input = input.replace( /_/g , "-" );
		input = input.replace( /"/g , "-" );
		input = input.replace( /\//g , "-" );
		input = input.replace( "|" , "-" );
		var regex = /[^a-zA-Z0-9\-]/;
		if ( regex.test( input ) ) {
			return false;
		}
		return input;
	}

	function getModeNum(){
		var num = 0;
		$( ".confModeLi" ).each( function (){
			var id = $( this ).attr( "id" );
			var pos = id.split( "_" )[ 0 ];
			pos = parseInt( pos.replace( "M" , "" ) );
			if ( pos > num ){
				num = pos;
			}
		} );
		return num;
	}

	function showCurrentMode( id ){
		$( ".confModeLi" ).removeClass( "currentMode" );

		if ( id == "EMPTY" ){
			$( ".panlCurrentModeDiv" ).hide();
		}else{
			var name = id.split( "_" )[ 1 ];
			$( ".panlCurrentModeSn" ).html( name );
			$( ".panlCurrentModeDiv" ).show();

			$( "#" + id ).addClass( "currentMode" );
		}
	}

	function getCorrectModeID(){ // get correct mode id by checking net position
		var id = "";

		if ( gCurrMode == "EMPTY" || typeof gCurrMode == "undefined" || gCurrMode == "" ){
			return id;
		}

		var currName = gCurrMode.split( "_" )[ 1 ]; // e.g. "M01_X6B9" > "X6B9"

		var split_B = "B";
		var split_T = "T";

		if ( currName.search( split_B ) > 0 ){ // see whether the user separate number by "B" or "T"
			var splitStr = split_B;
		}else{
			var splitStr = split_T;
		}

		var currV = currName.slice( 0 , 1 ).toUpperCase(); // e.g. "X6B9" > "X"
		var currQtyA = parseInt( currName.slice( 1 , currName.length ).split( splitStr )[ 0 ] ); // e.g "X6B9" > 6
		var currQtyB = parseInt( currName.slice( 1 , currName.length ).split( splitStr )[ 1 ] ); // e.g "X6B9" > 9

		if ( ! $.isNumeric ( currQtyA ) ){ // check current mode include a number
			return id;
		}

		var netPos = getFirstRowNetPos();

		if ( ! $.isNumeric( netPos ) ){
			return id;
		}

		var modeQtyAry = [];
		for ( var i = 0 ; i < gModeAry.length ; i ++ ){
			var modeId = gModeAry[ i ][ 0 ];

			var name = modeId.split( "_" )[ 1 ];
			if ( name == "" || typeof name == "undefined" ){ // check mode name
				continue;
			}

			var mV = name.slice( 0 , 1 ); // check mode V
			if ( mV != currV ){
				continue;
			}

			if ( name.search( split_B ) > 0 ){ // check separate string ( B or T )
				var splitStr = split_B;
			}else{
				var splitStr = split_T;
			}

			var mQtyA = parseInt( name.slice( 1 , name.length ).split( splitStr )[ 0 ] ); // check mode qty
			var mQtyB = parseInt( name.slice( 1 , name.length ).split( splitStr )[ 1 ] );

			if ( ! $.isNumeric( mQtyA ) ){
				continue;
			}

			var mCalA = mQtyA; // modify array content for calulation
			var mCalB = mQtyB;

			if ( mQtyA < 0 && mQtyA > mQtyB ){
				mCalA = mQtyB;
				mCalB = mQtyA;
			}
			if ( mQtyA == 0 ){
				mCalA = -mQtyB;
			}
			if ( isNaN( mQtyB ) ){
				mCalB = mQtyA;
			}

			modeQtyAry[ modeQtyAry.length ] = [ modeId , mQtyA , mQtyB , mCalA , mCalB ];
		}

		modeQtyAry.sort( function ( a , b ){ // sort modeQtyAry
			return a[ 1 ] - b[ 1 ];
		} );

		var currIndex = -1;
		for ( var i = 0 ; i < modeQtyAry.length ; i ++ ){ // get the index number of current mode
			var mQtyA = modeQtyAry[ i ][ 1 ];
			var mQtyB = modeQtyAry[ i ][ 2 ];

			if ( currQtyA != mQtyA || currQtyB.toString() != mQtyB.toString() ){
				continue;
			}

			currIndex = i;
			break;
		}

		var newIndex = -1;
		for ( var i = 0 ; i < currIndex ; i ++ ){
			var lowLimit = modeQtyAry[ i ][ 4 ];
			if ( netPos <= lowLimit ){
				newIndex = i;
				break;
			}
		}

		for ( var i = modeQtyAry.length - 1 ; i > currIndex ; i-- ){
			var highLimit = modeQtyAry[ i ][ 3 ];
			if ( netPos >= highLimit ){
				newIndex = i;
				break;
			}
		}

		if ( newIndex == -1 ){
			return id;
		}

		if ( newIndex == currIndex ){ // see need to change mode or not
			return id;
		}

		id = modeQtyAry[ newIndex ][ 0 ];

		for ( var i = 0 ; i < modeQtyAry.length ; i++ ){
			var str = "getCorrectModeID , modeQtyAry[ " + i + " ] : ";
			var ary = modeQtyAry[ i ];
			for ( var j = 0 ; j < ary.length ; j++ ){
				str += ary[ j ] + " ";
			}
		}
		return id;
	}

	function autoCheckChangeMode(){ // check auto change mode every 1 second
		if ( !gStatusObj.on ) { // check the big on off switch is on or not
			return;
		}

		var on = readParmAry( gConfPanelAry , "AutoChgMode" ).on;

		if ( !on ){
			return;
		}

		if ( gChgingMode == 1 ){
			return;
		}

		var id = getCorrectModeID();
		if ( id == "" ){
			return;
		}

		$.when( checkIsLeader() ).then( function (){
			if ( gIsLeader == 0 ){
				return;
			}
			changeMode( id , "Auto" );
		} );
	}

	function changeMode( id , type ){ 	// start changing mode process
		gChgingMode = 1;
		setTimeout( function (){
			var name = id.split( "_" )[ 1 ];

			useMode( id );
			setCurrentMode( id );

			var netPos = getFirstRowNetPos();

			if ( type == "Auto" ){
				var msg = "Auto Change Mode to " + name + " ( Net Position: " + netPos + " ) .";

			}else if ( type == "Manu" ){
				var msg = "Manual Change Mode to " + name + "."
			}

			if ( type == "Auto" ){
				sendMsg( gInitUserID , "MMNOTICE" , "CHGMODE " + msg ); // send triggered change mode notice
			}

			getSeqNum( gByFunction );

			msg += " Seq No.: " + gSeqNum + "."; // send change mode notice
			sendTabMsg( msg );

			clearTimeout( gDelayTurnOnTimer );

			stopExtraMacro(); // just stop extra macro , not stop all macro

			setTimeout( function (){
				autoTurnOn( type );

				gChgingMode = 0;
			} , 500 );
		} , 200 );
	}

	function autoTurnOn( type ){
		if ( !gStatusObj.on ) { // check the big on off switch is on or not
			return;
		}

		var autoTurnOn = readParmAry( gConfPanelAry , "AutoTurnOn" ).on;
		var chkOpenOrder = readParmAry( gConfGuardAry , "OpenOrder" ).off;

		if ( !autoTurnOn ){
			return;
		}

		$.when( checkIsLeader() ).then( function (){
			if ( gIsLeader == 0 ){
				return;
			}

			var openOrder = getOpenOrderNum(); // get open order number
			var algoNum = getRunningAlgoNum(); // get running algo number
			var limit = algoNum + gDefBufferOpenOrd; // default buffer = 3

			if ( openOrder < limit || !chkOpenOrder ){ // check outstanding open orders.
				turnOnExtraMacro( type );

			}else{
				var msg = "Fail to Auto Turn On Algo. Outstanding Open Order Number: " + openOrder + ". Algo Number: " + algoNum + ".";

				sendTabMsg( msg );

				if ( type == "Auto" ){
					sendMsg( gInitUserID , "MMNOTICE" , "CHGMODE_FAIL " + msg ); // send change mode fail notice
				}
			}
		} );
	}

	function showChgModeNotice( msg ){
		playSound( "notice" );
		showToastr( "success" , "ChangeMode" , msg );
	}

	function showChgModeFailNotice( msg ){
		$( ".panlGuarDiv" ).addClass( "fontColorNotice" );
		$( "#jpanel_msg" ).find( ".badgeSp" ).addClass( "bgColorNotice" );

		playSound( "alert" );
		showToastr( "error" , "ChangeModeFail" , msg );
	}

	//////////////////// select leader function //////////////

	function checkIsLeader(){ // check if itself is a leader or not
		var isLeaderDefer = $.Deferred();

		if ( gIsLeader == 1 ){ // already is leader.
			replylLeaderOn();
			isLeaderDefer.resolve( null );
			return isLeaderDefer;
		}

		$.when( getLeaderID() ).then( function (){
			isLeaderDefer.resolve( null );
			return isLeaderDefer;
		} );

		return isLeaderDefer;
	}

	function getLeaderID(){ // find current leader
		var getIDDefer = $.Deferred(); // create defer object
		if ( gLeaderOn == 1 || gCallingLeader == 1 || gAskingSessID == 1 ){
			getIDDefer.resolve( null );
			return getIDDefer;
		}

		clearInterval( gGetLeaderIDInterval ); // clear previous timer
		gGetLeaderIDInterval = false;

		var interval = 950; // check received response every 0.3 second
		var wait = gLeaderLiveTime; // wait 3 seconds for leader response

		var leader = gLeaderID + "::" + gLeaderIP;
		var sess = gSessID + "::" + gIP;

		callLeader();
		return getIDDefer;

		function callLeader(){
			if ( gLeaderOn == 1 || gCallingLeader == 1 || gAskingSessID == 1 ){ // check if leader has response
				getIDDefer.resolve( null );
				return getIDDefer;
			}

			sendMsg( gInitUserID , "CALL_LEADER" , leader + " " + sess ); // call leader
			gCallingLeader = 1;

			if ( !gGetLeaderIDInterval ){
				gGetLeaderIDInterval = setInterval( getLeader , interval );
			}
		}

		function getLeader(){
			if ( gLeaderOn == 1 ){ // check if leader has response
				clearInterval( gGetLeaderIDInterval );
				gGetLeaderIDInterval = false;
				getIDDefer.resolve( null );
				return getIDDefer;
			}

			if ( wait <= 0 ){
				clearInterval( gGetLeaderIDInterval );
				gGetLeaderIDInterval = false;
			}

			if ( wait <= 0 && gLeaderOn == 0 ){
				$.when( electLeader() ).then( function (){
					clearInterval( gGetLeaderIDInterval );
					gGetLeaderIDInterval = false;

					getIDDefer.resolve( null );
					return getIDDefer;
				} );
			}

			wait -= interval;
		}
	}

	function electLeader(){
		var eleDefer = $.Deferred();

		if ( gAskingSessID == 1 ){
			eleDefer.resolve( null );
			return eleDefer;
		}

		gSessIDAry = [];
		gLeaderOn = 0;

		var idIP = gSessID + "::" + gIP;

		sendMsg( gInitUserID , "GET_SESSID" , idIP ); // ask every one to send his session ID if leader do no response

		setTimeout( function (){ // wait 2 second to collect session ID
			if ( gLeaderOn == 1 ){ // check if leader has response
				eleDefer.resolve( null );
				return eleDefer;
			}

			if ( gSessIDAry.length == 0 ){
				setMacro( "_LEADER_ID" , idIP ); // assign leader

				gIsLeader = 1;
				$( "#userInfoIcon" ).show(); // show leader icon

				eleDefer.resolve( null );
				return eleDefer;
			}

			gSessIDAry = gSessIDAry.sort(); // sort session ID
			var lastIndex = gSessIDAry.length - 1;

			gLeaderID = parseInt( gSessIDAry[ lastIndex ][ 0 ] ); // use first session ID as leader ID
			gLeaderIP = gSessIDAry[ lastIndex ][ 1 ];

			if ( gLeaderID == gSessID && gLeaderIP == gIP ){
				gIsLeader = 1;
				$( "#userInfoIcon" ).show(); // show leader icon

			}else{
				gIsLeader = 0;
				$( "#userInfoIcon" ).hide();
			}

			setMacro( "_LEADER_ID" , gLeaderID + "::" + gLeaderIP ); // assign leader

			eleDefer.resolve( null );
			return eleDefer;
		} , 2000 );

		return eleDefer;
	}

	function replylLeaderOn(){ // reply leader on if it is a leader
		if ( gRepliedLeaderOn == 1 ){
			return;
		}

		sendMsg( gInitUserID , "LEADER_ON" , gSessID + "::" + gIP ); // response if itself is the leader

		gRepliedLeaderOn = 1;

		setTimeout( function (){
			gRepliedLeaderOn = 0;
		} , 500 );
	}

	function readLeaderID( cmrAry ){
		// e.g. CMR 302 MACRO _LEADER_ID NAME 927::192.168.0.110 ( someone assigned leader );
		gCallingLeader = 0;
		gAskingSessID = 0;
		clearInterval( gGetLeaderIDInterval );
		gGetLeaderIDInterval = false;

		leaderID = parseInt( cmrAry[ 5 ].split( "::" )[ 0 ] );
		leaderIP = cmrAry[ 5 ].split( "::" )[ 1 ];

		if ( typeof leaderID == "undefined" || isNaN( leaderID ) || leaderIP == 0 || typeof leaderIP == "undefined" ){
			return;
		}

		gLeaderID = leaderID;
		gLeaderIP = leaderIP;

		if ( gLeaderID == gSessID && gLeaderIP == gIP ){
			replylLeaderOn();
			resetLongInterval( 0 );
			gIsLeader = 1;
			$( "#userInfoIcon" ).show(); // show leader icon

		}else{
			gIsLeader = 0;
			$( "#userInfoIcon" ).hide();
		}
	}

	function readLeaderOnMsg( cmrAry ){
		// e.g. CMR 309 MSG LEADER_ON 308::192.168.0.110
		if ( gIsLeader == 0 ){
			resetLongInterval( 0 );
		}

		gCallingLeader = 0;
		gAskingSessID = 0;
		clearInterval( gGetLeaderIDInterval );
		gGetLeaderIDInterval = false;

		clearTimeout( gLeaderOnTimer );

		gLeaderOnTimer = setTimeout( function (){
			gLeaderOn = 0;
		} , gLeaderLiveTime );

		if ( gLeaderOn == 1 ){ // check if received leader response already
			var leaderID = parseInt( cmrAry[ 4 ].split( "::" )[ 0 ] );
			var leaderIP = cmrAry[ 4 ].split( "::" )[ 1 ];

			if ( gLeaderID != leaderID || gLeaderIP != leaderIP ){
				multipleLeader( leaderID , leaderIP ); // ask the another leader to get new ID
			}

			return;
		}

		gLeaderOn = 1;

		gLeaderID = parseInt( cmrAry[ 4 ].split( "::" )[ 0 ] );
		gLeaderIP = cmrAry[ 4 ].split( "::" )[ 1 ];

		if ( typeof gLeaderIP == "undefined" ){
			gLeaderIP = 0;
		}
	}

	function replyLeaderID( cmrAry ){ // response if someone call leader
		//e.g. CMR 302 MSG CALL_LEADER 308::192.168.0.110
		gCallingLeader = 1;

		clearTimeout( gReplyLeaderTimer ); // clear previous timer

		gReplyLeaderTimer = setTimeout( function (){ // wait 0.5 second to avoid reading too many CALL_LEADER msg
			var leaderID = cmrAry[ 4 ].split( "::" )[ 0 ];
			var ip = cmrAry[ 4 ].split( "::" )[ 1 ];

			if ( gSessID != leaderID || gIP != ip ){ // check itself is leader or not
				return;
			}

			replylLeaderOn();
		} , 500 );
	}

	function sendSessID(){ // send session id to every one
		gAskingSessID = 1;
		clearTimeout( gSendSessIDTimer ); // clear previous timer

		gSendSessIDTimer = setTimeout( function (){ // wait 0.5 sec to avoid reading too many send sessID request
			sendMsg( gInitUserID , "SEND_SESSID" , gSessID + "::" + gIP );
		} , 500 );
	}

	function collectSessID( cmrAry ){ // collect session ID information
		// e.g. CMR 302 MSG SEND_SESSID 928::192.168.0.110
		var sessID = parseInt( cmrAry[ 4 ].split( "::" )[ 0 ] );
		var ip = cmrAry[ 4 ].split( "::" )[ 1 ];

		if ( sessID == 0 || sessID.toString() == "NaN" || typeof ip == "undefined" || ip == 0 ){
			return; // ignore if it has wrong seeID or ip
		}

		if ( jQuery.inArray( sessID , gSessIDAry ) >= 0 ){
			return;
		}
		gSessIDAry[ gSessIDAry.length ] = [ sessID , ip ]; // save session ID to an array
	}

	function getSessID( cmrAry ){ // store session ID after get macro
		// e.g. CMR 302 GET_MACRO SESSID 198 SEQ 192.168.0.110::48
		var seq = cmrAry[ 6 ];

		var ip = seq.split( "::" )[ 0 ];
		var num = parseInt( seq.split( "::" )[ 1 ] );

		if ( gSessID != 0 ){
			return;
		}

		if ( ip != gIP || num != gRandomNum ){
			return;
		}

		gSessID = parseInt( cmrAry[ 4 ] );
		$( "#userInfoID" ).text( gSessID );
	}

	function multipleLeader( sessID , ip ){
		sendMsg( gInitUserID , "MULTIPLE_LEADER" , sessID + "::" + ip ); // request this user to get new session ID
	}

	function getNewSessID( cmrAry ){ // get a new session ID
		if ( gGettingSessID == 1 ){ // check the user is getting new ID or not
			return;
		}

		var sessID = parseInt( cmrAry[ 4 ].split( "::" )[ 0 ] );
		var ip = cmrAry[ 4 ].split( "::" )[ 1 ];

		if ( gSessID != sessID || gIP != ip ){ // check is it this user
			return;
		}

		gSessID = 0; // delete old ID
		gGettingSessID = 1; // getting new ID

		getMacro(); // getMacro to get new session ID

		setTimeout( function (){
			gGettingSessID == 0; // wait 0.2 second to get new ID
		} , 200 );
	}

	/////////////// get , set , read engine parameter function //////

	function getParameter( name ){
		var cmd = "CMD " + gInitUserID + " GET " + name;
		pushCMD( cmd );

		showLog( "getParameter" , cmd );
	}

	function getAllParameter(){
		for( var prop in gStatusObj ){
			getParameter( "_aStatus_" + prop );
		}
	}

	function checkParameter( name ){ // check lastest algo on off situation
		gGotParameter = 0;

		getParameter( name );

		var defer = $.Deferred();

		clearInterval( gCheckParameterInterval );
		gCheckParameterInterval = false;

		var interval = 100; // check received response every 0.3 second
		var wait = 2000; // wait 2 seconds for server response

		if ( !gCheckParameterInterval ){
			gCheckParameterInterval = setInterval( getParm , interval );
		}

		return defer;

		function getParm(){
			if ( wait > 0 && gGotParameter == 0 ){
				wait -= interval;
				return;
			}

			clearInterval( gCheckParameterInterval );
			gCheckParameterInterval = false;

			if ( wait <= 0 ){
				var msg = "Server response time out. Seq No.: " + gSeqNum + "."; // send a stop all notice if it is a leader
				sendTabMsg( msg );
			}

			defer.resolve( null );
			return defer;
		}
	}

	function setParameter( name , parm ){
		if ( ! validateParameter( name , parm , parm ) ){
			throw new Error( "Parameter is NaN or undefined." );
		}

		getSeqNum( gByFunction );
		var cmd = "CMD " + gInitUserID + " DEFAULT " + name + " " + parm;
		//var cmd = "CMD " + gInitUserID + " DEFAULT " + defName + " " + parm + " SEQ " + gSeqNum;

		pushCMD( cmd );
		showLog( "setParameter" , cmd );

		getParameter( name );
	}

	function readParameter( cmrAry ){ // read get parameter msg from server
		// e.g. CMR 302 GET _aStatus_ON 0
		var name = cmrAry[ 3 ];
		var parm = cmrAry[ 4 ];
		var group = name.split( "_" )[ 1 ];
		var type = name.split( "_" )[ 2 ];

		gGotParameter = 1;

		if ( group != "aStatus" ){
			return;
		}
		if ( parm == "NOT_FOUND" ){
			parm = 0;
		}
		parm = Boolean( parseFloat( parm ) );
		gStatusObj[ type ] = parm;

		if ( type == "on" ){
			trunPanelSwitch( parm );
		}else if ( type == "pause" ){
			if ( parm == 1 ){
				showPauseNotice();
			}else if ( parm == 0 ){
				hidePauseNotice();
				clearTimeout( gPauseMarcoTimer );
			}
		}
	}

	/////////////////// sort Row function ////////////

	function saveRowSeq(){ // save row sequence info
		gRowSeqAry = []; // clear old row sequence info

		$( ".macRow" ).each( function (){
			var rowID = $( this ).attr( 'id' );
			gRowSeqAry.push( rowID ); // add each row ID to an array
		} );

		setMacro( "_ROW_SEQ" , gRowSeqAry );
		// save row sequence info to a macro
		// e.g. CMD 302 MACRO _ROW_SEQ NAME { macRow_01 macRow_03 macRow_02 }
	}

	function readRowSeq( cmrAry ){ // read row sequence info
		// e.g. CMR 302 MACRO _ROW_SEQ NAME { macRow_01 macRow_03 macRow_02 }
		gRowSeqAry = [];
		//for ( var i = 6 ; i < cmrAry.length - 3 ; i ++ ){ // use this if cmd added seq number
		for ( var i = 6 ; i < cmrAry.length - 1 ; i ++ ){
			var rowID = cmrAry[ i ];
			gRowSeqAry.push( rowID );
		}
		applyRowSeq(); // apply the row sequence
	}

	function addRowSeq( rowID ){ // add a row to row sequence list
		var newRow = 1;

		for ( var i = 0 ; i < gRowSeqAry.length ; i ++ ){
			if ( gRowSeqAry[ i ] == rowID ){
				newRow = 0;
				break;
			}
		}

		if ( newRow == 1 ){
			gRowSeqAry.push( rowID );
		}

		setMacro( "_ROW_SEQ" , gRowSeqAry ); // update row sequence info to macro
	}

	function removeRowSeq( rowID ){ // remove row from row sequence list
		var index = gRowSeqAry.indexOf( rowID ); // find the row

		if ( index == -1 ) {
			return;
		}

		gRowSeqAry.splice( index , 1 ); // delete it from list

		setMacro( "_ROW_SEQ" , gRowSeqAry );
	}


	function applyRowSeq(){ // apply row sequence
		for ( var i = 0 ; i < gRowSeqAry.length ; i ++ ){
			var rowID = "#" + gRowSeqAry[ i ];
			$( rowID ).hide();
			$( rowID ).appendTo( ".panlSec" ).show(); // append the row behind last row
		}
	}

	function uiClickSortRow( mode ){ // sort row
		if ( mode == 1 ){ // ascending sort
			$( ".panlSortAscBtn" ).hide();
			$( ".panlSortDescBtn" ).fadeIn();
		}else if ( mode == 2 ){ // descending sort
			$( ".panlSortDescBtn" ).hide();
			$( ".panlSortAscBtn" ).fadeIn();
		}

		for ( var i = 0 ; i < $( ".macRow" ).length ; i ++ ){ // sort row by pips
			$( ".macRow" ).sort( function ( a , b ){
				var a_bid = Math.abs( parseFloat( $( a ).find( ".bidSec" ).find( ".ctrlPipsDiv" ).find( ".txtIpt" ).val() ) );
				var a_ask = Math.abs( parseFloat( $( a ).find( ".askSec" ).find( ".ctrlPipsDiv" ).find( ".txtIpt" ).val() ) );
				var a_spread = ( a_bid + a_ask ) / 2;

				var b_bid = Math.abs( parseFloat( $( b ).find( ".bidSec" ).find( ".ctrlPipsDiv" ).find( ".txtIpt" ).val() ) );
				var b_ask = Math.abs( parseFloat( $( b ).find( ".askSec" ).find( ".ctrlPipsDiv" ).find( ".txtIpt" ).val() ) );
				var b_spread = ( b_bid + b_ask ) / 2;

				checkSort( a , b , a_spread , b_spread );
			} );
		}

		for ( var i = 0 ; i < $( ".macRow" ).length ; i ++ ){ // sort row by symbol
			$( ".macRow" ).sort( function ( a , b ){
				var a_sym = $( a ).find( ".symDiv" ).find( ".current" ).html();
				a_sym = a_sym.replace( / /g , "" );

				var b_sym = $( b ).find( ".symDiv" ).find( ".current" ).html();
				b_sym = b_sym.replace( / /g , "" );

				checkSort( a , b , a_sym , b_sym );
			} );
		}

		for ( var i = 0 ; i < $( ".macRow" ).length ; i ++ ){ // sort row by ecn
			$( ".macRow" ).sort( function ( a , b ){
				var a_ecn = $( a ).find( ".ecnDiv" ).find( ".current" ).html();
				a_ecn = a_ecn.replace( / /g , "" );

				var b_ecn = $( b ).find( ".ecnDiv" ).find( ".current" ).html();
				b_ecn = b_ecn.replace( / /g , "" );

				checkSort( a , b , a_ecn , b_ecn );
			} );
		}

		function checkSort( a , b , aVal , bVal ){
			if ( mode == 1 ){
				if ( aVal <= bVal ){
					return;
				}
			}else if ( mode == 2 ){
				if ( aVal >= bVal ){
					return;
				}
			}
			$( a ).hide();
			$( a ).appendTo( ".panlSec" ).show();
		}
		saveRowSeq();
	}

	/////////////////// UI function ///////////////////

	function uiClickIfdSwitch(){ // click if done switch
		var ckedSw = $( this );
		var index = $( ckedSw ).index();
		var top = -122 + ( index * 27 );
		var ifdSwDiv = $( ckedSw ).closest( ".macRow" ).find( ".ifdSwDiv" );

		$( ifdSwDiv ).each( function (){ // change both bid side and ask side if done switch
			var ifdSw = $( this );
			$( ifdSw ).find( ".ifdSwLbl" ).removeClass( "ifdSwChked" , 300 );
			$( ifdSw ).find( ".ifdSwBg" ).css( "top" , top + "px" );
			$( ifdSw ).find( ".ifdSwLbl:eq( " + index + " )" ).addClass( "ifdSwChked" , 300 );
		} );
	}

	function uiInputTxtValue(){ // input number to input box
		var iptVal = this.value;
		var iptCtrl = $( this );
		var maxLimit = 50;

		if ( $( iptCtrl ).closest( ".ctrlDiv" ).hasClass( "ctrlPipsDiv" ) ){
			var iptClass = ".ctrlPipsDiv";
			var	iptValDef = gDefPipsMax;
			var iptValMax = gDefPipsMax * maxLimit;
			var	iptValMin = gDefPipsMin;
		}else if ( $( iptCtrl ).closest( ".ctrlDiv" ).hasClass( "ctrlQtyDiv" ) ){
			var iptClass = ".ctrlQtyDiv";
			var	iptValDef = gDefQtyMax;
			var iptValMax = gDefQtyMax * maxLimit;
			var	iptValMin = gDefQtyMin;
		}

		if ( ! $.isNumeric( iptVal ) ){ // input validation
			iptVal = iptValDef;
		}else{
			iptVal = Math.abs( iptVal );
		}

		if ( iptVal > ( iptValMax ) ){
			iptVal = iptValMax;
		}else if ( iptVal < iptValMin ){
			iptVal = iptValMin;
		}

		var iptGp = $( iptCtrl ).closest( ".macRow" ).find( iptClass );

		$( iptGp ).each( function (){ // change both bid side and ask side input value
			var iptDiv = $( this );
			var val = iptVal;
			$( iptDiv ).find( ".sldrBg" ).slider( "value" , val );

			if ( iptClass == ".ctrlPipsDiv" ){
				if ( $( iptDiv ).closest( ".sideSec" ).hasClass( "bidSec" ) ){
					val = -val;
				}else{
					val = "+" + val;
				}
			}

			$( iptDiv ).find( ".sldrHd" ).text( val );
			$( iptDiv ).find( ".txtIpt" ).val( val );
		} );
	}

	function uiClickCfgChkOffBox(){ // click config check box
		if ( $( this ).prop( "checked" ) ){
			$( this ).closest( ".confDiv" ).find( ".confPauseChk" ).addClass( "disableCtrl" , 200 );
			$( this ).closest( ".confDiv" ).find( ".confPauseChk" ).prop( "disabled" , true );

			if ( $( this ).closest( ".confDiv" ).hasClass( "confMaxSixtySecVolDiv" ) ){
				$( ".confNmlSixtySecVolDiv" ).find( "*" ).addClass( "disableCtrl" , 200 );
			}

		}else{
			$( this ).closest( ".confDiv" ).find( ".confPauseChk" ).removeClass( "disableCtrl" , 200 );
			$( this ).closest( ".confDiv" ).find( ".confPauseChk" ).prop( "disabled" , false );

			if ( $( this ).closest( ".confDiv" ).hasClass( "confMaxSixtySecVolDiv" ) ){
				$( ".confNmlSixtySecVolDiv" ).find( "*" ).removeClass( "disableCtrl" , 200 );
			}
		}
	}

	function uiClickCfgChkFlatAllBox(){ // click config check box
		if ( $( this ).prop( "checked" ) ){
			$( this ).closest( ".confDiv" ).find( ".confFlatChk" ).addClass( "disableCtrl" , 200 );
			$( this ).closest( ".confDiv" ).find( ".confFlatChk" ).prop( "disabled" , true );
		}else{
			$( this ).closest( ".confDiv" ).find( ".confFlatChk" ).removeClass( "disableCtrl" , 200 );
			$( this ).closest( ".confDiv" ).find( ".confFlatChk" ).prop( "disabled" , false );
		}
	}

	function uiClickCfgChkPauseBox(){ // click config check box
		if ( !$( this ).closest( ".confDiv" ).hasClass( "confMaxSixtySecVolDiv" ) ){
			return;
		}

		var chk = $( this ).prop( "checked" );
		$( ".confNmlSixtySecVolDiv" ).find( ".confOnChk" ).prop( "checked" , chk );
	}

	function uiChgRowEcn(){ // change row ecn
		var ecn = $( this ).val();
		var sym = $( this ).closest( ".rowInfSec" ).find( ".symDiv" ).find( ".current" ).html();
		sym = sym.replace( / /g , "" );

		var symSlt = $( this ).closest( ".rowInfSec" ).find( ".symSlt" );
		$( symSlt ).empty(); // delete old symbol list

		var symECN = eval( "gSymOpt_" + ecn );
		var symList = "";
		$.each( symECN , function ( value ) {
			symList += "<option value = " + value + "> "+ value + "</option>";
		} );
		$( symSlt ).append( symList ); // append new symbol list
		var hasThisSym = $( symSlt ).find( 'option[ value = "' + sym + '" ]' ).length; // check new symbol list include old symbol or not

		if ( hasThisSym == 1 ){
			$( symSlt ).val( sym ); // select old symbol
		}else if ( ecn == "MREX" || ecn == "PATS" || ecn == "PATX" ){
			$( symSlt ).val( gDefSym ); // select default symbol
		}else{
			$( symSlt ).eq( 0 ).prop( 'selected' , true ); // select the first symbol
		}

		$( symSlt ).foundationCustomForms(); // call foundationCustomForms to apply symbol list change
		$( this ).closest( ".macRow" ).find( ".symDiv" ).find( ".current" ).html( $( symSlt ).val() );

		showSymSpread(); // show spread for new symbol
	}

	function uiChgRowSym(){ // change row symbol
		showSymSpread(); // show spread for new symbol
	}

	function showSlider(){ // show macro pip control slider
		$( ".sldrBg" ).stop( true , true ); // clear previous jquery animation
		$( ".sldrBg" ).hide();
		var sldrVal = Math.abs( $( this ).val() );
		$( this ).closest( ".ctrlDiv" ).find( ".sldrBg" ).slider( "value" , sldrVal );
		$( this ).closest( ".ctrlDiv" ).find( ".sldrBg" ).fadeIn();
	}

	function hideSlider( evt ){ // hide macro pip control slider
		if ( $( evt.target ).closest( ".ctrlSec" ).length > 0 ){
			return;
		}
		$( ".sldrBg" ).stop( true , true );
		$( ".sldrBg" ).fadeOut();
	}

	function showMacOnWarn(){ // show warnning if the macro is on
		var macSw = $( this ).closest( ".sideSec" ).find( ".macSw" );

		if ( $( macSw ).find( ".swOnLbl" ).hasClass( "swMacOn" ) ){
			$( macSw ).find( ".swOffLbl" ).stop( true , true );
			$( macSw ).find( ".swOffLbl" ).addClass( "swOnNotice" , 300 );
			$( macSw ).find( ".swOffLbl" ).delay( 600 ).removeClass( "swOnNotice" , 300 );
		}
	}

	function showRowOnWarn(){ // show warnning if bid or ask side macro is on
		$( this ).closest( ".macRow" ).find( ".swOnLbl" ).each( function (){
			if ( !$( this ).hasClass( "swMacOn" ) ){
				return;
			}

			$( this ).siblings( ".swOffLbl" ).stop( true , true );
			$( this ).siblings( ".swOffLbl" ).addClass( "swOnNotice" , 300 );
			$( this ).siblings( ".swOffLbl" ).delay( 600 ).removeClass( "swOnNotice" , 300 );
		} );
	}

	function showOffAllWarn(){ // show warnning if one of the macro is on
		$( ".panlSwDiv" ).find( ".swOnLbl" ).addClass( "swOnNotice" , 300 );
		$( ".panlSwDiv" ).find( ".swOnLbl" ).delay( 600 ).removeClass( "swOnNotice" , 300 );
	}

	function readPlacedPx( cmrAry ){ // show macro placed order price
		// CMR 300 MSG ALGO Mac_01_BUY O LO2 MREX CME/CMX_GLD/AUG17 30270720033500022 BUY 2 1217.7
		if ( cmrAry.length < 12 ){
			return;
		}
		var ecn = cmrAry[ 7 ];
		var sym = cmrAry[ 8 ];
		var px = parseFloat( cmrAry[ 12 ] );
		var symDip = getSymItem( ecn , sym ).dip; // get symbol decimal places

		if ( ! $.isNumeric( px ) ){
			return;
		}

		px = px.toFixed( symDip );
		var numStr = cmrAry[ 4 ].split( "_" )[ 1 ];
		var side = cmrAry[ 4 ].split( "_" )[ 2 ];

		if ( side == "BUY" ){
			var bidOrAsk = ".bidSec";
		}else{
			var bidOrAsk = ".askSec";
		}

		var rowID = "#macRow_" + numStr;
		var oPx = $( rowID ).find( bidOrAsk ).find( ".infPxDiv" ).find( ".txtIpt" ).val();

		if ( px == oPx ){
			return;
		}
		$( rowID ).find( bidOrAsk ).find( ".infPxDiv" ).find( ".txtIpt" ).val( px );
	}

	function readTotalFill( cmrAry ){ // read total fill quantity message
		// e.g. CMR 300 TOTAL_FILL MREX CME/CMX_GLD/AUG17 15 46 SELL 1 1246.7 OPX 1246.4
		if ( cmrAry.length < 7 ){
			return;
		}
		saveLastFillSide( cmrAry );
		showFilledQty( cmrAry );
		noTradeGuardian();

		clearTimeout( gReadTotalFillTimer );
		gReadTotalFillTimer = setTimeout( function (){ // wait 1 second to avoid reading too many partil fill msg
			$.when( checkIsLeader() ).then( function (){
				if ( gIsLeader == 0 ){
					return;
				}
				sendTradeMsg( cmrAry );
				filledQtyGuardian( cmrAry );
				slowFedGuardian( cmrAry );
				triggerOffRowGuardian();
			} );
		} , 1000 );
	}

	function showFilledQty( cmrAry ){
		if ( !gStatusObj.on ){
			return;
		}

		var rEcn = cmrAry[ 3 ]; // new add
		var rSym = cmrAry[ 4 ];
		var rBuy = parseFloat( cmrAry[ 5 ] );
		var rSell = parseFloat( cmrAry[ 6 ] );
		var newSym = true;

		for ( var i = 0 ; i < gTtlFillAry.length ; i ++ ){
			var oEcn = gTtlFillAry[ i ][ 0 ];
			var oSym = gTtlFillAry[ i ][ 1 ];
			if ( rEcn != oEcn ){
				continue;
			}
			if ( rSym != oSym ){
				continue;
			}

			newSym = false;

			var oBuy = gTtlFillAry[ i ][ 2 ];
			var oSell = gTtlFillAry[ i ][ 3 ];
			var netBuy = rBuy - oBuy;
			var netSell = rSell - oSell;

			$( ".macRow" ).each( function (){
				var mEcn = $( this ).find( ".ecnDiv" ).find( ".current" ).html();
				mEcn = mEcn.replace( / /g , "" );

				var mSym = $( this ).find( ".symDiv" ).find( ".current" ).html();
				mSym = mSym.replace( / /g , "" );
				if ( mEcn != rEcn ){
					return;
				}

				if ( mSym != rSym ){
					return;
				}

				var oNetBuy = $( this ).find( ".bidSec" ).find( ".infQtyDiv" ).find( ".txtIpt" ).val();
				var oNetSell = $( this ).find( ".askSec" ).find( ".infQtyDiv" ).find( ".txtIpt" ).val();
				if ( netBuy != oNetBuy ){
					$( this ).find( ".bidSec" ).find( ".infQtyDiv" ).find( ".txtIpt" ).val( netBuy );
				}
				if ( netSell != oNetSell ){
					$( this ).find( ".askSec" ).find( ".infQtyDiv" ).find( ".txtIpt" ).val( netSell );
				}
			} );
			break;
		}

		if ( newSym ){
			gTtlFillAry[ gTtlFillAry.length ] = [ rEcn , rSym , rBuy , rSell ]; // store total fill info to an array
		}
	}

	function saveLastFillSide( cmrAry ){
		gLastFillSide = cmrAry[ 7 ];
	}

	function sendTradeMsg( cmrAry ){
		var ecn = cmrAry[ 3 ];
		var sym = cmrAry[ 4 ];
		var side = cmrAry[ 7 ];
		var qty = parseFloat( cmrAry[ 8 ] );
		var px = parseFloat( cmrAry[ 9 ] );
		var ordPx = cmrAry[ 11 ];

		if ( ordPx == "UNKNOWN" || ordPx == "NIL" ){
			return;
		}

		var rEcn = getEcnSym().ecn;
		var rSym = getEcnSym().sym;

		if ( ecn != rEcn || sym != rSym ){
			return;
		}

		var ts = parseFloat( getSymItem( ecn , sym ).ts ); // get tick size
		var dip = parseFloat( getSymItem( ecn , sym ).dip ); // get symbol decimal places

		if ( isNaN( qty ) || isNaN( px ) || isNaN( ts ) || isNaN( dip ) ){
			showLog( "sendTradeMsg" , "Error On Reading Trade Msg. qty : " + qty + " . trade px : " + px + " . tick size : " + ts + " . decimal places : " + dip );
			return;
		}

		px = px.toFixed( dip );

		var tradeMsg = side + " " + ecn + " " + sym + " at " + px + ". ";
		var symMsg = "";
		var symInfo = "";

		symInfo = getSymNetPos( ecn , sym );

		var netPos = symInfo.netPos; // calulate total net position for a product
		var avgPx = symInfo.avgPx;

		if ( $.isNumeric( netPos ) && $.isNumeric( avgPx ) ){
			symMsg = "Net Position: " + netPos + ". Average Price: " + avgPx + ". ";
		}

		var accMsg;
		accPnL = getAccPnL(); // get account Pnl
		if ( $.isNumeric( accPnL ) ){
			accMsg = "P&L: " + accPnL + ". ";
		}

		if ( gMAQuoteAry.length < gFiveSecondTick ){
			var sec5Vol = 0;
		}else{
			var sec5Vol = gMAQuoteAry[ 0 ].totalVol - gMAQuoteAry[ gFiveSecondTick - 1 ].totalVol;
		}

		var volMsg = "";
		var obj = gMAQuoteAry[ 0 ];

		if ( typeof obj !== "undefined" ){
			volMsg = "Total Volume: " + obj.totalVol + ". 15Minute Volume: " + obj.sec900Vol + ". 1Minute Volume: " + obj.sec60Vol + ". 5Second Volume: " + sec5Vol + ". MA Spread: " + obj.maSpreadPip + ". ";
		}

		var msg = tradeMsg + symMsg + accMsg + volMsg;
		sendTabMsg( msg );
	}

	/////////////////////////// Other function ///////////////

	function getSeqNum( type ){ // get sequence number
		if ( gActNo >= 100 ){
			gActNo = 0;
		}

		var actNo = gActNo;

		if ( gActNo < 10 ){
			actNo = "0" + gActNo
		}

		gSeqNum = type.toString() + gSessID.toString() + actNo.toString() + gVersion; // e.g. SEQ 7378391a
		gActNo ++;
	}


	function getSymItem( ecn , sym ){ // get symbol details ( tick size , decimal place )
		if ( ecn == "PATX" ){
			ecn == "MREX";
		}

		var symObj = { };
		symObj.e = ecn;
		symObj.s = sym;
		var symItem = gInitSymItem( symObj );
		return symItem;
	}

	function getSymNetPos( ecn , sym ){ // get symbol net position
		sym = sym.replace( /\//g , "_" );

		var netPos = gInitNetPos( gInitUserID , ecn , sym )[ 0 ];
		var avgPx = gInitNetPos( gInitUserID , ecn , sym )[ 1 ];

		var netPosNum = parseFloat ( netPos );
		var avgPxNum = Math.round( parseFloat ( avgPx ) * 100 ) / 100;

		if ( ! validateParameter( "netPos" , netPosNum , netPos ) ){
			netPosNum = 0;
		}

		if ( ! validateParameter( "avgPx" , avgPxNum , avgPx ) ){
			avgPxNum = 0;
		}

		symInfo = { netPos: netPosNum , avgPx: avgPxNum };
		return symInfo;
	}

	function getFirstRowNetPos(){
		var ecn = getEcnSym().ecn;
		var sym = getEcnSym().sym;
		var netPos = getSymNetPos( ecn , sym ).netPos;

		return netPos;
	}

	function getSymMarketPx( ecn , sym ){ // get symbol market price
		if ( ecn == "PATX" ){
			ecn == "MREX";
		}

		sym = sym.replace( /\//g , "_" );

		var mktPx = gInitMktPx( ecn , sym );
		var mktPxNum = parseFloat ( mktPx );

		if ( ! validateParameter( "mktPx" , mktPxNum , mktPx ) ){
			mktPxNum = 0;
		}

		return mktPxNum;
	}

	function getSymBidAskPx( ecn , sym ){
		if ( ecn == "PATX" ){
			ecn == "MREX";
		}

		var bidPx = getPx( ecn , sym ).bidPx;
		var askPx = getPx( ecn , sym ).askPx;

		if( ecn == "PATS" && ( bidPx == 0 || askPx == 0 ) ){
			ecn = "MREX";
			var bidPx = getPx( ecn , sym ).bidPx;
			var askPx = getPx( ecn , sym ).askPx;
		}

		var pxAry = [ bidPx , askPx ];
		return pxAry;

		function getPx( ecn , sym ){
			var sym = sym.replace( /\//g , "_" );
			var str = ecn + "-" + sym;

			var bidPx = gInitBidAry[ str ];
			var askPx = gInitAskAry[ str ];
			if ( typeof( bidPx ) == "undefined" ){
				bidPx = 0;
			}
			if ( typeof( askPx ) == "undefined" ){
				askPx = 0;
			}
			var obj = { bidPx: parseFloat( bidPx ) , askPx: parseFloat( askPx ) };

			return obj;
		}
	}

	function getCutLossPx( ecn , sym , netPos , qty , bidPx , askPx ){
		var px = 0;
		var pnl = getAccPnL(); // get current PnL
		var maxLoss = gDefBufferMaxLoss;
		var buffer = pnl + maxLoss;

		var ts = getSymItem( ecn , sym ).ts; // get tick size
		var tc = getSymItem( ecn , sym ).tc; // get tick cent

		var pnlPerTick = ts * tc * Math.abs( netPos );

		var bufferTick = Math.abs( buffer / pnlPerTick );

		if ( bufferTick > gDefAutoFlatSlip ){
			bufferTick = gDefAutoFlatSlip;
		}

		var bufferPx = Math.round( bufferTick ) * ts;

		if ( qty > 0 ){
			px = askPx + bufferPx;
		}else{
			px = bidPx - bufferPx;
		}

		return px;
	}

	function getAccPnL(){ // get account PnL
		var pnl = gInitAccPnl[ gInitUserID ];

		if ( typeof pnl == "undefined" ){
			pnl = 0;
		}

		var pnlNum = Math.round( parseFloat( pnl ) * 10 ) / 10; // round number

		if ( ! validateParameter( "pnl" , pnlNum , pnl ) ){
			pnlNum = 0;
		}

		if ( pnlNum < -100000 || pnlNum > 100000 ){ // prevent reading wrong pnl number from GFA
			var msg = "Error on reading account PnL. PnL : " + pnl;
			sendLogMsg( msg );

			pnlNum = 0;
		}

		return pnlNum;
	}

	function getOpenOrderNum(){ // get open order number
		var openOrder = gInitOpenOrdCount();
		var orderNum = parseFloat( openOrder );

		if ( ! validateParameter( "orderNum" , orderNum , openOrder ) ){
			orderNum = 0;
		}

		return orderNum;
	}

	function getOnAlgoNum(){ // get on algo number
		var onAlgo = 0;
		$( ".macSw" ).each( function (){
			if ( $( this ).find( ".swOnLbl" ).hasClass( "swChked" ) && $( this ).find( ".swOnLbl" ).hasClass( "swMacOn" ) ){
				onAlgo ++;
			}
		} );
		return onAlgo;
	}

	function getRunningAlgoNum(){ // get total runnning algo number
		var num = gMacOnAry.length;
		return num;
	}

	function getSymAry(){
		var symAry = [];

		$( ".macRow" ).each( function (){
			var macRow = $( this );

			var rEcn = $( macRow ).find( ".ecnDiv" ).find( ".current" ).html();
			rEcn = rEcn.replace( / /g , "" );

			var rSym = $( macRow ).find( ".symDiv" ).find( ".current" ).html();
			rSym = rSym.replace( / /g , "" );

			var newSym = 1;

			for ( var i = 0 ; i < symAry.length ; i ++ ){
				var ecn = symAry[ i ][ 0 ];
				var sym = symAry[ i ][ 1 ];

				if ( rEcn == ecn && rSym == sym ){
					newSym = 0;
					break;
				}
			}

			if ( newSym == 0 ){
				return;
			}
			symAry[ symAry.length ] = [ rEcn , rSym ];
		} );

		return symAry;
	}

	function getLocalStore(){ // load local conifg.
		writeToAry( "ShowConsole" );
		writeToAry( "PlayAlert" );

		function writeToAry( name ){
			var on = store.get( "j" + name );
			if ( on != null ){
				editParmAry( gLocalCfgAry , name , "on" , on );
			}
		}
	}

	function getRandomNumber(){ // 1 - 100
		var num = Math.floor( ( Math.random() * 100 ) + 1 );
		return num;
	}

	function uiClickReloadAll(){ // request every use to reload the page
		sendMsg( gInitUserID , "MMNOTICE" , "RELOAD_PANEL" );

		// for ( i = 1 ; i < 10 ; i++ ){
			// var user = "30" + i.toString();
			// sendMsg( user , "MMNOTICE" , "RELOAD_PANEL" );
		// }
	}

	function uiClickReloadEveryOne(){ // request every use to reload the page
		var i = 1;
		loop();

		function loop(){
			var sendMsgLoop = setTimeout( function () {
				if ( i < 10 ){
					var userStr = "30" + i;
				}else if ( i >= 10 && i < 26 ){
					var userStr = "3" + i;
				}else{
					sendMsg( gInitUserID , "MMNOTICE" , "RELOAD_PANEL" );
					return;
				}
				if ( parseInt( userStr ) != gInitUserID ){
					sendMsg( userStr , "MMNOTICE" , "RELOAD_PANEL" );
				}
				i++;
				loop();
			} , 1000 );
		}
	}

	function reloadAll(){ // reload page count down
		clearInterval( gReloadInterval );
		clearTimeout( gReloadTimer );

		$( ".panlReldCvr" ).show(); // show a cover to cover all button ( not include STOPALL )
		$( ".panlReldNtcSec" ).show();

		var randomMSec = Math.random() * 10000;
		var randomSec = Math.round( randomMSec / 1000 );
		var reloadMSec = 5000;
		var reloadSec = reloadMSec / 1000;
		var totalSec = reloadSec + randomSec; // count down 5 - 15 seconds
		var totalMSec = randomMSec + reloadMSec; // add a random second to avoid everyone reload at the same time

		$( ".panlReldTimeSn" ).html( totalSec );

		showReload();

		gReloadInterval = setInterval( showReload , 1000 );
		gReloadTimer = setTimeout( reload , totalMSec );

		function showReload(){
			totalSec -= 1;
			$( ".panlReldTimeSn" ).html( totalSec );

			if ( totalSec <= 0 ){
				clearInterval( gReloadInterval );
			}
		}

		function reload(){
			gStatusObj.reloaded = true; // mark this user is reloaded. unable to send and receive any CMD and CMR

			clearInterval( gPanelShortInterval ); // stop panel timer
			gPanelShortInterval = false;
			clearInterval( gPanelLongInterval );
			gPanelLongInterval = false;

			showReloadNotice(); // show reload notice
		}
	}

	function showReloadNotice(){
		location.reload();
		$( ".panlReldNtcSn" ).hide();
		$( ".panlReldLdDiv" ).hide();
		$( ".panlReldWarnSn" ).fadeIn();
	}

	function flashMacroLight( rowID , side ){ // show a notice if the macro is triggered
		var infDiv = $( rowID ).find( side ).find( ".macInfDiv" );
		$( infDiv ).find( ".trigLgtDiv" ).addClass( "trigLgtOnDiv" ).removeClass( "trigLgtOnDiv" , 3000 );
	}

	function checkIptVal( ctrlDiv , maxVal , MinVal ){ // input validation
		var iptElem = $( ctrlDiv ).find( ".txtIpt" );
		var iptVal = parseFloat( $( iptElem ).val() );

		if ( iptVal <= maxVal && iptVal >= MinVal ){
			return iptVal;

		}else if ( iptVal > maxVal ){
			$( iptElem ).attr( "value" , maxVal );
		}else if ( iptVal < MinVal ){
			$( iptElem ).attr( "value" , MinVal );
		}

		$( iptElem ).addClass( "confWarnIpt" , 300 );
		$( iptElem ).delay( 600 ).removeClass( "confWarnIpt" , 5000 );
		$( iptElem ).focus();
		return false;
	}

	function clearGlobalVal(){ // clear global variable
		gLastMmoNum = 0;
		gTtlFillAry = [];
		gMacOnAry = [];
		gSessIDAry = [];
		gSymSprdAry = [];
		gModeAry = [];
	}

	function showLog( name , content ){ // show con sole log
		var on = readParmAry( gLocalCfgAry , "ShowConsole" ).on;

		if ( !on ){
			return;
		}

		console.log( name + " : " + content );
	}

	function showDebugLog( name , content ){ // show con sole log
		var on = readParmAry( gLocalCfgAry , "PlayAlert" ).on;

		if ( !on ){
			return;
		}

		console.log( " DEBUG : " + name + " : " + content );
	}

	function ctrlKeyDown( e ){ // ctrl key down
		if ( e.keyCode != 17 ){
			return;
		}

		var $keyDelBtn = $( ".panlAddDiv" );
		if ( $keyDelBtn.is( ":hover" ) ){
			$( ".panlAddDiv" ).hide();
			$( ".panlDelAllDiv" ).show();
		}

		var $keyReldBtn = $( ".confPanlCfmDiv" );
		if ( $keyReldBtn.is( ":hover" ) ){
			$( ".confPanlCfmDiv" ).hide();
			$( ".confReldDiv" ).show();
		}

		var $modeDeleteBtn = $( ".confModeDeleteDiv" );
		if ( $modeDeleteBtn.is( ":hover" ) ){
			$( ".confModeDeleteDiv" ).hide();
			$( ".confModeDeleteAllDiv" ).show();
		}
	}

	function ctrlKeyUp( e ){ // ctrl key up
		if ( e.keyCode != 17 ){
			return;
		}
		$( ".panlDelAllDiv" ).hide();
		$( ".panlAddDiv" ).show();
		$( ".confReldDiv" ).hide();
		$( ".confPanlCfmDiv" ).show();
		$( ".confModeDeleteAllDiv" ).hide();
		$( ".confModeDeleteDiv" ).show();
	}

	function shiftKeyDown( e ){ // ctrl key down
		if ( e.keyCode != 16 ){
			return;
		}

		var $keyReldBtn = $( ".confPanlCfmDiv" );
		if ( $keyReldBtn.is( ":hover" ) ){
			$( ".confPanlCfmDiv" ).hide();
			$( ".confReldAllDiv" ).show();
		}
	}

	function shiftKeyUp( e ){ // ctrl key up
		if ( e.keyCode != 16 ){
			return;
		}
		$( ".confReldAllDiv" ).hide();
		$( ".confPanlCfmDiv" ).show();
	}

	function pushCMD( cmd ){
		if ( gStatusObj.disconnected || gStatusObj.reloaded ){
			return;
		}

		gInitOrdSubscription.push( cmd );
		waitCMR();
	}

	function sendMsg( target , topic , msg ){ // send message
		getSeqNum( gByFunction );
		//var cmd = "CMD " + gInitUserID + " MSG " + target + " " + topic + " { " + msg + " }" + " SEQ " + gSeqNum;
		var cmd = "CMD " + gInitUserID + " MSG " + target + " " + topic + " { " + msg + " }";

		pushCMD( cmd );
		showLog( "sendMsg" , cmd );
	}

	function sendDataMsg( msg ){
		var cmd = "CMD " + gInitUserID + " MSG " + gInitUserID + " LOG { " + msg + " }";
		pushCMD( cmd );
		showLog( "sendDataMsg" , cmd );
	}

	function sendTabMsg( msg ){
		sendMsg( gInitUserID , "MMALGO" , msg );

		var topic = gInitUserID + ":";
		sendTelegramMsg( topic , msg );
	}

	function sendAlertMsg( msg ){
		sendMsg( gAlertTarget , "ALGO_ALERT" , msg );
	}

	function sendLogMsg( msg ){ // send log message to server for debug
		getSeqNum( gByFunction );
		var topic = "LOGMSG";
		var cmd = "CMD " + gInitUserID + " MSG " + gInitUserID + " " + topic + " { " + msg + " . Seq No. : " + gSeqNum + " . IP : " + gIP + " }";

		pushCMD( cmd );
		showLog( "sendLogMsg" , cmd );
	}

	function readMsg( cmrAry ){ // read MSG cmr message
		// e.g. CMR 302 MSG MMALGO Stopped All Algo.
		if ( cmrAry.length < 6 ){
			return;
		}

		var cmrMsg = "";

		for ( var i = 4 ; i < cmrAry.length -1 ; i ++ ){
			if ( i > 4 ){
				cmrMsg += " ";
			}
			cmrMsg += cmrAry[ i ];
		}

		if ( cmrMsg == "Server Started." ){
			if ( gStatusObj.reloaded ){
				return
			}
			gStatusObj.reloaded = true;

			$.when( checkIsLeader() ).then( function (){
				if ( gIsLeader == 0 ){
					return;
				}

				var topic = gInitUserID + ":";
				sendTelegramMsg( topic , cmrMsg );

				turnOffPanel();
				reloadAll(); // reload page if server is restarted.
			} );

		}else if ( cmrMsg.slice( 0 , 20 ) == "Updated Algo Setting" ){
			$( ".confPanlSec" ).fadeOut(); // hide panel config box if someone changed parameter

		}else if ( cmrMsg.slice( 0 , 24 ) == "Updated Guardian Setting" ){
			$( ".confGuarSec" ).fadeOut();
		}
	}

	function readNotice( cmrAry ){ // read notice msg
		// e.g. CMR 304 MSG MMNOTICE NOTICE2 Volume Update: CME/CRUDE/JAN18 994
		if ( cmrAry.length < 4 ){
			return;
		}

		notice = cmrAry[ 4 ];
		topic = notice.split( "_" )[ 0 ];
		type = notice.split( "_" )[ 1 ];

		var msg = "";
		for ( var i = 5 ; i < cmrAry.length ; i ++ ){
			msg += cmrAry[ i ] + " ";
		}

		if ( topic == "GUARD" ){ // guardian msg
			if ( type == "NmlSixtySecVol" ){
				editParmAry( gConfGuardAry , "MaxSixtySecVol" , "triggered" , false );
			}
			editParmAry( gConfGuardAry , type , "triggered" , true );
			showGuardianNotice( type );

		}else if ( topic == "ALGOFUSE" ){ // hit algo fuse
			showAlgoFuseNotice( msg );

		}else if ( topic == "CHGMODE" ){ // change mode
			if ( type == "FAIL" ){
				showChgModeFailNotice( msg );
			}else{
				showChgModeNotice( msg );
			}

		}else if ( topic == "NOTICE1" ){ // roger's notice
			//showRNotice( msg );
			twiceFillGuardian( msg );

		}else if ( topic == "NOTICE2" ){ // roger's notice
			sixtySecVolGuardian( msg );
			resumeVolumeGuardian( msg );
		}else if ( notice == "TOUCH_PX" ){
			readIfTouchMsg( msg );
		}else if ( notice == "TRIGGERED_RESET" ){
			resetAllGuardianTriggered( msg );
		}else if ( notice == "RELOAD_PANEL" ){
			reloadAll();
		}
	}

	function sendTelegramMsg( topic , msg ){ // show message to telegram
		var on = readParmAry( gConfPanelAry , "SendTelegram" ).on;
		if ( !on ){
			return;
		}

		var msgStr = msg.split( " " )[ 0 ];

		for ( var i = 0 ; i < gTelegramTargetAry.length ; i ++ ){
			var obj = gTelegramTargetAry[ i ];
			if ( obj.userID.indexOf( gInitUserID ) == -1 ){
				continue;
			}

			for ( var j = 0 ; j < obj.target.length ; j ++ ){
				if ( obj.target[ j ] == "kellog" && ( msgStr != "BUY" && msgStr != "SELL" ) ){
					continue;
				}
				sendMsg( obj.target[ j ] , topic , msg );
			}
			break;
		}
	}

	function showBgAlgoCount(){ // show algo count on tab badge
		clearTimeout( gBgCountTimer ); // clear previous timer

		gBgCountTimer = setTimeout( function (){
			var onAlgoNum = getOnAlgoNum(); // get on algo number
			var badgeSp = $( gPanelID ).find( ".badgeSp" );
			var oOnAlgoNum = parseInt( $( badgeSp ).text() ); // get previous on algo number
			if ( onAlgoNum != oOnAlgoNum ){ // update the badge if the number is changed
				$( badgeSp ).text( onAlgoNum );
				$( badgeSp ).show();
			}
		} , 800 );
	}

	function hideBgAlgoCount(){ // hide algo count on tab badge
		clearTimeout( gBgCountTimer );

		gBgCountTimer = setTimeout( function (){
			var badgeSp = $( gPanelID ).find( ".badgeSp" );
			$( badgeSp ).text( "" );
			$( badgeSp ).hide();
		} , 800 );
	}

	function readAlgoRunning( cmrAry ){ // read ALGO-RUNNING message
		// e.g. CMR 302 ALGO-RUNNING 0
		gRunning = cmrAry[ 3 ];

		if ( gRunning == 0 ){
			hideOnAlgoGuardian(); // stop algo guardian if all algo are stopped
			$( gPanelID ).find( ".badgeSp" ).removeClass( "badgeOnSp" );
		}else if ( gRunning == 1 ){
			$( gPanelID ).find( ".badgeSp" ).addClass( "badgeOnSp" );
		}
	}

	function validateParameter( parmName , parmNum , parm ){ // validate algo parameter
		if ( typeof parm == "undefined" || parmNum.toString() == "NaN" ){
			var msg = "parameter name: " + parmName + " is NaN or undefined. Parameter : " + parm;
			showDebugLog( "validateParameter" , msg );
			return false;
		}
		return true;
	}

	function showParmErrorNotice( type ){ // show algo parameter error ( NaN or undefined )
		var receivedType = 1;
		var sendType = 2;

		if ( type == receivedType ){
			$( ".confErrorSec" ).find( ".confMsgSp" ).html( "Received Invalid Algo Parameter From Server.<br>Please Review Algo Setting." );
		}else if ( type == sendType ){
			$( ".confErrorSec" ).find( ".confMsgSp" ).html( "Invalid Algo Parameter.<br>Please Review Algo Setting." );
		}

		$( ".confErrorSec" ).fadeIn();
	}

	function uiClickCloseErrorDiv(){ // close error notice box
		$( ".confErrorSec" ).fadeOut();
	}

	function showRNotice( msg ){ // notice msg from roger
		// e.g. BUY Twice in 1 minute , Same Algo FullyFilled > 1
		playSound( "order" );
		showToastr( "warning" , "AlgoSignal" , msg );
	}

	function showAlgoFuseNotice( msg ){
		playSound( "order" );
		showToastr( "warning" , "AlgoFuse" , msg );
	}

	function getCurrentTime(){ // get time. format 2017_10_19_12_53_32 = 19/11/2017 12:53:32a.m.
		var date = new Date();
		var year = date.getFullYear().toString();
		var month = date.getMonth().toString();
		var day = date.getDate().toString();
		var hour = date.getHours().toString();
		var minute = date.getMinutes().toString();
		var second = date.getSeconds().toString();

		var time = year + "_" + month + "_" + day + "_" + hour + "_" + minute + "_" + second;
		return time;
	}

	function saveMaxProfit(){ // save daily maximum profit to server
		var cfg = readParmAry( gConfGuardAry , "LossFromTop" );

		if ( !cfg.pause && !cfg.off && !cfg.flat && !cfg.flatAll && !cfg.notice ){
			return;
		}

		var pnl = getAccPnL();

		if ( pnl <= gMaxProfit ){ // check if got new day high
			return;
		}

		gMaxProfit = pnl;
		updatePnLStopLable();

		$.when( checkIsLeader() ).then( function (){
			if ( gIsLeader == 0 ){ // check if the user is leader or not
				return;
			}

			var time = getCurrentTime();
			var ary = [ time , gMaxProfit ];

			setMacro( "_MAX_PROFIT" , ary );
		} );
	}

	function showLossFromTop( show ){ // show loss from top information on algo tab
		var type = "LossFromTop";
		var cfg = readParmAry( gConfGuardAry , type );

		if ( cfg.off || cfg.pause || cfg.flat || cfg.flatAll || cfg.notice ){
			updatePnLStopLable();
			$( ".panlMaxProfitDiv" ).show();
		}else{
			$( ".panlMaxProfitDiv" ).hide();
		}
	}

	function renewMaxProfit(){
		var time = getCurrentTime();
		var pnl = getAccPnL();
		var ary = [ time , pnl ];

		setMacro( "_MAX_PROFIT" , ary );
	}

	function readMaxProfit( cmrAry ){ // read maximum profit msg
		// e.g. CMR 302 MACRO _MAX_PROFIT NAME { 2017_10_19_12_53 2345 }
		var rHour = 5; // restart hour 5a.m.

		var mDateStr = cmrAry[ 6 ];
		var mYear = parseInt( mDateStr.split( "_" )[ 0 ] );
		var mMonth = parseInt( mDateStr.split( "_" )[ 1 ] );
		var mDay = parseInt( mDateStr.split( "_" )[ 2 ] );
		var mHour = parseInt( mDateStr.split( "_" )[ 3 ] );
		var mMinute = parseInt( mDateStr.split( "_" )[ 4 ] );
		var mSecond = parseInt( mDateStr.split( "_" )[ 5 ] );

		var mDate = new Date( mYear , mMonth , mDay , mHour , 0 , 0 ); // macro day

		var cDate = new Date(); // get current date

		var cDateStr = getCurrentTime();
		var cYear = parseInt( cDateStr.split( "_" )[ 0 ] );
		var cMonth = parseInt( cDateStr.split( "_" )[ 1 ] );
		var cDay = parseInt( cDateStr.split( "_" )[ 2 ] );
		var cHour = parseInt( cDateStr.split( "_" )[ 3 ] );

		var rDate = new Date( cYear , cMonth , cDay , rHour , 0 , 0 );
		if ( cHour < rHour ){
			rDate.setDate( rDate.getDate() - 1 ); // server restart time;
		}

		if ( mDate < rDate ){
			return; // macro date too old
		}

		if ( mDate > cDate ){
			return; // macro date too new
		}

		var maxProfit = parseFloat( cmrAry[ 7 ] );
		gMaxProfit = maxProfit;

		updatePnLStopLable();
	}

	function GFA_disconnected(){
		reloadAll(); // reload the page if GFA is disconnected.

		gStatusObj.disconnected = true; // disable panel react if gfa is disconnected
	}

	function showToastr( type , title , msg ){ // show GFA notice box
		var toastNum = $( ".toast" ).length;

		if ( toastNum >= 5 ){ // prevent showing too many toast box
			return;
		}

		if ( type == "success" ){
			var timeOut = 5000;
		}else if ( type == "info" ){
			var timeOut = 5000;
		}else if ( type == "warning" ){
			var timeOut = 600000;
		}else if ( type == "error" ){
			var timeOut = 600000;
		}

		var options = {
			"closeButton": true ,
			"debug": false ,
			"newestOnTop": false ,
			"progressBar": true ,
			"positionClass": "toast-bottom-left" ,
			"preventDuplicates": false ,
			"onclick": null ,
			"showDuration": "300" ,
			"hideDuration": "300" ,
			"timeOut": timeOut ,
			"extendedTimeOut": "1000" ,
			"showEasing": "swing" ,
			"hideEasing": "linear" ,
			"showMethod": "fadeIn" ,
			"hideMethod": "fadeOut"
		}

		gInitNotice( type , msg , title , options );
	}

	function sendStatus(){ // if-done example
		setTimeout( function (){
			getSeqNum( gByFunction );

			var cmd = "CMD " + gInitUserID + " IF { EQ _aStatus_on 1 } { MSG " + gInitUserID + " MMALGO { ALGO is ON! } } { MSG " + gInitUserID + " MMALGO { ALGO is OFF! } }" + " SEQ " + gSeqNum;

			showLog( "Testing If Done" , cmd );
			pushCMD( cmd );
		} , 1000 );
	}

	function countDecimals( num ){ // count decimal places. e.g. 3.12 > 2
		if ( Math.floor( num ) === num ){
			return 0;
		}
		return num.toString().split( "." )[ 1 ].length || 0;
	}

	function getClientIP(){ // get user ip address
		var defer = $.Deferred();
		var retry = 0;

		gGetIPInterval = setInterval( getIP , 500 );
		return defer;

		function getIP(){
			if ( ( gIP != "" && typeof gIP != "undefined" ) || retry > 10 ){
				clearInterval( gGetIPInterval );
				defer.resolve( null );
				return defer;
			}

			window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
			var pc = new RTCPeerConnection( { iceServers:[] } ) , noop = function (){ };
			pc.createDataChannel( "" );
			pc.createOffer( pc.setLocalDescription.bind( pc ) , noop );

			pc.onicecandidate = function ( ice ){
				if ( !ice || !ice.candidate || !ice.candidate.candidate ) {
					return;
				}
				var regex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/;

				gIP = regex.exec( ice.candidate.candidate )[ 1 ];
				$( "#userInfoIP" ).text( gIP );

				pc.onicecandidate = noop;
			};

			retry ++;
		}
	}

	function updatePnLStopLable(){
		var type = "LossFromTop";
		var cfg = readParmAry( gConfGuardAry , type );

		var pnlStop = gMaxProfit + cfg.parm;
		pnlStop = pnlStop.toFixed( 2 ).replace( /(\d)(?=(\d{3})+\.)/g , '$1,' );

		$( ".panlMaxProfitSn" ).text( pnlStop );
	}

	function readParmAry( array , name ) {
		for ( var i = 0 ; i < array.length ; i++ ) {
			var obj = array[ i ];
			if ( obj[ "name" ] == name ) {
				return obj;
			}
		}
	}

	function editParmAry( array , name , property , value ) {
		for ( var i = 0 ; i < array.length ; i++ ) {
			if ( array[ i ][ "name" ] == name ) {
				array[ i ][ property ] = value;
				return;
			}
		}
	}

	function sendToastrMsg( type , topic , msg ){
		// available type : error , warning , success , info
		sendMsg( gInitUserID , "TOASTR_MSG" , type + "_" + topic + " " + msg ); // send triggered guardian notice
	}

	function sendSoundMsg( sound ){
		// available sound: alert , order , notice
		sendMsg( gInitUserID , "SOUND_MSG" , sound ); // send triggered guardian notice
	}

	function readToastrMsg( cmrAry ){ // read notice msg
		// e.g. CMR 304 MSG TOASTR_MSG error_AlgoGuardian Stop All.
		if ( cmrAry.length < 6 ){
			return;
		}

		cmr = cmrAry[ 4 ];
		type = cmr.split( "_" )[ 0 ];
		topic = cmr.split( "_" )[ 1 ];

		var msg = "";
		for ( var i = 5 ; i < cmrAry.length ; i ++ ){
			var str = cmrAry[ i ];
			if ( str == gIconObj.doubleUp ){
				str = gFAIconObj.doubleUp;
			}else if ( str == gIconObj.up ){
				str = gFAIconObj.up;
			}else if ( str == gIconObj.down ){
				str = gFAIconObj.down;
			}else if ( str == gIconObj.doubleDown ){
				str = gFAIconObj.doubleDown;
			}else if ( str == gIconObj.minus ){
				str = gFAIconObj.minus;
			}else if ( str == gIconObj.doubleExcMark ){
				str = gFAIconObj.doubleExcMark;
			}else if ( str == gIconObj.warnning ){
				str = gFAIconObj.warnning;
			}
			msg += str + " ";
		}

		showToastr( type , topic , msg );
	}

	function readSoundMsg( cmrAry ){
		// e.g. CMR 304 MSG SOUND_MSG alert
		if ( cmrAry.length < 5 ){
			return;
		}

		var sound = cmrAry[ 4 ];
		playSound( sound );
	}

	function playSound( sound ){
		var on = readParmAry( gLocalCfgAry , "PlayAlert" ).on;

		if ( !on ){
			return;
		}

		soundManager.play( sound );
	}

	function showPauseNotice(){
		$( ".panlPauseNtcSec" ).show();
	}

	function hidePauseNotice(){
		$( ".panlPauseNtcSec" ).hide();
	}

	function readPriceQuote( m ){
		var ecn = m.e;
		var mSym = m.s;

		if ( mSym.indexOf( "MA_60_" ) >= 0 ){
			var sym = mSym.split( "MA_60_" )[ 1 ];
			var maMsg = true;
		}else{
			var sym = mSym;
			var maMsg = false;
		}

		var rEcn = getEcnSym().ecn;
		var rSym = getEcnSym().sym;

		if ( sym != rSym ){
			return;
		}

		if ( ( rEcn == "PATS" || rEcn == "PATX" ) && ecn == "MREX" ){
			ecn = rEcn;
		}

		if ( ecn != rEcn ){
			return;
		}

		if ( !maMsg ){
			storeQuoteDate( m );
			marketDepthGuardian();

		}else{
			storeMAQuoteDate( m );
			wideMASpreadGuardian();
			fiveSecVolGuardian();
			pushFiveSecondVol();
			countTickVolume();
		}

		clearQuoteDate();
	}

	function marketDepthGuardian(){
		var type = "MinMarketDepth";
		var cfg = readParmAry( gConfGuardAry , type );
		var obj = gQuoteAry[ 0 ];

		if ( !cfg.off && !cfg.pause && !cfg.flat && !cfg.flatAll && !cfg.notice ){
			return;
		}

		if ( typeof obj == "undefined" ) {
			return;
		}

		if ( cfg.triggered ){
			return;
		}

		if ( obj.totalBidLiq == obj.bidLiq ){
			return;
		}

		if ( obj.totalAskLiq == obj.askLiq ){
			return;
		}

		if ( obj.totalBidLiq > cfg.parm && obj.totalAskLiq > cfg.parm ){
			gGuaMarketDepthCount = 0;
			editParmAry( gConfGuardAry , type , "triggered" , false );
			return;
		}

		if ( gGuaMarketDepthCount < 5 ) {
			gGuaMarketDepthCount ++;
			return;
		}

		var reason = "Thin Market Depth on " + obj.ecn + " " + obj.sym + ". Liquidity Limit: " + cfg.parm + ". Bid Liquidity: " + obj.totalBidLiq + ". Ask Liquidity: " + obj.totalAskLiq + ". ";

		triggerGuardian( type , reason );
	}

	function storeMAQuoteDate( m ){
		var ecn = m.e;
		var maSym = m.s;

		var sym = maSym.split( "MA_60_" )[ 1 ];
		var maBid = checkValue( m.d[ 0 ].bid );
		var maAsk = checkValue( m.d[ 0 ].ask );
		var bid = checkValue( m.d[ 1 ].bid );
		var ask = checkValue( m.d[ 1 ].ask );
		var totalVol = checkValue( m.d[ 2 ].bid );
		var sec900Vol = checkValue( m.d[ 2 ].bidVol );
		var sec60Vol = checkValue( m.d[ 2 ].ask );

		if ( bid == 0 || ask == 0 || maBid == 0 || maAsk == 0 ){
			return;
		}

		var ts = getSymItem( ecn , sym ).ts;
		var dip = getSymItem( ecn , sym ).dip;
		var maMid = ( ( maBid + maAsk ) / 2 ).toFixed( dip + 1 );
		var maSpreadPip = ( ( maAsk - maBid ) / ts ).toFixed( dip );

		var obj = { ecn: ecn ,
					sym: sym ,
					maBid: maBid ,
					maAsk: maAsk ,
					bid: bid ,
					ask: ask ,
					totalVol: totalVol ,
					sec900Vol: sec900Vol ,
					sec60Vol: sec60Vol ,
					ts: ts ,
					dip: dip ,
					maMid: maMid ,
					maSpreadPip: maSpreadPip ,
					bidVol: 0 ,
					askVol: 0 ,
					rtsBidVol: 0 ,
					rtsAskVol: 0 ,
					rlBidVol: 0 ,
					rlAskVol: 0 ,
					};

		gMAQuoteAry.unshift( obj ); // add 1 row on top

		if ( gMAQuoteAry.length > 100 ){
			gMAQuoteAry.pop(); // remove last row
		}

		function checkValue( num ){
			num = parseFloat( num );
			if ( isNaN( num ) ){
				return 0;
			}
			return num;
		}
	}

	function storeQuoteDate( m ){
		for ( i = 0 ; i < m.d.length ; i++ ){
			if ( !checkValue( m.d[ i ].bid ) ||
			!checkValue( m.d[ i ].ask ) ||
			!checkValue( m.d[ i ].bidVol ) ||
			!checkValue( m.d[ i ].askVol ) ){
				gotWrongQuoteData();
				return;
			}
		}

		var ecn = m.e;
		var sym = m.s;

		var bid = checkValue( m.d[ 0 ].bid );
		var ask = checkValue( m.d[ 0 ].ask );

		var bidLiq = checkValue( m.d[ 0 ].bidVol );
		var askLiq = checkValue( m.d[ 0 ].askVol );

		var totalBidLiq = 0;
		var totalAskLiq = 0;

		for ( i = 0 ; i < m.d.length ; i++ ){
			totalBidLiq += checkValue( m.d[ i ].bidVol );
			totalAskLiq += checkValue( m.d[ i ].askVol );
		}

		var obj = { ecn: ecn ,
					sym: sym ,
					bid: bid ,
					ask: ask ,
					bidLiq: bidLiq ,
					askLiq: askLiq ,
					totalBidLiq: totalBidLiq ,
					totalAskLiq: totalAskLiq ,
					};

		gQuoteAry.unshift( obj ); // add 1 row on top

		if ( gQuoteAry.length > 100 ){
			gQuoteAry.pop(); // remove last row
		}

		function checkValue( num ){
			num = parseFloat( num );

			if ( isNaN( num ) ){
				return false;
			}
			return num;
		}
	}

	function clearQuoteDate(){
		clearTimeout( gClearQuoteTimer );

		gClearQuoteTimer = setTimeout( function (){
			gQuoteAry = [];
			gMAQuoteAry = [];
		} , 2000 );
	}

	function readIfTouchMsg( msg ){
		//e.g. MREX CME/CMX_GLD/DEC17 Near Resistance PxHigh-1 1315
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){ // check if the panel is reloaded/disconnected or not
			return;
		}

		var msgAry = msg.split( " " );

		if ( msgAry.length < 5 ){
			return;
		}

		var ecn = msgAry[ 0 ];
		var sym = msgAry[ 1 ];
		var action = msgAry[ 2 ];
		var side = msgAry[ 3 ];
		var type = msgAry[ 4 ];
		var px = parseFloat( msgAry[ 5 ] );
		var ts = parseFloat( getSymItem( ecn , sym ).ts );
		var dip = parseFloat( getSymItem( ecn , sym ).dip );
		var cfg = readParmAry( gConfGuardAry , type );
		var pxResume = "PxResume";

		if ( px != cfg.parm ){
			var msg = " wrong on if touch px. px : " + px + " . cfg.parm : " + cfg.parm;
			showDebugLog( "readIfTouchMsg" , msg );
			return;
		}

		if ( isNaN( ts ) ){
			var msg = " wrong ts on ecn : " + ecn + " , sym : " + sym ;
			showDebugLog( "readIfTouchMsg" , msg );
			return;
		}

		if ( ecn == "HKE" ){
			gNearBuffer = 30;
			gLeaveBuffer = gNearBuffer + 10;
			gBreakBuffer = -30;
		}

		if ( side = "Resistance" ){
			var nearPx = px - ( gNearBuffer * ts );
			var leavePx = px - ( gLeaveBuffer * ts );
			var breakPx = px - ( gBreakBuffer * ts );
		}else if ( side = "Support" ){
			var nearPx = px + ( gNearBuffer * ts );
			var leavePx = px + ( gLeaveBuffer * ts );
			var breakPx = px + ( gBreakBuffer * ts );
		}

		if ( action == "Near" ){
			var triggerPx = nearPx;
		}else if ( action == "Leave" ){
			var triggerPx = leavePx;
		}else if ( action == "Break" ){
			var triggerPx = breakPx;
		}

		nearPx = nearPx.toFixed( dip );
		leavePx = leavePx.toFixed( dip );
		breakPx = breakPx.toFixed( dip );
		triggerPx = triggerPx.toFixed( dip );

		var reason = action + " " + side + " on " + ecn + " " + sym + ". Current Price: " + triggerPx + ". " + side + ": " + px + ". ";

		if ( action == "Near" ){
			triggerGuardian( type , reason );
		}else{
			if ( cfg.pause ){
				triggerGuardian( pxResume , reason );
			}
		}

		setTimeout( function (){
			$.when( checkIsLeader() ).then( function (){
				if ( gIsLeader == 0 ){
					return;
				}

				if ( action == "Near" ){
					runIfTouchMarco( "Leave" , type );
					runIfTouchMarco( "Break" , type );
				}else if ( action == "Leave" ){
					runIfTouchMarco( "Near" , type );
				}else if ( action == "Break" ){
					var parmName = "_guard_" + type + "_parm";
					setMacro( parmName , 0 );
				}
			} );
		} , 2000 );
	}

	function runIfTouchMarco( stage , type ){
		var macName = "RUN_" + stage + "_" + type;
		startMacro( macName );
	}

	function stopIfTouchMarco( type ){
		var macName = "CANCEL_MACRO_" + type;
		startMacro( macName );
	}

	function buildIfTouchMarco( type , px ){
		if ( px == 0 ){
			return;
		}

		var ecn = getEcnSym().ecn;
		var sym = getEcnSym().sym;

		var ts = parseFloat( getSymItem( ecn , sym ).ts ); // get tick size
		var dip = parseFloat( getSymItem( ecn , sym ).dip );

		if ( isNaN( ts ) ){
			showDebugLog( "buildIfTouchMarco" , "Error On Reading Tick size : " + ts );
			return;
		}

		if ( ecn == "HKE" ){
			gNearBuffer = 30;
			gLeaveBuffer = gNearBuffer + 10;
			gBreakBuffer = -30;
		}

		var highLow = type.split( "Px" )[ 1 ].split( "-" )[ 0 ];
		var num = type.split( "-" )[ 1 ];

		if ( highLow == "High" ){
			var side = "Resistance";
			var bidOrAsk = "BID";
			var bidOrAskOpp = "ASK";
			var calu = "GE";
			var caluOpp = "LE";
			var nearPx = px - ( gNearBuffer * ts );
			var leavePx = px - ( gLeaveBuffer * ts );
			var breakPx = px - ( gBreakBuffer * ts );
			var sideID = "1";
		}else if ( highLow == "Low" ){
			var side = "Support";
			var bidOrAsk = "ASK";
			var bidOrAskOpp = "BID";
			var calu = "LE";
			var caluOpp = "GE";
			var nearPx = px + ( gNearBuffer * ts );
			var leavePx = px + ( gLeaveBuffer * ts );
			var breakPx = px + ( gBreakBuffer * ts );
			var sideID = "2";
		}else{
			return;
		}

		nearPx = nearPx.toFixed( dip );
		leavePx = leavePx.toFixed( dip );
		breakPx = breakPx.toFixed( dip );

		var id = "99" + sideID + num;
		var nearId = id + "1";
		var leaveId = id + "2";
		var breakId = id + "3";

		getSeqNum( gByFunction );

		buildCMD( "Near" , nearPx , nearId );
		buildCMD( "Leave" , leavePx , leaveId );
		buildCMD( "Break" , breakPx , breakId );
		buildCancel( nearId , leaveId , breakId );

		function buildCMD( action , triggerPx , actionID ){
			var setName = "SET_" + action + "_" + type;
			var triggerName = "TRIGGER_" + action + "_" + type;
			var macName = "MACRO_" + action + "_" + type;
			var ifTouchName = "IFTOUCH_" + action + "_" + type;
			var ifDoneName = "IFDONE_" + action + "_" + type;
			var runName = "RUN_" + action + "_" + type;
			var deleteName = "CANCEL_MACRO_" + type;

			var cmdStart = "CMD " + gInitUserID + " MACRO ";

			var cmdifDoneMsg = "MSG " + gInitUserID + " MMNOTICE { TOUCH_PX " + ecn + " " + sym + " " + action + " " + side + " " + type + " " + px + " }";
			if ( action == "Near" ){
				var cmdIfTouch = "IF_TOUCH { " + ecn + " " + sym + " " + actionID + " } { D_" + bidOrAsk + " " + calu + " " + triggerName + " } NAME " + ifTouchName;
				var cmdIfDoneAction = "CALL " + deleteName + " | " + cmdifDoneMsg;;
			}else if ( action == "Leave" ){
				var cmdIfTouch = "IF_TOUCH { " + ecn + " " + sym + " " + actionID + " } { D_" + bidOrAskOpp + " " + caluOpp + " " + triggerName + " } NAME " + ifTouchName;
				var cmdIfDoneAction = "CALL " + deleteName + " | " + cmdifDoneMsg;
			}else if ( action == "Break" ){
				var cmdIfTouch = "IF_TOUCH { " + ecn + " " + sym + " " + actionID + " } { D_" + bidOrAsk + " " + calu + " " + triggerName + " } NAME " + ifTouchName;
				var cmdIfDoneAction = "CALL " + deleteName + " | " + cmdifDoneMsg;
			}

			var cmdIfDone = "IF_DONE 1 " + ecn + " " + actionID + " 2 { " + cmdIfDoneAction + " } NAME " + ifDoneName;

			var cmdSet = cmdStart + setName + " " + "DEFAULT " + triggerName + " " + triggerPx;
			var cmdMain = cmdStart + macName + " " + cmdIfDone + " | " + cmdIfTouch;
			var cmdRun = cmdStart + runName + " CALL " + setName + " | CALL " + macName;

			pushCMD( cmdSet );
			pushCMD( cmdMain );
			pushCMD( cmdRun );

			showLog( "buildIfTouchMarco" , cmdSet );
			showLog( "buildIfTouchMarco" , cmdMain );
			showLog( "buildIfTouchMarco" , cmdRun );
		}

		function buildCancel( nearId , leaveId , breakId ){
			var cmdStart = "CMD " + gInitUserID + " MACRO ";
			var cancelName = "CANCEL_MACRO_" + type;
			var cancel = "CANCEL_ALGO";
			var cmdCancel = cmdStart + cancelName + " " + cancel + " " + nearId + " | " + cancel + " " + leaveId + " | " + cancel + " " + breakId;

			pushCMD( cmdCancel );
			showLog( "buildIfTouchMarco" , cmdCancel );
		}
	}

	function sendResetTriggered(){
		sendMsg( gInitUserID , "MMNOTICE" , "TRIGGERED_RESET" );
	}

	function resetAllGuardianTriggered(){
		for ( i = 0 ; i < gConfGuardAry.length ; i++ ){
			var type = gConfGuardAry[ i ].name;
			editParmAry( gConfGuardAry , type , "triggered" , false );
		}
	}

	function restartAllGuardianIfTouch(){
		var ecn = getEcnSym().ecn;
		var sym = getEcnSym().sym;

		var bidPx = getSymBidAskPx( ecn , sym )[ 0 ];
		var askPx = getSymBidAskPx( ecn , sym )[ 1 ];
		var ts = parseFloat( getSymItem( ecn , sym ).ts ); // get tick size

		if ( ecn == "HKE" ){
			gNearBuffer = 30;
			gLeaveBuffer = gNearBuffer + 10;
			gBreakBuffer = -30;
		}

		for ( i = 0 ; i < gConfGuardAry.length ; i++ ){
			var cfg = gConfGuardAry[ i ];

			if ( !cfg.ifTouch ){
				continue;
			}

			if ( cfg.parm == 0 ){
				continue;
			}

			if ( !cfg.off && !cfg.pause && !cfg.flat && !cfg.flatAll && !cfg.notice ){
				continue;
			}

			var side = cfg.name.split( "Px" )[ 1 ];
			side = side.split( "-" )[ 0 ];

			if ( side == "High" && bidPx > ( cfg.parm - gNearBuffer * ts ) ){
				return;
			}

			if ( side == "Low" && askPx < ( cfg.parm + gNearBuffer * ts ) ){
				return;
			}

			stopIfTouchMarco( cfg.name );

			runIfTouchMarco( "Near" , cfg.name );
		}
	}

	function pushFiveSecondVol(){
		var obj = gMAQuoteAry[ 0 ];

		if ( typeof obj == "undefined" ) {
			return;
		}

		if ( obj.ecn == "PATS" || obj.ecn == "PATX" ){
			ecn = "MREX";
		}

		if ( gMAQuoteAry.length < gFiveSecondTick ){
			return;
		}

		var dataName = obj.ecn + "MA_60_" + obj.sym;

		var sec5Vol = gMAQuoteAry[ 0 ].totalVol - gMAQuoteAry[ gFiveSecondTick - 1 ].totalVol;
		gInitDataStore[ "ma_rollavg" ].set( dataName , sec5Vol );
	}

	function waitCMR(){
		if ( gWaitCMRInterval ){
			return;
		}

		clearInterval( gWaitCMRInterval );
		gWaitCMRInterval = false;

		gGotCMR = false;
		var retryTime = 0;
		var waitCMRTime = 1000;

		if ( gWaitCMRInterval ){
			return;
		}

		gWaitCMRInterval = setInterval( function (){
			if ( gGotCMR ){
				clearInterval( gWaitCMRInterval );
				gWaitCMRInterval = false;
				return;
			}

			if ( retryTime < 5 ){
				retryTime ++;
				return;
			}

			clearInterval( gWaitCMRInterval );
			gWaitCMRInterval = false;

			gStatusObj.disconnected = true;
			var msg = "Unable to receive CMR. Disconnect Client.";
			sendLogMsg( msg );
		} , waitCMRTime );
	}

	function updateAlgoStatus(){
		$.when( checkIsLeader() ).then( function (){
			if ( gIsLeader == 0 ){
				return;
			}

			var status = getAlgoStatus();

			var name = "gsRO" + gInitUserID.toString();
			var parm = "{ " + status + " }";

			setParameter( name , parm );
		} );
	}

	function getAlgoStatus(){
		var date = new Date();
		var weekday = [];
		weekday[ 0 ] = "Sunday";
		weekday[ 1 ] = "Monday";
		weekday[ 2 ] = "Tuesday";
		weekday[ 3 ] = "Wednesday";
		weekday[ 4 ] = "Thursday";
		weekday[ 5 ] = "Friday";
		weekday[ 6 ] = "Saturday";

		var week = weekday[ date.getDay() ];
		var hour = date.getHours().toString();
		var ampm = date.getHours() >= 12 ? 'pm' : 'am';
		var minute = date.getMinutes().toString();
		var time = week + " " + hour + ":" + minute + ampm;

		var pnl = getAccPnL();

		var ecn = getEcnSym().ecn;
		var sym = getEcnSym().sym;

		var netPos = getSymNetPos( ecn , sym ).netPos;

		var openOrder = getOpenOrderNum();

		if ( gStatusObj.on ){
			var on = "On";
		}else if ( !gStatusObj.on ){
			if ( gStatusObj.pause ){
				var on = "Pause";
			}else{
				var on = "Off";
			}
		}

		var status = gInitUserID + " PnL: " + pnl + ". Net Position: " + netPos + ". Algo Status: " + on + ". Open Order: " + openOrder + ". " + time + ".";

		return status;
	}

	function readTradeQuote( m ){
		var ecn = m[ 1 ];
		var mSym = m[ 2 ];

		if ( mSym.indexOf( "K_" ) >= 0 ){
			var sym = mSym.split( "K_" )[ 1 ];
			var type = "K";
		}else if ( mSym.indexOf( "IND_AI_" ) >= 0 ){
			var sym = mSym.split( "IND_AI_" )[ 1 ].split( "#" )[ 0 ];
			var type = "IND_AI";
		}else if ( mSym.indexOf( "RL_" ) >= 0 ){
			var sym = mSym.split( "RL_" )[ 1 ];
			var type = "RL";
		}else{
			return;
		}

		var rEcn = getEcnSym().ecn;
		var rSym = getEcnSym().sym;

		if ( sym != rSym ){
			return;
		}

		if ( type == "K" ){
			if ( rEcn == "MREX" || rEcn == "PATS" || rEcn == "PATX" ) {
				if ( ecn == "MREX" || ecn == "PATS" || ecn == "PATX" ){
					storeTradeDate( m );
				}else if ( ecn == "TREP" ){
					storeReutersTradeDate( m );
				}
			}else if ( rEcn == "HKE" ) {
				if ( ecn == "HKE" ){
					storeTradeDate( m );
				}
			}
		}else if ( type == "RL" ){
			storeRateLimitData( m );
		}
	}

	function storeTradeDate( m ){
		var px = parseFloat( m[ 3 ] );
		var qty = parseFloat( m[ 4 ] );
		var totalVol = parseFloat( m[ 5 ] );

		var obj = gMAQuoteAry[ 0 ];

		if ( typeof( obj ) == "undefined" ){
			return;
		}

		if ( px <= obj.bid ){
			gMAQuoteAry[ 0 ].bidVol += qty;
		}else if ( px >= obj.ask ){
			gMAQuoteAry[ 0 ].askVol += qty;
		}
	}

	function storeReutersTradeDate( m ){
		if ( m.length < 7 ){
			return;
		}

		var px = parseFloat( m[ 3 ] );
		var qty = parseFloat( m[ 4 ] );
		var totalVol = parseFloat( m[ 5 ] );
		var bidOrAsk = parseFloat( m[ 7 ] );

		var obj = gMAQuoteAry[ 0 ];

		if ( typeof( obj ) == "undefined" ){
			return;
		}

		if ( bidOrAsk == 1 ){
			gMAQuoteAry[ 0 ].rtsAskVol += qty;
		}else if ( bidOrAsk == 2 ){
			gMAQuoteAry[ 0 ].rtsBidVol += qty;
		}
	}

	function subscribeSymbol( ecn , sym ){
		var name = "cmeSymbolSubscribe";
		var obj = { ecn: ecn , symbol: sym };
		ee.emit( name , obj );
	}

	function subscribePanelSymbol(){
		var ecn = getEcnSym().ecn;
		var sym = getEcnSym().sym;

		if ( ecn == "PATX" ){
			ecn = "MREX";
		}

		subscribeSymbol( ecn , sym );
		subscribeSymbol( ecn , "K_" + sym );
		subscribeSymbol( ecn , "MA_60_" + sym );
		subscribeSymbol( "TREP" , "K_" + sym ); // for reuters
		subscribeSymbol( "TREP" , "RL_" + sym );

		if( ecn == "PATS" ){
			ecn = "MREX";
		}

		subscribeSymbol( ecn , "IND_AI_" + sym + "#BATT1" ); // for AI
		subscribeSymbol( ecn , "IND_AI_" + sym + "#S20120" );
		subscribeSymbol( ecn , "IND_AI_" + sym + "#VT1" );
		subscribeSymbol( ecn , "IND_AI_" + sym + "#PVR1" );
	}

	function storeRateLimitData( m ){
		if ( m.length < 10 ){
			return;
		}

		var obj = gMAQuoteAry[ 0 ];
		if ( typeof( obj ) == "undefined" ){
			return;
		}

		var askPx = parseFloat( m[ 3 ] );
		var askVol = parseFloat( m[ 4 ] );
		var bidPx = parseFloat( m[ 5 ] );
		var bidVol = parseFloat( m[ 6 ] );
		var noSidePx = parseFloat( m[ 7 ] );
		var noSideVol = parseFloat( m[ 8 ] );

		if ( bidPx > 0 && bidVol > 0 ){
			gMAQuoteAry[ 0 ].rlBidVol += bidVol;
		}
		if ( askPx > 0 && askVol > 0 ){
			gMAQuoteAry[ 0 ].rlAskVol += askVol;
		}
	}

	function gotWrongQuoteData(){
		clearTimeout( gClearQuoteStatusTimer );

		gClearQuoteStatusTimer = setTimeout( function (){
			if ( gGotWrongQuote ){
				gGotWrongQuote = false;
			}
		} , 1000 );
	}

	function getEcnSym(){
		var obj = { ecn: gDefECN , sym: gDefSym };

		var symAry = getSymAry();
		if ( symAry.length == 0 ){
			return obj;
		}
		var ecn = symAry[ 0 ][ 0 ];
		var sym = symAry[ 0 ][ 1 ];

		obj = { ecn: ecn , sym: sym };
		return obj;
	}

	function sendTableMsg( type , midPx , preMid , vol , bidVol , askVol ){
		if ( !gStatusObj.on ){
			return;
		}
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){
			return;
		}
		$.when( checkIsLeader() ).then( function (){
			if ( gIsLeader == 0 ){
				return;
			}
			var topic = "a_tableMsg";
			var date = new Date();
			var minutes = ( "0" + ( date.getMinutes() ) ).slice( -2 );
			var seconds = ( "0" + ( date.getSeconds() ) ).slice( -2 );
			var time = date.getHours() + ":" + minutes + ":" + seconds;


			var msg = topic + " " + type + " " + time + " " + midPx + " " + preMid + " " + vol + " " + bidVol + " " + askVol;
			sendDataMsg( msg );
		} );
	}

	function readTableMsg( cmrAry , i ){
		var topic = cmrAry[ 5 + i ];
		var time = cmrAry[ 6 + i ];
		var midPx = parseFloat( cmrAry[ 7 + i ] );
		var prevMidPx = parseFloat( cmrAry[ 8 + i ] );
		var vol = parseFloat( cmrAry[ 9 + i ] );
		var bidVol = parseFloat( cmrAry[ 10 + i ] );
		var askVol = parseFloat( cmrAry[ 11 + i ] );

		var ecn = getEcnSym().ecn;
		var sym = getEcnSym().sym;
		var ts = parseFloat( getSymItem( ecn , sym ).ts );

		var pipChg = parseFloat( ( ( midPx - prevMidPx ) / ts ).toFixed() );

		var pxDirection = 0;
		if ( midPx > prevMidPx ){
			pxDirection = 1;
		}else if ( midPx < prevMidPx ){
			pxDirection = -1;
		}

		if ( bidVol == 0 && askVol == 0 ){
			var ratio = 0.5;
		}else{
			var ratio = parseFloat( ( bidVol / ( bidVol + askVol ) ).toFixed( 2 ) );
		}

		var obj = { topic: topic ,
					time: time ,
					midPx: midPx ,
					prevMidPx: prevMidPx ,
					pipChg: pipChg ,
					pxDirection: pxDirection ,
					vol: vol ,
					bidVol: bidVol ,
					askVol: askVol ,
					ratio: ratio ,
					};

		gTableMsgAry.unshift( obj );

		if ( gTableMsgAry.length > 2000 ){
			gTableMsgAry.pop(); // remove last row
		}
	}

	function readDataTableMsg( cmrAry ){
		var type = cmrAry[ 7 ];
		var time = cmrAry[ 8 ];
		var midPx = parseFloat( cmrAry[ 9 ] );
		var prevMidPx = parseFloat( cmrAry[ 10 ] );
		var pxIcon = cmrAry[ 11 ];
		var vol = parseFloat( cmrAry[ 12 ] );
		var parm = parseFloat( cmrAry[ 13 ] );
		var level = parseFloat( cmrAry[ 14 ] );

		var obj = { type: type , time: time , midPx: midPx , prevMidPx: prevMidPx , pxIcon: pxIcon , vol: vol , parm: parm , level: level };
		gTableMsgAry.unshift( obj );

		if ( gTableMsgAry.length > 1000 ){
			gTableMsgAry.pop(); // remove last row
		}
	}

	function uiClickShowFiveSecVolTable(){
		$( ".fiveSecVolTableDiv" ).show();
	}

	function uiClickShowTickVolTable(){
		$( ".tickVolTableDiv" ).show();
	}

	function uiClickShowOrderCountTable(){
		$( ".orderCountTableDiv" ).show();
	}

	function uiClickCloseSecDiv(){
		$( this ).closest( ".confSec" ).hide();
	}

	function getMsgData(){
		var type = "getmsg";
		var host = "https://bigmindtrader.com:28443/ts/";
		var uid = "uid=" + gInitUserID;
		var filter = "filter=LOG";
		var token = "token=123";
		var link = host + type + "?" + uid + "&" + filter + "&" + token;

		$.ajax( {
			url: link,
			dataType: "html",
			success: function( data ){
				// showLog( "getMsgData" , data );
				var cmrLine = data.split( "\n" );
				for ( var i = 0 ; i < cmrLine.length ; i ++ ){
					var cmrAry = cmrLine[ i ].split( " " );
					var msgType = cmrAry[ 6 ];
					if ( msgType == "a_tableMsg" ){
						readTableMsg( cmrAry , 2 );
					}
				}
			},
		} );
	}

	function checkTickVolume( parm ){
		if ( gCountingTickVolume ){
			return;
		}
		gCountingTickVolume = true;

		var ary = gMAQuoteAry.slice( 0 , gFiveSecondTick );
		var sumVol = ary.reduce( sumTotalVol ).sumVol;
		var index = gFiveSecondTick - 1;

		for ( var i = 0 ; i < gFiveSecondTick ; i++ ){
			if ( gMAQuoteAry[ i ].totalVol >= gMAQuoteAry[ i + 1 ].totalVol * 0.95 ){
				continue;
			}
			if ( gMAQuoteAry[ i ].totalVol > ( sumVol * 0.1 ) ){
				continue;
			}
			index = i;
			break;
		}

		if ( index == gFiveSecondTick - 1 ){
			sendTickVolume();
		}else{
			gTickCount = index;
		}

		function sumTotalVol( a , b ){
			return { sumVol: a.totalVol + b.totalVol };
		}
	}

	function countTickVolume(){
		if ( !gCountingTickVolume ){
			return;
		}
		gTickCount++;
		if ( gTickCount < gFiveSecondTick ){
			return;
		}
		sendTickVolume();
	}

	function sendTickVolume(){
		gCountingTickVolume = false;

		var ary = gMAQuoteAry.slice( 0 , gFiveSecondTick );
		var obj = ary[ 0 ];
		var preObj = ary[ gFiveSecondTick - 1 ];
		var vol = obj.totalVol - preObj.totalVol;

		var mid = parseFloat( ( ( obj.bid + obj.ask ) / 2 ).toFixed( obj.dip + 1 ) );
		var preMid = parseFloat( ( ( preObj.bid + preObj.ask ) / 2 ).toFixed( preObj.dip + 1 ) );

		var rlbidVol = ary.reduce( sumRlBidVol ).rlBidVol;
		var rlAskVol = ary.reduce( sumRlAskVol ).rlAskVol;
		if ( rlbidVol == 0 && rlAskVol == 0 ){
			var ratio = 50;
		}else{
			var ratio = ( rlbidVol / ( rlbidVol + rlAskVol ) * 100 ).toFixed();
		}

		sendTableMsg( "tickVol" , mid , preMid , vol , rlbidVol , rlAskVol );

		function sumRlBidVol( a , b ){
			return { rlBidVol: a.rlBidVol + b.rlBidVol };
		}
		function sumRlAskVol( a , b ){
			return { rlAskVol: a.rlAskVol + b.rlAskVol };
		}
	}

	function getDailyOrderCount(){
		var type = "cme-msg-stats";
		var host = "https://bigmindtrader.com:28443/ts/";
		var link = host + type;

		$.ajax( {
			url: link ,
			dataType: "html" ,
			success: function( data ){
				vueOrderCountTable.orderCountAry = [];
				var ary = JSON.parse( data );

				for ( var i = 0 ; i < ary.length ; i ++ ){
					var obj = {};
					obj.ecn = ary[ i ].e;
					obj.sym = ary[ i ].s;

					if ( typeof( ary[ i ].c.N ) == "undefined" ){
						obj.num = 0;
					}else{
						obj.num = ary[ i ].c.N;
					}

					vueOrderCountTable.orderCountAry.push( obj );
					dailyOrderGuardian( obj.ecn , obj.sym , obj.num )
				}
			} ,
		} );
	}

	function dailyOrderGuardian( ecn , sym , num ){
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){
			return;
		}

		var type = "MaxDailyOrder";
		var cfg = readParmAry( gConfGuardAry , type );

		if ( !cfg.pause && !cfg.off && !cfg.flat && !cfg.flatAll && !cfg.notice ){
			return;
		}

		if ( ecn != readParmAry( gConfOrderAry , "TradeEcn" ).select || sym != readParmAry( gConfOrderAry , "TradeSym" ).select ){
			return;
		}

		if ( num < cfg.parm ){
			return
		}

		var reason = "Exceed Maximum Daily Order Limit On " + ecn + " " + sym + ". Daily Order Limit: " + cfg.parm + ". ";

		triggerGuardian( type , reason );
	}
}