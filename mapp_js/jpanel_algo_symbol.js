var gSym_HKE = {"HSIV8" : "HSIV8" ,
				"MHIV8" : "MHIV8" ,
				"HHIV8" : "HHIV8" ,
				"MCHV8" : "MCHV8" ,
				"GDUV8" : "GDUV8" ,
				"GDRV8" : "GDRV8" ,
				"CUSV8" : "CUSV8" ,
				"UCNV8" : "UCNV8" ,
				"HSIX8" : "HSIX8" ,
				"MHIX8" : "MHIX8" ,
				"HHIX8" : "HHIX8" ,
				"MCHX8" : "MCHX8" ,
				"GDUX8" : "GDUX8" ,
				"GDRX8" : "GDRX8" ,
				"CUSX8" : "CUSX8" ,
				"UCNX8" : "UCNX8" ,
				"HSIZ8" : "HSIZ8" ,
				"MHIZ8" : "MHIZ8" ,
				"HHIZ8" : "HHIZ8" ,
				"MCHZ8" : "MCHZ8" ,
				"GDUZ8" : "GDUZ8" ,
				"GDRZ8" : "GDRZ8" ,
				"CUSZ8" : "CUSZ8" ,
				"UCNZ8" : "UCNZ8" ,
				};

var gSym_CME = {"CME/CMX_GLD/DEC18" : "CME/CMX_GLD/DEC18" ,
				"CME/CMX_GLD/FEB19" : "CME/CMX_GLD/FEB19" ,
				"CME/CMX_GLD/MAY19" : "CME/CMX_GLD/MAY19" ,
				"CME/CMX_SIL/DEC18" : "CME/CMX_SIL/DEC18" ,
				"CME/CMX_SIL/MAR19" : "CME/CMX_SIL/MAR19" ,
				"CME/CMX_COP/DEC18" : "CME/CMX_COP/DEC18" ,
				"CME/CMX_COP/MAR19" : "CME/CMX_COP/MAR19" ,
				"CME/CRUDE/DEC18" : "CME/CRUDE/DEC18" ,
				"CME/CRUDE/JAN19" : "CME/CRUDE/JAN19" ,
				"CME_CBT/DJIA5/DEC18" : "CME_CBT/DJIA5/DEC18" ,
				"CME_CBT/DJIA5/MAR19" : "CME_CBT/DJIA5/MAR19" ,
				"CME/MINI_SNP/DEC18" : "CME/MINI_SNP/DEC18" ,
				"CME/MINI_SNP/MAR19" : "CME/MINI_SNP/MAR19" ,
				"SGXQ/QNK/DEC18" : "SGXQ/QNK/DEC18" ,
				"SGXQ/QNK/MAR19" : "SGXQ/QNK/MAR19" ,
				};
