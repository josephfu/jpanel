function jpanelAlgo( gInitObj , gDir ){
	var gInitUserID = gInitObj.u; //// get init object from GFA ////
	var gInitOrdSubscription = gInitObj.oc;
	var gInitSymItem = gInitObj.fsi;
	var gInitOpenOrdCount = gInitObj.ooc;
	var gInitAccPnl = gInitObj.tot;
	var gInitNetPos = gInitObj.onp;
	var gInitMktPx = gInitObj.mp;
	var gInitBidAry = gInitObj.cba;
	var gInitAskAry = gInitObj.caa;
	var gInitNotice = gInitObj.sg;
	var gInitDataStore = gInitObj.ds;

	if ( gStatusObj.hostName == "192.168.1.187" ){ // for m6 testing server setting
		gTelegramTargetAry = [];
		gTelegramTargetAry[ 0 ] = { userID : [ gInitUserID ] , target: [ "joseph" ] };
	}

	/////////////////////////////// initAlgo ////////////////////
	init();

	/////////////////////////////// function /////////////////////////
	function init(){
		ee.addListener( "MC_CMR" , readMacro ); // run readMacro when received "CMR" msg
		ee.addListener( "MMO_O_CH_DIS" , GFA_disconnected ); // run GFA_disconnected when GFA is disconnected
		ee.addListener( "MMO_M_M_M_D_E" , readPriceQuote );
		ee.addListener( "e_m_k_c" , readTradeQuote ); // gfa intrinsic

		$.when( getClientIP() ).then( function (){ // wait until client got IP address
			getLocalStore();
			getMacro();
		} );
	}

	function initPanel( cmrAry ){ // start panel
		// e.g. CMR 302 GET_MACRO SESSID 198 SEQ 192.168.0.110::48
		var seq = cmrAry[ 6 ];

		var ip = seq.split( "::" )[ 0 ];
		var num = parseInt( seq.split( "::" )[ 1 ] );

		if ( ip != gStatusObj.ip || num != gRandomNum ){ // check who are getting macro
			return;
		}

		setTimeout( function (){
			updatePnLStopLable();
			getAllParameter();
			setPanelButton();
			subscribePanelSymbol();
			gStatusObj.completedLoad = true;
			getCurrentOrder();
			getMsgData();

			$.when( electLeader() ).then( function (){
				$.when( checkIsLeader() ).then( function (){
					onPanelTimer();

					if ( !gLeaderObj.isLeader ){
						return;
					}
					setStopAll();
				} );
			} );
		} , 500 );
	}

	function setPanelButton(){
		if ( !gStatusObj.inited ){
			gStatusObj.inited = true;
		}else{
			return;
		}

		$( ".confPanlSec" ).on( "mouseup" , ".confPanlCfmDiv" , uiConfimPanlCfg );
		$( ".confPanlSec" ).on( "mouseup" , ".confReldDiv" , uiClickReloadAll );
		$( ".confPanlSec" ).on( "mouseup" , ".confReldAllDiv" , uiClickReloadEveryOne );
		$( ".confPanlSec" ).on( "mouseup" , ".confTypeIconDiv" , uiClickCfgAdvCfgBtn );
		$( ".confGuarSec" ).on( "mouseup" , ".confGuarCfmDiv" , uiConfimGuarCfg );
		$( ".confGuarSec" ).on( "change" , ".confOffChk" , uiClickCfgChkOffBox );
		$( ".confGuarSec" ).on( "change" , ".confPauseChk" , uiClickCfgChkPauseBox );
		$( ".confGuarSec" ).on( "change" , ".confFlatAllChk" , uiClickCfgChkFlatAllBox );
		$( ".confAdvCfgSec" ).on( "mouseup" , ".confAdvCfgCfmDiv" , uiConfimAdvCfg );
		$( ".confErrorSec" ).on( "click" , ".confErrorCnlBtnDiv" , uiClickCloseErrorDiv );
		$( ".confOrderSec" ).on( "click" , ".confOrderCfmDiv" , uiClickSaveOrder );
		$( ".confOrderSec" ).on( "change" , ".confSlet" , uiChgPanelEcn );
		$( ".confOrderSec" ).on( "mouseup" , ".confDeleteAllDiv" , uiClickDeleteAllMacro );
		$( ".cfmSec" ).on( "click" , ".cfmOnCnlBtn" , uiHideCfmSec );
		$( ".cfmSec" ).on( "click" , ".cfmOnCfmBtn" , uiClickCfmOnPanel );
		$( ".panlSec" ).on( "mousedown" , uiHideCfgSec );
		$( ".panlSec" ).on( "mouseup" , ".panlMode2Div" , uiClickPanelMode2 );
		$( ".titleSec" ).on( "mouseup" , ".panlCfgDiv" , uiClickCfgPanelBtn );
		$( ".titleSec" ).on( "mouseup" , ".panlGuarDiv" , uiClickCfgGuarBtn );
		$( ".titleSec" ).on( "mouseup" , ".panlOrderDiv" , uiClickCfgOrderBtn );
		$( ".titleSec" ).on( "mouseup" , ".panlSwLbl" , function (){ uiClickPanlSwitch( $( this ).find( ".swIpt" ).val() ); } );
		$( ".titleSec" ).on( "dblclick" , ".panlMaxProfitDiv" , renewMaxProfit );
		$( ".titleSec" ).on( "mouseup" , ".panlFiveSecVolTableDiv" , uiClickShowFiveSecVolTable );
		$( ".titleSec" ).on( "mouseup" , ".panlTickVolTableDiv" , uiClickShowTickVolTable );
		$( ".titleSec" ).on( "mouseup" , ".panlOrderCountTableDiv" , uiClickShowOrderCountTable );
		$( ".confSec" ).on( "mouseup" , ".closeSecBtn" , uiClickCloseSecDiv );
		$( ".confSec" ).on( "mouseup" , ".hideChartSecBtn" , uiClickHideCauseDataChart );
		$( ".confSec" ).resizable( {
			handles: "n , s" ,
			maxHeight: 1000 ,
			minHeight: 400 ,
		} );
		$( ".confSec" ).draggable( { // drag config section
			cancel: ".confModeListUl , .confTxtDiv , .iptDiv , .txtIpt , input , .modePanelCfgDiv" ,
		} );
		$( ".confMode2Sec" ).sortable( {
			items: ".modeDragCaseDiv" ,
			containment: ".modePanelCfgDiv" ,
			connectWith: ".modeDragTypeDiv" ,
			placeholder: "modePlaceHolder" ,
			dropOnEmpty: true ,
			cancel: ".modeTypeTitle" ,
			change: function( event , ui ){
				var name = ui.item.attr( "name" );
				var axis = $( event.toElement.parentNode ).attr( "axis" );
				var oAxis = readParmAry( gModeCauseAry , name ).axis;
				if ( axis == oAxis || typeof( axis ) == "undefined" ){
					return;
				}

				var macName = "a_mCauseCfg_" + name + "_axis";
				setMacro( macName , axis );
			} ,
			update: function( event , ui ){
				$( event.toElement.parentNode ).find( ".modeDragCaseDiv" ).each( function (){
					var index = $( this ).index();
					var name = $( this ).attr( "name" );

					if ( typeof name == "undefined" ){
						return;
					}

					var oIndex = readParmAry( gModeCauseAry , name ).index;
					if ( index == oIndex ){
						return;
					}

					var macName = "a_mCauseCfg_" + name + "_index";
					setMacro( macName , index );
				} );
			} ,
		} );
		$( document ).keydown( function ( e ){ ctrlKeyDown( e ) } );
		$( document ).keyup( function ( e ){ ctrlKeyUp( e ) } );
		$( document ).keydown( function ( e ){ shiftKeyDown( e ) } );
		$( document ).keyup( function ( e ){ shiftKeyUp( e ) } );
	}

	////////////////////////// Vue Object ///////////////

	var vueFunction = new Vue( {
		data: {
			causeAry: gModeCauseAry ,
			axisNumObj: gModeAxisNumObj ,
			statusObj: gAlgoStatusObj ,
		} ,
		methods: {
			clickCauseCfg: function( obj ){
				vueModeCauseCfg.causeObj = obj;
			} ,
			clearCauseStatus: function( obj ){
				editParmAry( gModeCauseAry , obj.name , "bidStatus" , "" );
				editParmAry( gModeCauseAry , obj.name , "askStatus" , "" );
				updateModeCause( obj.name );
			} ,
			openDataChart: function( obj ){
				var name = obj.name;
				var curName = $( ".modeChartCauseName" ).val();
				if ( name != curName ){
					$( ".modeChartCauseName" ).val( name );
					restartStoreChartDataInterval( obj );
				}else{
					drawCauseDataChart();
				}

				$( ".modeChartSec" ).show();
			} ,
			clickModeTable: function( event ){
				var causeCfg = $( event.target ).closest( "td" ).hasClass( "modeCauseCfgBtn" );
				if ( !causeCfg ){
					vueModeCauseCfg.show = false;
				}else{
					vueModeCauseCfg.show = true;
				}

				var panelCfg = $( event.target ).closest( "td" ).hasClass( "modePanelCfgBtn" );
				if ( !panelCfg ){
					vueModePanelCfg.show = false;
				}else{
					vueModePanelCfg.show = true;
				}

				var matrixGrid = $( event.target ).closest( "td" ).hasClass( "modeMatrixGrid" );
				if ( !matrixGrid ){
					vueModeOrderCfg.show = false;
				}else{
					vueModeOrderCfg.show = true;
				}

				var actionCfg = $( event.target ).closest( "td" ).hasClass( "modeActionCfgBtn" );
				if ( !actionCfg ){
					vueModeActionCfg.show = false;
				}else{
					vueModeActionCfg.show = true;
				}
			} ,
			clickCauseSwitch: function( obj ){
				if ( obj.on ){
					var parm = 0;
				}else{
					var parm = 1;
				}

				var prop = "on";
				var name = "a_mCauseCfg_" + obj.name + "_" + prop;
				setMacro( name , parm );
			} ,
			getCauseGridClass: function ( pos , bidOrAsk , obj ){
				var tdClass = "modeCauseGrid";
				tdClass += " mode" + obj.axis + "Grid";
				if ( pos == obj[ bidOrAsk + "Pos" ] ){
					tdClass += " modeSelectedGrid";
				}
				if ( obj.on ){
					tdClass += " modeEnabledGrid";
				}
				if ( this.statusObj.on && this.statusObj[ bidOrAsk + "On" ] ){
					tdClass += " modeOnGrid";
				}
				if ( obj.title == "Manual Adject" ){
					tdClass += " modeActivedGrid modeBtn";
				}else if ( this.statusObj[ bidOrAsk + "Auto" ] ){
					tdClass += " modeActivedGrid";
				}
				if ( obj.hasOwnProperty( "priority" + pos ) && obj[ "priority" + pos ] == 1 ){
					tdClass += " modePriorityGrid";
				}
				if ( obj.hasOwnProperty( "notice" + pos ) && obj[ "notice" + pos ] == 1 ){
					tdClass += " modeNoticeGrid";
				}
				return tdClass;
			} ,
			getCauseSwitchClass: function ( obj ){
				var tdClass = "modeCauseSwitchGrid modeCauseGrid modeBtn";
				if ( obj.on ){
					tdClass += " modeEnabledGrid";
				}
				if ( obj.title == "Manual Adject" && ( this.statusObj.bidOn || this.statusObj.askOn ) ){
					tdClass += " modeActivedGrid";
				}else if ( ( this.statusObj.bidAuto || this.statusObj.askAuto ) && ( this.statusObj.bidOn || this.statusObj.askOn ) ){
					tdClass += " modeActivedGrid";
				}
				if ( this.statusObj.on ){
					tdClass += " modeOnGrid";
				}
				return tdClass;
			} ,
			getPointerGridClass: function ( pos , bidOrAsk , axis ){
				var tdClass = "modePointerGrid";
				tdClass += " mode" + axis + "Grid";
				if ( pos == getPointerPos( axis , bidOrAsk ) ){
					tdClass += " modeSelectedGrid";
				}
				if ( this.statusObj.on && this.statusObj[ bidOrAsk + "On" ] ){
					tdClass += " modeOnGrid";
				}
				return tdClass;
			} ,
			clickCauseGrid: function( obj , pos , bidOrAsk ){
				if ( obj.title != "Manual Adject" ){
					return;
				}
				if ( bidOrAsk == "both" ){
					var macName = "a_mCauseCfg_" + obj.name + "_bidStatus";
					setMacro( macName , pos );
					var macName = "a_mCauseCfg_" + obj.name + "_askStatus";
					setMacro( macName , pos );
					return;
				}

				var property = bidOrAsk + "Status";

				var macName = "a_mCauseCfg_" + obj.name + "_" + property;
				setMacro( macName , pos );
			} ,
			getCauseParm: function ( pos , obj ){
				if ( pos == 0 ){
					return "";
				}
				if ( pos > 0 ){
					pos --;
				}
				if ( !obj.hasOwnProperty( "parm" + pos ) ){
					return "";
				}
				if ( isNaN( parseFloat( obj[ "parm" + pos ] ) ) ){
					return "";
				}
				return obj[ "parm" + pos ];
			} ,
			clickAddAxisNum: function( axis ){
				if ( this.axisNumObj[ axis + "Num" ] >= 15 ){
					return;
				}
				var parm = this.axisNumObj[ axis + "Num" ] + 2;
				var macName = "a_mAxisNum_" + axis + "Num";
				setMacro( macName , parm );
			} ,
			clickReduceAxisNum: function( axis ){
				if ( this.axisNumObj[ axis + "Num" ] <= 3 ){
					return;
				}
				var parm = this.axisNumObj[ axis + "Num" ] - 2;
				var macName = "a_mAxisNum_" + axis + "Num";
				setMacro( macName , parm );
			} ,
			getCausePos: function ( index , axis ){
				if ( axis == "None" ){
					axis = "Spread";
				}
				return index + 1 - ( ( this.axisNumObj[ axis + "Num" ] + 1 ) / 2 );
			} ,
			sortedCause: function (){
				var sortAry = gModeCauseAry.sort( function( a , b ){
					if ( a.index > b.index ){
						return 1;
					}else if ( a.index < b.index ){
						return -1;
					}
					return 0;
				} );
				return sortAry;
			} ,
		} ,
	} );

	var vueModeMatrix = new Vue( {
		el: ".modeMatrixDiv" ,
		data: {
			axisNumObj: gModeAxisNumObj ,
			statusObj: gAlgoStatusObj ,
			posObj: gModePositionObj ,
			actionAry: gModeActionAry ,
			hideCause: true ,
			orderAry: gModeOrderAry ,
		} ,
		methods: {
			getMatrixGridClass: function ( curShift , curSpread , bidOrAsk ){
				var newShift = getPointerPos( "Shift" , bidOrAsk );
				var newSpread = getPointerPos( "Spread" , bidOrAsk );
				var oldShift = this.posObj[ bidOrAsk + "Shift" ];
				var oldSpread = this.posObj[ bidOrAsk + "Spread" ];

				var isShift = false;
				var isSpread = false;

				var bidOrAskStr = bidOrAsk.charAt( 0 ).toUpperCase() + bidOrAsk.slice( 1 );
				var tdClass = "modeMatrixGrid modeBtn modeMatrix" + bidOrAskStr + "Grid";

				if ( curShift == oldShift ){
					tdClass += " modeMacroShift";
					isShift = true;
				}
				if ( curSpread == oldSpread ){
					tdClass += " modeMacroSpread";
					isSpread = true;
				}
				if ( this.statusObj.on && this.statusObj[ bidOrAsk + "On" ] ){
					tdClass += " modeOnGrid";
				}
				if ( isShift && isSpread ){
					if ( newShift != oldShift || newSpread != oldSpread ){
						changeModePos( bidOrAsk , newShift , newSpread , oldShift , oldSpread );
					}
				}
				if ( typeof ( vueModeOrderCfg ) != "undefined" ){
					if ( curShift == vueModeOrderCfg.shiftPos &&
						curSpread == vueModeOrderCfg.spreadPos &&
						vueModeOrderCfg.show ){
						tdClass += " modeCfgGrid";
					}
				}
				for ( var i = 0; i < this.actionAry.length ; i++ ){
					if ( !this.actionAry[ i ].on ){
						continue;
					}
					if ( this.actionAry[ i ].axis == "Spread" && this.actionAry[ i ].pos != curSpread ){
						continue;
					}
					if ( this.actionAry[ i ].axis == "Shift" && this.actionAry[ i ].pos != curShift ){
						continue;
					}
					tdClass += " modeActionGrid";
					break;
				}
				return tdClass;
			} ,
			getCoolDownGridId: function ( pos , axis ){
				var id = "modeCoolDown" + axis + pos;
				return id;
			} ,
			clickModeTable: function( event ){
				vueFunction.clickModeTable( event );
			} ,
			getCausePos: function ( index , axis ){
				return vueFunction.getCausePos( index , axis );
			} ,
			clickModeSwitchBtn: function ( bidOrAsk , type ){
				if ( gAlgoStatusObj[ bidOrAsk + type ] ){
					var on = false;
				}else{
					var on = true;
				}
				changeModeStatus( bidOrAsk , type , on );
			} ,
			clickPanelControlBtn: function ( type ){
				uiClickPanlSwitch( type );
			} ,
			clickMatrixGrid: function( shiftPos , spreadPos ){
				vueModeOrderCfg.shiftPos = shiftPos;
				vueModeOrderCfg.spreadPos = spreadPos;
			} ,
			clickHideCauseGrid: function(){
				vueModeShift.show = false;
				vueModeSpread.show = false;
				vueModeNone.show = false;
				this.hideCause = true;
			} ,
			clickShowCauseGrid: function(){
				vueModeShift.show = true;
				vueModeSpread.show = true;
				vueModeNone.show = true;
				this.hideCause = false;
			} ,
			clickHidePanelBtn: function(){
				$( ".confMode2Sec" ).hide();
			} ,
			clickActionCfgBtn: function( axis , pos ){
				for ( var i = 0 ; i < gModeActionCfgAry.length ; i++ ){
					gModeActionCfgAry[ i ].on = false;
					gModeActionCfgAry[ i ].parm = "";
				}
				for ( var i = 0 ; i < gModeActionAry.length ; i++ ){
					var obj = gModeActionAry[ i ];
					if ( obj.axis != axis ){
						continue;
					}
					if ( obj.pos != pos ){
						continue;
					}
					for ( var j = 0 ; j < gModeActionCfgAry.length ; j++ ){
						if ( obj.name != gModeActionCfgAry[ j ].name ){
							continue;
						}
						gModeActionCfgAry[ j ].on = obj.on;
						gModeActionCfgAry[ j ].parm = obj.parm;
						break;
					}
				}
				vueModeActionCfg.axis = axis;
				vueModeActionCfg.pos = pos;
				vueModeActionCfg.show = true;
			} ,
			chgManualAxis: function ( axis , type , side ){
				var name = "Manual" + axis;
				var causeAry = readParmAry( gModeCauseAry , name );
				var bidPos = causeAry.bidStatus;
				var askPos = causeAry.askStatus;

				if ( type == "minus" ){
					var newBidPos = bidPos - 1;
					var newAskPos = askPos - 1;
				}else if ( type == "add" ){
					var newBidPos = bidPos + 1;
					var newAskPos = askPos + 1;
				}

				var posMax = ( gModeAxisNumObj[ axis + "Num" ] - 1 ) / 2;
				var posMin = posMax * -1;

				if ( side == "both" || side == "bid" ){
					if ( newBidPos <= posMax && newBidPos >= posMin ){
						var macName = "a_mCauseCfg_" + name + "_bidStatus";
						setMacro( macName , newBidPos );
					}
				}
				if ( side == "both" || side == "ask" ){
					if ( newAskPos <= posMax && newAskPos >= posMin ){
						var macName = "a_mCauseCfg_" + name + "_askStatus";
						setMacro( macName , newAskPos );
					}
				}
			} ,
			getOrderPip: function ( shiftPos , spreadPos , bidOrAsk ){
				var pips = "";
				for ( var i = 0 ; i < this.orderAry.length ; i++ ){
					var obj = this.orderAry[ i ];
					if ( obj.shift != shiftPos ){
						continue;
					}
					if ( obj.spread != spreadPos ){
						continue;
					}
					if ( obj.bidOrAsk != bidOrAsk ){
						continue;
					}
					pips = obj.pips;
					break;
				}
				return pips;
			} ,
		} ,
	} );

	var vueModeShift = new Vue( {
		el: ".modeShiftDiv" ,
		data: {
			axis: "Shift" ,
			causeAry: gModeCauseAry ,
			axisNumObj: gModeAxisNumObj ,
			show: false ,
		} ,
		methods: {
			clickAddAxisNum: function(){
				vueFunction.clickAddAxisNum( this.axis );
			} ,
			clickReduceAxisNum: function(){
				vueFunction.clickReduceAxisNum( this.axis );
			} ,
			getCausePos: function ( index ){
				return vueFunction.getCausePos( index , this.axis );
			} ,
			getPointerGridClass: function ( pos , bidOrAsk ){
				return vueFunction.getPointerGridClass( pos , bidOrAsk , this.axis );
			} ,
			getCauseSwitchClass: function ( obj ){
				return vueFunction.getCauseSwitchClass( obj );
			} ,
			getCauseGridClass: function ( pos , bidOrAsk , obj ){
				return vueFunction.getCauseGridClass( pos , bidOrAsk , obj );
			} ,
			clickCauseGrid: function( obj , pos , bidOrAsk ){
				vueFunction.clickCauseGrid( obj , pos , bidOrAsk );
			} ,
			clickCauseSwitch: function( obj ){
				vueFunction.clickCauseSwitch( obj );
			} ,
			clickModeTable: function( event ){
				vueFunction.clickModeTable( event );
			} ,
			clickCauseCfg: function( obj ){
				vueFunction.clickCauseCfg( obj );
			} ,
			clearCauseStatus: function( obj ){
				vueFunction.clearCauseStatus( obj );
			} ,
			openDataChart: function( obj ){
				vueFunction.openDataChart( obj );
			} ,
			getCauseParm: function ( pos , obj ){
				return vueFunction.getCauseParm( pos , obj );
			} ,
		} ,
		computed: {
			sortedCause: function(){
				return vueFunction.sortedCause();
			} ,
		} ,
	} );

	var vueModeSpread = new Vue( {
		el: ".modeSpreadDiv" ,
		data: {
			axis: "Spread" ,
			causeAry: gModeCauseAry ,
			axisNumObj: gModeAxisNumObj ,
			show: false ,
		} ,
		methods: {
			clickAddAxisNum: function(){
				vueFunction.clickAddAxisNum( this.axis );
			} ,
			clickReduceAxisNum: function(){
				vueFunction.clickReduceAxisNum( this.axis );
			} ,
			getCausePos: function ( index ){
				return vueFunction.getCausePos( index , this.axis );
			} ,
			getPointerGridClass: function ( pos , bidOrAsk ){
				return vueFunction.getPointerGridClass( pos , bidOrAsk , this.axis );
			} ,
			getCauseSwitchClass: function ( obj ){
				return vueFunction.getCauseSwitchClass( obj );
			} ,
			getCauseGridClass: function ( pos , bidOrAsk , obj ){
				return vueFunction.getCauseGridClass( pos , bidOrAsk , obj );
			} ,
			clickCauseGrid: function( obj , pos , bidOrAsk ){
				vueFunction.clickCauseGrid( obj , pos , bidOrAsk );
			} ,
			clickCauseSwitch: function( obj ){
				vueFunction.clickCauseSwitch( obj );
			} ,
			clickModeTable: function( event ){
				vueFunction.clickModeTable( event );
			} ,
			clickCauseCfg: function( obj ){
				vueFunction.clickCauseCfg( obj );
			} ,
			clearCauseStatus: function( obj ){
				vueFunction.clearCauseStatus( obj );
			} ,
			openDataChart: function( obj ){
				vueFunction.openDataChart( obj );
			} ,
			getCauseParm: function ( pos , obj ){
				return vueFunction.getCauseParm( pos , obj );
			} ,
		} ,
		computed: {
			sortedCause: function(){
				return vueFunction.sortedCause();
			} ,
		} ,
	} );

	var vueModeCauseCfg = new Vue( {
		el: ".modeCauseCfgDiv" ,
		data: {
			causeAry: gModeCauseAry ,
			axisNumObj: gModeAxisNumObj ,
			causeObj: "" ,
			show: false ,
		} ,
		methods: {
			getCausePos: function ( index , axis ){
				return vueFunction.getCausePos( index , axis );
			} ,
			submitCauseParm: function (){
				var name = this.causeObj.name;
				var causeAry = readParmAry( gModeCauseAry , name );

				$( ".modeCauseCfgParm" ).each( function (){
					var pos = $( this ).attr( "id" ).split( "modeCauseCfgParm_" )[ 1 ];
					var prop = "parm" + pos;
					var macName = "a_mCauseCfg_" + name + "_" + prop;
					var parm = parseFloat( $( this ).val() );
					var oParm = parseFloat( causeAry[ prop ] );

					if ( isNaN( parm ) ){
						if ( !isNaN( oParm ) ){
							deleteMacro( macName );
						}
						return;
					}

					if ( parm == oParm ){
						return;
					}

					setMacro( macName , parm );
				} );

				$( ".modeCausePropParm" ).each( function (){
					var pos = $( this ).attr( "id" ).split( "modeCausePropParm_" )[ 1 ];
					var prop = "propParm" + pos;
					var macName = "a_mCauseCfg_" + name + "_" + prop;
					var parm = parseFloat( $( this ).val() );
					var oParm = parseFloat( causeAry[ prop ] );

					if ( isNaN( parm ) ){
						if ( !isNaN( oParm ) ){
							deleteMacro( macName );
						}
						return;
					}

					if ( parm == oParm ){
						return;
					}

					setMacro( macName , parm );
				} );

				$( ".modeCausePriorityChk" ).each( function (){
					var pos = $( this ).attr( "id" ).split( "modeCausePriorityChk_" )[ 1 ];
					var prop = "priority" + pos;
					var macName = "a_mCauseCfg_" + name + "_" + prop;
					var chk = $( this ).is( ":checked" );
					var chkNum = chk ? 1 : 0;
					var oChkNum = causeAry[ prop ];

					if ( typeof( oChkNum ) == "undefined" ){
						oChkNum = 0;
					}

					if ( chk == oChkNum ){
						return;
					}
					setMacro( macName , chkNum );
				} );

				$( ".modeCauseNoticeChk" ).each( function (){
					var pos = $( this ).attr( "id" ).split( "modeCauseNoticeChk_" )[ 1 ];
					var prop = "notice" + pos;
					var macName = "a_mCauseCfg_" + name + "_" + prop;
					var chk = $( this ).is( ":checked" );
					var chkNum = chk ? 1 : 0;
					var oChkNum = causeAry[ prop ];

					if ( typeof( oChkNum ) == "undefined" ){
						oChkNum = 0;
					}

					if ( chk == oChkNum ){
						return;
					}
					setMacro( macName , chkNum );
				} );
				this.show = false;

				setTimeout( function(){
					updateModeCause( name );
				} , 500 );
			} ,
			getCauseInputValue: function ( type , pos ){
				var parm = this.causeObj[ type + pos ];
				return parm;
			} ,
			clickChgDirectionBtn: function (){
				var name = this.causeObj.name;
				var prop = "direction";
				var macName = "a_mCauseCfg_" + name + "_" + prop;

				if ( this.causeObj.direction == "down" ){
					var parm = "up";
				}else{
					var parm = "down";
				}

				setMacro( macName , parm );
			} ,
		} ,
	} );

	var vueModePanelCfg = new Vue( {
		el: ".modePanelCfgDiv" ,
		data: {
			show: false ,
		} ,
		computed: {
			sortedCause: function(){
				return vueFunction.sortedCause();
			} ,
		} ,
	} );

	var vueModeOrderCfg = new Vue( {
		el: ".modeOrderCfgSec" ,
		data: {
			defObj: gModeDefaultOrderObj ,
			orderAry: gModeOrderAry ,
			show: false ,
			spreadPos: 0 ,
			shiftPos: 0 ,
		} ,
		methods: {
			removeModeOrder: function( orderObj ){
				for ( var i = 0 ; i < gModeOrderAry.length ; i++ ){
					var obj = gModeOrderAry[ i ];
					if ( obj.shift != orderObj.shift ){
						continue;
					}
					if ( obj.spread != orderObj.spread ){
						continue;
					}
					if ( obj.bidOrAsk != orderObj.bidOrAsk ){
						continue;
					}
					if ( obj.id != orderObj.id ){
						continue;
					}

					var macName = "a_mOrderParm_" + orderObj.shift + "_" + orderObj.spread + "_" + orderObj.bidOrAsk + "_" + orderObj.id;

					deleteMacro( macName );
					break;
				}
			} ,
			addModeOrder: function( bidOrAsk , id ){
				var macName = "a_mOrderParm_" + this.shiftPos + "_" + this.spreadPos + "_" + bidOrAsk + "_" + id;
				var ordMethod = readParmAry( gConfOrderAry , "OrderMethod" ).select;
				var methodSec = readParmAry( gConfOrderAry , "OrderMethod" ).parm;
				if ( bidOrAsk == "bid" ){
					var pips = this.defObj.pips * -1;
				}else{
					var pips = this.defObj.pips;
				}
				var parmStr = this.defObj.qty + "_" + pips + "_" + this.defObj.update + "_" + this.defObj.replace + "_" + ordMethod + "_" + methodSec;

				setMacro( macName , parmStr );
			} ,
			getOrderId: function ( bidOrAsk ){
				var id = 0;
				for ( var i = 0 ; i < gModeOrderAry.length ; i++ ){
					var obj = gModeOrderAry[ i ];
					if ( obj.shift != this.shiftPos ){
						continue;
					}
					if ( obj.spread != this.spreadPos ){
						continue;
					}
					if ( obj.bidOrAsk != bidOrAsk ){
						continue;
					}
					if ( obj.id > id ){
						id = obj.id;
					}
				}
				return id + 1;
			} ,
			submitOrderParm: function(){
				var shiftPos = this.shiftPos;
				var spreadPos = this.spreadPos;
				$( ".modeOrderDiv" ).each( function (){
					var bidOrAsk = $( this ).find( ".modeOrderBidOrAskInput" ).val();
					var id = parseFloat( $( this ).find( ".modeOrderIdInput" ).val() );
					var pips = parseFloat( $( this ).find( ".modeOrderPipsInput" ).val() );
					var qty = parseFloat( $( this ).find( ".modeOrderQtyInput" ).val() );
					var update = parseFloat( $( this ).find( ".modeOrderUpdateInput" ).val() );
					var replace = parseFloat( $( this ).find( ".modeOrderReplaceInput" ).val() );
					var method = readParmAry( gConfOrderAry , "OrderMethod" ).select;
					var methodSec = readParmAry( gConfOrderAry , "OrderMethod" ).parm;

					if ( isNaN( pips ) || isNaN( qty ) || isNaN( update ) || isNaN( replace ) ){
						return;
					}

					if ( bidOrAsk == "bid" ){
						pips = pips * -1;
					}

					for ( var i = 0 ; i < gModeOrderAry.length ; i++ ){
						var obj = gModeOrderAry[ i ];
						if ( obj.shift != shiftPos ){
							continue;
						}
						if ( obj.spread != spreadPos ){
							continue;
						}
						if ( obj.bidOrAsk != bidOrAsk ){
							continue;
						}
						if ( obj.id != id ){
							continue;
						}
						var macName = "a_mOrderParm_" + shiftPos + "_" + spreadPos + "_" + bidOrAsk + "_" + id;

						var parmStr = qty + "_" + pips + "_" + update + "_" + replace + "_" + method + "_" + methodSec;

						setMacro( macName , parmStr );
					}
				} );

				this.show = false;
				setTimeout( function (){
					getCurrentOrder();
				} , 500 );
			} ,
		} ,
		computed: {
			getOrderAry: function(){
				var ary = [];
				for ( var i = 0 ; i < this.orderAry.length ; i++ ){
					var obj = this.orderAry[ i ];
					if ( obj.shift != this.shiftPos ){
						continue;
					}
					if ( obj.spread != this.spreadPos ){
						continue;
					}
					ary.push( obj );
				}

				ary = ary.sort( function( a , b ){
					if ( Math.abs( a.pips ) > Math.abs( b.pips ) ){
						return 1;
					}else if ( Math.abs( a.pips ) < Math.abs( b.pips ) ){
						return -1;
					}
					return 0;
				} );
				return ary;
			} ,
		} ,
	} );

	var vueCurrentOrder = new Vue( {
		el: ".currentOrderPanel" ,
		data: {
			bidOrderAry: gBidOrderAry ,
			askOrderAry: gAskOrderAry ,
		} ,
		methods: {
			turnOnOrder: function( orderObj , orderIndex ){
				var numStr = ( orderIndex + 1 ).toString();
				var bidOrAsk = orderObj.bidOrAsk;
				runModeOrderAction( "run" , bidOrAsk , numStr );
			} ,
			turnOffOrder: function( orderObj , orderIndex ){
				var numStr = ( orderIndex + 1 ).toString();
				var bidOrAsk = orderObj.bidOrAsk;
				runModeOrderAction( "stop" , bidOrAsk , numStr );
			} ,
		} ,
		computed: {
			getBidOrderAry: function(){
				var ary = this.bidOrderAry.sort( function( a , b ){
					if ( Math.abs( a.pips ) > Math.abs( b.pips ) ){
						return 1;
					}else if ( Math.abs( a.pips ) < Math.abs( b.pips ) ){
						return -1;
					}
					return 0;
				} );
				return ary;
			} ,
			getAskOrderAry: function(){
				var ary = this.askOrderAry.sort( function( a , b ){
					if ( Math.abs( a.pips ) > Math.abs( b.pips ) ){
						return 1;
					}else if ( Math.abs( a.pips ) < Math.abs( b.pips ) ){
						return -1;
					}
					return 0;
				} );
				return ary;
			} ,
		} ,
	} );

	var vueModeActionCfg = new Vue( {
		el: ".modeActionCfgSec" ,
		data: {
			show: false ,
			axis: "" ,
			pos: 0 ,
		} ,
		methods: {
			submitActionParm: function(){
				var axis = this.axis;
				var pos = this.pos;
				$( ".modeActionTr" ).each( function (){
					var name = $( this ).find( ".modeHiddenVal" ).val();
					var macName = "a_mAction_" + axis + "_" + pos + "_" + name;
					var chk = $( this ).find( ".modeChk" ).is( ":checked" );
					var chkNum = chk ? 1 : 0;
					var parm = $( this ).find( ".modeParm" ).val();

					if ( typeof( parm ) == "undefined" || isNaN( parseFloat( parm ) ) ){
						parm = 0;
					}

					var oChk = false;
					var oParm = false;

					for ( var i = 0 ; i < gModeActionAry.length ; i++ ){
						var obj = gModeActionAry[ i ];
						if ( obj.axis != axis ){
							continue;
						}
						if ( obj.pos != pos ){
							continue;
						}
						if ( obj.name != name ){
							continue;
						}
						oChk = obj.on;
						oParm = obj.parm;
						break;
					}

					if ( chk == oChk && parm == oParm ){
						return;
					}

					parmStr = chkNum.toString() + "_" + parm.toString();
					setMacro( macName , parmStr );
				} );

				this.show = false;
			} ,
			checkAxis: function( item ){
				return item[ "on" + this.axis ];
			} ,
		} ,
	} );

	var vueModeNone = new Vue( {
		el: ".modeNoneDiv" ,
		data: {
			axis: "None" ,
			causeAry: gModeCauseAry ,
			axisNumObj: gModeAxisNumObj ,
			show: false ,
			statusObj: gAlgoStatusObj ,
		} ,
		methods: {
			getCausePos: function ( index ){
				return vueFunction.getCausePos( index , this.axis );
			} ,
			getCauseSwitchClass: function ( obj ){
				return vueFunction.getCauseSwitchClass( obj );
			} ,
			getCauseGridClass: function ( pos , bidOrAsk , obj ){
				return vueFunction.getCauseGridClass( pos , bidOrAsk , obj );
			} ,
			clickCauseSwitch: function( obj ){
				vueFunction.clickCauseSwitch( obj );
			} ,
			clickModeTable: function( event ){
				vueFunction.clickModeTable( event );
			} ,
			clickCauseCfg: function( obj ){
				vueFunction.clickCauseCfg( obj );
			} ,
			clearCauseStatus: function( obj ){
				vueFunction.clearCauseStatus( obj );
			} ,
			openDataChart: function( obj ){
				vueFunction.openDataChart( obj );
			} ,
		} ,
		computed: {
			sortedCause: function(){
				return vueFunction.sortedCause();
			} ,
		} ,
	} );

	var vuePanl = new Vue( {
		el: ".confPanlSec" ,
		data: {
			confAry: gConfPanelAry ,
		} ,
	} );

	var vueGuard = new Vue( {
		el: ".confGuarSec" ,
		data: {
			confAry: gConfGuardAry ,
		} ,
	} );

	var vueOrder = new Vue( {
		el: ".confOrderSec" ,
		data: {
			confAry: gConfOrderAry ,
		} ,
	} );

	var vueFiveSecVolTable = new Vue( {
		el: ".fiveSecVolTableDiv" ,
		data: {
			volMsgAry: gTableMsgAry ,
		} ,
		methods: {
			getPxDirectionClass: function ( obj ){
				var tdClass = "";

				if ( obj.pxDirection == 1 ){
					tdClass = "tableTdUp";
				}else if ( obj.pxDirection == -1 ){
					tdClass = "tableTdDown";
				}else if ( obj.pxDirection == 0 ){
					tdClass = "tableTdFlat";
				}

				return tdClass;
			} ,
			getVolClass: function ( obj ){
				var tdClass = "";
				var vol = obj.vol;

				if ( obj.pxDirection == 1 ){
					tdClass = "tableTdUp";
				}else if ( obj.pxDirection == -1 ){
					tdClass = "tableTdDown";
				}else if ( obj.pxDirection == 0 ){
					tdClass = "tableTdFlat";
				}

				if ( vol >= 500 && vol < 1000 ){
					tdClass += " tableLvl1Td";
				}else if ( vol >= 1000 && vol < 2000 ){
					tdClass += " tableLvl2Td";
				}else if ( vol >= 2000 ){
					tdClass += " tableLvl3Td";
				}

				return tdClass;
			} ,
			getPipChgClass: function ( obj ){
				var tdClass = "";

				if ( obj.pxDirection == 1 ){
					tdClass = "tableTdUp";
				}else if ( obj.pxDirection == -1 ){
					tdClass = "tableTdDown";
				}else if ( obj.pxDirection == 0 ){
					tdClass = "tableTdFlat";
				}

				var chg = Math.abs( obj.pipChg );

				if ( chg >= 3 && chg < 6 ){
					tdClass += " tableLvl1Td";
				}else if ( chg >= 6 && chg < 10 ){
					tdClass += " tableLvl2Td";;
				}else if ( chg >= 10 ){
					tdClass += " tableLvl3Td";
				}
				return tdClass;
			} ,
			getFillDirectionClass: function ( obj ){
				var tdClass = "";

				if ( obj.bidVol < 10 && obj.askVol < 10 ){
					return tdClass;
				}
				if ( obj.ratio < 0.49 ){
					tdClass = "tableTdUp";
					if ( obj.ratio < 0.15 ){
						tdClass += " tableLvl1Td";
					}
				}else if ( obj.ratio > 0.51 ){
					tdClass = "tableTdDown";
					if ( obj.ratio > 0.85 ){
						tdClass += " tableLvl1Td";
					}
				}else{
					tdClass = "tableTdFlat";
				}
				return tdClass;
			} ,
			getFillRatioClass: function ( obj ){
				var tdClass = "";

				if ( obj.bidVol < 10 && obj.askVol < 10 ){
					return tdClass;
				}
				if ( obj.ratio < 0.35 ){
					tdClass = "tableTdUp";
					if ( obj.ratio < 0.15 ){
						tdClass += " tableLvl1Td";
					}
				}else if ( obj.ratio > 0.65 ){
					tdClass = "tableTdDown";
					if ( obj.ratio > 0.85 ){
						tdClass += " tableLvl1Td";
					}
				}else{
					tdClass = "tableTdFlat";
				}
				return tdClass;
			} ,
		} ,
	} );

	var vueTickVolTable = new Vue( {
		el: ".tickVolTableDiv" ,
		data: {
			volMsgAry: gTableMsgAry ,
		} ,
		methods: {
			getPxDirectionClass: function ( obj ){
				var tdClass = "";

				if ( obj.pxDirection == 1 ){
					tdClass = "tableTdUp";
				}else if ( obj.pxDirection == -1 ){
					tdClass = "tableTdDown";
				}else if ( obj.pxDirection == 0 ){
					tdClass = "tableTdFlat";
				}

				return tdClass;
			} ,
			getVolClass: function ( obj ){
				var tdClass = "";
				var vol = obj.vol;

				if ( obj.pxDirection == 1 ){
					tdClass = "tableTdUp";
				}else if ( obj.pxDirection == -1 ){
					tdClass = "tableTdDown";
				}else if ( obj.pxDirection == 0 ){
					tdClass = "tableTdFlat";
				}

				if ( vol >= 500 && vol < 1000 ){
					tdClass += " tableLvl1Td";
				}else if ( vol >= 1000 && vol < 2000 ){
					tdClass += " tableLvl2Td";
				}else if ( vol >= 2000 ){
					tdClass += " tableLvl3Td";
				}

				return tdClass;
			} ,
			getPipChgClass: function ( obj ){
				var tdClass = "";

				if ( obj.pxDirection == 1 ){
					tdClass = "tableTdUp";
				}else if ( obj.pxDirection == -1 ){
					tdClass = "tableTdDown";
				}else if ( obj.pxDirection == 0 ){
					tdClass = "tableTdFlat";
				}

				var chg = Math.abs( obj.pipChg );

				if ( chg >= 3 && chg < 6 ){
					tdClass += " tableLvl1Td";
				}else if ( chg >= 6 && chg < 10 ){
					tdClass += " tableLvl2Td";;
				}else if ( chg >= 10 ){
					tdClass += " tableLvl3Td";
				}
				return tdClass;
			} ,
			getFillDirectionClass: function ( obj ){
				var tdClass = "";

				if ( obj.bidVol < 10 && obj.askVol < 10 ){
					return tdClass;
				}
				if ( obj.ratio < 0.49 ){
					tdClass = "tableTdUp";
					if ( obj.ratio < 0.15 ){
						tdClass += " tableLvl1Td";
					}
				}else if ( obj.ratio > 0.51 ){
					tdClass = "tableTdDown";
					if ( obj.ratio > 0.85 ){
						tdClass += " tableLvl1Td";
					}
				}else{
					tdClass = "tableTdFlat";
				}
				return tdClass;
			} ,
			getFillRatioClass: function ( obj ){
				var tdClass = "";

				if ( obj.bidVol < 10 && obj.askVol < 10 ){
					return tdClass;
				}
				if ( obj.ratio < 0.35 ){
					tdClass = "tableTdUp";
					if ( obj.ratio < 0.15 ){
						tdClass += " tableLvl1Td";
					}
				}else if ( obj.ratio > 0.65 ){
					tdClass = "tableTdDown";
					if ( obj.ratio > 0.85 ){
						tdClass += " tableLvl1Td";
					}
				}else{
					tdClass = "tableTdFlat";
				}
				return tdClass;
			} ,
		} ,
	} );

	var vueOrderCountTable = new Vue( {
		el: ".orderCountTableDiv" ,
		data: {
			orderCountAry: [] ,
		} ,
	} );

	//////////// start , stop , set , delete macro function ///////////

	function getMacro(){
		var sessID = "";
		if ( gStatusObj.sessionId != 0 ){
			sessID = gStatusObj.sessionId + " ";
		}

		gRandomNum = getRandomNumber();

		var cmd = "CMD " + gInitUserID + " GET_MACRO " + sessID + "SEQ " + gStatusObj.ip + "::" + gRandomNum + "::" + gVersion;
		pushCMD( cmd );
		showLog( "getMacro" , cmd );
		//e.g. CMD 302 GET_MACRO SEQ 192.168.0.110::64::1a
	}

	function readMacro( cmrMsg ){ // handle "CMR" message
		// showLog( "readMacro" , cmrMsg ); // show all cmr msg
		gStatusObj.gotCMR = true;
		var cmrLine = cmrMsg.split( "\n" );

		for ( var i = 0 ; i < cmrLine.length ; i ++ ){
			var cmrAry = cmrLine[ i ].split( " " );
			if ( cmrAry.length < 4 || cmrAry[ 0 ] != "CMR" || cmrAry[ 1 ] != gInitUserID || gStatusObj.reloaded || gStatusObj.disconnected ){ // ignore if msg too short , not cmr , not for this user or user was requested to reload.
				return;
			}

			var msgType = cmrAry[ 0 ];
			var userID = cmrAry[ 1 ];
			var cmrType = cmrAry[ 2 ];

			if ( cmrAry[ 3 ].slice( 0 , 15 ) != "a_orderMac_loop" ){ // hide algoLp_ msg on con sole log
				showLog( "readMacro" , cmrLine[ i ] );
			}

			if ( cmrType == "MACRO" ){
				var macName = cmrAry[ 3 ];
				var macParm = cmrAry[ 5 ];
				if ( macName.split( "_" )[ 1 ] == "orderMac" ){
					countNewOrderMacro( cmrAry );
				}else if ( macName == "a_leaderID" ){
					readLeaderID( cmrAry );
				}else if ( macName == "a_maxProfit" ){
					readMaxProfit( cmrAry );
				}else if ( macName.split( "_" )[ 1 ] == "panlCfg" ){
					var name = macName.split( "_" )[ 2 ];
					var property = macName.split( "_" )[ 3 ];

					if ( property == "on" || property == "off" || property == "pause" || property == "flat" || property == "flatAll" || property == "notice" ){
						macParm = Boolean( parseInt( macParm ) );
					}

					if ( property == "parm" ){
						macParm = parseFloat( macParm );
					}

					editParmAry( gConfPanelAry , name , property , macParm );

					if ( name == "OrderMethod" && property == "parm" ){
						$( ".confRowOrderMethodDiv" ).each( function (){
							$( this ).find( ".txtIpt" ).val( macParm );
						} );
					}
				}else if ( macName.split( "_" )[ 1 ] == "guardCfg" ){
					var name = macName.split( "_" )[ 2 ];
					var property = macName.split( "_" )[ 3 ];

					if ( property == "on" || property == "off" || property == "pause" || property == "flat" || property == "flatAll" || property == "notice" ){
						macParm = Boolean( parseInt( macParm ) );
					}

					if ( property == "parm" ){
						macParm = parseFloat( macParm );
					}

					editParmAry( gConfGuardAry , name , property , macParm );
					if ( name == "LossFromTop" ){
						showLossFromTop();
					}
				}else if ( macName.split( "_" )[ 1 ] == "orderCfg" ){
					readOrderConfig( cmrAry );
				}else if ( macName.split( "_" )[ 1 ] == "mOrderParm" ){
					readModeOrderMacro( cmrAry );
				}else if ( macName.split( "_" )[ 1 ] == "mCauseCfg" ){
					readModeCauseCfg( cmrAry );
				}else if ( macName.split( "_" )[ 1 ] == "mAxisNum" ){
					var axis = macName.split( "_" )[ 2 ];
					gModeAxisNumObj[ axis ] = parseFloat( macParm );
				}else if ( macName.split( "_" )[ 1 ] == "mAction" ){
					readModeActionMacro( cmrAry );
				}else if ( macName.split( "_" )[ 1 ] == "mPosition" ){
					readModePosition( cmrAry );
				}

			}else if ( cmrType == "MACRO_DELETE" ){
				var macName = cmrAry[ 3 ];
				var macParm = cmrAry[ 5 ];
				if ( macName.split( "_" )[ 1 ] == "mCauseCfg" ){
					var name = macName.split( "_" )[ 2 ];
					var property = macName.split( "_" )[ 3 ];

					editParmAry( gModeCauseAry , name , property , "" );
				}else if ( macName.split( "_" )[ 1 ] == "mOrderParm" ){
					readDeleteModeOrderMacro( cmrAry );
				}else if ( macName.split( "_" )[ 1 ] == "orderMac" ){
					countRemoveOrderMacro( cmrAry );
				}

			}else if ( cmrType == "DO" ){
				cmrAction = cmrAry[ 3 ];
				if ( cmrAction == "_STOPALL" ){
					turnnedOffPanel();
				}else if ( cmrAction.split( "_" )[ 0 ] == "orderMac" ){
					// readModeOrderAction( cmrAry );
				}

			}else if ( cmrType == "ALGO-ON" || cmrType == "ALGO-OFF" ){
				readModeOrderStatus( cmrAry );

			}else if ( cmrType == "MSG" ){
				var msgTopic = cmrAry[ 3 ];

				if ( msgTopic == "MMNOTICE" ){
					readNotice( cmrAry );
				}else if ( msgTopic == "ALGO" ){
					readPlacedPx( cmrAry );
				}else if ( msgTopic == "a_tabMsg" ){
					readMsg( cmrAry );
				}else if ( msgTopic == "a_sendSessId" ){
					collectSessID( cmrAry );
				}else if ( msgTopic == "a_getSessID" ){
					sendSessID();
				}else if ( msgTopic == "a_callLeader" ){
					replyLeaderID( cmrAry );
				}else if ( msgTopic == "a_leaderOn" ){
					readLeaderOnMsg( cmrAry );
				}else if ( msgTopic == "a_multipleLeader" ){
					getNewSessID( cmrAry );
				}else if ( msgTopic == "a_toastrMsg" ){
					readToastrMsg( cmrAry );
				}else if ( msgTopic == "a_soundMsg" ){
					readSoundMsg( cmrAry );
				}else if ( msgTopic == "a_hotkey" ){
					readHotkeyMsg( cmrAry );
				}else if ( msgTopic == "LOG" ){
					var logTopic = cmrAry[ 4 ];
					if ( logTopic == "a_tableMsg" ){
						readTableMsg( cmrAry , 0 );
					}
				}

			}else if ( cmrType == "TOTAL_FILL" ){
				readTotalFill( cmrAry );

			}else if ( cmrType == "GET_MACRO" ){
				getSessID( cmrAry );
				initPanel( cmrAry );

			}else if ( cmrType == "ALGO-RUNNING" ){
				readAlgoRunning( cmrAry );

			}else if ( cmrType == "GET" ){
				readParameter( cmrAry );
			}
		}
	}

	function setMacro( macName , parm ){
		if ( jQuery.type( parm ) == "array" ){
			var parmStr = "{ ";
			for ( var i = 0 ; i < parm.length ; i ++ ){
				if ( i > 0 ){
					parmStr += " ";
				}
				parmStr += parm[ i ];
				if ( ! validateParameter( macName , parm[ i ] , parm[ i ] ) ){
					throw new Error( "Parameter is NaN or undefined." );
				}
			}
			parmStr = parmStr + " }";
		}else{
			var parmStr = parm;

			if ( ! validateParameter( macName , parm , parm ) ){
				throw new Error( "Parameter is NaN or undefined." );
			}
		}

		getSeqNum( gByFunction );
		var cmd = "CMD " + gInitUserID + " MACRO " + macName + " NAME " + parmStr;
		//var cmd = "CMD " + gInitUserID + " MACRO " + macName + " NAME " + parmStr + " SEQ " + gStatusObj.seqNum;

		pushCMD( cmd );
		showLog( "setMacro" , cmd );
	}

	function startMacro( macName ){
		getSeqNum( gByManual );
		var cmd = "CMD " + gInitUserID + " DO " + macName + " SEQ " + gStatusObj.seqNum;

		pushCMD( cmd );
		showLog( "startMacro" , cmd );
	}

	function stopMacro( cnlName ){
		getSeqNum( gByManual );
		var cmd = "CMD " + gInitUserID + " DO " + cnlName + " SEQ " + gStatusObj.seqNum;

		pushCMD( cmd );
		showLog( "stopMacro" , cmd );
	}

	function deleteMacro( name ){
		//var cmd = "CMD " + gInitUserID + " MACRO_DELETE " + name + " SEQ " + gStatusObj.seqNum;
		var cmd = "CMD " + gInitUserID + " MACRO_DELETE " + name;

		pushCMD( cmd );
		showLog( "deleteMacro" , cmd );
	}

	function turnOnPanel(){ // turn on algo
		try{
			setParameter( "a_status_on" , 1 );

			var msg = "Trun On Algo. Seq No.: " + gStatusObj.seqNum + ".";
			sendTabMsg( msg );

			turnOnAllCurrentOrder();

		}catch ( e ){ // show error when turnning on panel button
			showParmErrorNotice( 2 );
			showLog( "catch error" , e );
		}
	}

	function turnOffPanel(){
		startStopAll( gByManual );

		setParameter( "a_status_on" , 0 );
		gAlgoStatusObj.on = false;

		var msg = "Trun Off Algo. Seq No.: " + gStatusObj.seqNum + ".";
		sendTabMsg( msg );
	}

	function turnnedOffPanel(){
		clearTimeout( gTimerObj.delayTurnOn );
		trunOffAllMacroSwitch();
		clearModeCaseStatus( "TradeTimer" );
		turnOffAllModeOrderSwitch();
	}

	function trunPanelSwitch( parm ){ // switch on or off the Big on off switch
		var panlSwBg = $( ".panlSwDiv" ).find( ".panlSwBg" );
		var swOnLbl = $( ".panlSwDiv" ).find( ".swOnLbl" );
		var swOffLbl = $( ".panlSwDiv" ).find( ".swOffLbl" )

		$( panlSwBg ).stop( true , true );
		if ( parm ){
			if ( $( swOnLbl ).hasClass( "panlSwChked" ) ){
				return;
			}
			$( panlSwBg ).css( { "left":"-240px" , "width":"33.33%" } );
			$( swOnLbl ).addClass( "panlSwChked" , 300 );
			$( swOffLbl ).removeClass( "panlSwChked" , 300 );

			onPanelTimer();

		}else{
			if ( $( swOffLbl ).hasClass( "panlSwChked" ) ){
				return;
			}
			$( panlSwBg ).css( { "left":"-160px" , "width":"66.66%" } );
			$( swOnLbl ).removeClass( "panlSwChked" , 300 );
			$( swOffLbl ).addClass( "panlSwChked" , 300 );
		}
	}

	function pauseAlgo( rMsg ){
		startStopAll( gByFunction );
		setParameter( "a_status_pause" , 1 );
		var msg = "Pause Algo. " + rMsg;
		sendTabMsg( msg );
	}

	function resumeAlgo(){
		setParameter( "a_status_on" , 1 );
		setParameter( "a_status_pause" , 0 );

		var msg = "Resume Algo. ";
		sendTabMsg( msg );
	}

	function trunOffAllMacroSwitch(){
		if ( gAlgoStatusObj.on ){
			setParameter( "a_status_on" , 0 );
		}

		$( ".panlStopNtcSec " ).fadeOut();
		hideBgAlgoCount();
	}

	function setStopAll(){ // to cancel all orders and cancel all running algos
		var cnl = "O MREX CO DAY CANCEL ALL" + gLOType + " | O PATS CO DAY CANCEL ALL" + gLOType + " | O PATX CO DAY CANCEL ALL" + gLOType + " | O HKE CO DAY CANCEL ALL" + gLOType + " | O MREX CO DAY CANCEL ALL" + gSOType + " | O PATS CO DAY CANCEL ALL" + gSOType + " | O PATX CO DAY CANCEL ALL" + gSOType + "| O HKE CO DAY CANCEL ALL" + gSOType;

		var cmd = "CMD " + gInitUserID + " MACRO _STOPALL NAME _STOPALL CANCEL_ALGO ALL | " + cnl +
					" | DELAY 0 1000 { " + cnl + " }";

		if ( gInitUserID == 528 || gInitUserID == 529 ){
			cmd = "CMD " + gInitUserID + " MACRO _STOPALL NAME _STOPALL CANCEL_ALGO ALL"; // dont cancel dicky's orders when turn off algo
		}

		pushCMD( cmd );
		showLog( "setStopAll" , cmd );

		// e.g. CMD 302 MACRO _STOPALL NAME _STOPALL CANCEL_ALGO ALL | O MREX CO DAY CANCEL ALLLO2 | O PATS CO DAY CANCEL ALLLO2 | O HKE CO DAY CANCEL ALLLO2 | O MREX CO DAY CANCEL ALLSO2 | O PATS CO DAY CANCEL ALLSO2 | O HKE CO DAY CANCEL ALLSO2 | MSG 302 a_tabMsg { Stopped All Algo. } | DELAY 0 1000 { O MREX CO DAY CANCEL ALLLO2 | O PATS CO DAY CANCEL ALLLO2 | O HKE CO DAY CANCEL ALLLO2 | O MREX CO DAY CANCEL ALLSO2 | O PATS CO DAY CANCEL ALLSO2 | O HKE CO DAY CANCEL ALLSO2 }
	}

	function startStopAll( type ){ // call STOPALL macro
		$( ".panlStopNtcSec" ).stop( true , true );
		$( ".panlStopNtcSec" ).fadeIn();

		getSeqNum( type );

		var cmd = "CMD " + gInitUserID + " DO _STOPALL" + " SEQ " + gStatusObj.seqNum;

		pushCMD( cmd );
		showLog( "startStopAll" , cmd );
	}

	function deleteRow( cmrAry ){ // delete the row from UI
		// e.g. CMR 302 MACRO_DELETE algoMac_07_BUY
		var numStr = cmrAry[ 3 ].split( "_" )[ 1 ];
		var rowID = "#macRow_" + numStr;

		$( rowID ).fadeOut( 300 , function (){
			$( this ).remove();
		} );
	}

	/////////////// panel timer function ////////////

	function onPanelTimer(){ // check open order , pnl , netPos and mode every second
		resetLongInterval( true );
		resetShortInterval();
		resetMiddleInterval();
	}

	function shortPanelInterval(){
		lossFromTopGuardian();
		netPosGuardian();
		maxLossGuardian();
		saveMaxProfit();
		getDailyOrderCount();
		updateModeNetPosition();
		updateModeAbsPosition();
		updateModeOppAbsPosition();
		updateModeManualAdject();
		updateModeTradeTimer();
	}

	function middlePanelInterval(){
		getMinuteOhlc();
	}

	function longPanelInterval(){
		checkIsLeader();
	}

	function resetShortInterval(){
		clearInterval( gIntervalObj.panelShort ); // stop panel timer

		var interval = 1000;
		var delay = Math.random() * 1000; // 0 - 1 second

		setTimeout( function (){
			clearInterval( gIntervalObj.panelShort );
			gIntervalObj.panelShort = false;

			shortPanelInterval();
			gIntervalObj.panelShort = setInterval( shortPanelInterval , interval );
		} , delay );
	}

	function resetMiddleInterval(){
		clearInterval( gIntervalObj.panelMiddle ); // stop panel timer

		var interval = 30000;
		var delay = Math.random() * 1000; // 0 - 1 second

		setTimeout( function (){
			clearInterval( gIntervalObj.panelMiddle );
			gIntervalObj.panelMiddle = false;

			middlePanelInterval();
			gIntervalObj.panelMiddle = setInterval( middlePanelInterval , interval );
		} , delay );
	}

	function resetLongInterval( runNow ){
		clearInterval( gIntervalObj.panelLong );

		var interval = 60000;
		var delay = 3000 + Math.random() * 5000; // 3 - 8 second

		setTimeout( function (){
			clearInterval( gIntervalObj.panelLong );
			gIntervalObj.panelLong = false;

			if ( runNow ){
				longPanelInterval();
			}
			gIntervalObj.panelLong = setInterval( longPanelInterval , interval );
		} , delay );
	}

	/////////////// algo guardian function ////////////

	function triggerGuardian( type , reason ){
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){
			return;
		}

		var cfg = readParmAry( gConfGuardAry , type );

		var pauseTime = readParmAry( gConfGuardAry , "PauseTime" ).parm;
		var resumeVol = readParmAry( gConfGuardAry , "NmlSixtySecVol" ).parm;
		var algoOffFlat = readParmAry( gConfGuardAry , "AlgoOffFlat" ).on;
		var inFavorFlat = readParmAry( gConfGuardAry , "inFavorFlat" ).on;
		var flatQty = readParmAry( gConfGuardAry , "AutoFlatQty" ).parm;

		var doOff = guardianOff();
		var doPause = guardianPause();
		var doFlatAll = guardianFlatAll();
		var doFlat = guardianFlat();
		var doResume = guardianResume();
		var doNotice = guardianNotice();

		if ( doOff || doPause || doFlat || doFlatAll || doResume || doNotice ){
			editParmAry( gConfGuardAry , type , "triggered" , true );

			$.when( checkIsLeader() ).then( function (){
				if ( !gLeaderObj.isLeader ){
					return;
				}
				sendMsg( gInitUserID , "MMNOTICE" , "GUARD_" + type , "" );

				if ( doNotice ){
					var msg = "Triggered " + cfg.title + " Guardian. " + reason;
					var toastrTopic = "AlgoGuardian:";

					if ( doOff || doFlat || doFlatAll ){
						var toastrType = "error";
						var soundType = "alert";
						msg = msg + gIconObj.doubleExcMark + gIconObj.doubleExcMark + gIconObj.doubleExcMark + gIconObj.doubleExcMark + gIconObj.doubleExcMark;
					}else{
						var toastrType = "warning";
						var soundType = "notice";
					}

					if ( type != "MaxFiveSecVol" ){
						sendTabMsg( msg );
						sendToastrMsg( toastrType , toastrTopic , msg );
						sendSoundMsg( soundType );
					}
				}

				if ( type == "LossFromTop" ){
					renewMaxProfit();
				}

				if ( doOff ){
					startStopAll( gByFunction );
				}

				if ( doPause ){
					clearTimeout( gTimerObj.pausePanel );

					if ( cfg.autoResume ){
						var msg = "Resume Algo After " + pauseTime + " Second. ";
					}else if ( type == "MaxSixtySecVol" ){
						var msg = "Resume Algo When 60 Second Volume Drop to " + resumeVol + "Lots. "
					}else{
						var msg = "";
					}

					pauseAlgo( msg );
				}

				if ( doFlat || doFlatAll ){
					if ( doFlat ){
						var flatType = "flat";
					}else{
						var flatType = "flatAll";
						flatQty = 100;
					}
					clearTimeout( gTimerObj.flatPosition );

					var delay = 0;
					var paused = false;

					if ( !doOff && !doPause && algoOffFlat && type != "LossFromTop" && type != "MaxFiveSecVol" && type != "WideMASpread" ){ // LossFromTop will reset max profit
						editParmAry( gConfGuardAry , type , flatType , false );

						var parmName = "a_guardCfg_" + type + "_" + flatType;
						setMacro( parmName , 0 );
					}

					if ( !doOff && !doPause && gAlgoStatusObj.on ){
						pauseAlgo( "" );

						delay = 500;
						paused = true;

					}else{
						delay = 500;
					}

					gTimerObj.flatPosition = setTimeout( function (){ // wait 0.9 second to make sure leader is also triggered this guardian
						squarePosition( flatQty , gDefAutoFlatSlip );

						if ( paused ){
							setTimeout( function (){
								resumeAlgo();
							} , delay );
						}
					} , delay );
				}
			} );
		}
		if ( doResume ){
			clearTimeout( gTimerObj.pausePanel );

			if ( type == "NmlSixtySecVol" ){
				var resumeReason = "60 Second Volume Drop to " + resumeVol + ". ";
				var pauseDelay = 0;
			}else{
				var resumeReason = "Passed " + pauseTime + " Second. ";
				var pauseDelay = pauseTime * 1000;
			}

			if ( cfg.autoResume ){
				var resumeReason = "Passed " + pauseTime + " Second. ";
				var pauseDelay = pauseTime * 1000;
			}else if ( type == "NmlSixtySecVol" ){
				var resumeReason = "60 Second Volume Drop to " + resumeVol + ". ";
				var pauseDelay = 0;
			}else{
				var resumeReason = "";
				var pauseDelay = 0;
			}

			gTimerObj.pausePanel = setTimeout( function (){
				if ( gAlgoStatusObj.on ){
					return;
				}

				if ( !gAlgoStatusObj.pause ){
					return;
				}

				$.when( checkIsLeader() ).then( function (){
					if ( !gLeaderObj.isLeader ){
						return;
					}

					resumeAlgo();

					var msg = resumeReason + "Resume Algo by " + cfg.title + " Guardian. ";

					var toastrType = "success";
					var toastrTopic = "AlgoStatus:";
					var soundType = "notice";

					sendTabMsg( msg );
					sendToastrMsg( toastrType , toastrTopic , msg );
					sendSoundMsg( soundType );
				} );
			} , pauseDelay );
		}

		function guardianOff(){
			if ( !cfg.off ){
				return false;
			}

			if ( !gAlgoStatusObj.on ){
				return false;
			}

			return true;
		}

		function guardianPause(){
			if ( cfg.off ){
				return false;
			}

			if ( !cfg.pause ){
				return false;
			}

			if ( !gAlgoStatusObj.on ){ // // check the big on off switch is on or not again
				return false;
			}

			if ( gAlgoStatusObj.pause ){
				return false;
			}

			return true;
		}

		function guardianFlat(){
			if ( cfg.flatAll ){
				return false;
			}
			if ( !cfg.flat ){
				return false;
			}
			if ( flatQty == 0 ){
				return false;
			}

			var ecn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
			var sym = readParmAry( gConfOrderAry , "TradeSym" ).select;
			var netPos = getSymNetPos( ecn , sym ).netPos;
			if ( netPos == 0 || ! $.isNumeric( netPos ) ){
				return false;
			}

			if ( type == "MaxFiveSecVol" && inFavorFlat ){
				var pxIcon = reason.split( " " )[ 47 ].slice( 0 , 1 );
				if ( pxIcon == gIconObj.up && netPos > 0 ){
					return;
				}
				if ( pxIcon == gIconObj.down && netPos < 0 ){
					return;
				}
			}

			if ( !algoOffFlat && !doOff && !doPause && !gAlgoStatusObj.on ){
				return false;
			}

			return true;
		}

		function guardianResume(){
			if ( doPause && cfg.autoResume ){
				return true;
			}else if ( cfg.on ){
				return true;
			}else{
				return false;
			}
		}

		function guardianNotice(){
			if ( !cfg.notice ){
				return false;
			}
			if ( !gAlgoStatusObj.on && !doPause && !doOff && !doFlat && !doFlatAll && !doResume ){
				return false;
			}
			return true;
		}

		function guardianFlatAll(){
			if ( !cfg.flatAll ){
				return false;
			}

			if ( flatQty == 0 ){
				return false;
			}

			var ecn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
			var sym = readParmAry( gConfOrderAry , "TradeSym" ).select;
			var netPos = getSymNetPos( ecn , sym ).netPos;

			if ( netPos == 0 || ! $.isNumeric( netPos ) ){
				return false;
			}

			if ( !algoOffFlat && !doOff && !doPause && !gAlgoStatusObj.on ){
				return false;
			}

			return true;
		}
	}

	function maxLossGuardian(){ // guard maximum loss
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){ // check if the panel is reloaded/disconnected or not
			return;
		}

		var type = "MaxLoss";
		var cfg = readParmAry( gConfGuardAry , type );

		if ( !cfg.pause && !cfg.off && !cfg.flat && !cfg.flatAll && !cfg.notice ){
			return;
		}

		var pnl = getAccPnL(); // get current PnL

		if ( cfg.triggered ){
			if ( pnl > cfg.parm ){
				editParmAry( gConfGuardAry , type , "triggered" , false );
			}
			return;
		}

		if ( pnl >= cfg.parm ){
			return;
		}

		var reason = "Exceed Maximum Loss Limit. Current PnL: " + pnl + ". Maximum Loss Limit: " + cfg.parm + ". ";

		triggerGuardian( type , reason );
	}

	function lossFromTopGuardian(){ // check account daily loss from top. e.g. 1000 > 4000 > 2000 = loss from top 2000
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){ // check if the panel is reloaded/disconnected or not
			return;
		}

		var type = "LossFromTop";
		var cfg = readParmAry( gConfGuardAry , type );

		if ( !cfg.pause && !cfg.off && !cfg.flat && !cfg.flatAll && !cfg.notice ){
			return;
		}

		var pnl = getAccPnL(); // get current PnL
		var lossFromTop = ( pnl - gMaxProfit ).toFixed();

		if ( cfg.triggered ){
			if ( lossFromTop >= cfg.parm ){
				editParmAry( gConfGuardAry , type , "triggered" , false );
			}
			return;
		}

		if ( lossFromTop >= cfg.parm ){
			return;
		}

		var reason = "Exceed Maximum Loss From Top Limit. Loss From Top: " + lossFromTop + ". Maximum Loss From Top Limit: " + cfg.parm + ". ";

		triggerGuardian( type , reason );
	}

	function netPosGuardian(){ // guard net position
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){ // check if the panel is reloaded/disconnected or not
			return;
		}

		var type = "NetPos";
		var cfg = readParmAry( gConfGuardAry , type );

		if ( !cfg.pause && !cfg.off && !cfg.flat && !cfg.flatAll && !cfg.notice ){
			return;
		}

		var ecn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
		var sym = readParmAry( gConfOrderAry , "TradeSym" ).select;
		var netPos = getSymNetPos( ecn , sym ).netPos;

		if ( netPos == 0 || ! $.isNumeric( netPos ) ){
			return;
		}

		if ( cfg.triggered ){
			if ( Math.abs( netPos ) <= cfg.parm ){
				editParmAry( gConfGuardAry , type , "triggered" , false );
			}
			return;
		}

		if ( Math.abs( netPos ) <= cfg.parm ){
			return;
		}

		var reason = "Exceed Net Position Limit On " + ecn + " " + sym + ". Net Position: " + netPos + " . Net Position Limit: " + cfg.parm + ". ";

		triggerGuardian( type , reason );
	}

	function slowFedGuardian( cmrAry ){ // guard slow feed
		// e.g. CMR 300 TOTAL_FILL MREX CME/CMX_GLD/AUG17 15 46 SELL 1 1246.7 OPX 1246.4
		// e.g. CMR 302 TOTAL_FILL MREX CME/CMX_GLD/DEC17 13 9 BUY 1 1268.5 OPX UNKNOWN
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){ // check if the panel is reloaded/disconnected or not
			return;
		}

		var type = "SlowFeed";
		var cfg = readParmAry( gConfGuardAry , type );

		if ( !cfg.pause && !cfg.off && !cfg.flat && !cfg.flatAll && !cfg.notice ){
			return;
		}

		if ( cfg.triggered ){
			return;
		}

		if ( cmrAry.length < 11 ){
			return;
		}
		if ( cmrAry[ 11 ] == "UNKNOWN" || cmrAry[ 11 ] == "NIL" ){
			return; // manual hit
		}

		if ( cmrAry[ 13 ] == "algoFlat" ){
			return; // cut loss order
		}

		var ecn = cmrAry[ 3 ];
		var sym = cmrAry[ 4 ];
		var side = cmrAry[ 7 ];
		var fillPx = parseFloat( cmrAry[ 9 ] );
		var ordPx = parseFloat( cmrAry[ 11 ] );
		var dip = parseFloat( getSymItem( ecn , sym ).dip );

		var ts = parseFloat( getSymItem( ecn , sym ).ts ); // get tick size

		if ( isNaN( fillPx ) || isNaN( ordPx ) || isNaN( ts ) || isNaN( dip ) ){
			var msg = "Error On Reading Filled Price Msg. filled px : " + fillPx + " . order px : " + ordPx + " . tick size : " + ts + " . Decimal Places : " + dip;
			sendLogMsg( msg );
			return;
		}

		fillPx = fillPx.toFixed( dip );
		ordPx = ordPx.toFixed( dip );

		if ( side == "BUY" ){
			if ( fillPx >= ordPx ){
				return;
			}
		}else{
			if ( fillPx <= ordPx ){
				return;
			}
		}

		if ( gGuaSlowFedCount == 0 ){
			gGuaSlowFedCount += 1;

			clearTimeout( gTimerObj.guaSlowFed );
			gTimerObj.guaSlowFed = setTimeout( function (){
				gGuaSlowFedCount = 0;
				editParmAry( gConfGuardAry , type , "triggered" , false );
			} , 60000 );

			return;
		}

		var reason = "Slow Price Feed. Better Filled " + side + " " + ecn + " " + sym + " Order Twice in One Minute. Last Order Price: " + ordPx + ". Last Filled Price: " + fillPx + ". ";

		triggerGuardian( type , reason );
	}

	function twiceFillGuardian( msg ){
		// e.g. BUY Twice in 1 minute , Same Algo FullyFilled > 1
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){ // check if the panel is reloaded/disconnected or not
			return;
		}

		var type = "FilledTwice";
		var cfg = readParmAry( gConfGuardAry , type );

		if ( !cfg.pause && !cfg.off && !cfg.flat && !cfg.flatAll && !cfg.notice ){
			return;
		}

		var side = msg.split( " " )[ 0 ];
		if ( side != "BUY" && side != "SELL" ){
			return;
		}

		var reason = "Filled " + side + " Order Twice Within One Minute. ";

		triggerGuardian( type , reason );
	}

	function sixtySecVolGuardian( msg ){
		//e.g. Volume Update: CME/CMX_GLD/DEC17 2003
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){ // check if the panel is reloaded/disconnected or not
			return;
		}

		var type = "MaxSixtySecVol";
		var cfg = readParmAry( gConfGuardAry , type );

		if ( !cfg.pause && !cfg.off && !cfg.flat && !cfg.flatAll && !cfg.notice ){
			return;
		}

		var msgAry = msg.split( " " );

		if ( msgAry.length < 4 ){
			return;
		}

		var sym = msgAry[ 2 ];
		var vol = msgAry[ 3 ];

		var rEcn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
		var rSym = readParmAry( gConfOrderAry , "TradeSym" ).select;

		if ( sym != rSym ){
			return;
		}

		if ( cfg.triggered ){
			return;
		}

		if ( vol < cfg.parm ){
			return
		}

		var reason = "Exceed Sixty Second Volume Limit On " + sym + ". Sixty Second Volume: " + vol + "Lots. Volume Limit: " + cfg.parm + "Lots. ";

		triggerGuardian( type , reason );
	}

	function resumeVolumeGuardian( msg ){
		if ( gAlgoStatusObj.on || !gAlgoStatusObj.pause ){ // check the big on off switch is on or not
			return;
		}

		if ( gStatusObj.reloaded || gStatusObj.disconnected ){ // check if the panel is reloaded/disconnected or not
			return;
		}
		var type = "NmlSixtySecVol";
		var cfg = readParmAry( gConfGuardAry , type );

		var triggerType = "MaxSixtySecVol";
		var triggered = readParmAry( gConfGuardAry , triggerType ).triggered;

		if ( !cfg.on ){
			return;
		}

		if ( !triggered ){
			return;
		}

		var msgAry = msg.split( " " );

		if ( msgAry.length < 4 ){
			return;
		}

		var sym = msgAry[ 2 ];
		var vol = msgAry[ 3 ];

		var rEcn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
		var rSym = readParmAry( gConfOrderAry , "TradeSym" ).select;

		if ( sym != rSym ){
			return;
		}

		if ( vol > cfg.parm ){
			return;
		}

		editParmAry( gConfGuardAry , type , triggerType , false );

		var reason = "Sixty Second Volume Dropped to " + vol + "Lots On " + sym + ". ";

		triggerGuardian( type , reason );
	}

	function fiveSecVolGuardian(){
		var type = "MaxFiveSecVol";
		var cfg = readParmAry( gConfGuardAry , type );
		var ary = gMAQuoteAry.slice( 0 , gFiveSecondTick );
		var obj = ary[ 0 ];
		var preObj = ary[ gFiveSecondTick - 1 ];

		if ( typeof obj == "undefined" ){
			return;
		}
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){ // check if the panel is reloaded/disconnected or not
			return;
		}
		if ( !cfg.pause && !cfg.off && !cfg.flat && !cfg.flatAll && !cfg.notice ){
			return;
		}
		if ( ary.length < gFiveSecondTick ){
			return;
		}

		if ( ary[ 0 ].totalVol == 0 || ary[ gFiveSecondTick - 1 ].totalVol == 0 ){
			return;
		}

		var sec5Vol = ary[ 0 ].totalVol - ary[ gFiveSecondTick - 1 ].totalVol;

		if ( cfg.triggered ){
			if ( sec5Vol < ( cfg.parm / 2 ) ){
				editParmAry( gConfGuardAry , type , "triggered" , false );
			}
			return;
		}

		if ( sec5Vol < cfg.parm ){
			return
		}

		var pxIcon = gIconObj.minus;
		var tradeIcon = gIconObj.minus;
		var warnningIcon = "";

		var mid = parseFloat( ( ( obj.bid + obj.ask ) / 2 ).toFixed( obj.dip + 1 ) );
		var preMid = parseFloat( ( ( preObj.bid + preObj.ask ) / 2 ).toFixed( preObj.dip + 1 ) );

		if ( mid > preMid ){
			pxIcon = gIconObj.up;
		}else if ( mid < preMid ){
			pxIcon = gIconObj.down;
		}

		var rlbidVol = ary.reduce( sumRlBidVol ).rlBidVol;
		var rlAskVol = ary.reduce( sumRlAskVol ).rlAskVol;

		if ( rlbidVol < rlAskVol ){
			tradeIcon = gIconObj.up;
		}else if ( rlbidVol > rlAskVol ){
			tradeIcon = gIconObj.down;
		}

		if ( ( pxIcon == gIconObj.up && tradeIcon == gIconObj.down ) || ( pxIcon == gIconObj.down && tradeIcon == gIconObj.up ) ){
			warnningIcon = gIconObj.warnning;
		}

		var reason = "Exceed Five Second Volume Limit On " + obj.ecn + " " + obj.sym + ". Five Second Volume: " + sec5Vol + "Lots. Volume Limit: " + cfg.parm + "Lots. Bid Px: " + obj.bid + ". Ask Px: " + obj.ask + ". Previous Bid: " + preObj.bid + ". Previous Ask: " + preObj.ask + ". Filled on Bid: " + rlbidVol + ". Filled on Ask: " + rlAskVol + ". Px Direction: " + pxIcon + pxIcon + ". Trade Direction: " + tradeIcon + tradeIcon + ". " + warnningIcon + warnningIcon;

		triggerGuardian( type , reason );

		var maxVol = 0;
		var maxVolPx = 0;
		for ( var i = 0 ; i < ( gFiveSecondTick - 1 ) ; i++ ){
			var tickVol = ary[ i ].totalVol - ary[ i + 1 ].totalVol
			if ( tickVol > maxVol ){
				maxVol = tickVol;
				maxVolPx = ( ( ary[ i ].bid + ary[ i ].ask ) / 2 ).toFixed( obj.dip + 1 );
			}
		}

		sendTableMsg( "sec5Vol" , mid , preMid , sec5Vol , rlbidVol , rlAskVol , maxVol , maxVolPx );

		checkTickVolume( cfg.parm );

		function sumRlBidVol( a , b ){
			return { rlBidVol: a.rlBidVol + b.rlBidVol };
		}
		function sumRlAskVol( a , b ){
			return { rlAskVol: a.rlAskVol + b.rlAskVol };
		}
	}

	function wideMASpreadGuardian(){
		var type = "WideMASpread";
		var cfg = readParmAry( gConfGuardAry , type );
		var obj = gMAQuoteAry[ 0 ];
		var preObj = gMAQuoteAry[ gFiveSecondTick - 1 ];

		if ( typeof obj == "undefined" ){
			return;
		}
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){ // check if the panel is reloaded/disconnected or not
			return;
		}
		if ( !cfg.pause && !cfg.off && !cfg.flat && !cfg.flatAll && !cfg.notice ){
			return;
		}
		if ( gMAQuoteAry.length < gFiveSecondTick ){
			return;
		}

		if ( cfg.triggered ){
			if ( obj.maSpreadPip < ( cfg.parm / 2 ) ){
				editParmAry( gConfGuardAry , type , "triggered" , false );
			}
			return;
		}

		if ( obj.maSpreadPip < cfg.parm ){
			return;
		}

		if ( gStatusObj.gotWrongQuote ){
			return;
		}

		var sec5Vol = gMAQuoteAry[ 0 ].totalVol - gMAQuoteAry[ gFiveSecondTick - 1 ].totalVol;
		var sec5BidVol = 0;
		var sec5AskVol = 0;
		var sec5RtsBidVol = 0;
		var sec5RtsAskVol = 0;

		for ( var i = 0 ; i < ( gFiveSecondTick - 1 ) ; i++ ){
			sec5BidVol += gMAQuoteAry[ i ].bidVol;
			sec5AskVol += gMAQuoteAry[ i ].askVol;
			sec5RtsBidVol += gMAQuoteAry[ i ].rtsBidVol;
			sec5RtsAskVol += gMAQuoteAry[ i ].rtsAskVol;
		}

		var mid = ( ( obj.bid + obj.ask ) / 2 ).toFixed( obj.dip + 1 );
		var preMid = ( ( preObj.bid + preObj.ask ) / 2 ).toFixed( preObj.dip + 1 );

		var pxIcon = gIconObj.minus;
		var tradeIcon = gIconObj.minus;
		var warnningIcon = "";

		if ( mid > preMid ){
			pxIcon = gIconObj.up;
		}else if ( mid < preMid ){
			pxIcon = gIconObj.down;
		}

		if ( sec5BidVol < sec5AskVol ){
			tradeIcon = gIconObj.up;
		}else if ( sec5BidVol > sec5AskVol ){
			tradeIcon = gIconObj.down;
		}

		if ( ( pxIcon == gIconObj.up && tradeIcon == gIconObj.down ) || ( pxIcon == gIconObj.down && tradeIcon == gIconObj.up ) ){
			warnningIcon = gIconObj.warnning;
		}

		var reason = "Exceed MA Spread Limit on " + obj.ecn + " " + obj.sym + ". MA Spread: " + obj.maSpreadPip + ". MA Spread Limit: " + cfg.parm + ". MA Bid Px: " + obj.maBid + ". MA Ask Px: " + obj.maAsk + ". Bid Px: " + obj.bid + ". Ask Px: " + obj.ask + ". Previous Bid: " + preObj.bid + ". Previous Ask: " + preObj.ask + ". Filled on Bid: " + sec5BidVol + ". Filled on Ask: " + sec5AskVol + ". Filled on Reuters Bid: " + sec5RtsBidVol + ". Filled on Reuters Ask: " + sec5RtsAskVol + ". Px Direction: " + pxIcon + pxIcon + ". Trade Direction: " + tradeIcon + tradeIcon + ". " + warnningIcon + warnningIcon;

		triggerGuardian( type , reason );
	}

	function marketDepthGuardian(){
		var type = "MinMarketDepth";
		var cfg = readParmAry( gConfGuardAry , type );
		var obj = gQuoteAry[ 0 ];

		if ( !cfg.off && !cfg.pause && !cfg.flat && !cfg.flatAll && !cfg.notice ){
			return;
		}
		if ( typeof obj == "undefined" ){
			return;
		}
		if ( cfg.triggered ){
			return;
		}

		if ( obj.totalBidLiq == obj.bidLiq ){
			return;
		}

		if ( obj.totalAskLiq == obj.askLiq ){
			return;
		}

		if ( obj.totalBidLiq > cfg.parm && obj.totalAskLiq > cfg.parm ){
			gGuaMarketDepthCount = 0;
			editParmAry( gConfGuardAry , type , "triggered" , false );
			return;
		}

		if ( gGuaMarketDepthCount < 5 ){
			gGuaMarketDepthCount ++;
			return;
		}

		var reason = "Thin Market Depth on " + obj.ecn + " " + obj.sym + ". Liquidity Limit: " + cfg.parm + ". Bid Liquidity: " + obj.totalBidLiq + ". Ask Liquidity: " + obj.totalAskLiq + ". ";

		triggerGuardian( type , reason );
	}

	function dailyOrderGuardian( ecn , sym , num ){
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){
			return;
		}

		var type = "MaxDailyOrder";
		var cfg = readParmAry( gConfGuardAry , type );

		if ( !cfg.pause && !cfg.off && !cfg.flat && !cfg.flatAll && !cfg.notice ){
			return;
		}

		if ( ecn != readParmAry( gConfOrderAry , "TradeEcn" ).select || sym != readParmAry( gConfOrderAry , "TradeSym" ).select ){
			return;
		}

		if ( num < cfg.parm ){
			return
		}

		var reason = "Exceed Maximum Daily Order Limit On " + ecn + " " + sym + ". Daily Order Limit: " + cfg.parm + ". ";

		triggerGuardian( type , reason );
	}

	function resetAllGuardianTriggered(){
		for ( var i = 0 ; i < gConfGuardAry.length ; i++ ){
			var type = gConfGuardAry[ i ].name;
			editParmAry( gConfGuardAry , type , "triggered" , false );
		}
	}

	function showGuardianNotice( type ){ // show a notice if guardian is triggered.
		var cfg = readParmAry( gConfGuardAry , type );

		if ( !cfg.showWarn ){
			return;
		}

		$( ".panlGuarDiv" ).addClass( "fontColorNotice" );
		$( "#jpanel_msg" ).find( ".badgeSp" ).addClass( "bgColorNotice" );

		var div = $( ".conf" + type + "Div" );
		$( div ).find( ".txtIpt" ).addClass( "confWarnIpt" , 300 );
		$( div ).addClass( "confGuardWarn" , 300 );

		if ( $( ".confGuarSec" ).is( ":visible" ) ){
			return;
		}

		showGuardianPanel();
	}

	function autoFlatPosition( type ){ // auto square position
		var type = "AutoFlatQty";
		var cfg = readParmAry( gConfGuardAry , type );

		if ( cfg.parm == 0 ){ // do nothing if auto cut 0 percent
			return;
		}

		squarePosition( cfg.parm , gDefAutoFlatSlip );
	}

	function squarePosition( pct , slip ){
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){ // check if the panel is reloaded/disconnected or not
			return;
		}

		clearInterval( gIntervalObj.flatPosition ); // clear previous timer
		gIntervalObj.flatPosition = false;

		var type = "MaxAutoFlatQty";
		var cfg = readParmAry( gConfGuardAry , type );

		var interval = 200;
		var wait = 2000;
		var orderNum = 0;

		var done = false;

		flatPos();
		gIntervalObj.flatPosition = setInterval( flatPos , interval );

		function flatPos(){
			if ( gAlgoStatusObj.on ){
				orderNum = getOpenOrderNum();
			}

			if ( wait <= 0 ){
				clearInterval( gIntervalObj.flatPosition );
				gIntervalObj.flatPosition = false;

				var msg = "Exist Outstanding Open Order , Open Order No. : " + orderNum + ". Auto Flat is Aborted.";
				sendTabMsg( msg );

				return;
			}

			if ( orderNum > 1 ){
				wait -= interval;
				return;
			}

			if ( done ){
				return;
			}

			done = true;

			clearInterval( gIntervalObj.flatPosition );
			gIntervalObj.flatPosition = false;

			var ecn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
			var sym = readParmAry( gConfOrderAry , "TradeSym" ).select;

			var netPos = getSymNetPos( ecn , sym ).netPos;
			var dip = parseFloat( getSymItem( ecn , sym ).dip ); // get symbol decimal places
			var ts = parseFloat( getSymItem( ecn , sym ).ts );

			if ( netPos == 0 || ! $.isNumeric( netPos ) ){
				return;
			}

			var qty = Math.round( netPos * ( pct / 100 ) ) * -1; // square qty
			if ( qty == 0 ){
				return;
			}

			var bidPx = getSymBidAskPx( ecn , sym )[ 0 ];
			var askPx = getSymBidAskPx( ecn , sym )[ 1 ];

			if ( bidPx == 0 || askPx == 0 ){
				var msg = "Error on getting " + sym + " Bid Ask Price. Fail to send cut loss order.";
				sendLogMsg( msg );

				return;
			}

			if ( isNaN( dip ) ){
				var msg = "Error On Getting Decimal Places : " + dip + ". Fail to send cut loss order.";
				sendLogMsg( msg );

				return;
			}

			var msg = "Squaring Position. ecn : " + ecn + " .sym : " + sym + " .netPos : " + netPos + ". cut qty : " + qty;
			sendLogMsg( msg );

			if ( pct > 0 ){
				var px = getCutLossPx( ecn , sym , netPos , qty , bidPx , askPx );

			}else{
				if ( qty > 0 ){
					var px = askPx + ( slip * ts );
				}else{
					var px = bidPx - ( slip * ts );
				}
			}

			px = px.toFixed( dip );

			if ( qty > 0 ){
				var side = "Buy";
			}else{
				var side = "Sell";
			}

			if ( pct > 0 ){
				var type = "Flat";
			}else{
				var type = "Overweight";
			}

			if ( Math.abs( qty ) > cfg.parm ){
				if ( qty > 0 ){
					qty = cfg.parm;
				}else{
					qty = cfg.parm * -1;
				}

				var cutMsg = ". ( Maximum Auto " + type + " " + cfg.parm + " Lots. )";

			}else{
				var cutMsg = ". ( " + type + " " + Math.abs( pct ) + "% of Current Net Position. )";
			}

			sendSquarePositionOrder( ecn , sym , qty , px );

			var msg = "Auto " + type + " Position. Placed " + side + " " + Math.abs( qty ) + " Lots " + ecn + " " + sym + " Order at " + px + cutMsg;
			sendTabMsg( msg );
		}
	}

	function sendSquarePositionOrder( ecn , sym , qty , px ){
		var poi = gInitUserID.toString() + "999"; // e.g. 301999
		var type = "LO2";
		var tif = "DAY";

		if ( qty < 0 ){
			var side = "SELL";
		}else if ( qty > 0 ){
			var side = "BUY";
		}else{
			return;
		}

		var cmd = "CMD " + gInitUserID + " O " + ecn + " " + type + " " + tif + " " + side + " " + sym + " QTY " + Math.abs( qty ) + " AT " + px + " POI " + poi + " NAME algoFlat";

		pushCMD( cmd );
		showLog( "sendSquarePositionOrder" , cmd );
	}

	///////////////// config panel function //////////////

	function uiClickCfgPanelBtn(){ // config panel setting
		if ( $( ".confPanlSec" ).is( ":visible" ) ){
			return;
		}

		for ( var i = 0 ; i < gConfPanelAry.length ; i++ ){
			var obj = gConfPanelAry[ i ];
			var div = ".conf" + obj.name + "Div";

			if ( !obj.showCfg ){
				continue;
			}

			if ( obj.showSelect ){
				$( div ).find( ".confSlet" ).val( obj.select );
				$( div ).find( ".confSlet" ).trigger( "change" );
				$( div ).find( ".confSlet" ).foundationCustomForms();
			}

			if ( obj.showParm ){
				$( div ).find( ".txtIpt" ).attr( "value" , obj.parm );
			}

			if ( obj.showOn ){
				if ( obj.local ){
					var on = readParmAry( gLocalCfgAry , obj.name ).on;
				}else{
					var on = obj.on;
				}
				$( div ).find( ".confChk" ).attr( "checked" , on );
			}

			$( div ).find( "*" ).prop( "disabled" , obj.disable );
		}

		$( ".confPanlSec" ).fadeIn();
	}

	function uiConfimPanlCfg(){ // confirm panel config
		var updated = false;
		var msg = "";

		for ( var j = 0 ; j < gConfPanelAry.length ; j++ ){
			var obj = gConfPanelAry[ j ];
			var div = ".conf" + obj.name + "Div";
			var rowDiv = ".confRow" + obj.name + "Div";

			if ( !obj.showCfg ){
				continue;
			}

			if ( obj.showParm ){
				var chkParm = checkIptVal( div , obj.parmMax , obj.parmMin ); // input number validation

				if ( !chkParm ){
					return;
				}

				if ( obj.parm != chkParm ){ // check need to update the macro or not
					if ( obj.name == "UpdateFeq" || obj.name == "ReplaceDelay" || obj.name == "OrderMethod" ){
						updated = true;

						$( ".confRow" + obj.name + "Div" ).each( function (){
							$( this ).find( ".txtIpt" ).val( chkParm );
						} );
					}

					var parmName = "a_panlCfg_" + obj.name + "_parm";

					setMacro( parmName , chkParm );

					msg += obj.title + " Parameter Changed to " + chkParm + obj.unit + ". ";
				}
			}

			if ( obj.showOn ){
				var chk = $( div ).find( ".confChk" ).is( ":checked" );

				var chkNum = chk ? 1 : 0;
				var chkMsg = chk ? "Enabled" : "Disabled";

				if ( obj.local ){
					editParmAry( gLocalCfgAry , obj.name , "on" , chk );
					store.set( "j" + obj.name , chk );

				}else{
					if ( obj.on != chk ){
						var parmName = "a_panlCfg_" + obj.name + "_on";

						setMacro( parmName , chkNum );

						msg += chkMsg + " '" + obj.title + "' Function. ";
					}
				}
			}

			if ( obj.showSelect ){
				var select = $( div ).find( ".current" ).html();
				select = select.replace( / /g , "" );

				if ( obj.select != select ){
					$( rowDiv ).each( function (){
						$( this ).find( ".confSlet" ).val( select );
						$( this ).find( ".confSlet" ).trigger( "change" );
					} );

					var parmName = "a_panlCfg_" + obj.name + "_select";

					setMacro( parmName , select );

					msg += obj.title + " Changed to '" + select + "'. ";
				}
			}
		}

		if ( msg != "" ){ // tell every one panel conf is changed.
			getSeqNum( gByFunction );
			msg += "Seq No.: " + gStatusObj.seqNum + ".";

			var tabMsg = "Updated Algo Parameter : " + msg;
			sendTabMsg( tabMsg );
		}

		uiHideCfgSec();
	}

	function uiClickCfgGuarBtn(){ // config algo guardian setting
		if ( $( ".panlGuarDiv" ).hasClass( "fontColorNotice" ) ){
			$( ".panlGuarDiv" ).removeClass( "fontColorNotice" );
		}

		if ( $( ".confGuarSec" ).is( ":visible" ) ){
			return;
		}

		showGuardianPanel();
	}

	function showGuardianPanel(){
		for ( var i = 0 ; i < gConfGuardAry.length ; i++ ){
			var obj = gConfGuardAry[ i ];
			var div = ".conf" + obj.name + "Div";

			if ( !obj.showCfg ){
				continue;
			}

			if ( obj.showParm ){
				$( div ).find( ".txtIpt" ).attr( "value" , obj.parm );
			}

			if ( obj.showOn ){
				$( div ).find( ".confOnChk" ).attr( "checked" , obj.on );
			}

			if ( obj.showOff ){
				$( div ).find( ".confOffChk" ).attr( "checked" , obj.off );

				if ( obj.off ){
					$( div ).find( ".confPauseChk" ).addClass( "disableCtrl" , 200 );
					$( div ).find( ".confPauseChk" ).prop( "disabled" , true );

					if ( obj.name == "MaxSixtySecVol" ){
						$( ".confNmlSixtySecVolDiv" ).find( "*" ).addClass( "disableCtrl" , 200 );
					}

				}else{
					$( div ).find( ".confPauseChk" ).removeClass( "disableCtrl" , 200 );
					$( div ).find( ".confPauseChk" ).prop( "disabled" , false );

					if ( obj.name == "MaxSixtySecVol" ){
						$( ".confNmlSixtySecVolDiv" ).find( "*" ).removeClass( "disableCtrl" , 200 );
					}
				}
			}

			if ( obj.showPause ){
				$( div ).find( ".confPauseChk" ).attr( "checked" , obj.pause );
			}

			if ( obj.showFlat ){
				$( div ).find( ".confFlatChk" ).attr( "checked" , obj.flat );
			}

			if ( obj.showFlatAll ){
				$( div ).find( ".confFlatAllChk" ).attr( "checked" , obj.flatAll );

				if ( obj.flatAll ){
					$( div ).find( ".confFlatChk" ).addClass( "disableCtrl" , 200 );
					$( div ).find( ".confFlatChk" ).prop( "disabled" , true );
				}else{
					$( div ).find( ".confFlatChk" ).removeClass( "disableCtrl" , 200 );
					$( div ).find( ".confFlatChk" ).prop( "disabled" , false );
				}
			}

			if ( obj.showNotice ){
				$( div ).find( ".confNoticeChk" ).attr( "checked" , obj.notice );
			}
		}

		$( ".confGuarSec" ).fadeIn();
	}

	function uiConfimGuarCfg(){ // confirm guardian setting by user
		var msg = "";

		for ( var j = 0 ; j < gConfGuardAry.length ; j++ ){
			var obj = gConfGuardAry[ j ];
			var div = ".conf" + obj.name + "Div";
			var chkChg = false;
			var parmChg = false;

			if ( !obj.showCfg ){
				continue;
			}

			if ( obj.showParm ){
				var chkParm = checkIptVal( div , obj.parmMax , obj.parmMin ); // input number validation
				if ( chkParm === false ){
					return;
				}

				if ( obj.parm != chkParm ){ // check need to update the macro or not
					parmChg = true;
					obj.parm = chkParm;

					if ( obj.name == "MaxSixtySecVol" || obj.name == "NmlSixtySecVol" ){
						chkParm = Math.round( chkParm / 500 ) * 500;
					}else if ( obj.name == "LossFromTop" ){
						renewMaxProfit();
					}

					var parmName = "a_guardCfg_" + obj.name + "_parm";

					setMacro( parmName , chkParm );
					msg += obj.title + " Changed to " + chkParm + obj.unit + ". ";
				}
			}

			if ( obj.showOn ){
				var chk = $( div ).find( ".confOnChk" ).is( ":checked" );

				var chkNum = chk ? 1 : 0;
				var chkMsg = chk ? "Enabled" : "Disabled";

				if ( obj.on != chk ){
					obj.on = chk;

					var parmName = "a_guardCfg_" + obj.name + "_on";

					setMacro( parmName , chkNum );
					msg += chkMsg + " '" + obj.title + "' On Function. ";
				}
			}

			if ( obj.showOff ){
				var chk = $( div ).find( ".confOffChk" ).is( ":checked" );

				var chkNum = chk ? 1 : 0;
				var chkMsg = chk ? "Enable" : "Disabled";

				if ( obj.off != chk ){
					chkChg = true;
					obj.off = chk;

					if ( obj.name == "LossFromTop" && chk ){
						renewMaxProfit();
					}else if ( chk ){

					}

					var parmName = "a_guardCfg_" + obj.name + "_off";

					setMacro( parmName , chkNum );
					msg += chkMsg + " '" + obj.title + "' Off Function. ";
				}
			}

			if ( obj.showPause ){
				var chk = $( div ).find( ".confPauseChk" ).is( ":checked" );

				var chkNum = chk ? 1 : 0;
				var chkMsg = chk ? "Enable" : "Disabled";

				if ( obj.pause != chk ){
					chkChg = true;
					obj.pause = chk;

					if ( obj.name == "LossFromTop" && chk ){
						renewMaxProfit();
					}

					var parmName = "a_guardCfg_" + obj.name + "_pause";

					setMacro( parmName , chkNum );
					msg += chkMsg + " '" + obj.title + "' Pause Function. ";
				}
			}

			if ( obj.showFlat ){
				var chk = $( div ).find( ".confFlatChk" ).is( ":checked" );

				var chkNum = chk ? 1 : 0;
				var chkMsg = chk ? "Enable" : "Disabled";

				if ( obj.flat != chk ){
					chkChg = true;
					obj.flat == chk;

					if ( obj.name == "LossFromTop" && chk ){
						renewMaxProfit();
					}

					var parmName = "a_guardCfg_" + obj.name + "_flat";

					setMacro( parmName , chkNum );
					msg += chkMsg + " '" + obj.title + "' Flat Function. ";
				}
			}

			if ( obj.showFlatAll ){
				var chk = $( div ).find( ".confFlatAllChk" ).is( ":checked" );

				var chkNum = chk ? 1 : 0;
				var chkMsg = chk ? "Enable" : "Disabled";

				if ( obj.flatAll != chk ){
					chkChg = true;
					obj.flatAll == chk;

					if ( obj.name == "LossFromTop" && chk ){
						renewMaxProfit();
					}

					var parmName = "a_guardCfg_" + obj.name + "_flatAll";

					setMacro( parmName , chkNum );
					msg += chkMsg + " '" + obj.title + "' Flat All Function. ";
				}
			}

			if ( obj.showNotice ){
				var chk = $( div ).find( ".confNoticeChk" ).is( ":checked" );

				var chkNum = chk ? 1 : 0;
				var chkMsg = chk ? "Enable" : "Disabled";

				if ( obj.notice != chk ){
					chkChg = true;
					obj.notice == chk;

					if ( obj.name == "LossFromTop" && chk ){
						renewMaxProfit();
					}

					var parmName = "a_guardCfg_" + obj.name + "_notice";

					setMacro( parmName , chkNum );
					msg += chkMsg + " '" + obj.title + "' Notice Function. ";
				}
			}
		}

		if ( msg != "" ){ // tell every one panel conf is changed.
			getSeqNum( gByFunction );
			msg += "Seq No.: " + gStatusObj.seqNum + ".";

			var tabMsg = "Updated Guardian Parameter : " + msg;
			sendTabMsg( tabMsg );
		}

		$( ".confDiv" ).removeClass( "confGuardWarn" );
		$( ".panlBtnDiv" ).removeClass( "fontColorNotice" );
		$( ".txtIpt" ).removeClass( "confWarnIpt" );

		uiHideCfgSec();
	}

	//////////////////// select leader function //////////////

	function checkIsLeader(){ // check if itself is a leader or not
		var isLeaderDefer = $.Deferred();

		if ( gLeaderObj.isLeader ){ // already is leader.
			replylLeaderOn();
			isLeaderDefer.resolve( null );
			return isLeaderDefer;
		}

		$.when( getLeaderID() ).then( function (){
			isLeaderDefer.resolve( null );
			return isLeaderDefer;
		} );

		return isLeaderDefer;
	}

	function getLeaderID(){ // find current leader
		var getIDDefer = $.Deferred(); // create defer object
		if ( gLeaderObj.on || gStatusObj.callingLeader || gStatusObj.askingSessId ){
			getIDDefer.resolve( null );
			return getIDDefer;
		}

		clearInterval( gIntervalObj.getLeaderID ); // clear previous timer
		gIntervalObj.getLeaderID = false;

		var interval = 950; // check received response every 0.3 second
		var wait = gLeaderLiveTime; // wait 3 seconds for leader response

		var leader = gLeaderObj.id + "::" + gLeaderObj.ip;
		var sess = gStatusObj.sessionId + "::" + gStatusObj.ip;

		callLeader();
		return getIDDefer;

		function callLeader(){
			if ( gLeaderObj.on || gStatusObj.callingLeader || gStatusObj.askingSessId ){ // check if leader has response
				getIDDefer.resolve( null );
				return getIDDefer;
			}

			sendMsg( gInitUserID , "a_callLeader" , leader + " " + sess ); // call leader
			gStatusObj.callingLeader = true;

			if ( !gIntervalObj.getLeaderID ){
				gIntervalObj.getLeaderID = setInterval( getLeader , interval );
			}
		}

		function getLeader(){
			if ( gLeaderObj.on ){ // check if leader has response
				clearInterval( gIntervalObj.getLeaderID );
				gIntervalObj.getLeaderID = false;
				getIDDefer.resolve( null );
				return getIDDefer;
			}

			if ( wait <= 0 ){
				clearInterval( gIntervalObj.getLeaderID );
				gIntervalObj.getLeaderID = false;
			}

			if ( wait <= 0 && !gLeaderObj.on ){
				$.when( electLeader() ).then( function (){
					clearInterval( gIntervalObj.getLeaderID );
					gIntervalObj.getLeaderID = false;

					getIDDefer.resolve( null );
					return getIDDefer;
				} );
			}

			wait -= interval;
		}
	}

	function electLeader(){
		var eleDefer = $.Deferred();

		if ( gStatusObj.askingSessId ){
			eleDefer.resolve( null );
			return eleDefer;
		}

		gSessionIdAry = [];
		gLeaderObj.on = false;

		var idIP = gStatusObj.sessionId + "::" + gStatusObj.ip;

		sendMsg( gInitUserID , "a_getSessID" , idIP ); // ask every one to send his session ID if leader do no response

		setTimeout( function (){ // wait 2 second to collect session ID
			if ( gLeaderObj.on ){ // check if leader has response
				eleDefer.resolve( null );
				return eleDefer;
			}

			if ( gSessionIdAry.length == 0 ){
				setMacro( "a_leaderID" , idIP ); // assign leader

				gLeaderObj.isLeader = true;
				$( "#userInfoIcon" ).show(); // show leader icon

				eleDefer.resolve( null );
				return eleDefer;
			}

			gSessionIdAry = gSessionIdAry.sort(); // sort session ID
			var lastIndex = gSessionIdAry.length - 1;

			gLeaderObj.id = parseInt( gSessionIdAry[ lastIndex ][ 0 ] ); // use first session ID as leader ID
			gLeaderObj.ip = gSessionIdAry[ lastIndex ][ 1 ];

			if ( gLeaderObj.id == gStatusObj.sessionId && gLeaderObj.ip == gStatusObj.ip ){
				gLeaderObj.isLeader = true;
				$( "#userInfoIcon" ).show(); // show leader icon

			}else{
				gLeaderObj.isLeader = false;
				$( "#userInfoIcon" ).hide();
			}

			setMacro( "a_leaderID" , gLeaderObj.id + "::" + gLeaderObj.ip ); // assign leader

			eleDefer.resolve( null );
			return eleDefer;
		} , 2000 );

		return eleDefer;
	}

	function replylLeaderOn(){ // reply leader on if it is a leader
		if ( gLeaderObj.replied ){
			return;
		}

		sendMsg( gInitUserID , "a_leaderOn" , gStatusObj.sessionId + "::" + gStatusObj.ip ); // response if itself is the leader

		gLeaderObj.replied = true;

		setTimeout( function (){
			gLeaderObj.replied = false;
		} , 500 );
	}

	function readLeaderID( cmrAry ){
		// e.g. CMR 302 MACRO _LEADER_ID NAME 927::192.168.0.110 ( someone assigned leader );
		gStatusObj.callingLeader = false;
		gStatusObj.askingSessId = false;
		clearInterval( gIntervalObj.getLeaderID );
		gIntervalObj.getLeaderID = false;

		leaderID = parseInt( cmrAry[ 5 ].split( "::" )[ 0 ] );
		leaderIP = cmrAry[ 5 ].split( "::" )[ 1 ];

		if ( typeof leaderID == "undefined" || isNaN( leaderID ) || leaderIP == 0 || typeof leaderIP == "undefined" ){
			return;
		}

		gLeaderObj.id = leaderID;
		gLeaderObj.ip = leaderIP;

		if ( gLeaderObj.id == gStatusObj.sessionId && gLeaderObj.ip == gStatusObj.ip ){
			replylLeaderOn();
			resetLongInterval( false );
			gLeaderObj.isLeader = true;
			$( "#userInfoIcon" ).show(); // show leader icon

		}else{
			gLeaderObj.isLeader = false;
			$( "#userInfoIcon" ).hide();
		}
	}

	function readLeaderOnMsg( cmrAry ){
		// e.g. CMR 309 MSG LEADER_ON 308::192.168.0.110
		if ( !gLeaderObj.isLeader ){
			resetLongInterval( false );
		}

		gStatusObj.callingLeader = false;
		gStatusObj.askingSessId = false;
		clearInterval( gIntervalObj.getLeaderID );
		gIntervalObj.getLeaderID = false;

		clearTimeout( gTimerObj.waitLeaderOn );

		gTimerObj.waitLeaderOn = setTimeout( function (){
			gLeaderObj.on = false;
		} , gLeaderLiveTime );

		if ( gLeaderObj.on ){ // check if received leader response already
			var leaderID = parseInt( cmrAry[ 4 ].split( "::" )[ 0 ] );
			var leaderIP = cmrAry[ 4 ].split( "::" )[ 1 ];

			if ( gLeaderObj.id != leaderID || gLeaderObj.ip != leaderIP ){
				multipleLeader( leaderID , leaderIP ); // ask the another leader to get new ID
			}

			return;
		}

		gLeaderObj.on = true;

		gLeaderObj.id = parseInt( cmrAry[ 4 ].split( "::" )[ 0 ] );
		gLeaderObj.ip = cmrAry[ 4 ].split( "::" )[ 1 ];

		if ( typeof gLeaderObj.ip == "undefined" ){
			gLeaderObj.ip = 0;
		}
	}

	function replyLeaderID( cmrAry ){ // response if someone call leader
		//e.g. CMR 302 MSG CALL_LEADER 308::192.168.0.110
		gStatusObj.callingLeader = true;

		clearTimeout( gTimerObj.replyLeader ); // clear previous timer

		gTimerObj.replyLeader = setTimeout( function (){ // wait 0.5 second to avoid reading too many CALL_LEADER msg
			var leaderID = cmrAry[ 4 ].split( "::" )[ 0 ];
			var ip = cmrAry[ 4 ].split( "::" )[ 1 ];

			if ( gStatusObj.sessionId != leaderID || gStatusObj.ip != ip ){ // check itself is leader or not
				return;
			}

			replylLeaderOn();
		} , 500 );
	}

	function sendSessID(){ // send session id to every one
		gStatusObj.askingSessId = true;
		clearTimeout( gTimerObj.sendSessId ); // clear previous timer

		gTimerObj.sendSessId = setTimeout( function (){ // wait 0.5 sec to avoid reading too many send sessID request
			sendMsg( gInitUserID , "a_sendSessId" , gStatusObj.sessionId + "::" + gStatusObj.ip );
		} , 500 );
	}

	function collectSessID( cmrAry ){ // collect session ID information
		// e.g. CMR 302 MSG SEND_SESSID 928::192.168.0.110
		var sessID = parseInt( cmrAry[ 4 ].split( "::" )[ 0 ] );
		var ip = cmrAry[ 4 ].split( "::" )[ 1 ];

		if ( sessID == 0 || sessID.toString() == "NaN" || typeof ip == "undefined" || ip == 0 ){
			return; // ignore if it has wrong seeID or ip
		}

		if ( jQuery.inArray( sessID , gSessionIdAry ) >= 0 ){
			return;
		}
		gSessionIdAry[ gSessionIdAry.length ] = [ sessID , ip ]; // save session ID to an array
	}

	function getSessID( cmrAry ){ // store session ID after get macro
		// e.g. CMR 302 GET_MACRO SESSID 198 SEQ 192.168.0.110::48
		var seq = cmrAry[ 6 ];

		var ip = seq.split( "::" )[ 0 ];
		var num = parseInt( seq.split( "::" )[ 1 ] );

		if ( gStatusObj.sessionId != 0 ){
			return;
		}

		if ( ip != gStatusObj.ip || num != gRandomNum ){
			return;
		}

		gStatusObj.sessionId = parseInt( cmrAry[ 4 ] );
		$( "#userInfoID" ).text( gStatusObj.sessionId );
	}

	function multipleLeader( sessID , ip ){
		sendMsg( gInitUserID , "a_multipleLeader" , sessID + "::" + ip ); // request this user to get new session ID
	}

	function getNewSessID( cmrAry ){ // get a new session ID
		if ( gStatusObj.gettingSessId ){ // check the user is getting new ID or not
			return;
		}

		var sessID = parseInt( cmrAry[ 4 ].split( "::" )[ 0 ] );
		var ip = cmrAry[ 4 ].split( "::" )[ 1 ];

		if ( gStatusObj.sessionId != sessID || gStatusObj.ip != ip ){ // check is it this user
			return;
		}

		gStatusObj.sessionId = 0; // delete old ID
		gStatusObj.gettingSessId = true; // getting new ID

		getMacro(); // getMacro to get new session ID

		setTimeout( function (){
			gStatusObj.gettingSessId = false; // wait 0.2 second to get new ID
		} , 200 );
	}

	/////////////// get , set , read engine parameter function //////

	function getParameter( name ){
		var cmd = "CMD " + gInitUserID + " GET " + name;
		pushCMD( cmd );

		showLog( "getParameter" , cmd );
	}

	function getAllParameter(){
		for ( var prop in gAlgoStatusObj ){
			getParameter( "a_status_" + prop );
		}
	}

	function checkParameter( name ){ // check lastest algo on off situation
		gStatusObj.gotParameter = false;

		getParameter( name );

		var defer = $.Deferred();

		clearInterval( gIntervalObj.checkParm );
		gIntervalObj.checkParm = false;

		var interval = 100; // check received response every 0.3 second
		var wait = 2000; // wait 2 seconds for server response

		if ( !gIntervalObj.checkParm ){
			gIntervalObj.checkParm = setInterval( getParm , interval );
		}

		return defer;

		function getParm(){
			if ( wait > 0 && gStatusObj.gotParameter == false ){
				wait -= interval;
				return;
			}

			clearInterval( gIntervalObj.checkParm );
			gIntervalObj.checkParm = false;

			if ( wait <= 0 ){
				var msg = "Server response time out. Seq No.: " + gStatusObj.seqNum + "."; // send a stop all notice if it is a leader
				sendTabMsg( msg );
			}

			defer.resolve( null );
			return defer;
		}
	}

	function setParameter( name , parm ){
		if ( ! validateParameter( name , parm , parm ) ){
			throw new Error( "Parameter is NaN or undefined." );
		}

		getSeqNum( gByFunction );
		var cmd = "CMD " + gInitUserID + " DEFAULT " + name + " " + parm;
		//var cmd = "CMD " + gInitUserID + " DEFAULT " + defName + " " + parm + " SEQ " + gStatusObj.seqNum;

		pushCMD( cmd );
		showLog( "setParameter" , cmd );

		getParameter( name );
	}

	/////////////////////////// mode function ////////////////

	function buildModeOrderMacro( bidOrAsk , numStr , pips , qty , update , replace , method , methodSec ){
		if ( ! validateParameter( "bidOrAsk" , bidOrAsk , bidOrAsk ) ||
			! validateParameter( "numStr" , numStr , numStr ) ||
			! validateParameter( "pips" , pips , pips ) ||
			! validateParameter( "qty" , qty , qty ) ||
			! validateParameter( "update" , update , update ) ||
			! validateParameter( "replace" , replace , replace ) ||
			! validateParameter( "method" , method , method ) ||
			! validateParameter( "methodSec" , methodSec , methodSec ) ){

			throw new Error( "Parameter is NaN or undefined." ); // avoid sending "NaN" to server
		}

		replace = replace * 1000;
		update = update * 1000;

		var tradeEcn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
		var tradeSym = readParmAry( gConfOrderAry , "TradeSym" ).select;
		var ts = parseFloat( getSymItem( tradeEcn , tradeSym ).ts );
		var dip = parseFloat( getSymItem( tradeEcn , tradeSym ).dip );

		if ( bidOrAsk == "bid" ){
			var sideNum = "1";
			var ordSide = "BUY";
			pips = pips * -1;

		}else if ( bidOrAsk == "ask" ){
			var sideNum = "2";
			var ordSide = "SELL";
		}
		pips = ( pips * ts ).toFixed( dip );

		var suffix = "";
		if ( method != "Standard" ){
			suffix = "_" + method + "_" + methodSec;
		}
		var pxMethod = bidOrAsk.toUpperCase() + suffix;
		var poi = "999" + sideNum + numStr;

		var macName = "a_orderMac_main_" + bidOrAsk + "_" + numStr;
		var stopName = "a_orderMac_stop_" + bidOrAsk + "_" + numStr;
		var lpName = "a_orderMac_loop_" + bidOrAsk + "_" + numStr;
		var pxName = "a_orderMac_pxCheck_" + bidOrAsk + "_" + numStr;
		var setName = "a_orderMac_set_" + bidOrAsk + "_" + numStr;
		var runName = "a_orderMac_run_" + bidOrAsk + "_" + numStr;
		var cnlName = "a_orderMac_cancel_" + bidOrAsk + "_" + numStr;

		var macDefName = "[a_parm_" + bidOrAsk + "_" + numStr;
		var macDefEcn = macDefName + "_ecn]";
		var macDefSym = macDefName + "_sym]";
		var macDefPips = macDefName + "_pips]";
		var macDefQty = macDefName + "_qty]";
		var macDefUpdate = macDefName + "_update]";
		var macDefReplace = macDefName + "_replace]";
		var macDefPx = macDefName + "_px]";

		var macStart = "CMD " + gInitUserID + " MACRO ";
		var macCnlAlgo = "CANCEL_ALGO " + macName;
		var macCnlOrd = "O " + macDefEcn + " CO DAY CANCEL 0 POI " + poi;
		var macGetPx = "DEFAULT " + macDefEcn + " " + tradeEcn + " DEFAULT " + macDefSym + " " + tradeSym + " AT " + pxMethod + " + 0 | EVAL " + macDefPx + " { AT + " + macDefPips + " } | AT " + macDefPx;
		var macSendOrd = "O " + macDefEcn + " " + gLOType + " " + gOrdTIF + " " + ordSide + " " + macDefSym + " QTY " + macDefQty + " AT " + macDefPx;

		var macSavePOI = "POI " + poi;
		var macSavePx = "SAVE " + pxName + " AT";
		var macUpdMac = "DELAY 0 " + macDefUpdate + " { DO " + lpName + " SEQ " + gByMacro + " }";
		var macDoMac = "DO " + macName + " SEQ " + gByMacro;

		var rejectMsg = ordSide + " Order " + poi + " is rejected.";
		var macRejectToastrMsg = "MSG " + gInitUserID + " a_toastrMsg { Rejected_" + rejectMsg + " }";
		var macRejectTabMsg = "MSG " + gInitUserID + " a_tabMsg { " + rejectMsg + " }";

		var cmdMac = macStart + macName + " NAME " + macName + " " + macCnlAlgo +
			" | " + macCnlOrd +
			" | " + macGetPx +
			" | " + macSendOrd + " " + macSavePOI + " " + macSavePx +
			" | " + macUpdMac +
			" | IF_DONE 1 " + macDefEcn + " LAST_TRADE_OID 1 { " + macCnlOrd + " }" +
			" | IF_DONE 1 " + macDefEcn + " LAST_TRADE_OID 2 { NAME " + macName + " DELAY 0 " + replace + " { " + macDoMac + " } }" +
			" | IF_DONE 1 " + macDefEcn + " LAST_TRADE_OID 4 { NAME " + macName + " DELAY 0 0 { " + macDoMac + " } }" +
			" | IF_DONE 1 " + macDefEcn + " LAST_TRADE_OID 8 { " + macCnlAlgo + " | " + macRejectToastrMsg + " | " + macRejectTabMsg + " }";
		var cmdStop = macStart + stopName + " " + macCnlAlgo +
						" | " + macCnlOrd +
						" | DELAY 0 1000 { " + macCnlOrd + " }";
		var cmdLp = macStart + lpName + " NAME " + lpName +
						" | " + macGetPx +
						" | O " + macDefEcn + " " + gLOType + " " + gOrdTIF +
						" | " + "IF { NE AT " + pxName + " } " +
						"{ " + macCnlOrd + " } " +
						"{ NAME " + macName + " " + macUpdMac + " }";
		var cmdSet = macStart + setName +
		" DEFAULT " + macDefEcn + " " + tradeEcn +
		" DEFAULT " + macDefSym + " " + tradeSym +
		" DEFAULT " + macDefPips + " " + pips +
		" DEFAULT " + macDefQty + " " + qty +
		" DEFAULT " + macDefUpdate + " " + update +
		" DEFAULT " + macDefReplace + " " + replace;

		var cmdRun = macStart + runName + " CALL " + setName + " | CALL " + macName;
		var cmdCnl = macStart + cnlName + " " + macCnlOrd;

		pushCMD( cmdMac );
		pushCMD( cmdStop );
		pushCMD( cmdLp );
		pushCMD( cmdSet );
		pushCMD( cmdRun );
		pushCMD( cmdCnl );

		showLog( "buildModeOrderMacro" , cmdMac );
		showLog( "buildModeOrderMacro" , cmdStop );
		showLog( "buildModeOrderMacro" , cmdLp );
		showLog( "buildModeOrderMacro" , cmdSet );
		showLog( "buildModeOrderMacro" , cmdRun );
		showLog( "buildModeOrderMacro" , cmdCnl );
	}

	function runModeOrderAction( action , bidOrAsk , numStr ){
		var actionName = "a_orderMac_" + action + "_" + bidOrAsk + "_" + numStr;
		startMacro( actionName );
	}

	function editModeOrderParm( bidOrAsk , numStr , pips , qty , update , replace , method , methodSec ){
		if ( ! validateParameter( "bidOrAsk" , bidOrAsk , bidOrAsk ) ||
			! validateParameter( "numStr" , numStr , numStr ) ||
			! validateParameter( "pips" , pips , pips ) ||
			! validateParameter( "qty" , qty , qty ) ||
			! validateParameter( "update" , update , update ) ||
			! validateParameter( "replace" , replace , replace ) ){

			throw new Error( "Parameter is NaN or undefined." ); // avoid sending "NaN" to server
		}

		replace = replace * 1000;
		update = update * 1000;

		var tradeEcn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
		var tradeSym = readParmAry( gConfOrderAry , "TradeSym" ).select;
		var ts = parseFloat( getSymItem( tradeEcn , tradeSym ).ts );
		var dip = parseFloat( getSymItem( tradeEcn , tradeSym ).dip );

		if ( bidOrAsk == "bid" ){
			pips = pips * -1;
		}
		pips = ( pips * ts ).toFixed( dip );

		var setName = "a_orderMac_set_" + bidOrAsk + "_" + numStr;

		var macDefName = "[a_parm_" + bidOrAsk + "_" + numStr;
		var macDefEcn = macDefName + "_ecn]";
		var macDefSym = macDefName + "_sym]";
		var macDefPips = macDefName + "_pips]";
		var macDefQty = macDefName + "_qty]";
		var macDefUpdate = macDefName + "_update]";
		var macDefReplace = macDefName + "_replace]";

		var macStart = "CMD " + gInitUserID + " MACRO ";
		var cmdSet = macStart + setName +
		" DEFAULT " + macDefEcn + " " + tradeEcn +
		" DEFAULT " + macDefSym + " " + tradeSym +
		" DEFAULT " + macDefPips + " " + pips +
		" DEFAULT " + macDefQty + " " + qty +
		" DEFAULT " + macDefUpdate + " " + update +
		" DEFAULT " + macDefReplace + " " + replace;

		pushCMD( cmdSet );
		showLog( "editModeOrderParm" , cmdSet );
	}

	function readCurrentOrderParm( cmrAry ){
		var name = cmrAry[ 3 ];
		var type = name.split( "_" )[ 1 ];
		var bidOrAsk = name.split( "_" )[ 2 ];
		var numStr = name.split( "_" )[ 3 ];
		var id = parseInt( numStr ) - 1;

		if ( type != "set" ){
			return;
		}

		var newRecord = true;

		var ecn = cmrAry[ 6 ];
		var sym = cmrAry[ 9 ];
		var pips = parseFloat( cmrAry[ 12 ] );
		var qty = parseFloat( cmrAry[ 15 ] );
		var update = parseFloat( cmrAry[ 18 ] );
		var replace = parseFloat( cmrAry[ 21 ] );
		var ts = parseFloat( getSymItem( ecn , sym ).ts );

		pips = ( pips / ts ).toFixed();
		update = ( update / 1000 ).toFixed();
		replace = ( replace / 1000 ).toFixed();

		var parmObj = { ecn: ecn , sym: sym , bidOrAsk: bidOrAsk , pips: pips , qty: qty , update: update , replace: replace };

		vueCurrentOrder[ bidOrAsk + "OrderAry" ][ id ] = Object.assign( parmObj , vueCurrentOrder[ bidOrAsk + "OrderAry" ][ id ] );
		vueCurrentOrder[ bidOrAsk + "OrderAry" ].push();
	}

	function updateModeCause( name ){
		for ( var i = 0 ; i < gModeCauseAry.length ; i++ ){
			var obj = gModeCauseAry[ i ];
			var bidPos = false;
			var askPos = false;

			if ( obj.name != name ){
				continue;
			}
			if ( obj.axis == "Disable" ){
				break;
			}
			if ( obj.title == "Manual Adject" ){
				if ( obj.bidStatus == "" ){
					bidPos = 0;
				}else{
					bidPos = parseFloat( obj.bidStatus );
				}
				if ( obj.askStatus == "" ){
					askPos = 0;
				}else{
					askPos = parseFloat( obj.askStatus );
				}

			}else{
				var parmAry = [];
				var axisNum = gModeAxisNumObj[ obj.axis + "Num" ];

				for ( var j = 0 ; j < axisNum ; j++ ){
					var pos = j + 1 - ( ( axisNum + 1 ) / 2 );
					var parm = parseFloat( obj[ "parm" + pos.toString() ] );

					if ( isNaN( parm ) ){
						continue;
					}
					parmAry[ parmAry.length ] = [ pos , parm ];
				}

				if ( parmAry.length < 1 ){
					var bidPos = 0;
					var askPos = 0;
				}

				if ( parmAry.length == 1 ){
					var bidPos = getPosFromOne( obj.bidStatus );
					var askPos = getPosFromOne( obj.askStatus );

				}else if ( parmAry.length > 1 ){
					var bidPos = getPos( obj.bidStatus );
					var askPos = getPos( obj.askStatus );
				}
			}

			if ( bidPos != obj.bidPos ){
				editParmAry( gModeCauseAry , obj.name , "bidPos" , bidPos );
				checkCauseAction( obj , bidPos , obj.bidPos , "bid" );
			}
			if ( askPos != obj.askPos ){
				editParmAry( gModeCauseAry , obj.name , "askPos" , askPos );
				checkCauseAction( obj , askPos , obj.askPos , "ask" );
			}
			break;
		}

		function getPos( status ){
			if ( status == "" ){
				return 0;
			}
			for ( var j = 0 ; j < parmAry.length ; j++ ){
				var thisPos = parmAry[ j ][ 0 ];
				var thisParm = parmAry[ j ][ 1 ];
				if ( j == parmAry.length - 1 ){
					if ( thisPos < 0 ){
						var nextPos = 0;
					}else{
						var nextPos = parmAry[ j ][ 0 ] + 1;
					}
				}else{
					if ( thisPos < 0 ){
						var nextPos = parmAry[ j + 1 ][ 0 ];
					}else{
						var nextPos = parmAry[ j ][ 0 ] + 1;
					}
					var nextParm = parmAry[ j + 1 ][ 1 ];
				}

				if ( obj.direction == "up" ){
					if ( thisPos < 0 ){
						if ( status < thisParm ){
							return thisPos;
						}else if ( status == thisParm ){
							if ( nextPos > 0 ){
								return 0;
							}
							return nextPos;
						}
					}else if ( thisPos >= 0 ){
						if ( status > thisParm ){
							if ( j == parmAry.length - 1 || status <= nextParm ){
								return nextPos;
							}
						}else if ( status <= thisParm ){
							if ( j == 0 ){
								return 0;
							}
							return thisPos;
						}
					}
				}else if ( obj.direction == "down" ){
					if ( thisPos < 0 ){
						if ( status > thisParm ){
							return thisPos;
						}else if ( status == thisParm ){
							if ( nextPos > 0 ){
								return 0;
							}
							return nextPos;
						}
					}else if ( thisPos >= 0 ){
						if ( status < thisParm ){
							if ( j == parmAry.length - 1 || status >= nextParm ){
								return nextPos;
							}
						}else if ( status >= thisParm ){
							if ( j == 0 ){
								return 0;
							}
							return thisPos;
						}
					}
				}
			}
			return 0;
		}

		function getPosFromOne( status ){
			if ( status == "" ){
				return 0;
			}

			var pos = parmAry[ 0 ][ 0 ];
			var parm = parmAry[ 0 ][ 1 ];

			if ( obj.direction == "up" ){
				if ( pos < 0 && status < parm ){
					return pos;
				}else if ( pos >= 0 && status > parm ){
					return pos + 1;
				}else{
					return 0;
				}
			}else if ( obj.direction == "down" ){
				if ( pos < 0 && status > parm ){
					return pos;
				}else if ( pos >= 0 && status < parm ){
					return pos + 1;
				}else{
					return 0;
				}
			}
		}
	}

	function updateModeOrder( shift , spread , bidOrAsk ){
		if ( !gStatusObj.completedLoad ){
			return;
		}
		gModePositionObj[ bidOrAsk + "Shift" ] = shift;
		gModePositionObj[ bidOrAsk + "Spread" ] = spread;

		var oldOrderAry = [];
		var ary = vueCurrentOrder[ bidOrAsk + "OrderAry" ];

		for ( var i = 0 ; i < ary.length ; i++ ){
			Vue.set( vueCurrentOrder[ bidOrAsk + "OrderAry" ][ i ] , "show" , false );

			oldOrderAry[ i ] = { pips: ary[ i ].pips ,
								qty: ary[ i ].qty ,
								update: ary[ i ].update ,
								replace: ary[ i ].replace , };
		}

		vueCurrentOrder[ bidOrAsk + "OrderAry" ] = [];

		var j = 0;
		for ( var i = 0 ; i < gModeOrderAry.length ; i++ ){
			var obj = gModeOrderAry[ i ];
			if ( obj.shift != shift ){
				continue;
			}
			if ( obj.spread != spread ){
				continue;
			}
			if ( obj.bidOrAsk != bidOrAsk ){
				continue;
			}

			vueCurrentOrder[ bidOrAsk + "OrderAry" ][ j ] = obj;
			j++;
		}
		vueCurrentOrder[ bidOrAsk + "OrderAry" ].push();

		$.when( checkIsLeader() ).then( function (){
			if ( !gLeaderObj.isLeader ){
				return;
			}

			for ( var i = 0 ; i < vueCurrentOrder[ bidOrAsk + "OrderAry" ].length ; i++ ){
				var obj = vueCurrentOrder[ bidOrAsk + "OrderAry" ][ i ];
				var oldOrderObj = oldOrderAry[ i ];
				var numStr = ( i + 1 ).toString();

				if ( !gMacroCount[ bidOrAsk ][ i ] ){
					buildModeMacro( obj , bidOrAsk , numStr );

				}else if ( typeof( oldOrderObj ) == "undefined" ||
				oldOrderObj.pips != obj.pips ||
				oldOrderObj.qty != obj.qty ||
				oldOrderObj.update != obj.update ||
				oldOrderObj.replace != obj.replace ){
					editMacro( obj , bidOrAsk , numStr );
				}
			}

			for ( var i = vueCurrentOrder[ bidOrAsk + "OrderAry" ].length ; i < oldOrderAry.length ; i++ ){
				var numStr = ( i + 1 ).toString();
				runMacro( "stop" , bidOrAsk , numStr );

				clearTimeout( gTimerObj[ bidOrAsk + "RunMacro" ] );
			}
		} );

		function runMacro( action , bidOrAsk , numStr ){
			if ( !gAlgoStatusObj.on || !gAlgoStatusObj[ bidOrAsk + "On" ] ){
				return;
			}
			runModeOrderAction( action , bidOrAsk , numStr );
		}
		function buildModeMacro( obj , bidOrAsk , numStr ){
			runMacro( "stop" , bidOrAsk , numStr );
			var ordMethod = readParmAry( gConfOrderAry , "OrderMethod" ).select;
			var methodSec = readParmAry( gConfOrderAry , "OrderMethod" ).parm;
			buildModeOrderMacro( obj.bidOrAsk , numStr , obj.pips , obj.qty , obj.update , obj.replace , ordMethod , methodSec );

			clearTimeout( gTimerObj[ bidOrAsk + "RunMacro" ] );
			gTimerObj[ bidOrAsk + "RunMacro" ] = setTimeout( function(){
				runMacro( "run" , bidOrAsk , numStr );
			} , 600 );
		}
		function editMacro( obj , bidOrAsk , numStr ){
			runMacro( "stop" , bidOrAsk , numStr );
			editModeOrderParm( obj.bidOrAsk , numStr , obj.pips , obj.qty , obj.update , obj.replace , obj.method , obj.methodSec );

			clearTimeout( gTimerObj[ bidOrAsk + "RunMacro" ] );
			gTimerObj[ bidOrAsk + "RunMacro" ] = setTimeout( function(){
				runMacro( "run" , bidOrAsk , numStr );
			} , 600 );
		}
	}

	function checkQuoteModeCause(){
		var obj = gQuoteAry[ 0 ];
		if ( typeof obj == "undefined" ){
			return;
		}

		updateModeSimpleCause( "MarketLiquidity" , obj.bidDepth , obj.askDepth );
		updateModeSimpleCause( "LiquidityProportion" , obj.bidProp , obj.askProp );
		updateModeSimpleCause( "PxSpread" , obj.pxSpreadPip , obj.pxSpreadPip );
		updateModeNetPosProfit();
		updateModeNetPosLoss();
		updateModePnL();
		updateModeResistNSupport();
		updateModeResistNSupport2();
		updateModePriceDistance();
		updateModeSwingDistance();
		updateModeLossFromTarget();
	}

	function checkMaQuoteModeCause(){
		var obj = gMAQuoteAry[ 0 ];
		if ( typeof obj == "undefined" ){
			return;
		}

		updateModeSimpleCause( "FiftyMinVolume" , obj.sec900Vol , obj.sec900Vol );
		updateModeSimpleCause( "OneMinVolume" , obj.sec60Vol , obj.sec60Vol );
		updateModeSimpleCause( "MASpread" , obj.maSpreadPip , obj.maSpreadPip );
		updateModeSimpleCause( "TickVolatility" , obj.tickBidVol , obj.tickAskVol );
		updateModeTickVolume();
		updateModeMaTickVolume( obj );
		updateModeVolumeSignal( obj );
		updateModeVolatilitySignal( obj );
		updateModeMASpreadSignal( obj );
		updateModeFillProportion();
		updateModeFillCount();
		updateModePriceSolidness( obj );
		updateModePriceIndicator( obj );
		updateModeVolumeDirectionSignal();
	}

	function updateModeSimpleCause( name , nBidStatus , nAskStatus ){
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}
		checkUpdateModeCause( name , nBidStatus , nAskStatus );
	}

	function updateModeTickVolume(){
		var name = "TickVolume";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}
		var tick = readParmAry( gModeCauseAry , name ).propParm1;
		if ( gMAQuoteAry.length < tick ){
			return;
		}
		var vol = gMAQuoteAry[ 0 ].totalVol - gMAQuoteAry[ tick - 1 ].totalVol;

		checkUpdateModeCause( name , vol , vol );
	}

	function updateModeMaTickVolume(){
		var name = "MaTickVolume";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}
		var causeAry = readParmAry( gModeCauseAry , name );
		var tick = causeAry.propParm1;
		var period = causeAry.propParm2;

		if ( gMAQuoteAry.length < tick ){
			return;
		}

		var vol = gMAQuoteAry[ 0 ].totalVol - gMAQuoteAry[ tick - 1 ].totalVol;

		if ( typeof( gCauseAryObj[ name ] ) == "undefined" ){
			gCauseAryObj[ name ] = [];
		}

		gCauseAryObj[ name ].push( vol );
		if ( gCauseAryObj[ name ].length > period ){
			var start = gCauseAryObj[ name ].length - period;
			gCauseAryObj[ name ] = gCauseAryObj[ name ].slice( start , period + 1 );
		}

		var total = 0;
		for ( var i = 0 ; i < gCauseAryObj[ name ].length ; i++ ){
			total += gCauseAryObj[ name ][ i ];
		}
		var avg = total / gCauseAryObj[ name ].length;

		if ( vol > avg ){
			avg = vol;
			for ( var i = 0 ; i < gCauseAryObj[ name ].length ; i++ ){
				gCauseAryObj[ name ][ i ] = vol;
			}
		}

		checkUpdateModeCause( name , avg , avg );
	}

	function updateModeNetPosition(){
		var name = "NetPosition";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}

		var ecn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
		var sym = readParmAry( gConfOrderAry , "TradeSym" ).select;
		var netPos = getSymNetPos( ecn , sym ).netPos;

		checkUpdateModeCause( name , netPos , netPos );
	}

	function updateModeAbsPosition(){
		var name = "AbsPosition";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}

		var ecn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
		var sym = readParmAry( gConfOrderAry , "TradeSym" ).select;
		var netPos = getSymNetPos( ecn , sym ).netPos;
		var absPos = Math.abs( netPos );

		if ( netPos == 0 ){
			var nBidStatus = "";
			var nAskStatus = "";
		}else if ( netPos < 0 ){
			var nBidStatus = "";
			var nAskStatus = absPos;
		}else if ( netPos > 0 ){
			var nBidStatus = absPos;
			var nAskStatus = "";
		}

		checkUpdateModeCause( name , nBidStatus , nAskStatus );
	}

	function updateModeOppAbsPosition(){
		var name = "OppAbsPosition";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}

		var ecn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
		var sym = readParmAry( gConfOrderAry , "TradeSym" ).select;
		var netPos = getSymNetPos( ecn , sym ).netPos;
		var absPos = Math.abs( netPos );

		if ( netPos == 0 ){
			var nBidStatus = "";
			var nAskStatus = "";
		}else if ( netPos > 0 ){
			var nBidStatus = "";
			var nAskStatus = absPos;
		}else if ( netPos < 0 ){
			var nBidStatus = absPos;
			var nAskStatus = "";
		}

		checkUpdateModeCause( name , nBidStatus , nAskStatus );
	}

	function updateModeFillProportion(){
		var name = "FillProportion";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}

		var causeAry = readParmAry( gModeCauseAry , name );
		var tick = causeAry.propParm1;
		if ( gMAQuoteAry.length < tick ){
			return;
		}

		var ary = gMAQuoteAry.slice( 0 , tick );
		var rlbidVol = ary.reduce( sumRlBidVol ).rlBidVol;
		var rlAskVol = ary.reduce( sumRlAskVol ).rlAskVol;

		if ( rlbidVol == 0 && rlAskVol == 0 ){
			var prop = 50;
		}else{
			var prop = rlbidVol / ( rlbidVol + rlAskVol ) * 100;
		}

		checkUpdateModeCause( name , prop , prop );

		function sumRlBidVol( a , b ){
			return { rlBidVol: a.rlBidVol + b.rlBidVol };
		}
		function sumRlAskVol( a , b ){
			return { rlAskVol: a.rlAskVol + b.rlAskVol };
		}
	}

	function updateModeFillCount(){
		var name = "FillCount";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}
		var causeAry = readParmAry( gModeCauseAry , name );
		var tick = causeAry.propParm1;
		if ( gMAQuoteAry.length < tick ){
			return;
		}

		var ary = gMAQuoteAry.slice( 0 , tick );
		var rlbidVol = ary.reduce( sumRlBidVol ).rlBidVol;
		var rlAskVol = ary.reduce( sumRlAskVol ).rlAskVol;

		checkUpdateModeCause( name , rlbidVol , rlAskVol );

		function sumRlBidVol( a , b ){
			return { rlBidVol: a.rlBidVol + b.rlBidVol };
		}
		function sumRlAskVol( a , b ){
			return { rlAskVol: a.rlAskVol + b.rlAskVol };
		}
	}

	function updateModePriceSolidness( obj ){
		var preObj = gMAQuoteAry[ 1 ];
		if ( typeof preObj == "undefined" ){
			return;
		}
		var name = "PriceSolidness";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}
		var causeAry = readParmAry( gModeCauseAry , name );
		var decline = causeAry.propParm1;
		if ( decline < 100 ){
			decline = 1 / ( 1 - ( decline / 100 ) );
		}

		var bidStatus = causeAry.bidStatus;
		var askStatus = causeAry.askStatus;
		if ( bidStatus == "" ){
			var bidVol = 0;
		}else{
			var bidVol = parseFloat( bidStatus );
		}
		if ( askStatus == "" ){2
			var askVol = 0;
		}else{
			var askVol = parseFloat( askStatus );
		}

		bidVol += preObj.rlBidVol;
		askVol += preObj.rlAskVol;

		var bidChg = ( preObj.bid - obj.bid ) / obj.ts;
		var askChg = ( preObj.ask - obj.ask ) / obj.ts;

		if ( bidChg > 0 ){
			bidVol = bidVol / Math.pow( decline , Math.abs( bidChg ) );
		}
		if ( askChg < 0 ){
			askVol = askVol / Math.pow( decline , Math.abs( askChg ) );
		}

		checkUpdateModeCause( name , bidVol , askVol );
	}

	function updateModePriceIndicator( obj ){
		var preObj = gMAQuoteAry[ 1 ];
		if ( typeof preObj == "undefined" ){
			return;
		}
		var name = "PriceIndicator";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}

		var causeAry = readParmAry( gModeCauseAry , name );
		var bidVol = causeAry.bidVol;
		var askVol = causeAry.askVol;
		var decline = causeAry.propParm1;

		if ( decline < 100 ){
			decline = 1 / ( 1 - ( decline / 100 ) );
		}

		bidVol += preObj.rlBidVol;
		askVol += preObj.rlAskVol;

		var bidChg = ( preObj.bid - obj.bid ) / obj.ts;
		var askChg = ( preObj.ask - obj.ask ) / obj.ts;

		if ( bidChg > 0 ){
			bidVol = Math.round( bidVol / Math.pow( decline , Math.abs( bidChg ) ) , 2 );
		}

		if ( askChg < 0 ){
			askVol = Math.round( askVol / Math.pow( decline , Math.abs( askChg ) ) , 2 );
		}

		editParmAry( gModeCauseAry , name , "bidVol" , bidVol );
		editParmAry( gModeCauseAry , name , "askVol" , askVol );

		var indicator = bidVol - askVol;
		checkUpdateModeCause( name , indicator , indicator );
	}

	function updateModeVolumeSignal( obj ){
		var name = "VolumeSignal";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}

		var causeAry = readParmAry( gModeCauseAry , name );
		var status = causeAry.bidStatus;
		var tick = causeAry.propParm1;
		var volLimit = causeAry.propParm2;
		var reset = causeAry.propParm3;
		var triggered = causeAry.triggered;

		if ( gMAQuoteAry.length < tick ){
			return;
		}

		var preObj = gMAQuoteAry[ tick - 1 ];
		var vol = obj.totalVol - preObj.totalVol;

		if ( vol < volLimit ){
			if ( triggered ){
				editParmAry( gModeCauseAry , name , "triggered" , false );
			}
			return;
		}

		if ( triggered ){
			return;
		}

		var mid = ( obj.bid + obj.ask ) / 2;
		var preMid = ( preObj.bid + preObj.ask ) / 2;

		if ( mid > preMid ){
			if ( status < 0 || status == "" ){
				var nStatus = 1;
			}else{
				var nStatus = status + 1;
			}
		}else if ( mid < preMid ){
			if ( status > 0 || status == "" ){
				var nStatus = -1;
			}else{
				var nStatus = status - 1;
			}
		}else{
			return;
		}

		checkUpdateModeCause( name , nStatus , nStatus );
		resetModeTimer( name , reset );
	}

	function updateModeVolatilitySignal( obj ){
		var name = "VolatilitySignal";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}

		var causeAry = readParmAry( gModeCauseAry , name );
		var status = causeAry.bidStatus;
		var tick = causeAry.propParm1;
		var volLimit = causeAry.propParm2;
		var reset = causeAry.propParm3;
		var triggered = causeAry.triggered;

		if ( gQuoteAry.length < tick ){
			return;
		}

		var volAry = gQuoteAry.slice( 0 , tick );
		var bidMax = Math.max.apply( "" , volAry.map( function( o ){ return o.bid ; } ) );
		var bidMin = Math.min.apply( "" , volAry.map( function( o ){ return o.bid ; } ) );
		var askMax = Math.max.apply( "" , volAry.map( function( o ){ return o.ask ; } ) );
		var askMin = Math.min.apply( "" , volAry.map( function( o ){ return o.ask ; } ) );
		var bidVol = ( bidMax - bidMin ) / obj.ts;
		var askVol = ( askMax - askMin ) / obj.ts;
		var maxVol = Math.max( bidVol , askVol );

		if ( maxVol < volLimit ){
			if ( triggered ){
				editParmAry( gModeCauseAry , name , "triggered" , false );
			}
			return;
		}

		if ( triggered ){
			return;
		}

		var preObj = gQuoteAry[ tick - 1 ];
		var mid = ( obj.bid + obj.ask ) / 2;
		var preMid = ( preObj.bid + preObj.ask ) / 2;

		if ( mid > preMid ){
			if ( status < 0 || status == "" ){
				var nStatus = 1;
			}else{
				var nStatus = status + 1;
			}
		}else if ( mid < preMid ){
			if ( status > 0 || status == "" ){
				var nStatus = -1;
			}else{
				var nStatus = status - 1;
			}
		}else{
			return;
		}

		checkUpdateModeCause( name , nStatus , nStatus );
		resetModeTimer( name , reset );
	}

	function updateModeMASpreadSignal( obj ){
		var name = "MASpreadSignal";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}

		var preObj = gMAQuoteAry[ 9 ];
		if ( typeof preObj == "undefined" ){
			return;
		}

		var causeAry = readParmAry( gModeCauseAry , name );
		var status = causeAry.bidStatus;
		var spread = causeAry.propParm1;
		var reset = causeAry.propParm2;
		var triggered = causeAry.triggered;

		if ( spread == "" || reset == "" ){
			return;
		}

		if ( obj.maSpreadPip < spread ){
			if ( triggered ){
				editParmAry( gModeCauseAry , name , "triggered" , false );
			}
			return;
		}

		if ( triggered ){
			return;
		}

		var mid = ( obj.bid + obj.ask ) / 2;
		var preMid = ( preObj.bid + preObj.ask ) / 2;

		if ( mid > preMid ){
			if ( status < 0 || status == "" ){
				var nStatus = 1;
			}else{
				var nStatus = status + 1;
			}
		}else if ( mid < preMid ){
			if ( status > 0 || status == "" ){
				var nStatus = -1;
			}else{
				var nStatus = status - 1;
			}
		}else{
			return;
		}

		checkUpdateModeCause( name , nStatus , nStatus );
		resetModeTimer( name , reset );
	}

	function updateModeSwingDistance(){
		var name = "SwingDistance";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}
		var tick = readParmAry( gModeCauseAry , name ).propParm1;
		var ary = gQuoteAry.slice( 0 , tick );

		var fillIndex = ary.map( function( o ){ return o.filled ; } ).indexOf( true );

		if ( fillIndex != -1 ){
			ary = ary.slice( 0 , fillIndex );
		}

		var bid = ary[ 0 ].bid;
		var ask = ary[ 0 ].ask;
		var ts = ary[ 0 ].ts;
		var bidMax = Math.max.apply( "" , ary.map( function( o ){ return o.bid ; } ) );
		var bidMin = Math.min.apply( "" , ary.map( function( o ){ return o.bid ; } ) );
		var askMax = Math.max.apply( "" , ary.map( function( o ){ return o.ask ; } ) );
		var askMin = Math.min.apply( "" , ary.map( function( o ){ return o.ask ; } ) );

		var bidMaxDis = bidMax - bid;
		var bidMinDis = bid - bidMin;
		if ( bidMaxDis >= bidMinDis ){
			var nBidStatus = bidMaxDis / ts;
		}else{
			var nBidStatus = bidMinDis * -1 / ts;
		}

		var askMaxDis = askMax - ask;
		var askMinDis = ask - askMin;
		if ( askMaxDis >= askMinDis ){
			var nAskStatus = askMaxDis * -1 / ts;
		}else{
			var nAskStatus = askMinDis / ts;
		}

		checkUpdateModeCause( name , nBidStatus , nAskStatus );
	}

	function updateModeNetPosProfit(){
		var name = "PositionProfit";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}
		var ecn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
		var sym = readParmAry( gConfOrderAry , "TradeSym" ).select;

		var netPos = getSymNetPos( ecn , sym ).netPos;
		var avgPx = getSymNetPos( ecn , sym ).avgPx;
		var bidPx = parseFloat( getSymBidAskPx( ecn , sym )[ 0 ] );
		var askPx = parseFloat( getSymBidAskPx( ecn , sym )[ 1 ] );
		var ts = parseFloat( getSymItem( ecn , sym ).ts );

		if ( isNaN( netPos ) || isNaN( avgPx ) || isNaN( bidPx ) || isNaN( askPx ) || isNaN( ts ) ){
			return;
		}

		var nBidStatus = 0;
		var nAskStatus = 0;

		if ( netPos > 0 ){
			nBidStatus = ( bidPx - avgPx ) / ts;
			nAskStatus = ( askPx - avgPx ) / ts;
			if ( nBidStatus > 0 ){
				nBidStatus = 0;
			}
			if ( nAskStatus < 0 ){
				nAskStatus = 0;
			}
		}else if ( netPos < 0 ){
			nBidStatus = ( avgPx - bidPx ) / ts;
			nAskStatus = ( avgPx - askPx ) / ts;
			if ( nBidStatus < 0 ){
				nBidStatus = 0;
			}
			if ( nAskStatus > 0 ){
				nAskStatus = 0;
			}
		}

		checkUpdateModeCause( name , nBidStatus , nAskStatus );
	}

	function updateModeNetPosLoss(){
		var name = "PositionLoss";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}

		var ecn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
		var sym = readParmAry( gConfOrderAry , "TradeSym" ).select;

		var netPos = getSymNetPos( ecn , sym ).netPos;
		var avgPx = getSymNetPos( ecn , sym ).avgPx;
		var bidPx = parseFloat( getSymBidAskPx( ecn , sym )[ 0 ] );
		var askPx = parseFloat( getSymBidAskPx( ecn , sym )[ 1 ] );
		var ts = parseFloat( getSymItem( ecn , sym ).ts );

		if ( isNaN( netPos ) || isNaN( avgPx ) || isNaN( bidPx ) || isNaN( askPx ) || isNaN( ts ) ){
			return;
		}

		var nBidStatus = 0;
		var nAskStatus = 0;

		if ( netPos > 0 ){
			nBidStatus = ( bidPx - avgPx ) / ts;
			nAskStatus = ( askPx - avgPx ) / ts;
			if ( nBidStatus < 0 ){
				nBidStatus = 0;
			}
			if ( nAskStatus > 0 ){
				nAskStatus = 0;
			}
		}else if ( netPos < 0 ){
			nBidStatus = ( avgPx - bidPx ) / ts;
			nAskStatus = ( avgPx - askPx ) / ts;
			if ( nBidStatus > 0 ){
				nBidStatus = 0;
			}
			if ( nAskStatus < 0 ){
				nAskStatus = 0;
			}
		}

		checkUpdateModeCause( name , nBidStatus , nAskStatus );
	}

	function updateModePnL(){
		var pnl = getAccPnL();
		var lossFromTop = pnl - gMaxProfit;

		updateModeSimpleCause( "ProfitAndLoss" , pnl , pnl );
		updateModeSimpleCause( "LossFromTop" , lossFromTop , lossFromTop );
	}

	function updateModeManualAdject(){
		var name = "ManualShift";
		updateModeCause( name );

		var name = "ManualSpread";
		updateModeCause( name );
	}

	function updateModeAiDirection( m ){
		var name = "AiDirection";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}

		var side = parseFloat( m[ 6 ] );
		var noPct = parseFloat( m[ 7 ] );
		var upPct = parseFloat( m[ 8 ] );
		var downPct = parseFloat( m[ 9 ] );
		var reset = readParmAry( gModeCauseAry , name ).propParm1;

		if ( side == 1 ){
			var indicator = upPct * 100;
		}else if ( side == 2 ){
			var indicator = downPct * 100 * -1;
		}else if ( side == 0 ){
			var indicator = 0;
		}else{
			return;
		}

		checkUpdateModeCause( name , indicator , indicator );
		resetModeTimer( name , reset );
	}

	function updateModeAiVolatility( m ){
		var name = "AiVolatility";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}

		var reset = readParmAry( gModeCauseAry , name ).propParm1;
		var vol = parseFloat( m[ 4 ] );

		checkUpdateModeCause( name , vol , vol );
		resetModeTimer( name , reset );
	}

	function updateModeAiVolatility2( m ){
		var name = "AiVolatility2";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}

		var vol = parseFloat( m[ 4 ] );

		checkUpdateModeCause( name , vol , vol );
		resetModeTimer( name , reset );
	}

	function updateModeTradeTimer(){
		var name = "TradeTimer";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}
		if ( !gAlgoStatusObj.on ){
			return;
		}
		var causeAry = readParmAry( gModeCauseAry , name );
		var bidStatus = causeAry.bidStatus;
		var askStatus = causeAry.askStatus;

		var ecn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
		var sym = readParmAry( gConfOrderAry , "TradeSym" ).select;
		var netPos = getSymNetPos( ecn , sym ).netPos;

		var nBidStatus = 0;
		var nAskStatus = 0;

		if ( netPos > 0 ){
			if ( askStatus == "" ){
				askStatus = 0;
			}
			nAskStatus = askStatus + 1
		}else if ( netPos < 0 ){
			if ( bidStatus == "" ){
				bidStatus = 0;
			}
			nBidStatus = nBidStatus + 1
		}

		checkUpdateModeCause( name , nBidStatus , nAskStatus );
	}

	function updateModeTradeCount( cmrAry ){
		// CMR 314 TOTAL_FILL MREX CME/CMX_GLD/AUG18 16 17 SELL 1 1297.7 OPX 1297.70000 NAME a_orderMac_main_ask_1 // noraml algo order
		// CMR 301 TOTAL_FILL PATX CME/CMX_GLD/AUG18 14 15 SELL 1 1305.7 OPX 1305.70000 NAME NIL // algo order unable to cancel
		// CMR 314 TOTAL_FILL MREX CME/CMX_GLD/AUG18 1 1 BUY 1 1301.7 OPX NIL NAME NIL // ui hit
		var name = "TradeCount";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}
		if ( cmrAry.length < 14 ){
			return;
		}

		var causeAry = readParmAry( gModeCauseAry , name );
		var reset = causeAry.propParm1;

		var ecn = cmrAry[ 3 ];
		var sym = cmrAry[ 4 ];
		var side = cmrAry[ 7 ];
		var orderPx = cmrAry[ 11 ];
		var macName = cmrAry[ 13 ];

		var bidOrAsk = macName.split( "_" )[ 3 ];
		if ( macName == "NIL" ){
			if ( side == "BUY" ){
				bidOrAsk = "bid";
			}else if ( side == "SELL" ){
				bidOrAsk = "ask";
			}
		}

		if ( side == "BUY" && bidOrAsk != "bid" ){
			return;
		}
		if ( side == "SELL" && bidOrAsk != "ask" ){
			return;
		}
		if ( orderPx == "UNKNOWN" || orderPx == "NIL" ){
			return;
		}

		var rEcn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
		var rSym = readParmAry( gConfOrderAry , "TradeSym" ).select;

		if ( ecn != rEcn ){
			return;
		}
		if ( rSym != sym ){
			return;
		}

		var status = bidOrAsk + "Status";
		var count = parseInt( causeAry[ status ] );

		if ( isNaN( count ) ){
			count = 0;
		}
		count++;

		editParmAry( gModeCauseAry , name , status , count );
		updateModeCause( name );

		setTimeout( function (){
			var count = causeAry[ status ] - 1;
			if ( count < 0 ){
				editParmAry( gModeCauseAry , name , status , 0 );
			}else{
				editParmAry( gModeCauseAry , name , status , count.toFixed() );
			}
			updateModeCause( name );
		} , reset * 1000 );
	}

	function updateModeResistNSupport(){
		var name = "ResistanceNSupport";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}
		var causeAry = readParmAry( gModeCauseAry , name );

		var high = causeAry.propParm1;
		var low = causeAry.propParm2;

		var ecn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
		var sym = readParmAry( gConfOrderAry , "TradeSym" ).select;

		var bidPx = parseFloat( getSymBidAskPx( ecn , sym )[ 0 ] );
		var askPx = parseFloat( getSymBidAskPx( ecn , sym )[ 1 ] );
		var ts = parseFloat( getSymItem( ecn , sym ).ts );

		if ( isNaN( bidPx ) || isNaN( askPx ) || bidPx == 0 || askPx == 0 || isNaN( ts ) ){
			return;
		}

		if ( isNaN( high ) ){
			var highDis = "";
		}else{
			var highDis = ( ( high - askPx ) / ts ).toFixed();
		}

		if ( isNaN( low ) ){
			var lowDis = "";
		}else{
			var lowDis = ( ( bidPx - low ) / ts ).toFixed();
		}

		checkUpdateModeCause( name , lowDis , highDis );

		if ( highDis >= 0 && lowDis >= 0 ){
			return;
		}

		$.when( checkIsLeader() ).then( function (){
			if ( !gLeaderObj.isLeader ){
				return;
			}
			if ( highDis < 0 ){
				var macName = "a_mCauseCfg_" + name + "_propParm1";
				setMacro( macName , askPx );
			}else if ( lowDis < 0 ){
				var macName = "a_mCauseCfg_" + name + "_propParm2";
				setMacro( macName , bidPx );
			}
		} );
	}

	function updateModeResistNSupport2(){
		var name = "ResistanceNSupport2";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}
		var causeAry = readParmAry( gModeCauseAry , name );
		var high = causeAry.propParm1;
		var low = causeAry.propParm2;

		var ecn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
		var sym = readParmAry( gConfOrderAry , "TradeSym" ).select;

		var bidPx = parseFloat( getSymBidAskPx( ecn , sym )[ 0 ] );
		var askPx = parseFloat( getSymBidAskPx( ecn , sym )[ 1 ] );
		var ts = parseFloat( getSymItem( ecn , sym ).ts );

		if ( isNaN( bidPx ) || isNaN( askPx ) || bidPx == 0 || askPx == 0 || isNaN( ts ) ){
			return;
		}

		var highDis = ( high - askPx ) / ts;
		var lowDis = ( bidPx - low ) / ts;

		var nStatus = Math.min( highDis , lowDis );

		checkUpdateModeCause( name , nStatus , nStatus );

		if ( highDis >= 0 && lowDis >= 0 ){
			return;
		}
		$.when( checkIsLeader() ).then( function (){
			if ( !gLeaderObj.isLeader ){
				return;
			}
			if ( highDis < 0 ){
				var macName = "a_mCauseCfg_" + name + "_propParm1";
				setMacro( macName , askPx );
			}else if ( lowDis < 0 ){
				var macName = "a_mCauseCfg_" + name + "_propParm2";
				setMacro( macName , bidPx );
			}
		} );
	}

	function updateModePriceDistance(){
		var name = "PriceDistance";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}
		var causeAry = readParmAry( gModeCauseAry , name );

		var high = causeAry.propParm1;
		var low = causeAry.propParm2;
		var oHighDis = causeAry.askStatus;
		var oLowDis = causeAry.bidStatus;

		var ecn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
		var sym = readParmAry( gConfOrderAry , "TradeSym" ).select;

		var bidPx = parseFloat( getSymBidAskPx( ecn , sym )[ 0 ] );
		var askPx = parseFloat( getSymBidAskPx( ecn , sym )[ 1 ] );
		var ts = parseFloat( getSymItem( ecn , sym ).ts );

		if ( isNaN( bidPx ) || isNaN( askPx ) || bidPx == 0 || askPx == 0 || isNaN( ts ) ){
			return;
		}

		if ( high == "" ){
			var highDis = "";
		}else{
			var highDis = ( high - askPx ) / ts;
		}

		if ( low == "" ){
			var lowDis = "";
		}else{
			var lowDis = ( bidPx - low ) / ts;
		}

		checkUpdateModeCause( name , lowDis , highDis );
	}

	function updateModeFilledTwiceSignal( msg ){
		var name = "FilledTwiceSignal";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}
		var causeAry = readParmAry( gModeCauseAry , name );
		var reset = causeAry.propParm1;

		var side = msg.split( " " )[ 0 ];
		if ( side != "BUY" && side != "SELL" ){
			return;
		}

		if ( side == "BUY" ){
			var status = -1;
		}else{
			var status = 1;
		}

		checkUpdateModeCause( name , status , status );
		resetModeTimer( name , reset );
	}

	function updateModeMinuteVolatility( ary ){
		var name = "MinuteVolatility";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}
		var causeAry = readParmAry( gModeCauseAry , name );
		var minute = causeAry.propParm1;

		var ecn = readParmAry( gConfOrderAry , "RefEcn" ).select;
		var sym = readParmAry( gConfOrderAry , "RefSym" ).select;
		var ts = parseFloat( getSymItem( ecn , sym ).ts );

		ary = ary.slice( 0 , minute );

		var totalVol = 0;
		for ( var i = 0 ; i < ary.length ; i++ ){
			var high = parseFloat( ary[ i ][ 2 ] );
			var low = parseFloat( ary[ i ][ 3 ] );
			totalVol += ( high - low ) / ts;
		}
		var avgVol = Math.round( totalVol / ary.length * 100 ) / 100;

		checkUpdateModeCause( name , avgVol , avgVol );
	}

	function updateModeAiPvr1( m ){
		var name = "AiPVR";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}
		var causeAry = readParmAry( gModeCauseAry , name );
		var reset = causeAry.propParm1;

		var pastPredict = parseFloat( m[ 8 ] );
		var pastActual = parseFloat( m[ 10 ] );
		var curPredict = parseFloat( m[ 4 ] );
		var ind = curPredict;

		if ( pastPredict == pastActual ){
			return;
		}

		if ( pastPredict == 0 || pastActual == 0 ){
			return;
		}

		if ( isNaN( curPredict ) ){
			return;
		}

		if ( curPredict == 2 ){
			ind = -1;
		}

		checkUpdateModeCause( name , ind , ind );
		resetModeTimer( name , reset );
	}

	function updateModeAiBatt2( m ){
		var name = "AiBatt2";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}
		var causeAry = readParmAry( gModeCauseAry , name );
		var reset = causeAry.propParm1;

		var ind = parseFloat( m[ 4 ] );
		var targetPx = parseFloat( m[ 6 ] );
		if ( ind == 2 ){
			ind = -1;
		}

		checkUpdateModeCause( name , ind , ind );

		clearTimeout( gTimerObj[ "mode" + name ] );
		gTimerObj[ "mode" + name ] = setTimeout( function (){
			var causeAry = readParmAry( gModeCauseAry , name );
			var bidStatus = causeAry.bidStatus;
			if ( bidStatus == 0 ){
				return;
			}

			var ecn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
			var sym = readParmAry( gConfOrderAry , "TradeSym" ).select;
			var netPos = getSymNetPos( ecn , sym ).netPos;
			if ( netPos != 0 ){
				return;
			}

			clearModeCaseStatus( name );
		} , reset * 1000 );
	}

	function updateModeAiBatt3( m ){
		var name = "AiBatt3";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}
		var causeAry = readParmAry( gModeCauseAry , name );
		var reset = causeAry.propParm1;

		var ind = parseFloat( m[ 4 ] );
		var targetPx = parseFloat( m[ 6 ] );
		if ( ind == 2 ){
			ind = -1;
		}

		checkUpdateModeCause( name , ind , ind );

		clearTimeout( gTimerObj[ "mode" + name ] );
		gTimerObj[ "mode" + name ] = setTimeout( function (){
			var causeAry = readParmAry( gModeCauseAry , name );
			var bidStatus = causeAry.bidStatus;
			if ( bidStatus == 0 ){
				return;
			}

			var ecn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
			var sym = readParmAry( gConfOrderAry , "TradeSym" ).select;
			var netPos = getSymNetPos( ecn , sym ).netPos;
			if ( netPos != 0 ){
				return;
			}

			clearModeCaseStatus( name );
		} , reset * 1000 );
	}

	function updateModeMovingAvgSlope( ary ){
		var name = "MovingAvgSlope";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}
		var causeAry = readParmAry( gModeCauseAry , name );
		var minute = causeAry.propParm1;
		var wrongData = false;

		var ary1 = ary.slice( 0 , minute );
		var total1 = 0
		for ( var i = 0 ; i < ary1.length ; i++ ){
			var close = parseFloat( ary1[ i ][ 4 ] );
			if ( close == 0 ){
				wrongData = true;
				break;
			}
			total1 += close;
		}

		var ary2 = ary.slice( 1 , minute + 1 );
		var total2 = 0
		for ( var i = 0 ; i < ary2.length ; i++ ){
			var close = parseFloat( ary2[ i ][ 4 ] );
			if ( close == 0 ){
				wrongData = true;
				break;
			}
			total2 += close;
		}

		if ( total1 == 0 || total2 == 0 || wrongData ){
			var slope = 0;
		}else{
			var avg1 = total1 / ary1.length;
			var avg2 = total2 / ary2.length;
			var slope = Math.round( ( ( avg1 / avg2 ) - 1 ) * 1000000 ) / 10;
		}

		checkUpdateModeCause( name , slope , slope );
	}

	function updateModeLongestMinuteBar( ary ){
		var name = "LongestMinuteBar";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}
		var causeAry = readParmAry( gModeCauseAry , name );
		var minute = causeAry.propParm1;

		var ary = ary.slice( 0 , minute );
		var upMax = 0;
		var downMax = 0;

		var ecn = readParmAry( gConfOrderAry , "RefEcn" ).select;
		var sym = readParmAry( gConfOrderAry , "RefSym" ).select;
		var ts = parseFloat( getSymItem( ecn , sym ).ts );

		for ( var i = 0 ; i < ary.length ; i++ ){
			var open = parseFloat( ary[ i ][ 1 ] );
			var high = parseFloat( ary[ i ][ 2 ] );
			var low = parseFloat( ary[ i ][ 3 ] );
			var close = parseFloat( ary[ i ][ 4 ] );
			var length = high - low;

			if ( open < close && length > upMax ){
				upMax = length;
			}else if ( open > close && length > downMax ){
				downMax = length;
			}
		}

		if ( upMax > downMax ){
			var nBidStatus = upMax / ts;
			var nAskStatus = upMax / ts * -1;
		}else if ( downMax > upMax ){
			var nBidStatus = downMax / ts * -1;
			var nAskStatus = downMax / ts;
		}else{
			var nBidStatus = 0;
			var nAskStatus = 0;
		}

		checkUpdateModeCause( name , nBidStatus , nAskStatus );
	}

	function updateModeLossFromTarget(){
		var name = "LossFromTarget";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}
		var causeAry = readParmAry( gModeCauseAry , name );
		var target = causeAry.propParm1;
		var bidStatus = causeAry.bidStatus;
		var askStatus = causeAry.askStatus;
		var bidMaxProfit = causeAry.bidMaxProfit;
		var askMaxProfit = causeAry.askMaxProfit;

		var ecn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
		var sym = readParmAry( gConfOrderAry , "TradeSym" ).select;

		var netPos = getSymNetPos( ecn , sym ).netPos;
		var avgPx = getSymNetPos( ecn , sym ).avgPx;
		var bidPx = parseFloat( getSymBidAskPx( ecn , sym )[ 0 ] );
		var askPx = parseFloat( getSymBidAskPx( ecn , sym )[ 1 ] );
		var ts = parseFloat( getSymItem( ecn , sym ).ts );

		if ( isNaN( netPos ) || isNaN( avgPx ) || isNaN( bidPx ) || isNaN( askPx ) || isNaN( ts ) ){
			return;
		}

		var nBidStatus = 0;
		var nAskStatus = 0;
		var nBidMaxProfit = 0;
		var nAskMaxProfit = 0;

		if ( netPos > 0 ){
			nAskMaxProfit = ( askPx - avgPx ) / ts;
			if ( nAskMaxProfit < 0 ){
				nAskMaxProfit = 0;
			}
		}else if ( netPos < 0 ){
			nBidMaxProfit = ( avgPx - bidPx ) / ts;
			if ( nBidMaxProfit < 0 ){
				nBidMaxProfit = 0;
			}
		}else{
			if ( bidMaxProfit != 0 ){
				editParmAry( gModeCauseAry , name , "bidMaxProfit" , 0 );
			}
			if ( askMaxProfit != 0 ){
				editParmAry( gModeCauseAry , name , "askMaxProfit" , 0 );
			}
		}

		if ( nBidMaxProfit > bidMaxProfit ){
			bidMaxProfit = nBidMaxProfit;
			editParmAry( gModeCauseAry , name , "bidMaxProfit" , bidMaxProfit.toFixed() );
		}else if ( nAskMaxProfit > askMaxProfit ){
			askMaxProfit = nAskMaxProfit;
			editParmAry( gModeCauseAry , name , "askMaxProfit" , askMaxProfit.toFixed() );
		}

		if ( bidMaxProfit >= target ){
			var bidProfit = ( avgPx - bidPx ) / ts;
			var nBidStatus = bidProfit - bidMaxProfit;
		}else if ( askMaxProfit >= target ){
			var askProfit = ( askPx - avgPx ) / ts;
			var nAskStatus = askProfit - askMaxProfit;
		}

		checkUpdateModeCause( name , nBidStatus , nAskStatus );
	}

	function updateModeVolumeDirectionSignal(){
		var name = "VolumeDirectionSignal";
		if ( !checkModeCauseAxisNParm( name ) ){
			return;
		}
		var causeAry = readParmAry( gModeCauseAry , name );
		var tick = 10;
		var reset = causeAry.propParm1;
		var volTarget = causeAry.propParm2;
		var pips = causeAry.propParm3;
		var triggered = causeAry.triggered;

		if ( triggered ){
			return;
		}

		if ( gMAQuoteAry.length < tick ){
			return;
		}
		var vol = gMAQuoteAry[ 0 ].totalVol - gMAQuoteAry[ tick - 1 ].totalVol;
		if ( vol < volTarget ){
			return;
		}

		editParmAry( gModeCauseAry , name , "triggered" , true );
		resetMiddleInterval();
	}

	function updateModeVolumeDirectionSignalStep2( ary ){
		var name = "VolumeDirectionSignal";
		var causeAry = readParmAry( gModeCauseAry , name );
		if ( !causeAry.triggered ){
			return;
		}

		var reset = causeAry.propParm1;
		var pips = causeAry.propParm3;

		var bid = gMAQuoteAry[ 0 ].bid;
		var ask = gMAQuoteAry[ 0 ].ask;
		var ts = gMAQuoteAry[ 0 ].ts;

		var maxHighDis = 0;
		var maxLowDis = 0;
		var direction = 0;

		for ( var i = 0 ; i < ary.length ; i++ ){
			var high = parseFloat( ary[ i ][ 2 ] );
			var low = parseFloat( ary[ i ][ 3 ] );

			var highDis = ( high - ask ) / ts;
			var lowDis = ( bid - low ) / ts;

			if ( direction == 0 ){
				if ( highDis >= pips ){
					maxHighDis = highDis;
					direction = 1;
				}else if ( lowDis >= pips ){
					maxLowDis = lowDis;
					direction = -1;
				}
			}else if ( direction == 1 ){
				if ( highDis >= maxHighDis ){
					maxHighDis = highDis;
				}else{
					break;
				}
			}else if ( direction == -1 ){
				if ( lowDis >= maxLowDis ){
					maxLowDis = lowDis;
				}else{
					break;
				}
			}
		}

		if ( direction == 0 ){
			editParmAry( gModeCauseAry , name , "triggered" , false );
			return;
		}else if ( direction == 1 ){
			maxLowDis = maxHighDis * -1;
		}else if ( direction == -1 ){
			maxHighDis = maxLowDis * -1;
		}

		checkUpdateModeCause( name , maxLowDis , maxHighDis );
		resetModeTimer( name , reset );
	}

	function resetModeTradeTimer( cmrAry ){
		// e.g. CMR 300 TOTAL_FILL MREX CME/CMX_GLD/AUG17 15 46 SELL 1 1246.7 OPX 1246.4
		var ecn = cmrAry[ 3 ];
		var sym = cmrAry[ 4 ];
		var side = cmrAry[ 7 ];
		var orderPx = cmrAry[ 11 ];

		if ( orderPx == "UNKNOWN" || orderPx == "NIL" ){
			return;
		}

		var rEcn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
		var rSym = readParmAry( gConfOrderAry , "TradeSym" ).select;

		if ( rSym != sym ){
			return;
		}

		var name = "TradeTimer";
		if ( side == "BUY" ){
			editParmAry( gModeCauseAry , name , "bidStatus" , "" );
		}else{
			editParmAry( gModeCauseAry , name , "askStatus" , "" );
		}

		updateModeCause( name );
	}

	function readModeOrderMacro( cmrAry ){
		var name = cmrAry[ 3 ];
		var parm = cmrAry[ 5 ];

		var shift = parseFloat( name.split( "_" )[ 2 ] );
		var spread = parseFloat( name.split( "_" )[ 3 ] );
		var bidOrAsk = name.split( "_" )[ 4 ];
		var id = parseFloat( name.split( "_" )[ 5 ] )

		var qty = parseFloat( parm.split( "_" )[ 0 ] );
		var pips = parseFloat( parm.split( "_" )[ 1 ] );
		var update = parseFloat( parm.split( "_" )[ 2 ] );
		var replace = parseFloat( parm.split( "_" )[ 3 ] );
		var method = parm.split( "_" )[ 4 ];
		var methodSec = parseFloat( parm.split( "_" )[ 5 ] );

		var ts = parseFloat( getSymItem( readParmAry( gConfOrderAry , "TradeEcn" ).select , readParmAry( gConfOrderAry , "TradeSym" ).select ).ts );

		if ( bidOrAsk == "bid" ){
			pips = pips * -1;
		}

		var orderObj = { shift: shift ,
						spread: spread ,
						bidOrAsk: bidOrAsk ,
						id: id ,
						qty: qty ,
						pips: pips ,
						update: update ,
						replace: replace ,
						method: method ,
						methodSec: methodSec ,
		}

		var newOrder = true;

		for ( var i = 0 ; i < gModeOrderAry.length ; i++ ){
			var obj = gModeOrderAry[ i ];
			if ( obj.shift != shift ){
				continue;
			}
			if ( obj.spread != spread ){
				continue;
			}
			if ( obj.bidOrAsk != bidOrAsk ){
				continue;
			}
			if ( obj.id != id ){
				continue;
			}

			gModeOrderAry[ i ] = orderObj;
			newOrder = false;
			break;
		}

		if ( newOrder ){
			gModeOrderAry.push( orderObj );
		}
		getCurrentOrder();
		gModeOrderAry.push();
	}

	function readDeleteModeOrderMacro( cmrAry ){
		var name = cmrAry[ 3 ];

		var shift = parseInt( name.split( "_" )[ 2 ] );
		var spread = parseInt( name.split( "_" )[ 3 ] );
		var bidOrAsk = name.split( "_" )[ 4 ];
		var id = parseInt( name.split( "_" )[ 5 ] )

		for ( var i = 0 ; i < gModeOrderAry.length ; i++ ){
			var obj = gModeOrderAry[ i ];
			if ( obj.shift != shift ){
				continue;
			}
			if ( obj.spread != spread ){
				continue;
			}
			if ( obj.bidOrAsk != bidOrAsk ){
				continue;
			}
			if ( obj.id != id ){
				continue;
			}

			gModeOrderAry.splice( i , 1 );
			break;
		}
		getCurrentOrder();
	}

	function readModeOrderAction( cmrAry ){
		var name = cmrAry[ 3 ];
		var type = name.split( "_" )[ 1 ];
		var bidOrAsk = name.split( "_" )[ 2 ];
		var numStr = name.split( "_" )[ 3 ];
		var id = parseInt( numStr ) - 1;

		if ( typeof( vueCurrentOrder[ bidOrAsk + "OrderAry" ][ id ] ) == "undefined" ){
			return;
		}

		if ( type == "run" || type == "loop" ){
			Vue.set( vueCurrentOrder[ bidOrAsk + "OrderAry" ][ id ] , "on" , true );
		}else if ( type == "stop" ){
			Vue.set( vueCurrentOrder[ bidOrAsk + "OrderAry" ][ id ] , "on" , false );
		}

		vueCurrentOrder[ bidOrAsk + "OrderAry" ].push();
	}

	function readModeOrderStatus( cmrAry ){
		var status = cmrAry[ 2 ];
		var name = cmrAry[ 3 ];
		var group = name.split( "_" )[ 1 ];
		var type = name.split( "_" )[ 2 ];

		if ( group != "orderMac" ){
			return;
		}
		if ( type != "main" && type != "loop" ){
			return;
		}

		var bidOrAsk = name.split( "_" )[ 3 ];
		var numStr = name.split( "_" )[ 4 ];
		var id = parseInt( numStr ) - 1;

		if ( typeof( vueCurrentOrder[ bidOrAsk + "OrderAry" ][ id ] ) == "undefined" ){
			return;
		}

		if ( status == "ALGO-OFF" ){
			Vue.set( vueCurrentOrder[ bidOrAsk + "OrderAry" ][ id ] , "on" , false );
		}else if ( status == "ALGO-ON" ){
			Vue.set( vueCurrentOrder[ bidOrAsk + "OrderAry" ][ id ] , "on" , true );
		}
		vueCurrentOrder[ bidOrAsk + "OrderAry" ].push();
	}

	function readModeActionMacro( cmrAry ){
		var nameStr = cmrAry[ 3 ];
		var parmStr = cmrAry[ 5 ];

		var axis = nameStr.split( "_" )[ 2 ];
		var pos = parseFloat( nameStr.split( "_" )[ 3 ] );
		var name = nameStr.split( "_" )[ 4 ];

		var on = Boolean( parseFloat( parmStr.split( "_" )[ 0 ] ) );
		var parm = parseFloat( parmStr.split( "_" )[ 1 ] );

		if ( parm == 0 ){
			parm = "";
		}

		var actionObj = { axis: axis , pos: pos , name: name , on: on , parm: parm };

		var newAction = true;
		for ( var i = 0 ; i < gModeActionAry.length ; i++ ){
			var obj = gModeActionAry[ i ];
			if ( obj.axis != axis ){
				continue;
			}
			if ( obj.pos != pos ){
				continue;
			}
			if ( obj.name != name ){
				continue;
			}
			gModeActionAry[ i ] = actionObj;
			newAction = false;
		}

		if ( newAction ){
			gModeActionAry.push( actionObj );
		}
		gModeActionAry.push();
	}

	function readModePosition( cmrAry ){
		var name = cmrAry[ 3 ];
		var parm = cmrAry[ 5 ];

		var bidOrAsk = name.split( "_" )[ 1 ];
		var shift = parm.split( "_" )[ 0 ];
		var spread = parm.split( "_" )[ 1 ];

		gModePositionObj[ bidOrAsk + "Shift" ] = shift;
		gModePositionObj[ bidOrAsk + "Spread" ] = spread;
	}

	function readModeCauseCfg( cmrAry ){
		var macName = cmrAry[ 3 ];
		var macParm = cmrAry[ 5 ];
		var name = macName.split( "_" )[ 2 ];
		var property = macName.split( "_" )[ 3 ];

		if ( property == "on" ){
			macParm = Boolean( parseInt( macParm ) );
		}else if ( property != "direction" && property != "axis" ){
			macParm = parseFloat( macParm );
			if ( isNaN( macParm ) ){
				macParm = "";
			}
		}
		editParmAry( gModeCauseAry , name , property , macParm );
		gModeCauseAry.push();
	}

	function clearModeCaseStatus( name ){
		editParmAry( gModeCauseAry , name , "triggered" , false );
		editParmAry( gModeCauseAry , name , "bidStatus" , "" );
		editParmAry( gModeCauseAry , name , "askStatus" , "" );
		updateModeCause( name );
		showLog( "clearModeCaseStatus" , "clear case " + name );
	}

	function runModeAction( obj , shiftPos , spreadPos , bidOrAsk , index ){
		if ( obj.name == "coolDown" ){
			coolDown( obj.parm );
		}else if ( obj.name == "notice" ){
			notice();
		}else if ( obj.name == "pauseOne" ){
			pause( bidOrAsk );
		}else if ( obj.name == "pauseAll" ){
			pause( "bid" );
			pause( "ask" );
		}else if ( obj.name == "off" ){
			turnOffPanel();
		}else if ( obj.name == "flatOne" ){
			flat( bidOrAsk );
		}else if ( obj.name == "flatAll" ){
			flat( "bid" );
			flat( "ask" );
		}else if ( obj.name == "follow" || obj.name == "counter" ){
			setTimeout( function (){
				trade( obj.name , bidOrAsk );
			} , 500 );
		}else if ( obj.name == "resetSignal"){
			clearModeCaseStatus( "AiBatt2" );
			clearModeCaseStatus( "AiBatt3" );
		}

		function coolDown( time ){
			gModeActionAry[ index ].triggered = true;

			if ( obj.axis == "Shift" ){
				var axisNum = shiftPos;
			}else if ( obj.axis == "Spread" ){
				var axisNum = spreadPos;
			}
			var id = "#modeCoolDown" + obj.axis + axisNum;

			counter();
			var interval = setInterval( counter , 1000 );
			function counter(){
				if ( time <= 0 ){
					clearInterval( interval );
					$( id ).html( "" );
					gModeActionAry[ index ].triggered = false;
					return;
				}

				$( id ).html( time );
				time = time - 1;
			}
		}
		function notice(){
			var bidOrAskStr = bidOrAsk.charAt( 0 ).toUpperCase() + bidOrAsk.slice( 1 );
			var msg = bidOrAskStr + " Mode: "+ spreadPos + " , " + shiftPos + ". ";
			modeMsg( msg );
		}
		function pause( bidOrAsk ){
			changeModeStatus( bidOrAsk , "On" , false );
			changeModeStatus( bidOrAsk , "Pause" , true );

			if ( obj.name != "pauseOne" && obj.name != "pauseAll" ){
				var resume = 1;
			}else{
				var resume = obj.parm ;
			}

			var bidOrAskStr = bidOrAsk.charAt( 0 ).toUpperCase() + bidOrAsk.slice( 1 );
			var msg = "Pause " + bidOrAskStr + " Mode for " + resume + " seoond. ";
			modeMsg( msg );

			clearTimeout( gModePauseTimer[ bidOrAsk ] );
			gModePauseTimer[ bidOrAsk ] = setTimeout( function (){
				if ( !gAlgoStatusObj[ bidOrAsk + "Pause" ] ){
					return;
				}
				changeModeStatus( bidOrAsk , "On" , true );

				var msg = "Resume " + bidOrAskStr + " Mode. ";
				modeMsg( msg );
			} , resume * 1000 );
		}
		function flat( bidOrAsk ){
			var ecn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
			var sym = readParmAry( gConfOrderAry , "TradeSym" ).select;
			var netPos = getSymNetPos( ecn , sym ).netPos;
			if ( netPos == 0 || ! $.isNumeric( netPos ) ){
				return;
			}
			if ( bidOrAsk == "bid" && netPos < 0 ){
				return;
			}
			if ( bidOrAsk == "ask" && netPos > 0 ){
				return;
			}
			if ( obj.parm == 0 ){
				return;
			}
			if ( gAlgoStatusObj[ bidOrAsk + "On" ] ){
				pause( bidOrAsk );
			}
			if ( bidOrAsk == "bid" ){
				var posStr = "Long";
			}else if ( bidOrAsk == "ask" ){
				var posStr = "Short";
			}
			clearTimeout( gTimerObj.flatPosition );
			gTimerObj.flatPosition = setTimeout( function (){
				squarePosition( obj.parm , gDefAutoFlatSlip );
			} , 300 );

			var msg = "Flat " + obj.parm + "% of " + posStr + " position. ";
			modeMsg( msg );
		}
		function trade( action , bidOrAsk ){
			var ecn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
			var sym = readParmAry( gConfOrderAry , "TradeSym" ).select;
			var type = "LO2";
			var tif = "DAY";

			var bidPx = getSymBidAskPx( ecn , sym )[ 0 ];
			var askPx = getSymBidAskPx( ecn , sym )[ 1 ];
			var ts = parseFloat( getSymItem( ecn , sym ).ts );
			var dip = parseFloat( getSymItem( ecn , sym ).dip );
			var ls = parseFloat( getSymItem( ecn , sym ).ls );
			var slip = gDefAutoFlatSlip;

			if ( action == "follow" ){
				if ( bidOrAsk == "bid" ){
					var side = "SELL";
					var px = bidPx - ( slip * ts );
				}else if ( bidOrAsk == "ask" ){
					var side = "BUY";
					var px = askPx + ( slip * ts );
				}
			}else if ( action == "counter" ){
				if ( bidOrAsk == "bid" ){
					var side = "BUY";
					var px = askPx + ( slip * ts );
				}else if ( bidOrAsk == "ask" ){
					var side = "SELL";
					var px = bidPx - ( slip * ts );
				}
			}
			px = px.toFixed( dip );
			var qty = ( obj.parm * ls ).toFixed();

			submitOrder( ecn , sym , type , tif , side , qty , px );

			var actionStr = action.charAt( 0 ).toUpperCase() + action.slice( 1 );

			var msg = actionStr + " " + obj.parm + " Lots ( " + side + " ). ";
			modeMsg( msg );
		}

		function modeMsg( msg ){
			var toastrType = "success";
			var toastrTopic = "AlgoStatus:";
			var soundType = "notice";

			sendTabMsg( msg );
			sendToastrMsg( toastrType , toastrTopic , msg );
			sendSoundMsg( soundType );
		}
	}

	function resetModeTimer( name , reset ){
		if ( typeof gTimerObj[ "mode" + name ] == "undefined" ){
			return;
		}

		clearTimeout( gTimerObj[ "mode" + name ] );
		gTimerObj[ "mode" + name ] = setTimeout( function (){
			editParmAry( gModeCauseAry , name , "triggered" , false );
			editParmAry( gModeCauseAry , name , "bidStatus" , "" );
			editParmAry( gModeCauseAry , name , "askStatus" , "" );
			updateModeCause( name );
		} , reset * 1000 );
	}

	function checkUpdateModeCause( name , nBidStatus , nAskStatus ){
		var causeAry = readParmAry( gModeCauseAry , name );
		var bidStatus = causeAry.bidStatus;
		var askStatus = causeAry.askStatus;

		var update = false;
		if ( bidStatus != nBidStatus ){
			if ( !isNaN( parseFloat( nBidStatus ) ) ){
				nBidStatus = parseFloat( nBidStatus ).toFixed();
			}
			editParmAry( gModeCauseAry , name , "bidStatus" , nBidStatus );
			update = true;
		}
		if ( askStatus != nAskStatus ){
			if ( !isNaN( parseFloat( nAskStatus ) ) ){
				nAskStatus = parseFloat( nAskStatus ).toFixed();
			}
			editParmAry( gModeCauseAry , name , "askStatus" , nAskStatus );
			update = true;
		}
		if ( update ){
			updateModeCause( name );
		}
		if ( !causeAry.triggered ){
			editParmAry( gModeCauseAry , name , "triggered" , true );
		}
	}

	function checkModeCauseAxisNParm( name ){
		var ary = readParmAry( gModeCauseAry , name );
		if ( ary.axis == "Disable" ){
			return false;
		}
		for ( var i = 1 ; i <= 3 ; i++ ){
			if ( ary[ "propName" + i ] == "" ){
				break;
			}
			if ( isNaN( parseFloat( ary[ "propParm" + i ] ) ) ){
				return false;
			}
		}
		return true;
	}

	function chgModeEcnSym(){
		if ( gAlgoStatusObj.on ){
			turnOffPanel();
		}

		var ecnName = "ECN";
		var symName = "SYM";
		var tradeEcn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
		var tradeSym = readParmAry( gConfOrderAry , "TradeSym" ).select;

		setParameter( "ECN" , tradeEcn );
		setParameter( "SYM" , tradeSym );

		chgMacro( "bid" );
		chgMacro( "ask" );
		getCurrentOrder();

		function chgMacro( bidOrAsk ){
			for ( var i = 0 ; i < gMacroCount[ bidOrAsk ].length ; i++ ){
				var numStr = ( i + 1 ).toString();
				var name = "[a_parm_" + bidOrAsk + "_" + numStr;
				var ecnName = name + "_ecn]";
				var symName = name + "_sym]";

				setParameter( ecnName , tradeEcn );
				setParameter( symName , tradeSym );
			}
		}
	}

	function turnOffAllModeOrderSwitch(){
		offOrder( "bid" );
		offOrder( "ask" );

		function offOrder( bidOrAsk ){
			for ( var i = 0 ; i < vueCurrentOrder[ bidOrAsk + "OrderAry" ].length ; i++ ){
				Vue.set( vueCurrentOrder[ bidOrAsk + "OrderAry" ][ i ] , "on" , false );
			}
			vueCurrentOrder[ bidOrAsk + "OrderAry" ].push();
		}
	}

	function changeModePos( bidOrAsk , newShift , newSpread , oldShift , oldSpread ){
		updateModeOrder( newShift , newSpread , bidOrAsk );
		setModePosition( newShift , newSpread , bidOrAsk );
		checkModeAction( newShift , newSpread , bidOrAsk , oldShift , oldSpread );
	}

	function setModePosition( shiftPos , spreadPos , bidOrAsk ){
		$.when( checkIsLeader() ).then( function (){
			if ( !gLeaderObj.isLeader ){
				return;
			}
			var macName = "a_mPosition_" + bidOrAsk;
			var parm = shiftPos + "_" + spreadPos;
			setMacro( macName , parm );
		} );
	}

	function checkCauseAction( obj , newPos , oldPos , bidOrAsk ){
		if ( !gAlgoStatusObj.on ){
			return;
		}
		if ( !gAlgoStatusObj[ bidOrAsk + "Auto" ] ){
			return;
		}
		if ( newPos > 0 && newPos < oldPos ){
			return;
		}
		if ( newPos < 0 && newPos > newPos ){
			return;
		}
		if ( !obj.twoStatus && bidOrAsk == "ask" ){
			return;
		}
		if ( obj[ "notice" + newPos ] == 1 ){
			runCauseAction( obj , "notice" , newPos , bidOrAsk );
		}
	}

	function runCauseAction( obj , action , pos , bidOrAsk ){
		$.when( checkIsLeader() ).then( function (){
			if ( !gLeaderObj.isLeader ){
				return;
			}
			if ( action == "notice" ){
				notice();
			}
		} );

		function notice(){
			if ( obj.twoStatus ){
				var bidOrAskStr = " " + bidOrAsk.charAt( 0 ).toUpperCase() + bidOrAsk.slice( 1 );
			}else{
				var bidOrAskStr = "";
			}
			var msg = obj.title + bidOrAskStr + ": " + obj[ bidOrAsk + "Status" ] + ". " + obj.axis + ": " + pos + ". " ;
			causeMsg( msg );
		}

		function causeMsg( msg ){
			var toastrType = "success";
			var toastrTopic = "AlgoStatus:";
			var soundType = "notice";

			sendTabMsg( msg );
			sendToastrMsg( toastrType , toastrTopic , msg );
			sendSoundMsg( soundType );
		}
	}

	function changeModeStatus( bidOrAsk , type , on ){
		if ( type == "On" ){
			if ( on ){
				switchCurrentOrder( bidOrAsk , "run" );
				changeModeStatus( bidOrAsk , "Pause" , false );
			}else if ( !on ){
				switchCurrentOrder( bidOrAsk , "stop" );
			}
		}

		var parm = on ? 1 : 0;
		var name = "a_status_" + bidOrAsk + type;;
		setParameter( name , parm );
		gAlgoStatusObj[ bidOrAsk + type ] = on;
	}

	function checkModeAction( newShift , newSpread , bidOrAsk , oldShift , oldSpread ){
		if ( !gAlgoStatusObj.on ){
			return;
		}
		if ( !gAlgoStatusObj[ bidOrAsk + "Auto" ] ){
			return;
		}
		$.when( checkIsLeader() ).then( function (){
			if ( !gLeaderObj.isLeader ){
				return;
			}
			for ( var i = 0 ; i < gModeActionAry.length ; i++ ){
				var obj = gModeActionAry[ i ];
				if ( !obj.on ){
					continue;
				}
				if ( obj.name != "coolDown" ){
					continue;
				}
				if ( obj.axis == "Shift" ){
					if ( obj.pos != newShift ){
						continue;
					}
					if ( newShift == oldShift ){
						continue;
					}
					if ( newShift > 0 && newShift < oldShift ){
						continue;
					}
					if ( newShift < 0 && newShift > oldShift ){
						continue;
					}
					if ( bidOrAsk == "bid" && newShift >= 0 ){
						continue;
					}else if ( bidOrAsk == "ask" && newShift <= 0 ){
						continue;
					}
				}else if ( obj.axis == "Spread" ){
					if ( obj.pos != newSpread ){
						continue;
					}
					if ( newSpread == oldSpread ){
						continue;
					}
					if ( newSpread > 0 && newSpread < oldSpread ){
						continue;
					}
					if ( newSpread < 0 && newSpread > oldSpread ){
						continue;
					}
				}
				if ( obj.triggered ){
					return;
				}
				runModeAction( obj , newShift , newSpread , bidOrAsk , i );
				break;
			}

			for ( var i = 0 ; i < gModeActionAry.length ; i++ ){
				var obj = gModeActionAry[ i ];
				if ( !obj.on ){
					continue;
				}
				if ( obj.name == "coolDown" ){
					continue;
				}
				if ( obj.axis == "Shift" ){
					if ( obj.pos != newShift ){
						continue;
					}
					if ( newShift == oldShift ){
						continue;
					}
					if ( newShift > 0 && newShift < oldShift ){
						continue;
					}
					if ( newShift < 0 && newShift > oldShift ){
						continue;
					}
					if ( bidOrAsk == "bid" && newShift >= 0 ){
						continue;
					}else if ( bidOrAsk == "ask" && newShift <= 0 ){
						continue;
					}
				}else if ( obj.axis == "Spread" ){
					if ( obj.pos != newSpread ){
						continue;
					}
					if ( newSpread == oldSpread ){
						continue;
					}
					if ( newSpread > 0 && newSpread < oldSpread ){
						continue;
					}
					if ( newSpread < 0 && newSpread > oldSpread ){
						continue;
					}
				}
				runModeAction( obj , newShift , newSpread , bidOrAsk , i );
			}
		} );
	}

	/////////////////////////// ui function //////////////////

	function uiClickPanlSwitch( swVal ){
		var click = false;

		if ( swVal == "Off" || swVal == "Flat" ){
			click = true;

			turnOffPanel();
		}

		if ( swVal == "Flat" ){
			var squarePct = 100;
			var cutSlip = 10;

			clearTimeout( gTimerObj.autoFlat ); // clear previous timer

			//var flatQty = readParmAry( gConfGuardAry , "AutoFlatQty" ).parm;
			var flatQty = 100;

			gTimerObj.autoFlat = setTimeout( function (){
				squarePosition( flatQty , gDefAutoFlatSlip );
			} , 1000 ); // wait 1 second to ensure _STOPALL is completed.

		}else if ( swVal == "On" ){
			if ( $( this ).hasClass( "panlSwChked" ) ){
				return;
			}

			click = true;
			turnOnPanel();
		}

		if ( click ){
			sendResetTriggered();
			setParameter( "a_status_pause" , 0 );
		}
	}

	function uiClickCfmOnPanel(){ // confirm trun on panel even some algo are still runnning
		hideCfmSec();
		turnOnPanel();
	}

	function uiClickDeleteAllMacro(){ // delete all macro by user
		startStopAll( gByManual );

		getSeqNum( gByManual );
		var cmd = "CMD " + gInitUserID + " MACRO_DELETE_ALL";

		pushCMD( cmd );

		showLog( "uiClickDeleteAllMacro" , cmd );
		clearGlobalVal(); // clear all variables from browser

		setTimeout( function (){
			getMacro(); // get macro again to ensure everything are deleted
		} , 500 );
	}

	function uiConfimAdvCfg(){ // confirm guardian setting by user
		var target = parseFloat( $( ".confAdvCfgSec" ).find( ".txtIpt" ).val() );

		if ( isNaN( target ) || target >= 400 || target < 300 ){
			return;
		}

		var cmd1 = "CMD " + target + " DO _STOPALL" + " SEQ " + gStatusObj.seqNum;
		var cmd2 = "CMD " + target + " MACRO_DELETE_ALL";
		var cmd3 = "CMD " + gInitUserID + " MACRO_COPY_TO " + target;
		pushCMD( cmd1 );
		pushCMD( cmd2 );
		pushCMD( cmd3 );
		showLog( "uiConfimAdvCfg" , cmd1 );
		showLog( "uiConfimAdvCfg" , cmd2 );
		showLog( "uiConfimAdvCfg" , cmd3 );

		hideCfgSec();
	}

	function uiClickCfgAdvCfgBtn(){ // config panel setting
		if ( $( ".confAdvCfgSec" ).is( ":visible" ) ){
			return;
		}
		$( ".confAdvCfgSec" ).fadeIn();
	}

	function uiClickPanelMode2(){ // show mode panel
		$( ".confMode2Sec" ).css( "display" , "inline-table" );
	}

	function uiClickCfgChkOffBox(){ // click config check box
		if ( $( this ).prop( "checked" ) ){
			$( this ).closest( ".confDiv" ).find( ".confPauseChk" ).addClass( "disableCtrl" , 200 );
			$( this ).closest( ".confDiv" ).find( ".confPauseChk" ).prop( "disabled" , true );

			if ( $( this ).closest( ".confDiv" ).hasClass( "confMaxSixtySecVolDiv" ) ){
				$( ".confNmlSixtySecVolDiv" ).find( "*" ).addClass( "disableCtrl" , 200 );
			}

		}else{
			$( this ).closest( ".confDiv" ).find( ".confPauseChk" ).removeClass( "disableCtrl" , 200 );
			$( this ).closest( ".confDiv" ).find( ".confPauseChk" ).prop( "disabled" , false );

			if ( $( this ).closest( ".confDiv" ).hasClass( "confMaxSixtySecVolDiv" ) ){
				$( ".confNmlSixtySecVolDiv" ).find( "*" ).removeClass( "disableCtrl" , 200 );
			}
		}
	}

	function uiClickCfgChkFlatAllBox(){ // click config check box
		if ( $( this ).prop( "checked" ) ){
			$( this ).closest( ".confDiv" ).find( ".confFlatChk" ).addClass( "disableCtrl" , 200 );
			$( this ).closest( ".confDiv" ).find( ".confFlatChk" ).prop( "disabled" , true );
		}else{
			$( this ).closest( ".confDiv" ).find( ".confFlatChk" ).removeClass( "disableCtrl" , 200 );
			$( this ).closest( ".confDiv" ).find( ".confFlatChk" ).prop( "disabled" , false );
		}
	}

	function uiClickCfgChkPauseBox(){ // click config check box
		if ( !$( this ).closest( ".confDiv" ).hasClass( "confMaxSixtySecVolDiv" ) ){
			return;
		}

		var chk = $( this ).prop( "checked" );
		$( ".confNmlSixtySecVolDiv" ).find( ".confOnChk" ).prop( "checked" , chk );
	}

	function uiClickReloadAll(){ // request every use to reload the page
		sendMsg( gInitUserID , "MMNOTICE" , "RELOAD_PANEL" );
	}

	function uiClickReloadEveryOne(){ // request every use to reload the page
		var i = 1;
		loop();

		function loop(){
			var sendMsgLoop = setTimeout( function (){
				if ( i < 10 ){
					var userStr = "30" + i;
				}else if ( i >= 10 && i < 26 ){
					var userStr = "3" + i;
				}else{
					sendMsg( gInitUserID , "MMNOTICE" , "RELOAD_PANEL" );
					return;
				}
				if ( parseInt( userStr ) != gInitUserID ){
					sendMsg( userStr , "MMNOTICE" , "RELOAD_PANEL" );
				}
				i++;
				loop();
			} , 1000 );
		}
	}

	function uiClickCloseErrorDiv(){ // close error notice box
		$( ".confErrorSec" ).fadeOut();
	}

	function uiClickCfgOrderBtn(){ // config panel setting
		if ( $( ".confOrderSec" ).is( ":visible" ) ){
			return;
		}

		for ( var i = 0 ; i < gConfOrderAry.length ; i++ ){
			var obj = gConfOrderAry[ i ];
			var div = ".conf" + obj.name + "Div";

			if ( !obj.showCfg ){
				continue;
			}

			if ( obj.showSelect ){
				if ( obj.name == "RefSym" ){
					appendSymList( "Ref" );
				}else if ( obj.name == "TradeSym" ){
					appendSymList( "Trade" );
				}
				$( div ).find( ".confSlet" ).val( obj.select );
				$( div ).find( ".confSlet" ).trigger( "change" );
				$( div ).find( ".confSlet" ).foundationCustomForms();
			}

			if ( obj.showParm ){
				$( div ).find( ".txtIpt" ).attr( "value" , obj.parm );
			}

			if ( obj.showOn ){
				if ( obj.local ){
					var on = readParmAry( gLocalCfgAry , obj.name ).on;
				}else{
					var on = obj.on;
				}
				$( div ).find( ".confChk" ).attr( "checked" , on );
			}
		}

		$( ".confOrderSec" ).fadeIn();

		function appendSymList( type ){
			$( div ).find( ".confSlet" ).empty();
			var ecn = readParmAry( gConfOrderAry , type + "Ecn" ).select;
			var ecnOpt = eval( "gSymOpt_" + ecn );
			var symList = "";

			$.each( ecnOpt , function ( value ){
				symList += "<option value = " + value + "> "+ value + "</option>";
			} );

			$( div ).find( ".confSlet" ).append( symList );
		}
	}

	function uiClickSaveOrder(){
		var updated = false;
		var msg = "";

		for ( var j = 0 ; j < gConfOrderAry.length ; j++ ){
			var obj = gConfOrderAry[ j ];
			var div = ".conf" + obj.name + "Div";

			if ( !obj.showCfg ){
				continue;
			}

			if ( obj.showParm ){
				var chkParm = checkIptVal( div , obj.parmMax , obj.parmMin ); // input number validation

				if ( !chkParm ){
					return;
				}

				if ( obj.parm != chkParm ){ // check need to update the macro or not
					var parmName = "a_orderCfg_" + obj.name + "_parm";
					setMacro( parmName , chkParm );

					msg += obj.title + " Parameter Changed to " + chkParm + obj.unit + ". ";
				}
			}

			if ( obj.showOn ){
				var chk = $( div ).find( ".confChk" ).is( ":checked" );

				var chkNum = chk ? 1 : 0;
				var chkMsg = chk ? "Enabled" : "Disabled";

				if ( obj.on != chk ){
					var parmName = "a_orderCfg_" + obj.name + "_on";
					setMacro( parmName , chkNum );

					msg += chkMsg + " '" + obj.title + "' Function. ";
				}
			}

			if ( obj.showSelect ){
				var select = $( div ).find( ".current" ).html();
				select = select.replace( / /g , "" );

				if ( obj.select != select ){
					updated = true;
					var parmName = "a_orderCfg_" + obj.name + "_select";
					setMacro( parmName , select );

					msg += obj.title + " Changed to '" + select + "'. ";
					editParmAry( gConfOrderAry , obj.name , "select" , select );
				}
			}
		}

		if ( updated ){
			chgModeEcnSym();
		}
		gMacroCount = { bid: [] , ask: [] };

		hideCfgSec();
	}

	function uiChgPanelEcn(){
		if ( !$( ".confOrderSec" ).is( ":visible" ) ){
			return;
		}
		if ( $( this ).closest( ".confDiv" ).hasClass( "confRefEcnDiv" ) ){
			var type = "Ref";
		}else if ( $( this ).closest( ".confDiv" ).hasClass( "confTradeEcnDiv" ) ){
			var type = "Trade";
		}else{
			return;
		}

		var ecn = $( this ).val();

		var sym = $( ".conf" + type + "SymDiv" ).find( ".current" ).html();
		sym = sym.replace( / /g , "" );

		var slt = $( ".conf" + type + "SymDiv" ).find( ".confSlet" );
		$( slt ).empty();

		var symECN = eval( "gSymOpt_" + ecn );
		var symList = "";
		$.each( symECN , function ( value ){
			symList += "<option value = " + value + "> "+ value + "</option>";
		} );
		$( slt ).append( symList );
		var hasThisSym = $( slt ).find( 'option[ value = "' + sym + '" ]' ).length;

		if ( hasThisSym == 1 ){
			$( slt ).val( sym );
		}else if ( ecn == "MREX" || ecn == "PATS" || ecn == "PATX" ){
			$( slt ).val( gRefSym );
		}else{
			$( slt ).eq( 0 ).prop( 'selected' , true );
		}

		$( slt ).foundationCustomForms();
		$( ".conf" + type + "SymDiv" ).find( ".current" ).html( $( slt ).val() );
	}

	function uiClickHideCauseDataChart(){
		$( ".modeChartSec" ).hide();
	}

	function uiClickShowFiveSecVolTable(){
		$( ".fiveSecVolTableDiv" ).show();
	}

	function uiClickShowTickVolTable(){
		$( ".tickVolTableDiv" ).show();
	}

	function uiClickShowOrderCountTable(){
		$( ".orderCountTableDiv" ).show();
	}

	function uiClickCloseSecDiv(){
		$( this ).closest( ".confSec" ).hide();
	}

	function uiHideCfgSec(){
		$( ".confPanlSec" ).fadeOut();
		$( ".confGuarSec" ).fadeOut();
		$( ".confModeSec" ).fadeOut();
		$( ".confOrderSec" ).fadeOut();
		$( ".confAdvCfgSec" ).fadeOut();
	}

	function uiHideCfmSec(){
		$( ".cfmCvr" ).fadeOut();
		$( ".cfmSec" ).fadeOut();
	}

	/////////////////////////// Other function ///////////////

	function readParameter( cmrAry ){ // read get parameter msg from server
		// e.g. CMR 302 GET _PANEL_ON 0
		var name = cmrAry[ 3 ];
		var parm = cmrAry[ 4 ];
		var group = name.split( "_" )[ 0 ];
		var type = name.split( "_" )[ 2 ];

		gStatusObj.gotParameter = true;

		if ( group != "a" ){
			return;
		}
		if ( parm == "NOT_FOUND" ){
			//parm = 0;
			return;
		}
		parm = Boolean( parseFloat( parm ) );
		gAlgoStatusObj[ type ] = parm;

		if ( type == "on" ){
			trunPanelSwitch( parm );
		}else if ( type == "pause" ){
			if ( parm == 1 ){
				showPauseNotice();
			}else if ( parm == 0 ){
				hidePauseNotice();
				clearTimeout( gTimerObj.pausePanel );
			}
		}
	}

	function readPlacedPx( cmrAry ){ // show macro placed order price
		// CMR 300 MSG ALGO Mac_01_BUY O LO2 MREX CME/CMX_GLD/AUG17 30270720033500022 BUY 2 1217.7
		if ( cmrAry.length < 12 ){
			return;
		}
		var ecn = cmrAry[ 7 ];
		var sym = cmrAry[ 8 ];
		var px = parseFloat( cmrAry[ 12 ] );
		var symDip = getSymItem( ecn , sym ).dip; // get symbol decimal places

		if ( ! $.isNumeric( px ) ){
			return;
		}

		px = px.toFixed( symDip );
		var numStr = cmrAry[ 4 ].split( "_" )[ 1 ];
		var side = cmrAry[ 4 ].split( "_" )[ 2 ];
	}

	function readTotalFill( cmrAry ){ // read total fill quantity message
		// e.g. CMR 300 TOTAL_FILL MREX CME/CMX_GLD/AUG17 15 46 SELL 1 1246.7 OPX 1246.4
		if ( cmrAry.length < 7 ){
			return;
		}

		resetModeTradeTimer( cmrAry );
		updateModeTradeCount( cmrAry );
		storeFilledQuote();

		clearTimeout( gTimerObj.readTotalFill );
		gTimerObj.readTotalFill = setTimeout( function (){ // wait 1 second to avoid reading too many partil fill msg
			$.when( checkIsLeader() ).then( function (){
				if ( !gLeaderObj.isLeader ){
					return;
				}
				sendTradeMsg( cmrAry );
				slowFedGuardian( cmrAry );
			} );
		} , 1000 );
	}

	function readMsg( cmrAry ){ // read MSG cmr message
		// e.g. CMR 302 MSG a_tabMsg Stopped All Algo.
		if ( cmrAry.length < 6 ){
			return;
		}

		var cmrMsg = "";

		for ( var i = 4 ; i < cmrAry.length -1 ; i ++ ){
			if ( i > 4 ){
				cmrMsg += " ";
			}
			cmrMsg += cmrAry[ i ];
		}

		if ( cmrMsg == "Server Started." ){
			if ( gStatusObj.reloaded ){
				return
			}
			gStatusObj.reloaded = true;

			$.when( checkIsLeader() ).then( function (){
				if ( !gLeaderObj.isLeader ){
					return;
				}

				var topic = gInitUserID + ":";
				sendTelegramMsg( topic , cmrMsg );

				turnOffPanel();
				reloadAll(); // reload page if server is restarted.
			} );

		}else if ( cmrMsg.slice( 0 , 20 ) == "Updated Algo Setting" ){
			$( ".confPanlSec" ).fadeOut(); // hide panel config box if someone changed parameter

		}else if ( cmrMsg.slice( 0 , 24 ) == "Updated Guardian Setting" ){
			$( ".confGuarSec" ).fadeOut();
		}
	}

	function readNotice( cmrAry ){ // read notice msg
		// e.g. CMR 304 MSG MMNOTICE NOTICE2 Volume Update: CME/CRUDE/JAN18 994
		if ( cmrAry.length < 4 ){
			return;
		}

		notice = cmrAry[ 4 ];
		topic = notice.split( "_" )[ 0 ];
		type = notice.split( "_" )[ 1 ];

		var msg = "";
		for ( var i = 5 ; i < cmrAry.length ; i ++ ){
			msg += cmrAry[ i ] + " ";
		}

		if ( topic == "GUARD" ){ // guardian msg
			if ( type == "NmlSixtySecVol" ){
				editParmAry( gConfGuardAry , "MaxSixtySecVol" , "triggered" , false );
			}
			editParmAry( gConfGuardAry , type , "triggered" , true );
			showGuardianNotice( type );

		}else if ( topic == "NOTICE1" ){ // roger's notice
			//showRNotice( msg );
			twiceFillGuardian( msg );
			updateModeFilledTwiceSignal( msg );

		}else if ( topic == "NOTICE2" ){ // roger's notice
			sixtySecVolGuardian( msg );
			resumeVolumeGuardian( msg );
		}else if ( notice == "TRIGGERED_RESET" ){
			resetAllGuardianTriggered( msg );
		}else if ( notice == "RELOAD_PANEL" ){
			reloadAll();
		}
	}

	function readAlgoRunning( cmrAry ){ // read ALGO-RUNNING message
		// e.g. CMR 302 ALGO-RUNNING 0
		var running = cmrAry[ 3 ];
		if ( running == 0 ){
			gStatusObj.algoRunning = false;
			$( gPanelID ).find( ".badgeSp" ).removeClass( "badgeOnSp" );
		}else if ( running == 1 ){
			gStatusObj.algoRunning = true;
			$( gPanelID ).find( ".badgeSp" ).addClass( "badgeOnSp" );
		}
	}

	function readMaxProfit( cmrAry ){ // read maximum profit msg
		// e.g. CMR 302 MACRO _MAX_PROFIT NAME { 2017_10_19_12_53 2345 }
		var rHour = 5; // restart hour 5a.m.

		var mDateStr = cmrAry[ 6 ];
		var mYear = parseInt( mDateStr.split( "_" )[ 0 ] );
		var mMonth = parseInt( mDateStr.split( "_" )[ 1 ] );
		var mDay = parseInt( mDateStr.split( "_" )[ 2 ] );
		var mHour = parseInt( mDateStr.split( "_" )[ 3 ] );
		var mMinute = parseInt( mDateStr.split( "_" )[ 4 ] );
		var mSecond = parseInt( mDateStr.split( "_" )[ 5 ] );

		var mDate = new Date( mYear , mMonth , mDay , mHour , 0 , 0 ); // macro day

		var cDate = new Date(); // get current date

		var cDateStr = getCurrentTime();
		var cYear = parseInt( cDateStr.split( "_" )[ 0 ] );
		var cMonth = parseInt( cDateStr.split( "_" )[ 1 ] );
		var cDay = parseInt( cDateStr.split( "_" )[ 2 ] );
		var cHour = parseInt( cDateStr.split( "_" )[ 3 ] );

		var rDate = new Date( cYear , cMonth , cDay , rHour , 0 , 0 );
		if ( cHour < rHour ){
			rDate.setDate( rDate.getDate() - 1 ); // server restart time;
		}

		if ( mDate < rDate ){
			return; // macro date too old
		}

		if ( mDate > cDate ){
			return; // macro date too new
		}

		var maxProfit = parseFloat( cmrAry[ 7 ] );
		gMaxProfit = maxProfit;

		updatePnLStopLable();
	}

	function readToastrMsg( cmrAry ){ // read notice msg
		// e.g. CMR 304 MSG TOASTR_MSG error_AlgoGuardian Stop All.
		if ( cmrAry.length < 6 ){
			return;
		}

		cmr = cmrAry[ 4 ];
		type = cmr.split( "_" )[ 0 ];
		topic = cmr.split( "_" )[ 1 ];

		var msg = "";
		for ( var i = 5 ; i < cmrAry.length ; i ++ ){
			var str = cmrAry[ i ];
			if ( str == gIconObj.doubleUp ){
				str = gFAIconObj.doubleUp;
			}else if ( str == gIconObj.up ){
				str = gFAIconObj.up;
			}else if ( str == gIconObj.down ){
				str = gFAIconObj.down;
			}else if ( str == gIconObj.doubleDown ){
				str = gFAIconObj.doubleDown;
			}else if ( str == gIconObj.minus ){
				str = gFAIconObj.minus;
			}else if ( str == gIconObj.doubleExcMark ){
				str = gFAIconObj.doubleExcMark;
			}else if ( str == gIconObj.warnning ){
				str = gFAIconObj.warnning;
			}
			msg += str + " ";
		}

		showToastr( type , topic , msg );
	}

	function readSoundMsg( cmrAry ){
		// e.g. CMR 304 MSG SOUND_MSG alert
		if ( cmrAry.length < 5 ){
			return;
		}

		var sound = cmrAry[ 4 ];
		playSound( sound );
	}

	function readTableMsg( cmrAry , i ){
		var topic = cmrAry[ 5 + i ];
		var time = cmrAry[ 6 + i ];
		var midPx = parseFloat( cmrAry[ 7 + i ] );
		var prevMidPx = parseFloat( cmrAry[ 8 + i ] );
		var vol = parseFloat( cmrAry[ 9 + i ] );
		var bidVol = parseFloat( cmrAry[ 10 + i ] );
		var askVol = parseFloat( cmrAry[ 11 + i ] );
		var maxVol = parseFloat( cmrAry[ 11 + i ] );
		var maxVolPx = parseFloat( cmrAry[ 11 + i ] );

		var ecn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
		var sym = readParmAry( gConfOrderAry , "TradeSym" ).select;
		var ts = parseFloat( getSymItem( ecn , sym ).ts );

		var pipChg = parseFloat( ( ( midPx - prevMidPx ) / ts ).toFixed() );

		var pxDirection = 0;
		if ( midPx > prevMidPx ){
			pxDirection = 1;
		}else if ( midPx < prevMidPx ){
			pxDirection = -1;
		}

		if ( bidVol == 0 && askVol == 0 ){
			var ratio = 0.5;
		}else{
			var ratio = parseFloat( ( bidVol / ( bidVol + askVol ) ).toFixed( 2 ) );
		}

		var obj = { topic: topic ,
					time: time ,
					midPx: midPx ,
					prevMidPx: prevMidPx ,
					pipChg: pipChg ,
					pxDirection: pxDirection ,
					vol: vol ,
					bidVol: bidVol ,
					askVol: askVol ,
					ratio: ratio ,
					maxVol: maxVol ,
					maxVolPx: maxVolPx ,
					};

		gTableMsgAry.unshift( obj );

		if ( gTableMsgAry.length > 2000 ){
			gTableMsgAry.pop(); // remove last row
		}
	}

	function readOrderConfig( cmrAry ){
		var macName = cmrAry[ 3 ];
		var macParm = cmrAry[ 5 ];
		var name = macName.split( "_" )[ 2 ];
		var property = macName.split( "_" )[ 3 ];

		if ( property == "on" || property == "off" || property == "pause" || property == "flat" || property == "flatAll" || property == "notice" ){
			macParm = Boolean( parseInt( macParm ) );
		}

		if ( property == "parm" ){
			macParm = parseFloat( macParm );
		}

		editParmAry( gConfOrderAry , name , property , macParm );
	}

	function readHotkeyMsg( cmrAry ){
		$.when( checkIsLeader() ).then( function (){
			if ( !gLeaderObj.isLeader ){
				return;
			}
			var action = cmrAry[ 4 ];
			if ( action == "on" ){
				uiClickPanlSwitch( "On" );
			}else if ( action == "off" ){
				uiClickPanlSwitch( "Off" );
			}else if ( action == "flat" ){
				uiClickPanlSwitch( "Flat" );
			}else if ( action == "bidSw" ){
				vueModeMatrix.clickModeSwitchBtn( 'bid' , 'On' );
			}else if ( action == "askSw" ){
				vueModeMatrix.clickModeSwitchBtn( 'ask' , 'On' );
			}else if ( action == "bidAuto" ){
				vueModeMatrix.clickModeSwitchBtn( 'bid' , 'Auto' );
			}else if ( action == "askAuto" ){
				vueModeMatrix.clickModeSwitchBtn( 'ask' , 'Auto' );
			}else if ( action == "minusSpread" ){
				vueModeMatrix.chgManualAxis( "Spread" , "minus" , "both" );
			}else if ( action == "addSpread" ){
				vueModeMatrix.chgManualAxis( "Spread" , "add" , "both" );
			}else if ( action == "minusShift" ){
				vueModeMatrix.chgManualAxis( "Shift" , "minus" , "both" );
			}else if ( action == "addShift" ){
				vueModeMatrix.chgManualAxis( "Shift" , "add" , "both" );
			}else if ( action == "minusBidSpread" ){
				vueModeMatrix.chgManualAxis( "Spread" , "minus" , "bid" );
			}else if ( action == "addBidSpread" ){
				vueModeMatrix.chgManualAxis( "Spread" , "add" , "bid" );
			}else if ( action == "minusAskSpread" ){
				vueModeMatrix.chgManualAxis( "Spread" , "minus" , "ask" );
			}else if ( action == "addAskSpread" ){
				vueModeMatrix.chgManualAxis( "Spread" , "add" , "ask" );
			}
		} );
	}

	function readPriceQuote( m ){
		var ecn = m.e;
		var mSym = m.s;

		if ( mSym.indexOf( "MA_60_" ) >= 0 ){
			var sym = mSym.split( "MA_60_" )[ 1 ];
			var maQuote = true;
		}else{
			var sym = mSym;
			var maQuote = false;
		}

		var rEcn = readParmAry( gConfOrderAry , "RefEcn" ).select;
		var rSym = readParmAry( gConfOrderAry , "RefSym" ).select;

		if ( sym != rSym ){
			return;
		}

		if ( ( rEcn == "PATS" || rEcn == "PATX" ) && ecn == "MREX" ){
			ecn = rEcn;
		}

		if ( ecn != rEcn ){
			return;
		}

		if ( !maQuote ){
			for ( var i = 0 ; i < m.d.length ; i++ ){
				if ( !checkValue( m.d[ i ].bid ) ||
				!checkValue( m.d[ i ].ask ) ||
				!checkValue( m.d[ i ].bidVol ) ||
				!checkValue( m.d[ i ].askVol ) ){
					gotWrongQuoteData();
					return;
				}
			}
			storeQuoteDate( m );
			checkQuoteModeCause();
			marketDepthGuardian();

		}else{
			storeMAQuoteDate( m );
			pushFiveSecondVol();
			checkMaQuoteModeCause();
			wideMASpreadGuardian();
			fiveSecVolGuardian();
			countTickVolume();
		}

		clearQuoteDate();

		function checkValue( num ){
			num = parseFloat( num );
			if ( isNaN( num ) ){
				return false;
			}

			return num;
		}
	}

	function readTradeQuote( m ){
		var ecn = m[ 1 ];
		var mSym = m[ 2 ];

		if ( mSym.indexOf( "K_" ) >= 0 ){
			var sym = mSym.split( "K_" )[ 1 ];
			var type = "K";
		}else if ( mSym.indexOf( "IND_AI_" ) >= 0 ){
			var sym = mSym.split( "IND_AI_" )[ 1 ].split( "#" )[ 0 ];
			var type = "IND_AI";
		}else if ( mSym.indexOf( "RL_" ) >= 0 ){
			var sym = mSym.split( "RL_" )[ 1 ];
			var type = "RL";
		}else{
			return;
		}

		var rEcn = readParmAry( gConfOrderAry , "RefEcn" ).select;
		var rSym = readParmAry( gConfOrderAry , "RefSym" ).select;

		if ( sym != rSym ){
			return;
		}

		if ( type == "K" ){
			if ( rEcn == "MREX" || rEcn == "PATS" || rEcn == "PATX" ){
				if ( ecn == "MREX" || ecn == "PATS" || ecn == "PATX" ){
					storeTradeDate( m );
				}else if ( ecn == "TREP" ){
					storeReutersTradeDate( m );
				}
			}else if ( rEcn == "HKE" ){
				if ( ecn == "HKE" ){
					storeTradeDate( m );
				}
			}
			pushFiveSecondVol();
		}else if ( type == "IND_AI" ){
			readAIQuote( m );
		}else if ( type == "RL" ){
			storeRateLimitData( m );
		}
	}

	function readAIQuote( m ){
		showLog( "readAIQuote" , m );
		var type = m[ 2 ].split( "#" )[ 1 ];

		if ( type == "S20120" ){
			updateModeAiDirection( m );
		}else if ( type == "VT1" ){
			updateModeAiVolatility( m );
		}else if ( type == "VT2" ){
			updateModeAiVolatility2( m );
		}else if ( type == "PVR1" ){
			updateModeAiPvr1( m );
		}else if ( type == "BATT2" ){
			updateModeAiBatt2( m );
		}else if ( type == "BATT3" ){
			updateModeAiBatt3( m );
		}
	}

	function sendMsg( target , topic , msg ){ // send message
		getSeqNum( gByFunction );
		//var cmd = "CMD " + gInitUserID + " MSG " + target + " " + topic + " { " + msg + " }" + " SEQ " + gStatusObj.seqNum;
		var cmd = "CMD " + gInitUserID + " MSG " + target + " " + topic + " { " + msg + " }";

		pushCMD( cmd );
		showLog( "sendMsg" , cmd );
	}

	function sendTradeMsg( cmrAry ){
		var ecn = cmrAry[ 3 ];
		var sym = cmrAry[ 4 ];
		var side = cmrAry[ 7 ];
		var qty = parseFloat( cmrAry[ 8 ] );
		var px = parseFloat( cmrAry[ 9 ] );
		var ordPx = cmrAry[ 11 ];

		if ( ordPx == "UNKNOWN" || ordPx == "NIL" ){
			return;
		}

		var rEcn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
		var rSym = readParmAry( gConfOrderAry , "TradeSym" ).select;

		if ( ecn != rEcn || sym != rSym ){
			return;
		}

		var ts = parseFloat( getSymItem( ecn , sym ).ts ); // get tick size
		var dip = parseFloat( getSymItem( ecn , sym ).dip ); // get symbol decimal places

		if ( isNaN( qty ) || isNaN( px ) || isNaN( ts ) || isNaN( dip ) ){
			showLog( "sendTradeMsg" , "Error On Reading Trade Msg. qty : " + qty + " . trade px : " + px + " . tick size : " + ts + " . decimal places : " + dip );
			return;
		}

		px = px.toFixed( dip );

		var tradeMsg = side + " " + ecn + " " + sym + " at " + px + ". ";
		var symMsg = "";
		var symInfo = "";

		symInfo = getSymNetPos( ecn , sym );

		var netPos = symInfo.netPos; // calulate total net position for a product
		var avgPx = symInfo.avgPx;

		if ( $.isNumeric( netPos ) && $.isNumeric( avgPx ) ){
			symMsg = "Net Position: " + netPos + ". Average Price: " + avgPx + ". ";
		}

		var accMsg;
		accPnL = getAccPnL(); // get account Pnl
		if ( $.isNumeric( accPnL ) ){
			accMsg = "P&L: " + accPnL + ". ";
		}

		if ( gMAQuoteAry.length < gFiveSecondTick ){
			var sec5Vol = 0;
		}else{
			var sec5Vol = gMAQuoteAry[ 0 ].totalVol - gMAQuoteAry[ gFiveSecondTick - 1 ].totalVol;
		}

		var volMsg = "";
		var obj = gMAQuoteAry[ 0 ];

		if ( typeof obj != "undefined" ){
			volMsg = "Total Volume: " + obj.totalVol + ". 15Minute Volume: " + obj.sec900Vol + ". 1Minute Volume: " + obj.sec60Vol + ". 5Second Volume: " + sec5Vol + ". MA Spread: " + obj.maSpreadPip + ". ";
		}

		var msg = tradeMsg + symMsg + accMsg + volMsg;
		sendTabMsg( msg );
	}

	function sendDataMsg( msg ){
		var cmd = "CMD " + gInitUserID + " MSG " + gInitUserID + " LOG { " + msg + " }";
		pushCMD( cmd );
		showLog( "sendDataMsg" , cmd );
	}

	function sendTabMsg( msg ){
		sendMsg( gInitUserID , "a_tabMsg" , msg );

		var topic = gInitUserID + ":";
		sendTelegramMsg( topic , msg );
	}

	function sendLogMsg( msg ){ // send log message to server for debug
		getSeqNum( gByFunction );
		var topic = "LOGMSG";
		var cmd = "CMD " + gInitUserID + " MSG " + gInitUserID + " " + topic + " { " + msg + " . Seq No. : " + gStatusObj.seqNum + " . IP : " + gStatusObj.ip + " }";

		pushCMD( cmd );
		showLog( "sendLogMsg" , cmd );
	}

	function sendTelegramMsg( topic , msg ){ // show message to telegram
		var on = readParmAry( gConfPanelAry , "SendTelegram" ).on;
		if ( !on ){
			return;
		}

		var msgStr = msg.split( " " )[ 0 ];

		for ( var i = 0 ; i < gTelegramTargetAry.length ; i ++ ){
			var obj = gTelegramTargetAry[ i ];
			if ( obj.userID.indexOf( gInitUserID ) == -1 ){
				continue;
			}

			for ( var j = 0 ; j < obj.target.length ; j ++ ){
				if ( obj.target[ j ] == "kellog" && ( msgStr != "BUY" && msgStr != "SELL" ) ){
					continue;
				}
				sendMsg( obj.target[ j ] , topic , msg );
			}
			break;
		}
	}

	function sendToastrMsg( type , topic , msg ){
		// available type : error , warning , success , info
		sendMsg( gInitUserID , "a_toastrMsg" , type + "_" + topic + " " + msg ); // send triggered guardian notice
	}

	function sendSoundMsg( sound ){
		// available sound: alert , order , notice
		sendMsg( gInitUserID , "a_soundMsg" , sound ); // send triggered guardian notice
	}

	function sendResetTriggered(){
		sendMsg( gInitUserID , "MMNOTICE" , "TRIGGERED_RESET" );
	}

	function sendTableMsg( type , midPx , preMid , vol , bidVol , askVol , maxVol , maxVolPx ){
		if ( !gAlgoStatusObj.on ){
			return;
		}
		if ( gStatusObj.reloaded || gStatusObj.disconnected ){
			return;
		}
		$.when( checkIsLeader() ).then( function (){
			if ( !gLeaderObj.isLeader ){
				return;
			}
			var topic = "a_tableMsg";
			var date = new Date();
			var minutes = ( "0" + ( date.getMinutes() ) ).slice( -2 );
			var seconds = ( "0" + ( date.getSeconds() ) ).slice( -2 );
			var time = date.getHours() + ":" + minutes + ":" + seconds;

			var msg = topic + " " + type + " " + time + " " + midPx + " " + preMid + " " + vol + " " + bidVol + " " + askVol + " " + maxVol + " " + maxVolPx;
			sendDataMsg( msg );
		} );
	}

	function showReloadNotice(){
		location.reload();
		$( ".panlReldNtcSn" ).hide();
		$( ".panlReldLdDiv" ).hide();
		$( ".panlReldWarnSn" ).fadeIn();
	}

	function showLog( name , content ){ // show con.sole log
		var on = readParmAry( gLocalCfgAry , "ShowConsole" ).on;
		if ( !on ){
			return;
		}

		console.log( name + " : " + content );
	}

	function showDebugLog( name , content ){ // show debug log
		var on = readParmAry( gLocalCfgAry , "ShowConsole" ).on;
		if ( !on ){
			return;
		}

		console.log( " DEBUG : " + name + " : " + content );
	}

	function showBgAlgoCount(){ // show algo count on tab badge
		clearTimeout( gTimerObj.getBadgeNum ); // clear previous timer

		gTimerObj.getBadgeNum = setTimeout( function (){
			var onAlgoNum = getOnAlgoNum(); // get on algo number
			var badgeSp = $( gPanelID ).find( ".badgeSp" );
			var oOnAlgoNum = parseInt( $( badgeSp ).text() ); // get previous on algo number
			if ( onAlgoNum != oOnAlgoNum ){ // update the badge if the number is changed
				$( badgeSp ).text( onAlgoNum );
				$( badgeSp ).show();
			}
		} , 800 );
	}

	function showParmErrorNotice( type ){ // show algo parameter error ( NaN or undefined )
		var receivedType = 1;
		var sendType = 2;

		if ( type == receivedType ){
			$( ".confErrorSec" ).find( ".confMsgSp" ).html( "Received Invalid Algo Parameter From Server.<br>Please Review Algo Setting." );
		}else if ( type == sendType ){
			$( ".confErrorSec" ).find( ".confMsgSp" ).html( "Invalid Algo Parameter.<br>Please Review Algo Setting." );
		}

		$( ".confErrorSec" ).fadeIn();
	}

	function showRNotice( msg ){ // notice msg from roger
		// e.g. BUY Twice in 1 minute , Same Algo FullyFilled > 1
		playSound( "order" );
		showToastr( "warning" , "AlgoSignal" , msg );
	}

	function showToastr( type , title , msg ){ // show GFA notice box
		var toastNum = $( ".toast" ).length;

		if ( toastNum >= 5 ){ // prevent showing too many toast box
			return;
		}

		if ( type == "success" ){
			var timeOut = 5000;
		}else if ( type == "info" ){
			var timeOut = 5000;
		}else if ( type == "warning" ){
			var timeOut = 600000;
		}else if ( type == "error" ){
			var timeOut = 600000;
		}

		var options = {
			"closeButton": true ,
			"debug": false ,
			"newestOnTop": false ,
			"progressBar": true ,
			"positionClass": "toast-bottom-left" ,
			"preventDuplicates": false ,
			"onclick": null ,
			"showDuration": "300" ,
			"hideDuration": "300" ,
			"timeOut": timeOut ,
			"extendedTimeOut": "1000" ,
			"showEasing": "swing" ,
			"hideEasing": "linear" ,
			"showMethod": "fadeIn" ,
			"hideMethod": "fadeOut"
		}

		gInitNotice( type , msg , title , options );
	}

	function showPauseNotice(){
		$( ".panlPauseNtcSec" ).show();
	}

	function hidePauseNotice(){
		$( ".panlPauseNtcSec" ).hide();
	}

	function hideCfgSec(){
		$( ".confPanlSec" ).fadeOut();
		$( ".confGuarSec" ).fadeOut();
		$( ".confModeSec" ).fadeOut();
		$( ".confOrderSec" ).fadeOut();
	}

	function getSeqNum( type ){ // get sequence number
		if ( gStatusObj.actionNum >= 100 ){
			gStatusObj.actionNum = 0;
		}

		var actNo = gStatusObj.actionNum;

		if ( gStatusObj.actionNum < 10 ){
			actNo = "0" + gStatusObj.actionNum
		}

		gStatusObj.seqNum = type.toString() + gStatusObj.sessionId.toString() + actNo.toString() + gVersion; // e.g. SEQ 7378391a
		gStatusObj.actionNum ++;
	}


	function getSymItem( ecn , sym ){ // get symbol details ( tick size , decimal place )
		if ( ecn == "PATX" ){
			ecn == "MREX";
		}

		var symObj = {};
		symObj.e = ecn;
		symObj.s = sym;
		var symItem = gInitSymItem( symObj );
		return symItem;
	}

	function getSymNetPos( ecn , sym ){ // get symbol net position
		sym = sym.replace( /\//g , "_" );

		var netPos = gInitNetPos( gInitUserID , ecn , sym )[ 0 ];
		var avgPx = gInitNetPos( gInitUserID , ecn , sym )[ 1 ];

		var netPosNum = parseFloat ( netPos );
		var avgPxNum = Math.round( parseFloat ( avgPx ) * 100 ) / 100;

		if ( ! validateParameter( "netPos" , netPosNum , netPos ) ){
			netPosNum = 0;
		}

		if ( ! validateParameter( "avgPx" , avgPxNum , avgPx ) ){
			avgPxNum = 0;
		}

		symInfo = { netPos: netPosNum , avgPx: avgPxNum };
		return symInfo;
	}

	function getSymMarketPx( ecn , sym ){ // get symbol market price
		if ( ecn == "PATX" ){
			ecn == "MREX";
		}

		sym = sym.replace( /\//g , "_" );

		var mktPx = parseFloat( gInitMktPx( ecn , sym ) );
		if ( isNaN( mktPx ) ){
			var bidPx = getSymBidAskPx( ecn , sym )[ 0 ];
			var askPx = getSymBidAskPx( ecn , sym )[ 1 ];
			mktPx = ( bidPx + askPx ) / 2;
		}

		if ( isNaN( mktPx ) ){
			return false;
		}
		return mktPx;
	}

	function getSymBidAskPx( ecn , sym ){
		if ( ecn == "PATX" ){
			ecn == "MREX";
		}

		var bidPx = getPx( ecn , sym ).bidPx;
		var askPx = getPx( ecn , sym ).askPx;

		if ( ecn == "PATS" && ( bidPx == 0 || askPx == 0 ) ){
			ecn = "MREX";
			var bidPx = getPx( ecn , sym ).bidPx;
			var askPx = getPx( ecn , sym ).askPx;
		}

		var pxAry = [ bidPx , askPx ];
		return pxAry;

		function getPx( ecn , sym ){
			var sym = sym.replace( /\//g , "_" );
			var str = ecn + "-" + sym;

			var bidPx = gInitBidAry[ str ];
			var askPx = gInitAskAry[ str ];
			if ( typeof( bidPx ) == "undefined" ){
				bidPx = 0;
			}
			if ( typeof( askPx ) == "undefined" ){
				askPx = 0;
			}
			var obj = { bidPx: parseFloat( bidPx ) , askPx: parseFloat( askPx ) };

			return obj;
		}
	}

	function getCutLossPx( ecn , sym , netPos , qty , bidPx , askPx ){
		var px = 0;
		var pnl = getAccPnL(); // get current PnL
		var maxLoss = gDefBufferMaxLoss;
		var buffer = pnl + maxLoss;

		var ts = getSymItem( ecn , sym ).ts; // get tick size
		var tc = getSymItem( ecn , sym ).tc; // get tick cent

		var pnlPerTick = ts * tc * Math.abs( netPos );

		var bufferTick = Math.abs( buffer / pnlPerTick );

		if ( bufferTick > gDefAutoFlatSlip ){
			bufferTick = gDefAutoFlatSlip;
		}

		var bufferPx = Math.round( bufferTick ) * ts;

		if ( qty > 0 ){
			px = askPx + bufferPx;
		}else{
			px = bidPx - bufferPx;
		}

		return px;
	}

	function getAccPnL(){ // get account PnL
		var pnl = gInitAccPnl[ gInitUserID ];

		if ( typeof pnl == "undefined" ){
			pnl = 0;
		}

		var pnlNum = Math.round( parseFloat( pnl ) * 10 ) / 10; // round number

		if ( ! validateParameter( "pnl" , pnlNum , pnl ) ){
			pnlNum = 0;
		}

		if ( pnlNum < -100000 || pnlNum > 100000 ){ // prevent reading wrong pnl number from GFA
			var msg = "Error on reading account PnL. PnL : " + pnl;
			sendLogMsg( msg );

			pnlNum = 0;
		}

		return pnlNum;
	}

	function getOpenOrderNum(){ // get open order number
		var openOrder = gInitOpenOrdCount();
		var orderNum = parseFloat( openOrder );

		if ( ! validateParameter( "orderNum" , orderNum , openOrder ) ){
			orderNum = 0;
		}

		return orderNum;
	}

	function getOnAlgoNum(){ // get on algo number
		var onAlgo = 0;
		// $( ".macSw" ).each( function (){
			// if ( $( this ).find( ".swOnLbl" ).hasClass( "swChked" ) && $( this ).find( ".swOnLbl" ).hasClass( "swMacOn" ) ){
				// onAlgo ++;
			// }
		// } );
		return onAlgo;
	}

	function getLocalStore(){ // load local conifg.
		writeToAry( "ShowConsole" );
		writeToAry( "PlayAlert" );

		function writeToAry( name ){
			var on = store.get( "j" + name );
			if ( on != null ){
				editParmAry( gLocalCfgAry , name , "on" , on );
			}
		}
	}

	function getRandomNumber(){ // 1 - 100
		var num = Math.floor( ( Math.random() * 100 ) + 1 );
		return num;
	}

	function getCurrentTime(){ // get time. format 2017_10_19_12_53_32 = 19/11/2017 12:53:32a.m.
		var date = new Date();
		var year = date.getFullYear().toString();
		var month = date.getMonth().toString();
		var day = date.getDate().toString();
		var hour = date.getHours().toString();
		var minute = date.getMinutes().toString();
		var second = date.getSeconds().toString();

		var time = year + "_" + month + "_" + day + "_" + hour + "_" + minute + "_" + second;
		return time;
	}

	function getClientIP(){ // get user ip address
		var defer = $.Deferred();
		var retry = 0;

		gIntervalObj.getClientID = setInterval( getIP , 500 );
		return defer;

		function getIP(){
			if ( ( gStatusObj.ip != "" && typeof gStatusObj.ip != "undefined" ) || retry > 10 ){
				clearInterval( gIntervalObj.getClientID );
				defer.resolve( null );
				return defer;
			}

			window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
			var pc = new RTCPeerConnection( { iceServers:[] } ) , noop = function (){};
			pc.createDataChannel( "" );
			pc.createOffer( pc.setLocalDescription.bind( pc ) , noop );

			pc.onicecandidate = function ( ice ){
				if ( !ice || !ice.candidate || !ice.candidate.candidate ){
					return;
				}
				var regex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/;

				gStatusObj.ip = regex.exec( ice.candidate.candidate )[ 1 ];
				$( "#userInfoIP" ).text( gStatusObj.ip );

				pc.onicecandidate = noop;
			};

			retry ++;
		}
	}

	function getMsgData(){
		var type = "getmsg";
		var host = "https://bigmindtrader.com:28443/ts/";
		var uid = "uid=" + gInitUserID;
		var filter = "filter=LOG";
		var token = "token=123";
		var link = host + type + "?" + uid + "&" + filter + "&" + token;

		$.ajax( {
			url: link ,
			dataType: "html" ,
			success: function( data ){
				var cmrLine = data.split( "\n" );
				for ( var i = 0 ; i < cmrLine.length ; i ++ ){
					var cmrAry = cmrLine[ i ].split( " " );
					var msgType = cmrAry[ 6 ];
					if ( msgType == "a_tableMsg" ){
						readTableMsg( cmrAry , 2 );
					}
				}
			} ,
		} );
	}

	function getPointerPos( axis , bidOrAsk ){
		var pos = 0;
		var priorPos = 0;
		var index = 1000;
		var maxPos = ( gModeAxisNumObj[ axis + "Num" ] - 1 ) / 2;
		var minPos = maxPos * -1;
		var auto = gAlgoStatusObj[ bidOrAsk + "Auto" ];
		var manualPos = 0;
		var usePrior = false;

		if ( auto ){
			for ( var i = 0 ; i < gModeCauseAry.length ; i++ ){
				var obj = gModeCauseAry[ i ];
				var objPos = obj[ bidOrAsk + "Pos" ];
				var objIndex = obj.index;
				var objPriority = obj[ "priority" + objPos ];
				if ( obj.axis != axis ){
					continue;
				}
				if ( obj.disable ){
					continue;
				}
				if ( !obj.on ){
					continue;
				}
				if ( obj.title != "Manual Adject" ){
					if ( objPriority == 1 && objIndex < index ){
						priorPos = objPos;
						index = objIndex;
						usePrior = true;
						continue;
					}
					pos += objPos;
				}else if ( obj.title == "Manual Adject" ){
					manualPos = objPos;
				}
			}

			if ( usePrior ){
				return priorPos;
			}

			if ( pos > maxPos ){
				pos = maxPos;
			}else if ( pos < minPos ){
				pos = minPos;
			}

			pos += manualPos;

			if ( pos > maxPos ){
				pos = maxPos;
			}else if ( pos < minPos ){
				pos = minPos;
			}

		}else{
			for ( var i = 0 ; i < gModeCauseAry.length ; i++ ){
				var obj = gModeCauseAry[ i ];
				var objPos = obj[ bidOrAsk + "Pos" ];
				var objIndex = obj.index;
				var objPriority = obj[ "priority" + objPos ];
				if ( obj.axis != axis ){
					continue;
				}
				if ( obj.disable ){
					continue;
				}
				if ( !obj.on ){
					continue;
				}
				if ( obj.title != "Manual Adject" ){
					continue;
				}
				pos = objPos;
				break;
			}
		}

		return pos;
	}

	function getSymbolList(){
		var slt = $( ".confRefSymDiv" ).find( ".confSlet" );
		$( slt ).empty();
		var ecn = eval( "gSymOpt_" + readParmAry( gConfOrderAry , "RefEcn" ).select );
		var symList = "";

		$.each( ecn , function ( value ){
			symList += "<option value = " + value + "> "+ value + "</option>";
		} );

		$( slt ).append( symList );
		$( slt ).val( readParmAry( gConfOrderAry , "RefSym" ).select );
		$( slt ).trigger( "change" );
		$( slt ).foundationCustomForms();


		var slt = $( ".confTradeSymDiv" ).find( ".confSlet" );
		$( slt ).empty();
		var ecn = eval( "gSymOpt_" + readParmAry( gConfOrderAry , "TradeEcn" ).select );
		var symList = "";

		$.each( ecn , function ( value ){
			symList += "<option value = " + value + "> "+ value + "</option>";
		} );

		$( slt ).append( symList );
		$( slt ).val( readParmAry( gConfOrderAry , "TradeSym" ).select );
		$( slt ).trigger( "change" );
		$( slt ).foundationCustomForms();
	}

	function getCurrentOrder(){
		updateModeOrder( gModePositionObj.bidShift , gModePositionObj.bidSpread , "bid" );
		updateModeOrder( gModePositionObj.askShift , gModePositionObj.askSpread , "ask" );
	}

	function getDailyOrderCount(){
		var type = "cme-msg-stats";
		var host = "https://bigmindtrader.com:28443/ts/";
		var link = host + type;

		$.ajax( {
			url: link ,
			dataType: "html" ,
			success: function( data ){
				vueOrderCountTable.orderCountAry = [];
				var ary = JSON.parse( data );

				for ( var i = 0 ; i < ary.length ; i ++ ){
					var obj = {};
					obj.ecn = ary[ i ].e;
					obj.sym = ary[ i ].s;

					if ( typeof( ary[ i ].c.N ) == "undefined" ){
						obj.num = 0;
					}else{
						obj.num = ary[ i ].c.N;
					}

					vueOrderCountTable.orderCountAry.push( obj );
					dailyOrderGuardian( obj.ecn , obj.sym , obj.num )
				}
			} ,
		} );
	}

	function getMinuteOhlc(){
		var link = getOhlcLink( "1m" , "60m" );
		showLog( "getMinuteOhlc" , link );
		$.ajax( {
			url: link ,
			dataType: "html" ,
			success: function( data ){
				if ( data.length == 0 ){
					return;
				}
				var json = JSON.parse( data );
				var ary = json[ 0 ].Series[ 0 ].values;
				if ( ary.length == 0 ){
					return;
				}

				ary = ary.reverse();

				updateModeMinuteVolatility( ary );
				updateModeMovingAvgSlope( ary );
				updateModeLongestMinuteBar( ary );
				updateModeVolumeDirectionSignalStep2( ary );
			} ,
		} );
	}

	function getOhlcLink( barSide , duration ){
		var ecn = "TREP";
		var sym = readParmAry( gConfOrderAry , "RefSym" ).select;

		var symCat = sym.split( "/" )[ 1 ];
		if ( symCat == "DJIA5" || symCat == "CRUDE" ){
			ecn = "MREX";
		}

		sym = sym.replace( /\//g , "%2F" );
		var host = "https://bigmindtrader.com:28443/ts/";
		var type = "v1/ohlc";

		return host + type + "?e=" + ecn + "&s=" + sym + "&bs=" + barSide + "&pv=" + duration;
	}

	function reloadAll(){ // reload page count down
		clearInterval( gIntervalObj.reloadCount );
		clearTimeout( gTimerObj.reloadPanel );

		$( ".panlReldCvr" ).show(); // show a cover to cover all button ( not include STOPALL )
		$( ".panlReldNtcSec" ).show();

		var randomMSec = Math.random() * 10000;
		var randomSec = Math.round( randomMSec / 1000 );
		var reloadMSec = 5000;
		var reloadSec = reloadMSec / 1000;
		var totalSec = reloadSec + randomSec; // count down 5 - 15 seconds
		var totalMSec = randomMSec + reloadMSec; // add a random second to avoid everyone reload at the same time

		$( ".panlReldTimeSn" ).html( totalSec );

		showReload();

		gIntervalObj.reloadCount = setInterval( showReload , 1000 );
		gTimerObj.reloadPanel = setTimeout( reload , totalMSec );

		function showReload(){
			totalSec -= 1;
			$( ".panlReldTimeSn" ).html( totalSec );

			if ( totalSec <= 0 ){
				clearInterval( gIntervalObj.reloadCount );
			}
		}

		function reload(){
			gStatusObj.reloaded = true; // mark this user is reloaded. unable to send and receive any CMD and CMR

			clearInterval( gIntervalObj.panelShort ); // stop panel timer
			gIntervalObj.panelShort = false;
			clearInterval( gIntervalObj.panelLong );
			gIntervalObj.panelLong = false;

			showReloadNotice(); // show reload notice
		}
	}

	function checkIptVal( ctrlDiv , maxVal , MinVal ){ // input validation
		var iptElem = $( ctrlDiv ).find( ".txtIpt" );
		var iptVal = parseFloat( $( iptElem ).val() );

		if ( iptVal <= maxVal && iptVal >= MinVal ){
			return iptVal;

		}else if ( iptVal > maxVal ){
			$( iptElem ).attr( "value" , maxVal );
		}else if ( iptVal < MinVal ){
			$( iptElem ).attr( "value" , MinVal );
		}

		$( iptElem ).addClass( "confWarnIpt" , 300 );
		$( iptElem ).delay( 600 ).removeClass( "confWarnIpt" , 5000 );
		$( iptElem ).focus();
		return false;
	}

	function clearGlobalVal(){ // clear global variable
		gSessionIdAry = [];
	}

	function ctrlKeyDown( e ){ // ctrl key down
		if ( e.keyCode != 17 ){
			return;
		}

		var $keyReldBtn = $( ".confPanlCfmDiv" );
		if ( $keyReldBtn.is( ":hover" ) ){
			$( ".confPanlCfmDiv" ).hide();
			$( ".confReldDiv" ).show();
		}

		var $modeDeleteBtn = $( ".confModeDeleteDiv" );
		if ( $modeDeleteBtn.is( ":hover" ) ){
			$( ".confModeDeleteDiv" ).hide();
			$( ".confModeDeleteAllDiv" ).show();
		}

		var $keyCfmBtn = $( ".confOrderCfmDiv" );
		if ( $keyCfmBtn.is( ":hover" ) ){
			$( ".confOrderCfmDiv" ).hide();
			$( ".confDeleteAllDiv" ).show();
		}
	}

	function ctrlKeyUp( e ){ // ctrl key up
		if ( e.keyCode != 17 ){
			return;
		}
		$( ".confReldDiv" ).hide();
		$( ".confPanlCfmDiv" ).show();
		$( ".confDeleteAllDiv" ).hide();
		$( ".confOrderCfmDiv" ).show();
	}

	function shiftKeyDown( e ){ // ctrl key down
		if ( e.keyCode != 16 ){
			return;
		}

		var $keyReldBtn = $( ".confPanlCfmDiv" );
		if ( $keyReldBtn.is( ":hover" ) ){
			$( ".confPanlCfmDiv" ).hide();
			$( ".confReldAllDiv" ).show();
		}
	}

	function shiftKeyUp( e ){ // ctrl key up
		if ( e.keyCode != 16 ){
			return;
		}
		$( ".confReldAllDiv" ).hide();
		$( ".confPanlCfmDiv" ).show();
	}

	function pushCMD( cmd ){
		if ( gStatusObj.disconnected || gStatusObj.reloaded ){
			return;
		}

		gInitOrdSubscription.push( cmd );
		waitCMR();
	}

	function hideBgAlgoCount(){ // hide algo count on tab badge
		clearTimeout( gTimerObj.getBadgeNum );

		gTimerObj.getBadgeNum = setTimeout( function (){
			var badgeSp = $( gPanelID ).find( ".badgeSp" );
			$( badgeSp ).text( "" );
			$( badgeSp ).hide();
		} , 800 );
	}

	function validateParameter( parmName , parmNum , parm ){ // validate algo parameter
		if ( typeof parm == "undefined" || parmNum.toString() == "NaN" ){
			var msg = "parameter name: " + parmName + " is NaN or undefined. Parameter : " + parm;
			showDebugLog( "validateParameter" , msg );
			return false;
		}
		return true;
	}

	function saveMaxProfit(){ // save daily maximum profit to server
		var pnl = getAccPnL();

		if ( pnl <= gMaxProfit ){ // check if got new day high
			return;
		}

		gMaxProfit = pnl;
		updatePnLStopLable();

		$.when( checkIsLeader() ).then( function (){
			if ( !gLeaderObj.isLeader ){ // check if the user is leader or not
				return;
			}

			var time = getCurrentTime();
			var ary = [ time , gMaxProfit ];

			setMacro( "a_maxProfit" , ary );
		} );
	}

	function showLossFromTop( show ){ // show loss from top information on algo tab
		var type = "LossFromTop";
		var cfg = readParmAry( gConfGuardAry , type );

		if ( cfg.off || cfg.pause || cfg.flat || cfg.flatAll || cfg.notice ){
			updatePnLStopLable();
			$( ".panlMaxProfitDiv" ).show();
		}else{
			$( ".panlMaxProfitDiv" ).hide();
		}
	}

	function renewMaxProfit(){
		var time = getCurrentTime();
		var pnl = getAccPnL();
		var ary = [ time , pnl ];

		setMacro( "a_maxProfit" , ary );
	}

	function GFA_disconnected(){
		reloadAll(); // reload the page if GFA is disconnected.

		gStatusObj.disconnected = true; // disable panel react if gfa is disconnected
	}

	function updatePnLStopLable(){
		var type = "LossFromTop";
		var cfg = readParmAry( gConfGuardAry , type );

		var pnlStop = gMaxProfit + cfg.parm;
		var regex = /(\d)(?=(\d{3})+\.)/g;
		pnlStop = pnlStop.toFixed( 2 ).replace( regex , "$1 , " );

		$( ".panlMaxProfitSn" ).text( pnlStop );
	}

	function readParmAry( array , name ){
		for ( var i = 0 ; i < array.length ; i++ ){
			var obj = array[ i ];
			if ( obj[ "name" ] == name ){
				return obj;
			}
		}
	}

	function editParmAry( array , name , property , value ){
		for ( var i = 0 ; i < array.length ; i++ ){
			if ( array[ i ][ "name" ] == name ){
				array[ i ][ property ] = value;
				return;
			}
		}
	}

	function playSound( sound ){
		var on = readParmAry( gLocalCfgAry , "PlayAlert" ).on;
		if ( !on ){
			return;
		}

		soundManager.play( sound );
	}

	function storeMAQuoteDate( m ){
		var ecn = m.e;
		var maSym = m.s;

		var sym = maSym.split( "MA_60_" )[ 1 ];
		var maBid = checkValue( m.d[ 0 ].bid );
		var maAsk = checkValue( m.d[ 0 ].ask );
		var bid = checkValue( m.d[ 1 ].bid );
		var ask = checkValue( m.d[ 1 ].ask );
		var totalVol = checkValue( m.d[ 2 ].bid );
		var sec900Vol = checkValue( m.d[ 2 ].bidVol );
		var sec60Vol = checkValue( m.d[ 2 ].ask );

		if ( bid == 0 || ask == 0 || maBid == 0 || maAsk == 0 ){
			return;
		}

		var ts = parseFloat( getSymItem( ecn , sym ).ts );
		var dip = parseFloat( getSymItem( ecn , sym ).dip );
		var maMid = ( ( maBid + maAsk ) / 2 ).toFixed( dip + 1 );
		var maSpreadPip = ( ( maAsk - maBid ) / ts ).toFixed( dip );

		var obj = { ecn: ecn ,
					sym: sym ,
					maBid: maBid ,
					maAsk: maAsk ,
					bid: bid ,
					ask: ask ,
					totalVol: totalVol ,
					sec900Vol: sec900Vol ,
					sec60Vol: sec60Vol ,
					ts: ts ,
					dip: dip ,
					maMid: maMid ,
					maSpreadPip: maSpreadPip ,
					bidVol: 0 ,
					askVol: 0 ,
					rtsBidVol: 0 ,
					rtsAskVol: 0 ,
					rlBidVol: 0 ,
					rlAskVol: 0 ,
					};

		gMAQuoteAry.unshift( obj ); // add 1 row on top

		if ( gMAQuoteAry.length > 100 ){
			gMAQuoteAry.pop(); // remove last row
		}

		function checkValue( num ){
			num = parseFloat( num );
			if ( isNaN( num ) ){
				return 0;
			}
			return num;
		}
	}

	function storeQuoteDate( m ){
		var depthParm = readParmAry( gModeCauseAry , "MarketLiquidity" ).propParm1;
		var depthPropParm = readParmAry( gModeCauseAry , "LiquidityProportion" ).propParm1;
		var tickVolParm = readParmAry( gModeCauseAry , "TickVolatility" ).propParm1;

		var ecn = m.e;
		var sym = m.s;
		var ts = parseFloat( getSymItem( ecn , sym ).ts );
		var dip = parseFloat( getSymItem( ecn , sym ).dip );

		var bid = parseFloat( m.d[ 0 ].bid );
		var ask = parseFloat( m.d[ 0 ].ask );

		var bidLiq = parseFloat( m.d[ 0 ].bidVol );
		var askLiq = parseFloat( m.d[ 0 ].askVol );

		var totalBidLiq = 0;
		var totalAskLiq = 0;

		var pxSpreadPip = ( ask - bid ) / ts;

		for ( var i = 0 ; i < m.d.length ; i++ ){
			totalBidLiq += parseFloat( m.d[ i ].bidVol );
			totalAskLiq += parseFloat( m.d[ i ].askVol );
		}

		var bidDepth = 0;
		var askDepth = 0;

		var bidPropCount = 0;
		var askPropCount = 0;

		if ( depthParm > m.d.length ){
			depthParm = m.d.length;
		}

		if ( depthPropParm > m.d.length ){
			depthPropParm = m.d.length;
		}

		for ( var i = 0 ; i < depthParm ; i++ ){
			bidDepth += parseFloat( m.d[ i ].bidVol );
			askDepth += parseFloat( m.d[ i ].askVol );
		}

		for ( var i = 0 ; i < depthPropParm ; i++ ){
			bidPropCount += parseFloat( m.d[ i ].bidVol );
			askPropCount += parseFloat( m.d[ i ].askVol );
		}

		var bidProp = ( bidPropCount / ( bidPropCount + askPropCount ) ) * 100;
		var askProp = ( askPropCount / ( bidPropCount + askPropCount ) ) * 100;

		var tickVolAry = gQuoteAry.slice( 0 , tickVolParm );

		if ( tickVolAry.length == tickVolParm ){
			var tickBidMax = Math.max.apply( "" , tickVolAry.map( function( o ){ return o.bid ; } ) );
			var tickBidMin = Math.min.apply( "" , tickVolAry.map( function( o ){ return o.bid ; } ) );
			var tickAskMax = Math.max.apply( "" , tickVolAry.map( function( o ){ return o.ask ; } ) );
			var tickAskMin = Math.min.apply( "" , tickVolAry.map( function( o ){ return o.ask ; } ) );
			var tickBidVol = ( tickBidMax - tickBidMin ) / ts;
			var tickAskVol = ( tickAskMax - tickAskMin ) / ts;
		}else{
			var tickBidVol = 0;
			var tickAskVol = 0;
		}

		var obj = { ecn: ecn ,
					sym: sym ,
					bid: bid ,
					ask: ask ,
					bidLiq: bidLiq ,
					askLiq: askLiq ,
					totalBidLiq: totalBidLiq ,
					totalAskLiq: totalAskLiq ,
					tickBidVol: tickBidVol ,
					tickAskVol: tickAskVol ,
					bidDepth: bidDepth ,
					askDepth: askDepth ,
					bidProp: bidProp ,
					askProp: askProp ,
					pxSpreadPip: pxSpreadPip ,
					ts: ts ,
					dip: dip ,
					};

		gQuoteAry.unshift( obj ); // add 1 row on top

		if ( gQuoteAry.length > 100 ){
			gQuoteAry.pop(); // remove last row
		}
	}

	function clearQuoteDate(){
		clearTimeout( gTimerObj.clearQuoteData );

		gTimerObj.clearQuoteData = setTimeout( function (){
			gQuoteAry = [];
			gMAQuoteAry = [];
		} , 2000 );
	}

	function pushFiveSecondVol(){
		var obj = gMAQuoteAry[ 0 ];

		if ( typeof obj == "undefined" ){
			return;
		}

		if ( obj.ecn == "PATS" || obj.ecn == "PATX" ){
			ecn = "MREX";
		}

		if ( gMAQuoteAry.length < gFiveSecondTick ){
			return;
		}

		var dataName = obj.ecn + "MA_60_" + obj.sym;

		var sec5Vol = gMAQuoteAry[ 0 ].totalVol - gMAQuoteAry[ gFiveSecondTick - 1 ].totalVol;
		gInitDataStore[ "ma_rollavg" ].set( dataName , sec5Vol );
	}

	function waitCMR(){
		if ( gIntervalObj.waitCMR ){
			return;
		}

		clearInterval( gIntervalObj.waitCMR );
		gIntervalObj.waitCMR = false;

		gStatusObj.gotCMR = false;
		var retryTime = 0;
		var waitCMRTime = 1000;

		if ( gIntervalObj.waitCMR ){
			return;
		}

		gIntervalObj.waitCMR = setInterval( function (){
			if ( gStatusObj.gotCMR ){
				clearInterval( gIntervalObj.waitCMR );
				gIntervalObj.waitCMR = false;
				return;
			}

			if ( retryTime < 5 ){
				retryTime ++;
				return;
			}

			clearInterval( gIntervalObj.waitCMR );
			gIntervalObj.waitCMR = false;

			gStatusObj.disconnected = true;
			var msg = "Unable to receive CMR. Disconnect Client.";
			sendLogMsg( msg );
		} , waitCMRTime );
	}

	function storeTradeDate( m ){
		var px = parseFloat( m[ 3 ] );
		var qty = parseFloat( m[ 4 ] );
		var totalVol = parseFloat( m[ 5 ] );

		var obj = gMAQuoteAry[ 0 ];

		if ( typeof( obj ) == "undefined" ){
			return;
		}

		if ( px <= obj.bid ){
			gMAQuoteAry[ 0 ].bidVol += qty;
		}else if ( px >= obj.ask ){
			gMAQuoteAry[ 0 ].askVol += qty;
		}
	}

	function storeReutersTradeDate( m ){
		if ( m.length < 7 ){
			return;
		}

		var obj = gMAQuoteAry[ 0 ];
		if ( typeof( obj ) == "undefined" ){
			return;
		}

		var px = parseFloat( m[ 3 ] );
		var qty = parseFloat( m[ 4 ] );
		var totalVol = parseFloat( m[ 5 ] );
		var bidOrAsk = parseFloat( m[ 7 ] );

		if ( bidOrAsk == 1 ){
			gMAQuoteAry[ 0 ].rtsAskVol += qty;
		}else if ( bidOrAsk == 2 ){
			gMAQuoteAry[ 0 ].rtsBidVol += qty;
		}
	}

	function subscribeSymbol( ecn , sym ){
		var name = "cmeSymbolSubscribe";
		var obj = { ecn: ecn , symbol: sym };
		ee.emit( name , obj );
	}

	function subscribePanelSymbol(){
		var ecn = readParmAry( gConfOrderAry , "TradeEcn" ).select;
		var sym = readParmAry( gConfOrderAry , "TradeSym" ).select;
		var rSym = readParmAry( gConfOrderAry , "RefSym" ).select;

		if ( ecn == "PATX" ){
			ecn = "MREX";
		}

		subscribeSymbol( ecn , sym );
		subscribeSymbol( ecn , "K_" + sym );
		subscribeSymbol( ecn , "MA_60_" + sym );
		subscribeSymbol( "TREP" , "K_" + sym ); // for reuters
		subscribeSymbol( "TREP" , "RL_" + sym );

		if ( ecn == "PATS" ){
			ecn = "MREX";
		}

		subscribeSymbol( ecn , "IND_AI_" + rSym + "#BATT1" ); // for AI
		subscribeSymbol( ecn , "IND_AI_" + rSym + "#S20120" );
		subscribeSymbol( ecn , "IND_AI_" + rSym + "#VT1" );
		subscribeSymbol( ecn , "IND_AI_" + rSym + "#VT2" );
		subscribeSymbol( ecn , "IND_AI_" + rSym + "#PVR1" );
		subscribeSymbol( ecn , "IND_AI_" + rSym + "#BATT2" );
		subscribeSymbol( ecn , "IND_AI_" + rSym + "#BATT3" );
	}

	function storeRateLimitData( m ){
		if ( m.length < 10 ){
			return;
		}

		var obj = gMAQuoteAry[ 0 ];
		if ( typeof( obj ) == "undefined" ){
			return;
		}

		var askPx = parseFloat( m[ 3 ] );
		var askVol = parseFloat( m[ 4 ] );
		var bidPx = parseFloat( m[ 5 ] );
		var bidVol = parseFloat( m[ 6 ] );
		var noSidePx = parseFloat( m[ 7 ] );
		var noSideVol = parseFloat( m[ 8 ] );

		if ( bidPx > 0 && bidVol > 0 ){
			gMAQuoteAry[ 0 ].rlBidVol += bidVol;
		}
		if ( askPx > 0 && askVol > 0 ){
			gMAQuoteAry[ 0 ].rlAskVol += askVol;
		}
	}

	function gotWrongQuoteData(){
		clearTimeout( gTimerObj.readQuoteAgain );

		gTimerObj.readQuoteAgain = setTimeout( function (){
			if ( gStatusObj.gotWrongQuote ){
				gStatusObj.gotWrongQuote = false;
			}
		} , 1000 );
	}

	function countNewOrderMacro( cmrAry ){
		var name = cmrAry[ 3 ];
		var type = name.split( "_" )[ 2 ];
		var bidOrAsk = name.split( "_" )[ 3 ];
		var numStr = name.split( "_" )[ 4 ];
		var id = parseInt( numStr ) - 1;

		if ( type != "main" ){
			return;
		}

		gMacroCount[ bidOrAsk ][ id ] = true;
	}

	function countRemoveOrderMacro( cmrAry ){
		var name = cmrAry[ 3 ];
		var type = name.split( "_" )[ 1 ];
		var bidOrAsk = name.split( "_" )[ 2 ];
		var numStr = name.split( "_" )[ 3 ];
		var id = parseInt( numStr ) - 1;

		if ( type != "main" ){
			return;
		}

		gMacroCount[ bidOrAsk ][ id ] = false;
	}

	function turnOnAllCurrentOrder(){
		onOrder( "bid" );
		onOrder( "ask" );

		function onOrder( bidOrAsk ){
			if ( !gAlgoStatusObj[ bidOrAsk + "On" ] ){
				return;
			}
			var ary = vueCurrentOrder[ bidOrAsk + "OrderAry" ];
			for ( var i = 0 ; i < ary.length ; i++ ){
				var numStr = ( i + 1 ).toString();
				runModeOrderAction( "run" , bidOrAsk , numStr );
			}
		}
	}

	function switchCurrentOrder( bidOrAsk , action ){
		if ( !gAlgoStatusObj.on ){
			return;
		}
		var ary = vueCurrentOrder[ bidOrAsk + "OrderAry" ];
		for ( var i = 0 ; i < ary.length ; i++ ){
			var numStr = ( i + 1 ).toString();
			runModeOrderAction( action , bidOrAsk , numStr );
		}
	}

	function submitOrder( ecn , sym , type , tif , side , qty , px ){
		var poi = gInitUserID.toString() + "999";

		var cmd = "CMD " + gInitUserID + " O " + ecn + " " + type + " " + tif + " " + side + " " + sym + " QTY " + qty + " AT " + px + " POI " + poi + " NAME algoOrder";

		pushCMD( cmd );
		showLog( "submitOrder" , cmd );
	}

	function checkTickVolume( parm ){
		if ( gStatusObj.countingTickVol ){
			return;
		}
		if ( gMAQuoteAry.length < gFiveSecondTick ){
			return;
		}

		gStatusObj.countingTickVol = true;

		var ary = gMAQuoteAry.slice( 0 , gFiveSecondTick );
		var sumVol = ary.reduce( sumTotalVol ).sumVol;
		var index = gFiveSecondTick - 1;

		for ( var i = 0 ; i < gFiveSecondTick ; i++ ){
			if ( typeof gMAQuoteAry[ i + 1 ] == "undefined" ){
				continue;
			}
			if ( gMAQuoteAry[ i ].totalVol >= gMAQuoteAry[ i + 1 ].totalVol * 0.95 ){
				continue;
			}
			if ( gMAQuoteAry[ i ].totalVol > ( sumVol * 0.1 ) ){
				continue;
			}
			index = i;
			break;
		}

		if ( index == gFiveSecondTick - 1 ){
			sendTickVolume();
		}else{
			gTickCount = index;
		}

		function sumTotalVol( a , b ){
			return { sumVol: a.totalVol + b.totalVol };
		}
	}

	function countTickVolume(){
		if ( !gStatusObj.countingTickVol ){
			return;
		}
		gTickCount++;
		if ( gTickCount < gFiveSecondTick ){
			return;
		}
		sendTickVolume();
	}

	function sendTickVolume(){
		gStatusObj.countingTickVol = false;

		var ary = gMAQuoteAry.slice( 0 , gFiveSecondTick );
		var obj = ary[ 0 ];
		var preObj = ary[ gFiveSecondTick - 1 ];
		var vol = obj.totalVol - preObj.totalVol;

		var mid = parseFloat( ( ( obj.bid + obj.ask ) / 2 ).toFixed( obj.dip + 1 ) );
		var preMid = parseFloat( ( ( preObj.bid + preObj.ask ) / 2 ).toFixed( preObj.dip + 1 ) );

		var rlbidVol = ary.reduce( sumRlBidVol ).rlBidVol;
		var rlAskVol = ary.reduce( sumRlAskVol ).rlAskVol;

		var maxVol = 0;
		var maxVolPx = 0;
		for ( var i = 0 ; i < ( gFiveSecondTick - 1 ) ; i++ ){
			var tickVol = ary[ i ].totalVol - ary[ i + 1 ].totalVol
			if ( tickVol > maxVol ){
				maxVol = tickVol;
				maxVolPx = ( ( ary[ i ].bid + ary[ i ].ask ) / 2 ).toFixed( obj.dip + 1 );
			}
		}

		sendTableMsg( "tickVol" , mid , preMid , vol , rlbidVol , rlAskVol , maxVol , maxVolPx );

		function sumRlBidVol( a , b ){
			return { rlBidVol: a.rlBidVol + b.rlBidVol };
		}
		function sumRlAskVol( a , b ){
			return { rlAskVol: a.rlAskVol + b.rlAskVol };
		}
	}

	function restartStoreChartDataInterval( obj ){
		gCauseChartDataAry = [];

		clearInterval( gIntervalObj.storeCauseData );
		gIntervalObj.storeCauseData = false;

		clearInterval( gIntervalObj.drawDataChart );
		gIntervalObj.drawDataChart = false;

		google.charts.load( "current" , { packages: [ "corechart" , "line" ] } );
		google.charts.setOnLoadCallback( function(){
			var title = obj.title;

			gChartObj = new google.visualization.LineChart( document.getElementById( "modeChartDiv" ) );

			gChartOption = {
				colors: [ "#097138" , "#a52714" ] ,
				title: title ,
				width: 750 ,
				height: 550 ,
				chartArea: { width: "70%" , height: "80%" } ,
			};

			gChartData = new google.visualization.DataTable();
			gChartData.addColumn( "datetime" , "X" );
			gChartData.addColumn( "number" , "Bid" );
			gChartData.addColumn( "number" , "Ask" );
		} );

		gIntervalObj.storeCauseData = setInterval( function(){
			storeCauseChartData( obj );
		} , 1000 );

		gIntervalObj.drawDataChart = setInterval( function(){
			drawCauseDataChart();
		} , 3000 );
	}

	function storeCauseChartData( obj ){
		var bidStatus = obj.bidStatus;
		var askStatus = obj.askStatus;

		if ( bidStatus == "" ){
			bidStatus = null;
		}else{
			bidStatus = parseFloat( bidStatus );;
		}
		if ( askStatus == "" ){
			askStatus = null;
		}else{
			askStatus = parseFloat( askStatus );;
		}

		var date = new Date();
		var minutes = ( "0" + ( date.getMinutes() ) ).slice( -2 );
		var seconds = ( "0" + ( date.getSeconds() ) ).slice( -2 );
		var time = date.getHours() + ":" + minutes + ":" + seconds;

		gCauseChartDataAry.push( [ date , bidStatus , askStatus ] );

		if ( gCauseChartDataAry.length > 180 ){
			gCauseChartDataAry.shift();
		}
	}

	function drawCauseDataChart(){
		if ( !$( ".modeChartSec" ).is( ":visible" ) ) {
			return;
		}
		if ( typeof( gChartData ) == "undefined" ){
			return;
		}

		var rowNum = gChartData.getNumberOfRows();
		gChartData.removeRows( 0 , rowNum );
		gChartData.addRows( gCauseChartDataAry );

		gChartObj.draw( gChartData , gChartOption );
	}

	function storeFilledQuote(){
		gQuoteAry[ 0 ].filled = true;
	}
}