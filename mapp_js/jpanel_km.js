function jpanelKM( gInitObj , showBadgeNotice , showJPanelMsg , gOnTrigNotice ){
	var gInitUserID = gInitObj.u;
	var gInitOrdSubscription = gInitObj.oc;

	// default setting //

	var gDefKeySide = "BUY";
	var gDefKeyQty = 1;
	var gDefKeySlip = 10;
	var gDefKeyECN = "ECN";
	var gDefKeyDur = "DAY";
	var gDefKeyRef = "BID";
	var gDefKeyGp = "1";
	var gDefCnlECN = "HKE";
	var gDefLOBUYPx = -20;
	var gDefLOSELLPx = 20;
	var gDefSOBUYPx = 20;
	var gDefSOSELLPx = -20;
	var gDefChdOrdDist = 10; // default ifdone order stop/limit distance

	////// jPanel object ( global variable ) ////

	var gKeySide = gDefKeySide; //default order side
	var gKeyKey = "";
	var gKeySleted = 0;
	var gKeyNameSleted = 0;
	var gKeySpecTabOn = 0;
	var gKeySpecAry = 0;
	var gKeyAryGet = new Array();
	var gKeyArySet = new Array();
	var gKeyAryLen = 0;
	var gKeyName = $( ".pxChtDiv" ).eq( 0 ).attr( "id" ); //default key template
	var gPressedCtrlBtn = 0; //check whether the Ctrl key is pressed or not
	var gSavingKey = 0;
	var gSetNewKey = 0;
	var gInstruct = 1;
	var gSletedECN = "";
	var gSletedSym = "";
	var gCnfmType = 0;
	var gLnkedKeyECN = 0;
	var gLnkedKeySym = 0;
	var gOrdPrefix = gInitUserID + "9";
	var gOrdGpAllId = "0";
	var gOrdGpAId = "1";
	var gOrdGpBId = "2";
	var gOrdGpCId = "3";
	var gOrdTypeLoId = "6";
	var gOrdTypeSoId = "7";
	var gOrdSideBuyId = "8";
	var gOrdSideSellId = "9";
	var gRandomNum = 0;
	var gSessID = 0;
	var gIP;
	var gVersion = "1a";
	var gGetIPInterval = false;

	var gDisconnected = false;
	var gReloaded = false;
	var gWaitCMRInterval = false;
	var gGotCMR = false;
	var gShowLog = false;

	///// d3 cht object ////
	var gChtScale = 0.3;
	var gChtMulti = 1.212;
	var gChtMin = 0;
	var gChtMax = 0;
	var gChtZoom = 1;
	var gChtZmScale = 1.5;
	var gChtZmLevel = 3;
	var gChtMinZmLv = 1;
	var gChtMaxZmLv = 10;
	var gSletedChtID = "";
	var gSletedLineNum = 1;
	var gSletedLineID = gKeyName + "_" + gKeySide + "Line" + gSletedLineNum;
	var dragEvent = d3.behavior.drag().on( "drag" , dragChtClass );
	var zoomEvent = d3.behavior.zoom().on( "zoom" , zoomChtClass );

	var gHostName = window.location.hostname;
	if ( gHostName == "192.168.1.187" ){ // for m6 testing server setting
		//gDefCnlECN = "MREX";
		gDefCnlECN = "SAXO";
		//gShowLog = true;
	}

	//default key array
	for ( var i = 0; i < 4; i++ ){
		gKeyArySet[ i ] = { keyKey:"" , keyName:"" , keyType:"" , keySide:gDefKeySide , keyPx:"" , keyQty:gDefKeyQty , keySlip:gDefKeySlip , keyECN:gDefKeyECN , keySym:"" , keyDur:gDefKeyDur , keyRef:gDefKeyRef , keyGp: gDefKeyGp };
	}

	///// jquery event

	$( ".kbUablKey" ).click( sletKeyKey ); //select key
	$( "#keySaveBttn" ).click( ckCnfmSaveKey );
	$( "#keyDelBtn" ).click( ckCnfmDelKey );
	$( "#keyDelAllBtn" ).click( ckCnfmDelAllKey );
	$( "#keyCnfmBtn" ).click( cnfmCnfmed );
	$( ".cnfmBtn" ).click( function(){
		$( "#cnfmBg" ).hide();
		$( "#cnfmDiv" ).hide();
	});
	$( ".keyBtn" ).click( showCnfmMsg );
	$( ".cnlOrdIpt" ).change( function(){ sletCnlOrdKey( $( this ).val() ); });
	$( ".cnlOrdGpKey" ).click( function(){ sletCnlOrdGp( $( this ).val() ); });
	$( "#keyDtlECNIpt" ).change( function(){ sletKeyECN( this ); });
	$( "#keyDtlSymIpt" ).change( function(){ sletKeySym( this ); });
	$( "#keyDtlDurIpt" ).change( function(){ sletKeyDur( this ); });
	$( "#keyDtlRefIpt" ).change( function(){ sletKeyRef( this ); });
	$( "#keyDtlGpIpt" ).change( function(){ sletKeyGp( this ); });
	$( ".keyTypeIpt" ).click( function(){
		sletKeyType( this.value );
		showInstruct( 3 );
	});
	$( ".keySideIpt" ).click( function(){
		sletKeySide( this.value , 1 );
		showInstruct( 4 );
	});
	$( ".keyDtlDiv" ).click( function(){
		showInstruct( 5 );
	});
	$( "#keyChdOrdDist" ).change( function(){ setChtChdLine( gSletedLineID , this.value ); });
	$( ".keyIfdTypeSec" ).hover( function(){
		if ( $( this ).hasClass( "keyIfdEnable" ) ){
			$( ".keyIfdTypeSec" ).each( function(){
				$( this ).removeClass( "keyIfdSleted" );
				$( this ).addClass( "keyIfdUnsleted" );
			});
			$( this ).removeClass( "keyIfdUnsleted" );
			$( this ).addClass( "keyIfdSleted" );
		}
	} , function(){
		$( ".keyIfdTypeSec" ).each( function(){
			$( this ).removeClass( "keyIfdSleted" );
			$( this ).removeClass( "keyIfdUnsleted" );
		});
	});
	$( document ).keydown( function( e ){ ctrlKeyDown( e ); });
	$( document ).keyup( function ( e ){ ctrlKeyUp( e ); });
	$( "#pxChtZmInBtn" ).click( zmChtIn );
	$( "#pxChtZmOutBtn" ).click( zmChtOut );
	$( "#keyDtlLnkECNIpt" ).change( function(){
		linkKeyECN( 3 );
	});
	$( "#keyDtlLnkSymIpt" ).change( function(){
		linkKeySym( 3 );
	});
	//$( ".keyDtlLnkTtp" ).tooltip();
	$( "#keyDtlQttyHadle" ).mousedown( function( e ){
		if ( e.button == 2 ){
			showKeyIptDiv( 1 );
		}
	});
	$( "#keyDtlSlipHadle" ).mousedown( function( e ){
		if ( e.button == 2 ){
			showKeyIptDiv( 2 );
		}
	});
	$( "#keyDtlQttyIpt" ).focusout( function(){
		iptKeyCtrlVal( this.value , 1 );
	});
	$( "#keyDtlQttyIpt" ).change( function(){
		iptKeyCtrlVal( this.value , 1 );
	});
	$( "#keyDtlSlipIpt" ).focusout( function(){
		iptKeyCtrlVal( this.value , 2 );
	});
	$( "#keyDtlSlipIpt" ).change( function(){
		iptKeyCtrlVal( this.value , 2 );
	});
	$( ".keyIfdIpt" ).change( function(){
		if ( this.checked ){
			addChtLine( this.value );
			$( this ).parents().eq( 2 ).addClass( "keyIfdEnable keyIfdSleted" );
		}else{
			rmChtLine( this.value );
			$( this ).parents().eq( 2 ).removeClass( "keyIfdEnable keyIfdSleted" );
		}
	});
	$( ".algoOrdIpt" ).click( sletAlgoOrdKey );
	//create slider object
	var qttyHandle = $( "#keyDtlQttyHadle" );
	$( "#keyDtlQttySldrDiv" ).slider( {
		min: 1 ,
		max: 10 ,
		value: 1 ,
		create: function(){
			qttyHandle.text( $( this ).slider( "value" ) );
		} ,
		slide: function( event , ui ){
			qttyHandle.text( ui.value ) ;
			gKeyArySet[ gSletedLineNum - 1 ].keyQty = ui.value;
		}
	});
	var slipHandle = $( "#keyDtlSlipHadle" );
	$( "#keyDtlSlipSldrDiv" ).slider( {
		min: 0 ,
		max: 20 ,
		value: 10 ,
		create: function(){
			slipHandle.text( $( this ).slider( "value" ) );
		} ,
		slide: function( event , ui ){
			slipHandle.text( ui.value );
			gKeyArySet[ gSletedLineNum - 1 ].keySlip = ui.value;
		}
	});
	var ifdLOQttyHandle = $( "#keyIfdLOQttyHadle" );
	$( "#keyIfdLOQttySldrDiv" ).slider( {
		min: 1 ,
		max: 10 ,
		value: 1 ,
		create: function(){
			ifdLOQttyHandle.text( $( this ).slider( "value" ) );
		} ,
		slide: function( event , ui ){
			ifdLOQttyHandle.text( ui.value );
			gKeyArySet[ gSletedLineNum - 1 ].keySlip = ui.value;
		}
	});
	var ifdSOQttyHandle = $( "#keyIfdSOQttyHadle" );
	$( "#keyIfdSOQttySldrDiv" ).slider( {
		min: 1 ,
		max: 10 ,
		value: 1 ,
		create: function(){
			ifdSOQttyHandle.text( $( this ).slider( "value" ) );
		} ,
		slide: function( event , ui ){
			ifdSOQttyHandle.text( ui.value );
			gKeyArySet[ gSletedLineNum - 1 ].keySlip = ui.value;
		}
	});
	var ifdSOSlipHandle = $( "#keyIfdSOSlipHadle" );
	$( "#keyIfdSOSlipSldrDiv" ).slider( {
		min: 1 ,
		max: 10 ,
		value: 1 ,
		create: function(){
			ifdSOSlipHandle.text( $( this ).slider( "value" ) );
		} ,
		slide: function( event , ui ){
			ifdSOSlipHandle.text( ui.value );
			gKeyArySet[ gSletedLineNum - 1 ].keySlip = ui.value;
		}
	});

	init(); // start panel

	// jPanelKM function ///
	function init(){
		ee.addListener( "MC_CMR" , readMacro ); // call readMacro when macro command emit listener received "CMR"
		ee.addListener( "MMO_O_CH_DIS" , GFA_disconnected ); // run GFA_disconnected when GFA is disconnected

		linkKeySym( 1 );
		$( "#keySide" + gDefKeySide ).prop( "checked" , true );
		drawAllCht();
		sletKeyType( "tpLOv1" );

		$.when( getClientIP() ).then( function (){ // wait until client got IP address
			getMacro();
		});
	}

	function getMacro(){ // get existing hot key macro
		var sessID = "";
		if ( gSessID != 0 ){
			sessID = gSessID + " ";
		}
		gRandomNum = getRandomNumber();

		var cmd = "CMD " + gInitUserID + " GET_MACRO " + sessID + "SEQ " + gIP + "::" + gRandomNum + "::" + gVersion;
		pushCMD( cmd );
		showLog( "getMacro" , cmd );
	}

	function setMacro( macroCMD ){ // set a new hotkey
		pushCMD( macroCMD.macroCMD1 );
		showLog( "setMacro" , macroCMD.macroCMD1 );

		pushCMD( macroCMD.macroCMD2 );
		showLog( "setMacro" , macroCMD.macroCMD2 );
	}

	function readMacro( cmrMsg ){ // receive "CMR" message
		showLog( "readMacro" , cmrMsg );
		gGotCMR = true;
		var cmrAry = cmrMsg.split( "\n" )
		var cmrHandled = 0;
		for ( var i = 0; i < cmrAry.length; i++ ){
			var cmrAryWs = cmrAry[ i ].split( " " );
			if ( cmrAryWs.length < 4 || cmrAryWs[ 0 ] != "CMR" || cmrAryWs[ 1 ] != gInitUserID ){
				return;
			}

			var cmrUserID = cmrAryWs[ 1 ];
			var cmdType = cmrAryWs[ 2 ];
			var cmrKey = cmrAryWs[ 3 ].slice( 3 );
			var cmrName = cmrAryWs[ 5 ];
			if ( cmrAryWs[ 2 ] == "MACRO" ){ // check if it is generated by jpanel
				if ( cmrAryWs[ 3 ].slice( 0 , 3 ) != "KEY" ){
					continue;
				}
				if ( cmrName != "tpLOv1" && cmrName != "tpSOv1" && cmrName != "tpCancel" && cmrName != "tpAlgo" ){
					continue;
				}
				gKeyAryGet[ gKeyAryLen ] = new Array();
				var cmrAryWnl = cmrAry[ i ].split( "|" );
				if ( cmrName == "tpCancel" ){ //import Cancel order
					for ( var j = 0; j < cmrAryWnl.length; j++ ){
						cmrAryWnlWs = cmrAryWnl[ j ].split( " " );
						if ( j == 0 ){
							cmrAryWnlWs = cmrAryWnlWs.splice( 6 , 20 );
						}

						var poi = cmrAryWnlWs[ 7 ].toString();
						var poiAry = poi.split( gOrdPrefix );

						var keyGp = gOrdGpAllId;
						var keyType = "LO2";
						var keySide = "BUY";

						if ( poiAry.indexOf( gOrdGpAId ) == -1 || poiAry.indexOf( gOrdGpBId ) == -1 || poiAry.indexOf( gOrdGpCId ) == -1 ){
							if ( poiAry.indexOf( gOrdGpAId ) != -1 ){
								keyGp = gOrdGpAId;
							}
							if ( poiAry.indexOf( gOrdGpBId ) != -1 ){
								keyGp = gOrdGpBId;
							}
							if ( poiAry.indexOf( gOrdGpCId ) != -1 ){
								keyGp = gOrdGpCId;
							}
						}
						if ( poiAry.indexOf( gOrdTypeLoId ) != -1 ){
							keyType = "LO2";
						}
						if ( poiAry.indexOf( gOrdTypeSoId ) != -1 ){
							keyType = "SO2";
						}
						if ( poiAry.indexOf( gOrdSideBuyId ) != -1 ){
							keySide = "BUY";
						}
						if ( poiAry.indexOf( gOrdSideSellId ) != -1 ){
							keySide = "SELL";
						}

						var keyCnlType = "ALL" + keySide + keyType;

						if ( cmrAryWnlWs[ 2 ] == "CO" && cmrAryWnlWs[ 4 ] == "CANCEL" ){
							gKeyAryGet[ gKeyAryLen ][ j ] = { keyUserID:cmrUserID , keyKey:cmrKey , keyName:cmrName , keyECN:cmrAryWnlWs[ 1 ] , keyDur:cmrAryWnlWs[ 3 ] , keyCnlType:keyCnlType , keySide:gDefKeySide , keyPx:"" , keyQty:gDefKeyQty , keySlip:gDefKeySlip , keySym:"" , keyRef:gDefKeyRef , keyClass:"pnt" , keyGp: keyGp };
						}
					}
				}else if ( cmrName == "tpAlgo" ){
					var keyAlgoOrder = cmrAryWs[ 10 ];
					var obj = { keyUserID:cmrUserID , keyKey:cmrKey , keyName:cmrName , keyECN:"" , keyDur:"" , keyCnlType:"" , keySide:gDefKeySide , keyPx:"" , keyQty:gDefKeyQty , keySlip:gDefKeySlip , keySym:"" , keyRef:gDefKeyRef , keyClass:"pnt" , keyGp: gDefKeyGp , keyAlgoOrder: keyAlgoOrder };
					gKeyAryGet[ gKeyAryLen ][ 0 ] = obj;

				}else if ( cmrName == "tpLOv1" || cmrName == "tpSOv1" ){ //import LO2 & SO2 order
					for ( var j = 0; j < cmrAryWnl.length; j++ ){
						cmrAryWnlWs = cmrAryWnl[ j ].split( " " );
						if ( cmrAryWnlWs[ 0 ] == "IF_DONE" ){
							var cmrClass = "chd";
						}else{
							var cmrClass = "pnt";
						}
						if ( j == 0 ){
							cmrAryWnlWs = cmrAryWnlWs.splice( 6 , 20 );
						}else if ( j > 0 && cmrAryWnlWs[ 0 ] == "IF_DONE" ){
							//cmrAryWnlWs = cmrAryWnlWs.splice( 8 , 20 );
							cmrAryWnlWs = cmrAryWnlWs.splice( 6 , 20 );
						}
						if ( cmrAryWnlWs[ 0 ] == "O" ){ // check it is if done or not
							var cmrGUISign = "BID";
							if ( cmrAryWnlWs[ 9 ] == "GUI_ASK" ){
								cmrGUISign = "ASK";
							}
							if ( cmrAryWnlWs[ 10 ] == "-" ){
								cmrAryWnlWs[ 11 ] = 0 - cmrAryWnlWs[ 11 ];
							}
							if ( cmrName == "tpLOv1" ){
								var cmrKeySlip = gDefKeySlip;
								var poi = cmrAryWnlWs[ 13 ].toString();
							}else if ( cmrName == "tpSOv1" ){
								var cmrKeySlip = cmrAryWnlWs[ 13 ];
								var poi = cmrAryWnlWs[ 17 ].toString();
							}

							var keyGp = "";
							var keyGpID = poi.slice( 0 , 5 ); //42891

							if ( keyGpID == gOrdPrefix + gOrdGpAllId ){
								keyGp = gOrdGpAllId;
							}else if ( keyGpID == gOrdPrefix + gOrdGpAId ){
								keyGp = gOrdGpAId;
							}else if ( keyGpID == gOrdPrefix + gOrdGpBId ){
								keyGp = gOrdGpBId;
							}else if ( keyGpID == gOrdPrefix + gOrdGpCId ){
								keyGp = gOrdGpCId;
							}

							gKeyAryGet[ gKeyAryLen ][ j ] = { keyUserID:cmrUserID , keyKey:cmrKey , keyName:cmrName , keyECN:cmrAryWnlWs[ 1 ] , keyType:cmrAryWnlWs[ 2 ] , keyDur:cmrAryWnlWs[ 3 ] , keySide:cmrAryWnlWs[ 4 ] , keySym:cmrAryWnlWs[ 5 ] , keyQty:cmrAryWnlWs[ 7 ] , keyRef:cmrGUISign , keyPx:cmrAryWnlWs[ 11 ] , keySlip:cmrKeySlip , keyClass:cmrClass , keyGp: keyGp };
						}
					}
					for ( var j = gKeyAryGet[ gKeyAryLen ].length; j < gKeyArySet.length; j++ ){
						gKeyAryGet[ gKeyAryLen ][ j ] = { keyKey:"" , keyName:"" , keyType:"" , keySide:gDefKeySide , keyPx:"" , keyQty:gDefKeyQty , keySlip:gDefKeySlip , keyECN:gDefKeyECN , keySym:"" , keyDur:gDefKeyDur , keyRef:gDefKeyRef , keyClass:"pnt" , keyGp: gDefKeyGp };
					}
				}
				gKeyAryLen++ ;
				if ( gSavingKey == 1 ){
					showKMNotice( 3 , cmrKey ); // show key saving notice
					gSavingKey = 0;
					if ( cmrKey == gKeyKey ){
						$( "#keyDelBtn" ).removeClass( "keyBtnEnabled" );
					}
				}
			}else if ( cmrAryWs[ 2 ] == "MACRO_DELETE" ){ //read delete hotkey message
				if ( cmrAryWs[ 3 ].slice( 0 , 3 ) != "KEY" ){
					continue;
				}
				$( "#kbKey" + cmrKey ).removeClass( "kbUsedKey" );
				$( "#kbKey" + cmrKey ).removeClass( "kbBUYKey" );
				$( "#kbKey" + cmrKey ).removeClass( "kbSELLKey" );
				$( "#kbKey" + cmrKey ).removeClass( "kbCnlOrdtKey" );
				$( "#kbKey" + cmrKey ).removeClass( "kbIfdLO" );
				$( "#kbKey" + cmrKey ).removeClass( "kbIfdSO" );
				var tempKeyArySet = new Array();
				for ( var i = 0; i < 4; i++ ){
					tempKeyArySet[ i ] = { keyKey:"" , keyName:"" , keyType:"" , keySide:gDefKeySide , keyPx:"" , keyQty:gDefKeyQty , keySlip:gDefKeySlip , keyECN:gDefKeyECN , keySym:"" , keyDur:gDefKeyDur , keyRef:"" , keyGp: "" };
				}
				$( "#kbKey" + cmrKey ).kbKeyAry = tempKeyArySet;
				for ( var j = 0; j < gKeyAryGet.length; j++ ){
					if ( gKeyAryGet[ j ][ 0 ].keyKey == cmrKey ){
						gKeyAryGet.splice( j , 1 );
						gKeyAryLen-- ;
						break;
					}
				}
				showKMNotice( 4 , cmrKey ); // show delete key notice
				if ( cmrKey == gKeyKey ){
					$( "#keyDelBtn" ).removeClass( "keyBtnEnabled" );
				}
			}else if ( cmrAryWs[ 2 ] == "DO" ){ // read DO message
				// if ( cmrHandled == 0 ){ // send one notice only even its a combined hotkey
					// showTrigNotice( cmrKey ); // show hotkey trigger notice
					// showBadgeNotice( "#jpanel_msg" ); // add badge notice.
					// showJPanelMsg( cmrMsg ); // show CMR message in message panel
					// cmrHandled = 1;
				// }
			}else if ( cmrAryWs[ 2 ] == "MSG" ){ // read error message
				// if ( cmrHandled == 0 ){
					// showBadgeNotice( "#jpanel_msg" ); // add badge notice.
					// showJPanelMsg( cmrMsg ); // show CMR message in message panel
					// cmrHandled = 1;
				// }
			}else{
				continue;
			}
			if ( cmrName == "tpLOv1" || cmrName == "tpSOv1" || cmrName == "tpCancel" || cmrName == "tpAlgo" ){
				for ( var k = 0; k < gKeyAryGet.length; k++ ){
					keyKey = gKeyAryGet[ k ][ 0 ].keyKey;
					if ( keyKey == "" || typeof ( keyKey ) == "undefined" ){
						continue;
					}
					document.getElementById( "kbKey" + keyKey ).kbKeyAry = gKeyAryGet[k];
					$( "#kbKey" + cmrKey ).removeClass( "kbUsedKey" );
					$( "#kbKey" + cmrKey ).removeClass( "kbBUYKey" );
					$( "#kbKey" + cmrKey ).removeClass( "kbSELLKey" );
					$( "#kbKey" + cmrKey ).removeClass( "kbCnlOrdtKey" );
					$( "#kbKey" + cmrKey ).removeClass( "kbIfdLO" );
					$( "#kbKey" + cmrKey ).removeClass( "kbIfdSO" );
					if ( gKeyAryGet[ k ][ 0 ].keyName == "tpCancel" ){
						if ( $( "#kbKey" + keyKey ).hasClass( "kbCnlOrdtKey" ) == false ){
							$( "#kbKey" + keyKey ).addClass( "kbCnlOrdtKey" );
							$( "#kbKey" + keyKey ).removeClass( "kbBUYKey" );
							$( "#kbKey" + keyKey ).removeClass( "kbSELLKey" );
						}
					}else if ( gKeyAryGet[ k ][ 0 ].keyName == "tpAlgo" ){
						$( "#kbKey" + keyKey ).addClass( "kbAlgoOrdtKey" );
					}else if ( gKeyAryGet[ k ][ 0 ].keySide == "BUY" ){
						if ( $( "#kbKey" + keyKey ).hasClass( "kbBUYKey" ) == false ){
							$( "#kbKey" + keyKey ).addClass( "kbBUYKey" );
							$( "#kbKey" + keyKey ).removeClass( "kbSELLKey" );
						}
					}else if ( gKeyAryGet[ k ][ 0 ].keySide == "SELL" ){
						if ( $( "#kbKey" + keyKey ).hasClass( "kbSELLKey" ) == false ){
							$( "#kbKey" + keyKey ).addClass( "kbSELLKey" );
							$( "#kbKey" + keyKey ).removeClass( "kbBUYKey" );
						}
					}
					$( "#kbKey" + keyKey ).addClass( "kbUsedKey" );
					var HasChd = 0;
					for ( var n = 1; n < gKeyAryGet[ k ].length; n++ ){
						if ( gKeyAryGet[ k ][ n ].keyKey == "" ){
							break;
						}else{
							if ( gKeyAryGet[ k ][ n ].keyClass == "chd" ){
								HasChd = 1;
								if ( gKeyAryGet[ k ][ n ].keyType == "LO2" ){
									$( "#kbKey" + keyKey ).addClass( "kbIfdLO" );
								}else if ( gKeyAryGet[ k ][ n ].keyType == "SO2" ){
									$( "#kbKey" + keyKey ).addClass( "kbIfdSO" );
								}
							}
						}
					}
					if ( HasChd == 0 ){
						$( "#kbKey" + keyKey ).removeClass( "kbIfdLO" );
						$( "#kbKey" + keyKey ).removeClass( "kbIfdSO" );
					}
				}
			}
		}
	}

	function saveHotkey(){
		if ( $( "#keySaveBttn" ).hasClass( "keyBtnEnabled" ) == true ){
			showKMNotice( 1 , gKeyKey ); // show key saving notice
			var macroCMD1;
			var macroCMD2;
			macroCMD1 = "CMD " + gInitUserID + " MACRO KEY" + gKeyKey + " NAME " + gKeyName + " ";
			//macroCMD1 = "CMD " + gInitUserID + " MACRO KEY" + gKeyKey + " ";
			if ( gKeyName == "tpLOv1" || gKeyName == "tpSOv1" ){ // save LO2 or SO2 key
				var cmdGUISide;
				var cmdGUISign;
				var d3LineCount = $( ".d3_" + gKeyName + "_" + gKeySide ).length;
				for ( var i = 0; i < d3LineCount; i++ ){
					var k = i + 1;
					if ( gKeyArySet.length < k ){ // avoid reading undefined chtOrd
						gKeyArySet[ i ] = new Array;
						gKeyArySet[ i ] = { keyKey:"" , keyName:"" , keyType:"" , keySide:gDefKeySide , keyPx:"" , keyQty:gDefKeyQty , keySlip:gDefKeySlip , keyECN:gDefKeyECN , keySym:"" , keyDur:gDefKeyDur , keyRef:"" , keyGp: gDefKeyGp };
					}
					gKeyArySet[ i ].keyPx = parseFloat( d3.select( "#" + gKeyName + "_" + gKeySide + "Line" + k ).attr( "ordPx" ) );
					gKeyArySet[ i ].keyType = d3.select( "#" + gKeyName + "_" + gKeySide + "Line" + k ).attr( "ordType" );
					gKeyArySet[ i ].keySide = d3.select( "#" + gKeyName + "_" + gKeySide + "Line" + k ).attr( "ordSide" );
				}
				for ( var i = 0; i < d3LineCount; i++ ){
					if ( gKeyArySet[ i ].keyRef == "BID" ){
						cmdGUISide = "GUI_BID";
					}else{
						cmdGUISide = "GUI_ASK";
					}
					if ( gKeyArySet[ i ].keyPx >= 0 ){
						cmdGUISign = "+";
					}else{
						cmdGUISign = "-";
					}
					var keySym = "SYM";
					if ( gKeyArySet[ i ].keySym !== "" ){
						keySym = gKeyArySet[ i ].keySym;
					}
					var keyECN = "ECN";
					if ( gKeyArySet[ i ].keyECN !== "" ){
						keyECN = gKeyArySet[ i ].keyECN;
					}
					var keyGp = gDefKeyGp;
					if ( typeof gKeyArySet[ i ].keyGp !== "undefined" && gKeyArySet[ i ].keyGp !== "" ){
						keyGp = gKeyArySet[ i ].keyGp;
					}

					var poi = "";

					if ( keyGp != gOrdGpAllId ){
						poi += gOrdPrefix + keyGp; // 4289981
					}

					var keyType = gKeyArySet[ i ].keyType;
					var keySide = gKeyArySet[ i ].keySide;

					if ( keyType == "LO2" ){ // 4289971
						poi += gOrdPrefix + gOrdTypeLoId;
					}else if ( keyType == "SO2" ){
						poi += gOrdPrefix + gOrdTypeSoId;
					}

					if ( keySide == "BUY" ){ // 4289961
						poi += gOrdPrefix + gOrdSideBuyId;
					}else if ( keySide == "SELL" ){
						poi += gOrdPrefix + gOrdSideSellId;
					}

					if ( i > 0 ){
						macroCMD1 = macroCMD1 + "|";
					}
					var ordClass = d3.select( "#" + gKeyName + "_" + gKeySide + "Line" + ( i + 1 ) ).attr( "ordClass" );
					if ( ordClass == "chd" ){ // for ifdone order
						macroCMD1 = macroCMD1 + "IF_DONE 1 " + keyECN + " LAST_TRADE_OID 2 { "
						//macroCMD1 = macroCMD1 + "IF_DONE 1 " + keyECN + " LAST_TRADE_OID 2 { C 428 "
					}
					if ( gKeyArySet[ i ].keyType == "LO2" ){ // save LO2 order
						macroCMD1 = macroCMD1 + "O " + keyECN + " " + gKeyArySet[ i ].keyType + " " + gKeyArySet[ i ].keyDur + " " + gKeyArySet[ i ].keySide + " " + keySym + " QTY " + gKeyArySet[ i ].keyQty + " AT " + cmdGUISide + " " + cmdGUISign + " " + Math.abs( gKeyArySet[ i ].keyPx );
					}else if ( gKeyArySet[ i ].keyType == "SO2" ){ // save SO2 order
						macroCMD1 = macroCMD1 + "O " + keyECN + " " + gKeyArySet[ i ].keyType + " " + gKeyArySet[ i ].keyDur + " " + gKeyArySet[ i ].keySide + " " + keySym + " QTY " + gKeyArySet[ i ].keyQty + " TRIG " + cmdGUISide + " " + cmdGUISign + " " + Math.abs( gKeyArySet[ i ].keyPx ) + " SLIP " + gKeyArySet[ i ].keySlip + " TS 1";
						//macroCMD1 = macroCMD1 + "O " + keyECN + " SO " + gKeyArySet[ i ].keyDur + " " + gKeyArySet[ i ].keySide + " " + keySym + " QTY " + gKeyArySet[ i ].keyQty + " TRIG " + cmdGUISide + " " + cmdGUISign + " " + Math.abs( gKeyArySet[ i ].keyPx ) + " SLIP " + gKeyArySet[ i ].keySlip + " TS 1 DIP 1";
					}

					macroCMD1 = macroCMD1 + " POI " + poi;

					if ( ordClass == "chd" ){
						macroCMD1 = macroCMD1 + " }";
					}
				}
				gSletedLineID = gKeyName + "_" + gKeySide + "Line1";
				sletChtLine( gSletedLineID );
			}else if ( gKeyName == "tpCancel" ){ // save cancel order key
				var cmdECN = gDefCnlECN;// use "HKE" order to prevent error ( default is "ECN" )

				var poiLo = gOrdPrefix + "6";
				var poiSo = gOrdPrefix + "7";
				var poiBuy = gOrdPrefix + "8";
				var poiSell = gOrdPrefix + "9";

				var poi = "";
				var poiGp = "";

				var ckType = $( "#cnlOrdSec .cnlOrdOrdType" );
				var ckedType = new Array;
				var ckedCount = 0; // count how many cancel button is checked
				for ( var i=0; i< ckType.length; i++ ){
					if ( ckType[ i ].checked === true ){
						ckedType[ ckedCount ] = ckType[ i ];
						ckedCount++ ;
					}
				}

				var gp = 0;
				$( "#cnlOrdSec .cnlOrdGp" ).each( function (){
					if ( !$( this ).prop( "checked" ) ){
						return;
					}

					poiGp = gOrdPrefix + $( this ).val();

					if ( gp > 0 ){
						macroCMD1 = macroCMD1 + "|";
					}
					gp++ ;

					for ( var i = 0 ; i< ckedType.length; i++ ){
						if ( i > 0 ){
							macroCMD1 = macroCMD1 + "|";
						}
						if ( ckedType[ i ].value == "ALLBUYLO2" ){
							poi = poiGp + poiLo + poiBuy;
						}else if ( ckedType[ i ].value == "ALLSELLLO2" ){
							poi = poiGp + poiLo + poiSell;
						}else if ( ckedType[ i ].value == "ALLBUYSO2" ){
							poi = poiGp + poiSo + poiBuy;
						}else if ( ckedType[ i ].value == "ALLSELLSO2" ){
							poi = poiGp + poiSo + poiSell;
						}
						macroCMD1 = macroCMD1 + "O " + cmdECN + " CO ALL CANCEL 0 POI " + poi;
					}
				});
			}else if ( gKeyName == "tpAlgo" ){
				var algoKey = $( ".algoOrdChked" ).val();
				macroCMD1 += "MSG " + gInitUserID + " a_hotkey { " + algoKey + " }";
			}
			macroCMD2 = "CMD " + gInitUserID + " MACRO_IN KEY" + gKeyKey + " ECN SYM GUI_BID GUI_ASK";
			var macroCMD = { macroCMD1: macroCMD1 , macroCMD2: macroCMD2 };
			setMacro( macroCMD );
			gSavingKey = 1; //send key save notice if saved a new key
		}
	}

	function delHotkey(){
		if ( $( "#keyDelBtn" ).hasClass( "keyBtnEnabled" ) == true ){
			if ( $( "#kbKey" + gKeyKey ).hasClass( "kbUsedKey" ) ){
				var cmdDelete = "CMD " + gInitUserID + " MACRO_DELETE KEY" + gKeyKey;
				showLog( "delHotkey" , cmdDelete );
				pushCMD( cmdDelete ); // delete hotkey
				showKMNotice( 2 , gKeyKey ); // show delete key notice
				if ( gKeyName == "tpLOv1" || gKeyName == "tpSOv1" ){
					gSletedLineID = gKeyName + "_" + gKeySide + "Line1";
					sletChtLine( gSletedLineID );
				}
			}
		}
	}

	function delAllHotkey(){ // delete all hotkey
		var cmdDeleteAll = "CMD " + gInitUserID + " MACRO_DELETE_ALL";
		pushCMD( cmdDeleteAll );
		showKMNotice( 5 , gKeyKey );
		gKeyAryGet = [];
		gKeyAryLen = 0;
		$( ".kbUablKey" ).each( function(){
			$( this ).removeClass( "kbUsedKey" );
			$( this ).removeClass( "kbBUYKey" );
			$( this ).removeClass( "kbSELLKey" );
			$( this ).removeClass( "kbCnlOrdtKey" );
			$( this ).removeClass( "kbIfdLO" );
			$( this ).removeClass( "kbIfdSO" );
		});
		if ( gKeyName == "tpLOv1" || gKeyName == "tpSOv1" ){
			gSletedLineID = gKeyName + "_" + gKeySide + "Line1";
			sletChtLine( gSletedLineID );
		}
		setTimeout( function(){
			getMacro(); // get macro again to confirm there is no hotkey on panel
		} , 500 );
	}

	function resetJKM(){
		drawAllCht();
		$( "#keyBtnIfD_LO2" ).prop( "checked" , false );
		$( "#keyBtnIfD_SO2" ).prop( "checked" , false );
		if ( gKeySleted == 0 ){
			sletKeyType( $( ".pxChtDiv" ).eq( 0 ).attr( "id" ) );
		}else{
			sletKeyType( $( ".pxChtDiv" ).eq( 0 ).attr( "id" ) );
			$( ".keySideSec" ).show();
		}
		setChtLine( $( ".pxChtDiv" ).eq( 0 ).attr( "id" ) , 'BUY' , gDefLOBUYPx , gDefKeyQty , 1 ); // reset chart px
		setChtLine( $( ".pxChtDiv" ).eq( 0 ).attr( "id" ) , 'SELL' , gDefLOSELLPx , gDefKeyQty , 0 );
		setChtLine( $( ".pxChtDiv" ).eq( 1 ).attr( "id" ) , 'BUY' , gDefSOBUYPx , gDefKeyQty , 0 );
		setChtLine( $( ".pxChtDiv" ).eq( 1 ).attr( "id" ) , 'SELL' , gDefSOSELLPx , gDefKeyQty , 0 );
		sletChtLine( gKeyName + "_" + gKeySide + "Line1" ); //select the first line to enable dragging line
		$( "#keyDtlQttySldrDiv" ).slider( "value" , gDefKeyQty ); // reset key spec panel
		$( "#keyDtlQttyHadle" ).text( gDefKeyQty );
		$( "#keyDtlSlipSldrDiv" ).slider( "value" , gDefKeySlip );
		$( "#keyDtlSlipHadle" ).text( gDefKeySlip );

		$( "#keyDtlECNIpt option" ).eq( 0 ).prop( "selected" , true );
		$( "#keyDtlSymIpt option" ).eq( 0 ).prop( "selected" , true );
		$( "#keyDtlDurIpt option" ).eq( 0 ).prop( "selected" , true );
		$( "#keyDtlGpIpt option" ).eq( 0 ).prop( "selected" , true );

		$( ".cnlOrdIpt" ).each( function(){
			$( this ).prop( "checked" , true );
		});

		sletCnlOrdGp( gOrdGpAllId );

		var tempKeyArySet = new Array(); // don't edit gKeyArySet directly
		for ( var i = 0; i < 4; i++ ){
			tempKeyArySet[ i ] = { keyKey:"" , keyName:"" , keyType:"" , keySide:gDefKeySide , keyPx:"" , keyQty:gDefKeyQty , keySlip:gDefKeySlip , keyECN:gDefKeyECN , keySym:"" , keyDur:gDefKeyDur , keyRef:gDefKeyRef , keyGp: gDefKeyGp };
		}
		gKeyArySet = tempKeyArySet; // reset gKeyArySet
		//$( "#keySide" + gKeySide ).click();
		sletKeySide( gKeySide , 1 );

		gSletedECN = "";
		gSletedSym = "";
		linkKeyECN( 1 );
		linkKeySym( 1 );
	}

	function sletKeyKey(){ // select a key on keyboard
		$( "#kbKeyDiv" ).fadeIn();
		$( "#kbKeyVal" ).show();
		var $this = $( this );
		var sletedKey = $this.html();
		if ( $( this ).hasClass( "kbUablKey" ) ){ //check if it is usable key
			$( "#keySaveBttn" ).addClass( "keyBtnEnabled" );
			if ( $( this ).hasClass( "kbUsedKey" ) ){ //check if it is used key
				$( "#keyDelBtn" ).addClass( "keyBtnEnabled" );
				gSetNewKey = 0;
				gKeyArySet = this.kbKeyAry;
				if ( this.kbKeyAry[ 0 ].keyName == "tpCancel" ){
					sletKeyType( this.kbKeyAry[ 0 ].keyName );
					sletCnlOrdGp( "" );
					gPressedCtrlBtn = 1;
					$( "#cnlAllIpt" ).prop( "checked" , false );
					sletCnlOrdKey( "ALL" );
					for ( var i = 0; i < this.kbKeyAry.length; i++ ){
						var ckBx =	$( ".cnlOrdIpt" );
						for ( var j=0; j< ckBx.length; j++ ){
							if ( ckBx[ j ].value == this.kbKeyAry[ i ].keyCnlType ){
								ckBx[ j ].checked = true;
								sletCnlOrdKey( ckBx[ j ].value );
								break;
							}
						}
						var keyGp = this.kbKeyAry[ i ].keyGp;
						sletCnlOrdGp( keyGp );
					}
					gPressedCtrlBtn = 0;
				}else if ( this.kbKeyAry[ 0 ].keyName == "tpAlgo" ){
					sletKeyType( this.kbKeyAry[ 0 ].keyName );
					var keyAlgoOrder = this.kbKeyAry[ 0 ].keyAlgoOrder;
					$( ".algoOrdIpt" ).each( function(){
						if ( $( this ).val() == keyAlgoOrder ){
							$( this ).prop( "checked" , true );
						}else{
							$( this ).prop( "checked" , false );
						}
					} );
				}else{
					drawAllCht();
					$( "#keyBtnIfD_LO2" ).prop( "checked" , false );
					$( "#keyBtnIfD_SO2" ).prop( "checked" , false );
					sletKeyType( this.kbKeyAry[ 0 ].keyName );
					sletKeySide( this.kbKeyAry[ 0 ].keySide , 0 );
					for ( var i = 0; i < this.kbKeyAry.length; i++ ){
						if ( this.kbKeyAry[ i ].keyKey != "" ){
							var k = i + 1; // d3 chart line number
							if ( this.kbKeyAry[ i ].keyClass == "pnt" ){
								setChtLine( this.kbKeyAry[ 0 ].keyName , this.kbKeyAry[ 0 ].keySide , this.kbKeyAry[ i ].keyPx , k , 1 );
							}else if ( this.kbKeyAry[ i ].keyClass == "chd" ){
								var keyType = this.kbKeyAry[ i ].keyType;
								if ( $( "#keyBtnIfD_" + keyType ).prop( "checked" ) == false ){ //add ifdone line
									addChtLine( keyType );
									$( "#keyBtnIfD_" + keyType ).prop( "checked" , true );
								}
								var lineID = gSletedChtID + "Line" + k;
								setChtChdLine( lineID , this.kbKeyAry[ i ].keyPx );
							}
						}
					}
				}
			}else{
				$( "#keyDelBtn" ).removeClass( "keyBtnEnabled" );
				resetJKM();
				gSetNewKey = 1;
			}
			gInstruct = 2;
			showInstruct( 2 );
			$( "#keyKbUI li" ).removeClass( "kbSletedKey" );
			this.className += " kbSletedKey";
			$( "#kbKeyVal" ).fadeOut( 200 , function(){
				if ( sletedKey == "SPACE" ){
					$( this ).val( "SP." );
				}else{
					$( this ).val( sletedKey );
				}
				$( this ).fadeIn( 200 );
			});
			gKeyKey = sletedKey;
			if ( gKeySleted == 0 ){
				$( "#keyNameTab" ).fadeIn();
				$( "#keyNameNav" ).fadeIn();
				$( ".keySideSec" ).fadeIn();
				if ( $( this ).hasClass( "kbUsedKey" ) && gKeyNameSleted == 1 ){
					// $( "#jPanelKM" ).animate( { width:"972px" } , 200 );
					// $( "#keyNameTab" ).animate( { width:"284px" } , 200 );
					$( "#keyTypeSec" ).delay( 200 ).fadeIn();
					gKeyNameSleted = 1;
				}else{
					// $( "#jPanelKM" ).animate( { width:"629px" } , 200 );
					// $( "#keyNameTab" ).animate( { width:"114px" } , 200 );
				}
				gKeySleted = 1;
			}else if ( $( this ).hasClass( "kbUsedKey" ) && gKeyNameSleted == 0 ){
				// $( "#jPanelKM" ).animate( { width:"972px" } , 400 );
				// $( "#keyNameTab" ).animate( { width:"284px" } , 200 );
				$( "#keyTypeSec" ).delay( 200 ).fadeIn();
				gKeyNameSleted = 1;
			}
		}
	}

	function sletKeyType( keyName ){
		gKeyNameSleted = 1;
		gKeyName = keyName;
		if ( keyName == "tpLOv1" ){
			var keyType = 0;
		}else if ( keyName == "tpSOv1" ){
			var keyType = 1;
		}else if ( keyName == "tpCancel" ){
			var keyType = 2;
		}else if ( keyName == "tpAlgo" ){
			var keyType = 3;
		}
		$( ".keyTypeLbl" ).removeClass( "keyTypeSleted" );
		$( ".keyTypeLbl" ).eq( keyType ).addClass( "keyTypeSleted" );
		$( "#keyTypeBg" ).animate( { "top" : ( 12 + ( keyType * 51 ) ) + "px" });
		if ( keyType <= 1 ){
			drawAllCht();
			$( "#keyBtnIfD_LO2" ).prop( "checked" , false );
			$( "#keyBtnIfD_SO2" ).prop( "checked" , false );
			gKeyName = keyName;
			gSletedLineNum = 1;
			gSletedLineID = gKeyName + "_" + gKeySide + "Line1";
			var tabType = "#" + keyName;
			$( ".pxChtDiv" ).each( function(){
				$( this ).hide();
			});
			$( "#" + keyName ).fadeIn();
			sletKeySide( gKeySide , 0 );
			sletChtLine( gSletedLineID );

			$( "#keyCnlOrdSec" ).hide();
			$( "#keyAlgoOrdSec" ).hide();

			$( "#keyOrdSec" ).fadeIn();
			$( ".keySideSec" ).fadeIn();
			//$( "#keySaveBttn" ).addClass( "keyBtnEnabled" );

		}else if ( keyType == 2 ){
			if ( $( "#cnlLOBUYIpt" ).prop( "checked" ) || $( "#cnlLOSELLIpt" ).prop( "checked" ) || $( "#cnlSOBUYIpt" ).prop( "checked" ) || $( "#cnlSOSELLIpt" ).prop( "checked" ) ){
				$( "#keySaveBttn" ).addClass( "keyBtnEnabled" );
			}else{
				$( "#keySaveBttn" ).removeClass( "keyBtnEnabled" );
			}
			$( ".keySideSec" ).fadeOut();
			$( "#keyOrdSec" ).hide();
			$( "#keyAlgoOrdSec" ).hide();

			$( "#keyCnlOrdSec" ).fadeIn();
			$( "#cnlLOBUYIpt" ).prop( "checked" )

		}else if ( keyType == 3 ){
			$( "#keyCnlOrdSec" ).hide();
			$( ".keySideSec" ).fadeOut();
			$( "#keyOrdSec" ).hide();

			$( "#keyAlgoOrdSec" ).show();
		}
	}

	function sletKeySide( keySide , chgKeyRef ){ // change hotkey BUY or SELL
		drawAllCht();
		$( "#keyBtnIfD_LO2" ).prop( "checked" , false );
		$( "#keyBtnIfD_SO2" ).prop( "checked" , false );
		if ( keySide == "BUY" ){
			var keySideOpp = "SELL";
		}else if ( keySide == "SELL" ){
			var keySideOpp = "BUY";
		}
		$( "#keySide" + keySide ).prop( "checked" , true );
		$( "#" + gKeyName + "_" + keySideOpp ).hide();
		$( "#" + gKeyName + "_" + keySide ).fadeIn();
		$( ".keyDtlSideTxt" ).text( keySide );
		if ( chgKeyRef == 1 ){
			if ( keySide == "BUY" ){
				$( "#keyDtlRefIpt" ).val( "BID" );
				gKeyArySet[ gSletedLineNum - 1 ].keyRef = "BID";
			}else{
				$( "#keyDtlRefIpt" ).val( "ASK" );
				gKeyArySet[ gSletedLineNum - 1 ].keyRef = "ASK";
			}
		}
		gKeySide = keySide;
		gSletedLineID = gKeyName + "_" + gKeySide + "Line1";
		sletChtLine( gSletedLineID );
	}

	function updateOrdDtlSec(){ // show the key details ( quantity , slippage , ECN , Symbol... )
		var ordType = d3.select( "#" + gSletedLineID ).attr( "ordType" );
		var ordSide = d3.select( "#" + gSletedLineID ).attr( "ordSide" );
		var ordClass = d3.select( "#" + gSletedLineID ).attr( "ordClass" );
		var lineNum = d3.select( "#" + gSletedLineID ).attr( "lineNum" );
		if ( gKeySpecTabOn == 0 ){
			gSletedLineNum = lineNum;
			// $( "#keyTypeSec" ).animate( { width:"456px" } , 200 );
			$( "#keyDtlSec" ).delay( 200 ).fadeIn();
			gKeySpecTabOn = 1;
		}
		var keyQty = gKeyArySet[ lineNum - 1 ].keyQty
		var keyECN = gKeyArySet[ lineNum - 1 ].keyECN;
		var keySym = gKeyArySet[ lineNum - 1 ].keySym;
		var keyDur = gKeyArySet[ lineNum - 1 ].keyDur;
		var keyRef = gKeyArySet[ lineNum - 1 ].keyRef;
		var keySlip = gKeyArySet[ lineNum - 1 ].keySlip;
		var keyGp = gKeyArySet[ lineNum - 1 ].keyGp;

		if ( keyECN == "ECN" ){
			linkKeyECN( 1 );
		}else{
			linkKeyECN( 2 );
			$( "#keyDtlECNIpt" ).val( keyECN );
			gSletedECN = keyECN;
		}
		if ( keySym == "SYM" ){
			linkKeySym( 1 );
		}else{
			linkKeySym( 2 );
			$( "#keyDtlSymIpt" ).val( keySym );
			gSletedSym = keySym;
		}
		$( "#keyDtlQttySldrDiv" ).slider( "value" , keyQty )
		$( "#keyDtlQttyHadle" ).text( keyQty );
		$( "#keyDtlDurIpt" ).val( keyDur );
		$( "#keyDtlRefIpt" ).val( keyRef );
		$( "#keyDtlGpIpt" ).val( keyGp );
		if ( ordType == "SO2" ){
			$( "#keyDtlSlipDiv" ).show();
			$( "#keyDtlSlipSldrDiv" ).slider( "value" , keySlip )
			$( "#keyDtlSlipHadle" ).text( keySlip );
		}else if ( ordType == "LO2" ){
			$( "#keyDtlSlipDiv" ).hide();
		}
	}

	function sletKeyECN( selected ){ // change order ECN
		gKeyArySet[ gSletedLineNum - 1 ].keyECN = selected.value;
		gSletedECN = selected.value;
	}

	function sletKeySym( selected ){ // change order symbol
		gKeyArySet[ gSletedLineNum - 1 ].keySym = selected.value;
		gSletedSym = selected.value;
	}

	function sletKeyDur( selected ){ // change order duration ( "DAY" , "IOC"... )
		gKeyArySet[ gSletedLineNum - 1 ].keyDur = selected.value;
	}

	function sletKeyRef( selected ){ // change order reference price ( by Bid/Ask )
		gKeyArySet[ gSletedLineNum - 1 ].keyRef = selected.value;
	}

	function sletKeyGp( selected ){ // change order group
		gKeyArySet[ gSletedLineNum - 1 ].keyGp = selected.value;
	}

	function iptKeyCtrlVal( inputValue , type ){
		if ( type == 1 ){
			var ctrlType = "Qtty";
		}else if ( type == 2 ){
			var ctrlType = "Slip";
		}
		if ( $.isNumeric( inputValue ) == true ){
			if ( ( type == 1 && inputValue > 0 ) || ( type == 2 && inputValue >= 0 ) ){
				$( "#keyDtl" + ctrlType + "SldrDiv" ).slider( "value" , inputValue );
				$( "#keyDtl" + ctrlType + "Hadle" ).text( inputValue );
				if ( type == 1 ){
					gKeyArySet[ gSletedLineNum - 1 ].keyQty = inputValue;
				}else if ( type == 2 ){
					gKeyArySet[ gSletedLineNum - 1 ].keySlip = inputValue;
				}
			}
		}
		$( "#keyDtl" + ctrlType + "IptDiv" ).hide();
		$( "#keyDtl" + ctrlType + "SldrDiv" ).fadeIn();
	}

	function showKeyIptDiv( type ){
		if ( type == 1 ){
			var ctrlType = "Qtty";
		}else if ( type == 2 ){
			var ctrlType = "Slip";
		}
		$( "#keyDtl" + ctrlType + "SldrDiv" ).hide();
		$( "#keyDtl" + ctrlType + "Ipt" ).val( $( "#keyDtl" + ctrlType + "Hadle" ).text() );
		$( "#keyDtl" + ctrlType + "IptDiv" ).fadeIn();
	}

	function linkKeyECN( linkECN ){
		if ( linkECN == 1 || ( linkECN == 3 && $( "#keyDtlLnkECNIpt" ).prop( "checked" ) == true ) ){
			gLnkedKeyECN = 1;
			$( "#keyDtlLnkECNIpt" ).prop( "checked" , true );
			$( "#keyDtlECNIpt" ).attr( "disabled" , "disabled" );
			$( "#keyDtlECNIpt" ).addClass( "keyDtlDsabdCtrl" );
			$( "#keyDtlECNIpt option" ).eq( 0 ).prop( "selected" , true );
			gKeyArySet[ gSletedLineNum - 1 ].keyECN = "ECN";
		}else if ( linkECN == 2 || ( linkECN == 3 && $( "#keyDtlLnkECNIpt" ).prop( "checked" ) == false ) ){
			gLnkedKeyECN = 0;
			$( "#keyDtlLnkECNIpt" ).prop( "checked" , false );
			$( "#keyDtlECNIpt" ).val( gSletedECN );
			$( "#keyDtlECNIpt" ).removeAttr( "disabled" );
			$( "#keyDtlECNIpt" ).removeClass( "keyDtlDsabdCtrl" );
		}
	}

	function linkKeySym( linkSym ){
		if ( linkSym == 1 || ( linkSym == 3 && $( "#keyDtlLnkSymIpt" ).prop( "checked" ) == true ) ){
			$( "#keyDtlLnkSymIpt" ).prop( "checked" , true );
			$( "#keyDtlSymIpt" ).prop( "disabled" , true );
			$( "#keyDtlSymIpt" ).addClass( "keyDtlDsabdCtrl" );
			$( "#keyDtlSymIpt" ).val( "default" );
			gKeyArySet[ gSletedLineNum - 1 ].keySym = "SYM";
		}else if ( linkSym == 2 || ( linkSym == 3 && $( "#keyDtlLnkSymIpt" ).prop( "checked" ) == false ) ){
			$( "#keyDtlLnkSymIpt" ).prop( "checked" , false );
			$( "#keyDtlSymIpt" ).val( gSletedSym );
			$( "#keyDtlSymIpt" ).prop( "disabled" , false );
			$( "#keyDtlSymIpt" ).removeClass( "keyDtlDsabdCtrl" );
		}
	}

	function sletCnlOrdKey( keyType ){
		var ckboxCk;
		var ckAllboxCk = $( "#cnlAllIpt" ).prop( "checked" );
		if ( gPressedCtrlBtn == 1 ){ // check if Ctrl or Shift key is pressed
			if ( keyType == "ALL" ){
				$( "#cnlBUYIpt" ).prop( "checked" , ckAllboxCk );
				$( "#cnlSELLIpt" ).prop( "checked" , ckAllboxCk );
				$( "#cnlLOIpt" ).prop( "checked" , ckAllboxCk );
				$( "#cnlSOIpt" ).prop( "checked" , ckAllboxCk );
				$( "#cnlLOBUYIpt" ).prop( "checked" , ckAllboxCk );
				$( "#cnlSOBUYIpt" ).prop( "checked" , ckAllboxCk );
				$( "#cnlLOSELLIpt" ).prop( "checked" , ckAllboxCk );
				$( "#cnlSOSELLIpt" ).prop( "checked" , ckAllboxCk );
			}else if ( keyType == "ALLBUY" ){
				ckboxCk = $( "#cnlBUYIpt" ).prop( "checked" );
				$( "#cnlLOBUYIpt" ).prop( "checked" , ckboxCk );
				$( "#cnlSOBUYIpt" ).prop( "checked" , ckboxCk );
				if ( ckboxCk == false ){
					$( "#cnlAllIpt" ).prop( "checked" , ckboxCk );
					$( "#cnlLOIpt" ).prop( "checked" , ckboxCk );
					$( "#cnlSOIpt" ).prop( "checked" , ckboxCk );
				}
			}else if ( keyType == "ALLSELL" ){
				ckboxCk = $( "#cnlSELLIpt" ).prop( "checked" );
				$( "#cnlLOSELLIpt" ).prop( "checked" , ckboxCk );
				$( "#cnlSOSELLIpt" ).prop( "checked" , ckboxCk );
				if ( ckboxCk == false ){
					$( "#cnlAllIpt" ).prop( "checked" , ckboxCk );
					$( "#cnlLOIpt" ).prop( "checked" , ckboxCk );
					$( "#cnlSOIpt" ).prop( "checked" , ckboxCk );
				}
			}else if ( keyType == "ALLLO2" ){
				ckboxCk = $( "#cnlLOIpt" ).prop( "checked" );
				$( "#cnlLOBUYIpt" ).prop( "checked" , ckboxCk );
				$( "#cnlLOSELLIpt" ).prop( "checked" , ckboxCk );
				if ( ckboxCk == false ){
					$( "#cnlAllIpt" ).prop( "checked" , ckboxCk );
					$( "#cnlBUYIpt" ).prop( "checked" , ckboxCk );
					$( "#cnlSELLIpt" ).prop( "checked" , ckboxCk );
				}
			}else if ( keyType == "ALLSO2" ){
				ckboxCk = $( "#cnlSOIpt" ).prop( "checked" );
				$( "#cnlSOBUYIpt" ).prop( "checked" , ckboxCk );
				$( "#cnlSOSELLIpt" ).prop( "checked" , ckboxCk );
				if ( ckboxCk == false ){
					$( "#cnlAllIpt" ).prop( "checked" , ckboxCk );
					$( "#cnlBUYIpt" ).prop( "checked" , ckboxCk );
					$( "#cnlSELLIpt" ).prop( "checked" , ckboxCk );
				}
			}else if ( keyType == "ALLBUYLO2" && $( "#cnlLOBUYIpt" ).prop( "checked" ) == false ){
				$( "#cnlAllIpt" ).prop( "checked" , false );
				$( "#cnlBUYIpt" ).prop( "checked" , false );
				$( "#cnlLOIpt" ).prop( "checked" , false );
			}else if ( keyType == "ALLSELLLO2" && $( "#cnlLOSELLIpt" ).prop( "checked" ) == false ){
				$( "#cnlAllIpt" ).prop( "checked" , false );
				$( "#cnlSELLIpt" ).prop( "checked" , false );
				$( "#cnlLOIpt" ).prop( "checked" , false );
			}else if ( keyType == "ALLBUYSO2" && $( "#cnlSOBUYIpt" ).prop( "checked" ) == false ){
				$( "#cnlAllIpt" ).prop( "checked" , false );
				$( "#cnlBUYIpt" ).prop( "checked" , false );
				$( "#cnlSOIpt" ).prop( "checked" , false );
			}else if ( keyType == "ALLSELLSO2" && $( "#cnlSOSELLIpt" ).prop( "checked" ) == false ){
				$( "#cnlAllIpt" ).prop( "checked" , false );
				$( "#cnlSELLIpt" ).prop( "checked" , false );
				$( "#cnlSOIpt" ).prop( "checked" , false );
			}
			if ( $( "#cnlLOBUYIpt" ).prop( "checked" ) && $( "#cnlLOSELLIpt" ).prop( "checked" ) ){
				$( "#cnlLOIpt" ).prop( "checked" , true );
			}
			if ( $( "#cnlSOBUYIpt" ).prop( "checked" ) && $( "#cnlSOSELLIpt" ).prop( "checked" ) ){
				$( "#cnlSOIpt" ).prop( "checked" , true );
			}
			if ( $( "#cnlLOBUYIpt" ).prop( "checked" ) && $( "#cnlSOBUYIpt" ).prop( "checked" ) ){
				$( "#cnlBUYIpt" ).prop( "checked" , true );
			}
			if ( $( "#cnlLOSELLIpt" ).prop( "checked" ) && $( "#cnlSOSELLIpt" ).prop( "checked" ) ){
				$( "#cnlSELLIpt" ).prop( "checked" , true );
			}
			if ( $( "#cnlLOBUYIpt" ).prop( "checked" ) && $( "#cnlLOSELLIpt" ).prop( "checked" ) && $( "#cnlSOBUYIpt" ).prop( "checked" ) && $( "#cnlSOSELLIpt" ).prop( "checked" ) ){
				$( "#cnlAllIpt" ).prop( "checked" , true );
			}
		}else if ( gPressedCtrlBtn == 0 ){
			$( ".cnlOrdIpt" ).each( function(){
				$( this ).prop( "checked" , false );
			});
			if ( keyType == "ALL" ){
				$( "#cnlBUYIpt" ).prop( "checked" , true );
				$( "#cnlSELLIpt" ).prop( "checked" , true );
				$( "#cnlLOIpt" ).prop( "checked" , true );
				$( "#cnlSOIpt" ).prop( "checked" , true );
				$( "#cnlLOBUYIpt" ).prop( "checked" , true );
				$( "#cnlSOBUYIpt" ).prop( "checked" , true );
				$( "#cnlLOSELLIpt" ).prop( "checked" , true );
				$( "#cnlSOSELLIpt" ).prop( "checked" , true );
			}else if ( keyType == "ALLBUY" ){
				$( "#cnlLOBUYIpt" ).prop( "checked" , true );
				$( "#cnlSOBUYIpt" ).prop( "checked" , true );
			}else if ( keyType == "ALLSELL" ){
				$( "#cnlLOSELLIpt" ).prop( "checked" , true );
				$( "#cnlSOSELLIpt" ).prop( "checked" , true );
			}else if ( keyType == "ALLLO2" ){
				$( "#cnlLOBUYIpt" ).prop( "checked" , true );
				$( "#cnlLOSELLIpt" ).prop( "checked" , true );
			}else if ( keyType == "ALLSO2" ){
				$( "#cnlSOBUYIpt" ).prop( "checked" , true );
				$( "#cnlSOSELLIpt" ).prop( "checked" , true );
			}else if ( keyType == "ALLBUYLO2" ){
				$( "#cnlLOBUYIpt" ).prop( "checked" , true );
			}else if ( keyType == "ALLSELLLO2" ){
				$( "#cnlLOSELLIpt" ).prop( "checked" , true );
			}else if ( keyType == "ALLBUYSO2" ){
				$( "#cnlSOBUYIpt" ).prop( "checked" , true );
			}else if ( keyType == "ALLSELLSO2" ){
				$( "#cnlSOSELLIpt" ).prop( "checked" , true );
			}
			if ( $( "#cnlLOBUYIpt" ).prop( "checked" ) && $( "#cnlLOSELLIpt" ).prop( "checked" ) ){
				$( "#cnlLOIpt" ).prop( "checked" , true );
			}
			if ( $( "#cnlSOBUYIpt" ).prop( "checked" ) && $( "#cnlSOSELLIpt" ).prop( "checked" ) ){
				$( "#cnlSOIpt" ).prop( "checked" , true );
			}
			if ( $( "#cnlLOBUYIpt" ).prop( "checked" ) && $( "#cnlSOBUYIpt" ).prop( "checked" ) ){
				$( "#cnlBUYIpt" ).prop( "checked" , true );
			}
			if ( $( "#cnlLOSELLIpt" ).prop( "checked" ) && $( "#cnlSOSELLIpt" ).prop( "checked" ) ){
				$( "#cnlSELLIpt" ).prop( "checked" , true );
			}
			if ( $( "#cnlLOBUYIpt" ).prop( "checked" ) && $( "#cnlLOSELLIpt" ).prop( "checked" ) && $( "#cnlSOBUYIpt" ).prop( "checked" ) && $( "#cnlSOSELLIpt" ).prop( "checked" ) ){
				$( "#cnlAllIpt" ).prop( "checked" , true );
			}
		}
		if ( $( "#cnlLOBUYIpt" ).prop( "checked" ) || $( "#cnlLOSELLIpt" ).prop( "checked" ) || $( "#cnlSOBUYIpt" ).prop( "checked" ) || $( "#cnlSOSELLIpt" ).prop( "checked" ) ){
			$( "#keySaveBttn" ).addClass( "keyBtnEnabled" );
			gInstruct = 5;
			showInstruct( 5 );
		}else{
			$( "#keySaveBttn" ).removeClass( "keyBtnEnabled" );
		}
	}

	function sletCnlOrdGp( gp ){
		if ( gPressedCtrlBtn == 0 ){
			$( ".cnlOrdGpKey" ).prop( "checked" , false );
			$( "#cnlGpAIpt" ).prop( "checked" , false );
			$( "#cnlGpBIpt" ).prop( "checked" , false );
			$( "#cnlGpCIpt" ).prop( "checked" , false );
		}
		if ( gp == gOrdGpAllId ){
			$( ".cnlOrdGpKey" ).prop( "checked" , true );
			$( ".cnlGpAIpt" ).prop( "checked" , true );
			$( ".cnlGpBIpt" ).prop( "checked" , true );
			$( ".cnlGpCIpt" ).prop( "checked" , true );
		}else{
			if ( gp == gOrdGpAId ){
				$( "#cnlGpAIpt" ).prop( "checked" , true );
			}else if ( gp == gOrdGpBId ){
				$( "#cnlGpBIpt" ).prop( "checked" , true );
			}else if ( gp == gOrdGpCId ){
				$( "#cnlGpCIpt" ).prop( "checked" , true );
			}
		}
	}

	function showInstruct( step ){
		stopInstruct();
		if ( gSetNewKey == 1 || step ==1 ){
			if ( step <= gInstruct ){
				if ( step == 1 ){
					$( "#keyKbSec" ).addClass( "secShdwSleted" );
					$( "#keyTypeSec" ).addClass( "secShdwNone" );
					$( ".keySideSec" ).addClass( "secShdwNone" );
					$( "#pxChtSec" ).addClass( "secShdwNone" );
					$( "#keyDtlSec" ).addClass( "secShdwNone" );
					$( "#keySaveBttn" ).addClass( "secShdwNone" );
					gInstruct = 2;
				}else if ( step == 2 ){
					$( "#keyTypeSec" ).addClass( "secShdwSleted" );
					$( ".keySideSec" ).addClass( "secShdwNone" );
					$( "#pxChtSec" ).addClass( "secShdwNone" );
					$( "#keyDtlSec" ).addClass( "secShdwNone" );
					$( "#keySaveBttn" ).addClass( "secShdwNone" );
					gInstruct = 3;
				}else if ( step == 3 ){
					$( ".keySideSec" ).addClass( "secShdwSleted" );
					$( "#cnlOrdSec" ).addClass( "secShdwSleted" );
					$( "#pxChtSec" ).addClass( "secShdwNone" );
					$( "#keyDtlSec" ).addClass( "secShdwNone" );
					$( "#keySaveBttn" ).addClass( "secShdwNone" );
					gInstruct = 4;
				}else if ( step == 4 ){
					$( "#pxChtSec" ).addClass( "secShdwSleted" );
					$( "#keyDtlSec" ).addClass( "secShdwNone" );
					$( "#keySaveBttn" ).addClass( "secShdwNone" );
					gInstruct = 5;
				}else if ( step == 5 ){
					$( "#keySaveBttn" ).addClass( "secShdwSleted" );
				}
			}else{
				warnInstruct( gInstruct - 1 );
				gInstruct = 1;
			}
		}
	}

	function warnInstruct( step ){
		if ( step == 1 ){
			var secShdwWarn = "#keyKbSec";
		}else if ( step == 2 ){
			var secShdwWarn = "#keyTypeSec";
		}else if ( step == 3 ){
			var secShdwWarn = ".keySideSec";
		}else if ( step == 4 ){
			var secShdwWarn = "#pxChtSec";
		}else if ( step == 5 ){
			var secShdwWarn = "#keyDtlSec";
		}
		$( secShdwWarn ).addClass( "secShdwWarn" );
		setTimeout( function(){
			gInstruct = 5;
			showInstruct( 5 );
		} , 600 );
		setTimeout( function(){
			$( secShdwWarn ).removeClass( "secShdwWarn" );
		} , 1400 );
	}

	function stopInstruct(){
		$( "#keyKbSec" ).removeClass( "secShdwSleted" );
		$( "#keyTypeSec" ).removeClass( "secShdwSleted" );
		$( ".keySideSec" ).removeClass( "secShdwSleted" );
		$( "#cnlOrdSec" ).removeClass( "secShdwSleted" );
		$( "#pxChtSec" ).removeClass( "secShdwSleted" );
		$( "#keyDtlSec" ).removeClass( "secShdwSleted" );
		$( "#keySaveBttn" ).removeClass( "secShdwSleted" );
		$( "#keyKbSec" ).removeClass( "secShdwNone" );
		$( "#keyTypeSec" ).removeClass( "secShdwNone" );
		$( ".keySideSec" ).removeClass( "secShdwNone" );
		$( "#pxChtSec" ).removeClass( "secShdwNone" );
		$( "#keyDtlSec" ).removeClass( "secShdwNone" );
		$( "#keySaveBttn" ).removeClass( "secShdwNone" );
	}

	function ctrlKeyDown( e ){
		if ( e.keyCode == 17 || e.keyCode == 16 ){
			gPressedCtrlBtn = 1;
			if ( gKeyName == "tpCancel" ){
				$( ".cnlOrdBtn" ).addClass( "cnlOrdMltSlet" );
			}
			var $keyDelBtn = $( "#keyDelBtn" );
			if ( $keyDelBtn.is( ":hover" ) ){
				$( "#keyDelBtn" ).hide();
				$( "#keyDelAllBtn" ).show();
			}
		}
	}

	function ctrlKeyUp( e ){
		if ( e.keyCode == 17 || e.keyCode == 16 ){
			gPressedCtrlBtn = 0;
			$( ".cnlOrdBtn" ).removeClass( "cnlOrdMltSlet" );
			$( "#keyDelAllBtn" ).hide();
			$( "#keyDelBtn" ).show();
		}
	}

	function ckCnfmSaveKey(){
		if ( $( "#keySaveBttn" ).hasClass( "keyBtnEnabled" ) == true ){
			if ( gKeyName !=="tpCancel" && gKeyName !=="tpAlgo" ){
				if ( gLnkedKeyECN == 0 && $( "#keyDtlECNIpt" ).val() =="" ){
					$( "#keyDtlECNIpt" ).addClass( "keyDtlIptWarn" );
					$( "#keyDtlECNIpt" ).focus();
					setTimeout( function(){
						$( "#keyDtlECNIpt" ).removeClass( "keyDtlIptWarn" );
					} , 2000 );
					return;
				}else if ( $( "#keyDtlSymIpt" ).val() == "" ){
					$( "#keyDtlSymIpt" ).addClass( "keyDtlIptWarn" );
					$( "#keyDtlSymIpt" ).focus();
					setTimeout( function(){
						$( "#keyDtlSymIpt" ).removeClass( "keyDtlIptWarn" );
					} , 2000 );
					return;
				}
			}
			gCnfmType = 1;
			$( "#cnfmDiv" ).css( { "left":"550px" });
			$( "#cnfmKeyTxt" ).html( "Save Key" );
			$( "#cnfmKeyDetail" ).show();
			$( "#cnfmBg" ).fadeIn();
			$( "#cnfmDiv" ).fadeIn();
		}
	}

	function ckCnfmDelKey(){
		if ( $( "#keyDelBtn" ).hasClass( "keyBtnEnabled" ) == true ){
			gCnfmType = 2;
			$( "#cnfmDiv" ).css( { "left":"111px" });
			$( "#cnfmKeyTxt" ).html( "Delete key" );
			$( "#cnfmKeyDetail" ).show();
			$( "#cnfmBg" ).fadeIn();
			$( "#cnfmDiv" ).fadeIn();
		}
	}

	function ckCnfmDelAllKey(){
		gCnfmType = 3;
		$( "#cnfmDiv" ).css( { "left":"111px" });
		$( "#cnfmKeyTxt" ).html( "Delete All Keys ?" );
		$( "#cnfmKeyDetail" ).hide();
		$( "#cnfmBg" ).fadeIn();
		$( "#cnfmDiv" ).fadeIn();
	}

	function showCnfmMsg(){
		$( "#cnfmKeyKey" ).text( gKeyKey );
		if ( gKeyName == "tpLOv1" || gKeyName == "tpSOv1" ){
			var keyPx = parseFloat( d3.select( "#" + gKeyName + "_" + gKeySide + "Line" + 1 ).attr( "ordPx" ) );
			var keyType = d3.select( "#" + gKeyName + "_" + gKeySide + "Line" + 1 ).attr( "ordType" );
			if ( keyType == "LO2" ){
				keyType = "Limit";
			}else if ( keyType == "SO2" ){
				keyType = "Stop";
			}
			var keySide = d3.select( "#" + gKeyName + "_" + gKeySide + "Line" + 1 ).attr( "ordSide" );
			var keyQty = gKeyArySet[ 0 ].keyQty;
			var keyRef = gKeyArySet[ 0 ].keyRef;
			var keySpec = keyQty + "@ " + keyRef + " " + keyPx + " )";
			$( "#cnfmKeyKey" ).text( gKeyKey );
			$( "#cnfmKeyType" ).text( " ( " + keyType + " " );
			$( "#cnfmKeySide" ).text( keySide + " " );
			$( "#cnfmKeySpec" ).text( keySpec );
			$( "#cnfmKeySide" ).addClass( "cnfmKey" + keySide );
		}else if ( gKeyName == "tpCancel" ){
			if ( $( "#cnlAllIpt" ).prop( "checked" ) ){
				var keySpec = "( Cancel All Orders )";
			}else{
				var keySpec = "( Cancel Order )";
			}
			$( "#cnfmKeySpec" ).text( keySpec );
			$( "#cnfmKeyType" ).text( "" );
			$( "#cnfmKeySide" ).text( "" );
		}else if ( gKeyName == "tpAlgo" ){
			var keySpec = "( Algo Order )";
			$( "#cnfmKeySpec" ).text( keySpec );
			$( "#cnfmKeyType" ).text( "" );
			$( "#cnfmKeySide" ).text( "" );
		}
	}

	function cnfmCnfmed(){
		if ( gCnfmType == 1 ){
			saveHotkey();
		}else if ( gCnfmType == 2 ){
			delHotkey();
		}else if ( gCnfmType == 3 ){
			delAllHotkey();
		}
		gInstruct = 1;
		setTimeout( function(){
			showInstruct( 1 );
		} , 500 );
	}

	//////// d3 function ////////

	function drawCht( chtID , chtType , gOrdAry ){ //create a chart and draw line
		if ( chtType == 1 ){
			gChtMin = -200 * gChtScale;
			gChtMax = 100 * gChtScale;
			var chtY1 = 83;
		}else if ( chtType == 2 ){
			gChtMin = -100 * gChtScale;
			gChtMax = 200 * gChtScale;
			var chtY1 = 167;
		}
		var yScale = d3.scale.linear()
			.domain( [ gChtMin , gChtMax ] )
			.range( [ 250 , 0 ] );
		var line = d3.svg.line();
		var yAxis = d3.svg.axis()
			.scale( yScale )
			.ticks( 4 )
			.orient( "left" );
		var graph = d3.select( "#" + chtID )
			.append( "svg" )
			.attr( "id" , chtID + "_Svg" )
			.attr( "class" , "d3Svg" )
			.attr( "chtType" , chtType )
			.attr( "width" , 160 )
			.attr( "height" , 260 )
			.call( zoomEvent )
			.on( "mousedown.zoom" , null ) //prevent dragging line
			.on( "touchstart.zoom" , null )
			.on( "touchmove.zoom" , null )
			.on( "touchend.zoom" , null )
			.append( "svg:g" )
			.attr( "transform" , "translate( 30 , 5 )" );
		graph.append( "svg:g" )
			.attr( "class" , "d3yAxis" )
			.attr( "transform" , "translate( 0 , 0 )" )
			.call( yAxis );
		graph.append( "line" )
			.attr( "class" , "d3xAxis" )
			.attr( "x1" , 0 )
			.attr( "y1" , chtY1 )
			.attr( "x2" , 210 )
			.attr( "y2" , chtY1 );
		for ( var i = 0; i < gOrdAry.length; i++ ){ //draw line
			var chtY = convPxtoChtY( gOrdAry[ i ].chtOrdPx , chtType );
			var chtOrdNum = i + 1;
			graph.append( "rect" )
				.attr( "class" , "d3Line" + " d3_" + chtID + " " + chtID + "Line" + chtOrdNum )
				.attr( "id" , chtID + "Line" + chtOrdNum )
				.attr( "x" , 10 )
				.attr( "y" , chtY )
				.attr( "ordPx" , gOrdAry[ i ].chtOrdPx )
				.attr( "ordSide" , gOrdAry[ i ].chtOrdSide )
				.attr( "ordType" , gOrdAry[ i ].chtOrdType )
				.attr( "ordClass" , "pnt" )
				.attr( "chtType" , chtType )
				.attr( "chtID" , chtID )
				.attr( "lineNum" , chtOrdNum )
				.on( "mouseover" , function( d ){ d3.select( this ).style( "cursor" , "ns - resize" ); } )
				.on( "mouseout" , function( d ){ d3.select( this ).style( "cursor" , "" ); } )
				.on( "mouseup" , function(){ showInstruct( 5 ); } )
				.on( "click" , function(){
					sletChtLine( d3.select( this ).attr( "id" ) );
				});
		}
		var chtOrdBUYCount = 0;
		var chtOrdSELLCount = 0;
		var chtOrdBUYNum = 0;
		var chtOrdSELLNum = 0;
		for ( var i = 0; i < gOrdAry.length; i++ ){
			if ( gOrdAry[ i ].chtOrdSide == "BUY" )
				chtOrdBUYCount += 1;
			else if ( gOrdAry[ i ].chtOrdSide == "SELL" )
				chtOrdSELLCount += 1;
		}
		for ( var i = 0; i < gOrdAry.length; i++ ){ //draw tooltip background
			var chtOrdNum = i + 1;
			if ( gOrdAry[ i ].chtOrdSide == "BUY" ){
				var rectX = ( 80/( chtOrdBUYCount + 1 ) ) + ( chtOrdBUYNum * 30 ) ;
				chtOrdBUYNum += 1;
			}
			else if ( gOrdAry[ i ].chtOrdSide == "SELL" ){
				var rectX = ( 80/( chtOrdSELLCount + 1 ) ) + ( chtOrdSELLNum * 30 ) ;
				chtOrdSELLNum += 1;
			}
			graph.append( "rect" )
				.attr( "id" , chtID + "TipBg" + chtOrdNum )
				.attr( "class" , "d3TipBg" )
				.attr( "x" , rectX )
				.attr( "y" , d3.select( "#" + chtID + "Line" + chtOrdNum ).attr( "y" ) - 17 );
		}
		for ( var i = 0; i < gOrdAry.length; i++ ){ //draw tooltip text
			var chtOrdNum = i + 1;
			graph.append( "text" )
				.attr( "id" , chtID + "Tip" + chtOrdNum )
				.attr( "class" , "d3Tip" )
				.attr( "dy" , d3.select( "#" + chtID + "Line" + chtOrdNum ).attr( "y" ) - 6 )
				.text( d3.format( " + " )( d3.select( "#" + chtID + "Line" + chtOrdNum ).attr( "ordPx" ) ) )
				.attr( "dx" , parseFloat( d3.select( "#" + chtID + "TipBg" + chtOrdNum ).attr( "x" ) ) + 14 );
		}
	}

	function drawAllCht(){
		$( ".d3Svg" ).each( function(){
			d3.select( this ).remove();
		});
		//// draw d3 chart ////
		var gOrdAry = new Array();
		gOrdAry[ 0 ] = { chtOrdType:"LO2" , chtOrdSide:"BUY" , chtOrdPx:gDefLOBUYPx };
		drawCht( "tpLOv1_BUY" , 1 , gOrdAry );

		gOrdAry = new Array();
		gOrdAry[ 0 ] = { chtOrdType:"LO2" , chtOrdSide:"SELL" , chtOrdPx:gDefLOSELLPx };
		drawCht( "tpLOv1_SELL" , 2 , gOrdAry );

		gOrdAry = new Array();
		gOrdAry[ 0 ] = { chtOrdType:"SO2" , chtOrdSide:"BUY" , chtOrdPx:gDefSOBUYPx };
		drawCht( "tpSOv1_BUY" , 2 , gOrdAry );

		gOrdAry = new Array();
		gOrdAry[ 0 ] = { chtOrdType:"SO2" , chtOrdSide:"SELL" , chtOrdPx:gDefSOSELLPx };
		drawCht( "tpSOv1_SELL" , 1 , gOrdAry );

		if ( gKeyName == "tpLOv1" || gKeyName == "tpSOv1" ){
			gSletedLineID = gKeyName + "_" + gKeySide + "Line1";
		}
		sletChtLine( gSletedLineID );
	}

	function setChtLine( keyName , keySide , keyPx , keyLineNum , zmCht ){ //set line price
		if ( chtType == 1 ){
			gChtMin = - 200 * gChtScale;
			gChtMax = 100 * gChtScale;
		}else if ( chtType == 2 ){
			gChtMin = -100 * gChtScale;
			gChtMax = 200 * gChtScale;
		}
		if ( zmCht == 1 ){
			for ( var i = 0; i < gChtMaxZmLv; i++ ){
				if ( keyPx > 0 ){
					if ( keyPx > gChtMax && gChtZmLevel < gChtMaxZmLv ){
						zmChtOut();
					}else if ( keyPx < ( gChtMax/gChtZmScale ) && gChtZmLevel > 3 ){
						zmChtIn();
					}else{
						break;
					}
				}else if ( keyPx < 0 ){
					if ( keyPx < gChtMin && gChtZmLevel < gChtMaxZmLv ){
						zmChtOut();
					}else if ( keyPx > ( gChtMin/gChtZmScale ) && gChtZmLevel > 3 ){
						zmChtIn();
					}else{
						break;
					}
				}else{
					break;
				}
			}
		}
		var chtID = keyName + "_" + keySide;
		var lineID = chtID + "Line" + keyLineNum;
		var IDArray = getChtIDAry( lineID );
		var chtType = d3.select( "#" + IDArray.lineID ).attr( "chtType" );
		var chtY = convPxtoChtY( keyPx , chtType );
		d3.select( "#" + IDArray.lineID )
			.attr( "y" , chtY )
			.attr( "ordPx" , keyPx );
		d3.select( "#" + IDArray.tipID )
			.text( d3.format( " + " )( keyPx ) )
			.attr( "dy" , chtY - 6 );
		d3.select( "#" + IDArray.tipBgID )
			.attr( "y" , chtY - 17 );
	}

	function setChtChdLine( lineID , setValue ){ // change the ifdone order distance on key spec panel
		var IDArray = getChtIDAry( lineID );
		var chtType = d3.select( "#" + lineID ).attr( "chtType" );
		var pntID = d3.select( "#" + lineID ).attr( "pntID" );
		var pntOrdPx = parseFloat( d3.select( "#" + pntID ).attr( "ordPx" ) );
		var ordDist = parseFloat( setValue );
		var ordPx = pntOrdPx + ordDist;
		var chtY = convPxtoChtY( ordPx , chtType );
		d3.select( "#" + lineID )
			.attr( "y" , chtY )
			.attr( "ordPx" , ordDist );
		d3.select( "#" + IDArray.tipID )
			.text( d3.format( " + " )( ordDist ) )
			.attr( "dy" , chtY - 6 );
		d3.select( "#" + IDArray.tipBgID )
			.attr( "y" , chtY - 17 );
	}

	function sletChtLine( lineID ){
		d3.select( "#" + lineID ).call( dragEvent );
		var IDArray = getChtIDAry( lineID );
		gSletedChtID = d3.select( "#" + lineID ).attr( "chtID" );
		gSletedLineID = lineID;
		gSletedLineNum = d3.select( "#" + lineID ).attr( "lineNum" );
		d3.selectAll( ".d3SletedLine" ).each(
			function(){
				d3.select( this ).classed( "d3SletedLine" , false );
			}
		);
		d3.selectAll( ".d3SletedTipBg" ).each(
			function(){
				d3.select( this ).classed( "d3SletedTipBg" , false );
			}
		);
		d3.selectAll( ".d3SletedTip" ).each(
			function(){
				d3.select( this ).classed( "d3SletedTip" , false );
			}
		);
		if ( d3.select( "#" + lineID ).classed( "d3SletedLine" ) == false ){
			d3.select( "#" + lineID ).classed( "d3SletedLine" , true );
			d3.select( "#" + IDArray.tipBgID ).classed( "d3SletedTipBg" , true );
			d3.select( "#" + IDArray.tipID ).classed( "d3SletedTip" , true );
			dom = document.getElementById( lineID );
			dom.parentNode.appendChild( dom );
			dom = document.getElementById( IDArray.tipBgID );
			dom.parentNode.appendChild( dom );
			dom = document.getElementById( IDArray.tipID );
			dom.parentNode.appendChild( dom );
		}
		if ( d3.select( "#" + lineID ).attr( "ordType" ) == "LO2" ){
			var ordType = "Limit";
		}else if ( d3.select( "#" + lineID ).attr( "ordType" ) == "SO2" ){
			var ordType = "Stop";
		}
		var ordSide = d3.select( "#" + lineID ).attr( "ordSide" );
		$( ".keyDtlTypeTxt" ).text( ordType );
		$( ".keyDtlSideTxt" ).text( ordSide );

		var ordPx = d3.select( "#" + lineID ).attr( "ordPx" );
		var ordClass = d3.select( "#" + lineID ).attr( "ordClass" );
		if ( ordClass == "chd" ){
			$( "#keyChdOrdDist" ).val( ordPx );
		}
		updateOrdDtlSec();
	}

	function addChtLine( ordType ){ // add a line on chart
		var lineID = gSletedLineID;
		var chtID = gSletedChtID;
		var chtSVG = gSletedChtID + "_Svg";
		var pntChtType = d3.select( "#" + lineID ).attr( "chtType" );
		var pntOrdSide = d3.select( "#" + lineID ).attr( "ordSide" );
		var pntOrdPx = parseFloat( d3.select( "#" + lineID ).attr( "ordPx" ) );
		var pntOrdY = parseFloat( d3.select( "#" + lineID ).attr( "y" ) );
		var pntLineNum = d3.select( "#" + lineID ).attr( "lineNum" );
		var chdLineNum = $( ".d3_" + gSletedChtID ).length + 1;
		if ( pntOrdSide == "BUY" ){
			if ( ordType == "SO2" ){
				var ordDist = - gDefChdOrdDist;
			}else if ( ordType == "LO2" ){
				var ordDist = gDefChdOrdDist;
			}
			var ordSide = "SELL";
		}else if ( pntOrdSide == "SELL" ){
			if ( ordType == "SO2" ){
				var ordDist = gDefChdOrdDist;
			}else if ( ordType == "LO2" ){
				var ordDist = - gDefChdOrdDist;
			}
			var ordSide = "BUY";
		}
		var ordPx = pntOrdPx + ordDist;
		var chtY = convPxtoChtY( ordPx , pntChtType );
		d3.select( "#" + chtSVG ).select( "g" ).append( "rect" )
			.attr( "class" , "d3Line" + " d3_" + chtID + " d3Line_IfD" + " " + gSletedLineID )
			.attr( "id" , chtID + "Line" + chdLineNum )
			.attr( "x" , 40 )
			.attr( "y" , chtY )
			.attr( "ordPx" , ordDist )
			.attr( "ordSide" , ordSide )
			.attr( "ordType" , ordType )
			.attr( "chtType" , pntChtType )
			.attr( "ordClass" , "chd" )
			.attr( "chtID" , chtID )
			.attr( "lineNum" , chdLineNum )
			.attr( "pntID" , gSletedLineID )
			.on( "mouseover" , function( d ){ d3.select( this ).style( "cursor" , "ns - resize" ); } )
			.on( "mouseout" , function( d ){ d3.select( this ).style( "cursor" , "" ); } )
			.on( "click" , function(){
				sletChtLine( d3.select( this ).attr( "id" ) );
			});
		var rectX = parseFloat( d3.select( "#" + chtID + "TipBg" + pntLineNum ).attr( "x" ) ) + 30;
		d3.select( "#" + chtSVG ).select( "g" ).append( "rect" )
			.attr( "id" , chtID + "TipBg" + chdLineNum )
			.attr( "class" , 'd3TipBg d3TipBg_IfD' )
			.attr( "x" , rectX )
			.attr( "y" , parseFloat( d3.select( "#" + chtID + "Line" + chdLineNum ).attr( "y" ) ) - 17 );
		d3.select( "#" + chtSVG ).select( "g" ).append( "text" )
			.attr( "id" , chtID + "Tip" + chdLineNum )
			.attr( "class" , 'd3Tip d3Tip_IfD' )
			.attr( "dy" , d3.select( "#" + chtID + "Line" + chdLineNum ).attr( "y" ) - 6 )
			.text( d3.format( " + " )( ordDist ) )
			.attr( "dx" , parseFloat( d3.select( "#" + chtID + "TipBg" + chdLineNum ).attr( "x" ) ) + 14 );
		d3.select( "#" + lineID ).attr( "chdID_" + ordType , chtID + "Line" + chdLineNum );

		gKeyArySet[ chdLineNum - 1 ].keyECN = gKeyArySet[ pntLineNum - 1 ].keyECN;
		gKeyArySet[ chdLineNum - 1 ].keySym = gKeyArySet[ pntLineNum - 1 ].keySym;
		gKeyArySet[ chdLineNum - 1 ].keyQty = gKeyArySet[ pntLineNum - 1 ].keyQty;
		gKeyArySet[ chdLineNum - 1 ].keyDur = gKeyArySet[ pntLineNum - 1 ].keyDur;
		gKeyArySet[ chdLineNum - 1 ].keySlip = gKeyArySet[ pntLineNum - 1 ].keySlip;
		if ( pntOrdSide == "BUY" ){
			gKeyArySet[ chdLineNum - 1 ].keyRef = "ASK";
		}else if ( pntOrdSide == "SELL" ){
			gKeyArySet[ chdLineNum - 1 ].keyRef = "BID";
		}
	}

	function rmChtLine( btnType ){ // delete a line on chart
		var chdID = d3.select( "#" + gSletedLineID ).attr( "chdID_" + btnType );
		if ( chdID !== "" ){
			var IDArray = getChtIDAry( chdID );
			d3.select( "#" + IDArray.lineID ).remove();
			d3.select( "#" + IDArray.tipID ).remove();
			d3.select( "#" + IDArray.tipBgID ).remove();
			editChtLineID();
		}
	}

	function editChtLineID(){
		$( "." + gSletedLineID ).each( function(){
			var ordClass = d3.select( this ).attr( "ordClass" );
			var lineNum = d3.select( this ).attr( "lineNum" );
			var lineID = d3.select( this ).attr( "id" );
			var chtID = d3.select( this ).attr( "chtID" );
			var ordType = d3.select( this ).attr( "ordType" );
			var lineCount = $( "." + gSletedLineID ).length;
			var IDArray = getChtIDAry( lineID );
			if ( ordClass == "chd" && lineCount < lineNum ){
				var pntID = d3.select( this ).attr( "pntID" );
				d3.select( "#" + pntID ).attr( "chdID_" + ordType , chtID + "Line" + lineCount );
				d3.select( "#" + IDArray.tipID ).attr( "id" , chtID + "Tip" + lineCount );
				d3.select( "#" + IDArray.tipBgID ).attr( "id" , 	chtID + "TipBg" + lineCount );
				d3.select( "#" + lineID ).attr( "lineNum" , lineCount );
				d3.select( "#" + lineID ).attr( "id" , chtID + "Line" + lineCount );
			}
		});
	}

	function convChtYtoPx( chtY , chtType ){
		chtY = parseFloat( chtY );
		if ( chtType == 1 ){
			var px = Math.round( - ( chtY - 81 ) * gChtMulti * gChtScale );
		}else{
			var px = Math.round( ( - chtY + 165 ) * gChtMulti * gChtScale );
		}
		return px;
	}

	function convPxtoChtY( px , chtType ){
		px = parseFloat( px );
		if ( chtType == 1 ){
			var chtY = 158 - ( px/gChtScale/gChtMulti + 78 );
		}else{
			var chtY = ( - px/gChtScale/gChtMulti + 165 );
		}
		return chtY;
	}

	function getChtIDAry( lineID ){
		var chtID = d3.select( "#" + lineID ).attr( "chtID" );
		var lineNum = d3.select( "#" + lineID ).attr( "lineNum" );
		var tipID = chtID + "Tip" + lineNum;
		var tipBgID = chtID + "TipBg" + lineNum;
		return {
			lineID: lineID ,
			tipID: tipID ,
			tipBgID: tipBgID ,
			lineNum: lineNum
		};
	}

	function dragChtClass(){ // drag line class ( include parent line and each ifdone line )
		var ordClass = d3.select( "#" + gSletedLineID ).attr( "ordClass" );
		if ( ordClass == "pnt" ){
			$( "." + gSletedLineID ).each( function(){
				var lineID = d3.select( this ).attr( "id" );
				IDArray = getChtIDAry( lineID );
				dragChtLine( IDArray , 1 );
			});
		}else if ( ordClass == "chd" ){
			var IDArray = getChtIDAry( gSletedLineID );
			dragChtLine( IDArray , 2 );
		}
		showInstruct( 5 );
	}

	function dragChtLine( IDArray , dragType ){
		var ordClass = d3.select( "#" + IDArray.lineID ).attr( "ordClass" );
		var ordSide = d3.select( "#" + IDArray.lineID ).attr( "ordSide" );
		var ordType = d3.select( "#" + IDArray.lineID ).attr( "ordType" );
		var chtType = d3.select( "#" + IDArray.lineID ).attr( "chtType" );
		var ordPx = parseFloat( d3.select( "#" + IDArray.lineID ).attr( "ordPx" ) );
		var dy = d3.event.dy;
		var chtY = parseFloat( d3.select( "#" + IDArray.lineID ).attr( "y" ) ) + dy;
		var newOrdPx = parseFloat( convChtYtoPx( chtY , chtType ) );
		var tipY = chtY - 6;
		var tipBgY = chtY - 17;
		var trans = 0;
		if ( ordClass == "pnt" ){
			var chtPx = newOrdPx;
		}else if ( ordClass == "chd" ){
			var pntID = d3.select( "#" + IDArray.lineID ).attr( "pntID" );
			var pntChtY = parseFloat( d3.select( "#" + pntID ).attr( "y" ) ) + dy;
			var pntOrdPx = parseFloat( d3.select( "#" + pntID ).attr( "ordPx" ) );
			var chtPx = convChtYtoPx( pntChtY , chtType );
			if ( dragType == 1 ){
				newOrdPx = ordPx;
			}else if ( dragType == 2 ){
				var newOrdPx = newOrdPx - pntOrdPx;
			}
		}
		if ( ( chtType==1 && chtPx <= ( 100 * gChtScale ) && chtPx >= ( - 200 * gChtScale ) ) || ( chtType==2 && chtPx <= ( 200 * gChtScale ) && chtPx >= ( - 100 * gChtScale ) ) ){
			if ( ordClass == "pnt" && ( ( chtType==1 && newOrdPx > ( 85 * gChtScale ) ) || ( chtType==2 && newOrdPx > ( 185 * gChtScale ) ) ) ){
				tipY += 30;
				tipBgY += 30;
				transition = 100;
			}
			d3.select( "#" + IDArray.tipID )
				.text( d3.format( " + " )( newOrdPx ) ) // can't put this after transition
				.transition().duration( trans )
				.attr( "dy" , tipY );
			d3.select( "#" + IDArray.lineID )
				.attr( "y" , chtY )
				.attr( "ordPx" , newOrdPx );
			d3.select( "#" + IDArray.tipBgID )
				.transition().duration( trans )
				.attr( "y" , tipBgY );
			$( "#keyChdOrdDist" ).val( newOrdPx );
		}
	}

	function zoomChtClass(){
		if ( gSletedChtID !== "" ){
			var zmScale = d3.event.scale;
			var ordClass = d3.select( "#" + gSletedLineID ).attr( "ordClass" );
			if ( ordClass == "pnt" ){
				$( "." + gSletedLineID ).each( function(){
					var lineID = d3.select( this ).attr( "id" );
					IDArray = getChtIDAry( lineID );
					zoomChtLine( IDArray , zmScale , 1 );
				});
			}else if ( ordClass == "chd" ){
				var IDArray = getChtIDAry( gSletedLineID );
				zoomChtLine( IDArray , zmScale , 2 );
			}
			gChtZoom = d3.event.scale;
		}
	}

	function zoomChtLine( IDArray , zmScale , zmType ){
		var ordClass = d3.select( "#" + IDArray.lineID ).attr( "ordClass" );
		var ordSide = d3.select( "#" + IDArray.lineID ).attr( "ordSide" );
		var ordType = d3.select( "#" + IDArray.lineID ).attr( "ordType" );
		var chtType = d3.select( "#" + IDArray.lineID ).attr( "chtType" );
		var ordPx = parseFloat( d3.select( "#" + IDArray.lineID ).attr( "ordPx" ) );
		var newOrdPx = ordPx;
		var lineY = parseFloat( d3.select( "#" + IDArray.lineID ).attr( "y" ) );
		var tipY = parseFloat( d3.select( "#" + IDArray.tipID ).attr( "dy" ) );
		var chtY = parseFloat( d3.select( "#" + IDArray.lineID ).attr( "y" ) );
		var tipBgY = parseFloat( d3.select( "#" + IDArray.tipBgID ).attr( "y" ) );
		if ( zmScale > gChtZoom ){
			lineY -= ( 1/gChtMulti )/gChtScale;
			tipY -= ( 1/gChtMulti )/gChtScale;
			newOrdPx += 1;
			tipBgY -= ( 1/gChtMulti )/gChtScale;
			var chtPx = 1;
		}else{
			lineY += ( 1/gChtMulti )/gChtScale;
			tipY += ( 1/gChtMulti )/gChtScale;
			newOrdPx -= 1;
			tipBgY += ( 1/gChtMulti )/gChtScale;
			var chtPx = -1;
		}
		if ( ordClass == "pnt" ){
			chtPx = newOrdPx;
		}else if ( ordClass == "chd" ){
			var pntID = d3.select( "#" + IDArray.lineID ).attr( "pntID" );
			var pntOrdPx = parseFloat( d3.select( "#" + pntID ).attr( "ordPx" ) );
			chtPx = chtPx + pntOrdPx;
		}
		if ( ( chtType==1 && chtPx <= ( 100 * gChtScale ) && chtPx >= ( - 200 * gChtScale ) ) || ( chtType==2 && chtPx <= ( 200 * gChtScale ) && chtPx >= ( - 100 * gChtScale ) ) ){
			if ( ordClass == "pnt" ){
				d3.select( "#" + IDArray.lineID )
					.attr( "ordPx" , newOrdPx );
			}else if ( ordClass == "chd" ){
				if ( zmType == 1 ){
					newOrdPx = ordPx;
				}else if ( zmType == 2 ){
					d3.select( "#" + IDArray.lineID )
						.attr( "ordPx" , newOrdPx );
					$( "#keyChdOrdDist" ).val( newOrdPx );
				}
			}
			d3.select( "#" + IDArray.lineID )
				.attr( "y" , lineY );
			d3.select( "#" + IDArray.tipID )
				.text( d3.format( " + " )( newOrdPx ) )
				.attr( "dy" , tipY );
			d3.select( "#" + IDArray.tipBgID )
				.attr( "y" , tipBgY );
		}
	}

	function zmChtIn(){
		if ( gChtZmLevel > gChtMinZmLv ){
			var chtType = d3.select( "#" + gKeyName + "_" + gKeySide + "Line1" ).attr( "chtType" );
			var ordPx = d3.select( "#" + gKeyName + "_" + gKeySide + "Line1" ).attr( "ordPx" );
			if ( chtType == 1 ){
				var chtMin = - 200 * gChtScale / gChtZmScale;
				var chtMax = 100 * gChtScale / gChtZmScale;
			}else if ( chtType == 2 ){
				var chtMin = -100 * gChtScale / gChtZmScale;
				var chtMax = 200 * gChtScale / gChtZmScale;
			}
			if ( ordPx <= chtMax && ordPx>= chtMin ){
				gChtScale = gChtScale / gChtZmScale;
				gChtZmLevel -=1;
				drawAllCht();
				setChtLine( gKeyName , gKeySide , ordPx , 1 , 0 );
				$( "#keyBtnIfD_LO2" ).prop( "checked" , false );
				$( "#keyBtnIfD_SO2" ).prop( "checked" , false );
			}
		}
	}

	function zmChtOut(){
		if ( gChtZmLevel < gChtMaxZmLv ){
			gChtScale = gChtScale * gChtZmScale;
			gChtZmLevel +=1;
			var ordPx = d3.select( "#" + gKeyName + "_" + gKeySide + "Line1" ).attr( "ordPx" );
			drawAllCht();
			setChtLine( gKeyName , gKeySide , ordPx , 1 , 0 );
			$( "#keyBtnIfD_LO2" ).prop( "checked" , false );
			$( "#keyBtnIfD_SO2" ).prop( "checked" , false );
		}
	}

	// jpanel notification function

	function showTrigNotice( key ){ // show notification when hotkey is triggered
		if ( onTrigNotice == 1 ){
			$( "#jPanelNtcDiv" ).clearQueue().stop( true , false ); // clear animate
			$( "#jPanelNtcDiv" ).hide();
			if ( document.getElementById( "kbKey" + key ).kbKeyAry[ 0 ].keyName == "tpCancel" ){
				$( "#jPanelNtcMsg" ).html( "Hotkey <span class='jNoticeKeyCnl'>" + key + "</span>" );
			}else{
				if ( document.getElementById( "kbKey" + key ).kbKeyAry[ 0 ].keySide == "BUY" ){
					$( "#jPanelNtcMsg" ).html( "Hotkey <span class='jNoticeKeyBUY'>" + key + "</span>" );
				}else if ( document.getElementById( "kbKey" + key ).kbKeyAry[ 0 ].keySide == "SELL" ){
					$( "#jPanelNtcMsg" ).html( "Hotkey <span class='jNoticeKeySELL'>" + key + "</span>" );
				}
			}
			if ( key == "SPACE" ){
				$( "#jPanelNtcDiv" ).css( { "width":"170px" });
			}else{
				$( "#jPanelNtcDiv" ).css( { "width":"126px" });
			}
			$( "#jPanelNtcDiv" ).fadeIn( 100 , function(){
				$( "#jPanelNtcDiv" ).delay( 800 ).fadeOut( 200 );
			});
		}
	}

	function showKMNotice( msgType , key ){ // show notification when hotkey setting is changed
		if ( gKeyName == "tpCancel" ){
			var keyType = "Cnl";
		}else if ( gKeyName == "Algo" ){
			var keyType = "Algo";
		}else{
			if ( gKeySide == "BUY" ){
				var keyType = "BUY";
			}else if ( gKeySide == "SELL" ){
				var keyType = "SELL";
			}
		}
		if ( key == "SPACE" ){
			$( "#jNoticeKMDiv" ).css( { "width":"240px" });
		}else{
			$( "#jNoticeKMDiv" ).css( { "width":"200px" });
		}
		$( "#jNoticeKMDiv" ).clearQueue().stop( true , false ); // clear animate
		$( "#jNoticeKMMsg" ).clearQueue().stop( true , false );
		$( "#jNoticeKMDiv" ).hide();
		$( "#jNoticeKMMsg" ).css( { left:"6px" });
		if ( msgType == 1 ){
			$( "#jNoticeKMMsg" ).html( "Hotkey <span class='jNoticeKey" + keyType + "'>" + key + "</span> is saving ......" );
		}else if ( msgType == 2 ){
			$( "#jNoticeKMMsg" ).html( "Hotkey <span class='jNoticeKey" + keyType + "'>" + key + "</span> is deleting ......" ); // send key deleting notice
		}else if ( msgType == 3 ){
			$( "#jNoticeKMMsg" ).html( "Hotkey <span class='jNoticeKey" + keyType + "'>" + key + "</span> is saved." );
		}else if ( msgType == 4 ){
			$( "#jNoticeKMMsg" ).html( "Hotkey <span class='jNoticeKey" + keyType + "'>" + key + "</span> is deleted." ); // send key delete notice
		}else if ( msgType == 5 ){
			$( "#jNoticeKMMsg" ).html( "All Hotkey is deleted." ); // send deleting all hotkey notice
		}
		$( "#jNoticeKMDiv" ).fadeIn( 400 );
		$( "#jNoticeKMMsg" ).animate( { left:"40px" } , 1000 );
		if ( msgType >= 3 ){
			$( "#jNoticeKMDiv" ).delay( 1200 ).fadeOut( 400 );
			setTimeout( function(){
				$( "#kbKey" + key ).click();
			} , 100 );
		}
	}

	function getRandomNumber(){ // 1 - 100
		var num = Math.floor( ( Math.random() * 100 ) + 1 );
		return num;
	}

	function getClientIP(){ // get user ip address
		var defer = $.Deferred();
		var retry = 0;

		gGetIPInterval = setInterval( function (){
			if ( ( gIP != "" && typeof gIP != "undefined" ) || retry > 10 ){
				clearInterval( gGetIPInterval );
				defer.resolve( null );
				return defer;
			}

			window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
			var pc = new RTCPeerConnection( { iceServers:[] } ) , noop = function (){};
			pc.createDataChannel( "" );
			pc.createOffer( pc.setLocalDescription.bind( pc ) , noop );

			pc.onicecandidate = function ( ice ){
				if ( !ice || !ice.candidate || !ice.candidate.candidate ) {
					return;
				}
				var regex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/;

				gIP = regex.exec( ice.candidate.candidate )[1];
				$( "#userInfoIP" ).text( gIP );

				pc.onicecandidate = noop;
			};

			retry++ ;
		} , 500 );

		return defer;
	}

	///////// for adding algo function ////////

	function pushCMD( cmd ){
		if ( gDisconnected == 1 || gReloaded == 1 ){
			return;
		}

		gInitOrdSubscription.push( cmd );
		waitCMR();
	}

	function waitCMR(){
		if ( gWaitCMRInterval ){
			return;
		}

		clearInterval( gWaitCMRInterval );
		gWaitCMRInterval = false;

		gGotCMR = false;
		var retryTime = 0;
		var waitCMRTime = 1000;

		if ( gWaitCMRInterval ){
			return;
		}

		gWaitCMRInterval = setInterval( function (){
			if ( gGotCMR ){
				clearInterval( gWaitCMRInterval );
				gWaitCMRInterval = false;
				return;
			}

			if ( retryTime < 5 ){
				retryTime++ ;
				return;
			}

			clearInterval( gWaitCMRInterval );
			gWaitCMRInterval = false;

			gDisconnected = 1;
		} , waitCMRTime );
	}

	function showLog( name , content ){ // show con sole log
		if ( !gShowLog ){
			return;
		}

		console.log( name + " : " + content );
	}

	function GFA_disconnected(){
		location.reload();
		gDisconnected = 1; // disable panel react if gfa is disconnected
	}

	function sletAlgoOrdKey(){
		$( ".algoOrdIpt" ).prop( "checked" , false );
		$( ".algoOrdIpt" ).removeClass( "algoOrdChked" );
		$( this ).prop( "checked" , true );
		$( this ).addClass( "algoOrdChked" );
		$( "#keySaveBttn" ).addClass( "keyBtnEnabled" );
		gInstruct = 5;
		showInstruct( 5 );
	}
 }