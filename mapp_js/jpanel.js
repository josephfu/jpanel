$( function(){
	var gOnKM = 1; //trun on hotkey mapping panel
	var gOnHSI = -1; //trun on HSI panel`
	var gOnSC = -1; // trun on short cut panel
	var gOnMsg = 1; // trun on message panel
	var gOnBadge = 1; // trun on tab badge message counter
	var gOnLeader = 1; // trun on get leader function
	var gOnTrigNotice = -1;//trun on hotkey trigger notice
	var gOnCS = 1; // Chan Shing Algo
	var gOnAlgo = 1; // trun on algo
	var gOnAlgo2 = 1;

	var gOnUser = [ 111 , 301 , 302 , 303 , 304 , 305 , 306 , 307 , 308 , 309 , 310 , 311 , 312 , 313 , 314 , 315 , 316 , 317 , 318 , 319 , 320 , 321 , 322 , 323 , 324 , 325 , 326 , 327 , 328 , 329 , 340 , 341 , 342 , 343 , 345 , 346 , 347 , 348 , 349 , 428 , 528 , 529 , 527 ];
	var gKmUser = [ 301 , 302 , 303 , 304 , 305 , 306 , 307 , 308 , 309 , 310 , 311 , 312 , 313 , 314 , 315 , 316 , 317 , 318 , 319 , 320 , 321 , 322 , 323 , 324 , 325 , 326 , 327 , 328 , 329 , 340 , 341 , 342 , 343 , 345 , 346 , 347 , 348 , 349 , 428 , 527 , 528 , 529 ];
	var gHsiUser = [];
	var gScUser = [];
	var gMsgUser = [ 301 , 302 , 303 , 304 , 305 , 306 , 307 , 308 , 309 , 310 , 311 , 312 , 313 , 314 , 315 , 316 , 317 , 318 , 319 , 320 , 321 , 322 , 323 , 324 , 325 , 326 , 327 , 328 , 329 , 340 , 341 , 342 , 343 , 345 , 346 , 347 , 348 , 349 , 528 , 529 ];
	var gBadgeUser = [ 301 , 302 , 303 , 304 , 305 , 306 , 307 , 308 , 309 , 310 , 311 , 312 , 313 , 314 , 315 , 316 , 317 , 318 , 319 , 320 , 321 , 322 , 323 , 324 , 325 , 326 , 327 , 328 , 329 , 340 , 341 , 342 , 343 , 345 , 346 , 347 , 348 , 349 , 528 , 529 ];
	var gLeaderUser = [];
	var gCSUser = [];
	var gAlgoUser = [ 301 , 302 , 303 , 304 , 305 , 306 , 307 , 308 , 309 , 310 , 311 , 312 , 313 , 314 , 315 , 316 , 318 , 319 , 320 , 321 , 322 , 323 , 324 , 325 , 326 , 328 , 329 , 340 , 341 , 342 , 343 , 344 , 345 , 346 , 347 , 348 , 349 , 528 , 529 ];
	var gAlgo2User = [ 301 , 305 , 307 , 308 , 310 , 314 , 324 , 313 , 323 , 328 , 341 , 342 , 343 , 344 , 345 , 346 , 347 , 348 , 349 ];

	///// global variable ////
	var gDir = "mapp_js";
	var gMinDir ="mapp_js/min";
	var gJMsgCount = 0; // for jPanel Message box
	var gJMsgMaxNum = 10;
	//////////////////////////

	ee.addListener( "MMO_INIT" , jpanelStart ); // load the panel after received gfa userid.

	function jpanelStart( gInitObj ){
		var gUserID = gInitObj.u;

		if ( gOnUser.indexOf( gUserID ) == -1 ) {
			return;
		}

		if( gOnKM != -1 ){
			gOnKM = gKmUser.indexOf( gUserID );
		}
		if( gOnHSI != -1 ){
			gOnHSI = gHsiUser.indexOf( gUserID );
		}
		if( gOnSC != -1 ){
			gOnSC = gScUser.indexOf( gUserID );
		}
		if( gOnMsg != -1 ){
			gOnMsg = gMsgUser.indexOf( gUserID );
		}
		if( gOnLeader != -1 ){
			gOnLeader = gLeaderUser.indexOf( gUserID );
		}
		if( gOnCS != -1 ){
			gOnCS = gCSUser.indexOf( gUserID );
		}
		if( gOnAlgo != -1 ){
			gOnAlgo = gAlgoUser.indexOf( gUserID );
		}
		if( gOnAlgo2 != -1 ){
			gOnAlgo2 = gAlgo2User.indexOf( gUserID );
		}

		// append tab and tab content on gfa
		if ( gOnAlgo2 >= 0 ){
			$( "#ob_container .tabs" ).append( "<dd id='jpanel_algo' class='jpanelDD'><span class='badgeSp'></span><a href='#jPanelAlgo'>Algo2</a></dd>" );
			$( "#ob_container .tabs-content" ).append( "<li id='jPanelAlgoTab' class='jPanelTab'><div class='jPanelDiv' id='jPanelAlgoDiv'></div></li>" );
		}else if ( gOnAlgo >= 0 ){
			$( "#ob_container .tabs" ).append( "<dd id='jpanel_algo' class='jpanelDD'><span class='badgeSp'></span><a href='#jPanelAlgo'>Algo</a></dd>" );
			$( "#ob_container .tabs-content" ).append( "<li id='jPanelAlgoTab' class='jPanelTab'><div class='jPanelDiv' id='jPanelAlgoDiv'></div></li>" );
		}

		if ( gOnKM >= 0 ){
			$( "#ob_container .tabs" ).append( "<dd id='jpanel_km' class='jpanelDD'><a href='#jPanelKM'>Key Map</a></dd>" );
			$( "#ob_container .tabs-content" ).append( "<li id='jPanelKMTab' class='jPanelTab'><div class='jPanelDiv' id='jPanelKMDiv'></div></li>" );
		}
		if ( gOnHSI >= 0 ){
			$( "#ob_container .tabs" ).append( "<dd id='jpanel_hsi' class='jpanelDD'><a href='#jPanelHSI'>HSI</a></dd>" );
			$( "#ob_container .tabs-content" ).append( "<li id='jPanelHSITab' class='jPanelTab'><div class='jPanelDiv' id='jPanelHSIDiv'></div></li>" );
		}
		if ( gOnSC >= 0 ){
			$( "#ob_container .tabs" ).append( "<dd id='jpanel_sc' class='jpanelDD'><a href='#jPanelSC'>Shortcut</a></dd>" );
			$( "#ob_container .tabs-content" ).append( "<li id='jPanelSCTab' class='jPanelTab'><div class='jPanelDiv' id='jPanelSCDiv'></div></li>" );
		}
		if ( gOnCS >= 0 ){
			$( "#ob_container .tabs" ).append( "<dd id='jpanel_cs' class='jpanelDD'><span class='badgeSp'></span><a href='#jPanelCS'>CS Algo</a></dd>" );
			$( "#ob_container .tabs-content" ).append( "<li id='jPanelCSTab' class='jPanelTab'><div class='jPanelDiv' id='jPanelCSDiv'></div></li>" );
		}
		if ( gOnMsg >= 0 ){
			$( "#ob_container .tabs" ).append( "<dd id='jpanel_msg' class='jpanelDD'><span class='badgeSp'></span><a href='#jPanelMsg'>Message</a></dd>" );
			$( "#ob_container .tabs-content" ).append( "<li id='jPanelMsgTab' class='jPanelTab'><div class='jPanelDiv' id='jPanelMsgDiv'></div></li>" );
		}
		$( document ).foundationTabs(); //( GFA function ) connect dd and tabs

		// apend font and css
		$( "head" ).append( '<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">' );
		$( "head" ).append( '<link rel="stylesheet" href="' + gDir + '/jpanel.css" type="text/css" />' );

		$( ".jpanelDD" ).on( "click" , clearBadge );
		ee.addListener( "resize_grid_height" , chgHeight ); // return new height when grid is resized
		var height = $( "#bottom-component" ).height()
		$( ".jPanelTab" ).height( height - 120 );

		if ( gOnKM >= 0 ){
			$( "#jPanelKMDiv" ).load( gDir + "/jpanel_km.html" , function(){
				$( 'head' ).append( '<link rel="stylesheet" href="' + gDir + '/jpanel_km.css" type="text/css" />' );
				$.getScript( gDir + "/jpanel_km_d3.js" , function(){
					$.getScript( gDir + "/jpanel_km.js" , function(){ // load key map js
						jpanelKM( gInitObj , showBadgeNotice , showJPanelMsg , gOnTrigNotice );
					} );
				} );
			} );
		}
		if ( gOnHSI >= 0 ){
			$( "#jPanelHSIDiv" ).load( gDir + "/jpanel_hsi.html" , function(){
				$( 'head' ).append( '<link rel="stylesheet" href="' + gDir + '/jpanel_hsi.css" type="text/css" />' );
				$.getScript( gDir + "/jpanel_hsi.js" , function(){ // load hsi tool js
					jpanelHSI( gUserID , gOrdSubscription );
				} );
			} );
		}
		if ( gOnSC >= 0 ){
			$( "#jPanelSCDiv" ).load( gDir + "/jpanel_sc.html" , function(){
				$( 'head' ).append( '<link rel="stylesheet" href="' + gDir + '/jpanel_sc.css" type="text/css" />' );
				$.getScript( gDir + "/jpanel_sc.js" , function(){ // load shortcut js
					jpanelSC( gUserID , gOrdSubscription );
				} );
			} )
		}
		if ( gOnAlgo2 >= 0 ){
			$.getScript( gMinDir + "/jpanel_algo_symbol.min.js" , function(){
				$.getScript( gMinDir + "/jpanel_algo2_parameter.min.js" , function(){
					$.getScript( gMinDir + "/jpanel_algo2_config.min.js" , function(){
						$.getScript( gMinDir + "/jpanel_algo2_variable.min.js" , function(){
							$( "#jPanelAlgoDiv" ).load( gDir + "/jpanel_algo2.html" , function(){
								$.get( gDir + "/jpanel_algo2_mode.html" , function ( data ) {
									$( "body" ).prepend( data );
									$.get( gDir + "/jpanel_algo2_table.html" , function ( data ) {
										$( "body" ).prepend( data );
										setTimeout( function (){
											$.getScript( gMinDir + "/jpanel_algo2.min.js" , function(){
												$( "head" ).append( '<link rel="stylesheet" href="' + gDir + '/jpanel_algo2.css" type="text/css" />' );
												$.getScript( "https://www.gstatic.com/charts/loader.js" );
												jpanelAlgo( gInitObj , gDir );
											} );
										} , 100 );
									} );
								} );
							} );
						} );
					} );
				} );
			} );
		}else if ( gOnAlgo >= 0 ){
			$.getScript( gDir + "/jpanel_algo_symbol.js" , function(){
				$.getScript( gDir + "/jpanel_algo_parameter.js" , function(){
					$.getScript( gDir + "/jpanel_algo_config.js" , function(){
						$.getScript( gDir + "/jpanel_algo_variable.js" , function(){
							$( "#jPanelAlgoDiv" ).load( gDir + "/jpanel_algo.html" , function(){
								$.get( gDir + "/jpanel_algo_table.html" , function ( data ) {
									$( "body" ).prepend( data );
									setTimeout( function (){
										$.getScript( gDir + "/jpanel_algo.js" , function(){
											$( "head" ).append( '<link rel="stylesheet" href="' + gDir + '/jpanel_algo.css" type="text/css" />' );
											jpanelAlgo( gInitObj , gDir );
										} );
									} , 100 );
								} );
							} );
						} );
					} );
				} );
			} );
		}
		if ( gOnMsg >= 0 ){
			$( "#jPanelMsgDiv" ).append( '<div class="jPanelMsg"></div>' );
			$( 'head' ).append( '<link rel="stylesheet" href="' + gDir + '/jpanel_msg.css" type="text/css" />' );
			$.getScript( gDir + "/jpanel_msg.js" , function(){
				jpanelMSG( gUserID , showBadgeNotice );
			} );
		}
		if ( gOnLeader >= 0 ){
			$.get( gDir + "/jpanel_leader.html" , function ( data ) {
				$( "#ob_container" ).prepend( data );
				$( 'head' ).append( '<link rel="stylesheet" href="' + gDir + '/jpanel_leader.css" type="text/css" />' );
				$.getScript( gDir + "/jpanel_leader.js" , function(){
					jpanelGetLeader( gInitObj );
				} );
			} );
		}
		if ( gOnCS >= 0 ){
			$( "#jPanelCSDiv" ).load( gDir + "/jpanel_cs.html" , function(){
				$( 'head' ).append( '<link rel="stylesheet" href="' + gDir + '/jpanel_cs.css" type="text/css" />' );
				$.getScript( gDir + "/jpanel_cs.js" , function(){
					jpanelCS( gInitObj , gDir );
				} );
			} );
		}
		if ( gOnTrigNotice > 0 ){
			$( "#ob_container" ).append( "<div class='jNoticeDiv' id='jPanelNtcDiv'><div class='jNoticeIcon'><span class='jNoticeSym'></span></div><div class='jNoticeMsg' id='jPanelNtcMsg'></div></div>" ) // append notice box.
		}

		var showBadgeNotice = function ( panelID ){ // show message count on tab badge
			if ( gOnBadge == -1 ){
				return;
			}
			if ( $( panelID ).hasClass( "active" ) ){
				return;
			}
			var badgeSp = $( panelID ).find( ".badgeSp" );
			var msgNum = $( badgeSp ).html();
			msgNum ++;
			$( badgeSp ).text( msgNum );
			$( badgeSp ).show();
			$( badgeSp ).animate( {
				'padding-top' : 2 ,
				'padding-bottom' : 2 ,
				'padding-right' : 9 ,
				'padding-left' : 9 ,
			} , 100 );
			$( badgeSp ).animate( {
				'padding-top' : 1 ,
				'padding-bottom' : 1 ,
				'padding-right' : 7 ,
				'padding-left' : 7 ,
			} , 100 );
		}

		var showJPanelMsg = function ( msg ){
			if ( gOnMsg == -1 ){
				return;
			}
			gJMsgCount ++;
			var date = new Date();
			var currentTime = date.getHours() + ":" + ( '0' + ( date.getMonth() + 1 ) ).slice( -2 );
			$( '.jPanelMsg' ).prepend( "<div id='jMsgNum" + gJMsgCount + "'>" + msg + "<span class='jMsgTime'>" + currentTime + "</span></div>" );
			$( "#jMsgNum" + gJMsgCount ).addClass( "jMsgDiv" );
			$( "#jMsgNum" + gJMsgCount ).fadeIn( 500 );
			$( "#jMsgNum" + gJMsgCount ).animate( {
				backgroundColor: "#fff" ,
			} , 500 );
			if ( gJMsgCount > gJMsgMaxNum ){
				$( "#jMsgNum" + ( gJMsgCount - gJMsgMaxNum ) ).remove();
			}
		}

		function chgHeight( adjustedHeight ){
			$( ".jPanelTab" ).height( adjustedHeight + 10 ); // change jPanel height
		}

		function clearBadge(){
			if ( $( this ).attr( "id" ) == "jpanel_algo" ){
				return;
			}
			$( this ).find( ".badgeSp" ).removeClass( "bgColorNotice" );
			$( this ).find( ".badgeSp" ).html( 0 );
			$( this ).find( ".badgeSp" ).fadeOut( 200 );
		}
	}
} );