// global variable //
var gByMacro = 1;
var gByManual = 7;
var gByFunction = 8;
var gMinSecToSec = 1000;
var gRandomNum = 0;
var gMaxProfit = 0;
var gGuaSlowFedCount = 0;
var gGuaMarketDepthCount = 0;
var gTickCount = 0;
var gOrdType = gDefOrdType;
var gOrdTIF = gDefOrdTIF;
var gChartOption;
var gChartObj;
var gChartData;
var gSymOpt_MREX = gSym_CME;
var gSymOpt_PATS = gSym_CME;
var gSymOpt_PATX = gSym_CME;
var gSymOpt_HKE = gSym_HKE;
var gSessionIdAry = [];
var gQuoteAry = [];
var gMAQuoteAry = [];
var gTableMsgAry = [];
var gCauseChartDataAry = [];
var gModeOrderAry = [];
var gBidOrderAry = [];
var gAskOrderAry = [];
var gModeActionAry = [];

var gModeOrderParmAryObj = {
	bid: [] ,
	ask: [] ,
};
var gLeaderObj = {
	id: 0 ,
	ip: 0 ,
	on: false ,
	isLeader: false ,
	replied: false ,
};
var gStatusObj = {
	ip: 0 ,
	sessionId: 0 ,
	sequenceNum: 0 ,
	actionNum: 0 ,
	hostName: window.location.hostname ,
	inited: false ,
	reloaded: false ,
	disconnected: false ,
	algoRunning: false ,
	askingSessId: false ,
	gettingSessId: false ,
	callingLeader: false ,
	gotParameter: false ,
	gotCMR: false ,
	gotWrongQuote: false ,
	completedLoad: false ,
	countingTickVol: false ,
};
var gAlgoStatusObj = {
	on: false ,
	pause: false ,
	bidOn: true ,
	askOn: true ,
	bidAuto: true ,
	askAuto: true ,
	bidPause: false ,
	askPause: false ,
} ;
var gIconObj = {
	doubleUp: "\u23EB" ,
	up: "\u2B06" ,
	down: "\u2B07" ,
	doubleDown: "\u23EC" ,
	minus: "\u2796" ,
	doubleExcMark: "\u203C" ,
	warnning: "\u26A0" ,
};
var gFAIconObj = {
	doubleUp: "&#xf102;" ,
	up: "&#xf062;" ,
	down: "&#xf063;" ,
	doubleDown: "&#xf103;" ,
	minus: "&#xf068;" ,
	doubleExcMark: "&#xf06a;" ,
	warnning: "&#xf071;" ,
};
var gModeDefaultOrderObj = {
	update: 2 ,
	replace: 10 ,
	method: "MA" ,
	methodSec: 60 ,
	qty: 1 ,
	pips: 10 ,
};
var gModeAxisNumObj = {
	ShiftNum: 7 ,
	SpreadNum: 5 ,
	NoneNum: 5 ,
};
var gModePositionObj = {
	bidShift: 0 ,
	bidSpread: 0 ,
	askShift: 0 ,
	askSpread: 0 ,
};
var gMacroCount = {
	bid: [] ,
	ask: [] ,
};
var gModePauseTimer = {
	bid: "" ,
	ask: "" ,
};
var gCauseAryObj = {};
var gIntervalObj = {
	panelShort: false ,
	panelLong: false ,
	panelMiddle: false ,
	getLeaderID: false ,
	checkParm: false ,
	getClientID: false ,
	flatPosition: false ,
	reloadCount: false ,
	waitCMR: false ,
	storeHighLow: false ,
	storeCauseData: false ,
	drawDataChart: false ,
};
var gTimerObj = {
	getBadgeNum: false ,
	delayTurnOn: false ,
	autoFlat: false ,
	readTotalFill: false ,
	replyLeader: false ,
	sendSessId: false ,
	waitLeaderOn: false ,
	pausePanel: false ,
	guaSlowFed: false ,
	clearQuoteData: false ,
	flatPosition: false ,
	reloadPanel: false ,
	readQuoteAgain: false ,
	bidRunMacro: false ,
	askRunMacro: false ,
	modeVolumeSignal: false ,
	modeVolatilitySignal: false ,
	modeFilledTwiceSignal: false ,
	modeMASpreadSignal: false ,
	modeAiDirectionSignal: false ,
	modeAiVolatilitySignal: false ,
	modeAiPVRSignal: false ,
	modeAiBatt2: false ,
	modeAiBatt3: false ,
};