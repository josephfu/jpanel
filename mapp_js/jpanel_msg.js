function jpanelMSG( gUserID,showBadgeNotice ){
	var gMsgCount = 0;
	var gMsgMaxNum = 300;

	function readCMR( cmrMsg ){
		var cmrAry = cmrMsg.split( "\n" );
		for( i = 0 ; i < cmrAry.length ; i++ ){
			var cmrAryWs = cmrAry[i].split( " " );
			if( cmrAryWs.length < 4 || cmrAryWs[0] != "CMR" || cmrAryWs[1] != gUserID ||  cmrAryWs[2] != "MSG" ){
				return;
			}
			if( cmrAryWs[3] == "MMALGO" || cmrAryWs[3] == "a_tabMsg" ){
				showMsg( cmrAryWs );
			}
		}
	}
	function showMsg( cmrAryWs ){
		var msg = "";
		for( i = 4 ; i < cmrAryWs.length ; i++ ){
			msg = msg + " " + cmrAryWs[ i ];
		}
		gMsgCount ++;
		var date = new Date();
		var currentTime = date.getHours() + ":" + ( "0" + ( date.getMinutes() ) ).slice( -2 );
		$( ".jPanelMsg" ).prepend( "<div id='jMsgNum" + gMsgCount +	"'>" + msg + "<span class='jMsgTime'>" + currentTime + "</span></div>" );
		$( "#jMsgNum" + gMsgCount ).addClass( "jMsgDiv" );
		$( "#jMsgNum" + gMsgCount ).fadeIn( 500 );
		if ( gMsgCount > gMsgMaxNum ){
			$( "#jMsgNum" + ( gMsgCount - gMsgMaxNum ) ).remove();
		}
		showBadgeNotice( "#jpanel_msg" );
	}
	ee.addListener( "MC_CMR", readCMR );
}