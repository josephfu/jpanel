function jpanelHSI(uid){
	// default setting
	var defUserID = uid;
	var defECN = "ECN";
	var defSym = "";
	var defSide = "BUY";
	var defTgrPnt = 24000;
	var defStepDist = 130;
	var defTrlStep = 100;
	var defStopDist = 100;
	var defStepNum = 3;
	var defQtty = 1;
	var defSlip = 10;
	var defDur = "DAY";
	var defChtName = "HSICht";
	if (defSide == "BUY"){
		var defSideOpp = "SELL";
	}else{
		var defSideOpp = "BUY";
	}

	// hsiPanel function ///
	function drawChart(){
		d3.select("#HSICht_Svg").remove(); // remove original chart
		gOrdTgrPnt = parseFloat(document.getElementById("ordTgrPntCtrl").value); //get order trigger point
		var gLineAry = new Array();
		for (var i=0;i<=gOrdStepNum; i++){
			if(gOrdSide == "BUY"){
				triPoint = gOrdTgrPnt + gOrdStepDist*i;
			}else{
				triPoint = gOrdTgrPnt - gOrdStepDist*i;
			}
			if (i < gOrdStepNum){
				gLineAry[i] = { chtOrdType:"SO2",chtOrdSide:gOrdSide,chtOrdPx:triPoint,chtOrdStopPx:gOrdStopDist};
			}else{
				gLineAry[i] = { chtOrdType:"LO2",chtOrdSide:gOrdSideOpp,chtOrdPx:triPoint,chtOrdStopPx:gOrdStopDist};
			}
		}
		for (var i=0;i<=gOrdStepNum; i++) {
			if (gOrdSide == "BUY"){
				ordPx = gOrdTgrPnt + (gOrdStepDist * i);
				ordStopPx = ordPx - gOrdStopDist;
			}else{
				ordPx = gOrdTgrPnt - (gOrdStepDist * i);
				ordStopPx = ordPx + gOrdStopDist;
			}
			if ( i < gOrdStepNum ){
				ordQtty = gOrdQtty;
				gOrdAry[i] = { sendOrdType: gLineAry[i].chtOrdType , sendOrdSide: gLineAry[i].chtOrdSide , sendOrdPx: ordPx , sendOrdQtty: ordQtty , sendOrdDur: defDur , sendOrdSlip: defSlip , sendOrdTrailStep: gOrdTrlStep , sendStopOrdType: gLineAry[i].chtOrdType , ordStepHndr: gOrdSideOpp , sendStopOrdPx: ordStopPx , sendStopOrdQtty: ordQtty , sendStopOrdDur: defDur , sendStopOrdSlip: defSlip , sendStopOrdTrailStep: gOrdTrlStep};
			}else{
				ordQtty = gOrdQtty*gOrdStepNum;
				gOrdAry[i] = { sendOrdType: gLineAry[i].chtOrdType , sendOrdSide: gLineAry[i].chtOrdSide , sendOrdPx: ordPx , sendOrdQtty: ordQtty , sendOrdDur: defDur , sendOrdSlip: defSlip , sendOrdTrailStep: gOrdTrlStep};
			}
		}
		if (gOrdSide == "BUY"){
			createCht(defChtName,2,gLineAry);
		}else{
			createCht(defChtName,1,gLineAry);
		}
	}
	function chgECN(control){ //change ECN
		gOrdECN = control.value;
	}
	function chgSide(control){ //change BUY/SELL side`
		gOrdSide = control.value;
		if (control.value == "BUY"){
			gOrdSideOpp = "SELL";
		}else{
			gOrdSideOpp = "BUY";
		}
	}
	function chgDur(control){ //change duration
		gOrdAry[ gLineNum - 1].sendOrdDur = control.value;
	}
	function chgStopDur(control){ //change stop order duration`
		gOrdAry[ gLineNum - 1].sendStopOrdDur = control.value;
	}
	function setOrdDetl(lineID){// set order detail
		if (d3.select("#" + lineID).attr("ordClass") == "pnt"){
			lineID1 = lineID;
			lineID2 = d3.select("#" + lineID).attr("oppLineID");
		}else{
			lineID1 = d3.select("#" + lineID).attr("oppLineID");
			lineID2 = lineID;
		}
		gLineNum = d3.select("#" + lineID1).attr("lineNum");
		var IDArrayLine1 = getIDAry(lineID1);
		if (d3.select("#" + IDArrayLine1.lineID).attr("ordType") == "SO2"){
			ordTypeStr = "Stop ";
			var IDArrayLine2 = getIDAry(lineID2);

			$('#stpTypeCtrl').text(ordTypeStr + d3.select("#" + IDArrayLine2.lineID).attr("ordSide"));
			$('#stpPxCtrl').val(d3.select("#" + IDArrayLine2.tipID).text());
			$('#stpCtrlDiv').show();
			document.getElementById("stpDurCtrl").value = gOrdAry[gLineNum - 1].sendStopOrdDur;

			$("#stpDurCtrl").val(gOrdAry[ gLineNum - 1].sendStopOrdDur);
			$("#stpQttySldr").slider( "value",gOrdAry[gLineNum - 1].sendStopOrdQtty);
			$("#stpQttyCtrl").text( gOrdAry[gLineNum - 1].sendStopOrdQtty);
			$("#stpSlipSldr").slider( "value",gOrdAry[gLineNum - 1].sendStopOrdSlip);
			$("#stpSlipCtrl").text( gOrdAry[gLineNum - 1].sendStopOrdSlip);
			
			$(".detlRmBtnDiv").show();
		}else if (d3.select("#" + IDArrayLine1.lineID).attr("ordType") == "LO2"){
			ordTypeStr = "Limit ";
			$('#stpCtrlDiv').hide();
			$(".detlRmBtnDiv").hide();
		}
		$("#etyDurCtrl").val(gOrdAry[ gLineNum - 1].sendOrdDur);
		$("#etyQttySldr").slider( "value",gOrdAry[gLineNum - 1].sendOrdQtty);
		$("#etyQttyCtrl").text( gOrdAry[gLineNum - 1].sendOrdQtty);
		$("#etySlipSldr").slider( "value",gOrdAry[gLineNum - 1].sendOrdSlip);
		$("#etySlipCtrl").text( gOrdAry[gLineNum - 1].sendOrdSlip);
		$("#jPanelHSI").animate({width:"700px"},200);
		$('#setDetlDiv').delay(200).show(300);
		$('#etyTypeCtrl').text(ordTypeStr + d3.select("#" + IDArrayLine1.lineID).attr("ordSide"));
		$('#etyPxCtrl').val(d3.select("#" + IDArrayLine1.tipID).text());
	}
	function sendOrd(){ //generate CMD message
		var macroCMD1 = "CMD " + gUserID + " ";
		for (i = 0; i < gOrdAry.length; i++) {
			if (gOrdAry[i] !== ""){
				if ( i > 0 ){
					macroCMD1 = macroCMD1 + "|";
				}
				var cmdECN = "ECN"
				if (gOrdECN !== ""){
					cmdECN = gOrdECN;
				}
				var cmdSym = "SYM";
				if (gOrdSym !== ""){
					var cmdSym = gOrdSym;
				}
				if (gOrdAry[i].sendOrdType =="SO2"){
					macroCMD1 = macroCMD1 + "O " + cmdECN + " " + gOrdAry[i].sendOrdType + " " + gOrdAry[i].sendOrdDur + " " + gOrdAry[i].sendOrdSide + " " + cmdSym + " QTY " + gOrdAry[i].sendOrdQtty + " TRIG " + gOrdAry[i].sendOrdPx + " SLIP " + gOrdAry[i].sendOrdSlip +	 " TS 1" + "|O " + cmdECN + " " + gOrdAry[i].sendStopOrdType + " " + gOrdAry[i].sendStopOrdDur + " " + gOrdAry[i].ordStepHndr + " " + cmdSym + " QTY " + gOrdAry[i].sendStopOrdQtty + " TRIG " + gOrdAry[i].sendStopOrdPx + " SLIP " + gOrdAry[i].sendStopOrdSlip + " TS 1";
				}else if (gOrdAry[i].sendOrdType =="LO2"){
					macroCMD1 = macroCMD1 + "O " + cmdECN + " " + gOrdAry[i].sendOrdType + " " + gOrdAry[i].sendOrdDur + " " + gOrdAry[i].sendOrdSide + " " + cmdSym + " QTY " + gOrdAry[i].sendOrdQtty + " AT " + gOrdAry[i].sendOrdPx + " SLIP " + gOrdAry[i].sendOrdSlip;
				}
			}
		}
		macroCMD2 = "CMD " + gUserID + " O ECN SYM GUI_BID GUI_ASK";
		console.log(macroCMD1);
		console.log(macroCMD2);
		orderSubscription.push(macroCMD1);
		orderSubscription.push(macroCMD2);
		showHSINotice();
	}
	function showHSINotice(){ // show hsi notification
		$("#jNoticeHSIDiv").clearQueue().stop(true,false); // clear animate
		$("#jNoticeHSIMsg").clearQueue().stop(true,false);
		$("#jNoticeHSIDiv").hide();
		$("#jNoticeHSIMsg").css({left:"6px"});
		$("#jNoticeHSIMsg").html("Orders have been sent.");
		$("#jNoticeHSIDiv").fadeIn(400);
		$("#jNoticeHSIMsg").animate({left:"40px"},1000);
		$("#jNoticeHSIDiv").delay(1200).fadeOut(400);
	}

	//// hsiPanel d3 function ///
	function createCht(chtID,chtType,gLineAry){
		gChtScale = ((gOrdStepDist*gOrdStepNum - 500) / 170) + 3;
		if (chtType == 1){
			gChtMin = (-200*gChtScale) + gOrdTgrPnt;
			gChtMax = (100*gChtScale) + gOrdTgrPnt;
		}else{
			gChtMin = (-100*gChtScale) + gOrdTgrPnt;
			gChtMax = (200*gChtScale) + gOrdTgrPnt;
		}
		var yScale = d3.scale.linear()
			.domain([gChtMin, gChtMax])
			.range([247, 0]);
		var yAxis = d3.svg.axis()
			.scale(yScale)
			.ticks(5)
			.orient("left");
		var graph = d3.select("#"+chtID)
			.append("svg")
			.attr("id", chtID+"_Svg")
			.attr("width", 270)
			.attr("height", 270)
			.call(zoomEvent)
			.on("mousedown.zoom", null) //prevent dragging line
			.on("touchstart.zoom", null)
			.on("touchmove.zoom", null)
			.on("touchend.zoom", null)
			.append("svg:g")
			.attr("transform", "translate(50,10)");
		graph.append("svg:g")
			.attr("class", "d3yAxis")
			.attr("transform", "translate(0,0)")
			.call(yAxis);
		var chtX = 10;
		for (i = 0; i < gLineAry.length; i++) {
			if (gLineAry[i].chtOrdSide == "BUY"){
				stopPx = gLineAry[i].chtOrdPx - gLineAry[i].chtOrdStopPx;
			}else{
				stopPx = gLineAry[i].chtOrdPx + gLineAry[i].chtOrdStopPx;
			}
			var chtY = convPxtoChtY(gLineAry[i].chtOrdPx,chtType);
			var chdChtY = convPxtoChtY(stopPx,chtType);
			var lineNum = i+1;
			if (gLineAry[i].chtOrdType == "SO2"){
				var oppLineID = chtID + 'Line' + lineNum + 'STOP';
				var lineClass = 'd3'+gLineAry[i].chtOrdSide + ' d3Line';
				var tipBgClass = 'd3TipBg d3TipBg'+ gLineAry[i].chtOrdSide;
				var tipClass = 'd3Tip d3Tip'+ gLineAry[i].chtOrdSide;
			}else if (gLineAry[i].chtOrdType == "LO2"){
				var oppLineID = "";
				var lineClass = 'd3LineTkPf d3Line';
				var tipBgClass = 'd3TipBg d3TipBgTkPf';
				var tipClass = 'd3Tip d3TipTkPf';
			}
			graph.append("rect") //draw line
				.attr('class',lineClass)
				.attr('id',chtID + 'Line' + lineNum)
				.attr("x",chtX)
				.attr("y",chtY)
				.attr("ordSide",gLineAry[i].chtOrdSide)
				.attr("ordType",gLineAry[i].chtOrdType)
				.attr("ordClass","pnt")
				.attr("oppLineID",oppLineID)
				.attr("chtType",chtType)
				.attr("chtID",chtID)
				.attr("lineNum",lineNum)
				.on("click", function(){
					sletLine( d3.select(this).attr("id"));
				});
			graph.append("rect") //draw tooltip background
				.attr("id", chtID + "TipBg" + lineNum)
				.attr('class',tipBgClass)
				.attr("x",chtX)
				.attr("y",d3.select("#" + chtID + "Line" + lineNum).attr("y")-17);
			graph.append("text") //draw tooltip text
				.attr("id", chtID + "Tip" + lineNum)
				.attr('class',tipClass)
				.attr("dy",d3.select("#" + chtID + "Line" + lineNum).attr("y")-6)
				.text((convChtYtoPx(d3.select("#" + chtID + "Line" + lineNum).attr("y"),d3.select("#" + chtID + "Line" + lineNum).attr("chtType"))))
				.attr("dx",parseFloat(d3.select("#" + chtID + "TipBg" + lineNum).attr("x"))+18);
			if (gLineAry[i].chtOrdType == "SO2"){
				graph.append("rect")
					.attr('class','d3LineStop')
					.attr('id',chtID + 'Line' + lineNum + 'STOP')
					.attr("x",chtX+75)
					.attr("y",chdChtY)
					.attr("ordSide",gOrdSideOpp)
					.attr("ordType",gLineAry[i].chtOrdType)
					.attr("ordClass","chd")
					.attr("oppLineID",chtID + 'Line' + lineNum)
					.attr("chtType",chtType)
					.attr("chtID",chtID)
					.attr("lineNum",lineNum)
					.on("click", function(){
						sletLine( d3.select(this).attr("id"));
					});
				graph.append("rect")
					.attr("id", chtID + "TipBg" + lineNum + "STOP")
					.attr('class','d3TipBg d3TipBgStop')
					.attr("x",chtX+90)
					.attr("y",d3.select("#" + chtID + "Line" + lineNum+"STOP").attr("y")-17);
				graph.append("text")
					.attr("id", chtID + "Tip" + lineNum + "STOP")
					.attr('class','d3Tip d3TipStop')
					.attr("dy",d3.select("#" + chtID + "Line" + lineNum + "STOP").attr("y")-6)
					.text((convChtYtoPx(d3.select("#" + chtID + "Line" + lineNum + "STOP").attr("y"),d3.select("#" + chtID + "Line" + lineNum + "STOP").attr("chtType"))))
					.attr("dx",parseFloat(d3.select("#" + chtID + "TipBg" + lineNum + "STOP").attr("x"))+18);
			}
			if (gOrdStepNum < 4){
				chtX = chtX + 30;
			}else{
				chtX = chtX + (100/gOrdStepNum);
			}
		}
		d3.selectAll(".d3Line").each( //sort line
			function(){
				var IDArray = getIDAry(this.id);
				dom = document.getElementById(this.id);
				dom.parentNode.appendChild(dom);
				dom = document.getElementById(IDArray.tipBgID);
				dom.parentNode.appendChild(dom);
				dom = document.getElementById(IDArray.tipID);
				dom.parentNode.appendChild(dom);
			}
		)
	}
	function dragClass(){
		var IDArray = getIDAry(gSletedLineID);
		var ordClass = d3.select('#'+gSletedLineID).attr("ordClass");
		if (ordClass == "pnt"){
			var classType1 = 1;
			var classType2 = 2;
		}else{
			var classType1 = 2;
			var classType2 = 1;
		}
		dragLine(IDArray,classType1);
		var oppLineID = d3.select('#'+IDArray.lineID).attr("oppLineID");
		if (oppLineID !== ""){
			var IDArray = getIDAry(oppLineID);
			dragLine(IDArray,classType2);
		}
	}
	function dragLine(IDArray,classType){
		var ordSide = d3.select('#'+IDArray.lineID).attr("ordSide");
		var ordType = d3.select('#'+IDArray.lineID).attr("ordType");
		var chtType = d3.select('#'+IDArray.lineID).attr("chtType");
		var dy = d3.event.dy;
		var chtY = parseFloat(d3.select('#' + IDArray.lineID).attr('y'))+ dy;
		var newOrdPx = convChtYtoPx(chtY,chtType);
		var chtPx = newOrdPx;
		if(classType == 2){
			var pntLineID = d3.select('#'+IDArray.lineID).attr("oppLineID");
			var pntChtY = parseFloat(d3.select('#' + pntLineID).attr('y'))+ dy;
			chtPx = convChtYtoPx(pntChtY,chtType);
		}
		var tipY = chtY - 6;
		var tipBgY = chtY - 17;
		if ((((ordType == "LO2" && ordSide == "BUY") || (ordType == "SO2" && ordSide == "SELL"))) || (((ordType == "LO2" && ordSide == "SELL") || (ordType == "SO2" && ordSide == "BUY")))){
			if ((chtType==1 && chtPx <= gChtMax && chtPx >= gChtMin) || (chtType==2 && chtPx <= gChtMax && chtPx >= gChtMin)){
				d3.select('#' + IDArray.lineID)
					.attr("y",chtY);
				if ( newOrdPx > (gChtMax * 0.999) ){
					tipY += 30;
					tipBgY += 30;
					d3.select('#' + IDArray.tipID)
						.transition().duration(100)
						.text(newOrdPx)
						.attr("dy",tipY);
					d3.select('#' + IDArray.tipBgID)
						.transition().duration(100)
						.attr("y",tipBgY);
				}else{
					d3.select('#' + IDArray.tipID)
						.text(newOrdPx)
						.attr("dy",tipY);
					d3.select('#' + IDArray.tipBgID)
						.attr("y",tipBgY);
				}
				if (d3.select("#" + IDArray.lineID).attr("ordClass") == "pnt"){
					$('#etyPxCtrl').val(newOrdPx);
					gOrdAry[ gLineNum - 1].sendOrdPx = newOrdPx;
				}else{
					$('#stpPxCtrl').val(newOrdPx);
					gOrdAry[ gLineNum - 1].sendStopOrdPx = newOrdPx;
				}
			}
		}
	}
	function zoomClass() {
		if (gSletedChtID !== ""){
			var zmScale = d3.event.scale;
			var IDArray = getIDAry(gSletedLineID);
			zoomLine(IDArray,zmScale);

			var childLineID = d3.select('#'+IDArray.lineID).attr("oppLineID");
			if (childLineID !== ""){
				var IDArray = getIDAry(childLineID);
				zoomLine(IDArray,zmScale);
			}
			gChtZoom = d3.event.scale;
		}
	}
	function zoomLine(IDArray,zmScale) {
		var ordSide = d3.select('#' + IDArray.lineID).attr("ordSide");
		var ordType = d3.select('#' + IDArray.lineID).attr("ordType");
		var chtType = d3.select('#' + IDArray.lineID).attr("chtType");
		var lineY = parseFloat(d3.select('#' + IDArray.lineID).attr('y'));
		var tipY = parseFloat(d3.select('#' + IDArray.tipID).attr('dy'));
		var tipText = parseFloat(d3.select('#' + IDArray.tipID).text());
		var tipBgY = parseFloat(d3.select('#' + IDArray.tipBgID).attr('y'));
		if ( zmScale > gChtZoom){
			lineY -= 0.82/gChtScale;
			tipY -= 0.82/gChtScale;
			tipText += 1;
			tipBgY -= 0.82/gChtScale;
		}else{
			lineY += 0.82/gChtScale;
			tipY += 0.82/gChtScale;
			tipText -= 1;
			tipBgY += 0.82/gChtScale;
		}
		if ((((ordType == "LO2" && ordSide == "BUY") || (ordType == "SO2" && ordSide == "SELL"))) || (((ordType == "LO2" && ordSide == "SELL") || (ordType == "SO2" && ordSide == "BUY")))){
			if ((chtType==1 && tipText <= gChtMax && tipText >= gChtMin) || (chtType==2 && tipText <= gChtMax && tipText >= gChtMin)){
				var yValue = convChtYtoPx(lineY,chtType);
				d3.select('#' + IDArray.lineID)
				.attr("y",lineY)
				d3.select('#' + IDArray.tipID)
				.text(tipText)
				.attr("dy",tipY);
				d3.select('#' + IDArray.tipBgID)
				.attr("y",tipBgY);
				if (d3.select("#" + IDArray.lineID).attr("ordClass") == "pnt"){
					$('#etyPxCtrl').val(tipText);
					gOrdAry[ gLineNum - 1].sendOrdPx = tipText;
				}else{
					$('#stpPxCtrl').val(tipText);
					gOrdAry[ gLineNum - 1].sendStopOrdPx = tipText;
				}
			}
		}
	}
	function sletLine(lineID) {
		var IDArray = getIDAry(lineID);
		d3.select("#" + lineID).call(dragEvent);
		gSletedChtID = d3.select("#" + lineID).attr("chtID");
		gSletedLineID = lineID;
		sletedLineSide = d3.select("#"+ lineID).attr("ordSide");
		setOrdDetl(lineID);
		if (d3.select("#" + lineID).attr("ordClass") == "chd"){
			IDArray = getIDAry(d3.select("#" + lineID).attr("oppLineID"));
		}

		lineSide = d3.select("#"+ IDArray.lineID).attr("ordSide");
		d3.selectAll(".d3SletedLine").each(
			function(){
				d3.select(this).classed("d3SletedLine",false);
				d3.select(this).classed("d3SletedLineBUY",false);
				d3.select(this).classed("d3SletedLineSELL",false);
			}
		);
		d3.selectAll(".d3SletedTipBg").each(
			function(){
				d3.select(this).classed("d3SletedTipBg",false);
				d3.select(this).classed("d3SletedTipBgBUY",false);
				d3.select(this).classed("d3SletedTipBgSELL",false);
			}
		);
		d3.selectAll(".d3SletedTip").each(
			function(){
				d3.select(this).classed("d3SletedTip",false);
			}
		);
		if (d3.select("#" + IDArray.lineID).classed("d3SletedLine") == false){
			d3.select("#" + IDArray.lineID).classed("d3SletedLine", true);
			d3.select("#" + IDArray.tipBgID).classed("d3SletedTipBg", true);
			d3.select("#" + IDArray.tipID).classed("d3SletedTip", true);
			if (d3.select("#" + IDArray.lineID).attr("ordType") == "SO2"){
				d3.select("#" + IDArray.lineID).classed("d3SletedLine"+lineSide, true);
				d3.select("#" + IDArray.tipBgID).classed("d3SletedTipBg"+lineSide, true);
			}else if (d3.select("#" + IDArray.lineID).attr("ordType") == "LO2"){
				d3.select("#" + IDArray.lineID).classed("d3SletedLineTkPf", true);
				d3.select("#" + IDArray.tipBgID).classed("d3SletedTipBgTkPf", true);
			}
			dom = document.getElementById(IDArray.lineID);
			dom.parentNode.appendChild(dom);
			dom = document.getElementById(IDArray.tipBgID);
			dom.parentNode.appendChild(dom);
			dom = document.getElementById(IDArray.tipID);
			dom.parentNode.appendChild(dom);
		}
	}
	function convChtYtoPx(chtY,chtType){ //convert chart Y value to order price
		chtY = parseFloat(chtY);
		if (chtType == 1){
			var px = Math.round((-(chtY-82.5)*1.212*gChtScale) + gOrdTgrPnt);
		}else{
			var px = Math.round(((-chtY+165)*1.212*gChtScale)+ gOrdTgrPnt);
		}
		return px;
	}
	function convPxtoChtY(px,chtType){ //convert order price to chart Y value
		px = parseFloat(px);
		if (chtType == 1){
			var yDomainMax = (100*gChtScale) + gOrdTgrPnt;
		}else{
			var yDomainMax = (200*gChtScale) + gOrdTgrPnt;
		}
		var chtY = (yDomainMax - px) / 1.212 / gChtScale;
		return chtY;
	}
	function getIDAry(lineID) {
		var chtID = d3.select('#'+lineID).attr("chtID");
		var lineNum = d3.select('#'+lineID).attr("lineNum");
		var tipID = chtID + "Tip" + lineNum;
		var tipBgID = chtID + "TipBg" + lineNum;
		var ordClass = d3.select('#'+lineID).attr("ordClass");
		if (ordClass == "chd"){
			tipID = tipID + "STOP";
			tipBgID = tipBgID + "STOP";
		}
		return {
			lineID: lineID,
			tipID: tipID,
			tipBgID: tipBgID
		};
	}
	function chgPx(control,lineType){
		var newPx = parseFloat(control.value);
		if (newPx > gChtMax){
			newPx = Math.round(gChtMax);
		}
		if (newPx < gChtMin){
			newPx = Math.round(gChtMin);
		}
		var IDArray = getIDAry(gSletedLineID);
		var ordClass = d3.select('#'+IDArray.lineID).attr("ordClass");
		var oppLineID = d3.select('#'+IDArray.lineID).attr("oppLineID");
		if (ordClass == "pnt" && lineType == 2){
			IDArray = getIDAry(oppLineID);
		}
		var chtType = d3.select('#'+IDArray.lineID).attr("chtType");
		var chtY = convPxtoChtY(newPx,chtType);
		d3.select('#' + IDArray.lineID)
			.attr("y",chtY);
		d3.select('#' + IDArray.tipID)
			.text(newPx)
			.attr("dy",chtY-6);
		d3.select('#' + IDArray.tipBgID)
			.attr("y",chtY-17);

		var ordType = d3.select('#'+IDArray.lineID).attr("ordType");
		var ordSide = d3.select('#'+IDArray.lineID).attr("ordSide");
		var oppLineID = d3.select('#'+IDArray.lineID).attr("oppLineID");
		if ( ordType == "SO2" ){
			if ( (ordClass == "pnt" && ordSide == "BUY") || (ordClass == "chd" && ordSide == "BUY") ){
				newPx = newPx - gOrdStopDist;
			}else{
				newPx = newPx + gOrdStopDist;
			}
			var IDArray = getIDAry(oppLineID);
			var chtY = convPxtoChtY(newPx,chtType);
			d3.select('#' + IDArray.lineID)
				.attr("y",chtY);
			d3.select('#' + IDArray.tipID)
				.text(newPx)
				.attr("dy",chtY-6);
			d3.select('#' + IDArray.tipBgID)
				.attr("y",chtY-17);
		}
	}

	///// jquery event
	$('#ordECNCtrl').change(function() {chgECN(this);});
	$('#ordSideCtrl').change(function() {chgSide(this);});
	$('#etyPxCtrl').change(function() {chgPx(this,1);});
	$('#etyDurCtrl').change(function() {chgDur(this);});
	$('#drawChtBtn').click(function() {drawChart();});
	$('#sendOrdBtn').dblclick(function() {sendOrd();});
	$('#stpPxCtrl').change(function() {chgPx(this,2);});
	$('#stpDurCtrl').change(function() {chgStopDur(this);});

	////// jpanel_hsi global variable ////
	var gOrdECN = defECN;
	var gOrdSym = defSym;
	var gOrdTgrPnt = defTgrPnt;
	var gOrdStepDist = defStepDist;
	var gOrdTrlStep = defTrlStep;
	var gOrdStopDist = defStopDist;
	var gOrdStepNum = defStepNum;
	var gOrdQtty = defQtty;
	var gOrdSide = defSide;
	if (gOrdSide == "BUY"){
		var gOrdSideOpp = "SELL";
	}else{
		var gOrdSideOpp = "BUY";
	}
	var gUserID = defUserID;
	var gLineNum = 0;
	var gOrdAry = new Array();
	for (var i=0;i<=defStepNum; i++) { // default order array
		if (defSide == "BUY"){
			ordPx = defTgrPnt + (defStepDist * i);
		}else{
			ordPx = defTgrPnt - (defStepDist * i);
		}
		if ( i < defStepNum ){
			ordQtty = defQtty;
			if ( defSide == "BUY"){
				ordStopPx = ordPx - defStopDist;
			}
			else{
				ordStopPx = ordPx + defStopDist;
			}
			gOrdAry[i] = { sendOrdType: "SO2" , sendOrdSide: defSide , sendOrdPx: ordPx , sendOrdQtty: ordQtty , sendOrdDur: defDur , sendOrdSlip: defSlip , sendOrdTrailStep: defTrlStep , sendStopOrdType: "SO2" , ordStepHndr: defSideOpp , sendStopOrdPx: ordStopPx , sendStopOrdQtty: ordQtty , sendStopOrdDur: defDur , sendStopOrdSlip: defSlip , sendStopOrdTrailStep: defTrlStep};
		}
		else{
			ordQtty = defQtty*defStepNum;
			gOrdAry[i] = { sendOrdType: "LO2" , sendOrdSide: defSideOpp , sendOrdPx: ordPx , sendOrdQtty: ordQtty , sendOrdDur: defDur , sendOrdSlip: defSlip , sendOrdTrailStep: defTrlStep};
		}
	}

	///// d3 Object ////
	var gChtZoom = 1;
	var gChtZoomTime = 1;
	var gChtScale = 3;
	var gSletedChtID = "";
	var gSletedLineID = "";
	var gChtMin = 0;
	var gChtMax = 0;
	var dragEvent = d3.behavior.drag().on('drag', dragClass);
	var zoomEvent = d3.behavior.zoom().on('zoom', zoomClass);

	gLineAry = new Array();	// default line array for d3 chart
	for (var i=0;i<=defStepNum; i++) {
		if(defSide == "BUY"){
			triPoint = defTgrPnt + defStepDist*i;
		}else{
			triPoint = defTgrPnt -(defStepDist*i);
		}
		if (i < defStepNum){
			gLineAry[i] = { chtOrdType:"SO2",chtOrdSide:defSide,chtOrdPx:triPoint,chtOrdStopPx:defStopDist};
		}else{
			gLineAry[i] = { chtOrdType:"LO2",chtOrdSide:defSideOpp,chtOrdPx:triPoint,chtOrdStopPx:defStopDist};
		}
	}

	// set control value to default value
	$("#ordECNCtrl").val(defECN);
	$("#ordSideCtrl").val(defSide);
	$("#ordTgrPntCtrl").val(defTgrPnt);

	// create default d3 chart
	if (defSide == "BUY"){
		createCht(defChtName,2,gLineAry);
	}else{
		createCht(defChtName,1,gLineAry);
	}

	// create silder handler
	var ordStepHndr = $( "#ordStepCtrl" );
	$( "#ordStepSldr" ).slider({
		min: 1,
		max: 10,
		value: gOrdStepNum,
		create: function() {
			ordStepHndr.text( $( this ).slider( "value" ) );
		},
		slide: function( event, ui ) {
			ordStepHndr.text( ui.value) ;
			gOrdStepNum = ui.value;
		}
	});
	var ordStepDistHndr = $( "#ordStepDistCtrl" );
	x = 10;
	$( "#ordStepDistSldr" ).slider({
		min: 10/x,
		max: 500/x,
		value: gOrdStepDist/x,
		create: function() {
			ordStepDistHndr.text( ($( this ).slider( "value" )*x) );
		},
		slide: function( event, ui ) {
			ordStepDistHndr.text( ui.value * x) ;
			gOrdStepDist = ui.value * x;
		}
	});
	var ordTrlStepHndr = $( "#ordTrlStepCtrl" );
	x = 10;
	$( "#ordTrlStepSldr" ).slider({
		min: 10/x,
		max: 500/x,
		value: gOrdTrlStep/x,
		create: function() {
			ordTrlStepHndr.text( ($( this ).slider( "value" )*x) );
		},
		slide: function( event, ui ) {
			ordTrlStepHndr.text( ui.value * x) ;
			gOrdTrlStep = ui.value * x;
		}
	});
	var ordStopDistHndr = $( "#ordStpDistCtrl" );
	x = 10;
	$( "#ordStpDistSldr" ).slider({
		min: 10/x,
		max: 500/x,
		value: gOrdStopDist/x,
		create: function() {
			ordStopDistHndr.text( ($( this ).slider( "value" )*x) );
		},
		slide: function( event, ui ) {
			ordStopDistHndr.text( ui.value * x) ;
			gOrdStopDist = ui.value * x;
		}
	});
	var ordQttyHndr = $( "#ordQttyCtrl" );
	$( "#ordQttySldr" ).slider({
		min: 1,
		max: 10,
		value: gOrdQtty,
		create: function() {
			ordQttyHndr.text( $( this ).slider( "value" ) );
		},
		slide: function( event, ui ) {
			ordQttyHndr.text( ui.value) ;
			gOrdQtty = ui.value;
		}
	});
	var entQttyHndr = $( "#etyQttyCtrl" );
	$( "#etyQttySldr" ).slider({
		min: 1,
		max: 30,
		value: defQtty,
		create: function() {
			entQttyHndr.text( $( this ).slider( "value" ) );
		},
		slide: function( event, ui ) {
			entQttyHndr.text( ui.value);
			gOrdAry[ gLineNum - 1].sendOrdQtty = ui.value;
		}
	});
	var enySlipHndr = $( "#etySlipCtrl" );
	$( "#etySlipSldr" ).slider({
		min: 1,
		max: 30,
		value: defSlip,
		create: function() {
			enySlipHndr.text( $( this ).slider( "value" ) );
		},
		slide: function( event, ui ) {
			enySlipHndr.text( ui.value);
			gOrdAry[ gLineNum - 1].sendOrdSlip = ui.value;
		}
	});
	var stpQttyHndr = $( "#stpQttyCtrl" );
	$( "#stpQttySldr" ).slider({
		min: 1,
		max: 30,
		value: defQtty,
		create: function() {
			stpQttyHndr.text( $( this ).slider( "value" ) );
		},
		slide: function( event, ui ) {
			stpQttyHndr.text( ui.value);
			gOrdAry[ gLineNum - 1].sendStopOrdQtty = ui.value;
		}
	});
	var stpSlipHndr = $( "#stpSlipCtrl" );
	$( "#stpSlipSldr" ).slider({
		min: 1,
		max: 30,
		value: defSlip,
		create: function() {
			stpSlipHndr.text( $( this ).slider( "value" ) );
		},
		slide: function( event, ui ) {
			stpSlipHndr.text( ui.value);
			gOrdAry[ gLineNum - 1].sendStopOrdSlip = ui.value;
		}
	});
	$("#rmvBtn").click(function(){
		rmChtLine();
	});
	function rmChtLine(){ // delete a line on chart
		var oppLineID = d3.select("#"+gSletedLineID).attr("oppLineID");
		var lineNum = d3.select("#"+gSletedLineID).attr("lineNum");
		
		var IDArray = getIDAry(gSletedLineID);
		d3.select("#"+IDArray.lineID).remove();
		d3.select("#"+IDArray.tipID).remove();
		d3.select("#"+IDArray.tipBgID).remove();
		
		var IDArray = getIDAry(oppLineID);
		d3.select("#"+IDArray.lineID).remove();
		d3.select("#"+IDArray.tipID).remove();
		d3.select("#"+IDArray.tipBgID).remove();
		gOrdAry[lineNum - 1] = "";
		console.log(gOrdAry);
	}
}