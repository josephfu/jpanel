function jpanelGetLeader( gInitObj ){
	//// get init object from GFA ////
	var gInitUserID = gInitObj.u;
	var gInitOrdSubscription = gInitObj.oc;
	var gInitNotice = gInitObj.sg;
	///////////////////////////////

	//// global setting ////
	var gVersion = "1a";
	var gMsgTopic = "MMALGO";
	var gTelgUserID = [ 428 ];
	var gTelgTarget = [ "joseph" ];
	var gByFunction = 8;
	var gShowLog = 0;
	var gAlertSound = 1;
	var gSeqNum = 0;
	var gActNo = 1;
	var gSessID = 0;
	var gRandomNum = 0;
	var gLeaderID = 0;
	var gLeaderIP = 0;
	var gLeaderOn = 0;
	var gLeaderLiveTime = 3000;
	var gIsLeader = 0;
	var gGettingSessID = 0;
	var gReloaded = 0;
	var gDisconnected = 0;
	var gGotDefaultParm = 0;
	var gCallingLeader = 0;
	var gRepliedLeaderOn = 0;
	var gAskingSessID = 0;
	var gSendingNotice = 0;
	var gSendTelg = 1;
	var gSessIDAry = [];
	var gHostName = window.location.hostname;
	var gIP;
	var gReplyLeaderTimer;
	var gSendSessIDTimer;
	var gLeaderOnTimer;
	var gPanelShortInterval = false;
	var gPanelLongInterval = false;
	var gGetLeaderIDInterval = false;
	var gGetIPInterval = false;

	if ( gHostName == "192.168.1.187" ){ // for m6 testing server setting
		gShowLog = 1;
		gTelgTarget = [ "joseph" ];
	}

////////////////////////// initAlgo ////////////////////

	init();

/////////////////////////////// jquery event /////////////////////////

	$( "#jPanelGetLeader" ).on( "click" , ".btnDiv" , uiClickSendMsg );

/////////////////////////////// function /////////////////////////

	function init(){
		ee.addListener( "MC_CMR" , readMacro ); // run readMacro when received "CMR" msg
		ee.addListener( "MMO_O_CH_DIS" , GFA_disconnected ); // run GFA_disconnected when GFA is disconnected

		$.when( getClientIP() ).then( function (){ // wait until client got IP address
			getLocalStore();
			getMacro();
		});
	}

	function initPanel( cmrAry ){ // start panel
		// e.g. CMR 302 GET_MACRO SESSID 198 SEQ 192.168.0.110::48
		var seq = cmrAry[6];

		var ip = seq.split( "::" )[0];
		var num = parseInt( seq.split( "::" )[1] );

		if ( ip != gIP || num != gRandomNum ){ // check who are getting macro
			return;
		}

		onPanelTimer();
		checkIsLeader();
	}

//////////// start , stop , set , delete macro function ///////////

	function getMacro(){
		var sessID = "";
		if ( gSessID != 0 ){
			sessID = gSessID + " ";
		}
		gRandomNum = getRandomNumber();

		//var cmd = "CMD " + gInitUserID + " GET_MACRO " + sessID + "SEQ " + gIP + "::" + gRandomNum;
		var cmd = "CMD " + gInitUserID + " GET_MACRO " + sessID + "SEQ " + gIP + "::" + gRandomNum + "::" + gVersion;
		gInitOrdSubscription.push( cmd );
		showLog( "getMacro" , cmd );
		//e.g. CMD 302 GET_MACRO SEQ 192.168.0.110::64::1a
	}

	function readMacro( cmrMsg ){ // handle "CMR" message
		showLog( "readMacro" , cmrMsg ); // show all cmr msg

		var cmrLine = cmrMsg.split( "\n" );
		for ( i = 0 ; i < cmrLine.length ; i++ ){
			var cmrAry = cmrLine[i].split( " " );
			var msgType = cmrAry[0];
			var userID = cmrAry[1];
			var cmrType = cmrAry[2];
			if ( cmrAry.length < 4 || msgType != "CMR" || userID != gInitUserID || gReloaded == 1 || gDisconnected == 1 ){ // ignore if msg too short , not cmr , not for this user or user was requested to reload.
				return;
			}
			if ( cmrType == "MACRO" ){
				var macName = cmrAry[3];
				var macParm = cmrAry[5];
				if ( macName == "_LEADER_ID" ){
					readLeaderID( cmrAry );
				}

			}else if ( cmrType == "DO" ){
				cmrAction = cmrAry[3];
				if ( cmrAction == "_RELOAD" ){
					reloadAll();
				}

			}else if ( cmrType == "MSG" ){
				var msgTopic = cmrAry[3];
				var msgContent = cmrAry[4];

				if ( msgTopic == "MMALGO" ){
					readMsg( cmrAry );
				}else if ( msgTopic == "SEND_SESSID" ){
					collectSessID( cmrAry );
				}else if ( msgTopic == "GET_SESSID" ){
					sendSessID();
				}else if ( msgTopic == "CALL_LEADER" ){
					replyLeaderID( cmrAry );
				}else if ( msgTopic == "LEADER_ON" ){
					readLeaderOnMsg( cmrAry );
				}else if ( msgTopic == "MMNOTICE" ){
					readNotice( cmrAry );
				}else if ( msgTopic == "MULTIPLE_LEADER" ){
					getNewSessID( cmrAry );
				}else if ( msgTopic == "SEND_MSG" ){
					sendLeaderNotice( cmrAry );
				}else if ( msgTopic == "LEADER_NOTICE" ){
					readLeaderNotice( cmrAry );
				}

			}else if ( cmrType == "GET_MACRO" ){
				getSessID( cmrAry );
				initPanel( cmrAry );

			}else if ( cmrType == "DEFAULT" ){
				readDefaultParm( cmrAry );

			}else if ( cmrType == "GET" ){
				readGetParm( cmrAry );
			}
		}
	}

	function setMacro( macName , parm ){
		if ( jQuery.type( parm ) == "array" ){
			var parmStr = "{";
			for ( i = 0 ; i < parm.length ; i ++ ) {
				parmStr = parmStr + " " + parm[i];
				if ( ! validateParameter( macName , parm[i] , parm[i] ) ){
					throw new Error( "Parameter is NaN or undefined." );
				}
			}
			parmStr = parmStr + " }";
		}else{
			var parmStr = parm;

			if ( ! validateParameter( macName , parm , parm ) ){
				throw new Error( "Parameter is NaN or undefined." );
			}
		}

		getSeqNum( gByFunction );
		var cmd = "CMD " + gInitUserID + " MACRO " + macName + " NAME " + parmStr;
		//var cmd = "CMD " + gInitUserID + " MACRO " + macName + " NAME " + parmStr + " SEQ " + gSeqNum;

		gInitOrdSubscription.push( cmd );
		showLog( "setMacro" , cmd );
	}

	function setDefault( defName , parm ){
		if ( ! validateParameter( defName , parm , parm ) ){
			throw new Error( "Parameter is NaN or undefined." );
		}

		getSeqNum( gByFunction );
		var cmd = "CMD " + gInitUserID + " DEFAULT " + defName + " " + parm;
		//var cmd = "CMD " + gInitUserID + " DEFAULT " + defName + " " + parm + " SEQ " + gSeqNum;

		gInitOrdSubscription.push( cmd );
		showLog( "setDefault" , cmd );
	}

	/////////////// algo guardian function ////////////

	function onPanelTimer(){ // check open order , pnl , netPos and mode every second
		clearInterval( gPanelShortInterval ); // stop panel timer
		gPanelShortInterval = false;
		clearInterval( gPanelLongInterval );
		gPanelLongInterval = false;

		if ( !gPanelShortInterval ){
			gPanelShortInterval = setInterval( function (){

			} , 1000 ); // 1000 = 1 seconds
		}

		if ( !gPanelLongInterval ){
			gPanelLongInterval = setInterval( function (){
				checkIsLeader();
			} , 60000 );
		}
	}

	//////////////////// select leader function //////////////

	function checkIsLeader(){ // check if itself is a leader or not
		var defer = $.Deferred();

		if ( gIsLeader == 1 ){ // already is leader.
			replylLeaderOn();
			defer.resolve( null );
			return defer;
		}

		$.when( getLeaderID() ).then( function (){
			defer.resolve( null );
			return defer;
		});

		return defer;
	}

	function getLeaderID(){ // find current leader
		var deferred = $.Deferred(); // create defer object
		if ( gCallingLeader == 1 || gAskingSessID == 1 ){
			deferred.resolve( null );
			return deferred;
		}

		clearInterval( gGetLeaderIDInterval ); // clear previous timer
		gGetLeaderIDInterval = false;

		setTimeout( function (){ // wait 0.5 second for leader response

			if ( gLeaderOn == 1 ){ // check if leader has response
				deferred.resolve( null );
				return deferred;
			}

			var leader = gLeaderID + "::" + gLeaderIP;
			var sess = gSessID + "::" + gIP;

			sendMsg( gInitUserID , "CALL_LEADER" , leader + " " + sess ); // call leader

			var interval = 950; // check received response every 0.3 second
			var wait = gLeaderLiveTime; // wait 3 seconds for leader response

			if ( !gGetLeaderIDInterval ){
				gGetLeaderIDInterval = setInterval( function (){ // check every 0.2 second for leader response
					if ( gLeaderOn == 1 ){ // check if leader has response
						clearInterval( gGetLeaderIDInterval );
						gGetLeaderIDInterval = false;
						deferred.resolve( null );
						return deferred;
					}

					if ( wait <= 0 ){
						clearInterval( gGetLeaderIDInterval );
						gGetLeaderIDInterval = false;
					}

					if ( wait <= 0 && gLeaderOn == 0 ){
						gSessIDAry = [];

						sendMsg( gInitUserID , "GET_SESSID" , sess ); // ask every one to send his session ID if leader do no response

						setTimeout( function (){ // wait 2 second to collect session ID
							if ( gLeaderOn == 1 ){ // check if leader has response
								deferred.resolve( null );
								return deferred;
							}

							if ( gSessIDAry.length == 0 ){
								setMacro( "_LEADER_ID" , gSessID + "::" + gIP ); // assign leader

								gIsLeader = 1;
								$( "#userInfoIcon" ).show(); // show leader icon

								deferred.resolve( null );
								return deferred;
							}

							gSessIDAry = gSessIDAry.sort(); // sort session ID
							gLeaderID = parseInt( gSessIDAry[0][0] ); // use first session ID as leader ID
							gLeaderIP = gSessIDAry[0][1];

							if ( gLeaderID == gSessID && gLeaderIP == gIP ){
								gIsLeader = 1;
								$( "#userInfoIcon" ).show(); // show leader icon

							}else{
								gIsLeader = 0;
								$( "#userInfoIcon" ).hide();
							}

							setMacro( "_LEADER_ID" , gLeaderID + "::" + gLeaderIP ); // assign leader

							clearInterval( gGetLeaderIDInterval );
							gGetLeaderIDInterval = false;

							deferred.resolve( null );
							return deferred;
						} , 2000 );
					}

					wait -= interval;
				} , interval );
			}
		} , 500 );

		return deferred;
	}

	function replylLeaderOn(){ // reply leader on if it is a leader
		if ( gRepliedLeaderOn == 1 ){
			return;
		}

		sendMsg( gInitUserID , "LEADER_ON" , gSessID + "::" + gIP ); // response if itself is the leader

		gRepliedLeaderOn = 1;

		setTimeout( function (){
			gRepliedLeaderOn = 0;
		} , 500 );
	}

	function readLeaderID( cmrAry ){
		// e.g. CMR 302 MACRO _LEADER_ID NAME 927::192.168.0.110 ( someone assigned leader );
		gCallingLeader = 0;
		gAskingSessID = 0;
		clearInterval( gGetLeaderIDInterval );
		gGetLeaderIDInterval = false;

		leaderID = parseInt( cmrAry[5].split( "::" )[0] );
		leaderIP = cmrAry[5].split( "::" )[1];

		if ( typeof leaderID == "undefined" || isNaN( leaderID ) || leaderIP == 0 || typeof leaderIP == "undefined" ){
			return;
		}

		gLeaderID = leaderID;
		gLeaderIP = leaderIP;

		if ( gLeaderID == gSessID && gLeaderIP == gIP ){
			gIsLeader = 1;
			$( "#userInfoIcon" ).show(); // show leader icon

		}else{
			gIsLeader = 0;
			$( "#userInfoIcon" ).hide();
		}
	}

	function readLeaderOnMsg( cmrAry ){
		// e.g. CMR 309 MSG LEADER_ON 308::192.168.0.110
		gCallingLeader = 0;
		gAskingSessID = 0;
		clearInterval( gGetLeaderIDInterval );
		gGetLeaderIDInterval = false;

		clearTimeout( gLeaderOnTimer );
		onPanelTimer();

		gLeaderOnTimer = setTimeout( function (){
			gLeaderOn = 0;
		} , gLeaderLiveTime );

		if ( gLeaderOn == 1 ){ // check if received leader response already
			var leaderID = parseInt( cmrAry[4].split( "::" )[0] );
			var leaderIP = cmrAry[4].split( "::" )[1];

			if ( gLeaderID != leaderID || gLeaderIP != leaderIP ){
				multipleLeader( leaderID , leaderIP ); // ask the another leader to get new ID
			}

			return;
		}

		gLeaderOn = 1;

		gLeaderID = parseInt( cmrAry[4].split( "::" )[0] );
		gLeaderIP = cmrAry[4].split( "::" )[1];

		if ( typeof gLeaderIP == "undefined" ){
			gLeaderIP = 0;
		}
	}

	function replyLeaderID( cmrAry ){ // response if someone call leader
		//e.g. CMR 302 MSG CALL_LEADER 308::192.168.0.110
		gCallingLeader = 1;

		clearTimeout( gReplyLeaderTimer ); // clear previous timer

		gReplyLeaderTimer = setTimeout( function (){ // wait 0.5 second to avoid reading too many CALL_LEADER msg
			var leaderID = cmrAry[4].split( "::" )[0];
			var ip = cmrAry[4].split( "::" )[1];

			if ( gSessID != leaderID || gIP != ip ){ // check itself is leader or not
				return;
			}

			replylLeaderOn();
		} , 500 );
	}

	function sendSessID(){ // send session id to every one
		gAskingSessID = 1;
		clearTimeout( gSendSessIDTimer ); // clear previous timer

		gSendSessIDTimer = setTimeout( function (){ // wait 0.5 sec to avoid reading too many send sessID request
			sendMsg( gInitUserID , "SEND_SESSID" , gSessID + "::" + gIP );
		} , 500 );
	}

	function collectSessID( cmrAry ){ // collect session ID information
		// e.g. CMR 302 MSG SEND_SESSID 928::192.168.0.110
		var sessID = parseInt( cmrAry[4].split( "::" )[0] );
		var ip = cmrAry[4].split( "::" )[1];

		if ( sessID == 0 || sessID.toString() == "NaN" || typeof ip == "undefined" || ip == 0 ){
			return; // ignore if it has wrong seeID or ip
		}

		if ( jQuery.inArray( sessID , gSessIDAry ) >= 0 ){
			return;
		}
		gSessIDAry[ gSessIDAry.length ] = [ sessID , ip ]; // save session ID to an array
	}

	function getSessID( cmrAry ){ // store session ID after get macro
		// e.g. CMR 302 GET_MACRO SESSID 198 SEQ 192.168.0.110::48
		var seq = cmrAry[6];

		var ip = seq.split( "::" )[0];
		var num = parseInt( seq.split( "::" )[1] );

		if ( gSessID != 0 ){
			return;
		}

		if ( ip != gIP || num != gRandomNum ){
			return;
		}

		gSessID = parseInt( cmrAry[4] );
		$( "#userInfoID" ).text( gSessID );
	}

	function multipleLeader( sessID , ip ){
		sendMsg( gInitUserID , "MULTIPLE_LEADER" , sessID + "::" + ip ); // request this user to get new session ID
	}

	function getNewSessID( cmrAry ){ // get a new session ID
		if ( gGettingSessID == 1 ){ // check the user is getting new ID or not
			return;
		}

		var sessID = parseInt( cmrAry[4].split( "::" )[0] );
		var ip = cmrAry[4].split( "::" )[1];

		if ( gSessID != sessID || gIP != ip ){ // check is it this user
			return;
		}

		gSessID = 0; // delete old ID
		gGettingSessID = 1; // getting new ID

		getMacro(); // getMacro to get new session ID

		setTimeout( function (){
			gGettingSessID == 0; // wait 0.2 second to get new ID
		} , 200 );
	}

	/////////////////////////// Other function ///////////////

	function getSeqNum( type ){ // get sequence number
		if ( gActNo >= 100 ){
			gActNo = 0;
		}

		var actNo = gActNo;

		if ( gActNo < 10 ){
			actNo = "0" + gActNo
		}

		gSeqNum = type.toString() + gSessID.toString() + actNo.toString() + gVersion; // e.g. SEQ 7378391a
		gActNo ++;
	}

	function getLocalStore(){ // load local conifg.
		var jShowLog = store.get( "jShowLog" );
		if ( $.isNumeric( jShowLog ) ){
			gShowLog = jShowLog;
		}

		var jAlertSound = store.get( "jAlertSound" );
		if ( $.isNumeric( jAlertSound ) ){
			gAlertSound = jAlertSound;
		}
	}

	function getRandomNumber(){ // 1 - 100
		var num = Math.floor( ( Math.random() * 100 ) + 1 );
		return num;
	}

	function reloadAll(){ // reload page count down
		gReloaded = 1; // mark this user is reloaded. unable to send and receive any CMD and CMR

		clearInterval( gPanelShortInterval ); // stop panel timer
		gPanelShortInterval = false;
		clearInterval( gPanelLongInterval );
		gPanelLongInterval = false;

		location.reload();
	}

	function showLog( name , content ){ // show con sole log
		if ( gShowLog != 1 ){
			return;
		}
		console.log( name + " : " + content );
	}

	function showDebugLog( name , content ){ // show con sole log
		if ( gShowLog != 1 ){
			return;
		}
		console.log( " DEBUG : " + name + " : " + content );
	}

	function sendMsg( target , topic , msg ){ // send message
		getSeqNum( gByFunction );
		//var cmd = "CMD " + gInitUserID + " MSG " + target + " " + topic + " { " + msg + " }" + " SEQ " + gSeqNum;
		var cmd = "CMD " + gInitUserID + " MSG " + target + " " + topic + " { " + msg + " }";

		gInitOrdSubscription.push( cmd );
		showLog( "sendMsg" , cmd );
	}

	function sendLogMsg( msg ){ // send log message to server for debug
		getSeqNum( gByFunction );
		var topic = "LOGMSG";
		var cmd = "CMD " + gInitUserID + " MSG " + gInitUserID + " " + topic + " { " + msg + " }" + ". SEQ number : " + gSeqNum;

		gInitOrdSubscription.push( cmd );
		showLog( "sendLogMsg" , cmd );
	}

	function readMsg( cmrAry ){ // read MSG cmr message
		// e.g. CMR 302 MSG MMALGO Stopped All Algo.
		if ( cmrAry.length < 6 ){
			return;
		}

		var cmrMsg = "";

		for ( i = 4 ; i < cmrAry.length - 1 ; i ++ ){
			cmrMsg += cmrAry[i] + " ";
		}

		if ( cmrMsg == "Server Started." ){
			reloadAll(); // reload page if server is restarted.

		}
	}

	function readNotice( cmrAry ){ // read notice msg
		// e.g. CMR 302 MSG NOTICE GUARD
		if ( cmrAry.length < 4 ){
			return;
		}

		notice = cmrAry[4];
		topic = notice.split( "_" )[0];
		type = notice.split( "_" )[1];

		var msg = "";
		for ( i = 5 ; i < cmrAry.length ; i++ ){
			msg += cmrAry[i] + " ";
		}
	}

	function sendTelgMsg( topic , msg ){ // show message to telegram
		if ( gSendTelg == 0 ){
			return;
		}

		for ( i = 0 ; i < gTelgTarget.length ; i++ ){
			sendMsg( gTelgTarget[i] , topic , msg );
		}
	}

	function readDefaultParm( cmrAry ){ // read default algo parameter
		var name = cmrAry[3];
		var parm = parseFloat( cmrAry[4] );
	}

	function readGetParm( cmrAry ){ // read get parameter msg from server
		// e.g. CMR 302 GET _ONALL_MACRO 0
		var name = cmrAry[3];
		var parm = parseFloat( cmrAry[4] );

		gGotDefaultParm = 1;
	}

	function getCurrentTime(){ // get time. format 2017_10_19_12_53_32 = 19/11/2017 12:53:32a.m.
		var date = new Date();

		var year = date.getFullYear().toString();
		var month = date.getMonth().toString();
		var day = date.getDate().toString();
		var hour = date.getHours().toString();
		var minute = date.getMinutes().toString();
		var second = date.getSeconds().toString();

		var time = year + "_" + month + "_" + day + "_" + hour + "_" + minute + "_" + second;

		return time;
	}

	function GFA_disconnected(){
		gDisconnected = 1; // disable panel react if gfa is disconnected
	}

	function showToastr( type , msg , title ){ // show GFA notice box
		if ( type == "success" ){
			var timeOut = 5000;
		}else if ( type == "info" ){
			var timeOut = 5000;
		}else if ( type == "warning" ){
			var timeOut = 600000;
		}else if ( type == "error" ){
			var timeOut = 600000;
		}

		var options = {
			"closeButton": true ,
			"debug": false ,
			"newestOnTop": false ,
			"progressBar": true ,
			"positionClass": "toast-bottom-left" ,
			"preventDuplicates": false ,
			"onclick": null ,
			"showDuration": "300" ,
			"hideDuration": "300" ,
			"timeOut": timeOut ,
			"extendedTimeOut": "1000" ,
			"showEasing": "swing" ,
			"hideEasing": "linear" ,
			"showMethod": "fadeIn" ,
			"hideMethod": "fadeOut",
		}

		gInitNotice( type , msg , title , options );
	}

	function getDefaultParameter( name ){
		var cmd = "CMD " + gInitUserID + " GET " + name;
		gInitOrdSubscription.push( cmd );

		showLog( "getDefaultParameter" , cmd );
	}

	function validateParameter( parmName , parmNum , parm ){ // validate algo parameter
		if ( typeof parm == "undefined" || parmNum.toString() == "NaN" ){
			var msg = "parameter name: " + parmName + " is NaN or undefined. Parameter : " + parm;
			showLog( "validateParameter" , msg );
			return false;
		}
		return true;
	}

	function getClientIP(){ // get user ip address
		var defer = $.Deferred();
		var retry = 0;

		gGetIPInterval = setInterval( function (){
			if( ( gIP != "" && typeof gIP != "undefined" ) || retry > 10 ){
				defer.resolve( null );
				return defer;
			}

			window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
			var pc = new RTCPeerConnection( { iceServers:[] }) , noop = function (){ };
			pc.createDataChannel( "" );
			pc.createOffer( pc.setLocalDescription.bind( pc ) , noop );

			pc.onicecandidate = function ( ice ){
				if ( !ice || !ice.candidate || !ice.candidate.candidate ) {
					return;
				}
				var regex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/;

				gIP = regex.exec( ice.candidate.candidate )[1];
				$( "#userInfoIP" ).text( gIP );

				pc.onicecandidate = noop;
			};

			retry ++;
		} , 500 );

		return defer;
	}

	function uiClickSendMsg(){
		var topic = "TESTING"
		var msg = "testing to send a notice from leader......";
		sendMsg( gInitUserID , "SEND_MSG" , topic + " " + msg );
	}

	function readLeaderNotice( cmrAry ){
		if ( cmrAry.length < 4 ){
			return;
		}

		topic = cmrAry[4];

		var msg = "";
		for ( i = 5 ; i < cmrAry.length ; i++ ){
			msg += cmrAry[i] + " ";
		}

		if ( gAlertSound == 1 ){
			soundManager.play( "order" );
		}
		showToastr( "warning" , msg , topic );
	}

	function sendLeaderNotice( cmrAry ){
		if ( gSendingNotice == 1 ){
			return;
		}
		$.when( checkIsLeader() ).then( function (){
			if ( gIsLeader == 0 ){ // check if the user is leader or not
				return;
			}

			gSendingNotice = 1;

			var msg = "";
			for ( i = 4 ; i < cmrAry.length ; i++ ){
				msg += cmrAry[i] + " ";
			}

			sendMsg( gInitUserID , "LEADER_NOTICE" , msg );
			sendTelgMsg( "Testing:" , msg );

			setTimeout( function (){
				gSendingNotice = 0;
			} , 2000 );
		});
	}
}