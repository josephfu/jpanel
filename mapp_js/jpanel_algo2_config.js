// algo setting //
var gVersion = "1a";
var gPanelID = "#jpanel_algo";
var gRefECN = "MREX";
var gRefSym = "CME/CMX_GLD/AUG18";
var gTradeECN = "MREX";
var gTradeSym = "CME/CMX_GLD/AUG18";
var gDefOrdTIF = "DAY";
var gLOType = "LO2";
var gSOType = "SO2";
var gDefOrdType = gLOType;

var gDefBufferMaxLoss = -1000;
var gDefAutoFlatSlip = 30;
var gLeaderLiveTime = 3000;
var gFiveSecondTick = 10;

var gTelegramTargetAry = [];
gTelegramTargetAry.push( { userID: [ 300 , 310 , 314 , 317 , 318 , 319 , 320 , 324 , 326 , 327 , 328 , 329 ] , target: [ "joseph" ] } );
gTelegramTargetAry.push( { userID: [ 301 , 303 , 304 , 305 , 306 , 307 , 308 , 309 , 341 , 342 , 343 , 344 , 345 , 346 , 347 , 348 , 349 ] , target: [ "joseph" , "alfred" , "kellog" ] } );
gTelegramTargetAry.push( { userID: [ 302 ] , target: [ "joseph" , "alfred" , "kellog" , "yuri" ] } );
gTelegramTargetAry.push( { userID: [ 311 , 321 ] , target: [ "joseph" , "marky" ] } );
gTelegramTargetAry.push( { userID: [ 312 , 322 ] , target: [ "joseph" , "raymond" ] } );
gTelegramTargetAry.push( { userID: [ 313 , 323 ] , target: [ "joseph" , "kellog" ] } );
gTelegramTargetAry.push( { userID: [ 315 , 325 ] , target: [ "joseph" , "grand" ] } );
gTelegramTargetAry.push( { userID: [ 316 , 326 ] , target: [ "joseph" , "yuri" ] } );
gTelegramTargetAry.push( { userID: [ 319 , 329 ] , target: [ "joseph" , "kaho" ] } );
gTelegramTargetAry.push( { userID: [ 528 , 529 ] , target: [ "joseph" , "dicky" ] } );