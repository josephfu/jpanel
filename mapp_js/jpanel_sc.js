function jpanelSC(uid){
	var defUserID = uid; // get GFA user ID

	$(".scBtn").click(function(){
		var btnName = $(this).html();
		var btnID = this.id + "Btn";
		$('#scList').append("<li class='scLink' id='" + btnID + "' >" + btnName + "</li>");
	});

	$( "#scList" ).sortable({
	  revert: true
	});
	$( "#scLink" ).draggable();
	$( "#scDrop" ).droppable({
		drop: function( event, ui ) {
			$( ui.draggable).remove();
		}
	});

	$(document).on( "click","#scClearPanelBtn", function() {
		$("#qp_menu_reset_all").mousedown();
	});
	$(document).on( "click","#scAddPanelBtn", function() {
		$("#SAXO_XAUUSD").mousedown()
	});
	$(document).on( "click","#scOrderBookBtn", function() {
		$("#order_book").children().click()
	});
	$(document).on( "click","#scTradeBookBtn", function() {
		$("#trade_book").children().click()
	});
	$(document).on( "click","#scPnLBtn", function() {
		$("#pl_book").children().click()
	});
	$(document).on( "click","#scAggPxBtn", function() {
		$("#aggreg_px").children().click()
	});
	$("#scSettingBtn").click(function(){
		$("#scKbDiv").hide();
		$("#scSetting").show();
	});
}