function jpanelCS( gInitObj , gDir ){
	//// get init object from GFA ////
	var gInitUserID = gInitObj.u;
	var gInitOrdSubscription = gInitObj.oc;
	var gInitSymItem = gInitObj.fsi;
	var gInitOpenOrdCount = gInitObj.ooc;
	var gInitAccPnl = gInitObj.tot;
	var gInitNetPos = gInitObj.onp;
	var gInitMktPx = gInitObj.mp;
	var gInitBidAry = gInitObj.cba;
	var gInitAskAry = gInitObj.caa;
	var gInitNotice = gInitObj.sg;
	///////////////////////////////

	//// global setting ////
	var gVersion = "1a";
	var gPanelID = "#jpanel_cs";
	var gDefECN = "HKE";
	var gDefSym = "HSIZ7";
	var gDefOrdTIF = "DAY";
	var gLOType = "LO2";
	var gSOType = "SO2";
	var gDefOrdType = gLOType;
	var gDefPips = 1;
	var gDefPipsMin = 0;
	var gDefPipsMax = 10;
	var gDefQty = 1;
	var gDefQtyMin = 1;
	var gDefQtyMax = 20;
	var gDefCase = 9;
	var gDefUpdFeq = 3 ; // Order Update Fequency
	var gDefUpdMinFeq = 1;
	var gDefUpdMaxFeq = 900;
	var gDefPlcOrdDly = 0 ; // Delay to place order when a order is canceled on non-stop algo
	var gDefRepOrdDly = 6 ; // Delay to place order when a order is filled on non-stop algo
	var gDefRepOrdMinDly = 0;
	var gDefRepOrdMaxDly = 900;
	var gDefGuaFilledQty = 0;
	var gDefGuaNetPos = 0;
	var gDefGuaOpenOrd = 0;
	var gDefGuaMaxLoss = 0;
	var gDefGuaLossFromTop = 0;
	var gDefGuaSlowFed = 0;
	var gDefMaxFilledQty = 500;
	var gDefMaxBSMaxQty = 1000;
	var gDefMaxNetPos = 100;
	var gDefMaxNetMaxPos = 1000;
	var gDefMaxLoss = -5000;
	var gDefMaxLossBuffer = -1000;
	var gDefMaxMaxLoss = -50000;
	var gDefVolume = 2500;
	var gDefResumeVolume = 1500;
	var gDefLossFromTop = -3000;
	var gDefMaxLossFromTop = -50000;
	var gDefOrdMethod = "MA";
	var gDefOrdSpeed = 60;
	var gDefOrdMinSpeed = 60;
	var gDefOrdMaxSpeed = 60;
	var gDefSendTelg = 0;
	var gDefAutoOn = 0;
	var gDefOpenOrdBuff = 3;
	var gDefAutoFlatQty = 100;
	var gDefAutoFlatMaxQty = 20;
	var gDefAutoFlatSlip = 20;
	var gDefAutoChgMode = 0;
	var gDelayTurnOn = 15000;
	var gDefStopAllQty = 3;
	var gDefFuseStopMinQty = 1;
	var gDefFuseStopMaxQty = 50;
	var gDefFuseStopBuffer = 10;
	var gDefGuaCutOpenOrd = 0;
	var gDefGuaCutSlowFed = 0;
	var gDefGuaCutFilledQty = 0;
	var gDefGuaCutNetPos = 0;
	var gDefGuaCutMaxLoss = 0;
	var gDefGuaCutLossFromTop = 0;
	var gDefGuaCutVolume = 0;
	var gDefGuaVolume = 0;
	var gDefGuaResumeVolume = 0;
	var gDefGuaTwiceFill = 0;
	var gDefGuaCutTwiceFill = 0;
	var gDefGuaAlgoOffFlat = 0;
	var gDefGuaNoTradeOn = 0;
	var gDefGuaNoTradeParm = 300;
	var gDefGuaNoTradeMaxParm = 6000;
	var gDefGuaNoTradeMinParm = 5;
	var gDefEnterSprd = 10;
	var gDefHoldTime = 11000;
	var gDefExitSpread = 7;
	var gDefCutLossSpread = 30;
	var gDefExitOrderSpread = 30;
	var gDefRestartTime = 30000;
	var gDefOrderQty = 1;
	var gDefCommision = 10;
	////////////////////////////

	//// global variable ////
	var gCurrMode = "EMPTY";
	var gTelgTarget = [ "joseph" , "kellog" ];
	var gRowHtml = $( ".defMacRow" ).html();
	var gTelgUserID = [ 302 , 307 ];
	var gSpreadMax = 5;
	var gSpreadMin = 5;
	var gSpreadMid = 5;
	var gByMacro = 1;
	var gByManual = 7;
	var gByFunction = 8;
	var gShowLog = 0;
	var gAlertSound = 1;
	var gAlertVolume = 50;
	var gSeqNum = 0;
	var gActNo = 1;
	var gSessID = 0;
	var gLastMmoNum = 0;
	var gMinSecToSec = 1000;
	var gOnAllMacro = 0;
	var gRandomNum = 0;
	var gLeaderID = 0;
	var gLeaderIP = 0;
	var gLeaderOn = 0;
	var gLeaderLiveTime = 3000;
	var gIsLeader = 0;
	var gModeCfmAct = 0;
	var gRowUpdFeq = 0;
	var gRowRepOrdDly = 0;
	var gRowOrdType;
	var gRunning = 2;
	var gGettingSessID = 0;
	var gMaxProfit = 0;
	var gReloaded = 0;
	var gDisconnected = 0;
	var gGotDefaultParm = 0;
	var gCallingLeader = 0;
	var gRepliedLeaderOn = 0;
	var gChgingMode = 0;
	var gChgModeCount = 0;
	var gAskingSessID = 0;
	var gPause = 0;
	var gGuaSlowFedCount = 0;
	var gCSAllowTrade = 1;
	var gNotAllowTradeToday = 0;
	var gOrdMethod = gDefOrdMethod;
	var gOrdSpeed = gDefOrdSpeed;
	var gRowOrdMethod = gDefOrdMethod;
	var gRowOrdSpeed = gDefOrdSpeed;
	var gOrdType = gDefOrdType;
	var gOrdTIF = gDefOrdTIF;
	var gGuaFilledQtyOn = gDefGuaFilledQty;
	var gGuaNetPosOn = gDefGuaNetPos;
	var gGuaOpenOrdOn = gDefGuaOpenOrd;
	var gGuaMaxLossOn = gDefGuaMaxLoss;
	var gGuaLossFromTopOn = gDefGuaLossFromTop;
	var gGuaSlowFedOn = gDefGuaSlowFed;
	var gSendTelg = gDefSendTelg;
	var gAutoOn = gDefAutoOn;
	var gAutoChgMode = gDefAutoChgMode;
	var gUpdFeq = gDefUpdFeq;
	var gRepOrdDly = gDefRepOrdDly;
	var gGuaFilledQtyParm = gDefMaxFilledQty;
	var gGuaNetPosParm = gDefMaxNetPos;
	var gGuaMaxLossParm = gDefMaxLoss;
	var gGuaLossFromTopParm = gDefLossFromTop;
	var gGuaAutoFlatQtyParm = gDefAutoFlatQty;
	var gGuaAutoFlatMaxQtyParm = gDefAutoFlatMaxQty;
	var gGuaFuseStopQtyParm = gDefStopAllQty;
	var gGuaOpenOrdCut = gDefGuaCutOpenOrd;
	var gGuaSlowFedCut = gDefGuaCutSlowFed;
	var gGuaFilledQtyCut = gDefGuaCutFilledQty;
	var gGuaNetPosCut = gDefGuaCutNetPos;
	var gGuaMaxLossCut = gDefGuaCutMaxLoss;
	var gGuaLossFromTopCut = gDefGuaCutLossFromTop;
	var gGuaVolumeCut = gDefGuaCutVolume;
	var gGuaTwiceFillCut = gDefGuaCutTwiceFill;
	var gGuaResumeVolumeOn = gDefGuaResumeVolume;
	var gGuaAlgoOffFlatOn = gDefGuaAlgoOffFlat;
	var gGuaVolumeParm = gDefVolume;
	var gGuaResumeVolumeParm = gDefResumeVolume;
	var gGuaVolumeOn = gDefGuaVolume;
	var gGuaTwiceFillOn = gDefGuaTwiceFill;
	var gGuaNoTradeOn = gDefGuaNoTradeOn;
	var gGuaNoTradeParm = gDefGuaNoTradeParm;
	var gDefSymTs = getSymItem( gDefECN , gDefSym ).ts;
	var gSymTs = gDefSymTs;
	var gTtlFillAry = [];
	var gMacOnAry = [];
	var gSessIDAry = [];
	var gSymSprdAry = [];
	var gOnOffAry = [];
	var gModeAry = [];
	var gRowSeqAry = [];
	var gSpreadMsgAry = [];
	var gRecordAry = [];
	var gCSMacroAry = [];
	var gCSParmObj = {};
	var gHostName = window.location.hostname;
	var gIP;
	var gBgCountTimer;
	var gDelayTurnOnTimer;
	var gAutoFlatTimer;
	var gReadTotalFillTimer;
	var gReplyLeaderTimer;
	var gSendSessIDTimer;
	var gLeaderOnTimer;
	var gGuaSlowFedTimer;
	var gNoTradeTimer;
	var gPanelShortInterval = false;
	var gPanelLongInterval = false;
	var gGetLeaderIDInterval = false;
	var gCheckParameterInterval = false;
	var gGetIPInterval = false;
	var gFlatPosInterval = false;
	var gLastFillSide = "";
	var gSymOpt_MREX = "";
	var gSymOpt_PATS = "";
	var gSymOpt_PATX = "";
	var gSymOpt_HKE = "";
	var gEcnList = [ "MREX" , "PATS" , "PATX" , "HKE" ];

	$.getScript( gDir + "/jpanel_sym.js" , function (){ // load symbol list
		gSymOpt_MREX = gSym_CME;
		gSymOpt_PATS = gSym_CME;
		gSymOpt_PATX = gSym_CME;
		gSymOpt_HKE = gSym_HKE;
	} );

	if ( gHostName == "192.168.1.187" ){ // for m6 testing server setting
		gShowLog = 1;
		gDefECN = "HKE";
		gTelgTarget = [ "joseph" ];
	}
	gCSParmObj.ECN = gDefECN;
	gCSParmObj.SYM = gDefSym;

/////////////////////////////// jquery event /////////////////////////

	$( ".cfgSec" ).on( "change" , ".cfgChk" , uiClickCfgChkbox );
	$( ".cfgSec" ).on( "change" , ".cfgOnChk" , uiClickCfgChkbox );
	$( ".cfgPanlSec" ).on( "mouseup" , ".cfgPanlCfmDiv" , uiConfimPanlCfg );
	$( ".cfgPanlSec" ).on( "mouseup" , ".cfgReldDiv" , uiClickReloadAll );
	$( ".cfgGuarSec" ).on( "mouseup" , ".cfgGuarCfmDiv" , uiConfimGuarCfg );
	$( ".cfgModeSec" ).on( "click" , ".cfgModeSaveDiv" , uiClickSaveMode );
	$( ".cfgModeSec" ).on( "click" , ".cfgModeNewDiv" , uiClickNewMode );
	$( ".cfgModeSec" ).on( "click" , ".cfgModeRenameDiv" , uiClickRenameMode );
	$( ".cfgModeSec" ).on( "click" , ".cfgModeDeleteDiv" , uiClickDeleteMode );
	$( ".cfgModeSec" ).on( "click" , ".cfgModeDeleteAllDiv" , uiClickDeleteAllMode );
	$( ".cfgModeSec" ).on( "click" , ".cfgModeCfmDiv" , uiClickModeCfmBtn );
	$( ".cfgModeSec" ).on( "click" , ".cfgModeCnlDiv" , uiClickModeCnlBtn );
	$( ".cfgModeSec" ).on( "click" , ".cfgModeLi" , uiClickUseMode );
	$( ".cfgErrorSec" ).on( "click" , ".cfgErrorCnlBtnDiv" , uiClickCloseErrorDiv );
	$( ".cfmSec" ).on( "click" , ".cfmOnCnlBtn" , hideCfmSec );
	$( ".cfmSec" ).on( "click" , ".cfmOnCfmBtn" , uiClickCfmOnPanel );
	$( ".titleSec" ).on( "mouseup" , ".panlDelAllDiv" , uiClickDeleteAllMacro );
	$( ".titleSec" ).on( "mouseup" , ".panlCfgDiv" , uiClickCfgPanelBtn );
	$( ".titleSec" ).on( "mouseup" , ".panlGuarDiv" , uiClickCfgGuarBtn );
	//$( ".titleSec" ).on( "mouseup" , ".panlSwLbl" , uiClickPanlSwitch );
	$( ".titleSec" ).on( "mouseup" , ".panlSwLbl" , uiClickCSPanlSwitch );
	$( ".titleSec" ).on( "mouseup" , ".panlSortAscBtn" , function (){ uiClickSortRow( 1 ) ; } );
	$( ".titleSec" ).on( "mouseup" , ".panlSortDescBtn" , function (){ uiClickSortRow( 2 ) ; } );
	$( ".titleSec" ).on( "dblclick" , ".panlMaxProfitDiv" , renewMaxProfit );
	$( ".titleSec" ).on( "dblclick" , ".panlAllowTradeDiv" , changeCSAllowTrade );
	$( ".panlSec" ).on( "change" , ".cfgChk" , uiClickCfgChkbox );
	$( ".panlSec" ).on( "mouseup" , ".cfgRowCfmDiv" , uiConfimRowCfg );
	//$( ".panlSec" ).on( "mouseup" , ".rowCfgDiv" , uiClickCfgRowBtn );
	//$( ".panlSec" ).on( "mouseup" , ".macSwLbl" , uiClickMacroSwitch );
	$( ".panlSec" ).on( "mouseup" , ".ifdSwLbl" , uiClickIfdSwitch );
	$( ".panlSec" ).on( "mousedown" , hideCfgSec );
	$( ".panlSec" ).on( "mousedown" , ".titleSec" , hideCfgRowSec );
	$( ".panlSec" ).on( "mousedown" , ".rowSec" , hideCfgRowSec );
	$( ".panlSec" ).on( "mouseup" , ".panlAddDiv" , uiClickAddRowBtn );
	$( ".panlSec" ).on( "mouseup" , ".rowDelDiv" , uiClickDelRowBtn );
	$( ".panlSec" ).on( "change" , ".txtIpt" , uiInputTxtValue );
	$( ".panlSec" ).on( "mouseup" , ".txtIpt" , showSlider );
	$( ".panlSec" ).on( "mouseup" , ".ctrlCvr" , showMacOnWarn );
	$( ".panlSec" ).on( "mouseup" , ".rowInfCvr" , showRowOnWarn );
	//$( ".panlSec" ).on( "change" , ".ecnSlt" , uiChgRowEcn );
	//$( ".panlSec" ).on( "change" , ".symSlt" , uiChgRowSym );
	$( ".panlSec" ).on( "mouseup" , ".rowSprdDiv" , uiClickCfgSpread );
	$( ".panlSec" ).on( "click" , ".sprdBtn" , uiClickChgSpread );
	$( ".panlSec" ).on( "mouseup" , ".cfgSymSprdCfmDiv" , uiConfimSymSprdCfg );
	$( ".panlSec" ).on( "mouseup" , ".panlModeDiv" , uiClickPanelMode );
	$( ".panlSec" ).on( "mouseup" , function ( evt ){ hideSlider( evt ); } );
	$( ".titleSec" ).on( "mouseup" , ".panlSaveDiv" , updateCSParm );
	//$( ".titleSec" ).on( "mouseup" , ".panlSaveDiv" , updateAllMacro );
	$( ".panlSec" ).sortable( { // drag row
		items: '.macRow' ,
		handle: ".rowSortDiv" ,
		axis: "y" ,
		helper: "clone" ,
		containment: ".jPanelDiv" ,
		update: function( event , ui ) {
			saveRowSeq();
		} ,
	} );
	$( ".cfgSec " ).resizable( {
		handles: "n , s" ,
		maxHeight: 800 ,
		minHeight: 400 ,
	} );
	$( ".cfgSec " ).draggable( { // drag config section
		//containment: ".jPanelDiv" ,
		cancel: ".cfgModeListUl , .cfgTxtDiv , .iptDiv , .txtIpt" ,
	} );

	$( document ).keydown( function ( e ){ ctrlKeyDown( e ) } );
	$( document ).keyup( function ( e ){ ctrlKeyUp( e ) } );
////////////////////////// initAlgo ////////////////////

	init();

/////////////////////////////// function /////////////////////////

	function init(){
		ee.addListener( "MC_CMR" , readMacro ); // run readMacro when received "CMR" msg
		ee.addListener( "MMO_O_CH_DIS" , GFA_disconnected ); // run GFA_disconnected when GFA is disconnected
		//ee.addListener( "MMO_M_M_M_D_E" , getMovingAvgPx ); // run readMacro when received "CMR" msg

		$.when( getClientIP() ).then( function (){ // wait until client got IP address
			getLocalStore();
			getMacro();
		} );
	}

	function initPanel( cmrAry ){ // start panel
		// e.g. CMR 302 GET_MACRO SESSID 198 SEQ 192.168.0.110::48
		var seq = cmrAry[ 6 ];

		var ip = seq.split( "::" )[ 0 ];
		var num = parseInt( seq.split( "::" )[ 1 ] );

		if ( ip != gIP || num != gRandomNum ){ // check who are getting macro
			return;
		}

		setTimeout( function (){
			loadMacroSwtich();
			showSymSpread();
			applyRowSeq();
			showPnlStop();
			showCSRow();
			updateCSRow();
			checkParameter( "CS_GUARDIAN_ALLOW_TRADE" );
			$.when( electLeader() ).then( function (){
				$.when( checkIsLeader() ).then( function (){
					onPanelTimer();

					if ( gIsLeader == 0 ){
						return;
					}

					setStopAll();
					setFuseStopAll();
					setMacro( "_RELOAD" , 0 );

					setTimeout( function (){
						sendParmMsg();
					} , 500 );
				} );
			} );
		} , 500 );
	}

//////////// start , stop , set , delete macro function ///////////

	function getMacro(){
		var sessID = "";
		if ( gSessID != 0 ){
			sessID = gSessID + " ";
		}
		gRandomNum = getRandomNumber();

		//var cmd = "CMD " + gInitUserID + " GET_MACRO " + sessID + "SEQ " + gIP + "::" + gRandomNum;
		var cmd = "CMD " + gInitUserID + " GET_MACRO " + sessID + "SEQ " + gIP + "::" + gRandomNum + "::" + gVersion;
		pushCMD( cmd );
		showLog( "getMacro" , cmd );
		//e.g. CMD 302 GET_MACRO SEQ 192.168.0.110::64::1a
	}

	function readMacro( cmrMsg ){ // handle "CMR" message
		// showLog( "readMacro" , cmrMsg ); // show all cmr msg

		var cmrLine = cmrMsg.split( "\n" );
		for ( i = 0 ; i < cmrLine.length ; i ++ ){
			var cmrAry = cmrLine[ i ].split( " " );
			var msgType = cmrAry[ 0 ];
			var userID = cmrAry[ 1 ];
			var cmrType = cmrAry[ 2 ];
			if ( cmrAry.length < 4 || msgType != "CMR" || userID != gInitUserID || gReloaded == 1 || gDisconnected == 1 ){ // ignore if msg too short , not cmr , not for this user or user was requested to reload.
				return;
			}

			if ( cmrAry[ 3 ].slice( 0 , 7 ) != "algoLp_" ){ // hide algoLp_ msg on con sole log
				showLog( "readMacro" , cmrLine[ i ] );
			}

			if ( cmrType == "MACRO" ){
				var macName = cmrAry[ 3 ];
				var macParm = cmrAry[ 5 ];
				if ( macName.slice( 0 , 7 ) == "algoMac" ){
					try{
						updateRowInfo( cmrAry );
					}catch ( e ){
						showParmErrorNotice( 1 );
						showLog( "catch error" , e );
					}
				}else if ( macName == "_ONALL_MACRO" ){
					trunPanelSwitch( macParm );
				}else if ( macName == "_ON_MACRO" ){
					readOnMacroList( cmrAry );
				}else if ( macName == "_ON_SEND_TELG" ){
					gSendTelg = parseInt( macParm );
				}else if ( macName == "_ON_AUTO_ON" ){
					gAutoOn = parseInt( macParm );
				}else if ( macName == "_ON_AUTO_CHGMODE" ){
					gAutoChgMode = parseInt( macParm );
				}else if ( macName == "_CFG_UPY_FEQ" ){
					gUpdFeq = parseInt( macParm );
				}else if ( macName == "_CFG_REP_ORD_DLY" ){
					gRepOrdDly = parseInt( macParm );
				}else if ( macName == "_CFG_ORD_SPEED" ){
					gOrdSpeed = parseInt( macParm );
					$( ".cfgRowOrdMethodDiv" ).each( function (){
						$( this ).find( ".txtIpt" ).val( gOrdSpeed );
					} );
				}else if ( macName == "_CFG_ORDER_METHOD" ){
					gOrdMethod = macParm;
				}else if ( macName == "_LEADER_ID" ){
					readLeaderID( cmrAry );
				}else if ( macName == "_SYM_SPREAD" ){
					readSymSpread( cmrAry );
				}else if ( macName.slice( 0 , 5 ) == "_MODE" ){
					readMode( cmrAry );
				}else if ( macName == "_CURRENT_MODE" ){
					gCurrMode = macParm;
					showCurrentMode( macParm );
				}else if ( macName == "_ROW_SEQ" ){
					readRowSeq( cmrAry );
				}else if ( macName == "_MAX_PROFIT" ){
					readMaxProfit( cmrAry );
				}else if ( macName == "_NOT_ALLOW_DAY" ){
					readNotAllowDay( cmrAry );
				}else if ( macName == "_GUA_FILLEDQTY_ON" ){
					gGuaFilledQtyOn = parseInt( macParm );
				}else if ( macName == "_GUA_NETPOS_ON" ){
					gGuaNetPosOn = parseInt( macParm );
				}else if ( macName == "_GUA_OPENORD_ON" ){
					gGuaOpenOrdOn = parseInt( macParm );
				}else if ( macName == "_GUA_MAXLOSS_ON" ){
					gGuaMaxLossOn = parseInt( macParm );
				}else if ( macName == "_GUA_LOSSFROMTOP_ON" ){
					gGuaLossFromTopOn = parseInt( macParm );
					showLossFromTop( macParm );
				}else if ( macName == "_GUA_SLOWFED_ON" ){
					gGuaSlowFedOn = parseInt( macParm );
				}else if ( macName == "_GUA_VOLUME_ON" ){
					gGuaVolumeOn = parseInt( macParm );
				}else if ( macName == "_GUA_RESUMEVOLUME_ON" ){
					gGuaResumeVolumeOn = parseInt( macParm );
				}else if ( macName == "_GUA_TWICEFILL_ON" ){
					gGuaTwiceFillOn = parseInt( macParm );
				}else if ( macName == "_GUA_ALGOOFFCUT_ON" ){
					gGuaAlgoOffFlatOn = parseInt( macParm );
				}else if ( macName == "_GUA_NOTRADE_ON" ){
					gGuaNoTradeOn = parseInt( macParm );
					noTradeGuardian();
				}else if ( macName == "_GUA_OPENORD_FLAT" ){
					gGuaOpenOrdCut = parseInt( macParm );
				}else if ( macName == "_GUA_SLOWFED_FLAT" ){
					gGuaSlowFedCut = parseInt( macParm );
				}else if ( macName == "_GUA_FILLEDQTY_FLAT" ){
					gGuaFilledQtyCut = parseInt( macParm );
				}else if ( macName == "_GUA_NETPOS_FLAT" ){
					gGuaNetPosCut = parseInt( macParm );
				}else if ( macName == "_GUA_MAXLOSS_FLAT" ){
					gGuaMaxLossCut = parseInt( macParm );
				}else if ( macName == "_GUA_LOSSFROMTOP_FLAT" ){
					gGuaLossFromTopCut = parseInt( macParm );
				}else if ( macName == "_GUA_VOLUME_FLAT" ){
					gGuaVolumeCut = parseInt( macParm );
				}else if ( macName == "_GUA_TWICEFILL_FLAT" ){
					gGuaTwiceFillCut = parseInt( macParm );
				}else if ( macName == "_GUA_FILLEDQTY_PARM" ){
					gGuaFilledQtyParm = parseInt( macParm );
				}else if ( macName == "_GUA_NETPOS_PARM" ){
					gGuaNetPosParm = parseInt( macParm );
				}else if ( macName == "_GUA_MAXLOSS_PARM" ){
					gGuaMaxLossParm = parseInt( macParm );
				}else if ( macName == "_GUA_LOSSFROMTOP_PARM" ){
					gGuaLossFromTopParm = parseInt( macParm );
				}else if ( macName == "_GUA_VOLUME_PARM" ){
					gGuaVolumeParm = parseInt( macParm );
				}else if ( macName == "_GUA_RESUMEVOLUME_PARM" ){
					gGuaResumeVolumeParm = parseInt( macParm );
				}else if ( macName == "_GUA_AUTOFLATQTY_PARM" ){
					gGuaAutoFlatQtyParm = parseInt( macParm );
				}else if ( macName == "_GUA_AUTOFLATMAXQTY_PARM" ){
					gGuaAutoFlatMaxQtyParm = parseInt( macParm );
				}else if ( macName == "_GUA_FUSESTOPQTY_PARM" ){
					gGuaFuseStopQtyParm = parseInt( macParm );
					$( ".stopAllQtySn" ).text( gGuaFuseStopQtyParm );
				}else if ( macName == "_GUA_NOTRADE_PARM" ){
					gGuaNoTradeParm = parseInt( macParm );
					noTradeGuardian();
				}else if ( macName.slice( 0 , 3 ) == "CS_" ){
					var type = macName.split("_")[1];
					var side = macName.split("_")[2];

					storeMacroCMD( cmrLine[ i ] );

					if ( type == "INIT" && typeof( side ) == "string" ){
						readCSParm( cmrAry );
						updateCSRow();
					}
				}

			}else if ( cmrType == "MACRO_DELETE" ){
				var macName = cmrAry[ 3 ];
				var macParm = cmrAry[ 5 ];
				if ( macName.slice( 0 , 7 ) == "algoMac" ){
					deleteRow( cmrAry );
				}else if ( macName.slice( 0 , 5 ) == "_MODE" ){
					readDeleteMode( cmrAry );
				}

			}else if ( cmrType == "DO" ){
				cmrAction = cmrAry[ 3 ];
				if ( cmrAction == "_STOPALL" || cmrAction == "_STOPALL2" || cmrAction == "_FUSESTOPALL" ){
					turnnedOffPanel();
				}else if ( cmrAction == "_RELOAD" ){
					reloadAll();
				}else if ( cmrAction.slice( 0 , 7 ) == "algoMac" || cmrAction.slice( 0 , 7 ) == "algoCnl" || cmrAction.slice( 0 , 6 ) == "algoLp" ){
					triggerMacro( cmrAry );
				}else if ( cmrAction == "CS_START_ALL" || cmrAction == "CS_START_ALL_W_GUARD" ){
					sendParmMsg();
				}

			}else if ( cmrType == "ALGO-ON" || cmrType == "ALGO-OFF" ){
				triggerMacro( cmrAry );
				printAlgoLog( cmrLine );

			}else if ( cmrType == "MSG" ){
				var msgTopic = cmrAry[ 3 ];
				var msgContent = cmrAry[ 4 ];

				if ( msgTopic == "MMALGO" ){
					readMsg( cmrAry );
				}else if ( msgTopic == "ALGO" ){
					readPlacedPx( cmrAry );
				}else if ( msgTopic == "SEND_SESSID" ){
					collectSessID( cmrAry );
				}else if ( msgTopic == "GET_SESSID" ){
					sendSessID();
				}else if ( msgTopic == "CALL_LEADER" ){
					replyLeaderID( cmrAry );
				}else if ( msgTopic == "LEADER_ON" ){
					readLeaderOnMsg( cmrAry );
				}else if ( msgTopic == "MMNOTICE" ){
					readNotice( cmrAry );
				}else if ( msgTopic == "MULTIPLE_LEADER" ){
					getNewSessID( cmrAry );
				}else if ( msgTopic == "TEST" ){
					receiveCSMsg( cmrAry );
				}else if ( msgTopic == "HTTPCMD" ){
					receiveHTTPCMD( cmrAry );
				}

			}else if ( cmrType == "TOTAL_FILL" ){
				readTotalFill( cmrAry );

			}else if ( cmrType == "GET_MACRO" ){
				getSessID( cmrAry );
				initPanel( cmrAry );

			}else if ( cmrType == "ALGO-RUNNING" ){
				readAlgoRunning( cmrAry );
				trunPanelSwitch( cmrAry[3] );

			}else if ( cmrType == "DEFAULT" ){
				readDefaultParm( cmrAry );

			}else if ( cmrType == "GET" ){
				readGetParm( cmrAry );
			}
		}
	}

	function buildMacro( numStr , side , sym , ecn , qty , pips , updFeq , plcOrdDly , repOrdDly , mCase , spread , ordMethod , ordSpeed , ordType , symTs ){

		if ( ! validateParameter( "numStr" , numStr , numStr ) ||
			! validateParameter( "side" , side , side ) ||
			! validateParameter( "sym" , sym , sym ) ||
			! validateParameter( "ecn" , ecn , ecn ) ||
			! validateParameter( "qty" , qty , qty ) ||
			! validateParameter( "pips" , pips , pips ) ||
			! validateParameter( "updFeq" , updFeq , updFeq ) ||
			! validateParameter( "plcOrdDly" , plcOrdDly , plcOrdDly ) ||
			! validateParameter( "repOrdDly" , repOrdDly , repOrdDly ) ||
			! validateParameter( "mCase" , mCase , mCase ) ||
			! validateParameter( "spread" , spread , spread ) ||
			! validateParameter( "ordMethod" , ordMethod , ordMethod ) ||
			! validateParameter( "ordSpeed" , ordSpeed , ordSpeed ) ||
			! validateParameter( "ordType" , ordType , ordType ) ||
			! validateParameter( "symTs" , symTs , symTs ) ){

			throw new Error( "Parameter is NaN or undefined." ); // avoid sending "NaN" to server
		}

		var macName = "algoMac_" + numStr + "_" + side;
		var cnlName = "algoCnl_" + numStr + "_" + side;
		var lpName = "algoLp_" + numStr + "_" + side;
		var pxName = "algoPx_" + numStr + "_" + side;

		if ( side == "BUY" ){
			var sideNum = 1;
			var bidask = "BID"

			if ( ordType == gLOType ){
				var ordSide = "BUY";
			}else if ( ordType == gSOType ){
				var ordSide = "SELL";
			}
			var flatSide = "SELL";
			var flatSign = "-";

		}else if ( side == "SELL" ){
			var sideNum = 2;
			var bidask = "ASK"

			if ( ordType == gLOType ){
				var ordSide = "SELL";
			}else if ( ordType == gSOType ){
				var ordSide = "BUY";
			}
			var flatSide = "BUY";
			var flatSign = "+";
		}

		var flatPx = gDefFuseStopBuffer * symTs;

		var chkSide = "IF_" + bidask + "_CHECK"; // check whether bid px is checked
		var saveSide = "SAVE" + bidask; // check whether bid px is checked

		var bidaskSuffix = "";
		if ( ordMethod != "Normal" ){
			bidaskSuffix = "_" + ordMethod + "_" + ordSpeed;
			//bidaskSuffix = "_SLOW";
		}

		var bidOrAsk = bidask + bidaskSuffix;
		// e.g. "BID_SLOW_10" or "ASK_MA_20"

		qty = Math.abs( qty );
		if ( mCase == 11 ){
			qty = 1; // only allow user to set qty 1 if it is "stop all and buy/sell" case
		}

		pips = pips + spread;

		if ( pips >= 0 ){
			var sign = "+";
		}else{
			var sign = "-";
		}
		pips = Math.abs( pips );

		var slip = 10;
		var poi = gInitUserID + numStr + sideNum;
		var ordMac = "DEFAULT SYM " + sym + " O " + ecn + " " + ordType + " " + gOrdTIF + " " + ordSide + " " + sym + " QTY " + qty;
		if ( ordType == gLOType ){
			var ordPrefix = "AT";
			var ordTypeMac = ordPrefix + " " + bidOrAsk + " " + sign + " " + pips;
		}else if ( ordType == gSOType ){
			var ordPrefix = "TRIG";
			var ordTypeMac = ordPrefix + " " + bidOrAsk + " " + sign + " " + pips + " SLIP " + slip + " TS " + symTs + " DIP 1";
		}

		var macStartMac = "CMD " + gInitUserID + " MACRO " + macName + " NAME " + macName + " DEFAULT MODE " + mCase;
		var macCnlAlgo = "CANCEL_ALGO " + macName;
		var macCnlOrd = "O " + ecn + " CO DAY CANCEL 0 POI " + poi;
		var macSendOrd = ordMac + " " + ordTypeMac;
		var macSavePOI = "POI " + poi;
		var macSavePx = "SAVE " + pxName + " " + ordPrefix;
		var macUpdMac = "DELAY 0 " + updFeq + " { DO " + lpName + " SEQ " + gByMacro + " }";
		var macDoMac = "DO " + macName + " SEQ " + gByMacro;
		var macStopAll = "DO _STOPALL SEQ " + gByMacro;
		var macStopMsg = "MSG " + gInitUserID + " MMALGO { Hit ALGO Fuse. Stopped All Algo. }";
		var macStopNotice = "MSG " + gInitUserID + " MMNOTICE { ALGOFUSE Hit ALGO Fuse. Stopped All Algo. }";

		var macFuseStopAll = "DO _FUSESTOPALL SEQ " + gByMacro;
		var macFlat = "O " + ecn + " " + gLOType + " " + gOrdTIF + " " + flatSide + " " + sym + " QTY " + gGuaFuseStopQtyParm + " AT " + pxName + " " + flatSign + " " + flatPx + " SEQ 1";

		// if ( mCase == 1 ){ // Stop on Partial Fill
			// var macro = macStartMac + " " + macCnlAlgo +
				// " | " + macCnlOrd +
				// " | " + macSendOrd + " " + macSavePOI + " " + macSavePx +
				// " | " + macUpdMac +
				// " | IF_DONE 1 " + ecn + " LAST_TRADE_OID 1 { " + macCnlAlgo + " | " + macCnlOrd + " }" +
				// " | IF_DONE 1 " + ecn + " LAST_TRADE_OID 2 { " + macCnlAlgo + " }" +
				// " | IF_DONE 1 " + ecn + " LAST_TRADE_OID 4 { NAME " + macName + " DELAY 0 0 { " + macDoMac + " } }" +
				// " | IF_DONE 1 " + ecn + " LAST_TRADE_OID 8 { " + macCnlAlgo + " }";

		// }else if ( mCase == 2 ){ //stop on Complete Fill only
			// var macro = macStartMac + " " + macCnlAlgo +
				// " | " + macCnlOrd +
				// " | " + macSendOrd + " " + macSavePOI + " " + macSavePx +
				// " | " + macUpdMac +
				// " | IF_DONE 1 " + ecn + " LAST_TRADE_OID 1 { " + macCnlAlgo + " | " + macCnlOrd + " | DELAY 0 " + repOrdDly + " { " + macDoMac + " } }" +
				// " | IF_DONE 1 " + ecn + " LAST_TRADE_OID 4 { NAME " + macName + " DELAY 0 0 { " + macDoMac + " } }" +
				// " | IF_DONE 1 " + ecn + " LAST_TRADE_OID 8 { " + macCnlAlgo + " }";

		if ( mCase == 9 ){ //Non-Stop
			var macro = macStartMac + " " + macCnlAlgo +
				" | " + macCnlOrd +
				" | " + macSendOrd + " " + macSavePOI + " " + macSavePx +
				" | " + macUpdMac +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 1 { " + macCnlOrd + " }" +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 2 { NAME " + macName + " DELAY 0 " + repOrdDly + " { " + macDoMac + " } }" +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 4 { NAME " + macName + " DELAY 0 " + plcOrdDly + " { " + macDoMac + " } }" +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 8 { " + macCnlAlgo + " }";

		}else if ( mCase == 10 ){ // Stop All ALGO
			var macro = macStartMac + " " + macCnlAlgo +
				" | " + macCnlOrd +
				" | " + macSendOrd + " " + macSavePOI + " " + macSavePx +
				" | " + macUpdMac +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 1 { " + macStopAll + " }" +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 2 { " + macStopAll + " | " + macStopMsg + " | " + macStopNotice + " }" +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 4 { NAME " + macName + " DELAY 0 0 { " + macDoMac + " } }" +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 8 { " + macCnlAlgo + " }";

		}else if ( mCase == 11 ){ // Stop All ALGO and BUY/SELL
			var macro = macStartMac + " " + macCnlAlgo +
				" | " + macCnlOrd +
				" | " + macSendOrd + " " + macSavePOI + " " + macSavePx +
				" | " + macUpdMac +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 2 { " + macFuseStopAll + " | " + macFlat + " | " + macStopMsg + " | " + macStopNotice + " }" +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 4 { NAME " + macName + " DELAY 0 0 { " + macDoMac + " } }" +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 8 { " + macCnlAlgo + " }";

		}else if ( mCase == 12 ){ // Stop All ALGO and BUY/SELL
			var macro = macStartMac + " " + macCnlAlgo +
				" | " + macCnlOrd +
				" | " + macSendOrd + " " + macSavePOI + " " + macSavePx +
				" | " + macUpdMac +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 1 { " + macCnlAlgo + " | " + macCnlOrd + " }" +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 2 { " + macCnlAlgo + " }" +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 4 { NAME " + macName + " DELAY 0 0 { " + macDoMac + " } }" +
				" | IF_DONE 1 " + ecn + " LAST_TRADE_OID 8 { " + macCnlAlgo + " }";

		}else{
			return;
		}

		var cmdMac = macro; // algo macro
		var cmdMacIn = "CMD " + gInitUserID + " MACRO_IN KEYR ECN SYM GUI_BID GUI_ASK"; // macro in
		var cmdCnl = "CMD " + gInitUserID + " MACRO " + cnlName + " " + macCnlAlgo +
						" | " + macCnlOrd +
						" | DELAY 0 1000 { " + macCnlOrd + " }" ; // stop algo
		var cmdLp = "CMD " + gInitUserID + " MACRO " + lpName + " NAME " + lpName +
						" | DEFAULT SYM " + sym + " O " + ecn + " " + ordType + " " + gOrdTIF + " " + ordTypeMac +
						" | " + "IF { NE AT " + pxName + " } " +
						"{ " + macCnlOrd + " } " +
						"{ NAME " + macName + " DELAY 0 " + updFeq + " { DO " + lpName + " SEQ " + gByMacro + " } }";

		var cmd = { cmdMac: cmdMac , cmdMacIn: cmdMacIn , cmdCnl: cmdCnl , cmdLp: cmdLp };
		return cmd;
	}

	function setMacro( macName , parm ){
		if ( jQuery.type( parm ) == "array" ){
			var parmStr = "{ ";
			for ( i = 0 ; i < parm.length ; i ++ ) {
				if( i > 0 ){
					parmStr += " ";
				}
				parmStr += parm[ i ];
				if ( ! validateParameter( macName , parm[ i ] , parm[ i ] ) ){
					throw new Error( "Parameter is NaN or undefined." );
				}
			}
			parmStr = parmStr + " }";
		}else{
			var parmStr = parm;

			if ( ! validateParameter( macName , parm , parm ) ){
				throw new Error( "Parameter is NaN or undefined." );
			}
		}

		getSeqNum( gByFunction );
		var cmd = "CMD " + gInitUserID + " MACRO " + macName + " NAME " + parmStr;
		//var cmd = "CMD " + gInitUserID + " MACRO " + macName + " NAME " + parmStr + " SEQ " + gSeqNum;

		pushCMD( cmd );
		showLog( "setMacro" , cmd );
	}

	function setDefaultParameter( defName , parm ){
		if ( ! validateParameter( defName , parm , parm ) ){
			throw new Error( "Parameter is NaN or undefined." );
		}

		getSeqNum( gByFunction );
		var cmd = "CMD " + gInitUserID + " DEFAULT " + defName + " " + parm;
		//var cmd = "CMD " + gInitUserID + " DEFAULT " + defName + " " + parm + " SEQ " + gSeqNum;

		pushCMD( cmd );
		showLog( "setDefaultParameter" , cmd );

		getDefaultParameter( defName );
	}

	function updateMacro( rowID , side ){
		var numStr = rowID.substr( rowID.length - 2 );

		var ecn = $( rowID ).find( ".ecnDiv" ).find( ".current" ).html();
		ecn = ecn.replace( / /g , "" );

		var sym = $( rowID ).find( ".symDiv" ).find( ".current" ).html();
		sym = sym.replace( / /g , "" );

		var symTs = getSymItem( ecn , sym ).ts;
		var symLs = getSymItem( ecn , sym ).ls;

		var symSpread = getSymSpread( ecn , sym );

		if ( ! validateParameter( "symTs" , symTs , symTs ) || ! validateParameter( "symLs" , symLs , symLs ) || ! validateParameter( "symSpread" , symSpread , symSpread ) ){
			return;
		}

		if ( side == "BUY" ){
			var bidOrAsk = ".bidSec";
			var spread = symSpread[ 0 ];
		}else{
			var bidOrAsk = ".askSec";
			var spread = symSpread[ 1 ];
		}

		var qty = parseInt( $( rowID ).find( bidOrAsk ).find( ".ctrlQtyDiv" ).find( ".txtIpt" ).val() );
		var pips = parseInt( $( rowID ).find( bidOrAsk ).find( ".ctrlPipsDiv" ).find( ".txtIpt" ).val() );

		var sPips = pips * symTs;
		var sQty = qty * symLs;
		var sSpread = spread * symTs;

		var mCase = $( rowID ).find( bidOrAsk ).find( ".ifdSwChked" ).find( ".ifdSwIpt" ).attr( "value" );
		var repOrdDly = $( rowID ).find( ".cfgSec" ).find( ".cfgRowRepOrdDlyDiv" ).find( ".txtIpt" ).val() * gMinSecToSec;
		var updFeq = $( rowID ).find( ".cfgSec" ).find( ".cfgRowUpdFeqDiv" ).find( ".txtIpt" ).val() * gMinSecToSec;
		var plcOrdDly = gDefPlcOrdDly * gMinSecToSec;

		var ordMethod = $( rowID ).find( ".cfgSec" ).find( ".cfgRowOrdMethodDiv" ).find( ".cfgSeltDiv" ).find( ".current" ).html();
		ordMethod = ordMethod.replace( / /g , "" );

		var ordSpeed = $( rowID ).find( ".cfgSec" ).find( ".cfgRowOrdMethodDiv" ).find( ".txtIpt" ).val();

		var ordType = $( rowID ).find( ".cfgSec" ).find( ".cfgRowOrdTypeDiv" ).find( ".current" ).html();
		ordType = ordType.replace( / /g , "" );

		var cmd = buildMacro( numStr , side , sym , ecn , sQty , sPips , updFeq , plcOrdDly , repOrdDly , mCase , sSpread , ordMethod , ordSpeed , ordType , symTs );

		getSeqNum( gByFunction );

		pushCMD( cmd.cmdMac );
		pushCMD( cmd.cmdMacIn );
		pushCMD( cmd.cmdCnl );
		pushCMD( cmd.cmdLp );

		showLog( "updateMacro" , cmd.cmdMac );
		showLog( "updateMacro" , cmd.cmdCnl );
		showLog( "updateMacro" , cmd.cmdLp );
	}

	function updateAllMacro(){
		$( ".macRow" ).each( function (){
			var rowID = "#" + $( this ).attr( 'id' );
			updateMacro( rowID , "BUY" );
			updateMacro( rowID , "SELL" )
		} );
	}

	function restartAllMacro(){
		$( ".macRow" ).each( function (){
			var rowID = "#" + $( this ).attr( 'id' );
			restartMacro( rowID , "BUY" );
			restartMacro( rowID , "SELL" )
		} );
	}

	function startMacro( macName ){
		getSeqNum( gByManual );
		var cmd = "CMD " + gInitUserID + " DO " + macName + " SEQ " + gSeqNum;

		pushCMD( cmd );
		showLog( "startMacro" , cmd );
	}

	function stopMacro( cnlName ){
		getSeqNum( gByManual );
		var cmd = "CMD " + gInitUserID + " DO " + cnlName + " SEQ " + gSeqNum;

		pushCMD( cmd );
		showLog( "stopMacro" , cmd );
	}

	function restartMacro( rowID , side ){
		if ( side == "BUY" ){
			var bidOrAsk = ".bidSec";
		}else{
			var bidOrAsk = ".askSec";
		}

		var swDiv = $( rowID ).find( bidOrAsk ).find( ".macSw" );
		if ( !$( swDiv ).find( ".swOnLbl" ).hasClass( "swMacOn" ) ){
			return;
		}

		var numStr = rowID.substr( rowID.length - 2 );
		var macName = "algoMac_" + numStr + "_" + side;
		var cnlName = "algoCnl_" + numStr + "_" + side;

		stopMacro( cnlName );
		setTimeout( function (){
			startMacro( macName );
		} , 100 );
	}

	function startAllMacro(){
		for ( i = 0 ; i < gMacOnAry.length ; i ++ ){
			var num = gMacOnAry[ i ].split( "_" )[ 1 ];
			var side = gMacOnAry[ i ].split( "_" )[ 2 ];
			var rowID = "#macRow_" + num;

			if ( $( rowID ).length == 0 ){
				continue;
			}

			if ( side == "BUY" ){
				var bidOrAsk = ".bidSec";
			}else{
				var bidOrAsk = ".askSec";
			}

			var macName = "algoMac_" + num + "_" + side;
			startMacro( macName );
		}
	}

	function deleteMacro( name ){
		//var cmd = "CMD " + gInitUserID + " MACRO_DELETE " + name + " SEQ " + gSeqNum;
		var cmd = "CMD " + gInitUserID + " MACRO_DELETE " + name;

		pushCMD( cmd );
		showLog( "deleteMacro" , cmd );
	}

	function triggerMacro( cmrAry ){
		var cmrType = cmrAry[ 2 ];
		var cmrAction = cmrAry[ 3 ];
		var numStr = cmrAction.split( "_" )[ 1 ];
		var side = cmrAction.split( "_" )[ 2 ];
		var rowID = "#macRow_" + numStr;

		if ( side == "BUY" ){
			var secSide = ".bidSec";
		}else if ( side == "SELL" ){
			var secSide = ".askSec";
		}

		if ( cmrType == "DO" ){
			if ( cmrAction.slice( 0 , 7 ) == "algoMac" ){
				trunOnMacroSwitch( rowID , secSide );
				flashMacroLight( rowID , secSide );
			}else if ( cmrAction.slice( 0 , 7 ) == "algoCnl" ){
				trunOffMacroSwtich( rowID , secSide );
			}else if ( cmrAction.slice( 0 , 6 ) == "algoLp" ){
				trunOnMacroSwitch( rowID , secSide );
				flashMacroLight( rowID , secSide );
			}

		}else if ( cmrType == "ALGO-ON" ){
			trunOnMacroSwitch( rowID , secSide );

		}else if ( cmrType == "ALGO-OFF" ){
			trunOffMacroSwtich( rowID , secSide );
		}

		showBgAlgoCount();
	}

	function uiClickPanlSwitch(){
		var swVal = $( this ).find( ".swIpt" ).val();
		if ( swVal == "Off" ){
			turnOffPanel();
			triggerOffRowGuardian();

		}else if ( swVal == "Flat" ){
			turnOffPanel();
			triggerOffRowGuardian();

			var squarePct = 100;
			var cutSlip = 10;

			clearTimeout( gAutoFlatTimer ); // clear previous timer
			gAutoFlatTimer = setTimeout( function (){
				squarePosition( gGuaAutoFlatQtyParm , gDefAutoFlatSlip );
			} , 1000 ); // wait 1 second to ensure _STOPALL is completed.

		}else if ( swVal == "On" ){
			if ( $( this ).hasClass( "panlSwChked" ) ){
				return;
			}
			if ( onAlgoGuardian() == false ){
				return; // check whether has open order or not
			}

			turnOnPanel();
		}

		setDefaultParameter( "_PAUSE" , 0 );
		getDefaultParameter( "_PAUSE" );
	}

	function turnOnPanel(){ // turn on algo
		try{
			updateAllMacro();

			setMacro( "_ONALL_MACRO" , 1 ); // switch on algo panel Big on off switch
			setDefaultParameter( "_ONALL_MACRO" , 1 );

			gOnAllMacro = 1;
			startAllMacro();

			var msg = "Trun On Algo. Seq No.: " + gSeqNum + ".";
			sendTabMsg( msg );

			autoCheckChangeMode(); // check mode

		}catch ( e ){ // show error when turnning on panel button
			showParmErrorNotice( 2 );
			showLog( "catch error" , e );
		}
	}

	function turnOffPanel(){
		startStopAll( gByManual );

		setMacro( "_ONALL_MACRO" , 0 ); // switch off algo panel Big on off switch
		setDefaultParameter( "_ONALL_MACRO" , 0 );

		gOnAllMacro = 0;

		var msg = "Trun Off Algo. Seq No.: " + gSeqNum + ".";
		sendTabMsg( msg );
	}

	function turnnedOffPanel(){
		clearTimeout( gDelayTurnOnTimer );

		// clearInterval( gPanelShortInterval );
		// gPanelShortInterval = false; // mark the variable = false to avoid duplicate run
		// clearInterval( gPanelLongInterval );
		// gPanelLongInterval = false;

		trunOffAllMacroSwitch();
	}

	function uiClickCfmOnPanel(){ // confirm trun on panel even some algo are still runnning
		hideCfmSec();
		turnOnPanel();
	}

	function trunPanelSwitch( parm ){ // switch on or off the Big on off switch
		gOnAllMacro = parm;

		var panlSwBg = $( ".panlSwDiv" ).find( ".panlSwBg" );
		var swOnLbl = $( ".panlSwDiv" ).find( ".swOnLbl" );
		var swOffLbl = $( ".panlSwDiv" ).find( ".swOffLbl" )

		$( panlSwBg ).stop( true , true );
		if ( parm == 1 ){
			if ( $( swOnLbl ).hasClass( "panlSwChked" ) ){
				return;
			}
			$( panlSwBg ).css( { "left":"-240px" , "width":"33.33%" } );
			$( swOnLbl ).addClass( "panlSwChked" , 300 );
			$( swOffLbl ).removeClass( "panlSwChked" , 300 );

			showSymSpread(); // show symbol spread
			onPanelTimer();

		}else{
			if ( $( swOffLbl ).hasClass( "panlSwChked" ) ){
				return;
			}
			$( panlSwBg ).css( { "left":"-160px" , "width":"66.66%" } );
			$( swOnLbl ).removeClass( "panlSwChked" , 300 );
			$( swOffLbl ).addClass( "panlSwChked" , 300 );
		}
	}

	function trunOffAllMacroSwitch(){
		if ( gOnAllMacro == 1 ){
			setMacro( "_ONALL_MACRO" , 0 ); // switch off Big on off switch
			setDefaultParameter( "_ONALL_MACRO" , 0 );
		}

		$( ".infPxDiv" ).find( ".infDiv" ).find( ".txtIpt" ).val( 0 );
		$( ".panlStopNtcSec " ).fadeOut();
		//hideBgAlgoCount();

		var rowID;
		$( ".macRow" ).each( function (){
			rowID = "#" + $( this ).attr( "id" );
			trunOffMacroSwtich( rowID , ".bidSec" );
			trunOffMacroSwtich( rowID , ".askSec" );
		} );
	}

	function uiClickMacroSwitch(){ // click on or off button for a macro by user
		var macSwitch = this;

		if ( gOnAllMacro == 1 && $( macSwitch ).hasClass( "swMacOn" ) ){
			return;
		}else if ( gOnAllMacro == 0 && $( macSwitch ).hasClass( "swChked" ) ){
			return;
		}

		if ( $( macSwitch ).closest( ".sideSec" ).hasClass( "bidSec" ) ){
			var side = "BUY";
		}else{
			var side = "SELL";
		}

		var swVal = $( macSwitch ).find( ".swIpt" ).val();
		var rowID = "#" + $( macSwitch ).closest( ".macRow" ).attr( "id" );
		var numStr = rowID.substr( rowID.length - 2 );
		var macName = "algoMac_" + numStr + "_" + side;
		var cnlName = "algoCnl_" + numStr + "_" + side;

		if ( gOnAllMacro == 0 ){ // check whether the big on off button is on or not
			if ( swVal == "On" ){
				addOnMacroList( macName ); // store on off information to a list
			}else{
				removeOnMacroList( macName );
			}

		}else{
			if ( swVal == "On" ){
				try{
					updateMacro( rowID , "BUY" ); // update both bid side and ask side macros before turn macro on
					updateMacro( rowID , "SELL" );
					addOnMacroList( macName );
					startMacro( macName );
				}catch ( e ){
					showLog( "catch error" , e );
				}
			}else if ( swVal == "Off" ){
				removeOnMacroList( macName );
				stopMacro( cnlName );
			}
		}
	}

	function trunOnMacroSwitch( rowID , side ){
		var swDiv = $( rowID ).find( side ).find( ".macSw" );
		if ( $( swDiv ).find( ".swOnLbl" ).hasClass( "swMacOn" ) ){
			return;
		}

		$( swDiv ).find( ".swOnLbl" ).addClass( "swMacOn" );
		$( swDiv ).siblings( ".macSec" ).find( ".ctrlCvr" ).show();
		$( rowID ).find( ".rowInfCvr" ).show();
	}

	function trunOffMacroSwtich( rowID , side ){
		var swDiv = $( rowID ).find( side ).find( ".macSw" );
		if ( !$( swDiv ).find( ".swOnLbl" ).hasClass( "swMacOn" ) ){
			return;
		}
		$( swDiv ).find( ".swOnLbl" ).removeClass( "swMacOn" );
		$( swDiv ).siblings( ".macSec" ).find( ".ctrlCvr" ).hide();
		$( rowID ).find( ".rowInfCvr" ).hide();
		$( rowID ).find( side ).find( ".infPxDiv" ).find( ".txtIpt" ).attr( "value" , 0 );
	}

	function addOnMacroList( macName ){ // save macro on off information to a macro
		var newMac = 1;
		for ( i = 0 ; i < gMacOnAry.length ; i ++ ){
			if ( gMacOnAry[ i ] == macName ){
				newMac = 0;
				break;
			}
		}
		if ( newMac == 1 ){
			gMacOnAry.push( macName );
		}

		setMacro( "_ON_MACRO" , gMacOnAry );

		// e.g. CMD 302 MACRO _ON_MACRO NAME { algoMac_02_SELL algoMac_01_SELL }
	}

	function removeOnMacroList( macName ){
		var index = gMacOnAry.indexOf( macName );
		if ( index == -1 ) {
			return;
		}

		gMacOnAry.splice( index , 1 );

		setMacro( "_ON_MACRO" , gMacOnAry );
	}

	function readOnMacroList( cmrAry ){ // read macro on off information and store it to a array
		// e.g. CMR 302 MACRO _ON_MACRO NAME { algoMac_02_SELL algoMac_01_SELL }
		gMacOnAry = [];
		//for ( i = 6 ; i < cmrAry.length - 3 ; i ++ ){
		for ( i = 6 ; i < cmrAry.length - 1 ; i ++ ){
			var macName = cmrAry[ i ];
			gMacOnAry.push( macName );
		}

		loadMacroSwtich();
	}

	function loadMacroSwtich(){ // turn on macro if it was on
		$( ".macSw" ).each( function (){
			$( this ).find( ".swOnLbl" ).removeClass( "swChked" );
			$( this ).find( ".swOffLbl" ).addClass( "swChked" );
		} );

		for ( i = 0 ; i < gMacOnAry.length ; i ++ ){
			var num = gMacOnAry[ i ].split( "_" )[ 1 ];
			var side = gMacOnAry[ i ].split( "_" )[ 2 ];
			var rowID = "#macRow_" + num;
			if ( side == "BUY" ){
				var bidOrAsk = ".bidSec";
			}else{
				var bidOrAsk = ".askSec";
			}
			$( rowID ).find( bidOrAsk ).find( ".macSw" ).find( ".swOffLbl" ).removeClass( "swChked" );
			$( rowID ).find( bidOrAsk ).find( ".macSw" ).find( ".swOnLbl" ).addClass( "swChked" );
		}
	}

	function setStopAll(){ // to cancel all orders and cancel all running algos
		var cnl = "O MREX CO DAY CANCEL ALL" + gLOType + " | O PATS CO DAY CANCEL ALL" + gLOType + " | O PATX CO DAY CANCEL ALL" + gLOType + " | O HKE CO DAY CANCEL ALL" + gLOType + " | O MREX CO DAY CANCEL ALL" + gSOType + " | O PATS CO DAY CANCEL ALL" + gSOType + " | O PATX CO DAY CANCEL ALL" + gSOType + "| O HKE CO DAY CANCEL ALL" + gSOType;

		var cmd = "CMD " + gInitUserID + " MACRO _STOPALL NAME _STOPALL CANCEL_ALGO ALL | " + cnl +
					" | DELAY 0 1000 { " + cnl + " }";

		pushCMD( cmd );
		showLog( "setStopAll" , cmd );

		// e.g. CMD 302 MACRO _STOPALL NAME _STOPALL CANCEL_ALGO ALL | O MREX CO DAY CANCEL ALLLO2 | O PATS CO DAY CANCEL ALLLO2 | O HKE CO DAY CANCEL ALLLO2 | O MREX CO DAY CANCEL ALLSO2 | O PATS CO DAY CANCEL ALLSO2 | O HKE CO DAY CANCEL ALLSO2 | MSG 302 MMALGO { Stopped All Algo. } | DELAY 0 1000 { O MREX CO DAY CANCEL ALLLO2 | O PATS CO DAY CANCEL ALLLO2 | O HKE CO DAY CANCEL ALLLO2 | O MREX CO DAY CANCEL ALLSO2 | O PATS CO DAY CANCEL ALLSO2 | O HKE CO DAY CANCEL ALLSO2 }
	}

	function setFuseStopAll(){
		var cnl = "O MREX CO DAY CANCEL ALL" + gLOType + " | O PATS CO DAY CANCEL ALL" + gLOType + " | O PATX CO DAY CANCEL ALL" + gLOType + " | O HKE CO DAY CANCEL ALL" + gLOType + " | O MREX CO DAY CANCEL ALL" + gSOType + " | O PATS CO DAY CANCEL ALL" + gSOType + " | O PATX CO DAY CANCEL ALL" + gLOType + " | O HKE CO DAY CANCEL ALL" + gSOType;

		var cmd = "CMD " + gInitUserID + " MACRO _FUSESTOPALL NAME _FUSESTOPALL CANCEL_ALGO ALL | " + cnl;

		pushCMD( cmd );
		showLog( "setFuseStopAll" , cmd );
	}

	function startStopAll( type ){ // call STOPALL macro
		$( ".panlStopNtcSec" ).stop( true , true );
		$( ".panlStopNtcSec" ).fadeIn();

		getSeqNum( type );

		var cmd = "CMD " + gInitUserID + " DO _STOPALL" + " SEQ " + gSeqNum;
		pushCMD( cmd );

		var cmd = "CMD " + gInitUserID + " DO CS_STOP_ALL" + " SEQ " + gSeqNum;
		pushCMD( cmd );

		showLog( "startStopAll" , cmd );
	}

	function setExtraStopAll(){ // a extra stop for handling MREX problem
		var cnl = "";
		var i = 0;

		$( ".macRow" ).each( function (){
			var rowID = "#" + $( this ).attr( 'id' );
			var numStr = rowID.substr( rowID.length - 2 );
			var ecn = $( rowID ).find( ".ecnDiv" ).find( ".current" ).html();
			ecn = ecn.replace( / /g , "" );
			var poi = gInitUserID + numStr;

			if ( i > 0 ){
				cnl += " | ";
			}
			var ord = "O " + ecn + " CO DAY CANCEL 0 POI " + poi;
			cnl += ord + "1" + " | " + ord + "2";

			i ++;
		} );

		var cmd = "CMD " + gInitUserID + " MACRO _STOPALL2 NAME _STOPALL2 CANCEL_ALGO ALL | " + cnl + " | MSG " + gInitUserID + " MMALGO { Stopped All Algo. } | DELAY 0 1000 { " + cnl + " }";

		pushCMD( cmd );
		showLog( "setExtraStopAll" , cmd );
	}

	function startExtraStopAll( type ){ // call extra STOPALL macro
		$( ".panlStopNtcSec" ).stop( true , true );
		$( ".panlStopNtcSec" ).fadeIn();

		getSeqNum( type );
		//var cmd = "CMD " + gInitUserID + " DO _STOPALL2" + " SEQ " + gSeqNum;
		var cmd = "CMD " + gInitUserID + " DO _STOPALL2";

		pushCMD( cmd );
		showLog( "startExtraStopAll" , cmd );
	}

	function stopExtraMacro(){
		if ( gOnAllMacro == 0 ) { // check the big on off switch is on or not
			return;
		}

		$( ".swMacOn" ).each( function (){
			var swOnLbl = $( this );

			var rowID = "#" + $( swOnLbl ).closest( ".macRow" ).attr( "id" );
			var numStr = rowID.substr( rowID.length - 2 );

			if ( $( swOnLbl ).closest( ".sideSec" ).hasClass( "bidSec" ) ){
				var side = "BUY";
			}else{
				var side = "SELL";
			}

			var macName = "algoMac_" + numStr + "_" + side;

			if ( gMacOnAry.indexOf( macName ) == -1 ){
				var cnlName = "algoCnl_" + numStr + "_" + side;
				stopMacro( cnlName );
			}
		} );
	}

	function turnOnExtraMacro( type ){ // turn on extra macro when change mode
		if ( gOnAllMacro == 0 ) { // check the big on off switch is on or not
			return;
		}

		var fillSide = gLastFillSide;
		var delayed = false;

		onMacro( type ); // if filled bid > turn on ask side macro first

		if ( !delayed ){
			return;
		}

		gDelayTurnOnTimer = setTimeout( function (){
			fillSide = "";
			onMacro( type ); // after delay , turn on another side macro
		} , gDelayTurnOn );

		function onMacro( type ){
			for ( i = 0 ; i < gMacOnAry.length ; i ++ ){
				var macName = gMacOnAry[ i ];
				var numStr = macName.split( "_" )[ 1 ];
				var side = macName.split( "_" )[ 2 ];
				var rowID = "#macRow_" + numStr;

				if ( side == "BUY" ){
					var bidOrAsk = ".bidSec";
				}else{
					var bidOrAsk = ".askSec";
				}

				var swOnLbl = $( rowID ).find( bidOrAsk ).find( ".macSw" ).find( ".swOnLbl" );

				if ( $( swOnLbl ).hasClass( "swMacOn" ) ){
					continue;
				}

				if ( type = "Auto" && fillSide == side ){
					delayed = true;
					continue;
				}
				startMacro( macName );
			}
		}
	}

	function checkParameter( name ){ // check lastest algo on off situation
		getDefaultParameter( name );

		var defer = $.Deferred();

		clearInterval( gCheckParameterInterval );
		gCheckParameterInterval = false;

		var interval = 300; // check received response every 0.3 second
		var wait = 3000; // wait 2 seconds for server response

		if ( !gCheckParameterInterval ){
			gCheckParameterInterval = setInterval( getParm , interval );
		}

		return defer;

		function getParm(){
			if ( wait <= 0 ){
				var msg = "Server response time out. Seq No.: " + gSeqNum + "."; // send a stop all notice if it is a leader

				sendTabMsg( msg );
				return;
			}

			if ( gGotDefaultParm == 0 ){
				wait -= interval;
				return;
			}

			clearInterval( gCheckParameterInterval );
			gCheckParameterInterval = false;

			defer.resolve( null );
			return defer;
		}
	}

	function uiClickAddRowBtn(){ // add a macro row by user
		var rowNum = gLastMmoNum + 1;
		var pips = gDefPips * gDefSymTs;

		if ( rowNum < 10 ){
			var rowNumStr = "0" + rowNum;
		}else{
			var rowNumStr = rowNum;
		}

		var plcOrdDly = gDefPlcOrdDly * 1000;
		var updFeq = gUpdFeq * 1000;
		var repOrdDly = gRepOrdDly * 1000;
		var ecn = gDefECN;
		var sym = gDefSym;
		var qty = gDefQty;
		var mCase = gDefCase;
		var spread = 0;
		var ordMethod = gDefOrdMethod;
		var ordSpeed = gDefOrdSpeed;
		var ordType = gDefOrdType;
		var symTs = gDefSymTs;

		for ( i = 0 ; i < 2 ; i ++ ){
			if ( i == 0 ){
				var side = "BUY";
				var sidePips = pips * -1;
			}else{
				var side = "SELL";
				var sidePips = pips;
			}

			var cmd = buildMacro( rowNumStr , side , sym , ecn , qty , sidePips , updFeq , plcOrdDly , repOrdDly , mCase , spread , ordMethod , ordSpeed , ordType , symTs );

			pushCMD( cmd.cmdMac ); // create two new macros ( buy and sell )
			pushCMD( cmd.cmdMacIn );
			pushCMD( cmd.cmdCnl );
			pushCMD( cmd.cmdLp );

			showLog( "uiClickAddRowBtn" , cmd.cmdMac );
			showLog( "uiClickAddRowBtn" , cmd.cmdCnl );
			showLog( "uiClickAddRowBtn" , cmd.cmdLp );
		}

		var rowID = "macRow_" + rowNumStr;
		addRowSeq( rowID );
	}

	function addRow( numStr ){ // add a new row if it is not exist
		if ( parseInt( numStr ) > gLastMmoNum ){
			gLastMmoNum = parseInt( numStr );
		}

		var rowHtml = "<div class = 'macRow''>" + gRowHtml + "</div>";
		$( ".panlSec" ).append( rowHtml );

		var macRow = $( ".panlSec" ).find( ".macRow:last" );
		$( macRow ).attr( 'id' , "macRow_" + numStr );
		setSldr( ".ctrlPipsDiv" , gDefPipsMin , gDefPipsMax , gDefPips ); // create drag slider for controls
		setSldr( ".ctrlQtyDiv" , gDefQtyMin , gDefQtyMax , gDefQty );

		$( macRow ).find( ".sldrBg" ).slider( { // set the slider
			orientation: "vertical" ,
			range: "min" ,
			create: function (){
				var val = $( this ).slider( "value" );
				$( this ).find( ".sldrBg" ).text( val );
			} ,
			slide: function ( event , ui ){
				var bidSign = "";
				var askSign = "";
				if ( $( this ).closest( ".ctrlDiv" ).hasClass( "ctrlPipsDiv" ) ){
					var ctrl = ".ctrlPipsDiv";
					bidSign = "-";
					askSign = "+";
				}else if ( $( this ).closest( ".ctrlDiv" ).hasClass( "ctrlQtyDiv" ) ){
					var ctrl = ".ctrlQtyDiv";
				}

				var rowID = "#" + $( this ).closest( ".macRow" ).attr( "id" );
				slideSldr( ".bidSec" , bidSign );
				slideSldr( ".askSec" , askSign );

				function slideSldr( side , sign ){
					var swDiv = $( rowID ).find( side ).find( ".macSw" );
					if ( $( swDiv ).find( ".swOnLbl" ).hasClass( "swMacOn" ) ){
						return;
					}
					$( rowID ).find( side ).find( ctrl ).find( ".txtIpt" ).val( sign + ui.value );
					$( rowID ).find( side ).find( ctrl ).find( ".sldrHd" ).text( sign + ui.value );
				}
			} ,
		} );

		var symECN = eval( "gSymOpt_" + gDefECN );
		var symList = "";

		$.each( symECN , function ( value ) {
			symList += "<option value = " + value + "> "+ value + "</option>";
		} );

		$( macRow ).find( ".symSlt" ).append( symList );
		$( macRow ).find( ".ecnSlt" ).val( gDefECN );
		$( macRow ).find( ".symSlt" ).val( gDefSym );
		$( macRow ).find( ".ecnSlt" ).foundationCustomForms(); // call foundationCustomForms to apply the change after select form was changed
		$( macRow ).find( ".symSlt" ).foundationCustomForms();

		$( macRow ).find( ".sprdSlidr" ).dragslider( { // create symbol spread slider
			range: true ,
			rangeDrag: true ,
			min: - ( gSpreadMax * 2 ) ,
			max: gSpreadMax * 2 ,
			values: [ -gSpreadMid , gSpreadMid ] ,

			slide: function ( event , ui ){
				var sldr = $( this );
				var bid = ui.values[ 0 ];
				var ask = ui.values[ 1 ];
				var rowID = "#" + $( sldr ).closest( ".macRow" ).attr( "id" );

				var bidAskVal = getMinMaxBidAskPips( rowID );
				var maxBid = bidAskVal.maxBid;
				var minAsk = bidAskVal.minAsk;

				var newBid = bid + gSpreadMid;
				var newAsk = ask - gSpreadMid;

				if ( ( newBid + maxBid ) > 0 ){
					return false;
				}
				if ( ( newAsk + minAsk ) < 0 ){
					return false;
				}

				$( sldr ).closest( ".cfgSymSprdDiv" ).find( ".bidSprdDiv" ).find( ".txtIpt" ).val( newBid );
				$( sldr ).closest( ".cfgSymSprdDiv" ).find( ".askSprdDiv" ).find( ".txtIpt" ).val( newAsk );
			}
		} );

		$( macRow ).find( ".stopAllQtySn" ).text( gGuaFuseStopQtyParm );
		$( macRow ).show();

		function setSldr( ctrlClass , minVal , MaxVal , Val ){
			$( macRow ).find( ctrlClass ).find( ".sldrBg" ).slider( {
				min: minVal ,
				max: MaxVal ,
				value: Val ,
			} );
			var bidSign = "";
			var askSign = "";

			if ( ctrlClass == ".ctrlPipsDiv" ){
				bidSign = "-";
				askSign = "+";
			}
			$( macRow ).find( ".bidSec" ).find( ctrlClass ).find( ".txtIpt" ).val( bidSign + Val );
			$( macRow ).find( ".askSec" ).find( ctrlClass ).find( ".txtIpt" ).val( askSign + Val );
			$( macRow ).find( ".bidSec" ).find( ctrlClass ).find( ".sldrHd" ).text( bidSign + Val );
			$( macRow ).find( ".askSec" ).find( ctrlClass ).find( ".sldrHd" ).text( askSign + Val );
		}
	}

	function updateRowInfo( cmrAry ){ // hande algo macro message
		// e.g. CMD 302 MACRO algoMac_01_BUY NAME algoMac_01_BUY DEFAULT MODE 9 CANCEL_ALGO algoMac_01_BUY | O MREX CO DAY CANCEL 0 POI 302011 SAVEBID algoPx_01_BUY | DEFAULT SYM CME/CMX_GLD/DEC17 O MREX LO2 DAY BUY CME/CMX_GLD/DEC17 QTY 2 AT BID - 0.1 POI 302011 | DELAY 0 3000 { DO algoLp_01_BUY SEQ 0 } | IF_DONE 1 MREX LAST_TRADE_OID 1 { O MREX CO DAY CANCEL 0 POI 302011 } | IF_DONE 1 MREX LAST_TRADE_OID 2 { NAME algoMac_01_BUY DELAY 0 6000 { DO algoMac_01_BUY SEQ 0 } } | IF_DONE 1 MREX LAST_TRADE_OID 4 { NAME algoMac_01_BUY DELAY 0 0 { DO algoMac_01_BUY SEQ 0 } } | IF_DONE 1 MREX LAST_TRADE_OID 8 { CANCEL_ALGO algoMac_01_BUY }
		var macName = cmrAry[ 3 ];
		var numStr = macName.split( "_" )[ 1 ];
		var side = macName.split( "_" )[ 2 ];
		var mCase = parseFloat( cmrAry[ 8 ] );
		var repOrdDly = gRepOrdDly;

		var ecn = "";
		var ordType = "";
		var ordTIF = "";
		var ordSide = "";
		var sym = "";
		var qty = "";
		var ordMethod = "";
		var sign = "";
		var pips = "";
		var slip = "";
		var symTs = "";
		var updFeq = "";

		if ( mCase == 1 ){ // Stop on Partial Fill
			ecn = cmrAry[ 25 ];
			ordType = cmrAry[ 26 ];
			ordTIF = cmrAry[ 27 ];
			ordSide = cmrAry[ 28 ];
			sym = cmrAry[ 29 ];
			qty = parseFloat( cmrAry[ 31 ] );
			ordMethod = cmrAry[ 33 ];
			sign = cmrAry[ 34 ];
			pips = parseFloat( cmrAry[ 35 ] );
			if ( ordType == gLOType ){
				updFeq = parseFloat( cmrAry[ 44 ]/1000 );
			}else if ( ordType == gSOType ){
				slip = parseFloat( cmrAry[ 37 ] );
				symTs = parseFloat( cmrAry[ 39 ] );
				updFeq = parseFloat( cmrAry[ 50 ]/1000 );
			}

		}else if ( mCase == 2 ){ // stop on Complete Fill only
			ecn = cmrAry[ 25 ];
			ordType = cmrAry[ 26 ];
			ordTIF = cmrAry[ 27 ];
			ordSide = cmrAry[ 28 ];
			sym = cmrAry[ 29 ];
			qty = parseFloat( cmrAry[ 31 ] );
			ordMethod = cmrAry[ 33 ];
			sign = cmrAry[ 34 ];
			pips = parseFloat( cmrAry[ 35 ] );
			if ( ordType == gLOType ){
				updFeq = parseFloat( cmrAry[ 44 ]/1000 );
				repOrdDly = parseFloat( cmrAry[ 72 ]/1000 );
			}else if ( ordType == gSOType ){
				slip = parseFloat( cmrAry[ 37 ] );
				symTs = parseFloat( cmrAry[ 39 ] );
				updFeq = parseFloat( cmrAry[ 50 ]/1000 );
				repOrdDly = parseFloat( cmrAry[ 78 ]/1000 );
			}

		}else if ( mCase == 9 ){ // Non-Stop
			ecn = cmrAry[ 25 ];
			ordType = cmrAry[ 26 ];
			ordTIF = cmrAry[ 27 ];
			ordSide = cmrAry[ 28 ];
			sym = cmrAry[ 29 ];
			qty = parseFloat( cmrAry[ 31 ] );
			ordMethod = cmrAry[ 33 ];
			sign = cmrAry[ 34 ];
			pips = parseFloat( cmrAry[ 35 ] );
			if ( ordType == gLOType ){
				updFeq = parseFloat( cmrAry[ 44 ]/1000 );
				repOrdDly = parseFloat( cmrAry[ 78 ]/1000 );
			}else if ( ordType == gSOType ){
				slip = parseFloat( cmrAry[ 37 ] );
				symTs = parseFloat( cmrAry[ 39 ] );
				updFeq = parseFloat( cmrAry[ 50 ]/1000 );
				repOrdDly = parseFloat( cmrAry[ 84 ]/1000 );
			}

		}else if ( mCase == 10 ){ // Stop All ALGO
			ecn = cmrAry[ 25 ];
			ordType = cmrAry[ 26 ];
			ordTIF = cmrAry[ 27 ];
			ordSide = cmrAry[ 28 ];
			sym = cmrAry[ 29 ];
			qty = parseFloat( cmrAry[ 31 ] );
			ordMethod = cmrAry[ 33 ];
			sign = cmrAry[ 34 ];
			pips = parseFloat( cmrAry[ 35 ] );
			if ( ordType == gLOType ){
				updFeq = parseFloat( cmrAry[ 44 ]/1000 );
			}else if ( ordType == gSOType ){
				slip = parseFloat( cmrAry[ 37 ] );
				symTs = parseFloat( cmrAry[ 39 ] );
				updFeq = parseFloat( cmrAry[ 50 ]/1000 );
			}

		}else if ( mCase == 11 ){ // Stop All and flat
			ecn = cmrAry[ 25 ];
			ordType = cmrAry[ 26 ];
			ordTIF = cmrAry[ 27 ];
			ordSide = cmrAry[ 28 ];
			sym = cmrAry[ 29 ];
			qty = parseFloat( cmrAry[ 31 ] );
			ordMethod = cmrAry[ 33 ];
			sign = cmrAry[ 34 ];
			pips = parseFloat( cmrAry[ 35 ] );
			if ( ordType == gLOType ){
				updFeq = parseFloat( cmrAry[ 44 ]/1000 );
			}else if ( ordType == gSOType ){
				slip = parseFloat( cmrAry[ 37 ] );
				symTs = parseFloat( cmrAry[ 39 ] );
				updFeq = parseFloat( cmrAry[ 50 ]/1000 );
			}

		}else if ( mCase == 12 ){ // Guardian Algo
			ecn = cmrAry[ 25 ];
			ordType = cmrAry[ 26 ];
			ordTIF = cmrAry[ 27 ];
			ordSide = cmrAry[ 28 ];
			sym = cmrAry[ 29 ];
			qty = parseFloat( cmrAry[ 31 ] );
			ordMethod = cmrAry[ 33 ];
			sign = cmrAry[ 34 ];
			pips = parseFloat( cmrAry[ 35 ] );
			if ( ordType == gLOType ){
				updFeq = parseFloat( cmrAry[ 44 ]/1000 );
			}else if ( ordType == gSOType ){
				slip = parseFloat( cmrAry[ 37 ] );
				symTs = parseFloat( cmrAry[ 39 ] );
				updFeq = parseFloat( cmrAry[ 50 ]/1000 );
			}

		}else{
			return;
		}

		if ( ! validateParameter( "mCase" , mCase , mCase ) ||
			! validateParameter( "ecn" , ecn , ecn ) ||
			! validateParameter( "ordType" , ordType , ordType ) ||
			! validateParameter( "ordTIF" , ordTIF , ordTIF ) ||
			! validateParameter( "ordSide" , ordSide , ordSide ) ||
			! validateParameter( "sym" , sym , sym ) ||
			! validateParameter( "qty" , qty , qty ) ||
			! validateParameter( "ordMethod" , ordMethod , ordMethod ) ||
			! validateParameter( "sign" , sign , sign ) ||
			! validateParameter( "pips" , pips , pips ) ||
			! validateParameter( "updFeq" , updFeq , updFeq ) ||
			! validateParameter( "slip" , slip , slip ) ||
			! validateParameter( "symTs" , symTs , symTs ) ||
			! validateParameter( "updFeq" , updFeq , updFeq ) ){

			throw new Error( "Got NaN or undefined value when reading macro CMR" );
		}

		var rowID = "#macRow_" + numStr;
		var symTs = getSymItem( ecn , sym ).ts;
		var symLs = getSymItem( ecn , sym ).ls;
		var symSpread = getSymSpread( ecn , sym );

		if ( side == "BUY" ){
			var bidOrAsk = ".bidSec";
			var spread = symSpread[ 0 ];
		}else{
			var bidOrAsk = ".askSec";
			var spread = symSpread[ 1 ];
		}

		pips = Math.round( parseFloat( sign + pips ) / symTs );
		var sprdPips = pips - spread; // calulate actually pips
		var qty = Math.round( qty / symLs );

		if ( $( rowID ).length == 0 ){ // add a row if the row is not exist
			addRow( numStr );
			showSymSpread();
		}

		setSldr( ".ctrlPipsDiv" , sprdPips , pips ); // set slider
		setSldr( ".ctrlQtyDiv" , qty , qty );

		var oEcn = $( rowID ).find( ".ecnDiv" ).find( ".current" ).html();
		oEcn = oEcn.replace( / /g , "" ); // read current ecn

		if ( ecn != oEcn ){ // check whether ecn setting is changed or not
			$( rowID ).find( ".ecnSlt" ).val( ecn );
			$( rowID ).find( ".ecnSlt" ).trigger( "change" );
			$( rowID ).find( ".symSlt" ).empty();
			var symECN = eval( "gSymOpt_" + ecn );
			var symList = "";
			$.each( symECN , function ( value ) {
				symList += "<option value = " + value + "> "+ value + "</option>";
			} );
			$( rowID ).find( ".symSlt" ).append( symList ); // update symbol list after changing ecn setting
		}

		var oSym = $( rowID ).find( ".symDiv" ).find( ".current" ).html(); // read current symbol
		oSym = oSym.replace( / /g , "" );

		if ( sym != oSym ){ // check whether symbol setting is changed or not
			$( rowID ).find( ".symSlt" ).val( sym );
			$( rowID ).find( ".symSlt" ).trigger( "change" );
		}

		var oCase = parseInt( $( rowID ).find( bidOrAsk ).find( ".ifdSwChked" ).find( ".ifdSwIpt" ).val() ); // read macro if done action

		if ( mCase != oCase ){
			var ifdSwLbl = $( rowID ).find( bidOrAsk ).find( ".ifdSwDiv" ).find( ".ifdSwLbl" );
			var ifdSwBg = $( rowID ).find( bidOrAsk ).find( ".ifdSwBg" );
			$( ifdSwLbl ).removeClass( "ifdSwChked" );

			// if ( mCase == 1 ){ // Stop on Partial Fill
				// $( ifdSwLbl ).eq( 0 ).addClass( "ifdSwChked" );
				// $( ifdSwBg ).css( "top" , "-109px" );
			// }else if ( mCase == 2 ){ // stop on Complete Fill only
				// $( ifdSwLbl ).eq( 1 ).addClass( "ifdSwChked" );
				// $( ifdSwBg ).css( "top" , "-81px" );
			if ( mCase == 9 ){ // Non-Stop
				$( ifdSwLbl ).eq( 0 ).addClass( "ifdSwChked" );
				$( ifdSwBg ).css( "top" , "-122px" );
			}else if ( mCase == 10 ){ // Stop All ALGO
				$( ifdSwLbl ).eq( 1 ).addClass( "ifdSwChked" );
				$( ifdSwBg ).css( "top" , "-95px" );
			}else if ( mCase == 11 ){ // Stop All ALGO
				$( ifdSwLbl ).eq( 2 ).addClass( "ifdSwChked" );
				$( ifdSwBg ).css( "top" , "-68px" );
			}else if ( mCase == 12 ){ // Stop All ALGO
				$( ifdSwLbl ).eq( 3 ).addClass( "ifdSwChked" );
				$( ifdSwBg ).css( "top" , "-41px" );
			}
		}

		var oUpdFeq = parseFloat( $( rowID ).find( ".cfgRowUpdFeqDiv" ).find( ".txtIpt" ).val() ); // read current macro update fequency

		if ( updFeq != oUpdFeq ){
			$( rowID ).find( ".cfgRowUpdFeqDiv" ).find( ".txtIpt" ).val( updFeq ); // update update fequency if it is changed
		}

		var oRepOrdDly = parseFloat( $( rowID ).find( ".cfgRowRepOrdDlyDiv" ).find( ".txtIpt" ).val() ); // read current replace order delay

		if ( repOrdDly != oRepOrdDly ){ // update replace order delay if it is changed
			if ( mCase == 2 || mCase == 9 ){
				$( rowID ).find( ".cfgRowRepOrdDlyDiv" ).find( ".txtIpt" ).val( repOrdDly );
			}else if ( oRepOrdDly == 0 ){
				$( rowID ).find( ".cfgRowRepOrdDlyDiv" ).find( ".txtIpt" ).val( repOrdDly );
			}
		}

		var ordMethodSec = $( rowID ).find( ".cfgRowOrdMethodDiv" );

		ordMethodAry = ordMethod.split( "_" );
		if ( ordMethodAry.length == 3 ){ // e.g. BID_SLOW_10 or ASK_MA_20
			var ordMethod = ordMethodAry[ 1 ]; // SLOW or MA
			var ordSpeed = ordMethodAry[ 2 ]; // e.g. 10 or 30 seconds

			$( ordMethodSec ).find( ".txtIpt" ).val( ordSpeed );
			$( ordMethodSec ).find( ".cfgSlet" ).val( ordMethod );
			$( ordMethodSec ).find( ".cfgSlet" ).trigger( "change" ); // call trigger change to apply the select form change
		}else{ // e.g. BID or ASK
			$( ordMethodSec ).find( ".txtIpt" ).val( gDefOrdSpeed );
			$( ordMethodSec ).find( ".cfgSlet" ).val( "Normal" );
			$( ordMethodSec ).find( ".cfgSlet" ).trigger( "change" );
		}

		var cfgSlet = $( rowID ).find( ".cfgRowOrdTypeDiv" ).find( ".cfgSlet" ); // read order type ( LO or SO )
		$( cfgSlet ).val( ordType );
		$( cfgSlet ).trigger( "change" );

		$( rowID ).find( ".rowInfoID" ).text( rowID );

		function setSldr( ctrlClass , sldrVal , ctrlVal ){ // set slider
			$( rowID ).find( bidOrAsk ).find( ctrlClass ).find( ".ctrlVal" ).val( ctrlVal );
			if ( ctrlClass == ".ctrlPipsDiv" && sldrVal > 0 ){
				sldrVal = "+" + sldrVal;
			}
			var oSldrVal = Math.abs( $( rowID ).find( bidOrAsk ).find( ctrlClass ).find( ".sldrHd" ).text() );
			if ( sldrVal == oSldrVal ){
				return;
			}
			$( rowID ).find( bidOrAsk ).find( ctrlClass ).find( ".sldrHd" ).text( sldrVal );
			$( rowID ).find( bidOrAsk ).find( ctrlClass ).find( ".txtIpt" ).val( sldrVal );
		}

		///////////// temp setting ////////
		$( ordMethodSec ).find( ".txtIpt" ).val( gOrdSpeed );
		////////////////////////////////////
	}

	function uiClickDelRowBtn(){ // delete a row by user
		var runningMac = 0;
		var delRowBtn = this;

		$( delRowBtn ).closest( ".macRow" ).find( ".sideSec" ).each( function (){
			var swOnLbl = $( this ).find( ".macSw" ).find( ".swOnLbl" );
			if ( !$( swOnLbl ).hasClass( "swMacOn" ) ){
				return;
			}
			$( swOnLbl ).stop( true , true );
			$( swOnLbl ).addClass( "swOnNotice" , 300 );
			$( swOnLbl ).delay( 300 ).removeClass( "swOnNotice" , 300 );
			runningMac = 1;
			return false;
		} );

		if ( runningMac != 0 ){
			return
		}

		var rowID = $( delRowBtn ).closest( ".macRow" ).attr( "id" );
		var numStr = rowID.substr( rowID.length - 2 );
		var buyMacName = "algoMac_" + numStr + "_BUY";
		var buyCnlName = "algoCnl_" + numStr + "_BUY";
		var buyLpName = "algoLp_" + numStr + "_BUY";
		var buyPxName = "algoPx_" + numStr + "_BUY";

		var sellMacName = "algoMac_" + numStr + "_SELL";
		var sellCnlName = "algoCnl_" + numStr + "_SELL";
		var sellLpName = "algoLp_" + numStr + "_SELL";
		var sellPxName = "algoPx_" + numStr + "_SELL";

		getSeqNum( gByManual );

		deleteMacro( buyMacName ); // delete bid side macro
		deleteMacro( buyCnlName );
		deleteMacro( buyLpName );
		deleteMacro( buyPxName );
		deleteMacro( sellMacName ); // delete ask side macro
		deleteMacro( sellCnlName );
		deleteMacro( sellLpName );
		deleteMacro( sellPxName );

		removeOnMacroList( buyMacName ); // remove macro from on macro list
		removeOnMacroList( sellMacName );

		removeRowSeq( rowID ); // remove macro from macro sequence list
	}

	function deleteRow( cmrAry ){ // delete the row from UI
		// e.g. CMR 302 MACRO_DELETE algoMac_07_BUY
		var numStr = cmrAry[ 3 ].split( "_" )[ 1 ];
		var rowID = "#macRow_" + numStr;

		$( rowID ).fadeOut( 300 , function (){
			$( this ).remove();
		} );
	}

	function uiClickDeleteAllMacro(){ // delete all macro by user
		uiClickDeleteAllMode();
		startStopAll( gByManual );

		getSeqNum( gByManual );
		//var cmd = "CMD " + gInitUserID + " MACRO_DELETE_ALL" + " SEQ " + gSeqNum;
		var cmd = "CMD " + gInitUserID + " MACRO_DELETE_ALL";

		pushCMD( cmd );

		showLog( "uiClickDeleteAllMacro" , cmd );

		$( ".macRow" ).remove(); // delete all rows
		clearGlobalVal(); // clear all variables from browser

		setTimeout( function (){
			getMacro(); // get macro again to ensure everything are deleted
		} , 500 );
	}

	/////////////// panel timer function ////////////

	function onPanelTimer(){ // check open order , pnl , netPos and mode every second
		resetShortInterval( 1 );
		resetLongInterval( 1 );
		noTradeGuardian();
	}

	function shortPanelInterval(){
		lossFromTopGuardian();
		openOrderGuardian();
		netPosGuardian();
		maxLossGuardian();
		autoCheckChangeMode();
		saveMaxProfit();
	}

	function longPanelInterval(){
		checkIsLeader();
	}

	function resetShortInterval( run ){
		clearInterval( gPanelShortInterval ); // stop panel timer

		var delay = Math.random() * 1000; // 0 - 1 second
		setTimeout( function (){
			clearInterval( gPanelShortInterval );
			gPanelShortInterval = false;

			if ( gPanelShortInterval ){
				return;
			}

			if ( run == 1 ){
				shortPanelInterval();
			}

			gPanelShortInterval = setInterval( shortPanelInterval , 1000 ); // 1000 = 1 seconds

		} , delay );
	}

	function resetLongInterval( run ){
		clearInterval( gPanelLongInterval );

		var delay = 3000 + Math.random() * 5000; // 3 - 8 second
		setTimeout( function (){
			clearInterval( gPanelLongInterval );
			gPanelLongInterval = false;

			if ( gPanelLongInterval ){
				return;
			}

			if ( run == 1 ){
				longPanelInterval();
			}

			gPanelLongInterval = setInterval( longPanelInterval , 60000 );
		} , delay );
	}

	/////////////// algo guardian function ////////////

	function openOrderGuardian(){ // check open order number
		if ( gReloaded == 1 || gDisconnected == 1 ){ // check if the panel is reloaded/disconnected or not
			return;
		}
		if ( gGuaOpenOrdOn == 0 ){ // check the open order guard function is turnned on or not
			return;
		}

		var openOrder = getOpenOrderNum(); // get open order number

		var algoNum = getRunningAlgoNum(); // get running algo number
		var limit = algoNum + gDefOpenOrdBuff ; // default buffer = 3

		if ( openOrder <= limit ){ // check whether open order number is too much or not
			return;
		}

		var type = "OPENORDER";
		var msg = "Stop All Algo. Exceed Open Order Limit. Number of Algo: " + algoNum + ". Number of Open Orders: " + openOrder + "."; // send a stop all notice if it is a leader

		triggerStopGuardian( type , msg , gGuaOpenOrdCut );
	}

	function maxLossGuardian(){ // guard maximum loss
		if ( gReloaded == 1 || gDisconnected == 1 ){ // check if the panel is reloaded/disconnected or not
			return;
		}

		if ( gGuaMaxLossOn == 0 ){ // check the guard maximum loss function is turnned on or not
			return;
		}

		var pnl = getAccPnL(); // get current PnL

		if ( pnl >= gGuaMaxLossParm ){
			return;
		}

		var type = "MAXLOSS";
		var msg = "Stop All Algo. Exceed Maximum Loss Limit. Current PnL: " + pnl + ". Maximum Loss Limit: " + gGuaMaxLossParm + ".";

		triggerStopGuardian( type , msg , gGuaMaxLossCut );
	}

	function lossFromTopGuardian(){ // check account daily loss from top. e.g. 1000 > 4000 > 2000 = loss from top 2000
		if ( gReloaded == 1 || gDisconnected == 1 ){ // check if the panel is reloaded/disconnected or not
			return;
		}
		if ( gGuaLossFromTopOn == 0 ){ // check the guard maximum loss from top function is turnned on or not
			return;
		}

		var pnl = getAccPnL(); // get current PnL

		var lossFromTop = pnl - gMaxProfit;
		if ( lossFromTop >= gGuaLossFromTopParm ){
			return;
		}

		var type = "LOSSFROMTOP";
		var msg = "Stop All Algo. Exceed Maximum Loss From Top Limit. Loss From Top: " + lossFromTop + ". Maximum Loss From Top Limit: " + gGuaLossFromTopParm + ".";

		triggerStopGuardian( type , msg , gGuaLossFromTopCut );
	}

	function filledQtyGuardian( cmrAry ){ // guard filled quantity
		// e.g. CMR 300 TOTAL_FILL MREX CME/CMX_GLD/AUG17 15 46 SELL 1 1246.7 OPX 1246.4
		if ( gReloaded == 1 || gDisconnected == 1 ){ // check if the panel is reloaded/disconnected or not
			return;
		}
		if ( gGuaFilledQtyOn != 1 ){ // check guard buy sell quantity function is on or not
			return;
		}

		var rEcn = cmrAry[ 3 ];
		var rSym = cmrAry[ 4 ];

		$( ".infQtyDiv" ).each( function (){
			var infQtyDiv = $( this );

			var ecn = $( infQtyDiv ).closest( ".rowSec" ).find( ".rowInfSec" ).find( ".ecnDiv " ).find( ".current" ).html();
			ecn = ecn.replace( / /g , "" );

			var sym = $( infQtyDiv ).closest( ".rowSec" ).find( ".rowInfSec" ).find( ".symDiv " ).find( ".current" ).html();
			sym = sym.replace( / /g , "" ); // get row symbol

			if ( rEcn != ecn ){ // check the symbol is the same or not
				return;
			}

			if ( rSym != sym ){ // check the symbol is the same or not
				return;
			}

			var qty = Math.abs( $( infQtyDiv ).find( ".txtIpt" ).val() ); // read number of filled quantity for this macro

			if ( qty <= gGuaFilledQtyParm ){
				return;
			}

			if ( $( infQtyDiv ).closest( ".sideSec" ).hasClass( "bidSec" ) ){
				var side = "BUY";
			}else{
				var side = "SELL";
			}

			var type = "TRADEQTY";
			var msg = "Stop All Algo. Exceed Fill Quantity Limit On " + ecn + " " + sym + ". Filled " + side + " Quantity: " + qty + " . Fill Quantity Limit: " + gGuaFilledQtyParm + ".";

			triggerStopGuardian( type , msg , gGuaFilledQtyCut );

			return false;
		} );
	}

	function netPosGuardian(){ // guard net position
		if ( gReloaded == 1 || gDisconnected == 1 ){ // check if the panel is reloaded/disconnected or not
			return;
		}
		if ( gGuaNetPosOn != 1 ){
			return;
		}

		var symAry = getSymAry(); // get symbol array;

		for ( i = 0 ; i < symAry.length ; i ++ ){
			var ecn = symAry[ i ][ 0 ];
			var sym = symAry[ i ][ 1 ];
			var netPos = getSymNetPos( ecn , sym ).netPos;

			if ( netPos == 0 || ! $.isNumeric( netPos ) ){
				continue;
			}

			if ( Math.abs( netPos ) <= gGuaNetPosParm ){
				continue;
			}

			var type = "NETPOS";
			var msg = "Stop All Algo. Exceed Net Position Limit On " + sym + ". Net Position: " + netPos + " . Net Position Limit: " + gGuaNetPosParm + ".";

			triggerStopGuardian( type , msg , gGuaNetPosCut );

			break;
		}
	}

	function slowFedGuardian( cmrAry ){ // guard slow feed
		// e.g. CMR 300 TOTAL_FILL MREX CME/CMX_GLD/AUG17 15 46 SELL 1 1246.7 OPX 1246.4
		// e.g. CMR 302 TOTAL_FILL MREX CME/CMX_GLD/DEC17 13 9 BUY 1 1268.5 OPX UNKNOWN
		if ( gOnAllMacro == 0 ) { // check the big on off switch is on or not
			return;
		}
		if ( gReloaded == 1 || gDisconnected == 1 ){ // check if the panel is reloaded/disconnected or not
			return;
		}
		if ( gGuaSlowFedOn == 0 ){
			return;
		}
		if ( cmrAry.length < 11 ){
			return;
		}
		if ( cmrAry[ 11 ] == "UNKNOWN" || cmrAry[ 11 ] == "NIL" ){
			return; // manual hit
		}

		if ( cmrAry[ 13 ] == "algoFlat" ){
			return; // cut loss order
		}

		var ecn = cmrAry[ 3 ];
		var sym = cmrAry[ 4 ];
		var side = cmrAry[ 7 ];
		var fillPx = parseFloat( cmrAry[ 9 ] );
		var ordPx = parseFloat( cmrAry[ 11 ] );
		var dip = parseFloat( getSymItem( ecn , sym ).dip );

		var ts = parseFloat( getSymItem( ecn , sym ).ts ); // get tick size

		if ( isNaN( fillPx ) || isNaN( ordPx ) || isNaN( ts ) || isNaN( dip ) ){
			var msg = "Error On Reading Filled Price Msg. filled px : " + fillPx + " . order px : " + ordPx + " . tick size : " + ts + " . Decimal Places : " + dip;
			sendLogMsg( msg );
			return;
		}

		fillPx = fillPx.toFixed( dip );
		ordPx = ordPx.toFixed( dip );

		if ( side == "BUY" ){
			if ( fillPx >= ordPx ){
				return;
			}
		}else{
			if ( fillPx <= ordPx ){
				return;
			}
		}


		if ( gGuaSlowFedCount == 0 ) {
			var msg = "Better Filled " + side + " " + ecn + " " + sym + " Order. Order Price: " + ordPx + ". Filled Price: " + fillPx;

			sendLogMsg( msg );

			gGuaSlowFedCount += 1;

			clearTimeout( gGuaSlowFedTimer );
			gGuaSlowFedTimer = setTimeout( function (){
				gGuaSlowFedCount = 0;
			} , 60000 );

			return;
		}

		var type = "SLOWFEED";
		var msg = "Stop All Algo. Slow Price Feed. Better Filled " + side + " " + ecn + " " + sym + " Order Twice in One Minute. Order Price: " + ordPx + ". Filled Price: " + fillPx;

		triggerStopGuardian( type , msg , gGuaSlowFedCut );
	}

	function twiceFillGuardian( msg ){
		// e.g. BUY Twice in 1 minute , Same Algo FullyFilled > 1
		if ( gReloaded == 1 || gDisconnected == 1 ){ // check if the panel is reloaded/disconnected or not
			return;
		}
		if ( gGuaTwiceFillOn == 0 ){
			return;
		}

		var side = msg.split( "_" )[ 0 ];
		if( side != "BUY" && side != "SELL" ){
			return;
		}

		var type = "TWICEFILL";
		var msg = "Stop All Algo. Filled " + side + " Order Twice Within One Minute.";

		triggerStopGuardian( type , msg , gGuaTwiceFillCut );
	}

	function volumeGuardian( msg ){
		//e.g. Volume Update: CME/CMX_GLD/DEC17 2003
		if ( gReloaded == 1 || gDisconnected == 1 ){ // check if the panel is reloaded/disconnected or not
			return;
		}
		if ( gGuaVolumeOn != 1 ){
			return;
		}

		var msgAry = msg.split( " " );

		if ( msgAry.length < 4 ){
			return;
		}

		var sym = msgAry[ 2 ];
		var vol = msgAry[ 3 ];

		var symAry = getSymAry(); // get symbol array;

		var sameSym = 0;
		for ( i = 0 ; i < symAry.length ; i ++ ){
			if ( symAry[ i ][ 1 ] != sym ){
				continue;
			}

			sameSym = 1;
			break;
		}

		if ( sameSym == 0 ){
			return;
		}

		if ( vol > gGuaVolumeParm ){
			if ( gPause == 1 ){
				return;
			}

			if ( gOnAllMacro == 0 ) { // check the big on off switch is on or not
				return;
			}

			var type = "VOLUME";
			var msg = "Stop All Algo. Exceed Average Volume Limit On " + sym + ". Average Volume: " + vol + " . Average Volume Limit: " + gGuaVolumeParm + ".";

			triggerStopGuardian( type , msg , gGuaVolumeCut );
			return;
		}

		if ( vol < gGuaResumeVolumeParm ){
			if ( gGuaResumeVolumeOn == 0 ){
				return;
			}

			if ( gPause == 0 ){
				return;
			}

			if ( gOnAllMacro == 1 ) { // check the big on off switch is on or not
				return;
			}

			var type = "VOLUMERESUME";
			var msg = "Resume All Algo. Average Volume Dropped to " + vol + " On " + sym + ". Average Volume Limit: " + gGuaResumeVolumeParm + ".";

			triggerOnGuardian( type , msg );
		}
	}

	function onAlgoGuardian(){ // check if there is any open order or running algo before trunning on panel
		if ( gGuaOpenOrdOn == 1 ){ 	// check outstanding open order
			var orderNum = getOpenOrderNum(); // get open order number

			if ( orderNum != 0 ){
				$( ".cfmCvr" ).show(); // show a notice and cover if there is a algo is still running before
				$( ".cfmOnAlgoDiv" ).show();
				return false;
			}
		}

		if ( gRunning != 0 ){ // check if there is a algo is still running
			$( ".cfmCvr" ).show(); // show a notice and cover if there is a algo is still running before
			$( ".cfmOnAlgoDiv" ).show();
			return false;
		}

		return true;
	}

	function triggerStopGuardian( type , msg , autoCut ){ // trigger guardian to stop all algo
		if ( gOnAllMacro == 0 && ( autoCut == 0 || gGuaAlgoOffFlatOn == 0 ) ){
			return;
		}

		$.when( checkIsLeader() ).then( function (){ // check itself is leader
			if ( gIsLeader == 0 ){
				return;
			}

			var delay = 0;

			if ( gOnAllMacro == 1 ){
				startStopAll( gByFunction ); // execute guardian
				setDefaultParameter( "CS_GUARDIAN_ALLOW_TRADE" , 0 );
				saveCSNotAllowTradeDay();
				delay = 900;
			}else{
				delay = 0;
			}

			setTimeout( function (){ // wait 0.9 second to make sure leader is also triggerd this guardian
				sendTabMsg( msg );
				sendTestMsg( msg );

				sendMsg( gInitUserID , "MMNOTICE" , "GUARD_" + type + " " + msg ); // send triggered guardian notice

				//if ( type == "MAXLOSS" || type == "LOSSFROMTOP" || type == "OPENORDER" || type == "TRADEQTY" || type == "NETPOS" ){
				if ( type == "MAXLOSS" || type == "OPENORDER" || type == "TRADEQTY" || type == "NETPOS" ){
					setMacro( "_GUA_" + type + "_ON" , 0 );
				}else if ( type == "LOSSFROMTOP"  ){
					renewMaxProfit();
				}


				if ( type == "MAXLOSS" ){
					gGuaMaxLossOn = 0;
				// }else if ( type == "LOSSFROMTOP" ){
					// gGuaLossFromTopOn = 0;
				}else if ( type == "OPENORDER" ){
					gGuaOpenOrdOn = 0;
				}else if ( type == "TRADEQTY" ){
					gGuaFilledQtyOn = 0;
				}else if ( type == "NETPOS" ){
					gGuaNetPosOn = 0;
				}

				if ( gGuaResumeVolumeOn == 1 && type == "VOLUME" ){
					gPause = 1;
					setDefaultParameter( "_PAUSE" , gPause );

					var type2 = "VOLUMEPAUSE";
					var msg2 = "Algo Paused. Resume If Average Volume Drop to " + gGuaResumeVolumeParm + ".";

					sendMsg( gInitUserID , "MMNOTICE" , "GUARD_" + type2 + " " + msg2 ); // send triggered guardian notice
				}

				if ( autoCut == 1 ){
					autoCutGuardian( type );
				}
			} , delay );
		} );
	}

	function triggerOnGuardian( type , msg ){ // trigger guardian to stop all algo
		sendLogMsg( msg ); // send a log msg to server even it is not a leader for debug

		gGotDefaultParm = 0;
		gOnAllMacro = 0;

		$.when( checkParameter( "_ONALL_MACRO" ) ).then( function (){ // get lastest server status
			if ( gOnAllMacro == 1 ){ // // check the big on off switch is on or not again
				return;
			}
			$.when( checkParameter( "_PAUSE" ) ).then( function (){
				if ( gPause == 0 ){
					return;
				}
				setTimeout( function (){ // wait 0.9 second to make sure leader is also triggerd this guardian
					$.when( checkIsLeader() ).then( function (){ // check itself is leader
						if ( gIsLeader == 0 ){
							return;
						}

						gPause = 0;
						setDefaultParameter( "_PAUSE" , gPause );

						turnOnPanel();

						sendTabMsg( msg );
						sendMsg( gInitUserID , "MMNOTICE" , "GUARD_" + type + " " + msg ); // send triggered guardian notice
					} );
				} , 900 );
			} );
		} );
	}

	function hideOnAlgoGuardian(){
		$( ".cfmCvr" ).hide();
		$( ".cfmOnAlgoDiv" ).hide();
	}

	function showGuardianNotice( type , msg ){ // show a notice if guardian is triggerd.
		if ( type == "MAXLOSS" || type == "LOSSFROMTOP" || type == "TRADEQTY" || type == "NETPOS" || type == "SLOWFEED" || type == "OPENORDER" || type == "TWICEFILL" ){
			showToastr( "error" , "Algo Guardian" , msg ); // show GFA toastr at bottom-left

			if ( gAlertSound == 1 ){
				soundManager.play( "alert" ); // play alert
			}

			showGuardianPanel(); // show popup box on algo tab

		}else if ( type == "VOLUME" || type == "VOLUMERESUME" ){
			showToastr( "warning" , "Algo Guardian" , msg ); // show GFA toastr at bottom-left

			if ( gAlertSound == 1 ){
				soundManager.play( "order" ); // play alert
			}
		}

		if ( type == "MAXLOSS" ){
			$( ".cfgMaxLossDiv" ).find( ".txtIpt" ).addClass( "cfgWarnIpt" , 300 );
			$( ".cfgMaxLossDiv" ).addClass( "cfgGuardWarn" , 300 );
		}else if ( type == "TRADEQTY" ){
			$( ".cfgFilledQtyDiv" ).find( ".txtIpt" ).addClass( "cfgWarnIpt" , 300 );
			$( ".cfgFilledQtyDiv" ).addClass( "cfgGuardWarn" , 300 );
		}else if ( type == "NETPOS" ){
			$( ".cfgNetPosDiv" ).find( ".txtIpt" ).addClass( "cfgWarnIpt" , 300 );
			$( ".cfgNetPosDiv" ).addClass( "cfgGuardWarn" , 300 );
		}else if ( type == "LOSSFROMTOP" ){
			$( ".cfgLossFromTopDiv" ).find( ".txtIpt" ).addClass( "cfgWarnIpt" , 300 );
			$( ".cfgLossFromTopDiv" ).addClass( "cfgGuardWarn" , 300 );
		}else if ( type == "SLOWFEED" ){
			$( ".cfgSlowFedDiv" ).find( ".txtIpt" ).addClass( "cfgWarnIpt" , 300 );
			$( ".cfgSlowFedDiv" ).addClass( "cfgGuardWarn" , 300 );
		}else if ( type == "SLOWFEED" ){
			$( ".cfgSlowFedDiv" ).addClass( "cfgGuardWarn" , 300 );
		}else if ( type == "OPENORDER" ){
			$( ".cfgOpenOrdDiv" ).addClass( "cfgGuardWarn" , 300 );
		}else if ( type == "TWICEFILL" ){
			$( ".cfgTwiceFillDiv" ).addClass( "cfgGuardWarn" , 300 );
		}else if ( type == "VOLUME" ){
			$( ".cfgVolumeDiv" ).find( ".txtIpt" ).addClass( "cfgWarnIpt" , 300 );
			$( ".cfgVolumeDiv" ).addClass( "cfgGuardWarn" , 300 );
		}else if ( type == "VOLUMEPAUSE" ){
			$( ".panlPauseNtcSec" ).show();
		}else if ( type == "VOLUMERESUME" ){
			$( ".panlPauseNtcSec" ).hide();
		}

		$( ".panlGuarDiv" ).addClass( "fontColorNotice" );
		$( "#jpanel_msg" ).find( ".badgeSp" ).addClass( "bgColorNotice" );
	}

	function autoCutGuardian( type ){ // auto square position
		var msg = "Triggered Auto Flat Position Function by " + type + " Guardian. Auto Flat " + gGuaAutoFlatQtyParm + "% of Net Position.";
		sendTabMsg( msg );

		if ( gReloaded == 1 || gDisconnected == 1 ){ // check if the panel is reloaded/disconnected or not
			sendLogMsg( "gReloaded : " + gReloaded + " , gDisconnected : " + gDisconnected );
			return;
		}
		if ( gGuaAutoFlatQtyParm == 0 ){ // do nothing if auto cut 0 percent
			sendLogMsg( "gGuaAutoFlatQtyParm : " + gGuaAutoFlatQtyParm );
			return;
		}

		clearTimeout( gAutoFlatTimer ); // clear previous timer
		gAutoFlatTimer = setTimeout( function (){
			squarePosition( gGuaAutoFlatQtyParm , gDefAutoFlatSlip );
		} , 1000 ); // wait 1 second to ensure _STOPALL is completed.
	}

	function squarePosition( pct , slip ){
		if ( gReloaded == 1 || gDisconnected == 1 ){ // check if the panel is reloaded/disconnected or not
			sendLogMsg( "gReloaded : " + gReloaded + " , gDisconnected : " + gDisconnected );
			return;
		}

		clearInterval( gFlatPosInterval ); // clear previous timer
		gFlatPosInterval = false;

		var interval = 200;
		var wait = 2000;
		var orderNum = 0;

		gFlatPosInterval = setInterval( flatPos , interval );

		function flatPos(){
			orderNum = getOpenOrderNum();

			if ( wait <= 0 ){
				clearInterval( gFlatPosInterval );
				gFlatPosInterval = false;

				var msg = "Exist Outstanding Open Order , Open Order No. : " + orderNum + ". Auto Flat is Aborted.";
				sendTabMsg( msg );

				return;
			}

			if ( orderNum > 1 ){
				wait -= interval;

				return;
			}

			clearInterval( gFlatPosInterval );
			gFlatPosInterval = false;

			var symAry = getSymAry(); // get symbol array;

			for ( i = 0 ; i < symAry.length ; i ++ ){ // check netPos for every sym
				var ecn = symAry[ i ][ 0 ];
				var sym = symAry[ i ][ 1 ];
				var netPos = getSymNetPos( ecn , sym ).netPos;
				var dip = parseFloat( getSymItem( ecn , sym ).dip ); // get symbol decimal places

				if ( netPos == 0 || ! $.isNumeric( netPos ) ){
					continue;
				}

				var qty = Math.round( netPos * ( pct / 100 ) ) * -1; // square qty
				if ( qty == 0 ){
					continue;
				}

				var bidPx = getSymBidAskPx( ecn , sym )[ 0 ];
				var askPx = getSymBidAskPx( ecn , sym )[ 1 ];

				if ( bidPx == 0 || askPx == 0 ){
					var msg = "Error on getting " + sym + " Bid Ask Price. Fail to send cut loss order.";
					sendLogMsg( msg );

					continue;
				}

				if ( isNaN( dip ) ){
					var msg = "Error On Getting Decimal Places : " + dip + ". Fail to send cut loss order.";
					sendLogMsg( msg );

					continue;
				}

				var msg = "Squaring Position. ecn : " + ecn + " .sym : " + sym + " .netPos : " + netPos + ". cut qty : " + qty;
				sendLogMsg( msg );

				if ( pct > 0 ){
					var px = getCutLossPx( ecn , sym , netPos , qty , bidPx , askPx );

				}else{
					if ( qty > 0 ){
						var px = askPx + ( slip * ts );
					}else{
						var px = bidPx - ( slip * ts );
					}
				}

				px = px.toFixed( dip );

				if ( qty > 0 ){
					var side = "Buy";
				}else{
					var side = "Sell";
				}

				if ( pct > 0 ){
					var type = "Flat";
				}else{
					var type = "Overweight";
				}

				if ( Math.abs( qty ) > gGuaAutoFlatMaxQtyParm ){
					if ( qty > 0 ){
						qty = gGuaAutoFlatMaxQtyParm;
					}else{
						qty = gGuaAutoFlatMaxQtyParm * -1;
					}

					var cutMsg = ". ( Maximum Auto " + type + " " + gGuaAutoFlatMaxQtyParm + " Lots. )";

				}else{
					var cutMsg = ". ( " + type + " " + Math.abs( pct ) + "% of Current Net Position. )";
				}

				sendSquarePositionOrder( ecn , sym , qty , px );

				var msg = "Auto " + type + " Position. Placed " + side + " " + Math.abs( qty ) + " Lots " + ecn + " " + sym + " Order at " + px + cutMsg;
				sendTabMsg( msg );
			}
		}
	}

	function sendSquarePositionOrder( ecn , sym , qty , px ){
		var poi = gInitUserID.toString() + "999";
		var type = "LO2";
		var tif = "DAY";

		if ( qty < 0 ){
			var side = "SELL";
		}else if ( qty > 0 ){
			var side = "BUY";
		}else{
			return;
		}

		var cmd = "CMD " + gInitUserID + " O " + ecn + " " + type + " " + tif + " " + side + " " + sym + " QTY " + Math.abs( qty ) + " AT " + px + " POI " + poi + " NAME algoFlat";

		//var cmd = "CMD " + gInitUserID + " O " + ecn + " " + type + " " + tif + " " + side + " " + sym + " QTY NNETPOS AT " + px;

		pushCMD( cmd );
		showLog( "sendSquarePositionOrder" , cmd );
	}

	function noTradeGuardian(){
		clearTimeout( gNoTradeTimer );

		if ( gOnAllMacro == 0 ) { // check the big on off switch is on or not
			return;
		}
		if ( gReloaded == 1 || gDisconnected == 1 ){ // check if the panel is reloaded/disconnected or not
			return;
		}
		if ( gGuaNoTradeOn == 0 ){ // check the guard maximum loss from top function is turnned on or not
			return;
		}

		var noTradeTimer = gGuaNoTradeParm * 1000;

		gNoTradeTimer = setTimeout( function (){
			if ( gOnAllMacro == 0 ) { // check the big on off switch is on or not
				return;
			}

			var symAry = getSymAry();
			for ( i = 0 ; i < symAry.length ; i ++ ){ // check netPos for every sym
				var ecn = symAry[ i ][ 0 ];
				var sym = symAry[ i ][ 1 ];
				var netPos = getSymNetPos( ecn , sym ).netPos;
				//netPos++;
				if ( netPos == 0 || ! $.isNumeric( netPos ) ){
					continue;
				}

				if ( netPos > 0 ){
					var side = "SELL";
				}else{
					var side = "BUY";
				}

				triggerOnRowGuardian( ecn , sym , side );
			}
		} , noTradeTimer );
	}

	function triggerOnRowGuardian( ecn , sym , side ){
		gGotDefaultParm = 0;
		gOnAllMacro = 0;

		$.when( checkParameter( "_ONALL_MACRO" ) ).then( function (){ // get lastest server status
			if ( gOnAllMacro == 0 ){ // // check the big on off switch is on or not again
				return;
			}

			$.when( checkIsLeader() ).then( function (){ // check itself is leader
				if ( gIsLeader == 0 ){
					return;
				}

				onMacro();
			} );
		} );

		function onMacro(){
			$( ".macRow" ).each( function (){
				var row = $( this );
				var id = $( this ).attr( "id" );
				var numStr = id.split( "_" )[ 1 ].toString();

				var rEcn = $( row ).find( ".ecnDiv" ).find( ".current" ).html();
				rEcn = rEcn.replace( / /g , "" );
				var rSym = $( row ).find( ".symDiv" ).find( ".current" ).html();
				rSym = rSym.replace( / /g , "" );

				if ( rEcn != ecn ){
					return;
				}
				if ( rSym != sym ){
					return;
				}

				$( row ).find( ".sideSec" ).each( function (){
					var ifdSwVal = $( this ).find( ".ifdSwChked" ).find( ".ifdSwIpt" ).attr( "value" );

					if( ifdSwVal != 12 ){
						return;
					}

					if ( $( this ).find( ".swOnLbl" ).hasClass("swChked") ){
						return;
					}

					if ( $( this).hasClass( "bidSec" ) ){
						var rSide = "BUY";
					}else{
						var rSide = "SELL";
					}

					if ( rSide != side ){
						return;
					}

					var macName = "algoMac_" + numStr + "_" + side;
					addOnMacroList( macName );
					startMacro( macName );

					var msg = "No Trade in " + gGuaNoTradeParm + " Seconds. Turned On " + side + " " + ecn + " " + sym + " Order.";
					sendTabMsg( msg );
				});
			});
		}
	}

	function triggerOffRowGuardian(){
		$( ".macRow" ).each( function (){
			var row = $( this );
			var id = $( this ).attr( "id" );
			var numStr = id.split( "_" )[ 1 ].toString();
			var rEcn = $( row ).find( ".ecnDiv" ).find( ".current" ).html();
			rEcn = rEcn.replace( / /g , "" );
			var rSym = $( row ).find( ".symDiv" ).find( ".current" ).html();
			rSym = rSym.replace( / /g , "" );

			$( row ).find( ".sideSec" ).each( function (){
				var ifdSwVal = $( this ).find( ".ifdSwChked" ).find( ".ifdSwIpt" ).attr( "value" );

				if ( ifdSwVal != 12 ){
					return;
				}

				if ( !$( this ).find( ".swOnLbl" ).hasClass("swChked") ){
					return;
				}

				if ( $( this).hasClass( "bidSec" ) ){
					var rSide = "BUY";
				}else{
					var rSide = "SELL";
				}

				var macName = "algoMac_" + numStr + "_" + rSide;
				var cnlName = "algoCnl_" + numStr + "_" + rSide;

				removeOnMacroList( macName );
				stopMacro( cnlName );

				if ( gOnAllMacro == 1 ){
					var msg = "Order is Filled. Turned Off " + rSide + " " + rEcn + " " + rSym + " Order.";
					sendTabMsg( msg );
				}
			});
		});
	}

	///////////////// config panel function //////////////

	function uiClickCfgPanelBtn(){ // config panel setting
		if ( $( ".cfgPanlSec" ).is( ":visible" ) ){
			return;
		}

		if ( gTelgUserID.indexOf( gInitUserID ) == -1 ){ // allow 302 , 307 , 308 and 309 to send telegram
			gSendTelg = 0;
			$( ".cfgSendTelgDiv" ).find( ".cfgChk" ).prop( "disabled" , true );
		}

		checkChkbox( gSendTelg , ".cfgSendTelgDiv" );
		checkChkbox( gShowLog , ".cfgShowLogDiv" );
		checkChkbox( gAlertSound , ".cfgAlertSoundDiv" );
		checkChkbox( gAutoOn , ".cfgAutoOnDiv" );
		checkChkbox( gAutoChgMode , ".cfgAutoChgModeDiv" );

		$( ".cfgUpdFeqDiv" ).find( ".txtIpt" ).attr( "value" , gUpdFeq );
		$( ".cfgRepOrdDlyDiv" ).find( ".txtIpt" ).attr( "value" , gRepOrdDly );
		$( ".cfgOrdMethodDiv" ).find( ".txtIpt" ).attr( "value" , gOrdSpeed );
		$( ".cfgOrdMethodDiv" ).find( ".cfgSlet" ).val( gOrdMethod );
		$( ".cfgOrdMethodDiv" ).find( ".cfgSlet" ).trigger( "change" );

		$( ".cfgPanlSec" ).fadeIn();

		function checkChkbox( val , div ){
			if ( val == 0 ){
				$( div ).find( ".cfgChk" ).attr( "checked" , false );
			}else{
				$( div ).find( ".cfgChk" ).attr( "checked" , true );
			}
		}
	}

	function uiConfimPanlCfg(){ // confirm panel config
		var update = 0;
		var msg = "Updated Algo Setting : ";

		var chkUpdFeq = checkIptVal( ".cfgUpdFeqDiv" , gDefUpdMaxFeq , gDefUpdMinFeq ); // input number validation
		if ( chkUpdFeq == false ){
			return;
		}

		var chkRepOrdDly = checkIptVal( ".cfgRepOrdDlyDiv" , gDefRepOrdMaxDly , gDefRepOrdMinDly );
		if ( chkRepOrdDly == false ){
			return;
		}

		var chkOrdMethod = checkIptVal( ".cfgOrdMethodDiv" , gDefOrdMaxSpeed , gDefOrdMinSpeed );
		if ( chkOrdMethod == false ){
			return;
		}

		var ordMethod = $( ".cfgOrdMethodDiv" ).find( ".current" ).html();
		ordMethod = ordMethod.replace( / /g , "" );

		var chkAlertSound = $( ".cfgAlertSoundDiv" ).find( ".cfgChk" ).is( ":checked" ); // check check box is checked or not
		var chkAutoChgMode = $( ".cfgAutoChgModeDiv" ).find( ".cfgChk" ).is( ":checked" );
		var chkAutoOn = $( ".cfgAutoOnDiv" ).find( ".cfgChk" ).is( ":checked" );
		var chkSendTelg = $( ".cfgSendTelgDiv" ).find( ".cfgChk" ).is( ":checked" );
		var chkShowLog = $( ".cfgShowLogDiv" ).find( ".cfgChk" ).is( ":checked" );

		if ( chkAlertSound ){
			gAlertSound = 1;
		}else{
			gAlertSound = 0;
		}
		store.set( "jAlertSound" , gAlertSound );

		if ( chkAutoChgMode ){
			var onAutoChgMode = 1;
		}else{
			var onAutoChgMode = 0;
		}

		if ( chkAutoOn ){
			var onAutoOn = 1;
		}else{
			var onAutoOn = 0;
		}

		if ( chkSendTelg ){
			var onSendTelg = 1; // change checked = ture to 1
		}else{
			var onSendTelg = 0; // change checked = false to 0
		}

		if ( chkShowLog ){
			gShowLog = 1;
		}else{
			gShowLog = 0;
		}
		store.set( "jShowLog" , gShowLog );

		if ( gUpdFeq != chkUpdFeq ){ // check need to update the macro or not
			$( ".cfgRowUpdFeqDiv" ).each( function (){
				$( this ).find( ".txtIpt" ).val( chkUpdFeq );
			} );

			setMacro( "_CFG_UPY_FEQ" , chkUpdFeq );

			update = 1;
			msg += "Update Fequency Changed to " + chkUpdFeq + " Seconds. ";
		}

		if ( gRepOrdDly != chkRepOrdDly ){
			$( ".cfgRowRepOrdDlyDiv" ).each( function (){
				$( this ).find( ".txtIpt" ).val( chkRepOrdDly );
			} );

			setMacro( "_CFG_REP_ORD_DLY" , chkRepOrdDly );

			update = 1;
			msg += "Replace Order Delay Changed to " + chkRepOrdDly + " Seconds. ";
		}

		if ( gOrdMethod != ordMethod ){
			$( ".cfgRowOrdMethodDiv" ).each( function (){
				$( this ).find( ".cfgSlet" ).val( ordMethod );
				$( this ).find( ".cfgSlet" ).trigger( "change" );
				$( this ).find( ".txtIpt" ).val( ordMethod );
			} );

			setMacro( "_CFG_ORDER_METHOD" , ordMethod );

			update = 1;
			msg += "Order Method Changed to " + ordMethod + ". ";
		}

		if ( gSendTelg != onSendTelg ){
			setMacro( "_ON_SEND_TELG" , onSendTelg );

			msg += "Truned " + chkSendTelg + " 'Send Message to Telegram' Function. ";
		}

		if ( gAutoOn != onAutoOn ){
			setMacro( "_ON_AUTO_ON" , onAutoOn );

			msg += "Truned " + chkAutoOn + " 'Auto Turn On' Function. ";
		}

		if ( gAutoChgMode != onAutoChgMode ){
			setMacro( "_ON_AUTO_CHGMODE" , onAutoChgMode );

			msg += "Truned " + chkAutoChgMode + " 'Auto Change Mode' Function. ";
		}

		if ( gOrdSpeed != chkOrdMethod ){
			setMacro( "_CFG_ORD_SPEED" , chkOrdMethod );

			msg += "Changed " + ordMethod + " speed to " + chkOrdMethod + " Seconds. ";
			update = 1;
		}

		if ( update == 1 ){ // update and restart macro to apply the change
			updateAllMacro();
			restartAllMacro();
		}

		if ( msg != "Updated Algo Setting : " ){ // tell every one panel cfg is changed.
			getSeqNum( gByFunction );

			msg = msg.replace( /true/g , "ON" );
			msg = msg.replace( /false/g , "OFF" );
			msg += "Seq No.: " + gSeqNum + ".";

			sendTabMsg( msg );
		}

		hideCfgSec();
	}

	function uiClickCfgGuarBtn(){ // config algo guardian setting
		if ( $( ".panlGuarDiv" ).hasClass( "fontColorNotice" ) ){
			$( ".panlGuarDiv" ).removeClass( "fontColorNotice" );
		}

		if ( $( ".cfgGuarSec" ).is( ":visible" ) ){
			return;
		}

		showGuardianPanel();
	}

	function showGuardianPanel(){
		checkChkbox( gGuaOpenOrdOn , gGuaOpenOrdCut , 0 , ".cfgOpenOrdDiv" );	// check check box if guard is enabled
		checkChkbox( gGuaSlowFedOn , gGuaSlowFedCut , 0 , ".cfgSlowFedDiv" );
		checkChkbox( gGuaTwiceFillOn , gGuaTwiceFillCut , 0 , ".cfgTwiceFillDiv" );
		checkChkbox( gGuaFilledQtyOn , gGuaFilledQtyCut , gGuaFilledQtyParm , ".cfgFilledQtyDiv" );
		checkChkbox( gGuaNetPosOn , gGuaNetPosCut , gGuaNetPosParm , ".cfgNetPosDiv" );
		checkChkbox( gGuaMaxLossOn , gGuaMaxLossCut , gGuaMaxLossParm , ".cfgMaxLossDiv" );
		checkChkbox( gGuaLossFromTopOn , gGuaLossFromTopCut , gGuaLossFromTopParm , ".cfgLossFromTopDiv" );
		checkChkbox( gGuaVolumeOn , gGuaVolumeCut , gGuaVolumeParm , ".cfgVolumeDiv" );
		checkChkbox( gGuaResumeVolumeOn , 0 , gGuaResumeVolumeParm , ".cfgResumeVolumeDiv" );
		checkChkbox( gGuaAlgoOffFlatOn , 0 , 0 , ".cfgAlgoOffFlatDiv" );
		checkChkbox( gGuaNoTradeOn , 0 , gGuaNoTradeParm , ".cfgNoTradeDiv" );

		$( ".cfgAutoFlatQtyDiv" ).find( ".txtIpt" ).attr( "value" , gGuaAutoFlatQtyParm );
		$( ".cfgAutoFlatMaxQtyDiv" ).find( ".txtIpt" ).attr( "value" , gGuaAutoFlatMaxQtyParm );
		$( ".cfgFuseStopQtyDiv" ).find( ".txtIpt" ).attr( "value" , gGuaFuseStopQtyParm );

		$( ".cfgGuarSec" ).fadeIn();

		function checkChkbox( on , flat , val , type ){
			$( type ).find( ".txtIpt" ).attr( "value" , val );
			if ( on == 0 ){
				$( type ).find( ".cfgChk" ).attr( "checked" , false );
				$( type ).find( ".cfgOnChk" ).attr( "checked" , false );
				$( type ).find( ".cfgCutChk" ).prop( 'disabled' , true );
				$( type ).find( ".cfgCutChk" ).addClass( "disableCtrl" );
				$( type ).find( ".txtIpt" ).prop( 'disabled' , true );
				$( type ).find( ".txtIpt" ).addClass( "disableCtrl" );

			}else{
				$( type ).find( ".cfgCutChk" ).prop( 'disabled' , false );
				$( type ).find( ".cfgCutChk" ).removeClass( "disableCtrl" );
				$( type ).find( ".txtIpt" ).prop( 'disabled' , false );
				$( type ).find( ".txtIpt" ).removeClass( "disableCtrl" );

				$( type ).find( ".cfgChk" ).attr( "checked" , true );
				$( type ).find( ".cfgOnChk" ).attr( "checked" , true );
				if ( flat == 1 ){
					$( type ).find( ".cfgCutChk" ).attr( "checked" , true );
				}else{
					$( type ).find( ".cfgCutChk" ).attr( "checked" , false );
				}
			}

			if ( type == ".cfgResumeVolumeDiv" ){
				if ( gGuaVolumeOn == 1 ){
					$( type ).find( ".cfgOnChk" ).prop( 'disabled' , false );
				}else{
					$( type ).find( ".cfgOnChk" ).prop( 'disabled' , true );
				}
			}
		}
	}

	function uiConfimGuarCfg(){ // confirm guardian setting by user
		var msg = "Updated Guardian Setting : ";

		checkGuardian( "Open Order Monitor" , ".cfgOpenOrdDiv" , "_GUA_OPENORD_ON" , gGuaOpenOrdOn , "_GUA_OPENORD_FLAT" , gGuaOpenOrdCut );

		checkGuardian( "Slow Feed Detect" , ".cfgSlowFedDiv" , "_GUA_SLOWFED_ON" , gGuaSlowFedOn , "_GUA_SLOWFED_FLAT" , gGuaSlowFedCut );

		checkGuardian( "Twice Fill Detect" , ".cfgTwiceFillDiv" , "_GUA_TWICEFILL_ON" , gGuaTwiceFillOn , "_GUA_TWICEFILL_FLAT" , gGuaTwiceFillCut );

		checkGuardian( "Auto Flat When Algo Off" , ".cfgAlgoOffFlatDiv" , "_GUA_ALGOOFFCUT_ON" , gGuaAlgoOffFlatOn );

		if ( !checkGuardian( "Maximum Filled Quantity" , ".cfgFilledQtyDiv" , "_GUA_FILLEDQTY_ON" , gGuaFilledQtyOn , "_GUA_FILLEDQTY_FLAT" , gGuaFilledQtyCut , "_GUA_FILLEDQTY_PARM" , gGuaFilledQtyParm , gDefMaxBSMaxQty , 0 , "Lots" ) ){
			return;
		}

		if ( !checkGuardian( "Maximum Net Position" , ".cfgNetPosDiv" , "_GUA_NETPOS_ON" , gGuaNetPosOn , "_GUA_NETPOS_FLAT" , gGuaNetPosCut , "_GUA_NETPOS_PARM" , gGuaNetPosParm , gDefMaxNetMaxPos , 0 , "Lots" ) ){
			return;
		}

		if ( !checkGuardian( "Maximum Loss" , ".cfgMaxLossDiv" , "_GUA_MAXLOSS_ON" , gGuaMaxLossOn , "_GUA_MAXLOSS_FLAT" , gGuaMaxLossCut , "_GUA_MAXLOSS_PARM" , gGuaMaxLossParm , 1000000 , gDefMaxMaxLoss , "USD" ) ){
			return;
		}

		if ( !checkGuardian( "Maximum Loss From Top" , ".cfgLossFromTopDiv" , "_GUA_LOSSFROMTOP_ON" , gGuaLossFromTopOn , "_GUA_LOSSFROMTOP_FLAT" , gGuaLossFromTopCut , "_GUA_LOSSFROMTOP_PARM" , gGuaLossFromTopParm , -1 , -999999 , "USD" ) ){
			return;
		}

		if ( !checkGuardian( "Maximum Volume" , ".cfgVolumeDiv" , "_GUA_VOLUME_ON" , gGuaVolumeOn , "_GUA_VOLUME_FLAT" , gGuaVolumeCut , "_GUA_VOLUME_PARM" , gGuaVolumeParm , 100000 , 500 , "Lots" ) ){
			return;
		}

		if ( !checkGuardian( "Resume Volume" , ".cfgResumeVolumeDiv" , "_GUA_RESUMEVOLUME_ON" , gGuaResumeVolumeOn , "" , 0 , "_GUA_RESUMEVOLUME_PARM" , gGuaResumeVolumeParm , 100000 , 500 , "Lots" ) ){
			return;
		}

		if ( !checkCfg( "Flat Quantity" , ".cfgAutoFlatQtyDiv" , "_GUA_AUTOFLATQTY_PARM" , gGuaAutoFlatQtyParm , 100 , -100 , "%" ) ){
			return;
		}

		if ( !checkCfg( "Maximum Flat Quantity" , ".cfgAutoFlatMaxQtyDiv" , "_GUA_AUTOFLATMAXQTY_PARM" , gGuaAutoFlatMaxQtyParm , 200 , 1 , "Lots" ) ){
			return;
		}

		if ( !checkCfg( "Fuse Stop Quantity" , ".cfgFuseStopQtyDiv" , "_GUA_FUSESTOPQTY_PARM" , gGuaFuseStopQtyParm , gDefFuseStopMaxQty , gDefFuseStopMinQty , "Lots" ) ){
			return;
		}

		if ( !checkGuardian( "No Trade in a Period of Time" , ".cfgNoTradeDiv" , "_GUA_NOTRADE_ON" , gGuaNoTradeOn , "" , 0 , "_GUA_NOTRADE_PARM" , gGuaNoTradeParm , gDefGuaNoTradeMaxParm , gDefGuaNoTradeMinParm , "Seconds" ) ){
			return;
		}

		$( ".cfgDiv" ).removeClass( "cfgGuardWarn" );
		$( ".panlBtnDiv" ).removeClass( "fontColorNotice" );
		$( ".txtIpt" ).removeClass( "cfgWarnIpt" );

		if ( msg != "Updated Guardian Setting : " ){ // tell every one panel cfg is changed.
			getSeqNum( gByFunction );
			msg += "Seq No.: " + gSeqNum + ".";

			sendTabMsg( msg );
		}

		hideCfgSec();

		function checkGuardian( type , div , onName , oldOnGua , cutName , oldOnCut , valName , oldVal , maxVal , minVal , unit ){
			var onGua = $( div ).find( ".cfgChk" ).is( ":checked" );
			var onGua2 = $( div ).find( ".cfgOnChk" ).is( ":checked" );
			var onCut = $( div ).find( ".cfgCutChk" ).is( ":checked" );

			if ( onGua || onGua2 ){
				onGua = 1;
				var onGuaMsg = "ON";

				if ( $.isNumeric( maxVal ) ){
					var newVal = checkIptVal( div , maxVal , minVal );
					if ( !newVal ){
						return false;
					}

					if ( oldVal != newVal ){
						setMacro( valName , newVal );
						msg += type + " Changed to " + newVal + " " + unit + ". ";

						if ( valName == "_GUA_VOLUME_PARM" || valName == "_GUA_RESUMEVOLUME_PARM" ){
							newVal = Math.round( newVal / 500 ) * 500;
						}else if ( valName == "_GUA_LOSSFROMTOP_PARM" ){
							renewMaxProfit();
						}
					}
				}

				if ( $( div ).find( ".cfgCutChk" ).length ){
					if ( onCut ){
						onCut = 1;
						var onCutMsg = "ON";
					}else{
						onCut = 0;
						var onCutMsg = "OFF";
					}

					if ( oldOnCut != onCut ){
						setMacro( cutName , onCut );
						msg += "Truned " + onCutMsg + " '" + type + "' Auto Cut Loss. ";
					}
				}
			}else{
				onGua = 0;
				var onGuaMsg = "OFF";
			}

			if ( oldOnGua != onGua ){
				setMacro( onName , onGua ); // save guardian on off to a macro
				msg += "Truned " + onGuaMsg + " '" + type + "' Guardian. ";

				if ( div == ".cfgLossFromTopDiv" && onGua == 1 ){
					renewMaxProfit();
				}

				if ( div == ".cfgNoTradeDiv" && onGua == 0 ){
					triggerOffRowGuardian();
				}
			}
			return true;
		}

		function checkCfg( type , div , valName , oldVal , maxVal , minVal , unit ){
			var newVal = checkIptVal( div , maxVal , minVal );

			if ( ! $.isNumeric( newVal ) ){
				return false;
			}

			if ( oldVal != newVal ){
				setMacro( valName , newVal );
				msg += type + " Changed to " + newVal + " " + unit + ". ";
			}
			return true;
		}
	}

	function uiClickCfgRowBtn(){ // config single row setting
		var cfgRowSec = $( this ).closest( ".macRow" ).find( ".cfgRowSec" );
		var cfgRowRepOrdDlyDiv = $( cfgRowSec ).find( ".cfgRowRepOrdDlyDiv" );
		var cfgRowUpdFeqDiv = $( cfgRowSec ).find( ".cfgRowUpdFeqDiv" );
		var cfgRowOrdMethodDiv = $( cfgRowSec ).find( ".cfgRowOrdMethodDiv" );
		var cfgRowOrdTypeDivDiv = $( cfgRowSec ).find( ".cfgRowOrdTypeDiv" );

		if ( $( cfgRowSec ).is( ":visible" ) ){
			return;
		}

		gRowRepOrdDly = $( cfgRowRepOrdDlyDiv ).find( ".txtIpt" ).val(); // store current row setting to a global variable
		gRowUpdFeq = $( cfgRowUpdFeqDiv ).find( ".txtIpt" ).val();
		gRowOrdSpeed = $( cfgRowOrdMethodDiv ).find( ".txtIpt" ).val();
		gRowOrdMethod = $( cfgRowOrdMethodDiv ).find( ".current" ).html();
		gRowOrdMethod = gRowOrdMethod.replace( / /g , "" );

		var ordType = $( cfgRowSec ).find( ".cfgRowOrdTypeDiv" ).find( ".current" ).html();
		gRowOrdType = ordType.replace( / /g , "" );

		$( cfgRowSec ).fadeIn();
	}

	function uiConfimRowCfg(){
		var cfgRowSec = $( this ).closest( ".macRow" ).find( ".cfgRowSec" );

		var updFeqDiv = $( cfgRowSec ).find( ".cfgRowUpdFeqDiv" );
		var chkUpdFeq = checkIptVal( updFeqDiv , gDefUpdMaxFeq , gDefUpdMinFeq ); // input validation
		if ( chkUpdFeq == false ){
			return;
		}

		var repOrdDlyDiv = $( cfgRowSec ).find( ".cfgRowRepOrdDlyDiv" );
		var chkRepOrdDly = checkIptVal( repOrdDlyDiv , gDefRepOrdMaxDly , gDefRepOrdMinDly );
		if ( chkRepOrdDly == false ){
			return;
		}

		var ordMethodDiv = $( cfgRowSec ).find( ".cfgRowOrdMethodDiv" );
		var chkOrdMethod = checkIptVal( ordMethodDiv , gDefOrdMaxSpeed , gDefOrdMinSpeed );
		if ( chkOrdMethod == false ){
			return;
		}

		var ordMethod = $( ordMethodDiv ).find( ".current" ).html();
		ordMethod = ordMethod.replace( / /g , "" );

		var ordType = $( cfgRowSec ).find( ".cfgRowOrdTypeDiv" ).find( ".current" ).html();
		ordType = ordType.replace( / /g , "" );

		if ( gRowRepOrdDly == chkRepOrdDly && gRowUpdFeq == chkUpdFeq && gRowOrdSpeed == chkOrdMethod && gRowOrdMethod == ordMethod && gRowOrdType == ordType ){ // check need to update the macro or not
			hideCfgRowSec();
			return;
		}

		var rowID = "#" + $( this ).closest( ".macRow" ).attr( 'id' );

		updateMacro( rowID , "BUY" ); // update and restart macro to apply the change
		restartMacro( rowID , "BUY" );
		updateMacro( rowID , "SELL" );
		restartMacro( rowID , "SELL" );

		hideCfgRowSec();
	}

	function hideCfgSec(){
		$( ".cfgPanlSec" ).fadeOut();
		$( ".cfgGuarSec" ).fadeOut();
		$( ".cfgModeSec" ).fadeOut();
	}

	function hideCfgRowSec(){
		$( ".cfgRowSec" ).fadeOut();
		$( ".cfgSymSprdDiv" ).fadeOut();
	}

	function hideCfmSec(){
		$( ".cfmCvr" ).fadeOut();
		$( ".cfmSec" ).fadeOut();
	}

	///////////////// set price spread function ///////////////

	function uiClickCfgSpread(){ // config each symbol spread
		var macRow = $( this ).closest( ".macRow" );
		var cfgSymSprdDiv = $( macRow ).find( ".cfgSymSprdDiv" );
		if ( $( cfgSymSprdDiv ).is( ":visible" ) ){
			return;
		}

		var ecn = $( macRow ).find( ".ecnDiv" ).find( ".current" ).html();
		ecn = ecn.replace( / /g , "" );

		var sym = $( macRow ).find( ".symDiv" ).find( ".current" ).html();
		sym = sym.replace( / /g , "" );

		var bid = parseInt( $( cfgSymSprdDiv ).find( ".bidSprdDiv" ).find( ".txtIpt" ).val() );
		var ask = parseInt( $( cfgSymSprdDiv ).find( ".askSprdDiv" ).find( ".txtIpt" ).val() );

		symSpread = getSymSpread( ecn , sym );
		mBid = symSpread[ 0 ];
		mAsk = symSpread[ 1 ];

		if ( bid != mBid ){
			$( cfgSymSprdDiv ).find( ".bidSprdDiv" ).find( ".txtIpt" ).val( mBid );
			$( cfgSymSprdDiv ).find( ".sprdSlidr" ).dragslider( "values" , 0 , mBid - gSpreadMid );
		}

		if ( ask != mAsk ){
			$( cfgSymSprdDiv ).find( ".askSprdDiv" ).find( ".txtIpt" ).val( mAsk );
			$( cfgSymSprdDiv ).find( ".sprdSlidr" ).dragslider( "values" , 1 , mAsk + gSpreadMid );
		}
		$( cfgSymSprdDiv ).fadeIn();
	}

	function getMinMaxBidAskPips( rowID ){ // get the minimum and maximum pip for the symbol
		var ecn = $( rowID ).find( ".ecnDiv" ).find( ".current" ).html();
		ecn = ecn.replace( / /g , "" );
		var sym = $( rowID ).find( ".symDiv" ).find( ".current" ).html();
		sym = sym.replace( / /g , "" );

		var maxBid = -1000;
		var minAsk = 1000;

		$( ".macRow" ).each( function (){
			var row = $( this );
			var rEcn = $( row ).find( ".ecnDiv" ).find( ".current" ).html();
			rEcn = rEcn.replace( / /g , "" );

			var rEcn = $( row ).find( ".symDiv" ).find( ".current" ).html();
			rEcn = rEcn.replace( / /g , "" );

			if ( rEcn != ecn ){
				return;
			}
			if ( rSym != sym ){
				return;
			}

			bid = parseInt( $( row ).find( ".bidSec" ).find( ".ctrlPipsDiv" ).find( ".txtIpt" ).val() );
			ask = parseInt( $( row ).find( ".askSec" ).find( ".ctrlPipsDiv" ).find( ".txtIpt" ).val() );

			if ( bid > maxBid ){
				maxBid = bid;
			}
			if ( ask < minAsk ){
				minAsk = ask;
			}
		} );

		var bidAskVal = { maxBid: maxBid , minAsk: minAsk , };
		return bidAskVal;
	}

	function uiClickChgSpread(){ // click changing symbol spread button
		var sprdBtn = $( this );
		var rowID = "#" + $( sprdBtn ).closest( ".macRow" ).attr( "id" );

		var bidAskVal = getMinMaxBidAskPips( rowID );
		var maxBid = bidAskVal.maxBid;
		var minAsk = bidAskVal.minAsk;

		var cfgSymSprdDiv = $( rowID ).closest( ".macRow" ).find( ".cfgSymSprdDiv" );
		var bid = parseInt( $( cfgSymSprdDiv ).find( ".bidSprdDiv" ).find( ".txtIpt" ).val() );
		var ask = parseInt( $( cfgSymSprdDiv ).find( ".askSprdDiv" ).find( ".txtIpt" ).val() );

		if ( $( sprdBtn ).hasClass( "addSprdBtn" ) ){
			var newBid = bid - 1;
			var newAsk = ask + 1;
		}else if ( $( sprdBtn ).hasClass( "minusSprdBtn" ) ){
			var newBid = bid + 1;
			var newAsk = ask - 1;
		}else if ( $( sprdBtn ).hasClass( "bidShfitBtn" ) ){
			var newBid = bid - 1;
			var newAsk = ask - 1;
		}else if ( $( sprdBtn ).hasClass( "askShfitBtn" ) ){
			var newBid = bid + 1;
			var newAsk = ask + 1;
		}else if ( $( sprdBtn ).hasClass( "resetSprdBtn" ) ){
			var newBid = 0;
			var newAsk = 0;
		}

		if ( newBid < -gSpreadMax || ( newBid + maxBid ) > 0 ){ // validate the new bid and ask
			return;
		}
		if ( newAsk > gSpreadMax || ( newAsk + minAsk ) < 0 ){
			return;
		}

		$( cfgSymSprdDiv ).find( ".bidSprdDiv" ).find( ".txtIpt" ).val( newBid );
		$( cfgSymSprdDiv ).find( ".askSprdDiv" ).find( ".txtIpt" ).val( newAsk );
		$( cfgSymSprdDiv ).find( ".sprdSlidr" ).dragslider( "values" , 0 , newBid - gSpreadMid );
		$( cfgSymSprdDiv ).find( ".sprdSlidr" ).dragslider( "values" , 1 , newAsk + gSpreadMid );
	}

	function uiConfimSymSprdCfg(){ // confirm changing symbol spread
		var macRow = $( this ).closest( ".macRow" );
		var rowID = "#" + $( macRow ).attr( 'id' );
		var cfgSymSprdDiv = $( macRow ).find( ".cfgSymSprdDiv" );

		var ecn = $( macRow ).find( ".ecnDiv" ).find( ".current" ).html();
		ecn = ecn.replace( / /g , "" );

		var sym = $( macRow ).find( ".symDiv" ).find( ".current" ).html();
		sym = sym.replace( / /g , "" );

		var bid = parseInt( $( cfgSymSprdDiv ).find( ".bidSprdDiv" ).find( ".txtIpt" ).val() );
		var ask = parseInt( $( cfgSymSprdDiv ).find( ".askSprdDiv" ).find( ".txtIpt" ).val() );

		if ( ! $.isNumeric( bid ) || ! $.isNumeric( ask ) ){ // input validation
			return;
		}

		var setParm = [];
		if ( bid == 0 && ask == 0 ){
			setParm.push( ecn , sym , bid , ask );
			setMacro( "_SYM_SPREAD" , setParm );
		}

		var newSym = 1;
		for ( i = 0 ; i < gSymSprdAry.length ; i ++ ){
			var mEcn = gSymSprdAry[ i ][ 0 ];
			var mSym = gSymSprdAry[ i ][ 1 ];
			var mBid = gSymSprdAry[ i ][ 2 ];
			var mAsk = gSymSprdAry[ i ][ 3 ];

			if ( mEcn != ecn ){
				continue;
			}
			if ( mSym != sym ){
				continue;
			}

			newSym = 0;
			if ( mBid != 0 || mAsk != 0 ){
				gSymSprdAry[ i ] = [ ecn , sym , bid , ask ];
			}
			break;
		}

		if ( newSym == 1 ){
			gSymSprdAry[ gSymSprdAry.length ] = [ ecn , sym , bid , ask ];
		}

		setParm = [];

		for ( i = 0 ; i < gSymSprdAry.length ; i ++ ){
			var mEcn = gSymSprdAry[ i ][ 0 ];
			var mSym = gSymSprdAry[ i ][ 1 ];
			var mBid = gSymSprdAry[ i ][ 2 ];
			var mAsk = gSymSprdAry[ i ][ 3 ];
			if ( mBid != 0 || mAsk != 0 ){
				setParm.push( mEcn , mSym , mBid , mAsk );
			}
		}

		setMacro( "_SYM_SPREAD" , setParm ); // save symbol spread information to a macro
		// e.g. CMD 302 MACRO _SYM_SPREAD NAME { MREX CME/CMX_GLD/DEC17 -1 0 }

		$( ".macRow" ).each( function (){
			var macRow = $( this );

			var rEcn = $( macRow ).find( ".ecnDiv" ).find( ".current" ).html();
			rEcn = rEcn.replace( / /g , "" );

			var rSym = $( macRow ).find( ".symDiv" ).find( ".current" ).html();
			rSym = rSym.replace( / /g , "" );

			if ( rEcn != ecn ){
				return;
			}
			if ( rSym != sym ){
				return;
			}

			var rowID = "#" + $( macRow ).attr( 'id' );

			updateMacro( rowID , "BUY" ); // update and restart macro to apply the change
			restartMacro( rowID , "BUY" );
			updateMacro( rowID , "SELL" );
			restartMacro( rowID , "SELL" );
		} );

		hideCfgRowSec();
	}

	$.widget( "ui.dragslider" , $.ui.slider , { // create symbol spread slider

		options: $.extend( { } , $.ui.slider.prototype.options , { rangeDrag:false } ) ,

		_create: function (){
			$.ui.slider.prototype._create.apply( this , arguments );
			this._rangeCapture = false;
		} ,

		_mouseCapture: function( event ) {
			var o = this.options;

			if ( o.disabled ) {
				return false;
			}

			if ( event.target == this.range.get( 0 ) && o.rangeDrag == true && o.range == true ) {
				this._rangeCapture = true;
				this._rangeStart = null;
			} else {
				this._rangeCapture = false;
			}

			$.ui.slider.prototype._mouseCapture.apply( this , arguments );

			if ( this._rangeCapture == true ) {
				this.handles.removeClass( "ui-state-active" ).blur();
			}

			return true;
		} ,

		_mouseStop: function( event ) {
			this._rangeStart = null;
			return $.ui.slider.prototype._mouseStop.apply( this , arguments );
		} ,

		_slide: function( event , index , newVal ) {
			if ( !this._rangeCapture ) {
				return $.ui.slider.prototype._slide.apply( this , arguments );
			}

			if ( this._rangeStart == null ) {
				this._rangeStart = newVal;
			}

			var oldValLeft = this.options.values[ 0 ] ,
			oldValRight = this.options.values[ 1 ] ,
			slideDist = newVal - this._rangeStart ,
			newValueLeft = oldValLeft + slideDist ,
			newValueRight = oldValRight + slideDist ,
			allowed;

			if ( this.options.values && this.options.values.length ) {
				if ( newValueRight > this._valueMax() && slideDist > 0 ) {
					slideDist -= ( newValueRight-this._valueMax() );
					newValueLeft = oldValLeft + slideDist;
					newValueRight = oldValRight + slideDist;
				}

				if ( newValueLeft < this._valueMin() ) {
					slideDist += ( this._valueMin()-newValueLeft );
					newValueLeft = oldValLeft + slideDist;
					newValueRight = oldValRight + slideDist;
				}

				if ( slideDist != 0 ) {
					newValues = this.values();
					newValues[ 0 ] = newValueLeft;
					newValues[ 1 ] = newValueRight;

					allowed = this._trigger( "slide" , event , {
						handle: this.handles[ index ] ,
						value: slideDist ,
						values: newValues
					} );

					if ( allowed !== false ) {
						this.values( 0 , newValueLeft , true );
						this.values( 1 , newValueRight , true );
					}
					this._rangeStart = newVal;
				}
			}
			$( this ).closest( ".cfgSymSprdDiv" ).find( ".bidSprdDiv" ).find( ".txtIpt" ).val( newValueLeft + gSpreadMid );
			$( this ).closest( ".cfgSymSprdDiv" ).find( ".askSprdDiv" ).find( ".txtIpt" ).val( newValueRight - gSpreadMid );
		} ,
	} );

	function readSymSpread( cmrAry ){ // handle symbol spread CMR
		// e.g. CMR 302 MACRO _SYM_SPREAD NAME { MREX CME/CMX_GLD/DEC17 -1 1 }
		gSymSprdAry = []; // clear old information

		//for ( i = 6 ; i < cmrAry.length - 3 ; i = i + 4 ){ // use this if cmd added seq number
		for ( i = 6 ; i < cmrAry.length - 1 ; i = i + 4 ){
			var ecn = cmrAry[ i ];
			var sym = cmrAry[ i + 1 ];
			var bid = parseInt( cmrAry[ i + 2 ] );
			var ask = parseInt( cmrAry[ i + 3 ] );

			var newSym = 1;

			for ( j = 0 ; j < gSymSprdAry.length ; j ++ ){
				var mEcn = gSymSprdAry[ j ][ 0 ];
				var mSym = gSymSprdAry[ j ][ 1 ];

				if ( mEcn != ecn ){
					continue;
				}
				if ( mSym != sym ){
					continue;
				}

				newSym = 0;
				gSymSprdAry[ j ] = [ ecn , sym , bid , ask ];
				break;
			}

			if ( newSym == 1 ){
				gSymSprdAry[ gSymSprdAry.length ] = [ ecn , sym , bid , ask ]; // save spread information to an array
			}
		}

		showSymSpread(); // show the spread information
	}

	function getSymSpread( ecn , sym ){ // get current spread for specify product
		var symSpread = [ 0 , 0 ];

		for ( j = 0; j< gSymSprdAry.length ; j ++ ){
			var mEcn = gSymSprdAry[ j ][ 0 ];
			var mSym = gSymSprdAry[ j ][ 1 ];
			var mBid = gSymSprdAry[ j ][ 2 ];
			var mAsk = gSymSprdAry[ j ][ 3 ];

			if ( mEcn != ecn ){
				continue;
			}
			if ( mSym != sym ){
				continue;
			}
			symSpread = [ mBid , mAsk ];
			break;
		}

		return symSpread;
	}

	function showSymSpread(){ // show spread on row
		$( ".infSprdDiv" ).find( ".txtIpt" ).val( 0 );

		for ( i = 0 ; i < gSymSprdAry.length ; i ++ ){
			var ecn = gSymSprdAry[ i ][ 0 ];
			var sym = gSymSprdAry[ i ][ 1 ];
			var bidSp = parseFloat( gSymSprdAry[ i ][ 2 ] );
			var askSp = parseFloat( gSymSprdAry[ i ][ 3 ] );

			$( ".macRow" ).each( function (){
				var rowID = $( this );

				var mEcn = $( rowID ).find( ".ecnDiv" ).find( ".current" ).html();
				mEcn = mEcn.replace( / /g , "" );

				var mSym = $( rowID ).find( ".symDiv" ).find( ".current" ).html();
				mSym = mSym.replace( / /g , "" );

				if ( mEcn != ecn ){
					return;
				}
				if ( mSym != sym ){
					return;
				}
				setSp( rowID , ".bidSec" , bidSp );
				setSldr( rowID , ".bidSec" , bidSp );
				setSp( rowID , ".askSec" , askSp );
				setSldr( rowID , ".askSec" , askSp );
			} );
		}
		function setSp( rowID , bidOrAsk , spread ){
			var oSpread = parseFloat( $( rowID ).find( bidOrAsk ).find( ".infSprdDiv" ).find( ".txtIpt" ).val() );
			if ( oSpread == spread ){
				return;
			}
			if ( spread > 0 ){
				spread = "+" + spread;
			}
			$( rowID ).find( bidOrAsk ).find( ".infSprdDiv" ).find( ".txtIpt" ).val( spread );
		}
		function setSldr( rowID , bidOrAsk , spread ){
			var oPx = parseFloat( $( rowID ).find( bidOrAsk ).find( ".ctrlPipsDiv" ).find( ".ctrlVal" ).val() );
			var sldrVal = oPx - spread;
			var oSldrVal = parseFloat( $( rowID ).find( bidOrAsk ).find( ".ctrlPipsDiv" ).find( ".sldrHd" ).text() );

			if ( sldrVal == oSldrVal ){
				return;
			}
			if ( sldrVal > 0 ){
				sldrVal = "+" + sldrVal;
			}
			$( rowID ).find( bidOrAsk ).find( ".ctrlPipsDiv" ).find( ".sldrHd" ).text( sldrVal );
			$( rowID ).find( bidOrAsk ).find( ".ctrlPipsDiv" ).find( ".txtIpt" ).val( sldrVal );
		}
	}

	/////////////////// set , change mode function //////////////////

	function readMode( cmrAry ){ // read mode list
		// e.g. CMR 302 MACRO _MODE_M01_MODE01 NAME { algoMac_02_SELL algoMac_01_SELL }
		var ary = [];
		var id = cmrAry[ 3 ].replace( "_MODE_" , "" );

		ary.push( id );
		//for ( i = 6 ; i < cmrAry.length - 3 ; i ++ ){ // use this if cmd added seq number
		for ( i = 6 ; i < cmrAry.length - 1 ; i ++ ){
			var onMac = cmrAry[ i ];
			ary.push( onMac );
		}

		var newID = 1;
		for ( i = 0 ; i < gModeAry.length ; i ++ ){
			if ( gModeAry[ i ][ 0 ] != id ){
				continue;
			}

			gModeAry[ i ] = ary;
			newID = 0;
			break;
		}

		if ( newID == 1 ){
			gModeAry.push( ary );
		}
		showModeLi( id ); // show mode to a LI
	}

	function readDeleteMode( cmrAry ){ // read delete mode message
		// e.g. CMR 302 MACRO_DELETE _MODE_M01_MODE01
		var id = cmrAry[ 3 ].replace( "_MODE_" , "" );

		for ( i = 0 ; i < gModeAry.length ; i ++ ){
			if ( gModeAry[ i ][ 0 ] != id ){
				continue;
			}

			gModeAry.splice( i , 1 );
			break;
		}
		removeModeLi( id );
	}

	function uiClickPanelMode(){ // show mode panel
		$( ".cfgModeLi" ).remove(); // remove all old mode li
		showModeList(); // add all exist mode to li
		$( ".cfgModeSec" ).show();
	}

	function uiClickNewMode(){ // add a new mode by user
		var num = getModeNum() + 1;
		if ( num < 10 ){
			num = "0" + num;
		}
		var pos = num;
		var name = "MODE" + num;
		var id = "M" + pos + "_" + name;
		saveMode( id );
		setCurrentMode( id ); // set current mode to new mode
	}

	function setCurrentMode( id ){ // set current mode
		var name = "_CURRENT_MODE";
		setMacro( name , id );
	}

	function uiClickSaveMode(){ // replace mode
		if ( $( ".currentMode" ).length == 0 ){
			return;
		}

		gModeCfmAct = 1;
		var name = $( ".currentMode" ).html();
		var msg = "Confirm to replace " + name + "?";

		$( ".cfgModeTxtSn" ).html( msg );
		$( ".cfgModeTxtDiv" ).show();
		$( ".cfgModeIptDiv" ).hide();
		$( ".cfgModeSecCvr" ).show();
		$( ".cfgModeCfmSec" ).show();
	}

	function uiClickRenameMode(){ // rename mode
		if ( $( ".currentMode" ).length == 0 ){
			return;
		}

		gModeCfmAct = 3;
		var name = $( ".currentMode" ).html();

		$( ".cfgModeCfmSec" ).find( ".txtIpt" ).val( name );
		$( ".cfgModeTxtDiv" ).hide();
		$( ".cfgModeIptDiv" ).show();
		$( ".cfgModeSecCvr" ).show();
		$( ".cfgModeCfmSec" ).show();
		$( ".cfgModeCfmSec" ).show();
		$( ".cfgModeCfmSec" ).find( ".txtIpt" ).focus();
	}

	function uiClickDeleteMode(){ // delete mode
		if ( $( ".currentMode" ).length == 0 ){
			return;
		}

		gModeCfmAct = 4;
		var name = $( ".currentMode" ).html();
		var msg = "Confirm to delete " + name + "?";

		$( ".cfgModeTxtSn" ).html( msg );
		$( ".cfgModeTxtDiv" ).show();
		$( ".cfgModeIptDiv" ).hide();
		$( ".cfgModeSecCvr" ).show();
		$( ".cfgModeCfmSec" ).show();
	}

	function uiClickDeleteAllMode(){
		for ( i = 0 ; i < gModeAry.length ; i ++ ){
			var id = gModeAry[ i ][ 0 ];
			deleteMode( id );
		}

		setCurrentMode( "EMPTY" ); // clear current mode setting
	}

	function uiClickModeCfmBtn(){ // confirm to replace/rename/delete mode
		var id = $( ".currentMode" ).attr( "id" );

		if ( gModeCfmAct == 1 ){ // replace mode
			saveMode( id );
		}else if ( gModeCfmAct == 3 ){ // rename mode
			var name = $( ".cfgModeCfmSec" ).find( ".txtIpt" ).val();
			if ( ! validateModeName( name ) ){
				return;
			}
			name = validateModeName( name );
			chgModeName( id , name );
		}else if ( gModeCfmAct == 4 ){ // delete mode
			deleteMode( id );
			setCurrentMode( "EMPTY" );
		}

		$( ".cfgModeSecCvr" ).hide();
		$( ".cfgModeCfmSec" ).hide();
	}

	function uiClickModeCnlBtn(){ // cancel to replace/rename/delete mode
		$( ".cfgModeSecCvr" ).hide();
		$( ".cfgModeCfmSec" ).hide();
	}

	function uiClickUseMode(){ // use mode
		var id = $( this ).attr( "id" );
		changeMode( id , "Manu" , 0 )
	}

	function showModeList(){
		gModeAry.sort();
		for ( i = 0 ; i < gModeAry.length ; i ++ ){
			var id = gModeAry[ i ][ 0 ];
			showModeLi( id );
		}

		showCurrentMode( gCurrMode );
	}

	function showModeLi( id ) {
		if ( $( "#" + id ).length > 0 ){
			return;
		}
		var pos = id.split( "_" )[ 0 ];
		var name = id.split( "_" )[ 1 ];
		var str = name.substr( 0 , 1 ).toUpperCase();
		var className = "";

		if ( str == "X" ){ // add background color if mode name is start from "X" or "Y" or "Z"
			className = "cfgModeX";
		}else if ( str == "Y" ){
			className = "cfgModeY";
		}else if ( str == "Z" ){
			className = "cfgModeZ";
		}else if ( str == "W" ){
			className = "cfgModeW";
		}else if ( str == "S" ){
			className = "cfgModeS";
		}

		$( ".cfgModeListUl" ).append( "<li class = 'cfgModeLi " + className + "' id = '" + pos + "_" + name + "' >" + name + "</li>" );
	}

	function useMode( id ){
		var name = id.split( "_" )[ 1 ];
		showCurrentMode( id ); // change current mode to this mode

		var update = 0;

		for ( i = 0 ; i < gModeAry.length ; i ++ ){
			if ( gModeAry[ i ][ 0 ] != id ){
				continue;
			}

			gMacOnAry = [];

			for ( j = 1 ; j < gModeAry[ i ].length ; j ++ ){
				var macName = gModeAry[ i ][ j ];
				var numStr = macName.split( "_" )[ 1 ];
				var rowID = "#macRow_" + numStr;

				if ( $( rowID ).length == 0 ){
					update = 1;
					continue;
				}

				gMacOnAry.push( gModeAry[ i ][ j ] );
			}
			break;
		}

		setMacro( "_ON_MACRO" , gMacOnAry );

		if ( update == 1 ){ // check if mode content is changed. ( some row are deleted. )
			var mode = "_MODE_" + id;
			setMacro( mode , gMacOnAry );
		}

		loadMacroSwtich(); // turn on macro
	}

	function removeModeLi( id ) {
		$( "#" + id ).remove();
	}

	function saveMode( id ){
		var name = "_MODE_" + id;

		setMacro( name , gMacOnAry );

		updateAllMacro(); // save all macro setting before saving mode to ensure macro setting is correct
	}

	function deleteMode( id ){
		var name = "_MODE_" + id;

		getSeqNum( gByManual );
		deleteMacro( name );
	}

	function clearMode( id ){
		getSeqNum( gByManual );
		//var cmd = "CMD " + gInitUserID + " BASE_CMD { CLEAR_BASE " + id + " }" + " SEQ " + gSeqNum;
		var cmd = "CMD " + gInitUserID + " BASE_CMD { CLEAR_BASE " + id + " }";

		pushCMD( cmd );
		showLog( "clearMode" , cmd );
	}

	function chgModeName( id , name ){
		var pos = id.split( "_" )[ 0 ];
		pos = pos.replace( "M" , "" );
		deleteMode( id );

		var newID = "M" + pos + "_" + name;
		saveMode( newID );
		setCurrentMode( newID );

		setTimeout( function (){
			$( ".cfgModeLi" ).remove();
			showModeList();
		} , 100 );
	}

	function validateModeName( input ){ // validate mode name ( not allow just to input @ # ! etc )
		if ( input.length == "" ){
			return false;
		}
		input = input.replace( / /g , "-" );
		input = input.replace( /_/g , "-" );
		input = input.replace( /"/g , "-" );
		input = input.replace( /\//g , "-" );
		input = input.replace( "|" , "-" );
		var regex = /[^a-zA-Z0-9\-]/;
		if ( regex.test( input ) ) {
			return false;
		}
		return input;
	}

	function getModeNum(){
		var num = 0;
		$( ".cfgModeLi" ).each( function (){
			var id = $( this ).attr( "id" );
			var pos = id.split( "_" )[ 0 ];
			pos = parseInt( pos.replace( "M" , "" ) );
			if ( pos > num ){
				num = pos;
			}
		} );
		return num;
	}

	function showCurrentMode( id ){
		$( ".cfgModeLi" ).removeClass( "currentMode" );

		if ( id == "EMPTY" ){
			$( ".panlCurrentModeDiv" ).hide();
		}else{
			var name = id.split( "_" )[ 1 ];
			$( ".panlCurrentModeSn" ).html( name );
			$( ".panlCurrentModeDiv" ).show();

			$( "#" + id ).addClass( "currentMode" );
		}
	}

	function getCorrectModeID(){ // get correct mode id by checking net position
		var id = "";

		if ( gCurrMode == "EMPTY" || typeof gCurrMode == "undefined" || gCurrMode == "" ){
			return id;
		}

		var currName = gCurrMode.split( "_" )[ 1 ]; // e.g. "M01_X6B9" > "X6B9"

		var split_B = "B";
		var split_T = "T";

		if ( currName.search( split_B ) > 0 ){ // see whether the user separate number by "B" or "T"
			var splitStr = split_B;
		}else{
			var splitStr = split_T;
		}

		var currV = currName.slice( 0 , 1 ).toUpperCase(); // e.g. "X6B9" > "X"
		var currQtyA = parseInt( currName.slice( 1 , currName.length ).split( splitStr )[ 0 ] ); // e.g "X6B9" > 6
		var currQtyB = parseInt( currName.slice( 1 , currName.length ).split( splitStr )[ 1 ] ); // e.g "X6B9" > 9

		if ( ! $.isNumeric ( currQtyA ) ){ // check current mode include a number
			return id;
		}

		var netPos = getFirstRowNetPos();

		if ( ! $.isNumeric( netPos ) ){
			return id;
		}

		var modeQtyAry = [];
		for ( i = 0 ; i < gModeAry.length ; i ++ ){
			var modeId = gModeAry[ i ][ 0 ];

			var name = modeId.split( "_" )[ 1 ];
			if ( name == "" || typeof name == "undefined" ){ // check mode name
				continue;
			}

			var mV = name.slice( 0 , 1 ); // check mode V
			if ( mV != currV ){
				continue;
			}

			if ( name.search( split_B ) > 0 ){ // check separate string ( B or T )
				var splitStr = split_B;
			}else{
				var splitStr = split_T;
			}

			var mQtyA = parseInt( name.slice( 1 , name.length ).split( splitStr )[ 0 ] ); // check mode qty
			var mQtyB = parseInt( name.slice( 1 , name.length ).split( splitStr )[ 1 ] );

			if ( ! $.isNumeric( mQtyA ) ){
				continue;
			}

			var mCalA = mQtyA; // modify array content for calulation
			var mCalB = mQtyB;

			if ( mQtyA < 0 && mQtyA > mQtyB ){
				mCalA = mQtyB;
				mCalB = mQtyA;
			}
			if ( mQtyA == 0 ){
				mCalA = -mQtyB;
			}
			if ( isNaN( mQtyB ) ){
				mCalB = mQtyA;
			}

			modeQtyAry[ modeQtyAry.length ] = [ modeId , mQtyA , mQtyB , mCalA , mCalB ];
		}

		modeQtyAry.sort( function( a , b ){ // sort modeQtyAry
			return a[ 1 ] - b[ 1 ];
		} );

		var currIndex = -1;
		for ( i = 0 ; i < modeQtyAry.length ; i ++ ){ // get the index number of current mode
			var mQtyA = modeQtyAry[ i ][ 1 ];
			var mQtyB = modeQtyAry[ i ][ 2 ];

			if ( currQtyA != mQtyA || currQtyB.toString() != mQtyB.toString() ){
				continue;
			}

			currIndex = i;
			break;
		}

		var newIndex = -1;
		for ( i = 0 ; i < currIndex ; i ++ ){
			var lowLimit = modeQtyAry[ i ][ 4 ];
			if ( netPos <= lowLimit ){
				newIndex = i;
				break;
			}
		}

		for ( i = modeQtyAry.length - 1 ; i > currIndex ; i-- ){
			var highLimit = modeQtyAry[ i ][ 3 ];
			if ( netPos >= highLimit ){
				newIndex = i;
				break;
			}
		}

		if ( newIndex == -1 ){
			return id;
		}

		if ( newIndex == currIndex ){ // see need to change mode or not
			return id;
		}

		id = modeQtyAry[ newIndex ][ 0 ];

		return id;
	}

	function autoCheckChangeMode(){ // check auto change mode every 1 second
		if ( gOnAllMacro == 0 ) { // check the big on off switch is on or not
			return;
		}
		if ( gAutoChgMode != 1 ){ // already checked leader by read Total Fill function
			return;
		}
		if ( gChgingMode == 1 ){
			return;
		}

		var id = getCorrectModeID();

		if ( id == "" ){
			return;
		}

		$.when( checkIsLeader() ).then( function (){
			if ( gIsLeader == 0 ){
				return;
			}
			changeMode( id , "Auto" );
		} );
	}

	function changeMode( id , type ){ 	// start changing mode process
		gChgingMode = 1;
		setTimeout( function (){
			gChgModeCount ++;

			var name = id.split( "_" )[ 1 ];

			useMode( id );
			setCurrentMode( id );

			var netPos = getFirstRowNetPos();

			if ( type == "Auto" ){
				var msg = "Auto Change Mode to " + name + " ( Net Position: " + netPos + " ) .";

			}else if ( type == "Manu" ){
				var msg = "Manual Change Mode to " + name + "."
			}

			if ( type == "Auto" ){
				sendMsg( gInitUserID , "MMNOTICE" , "CHGMODE " + msg ); // send triggered change mode notice
			}

			getSeqNum( gByFunction );

			msg += " Seq No.: " + gSeqNum + "." ; // send change mode notice
			sendTabMsg( msg );

			clearTimeout( gDelayTurnOnTimer );

			stopExtraMacro(); // just stop extra macro , not stop all macro

			setTimeout( function (){
				autoTurnOn( type );

				gChgingMode = 0;
				gChgModeCount = 0;
			} , 500 );
		} , 200 );
	}

	function autoTurnOn( type ){
		if ( gOnAllMacro == 0 ) { // check the big on off switch is on or not
			return;
		}

		if ( gAutoOn == 0 ){
			return;
		}

		$.when( checkIsLeader() ).then( function (){
			if ( gIsLeader == 0 ){
				return;
			}

			var openOrder = getOpenOrderNum(); // get open order number
			var algoNum = getRunningAlgoNum(); // get running algo number
			var limit = algoNum + gDefOpenOrdBuff ; // default buffer = 3

			if ( openOrder < limit || gGuaOpenOrdOn == 0 ){ // check outstanding open orders.
				turnOnExtraMacro( type );

			}else{
				var msg = "Fail to Auto Turn On Algo. Outstanding Open Order Number: " + openOrder + ". Algo Number: " + algoNum + ".";

				sendTabMsg( msg );

				if ( type == "Auto" ){
					sendMsg( gInitUserID , "MMNOTICE" , "CHGMODE_FAIL " + msg ); // send change mode fail notice
				}
			}
		} );
	}

	function showChgModeNotice( msg ){
		if ( gAlertSound == 1 ){
			soundManager.play( "notice" );
		}

		showToastr( "success" , "Change Mode" , msg );
	}

	function showChgModeFailNotice( msg ){
		$( ".panlGuarDiv" ).addClass( "fontColorNotice" );
		$( "#jpanel_msg" ).find( ".badgeSp" ).addClass( "bgColorNotice" );

		if ( gAlertSound == 1 ){
			soundManager.play( "alert" );
		}

		showToastr( "error" , "Change Mode Fail" , msg );
	}

	//////////////////// select leader function //////////////

	function checkIsLeader(){ // check if itself is a leader or not
		var isLeaderDefer = $.Deferred();

		if ( gIsLeader == 1 ){ // already is leader.
			replylLeaderOn();
			isLeaderDefer.resolve( null );
			return isLeaderDefer;
		}

		$.when( getLeaderID() ).then( function (){
			isLeaderDefer.resolve( null );
			return isLeaderDefer;
		} );

		return isLeaderDefer;
	}

	function getLeaderID(){ // find current leader
		var getIDDefer = $.Deferred(); // create defer object
		if ( gLeaderOn == 1 || gCallingLeader == 1 || gAskingSessID == 1 ){
			getIDDefer.resolve( null );
			return getIDDefer;
		}

		clearInterval( gGetLeaderIDInterval ); // clear previous timer
		gGetLeaderIDInterval = false;

		var interval = 950; // check received response every 0.3 second
		var wait = gLeaderLiveTime; // wait 3 seconds for leader response

		var leader = gLeaderID + "::" + gLeaderIP;
		var sess = gSessID + "::" + gIP;

		callLeader();
		return getIDDefer;

		function callLeader(){
			if ( gLeaderOn == 1 || gCallingLeader == 1 || gAskingSessID == 1 ){ // check if leader has response
				getIDDefer.resolve( null );
				return getIDDefer;
			}

			sendMsg( gInitUserID , "CALL_LEADER" , leader + " " + sess ); // call leader

			if ( !gGetLeaderIDInterval ){
				gGetLeaderIDInterval = setInterval( getLeader , interval );
			}
		}

		function getLeader(){
			if ( gLeaderOn == 1 ){ // check if leader has response
				clearInterval( gGetLeaderIDInterval );
				gGetLeaderIDInterval = false;
				getIDDefer.resolve( null );
				return getIDDefer;
			}

			if ( wait <= 0 ){
				clearInterval( gGetLeaderIDInterval );
				gGetLeaderIDInterval = false;
			}

			if ( wait <= 0 && gLeaderOn == 0 ){
				$.when( electLeader() ).then( function (){
					clearInterval( gGetLeaderIDInterval );
					gGetLeaderIDInterval = false;

					getIDDefer.resolve( null );
					return getIDDefer;
				} );
			}

			wait -= interval;
		}
	}

	function electLeader(){
		var eleDefer = $.Deferred();

		if ( gAskingSessID == 1 ){
			eleDefer.resolve( null );
			return eleDefer;
		}

		var eleDefer = $.Deferred();

		gSessIDAry = [];
		gLeaderOn = 0;

		var idIP = gSessID + "::" + gIP;

		sendMsg( gInitUserID , "GET_SESSID" , idIP ); // ask every one to send his session ID if leader do no response

		setTimeout( function (){ // wait 2 second to collect session ID
			if ( gLeaderOn == 1 ){ // check if leader has response
				eleDefer.resolve( null );
				return eleDefer;
			}

			if ( gSessIDAry.length == 0 ){
				setMacro( "_LEADER_ID" , idIP ); // assign leader

				gIsLeader = 1;
				$( "#userInfoIcon" ).show(); // show leader icon

				eleDefer.resolve( null );
				return eleDefer;
			}

			gSessIDAry = gSessIDAry.sort(); // sort session ID
			var lastIndex = gSessIDAry.length - 1 ;

			gLeaderID = parseInt( gSessIDAry[ lastIndex ][ 0 ] ); // use first session ID as leader ID
			gLeaderIP = gSessIDAry[ lastIndex ][ 1 ];

			if ( gLeaderID == gSessID && gLeaderIP == gIP ){
				gIsLeader = 1;
				$( "#userInfoIcon" ).show(); // show leader icon

			}else{
				gIsLeader = 0;
				$( "#userInfoIcon" ).hide();
			}

			setMacro( "_LEADER_ID" , gLeaderID + "::" + gLeaderIP ); // assign leader

			eleDefer.resolve( null );
			return eleDefer;
		} , 2000 );

		return eleDefer;
	}

	function replylLeaderOn(){ // reply leader on if it is a leader
		if ( gRepliedLeaderOn == 1 ){
			return;
		}

		sendMsg( gInitUserID , "LEADER_ON" , gSessID + "::" + gIP ); // response if itself is the leader

		gRepliedLeaderOn = 1;

		setTimeout( function (){
			gRepliedLeaderOn = 0;
		} , 500 );
	}

	function readLeaderID( cmrAry ){
		// e.g. CMR 302 MACRO _LEADER_ID NAME 927::192.168.0.110 ( someone assigned leader );
		gCallingLeader = 0;
		gAskingSessID = 0;
		clearInterval( gGetLeaderIDInterval );
		gGetLeaderIDInterval = false;

		leaderID = parseInt( cmrAry[ 5 ].split( "::" )[ 0 ] );
		leaderIP = cmrAry[ 5 ].split( "::" )[ 1 ];

		if ( typeof leaderID == "undefined" || isNaN( leaderID ) || leaderIP == 0 || typeof leaderIP == "undefined" ){
			return;
		}

		gLeaderID = leaderID;
		gLeaderIP = leaderIP;

		if ( gLeaderID == gSessID && gLeaderIP == gIP ){
			replylLeaderOn();
			resetLongInterval( 0 );
			gIsLeader = 1;
			$( "#userInfoIcon" ).show(); // show leader icon

		}else{
			gIsLeader = 0;
			$( "#userInfoIcon" ).hide();
		}
	}

	function readLeaderOnMsg( cmrAry ){
		// e.g. CMR 309 MSG LEADER_ON 308::192.168.0.110
		if ( gIsLeader == 0 ){
			resetLongInterval( 0 );
		}

		gCallingLeader = 0;
		gAskingSessID = 0;
		clearInterval( gGetLeaderIDInterval );
		gGetLeaderIDInterval = false;

		clearTimeout( gLeaderOnTimer );

		gLeaderOnTimer = setTimeout( function (){
			gLeaderOn = 0;
		} , gLeaderLiveTime );

		if ( gLeaderOn == 1 ){ // check if received leader response already
			var leaderID = parseInt( cmrAry[ 4 ].split( "::" )[ 0 ] );
			var leaderIP = cmrAry[ 4 ].split( "::" )[ 1 ];

			if ( gLeaderID != leaderID || gLeaderIP != leaderIP ){
				multipleLeader( leaderID , leaderIP ); // ask the another leader to get new ID
			}

			return;
		}

		gLeaderOn = 1;

		gLeaderID = parseInt( cmrAry[ 4 ].split( "::" )[ 0 ] );
		gLeaderIP = cmrAry[ 4 ].split( "::" )[ 1 ];

		if ( typeof gLeaderIP == "undefined" ){
			gLeaderIP = 0;
		}
	}

	function replyLeaderID( cmrAry ){ // response if someone call leader
		//e.g. CMR 302 MSG CALL_LEADER 308::192.168.0.110
		gCallingLeader = 1;

		clearTimeout( gReplyLeaderTimer ); // clear previous timer

		gReplyLeaderTimer = setTimeout( function (){ // wait 0.5 second to avoid reading too many CALL_LEADER msg
			var leaderID = cmrAry[ 4 ].split( "::" )[ 0 ];
			var ip = cmrAry[ 4 ].split( "::" )[ 1 ];

			if ( gSessID != leaderID || gIP != ip ){ // check itself is leader or not
				return;
			}

			replylLeaderOn();
		} , 500 );
	}

	function sendSessID(){ // send session id to every one
		gAskingSessID = 1;
		clearTimeout( gSendSessIDTimer ); // clear previous timer

		gSendSessIDTimer = setTimeout( function (){ // wait 0.5 sec to avoid reading too many send sessID request
			sendMsg( gInitUserID , "SEND_SESSID" , gSessID + "::" + gIP );
		} , 500 );
	}

	function collectSessID( cmrAry ){ // collect session ID information
		// e.g. CMR 302 MSG SEND_SESSID 928::192.168.0.110
		var sessID = parseInt( cmrAry[ 4 ].split( "::" )[ 0 ] );
		var ip = cmrAry[ 4 ].split( "::" )[ 1 ];

		if ( sessID == 0 || sessID.toString() == "NaN" || typeof ip == "undefined" || ip == 0 ){
			return; // ignore if it has wrong seeID or ip
		}

		if ( jQuery.inArray( sessID , gSessIDAry ) >= 0 ){
			return;
		}
		gSessIDAry[ gSessIDAry.length ] = [ sessID , ip ]; // save session ID to an array
	}

	function getSessID( cmrAry ){ // store session ID after get macro
		// e.g. CMR 302 GET_MACRO SESSID 198 SEQ 192.168.0.110::48
		var seq = cmrAry[ 6 ];

		var ip = seq.split( "::" )[ 0 ];
		var num = parseInt( seq.split( "::" )[ 1 ] );

		if ( gSessID != 0 ){
			return;
		}

		if ( ip != gIP || num != gRandomNum ){
			return;
		}

		gSessID = parseInt( cmrAry[ 4 ] );
		$( "#userInfoID" ).text( gSessID );
	}

	function multipleLeader( sessID , ip ){
		sendMsg( gInitUserID , "MULTIPLE_LEADER" , sessID + "::" + ip ); // request this user to get new session ID
	}

	function getNewSessID( cmrAry ){ // get a new session ID
		if ( gGettingSessID == 1 ){ // check the user is getting new ID or not
			return;
		}

		var sessID = parseInt( cmrAry[ 4 ].split( "::" )[ 0 ] );
		var ip = cmrAry[ 4 ].split( "::" )[ 1 ];

		if ( gSessID != sessID || gIP != ip ){ // check is it this user
			return;
		}

		gSessID = 0; // delete old ID
		gGettingSessID = 1; // getting new ID

		getMacro(); // getMacro to get new session ID

		setTimeout( function (){
			gGettingSessID == 0; // wait 0.2 second to get new ID
		} , 200 );
	}

	/////////////////// sort Row function ////////////

	function saveRowSeq(){ // save row sequence info
		gRowSeqAry = []; // clear old row sequence info

		$( ".macRow" ).each( function (){
			var rowID = $( this ).attr( 'id' );
			gRowSeqAry.push( rowID ); // add each row ID to an array
		} );

		setMacro( "_ROW_SEQ" , gRowSeqAry );
		// save row sequence info to a macro
		// e.g. CMD 302 MACRO _ROW_SEQ NAME { macRow_01 macRow_03 macRow_02 }
	}

	function readRowSeq( cmrAry ){ // read row sequence info
		// e.g. CMR 302 MACRO _ROW_SEQ NAME { macRow_01 macRow_03 macRow_02 }
		gRowSeqAry = [];
		//for ( i = 6 ; i < cmrAry.length - 3 ; i ++ ){ // use this if cmd added seq number
		for ( i = 6 ; i < cmrAry.length - 1 ; i ++ ){
			var rowID = cmrAry[ i ];
			gRowSeqAry.push( rowID );
		}
		applyRowSeq(); // apply the row sequence
	}

	function addRowSeq( rowID ){ // add a row to row sequence list
		var newRow = 1;

		for ( i = 0 ; i < gRowSeqAry.length ; i ++ ){
			if ( gRowSeqAry[ i ] == rowID ){
				newRow = 0;
				break;
			}
		}

		if ( newRow == 1 ){
			gRowSeqAry.push( rowID );
		}

		setMacro( "_ROW_SEQ" , gRowSeqAry ); // update row sequence info to macro
	}

	function removeRowSeq( rowID ){ // remove row from row sequence list
		var index = gRowSeqAry.indexOf( rowID ); // find the row

		if ( index == -1 ) {
			return;
		}

		gRowSeqAry.splice( index , 1 ); // delete it from list

		setMacro( "_ROW_SEQ" , gRowSeqAry );
	}


	function applyRowSeq(){ // apply row sequence
		for ( i = 0 ; i < gRowSeqAry.length; i ++ ){
			var rowID = "#" + gRowSeqAry[ i ];
			$( rowID ).hide();
			$( rowID ).appendTo( ".panlSec" ).show(); // append the row behind last row
		}
	}

	function uiClickSortRow( mode ){ // sort row
		if ( mode == 1 ){ // ascending sort
			$( ".panlSortAscBtn" ).hide();
			$( ".panlSortDescBtn" ).fadeIn();
		}else if ( mode == 2 ){ // descending sort
			$( ".panlSortDescBtn" ).hide();
			$( ".panlSortAscBtn" ).fadeIn();
		}

		for ( i = 0 ; i < $( ".macRow" ).length; i ++ ){ // sort row by pips
			$( ".macRow" ).sort( function ( a , b ){
				var a_bid = Math.abs( parseFloat( $( a ).find( ".bidSec" ).find( ".ctrlPipsDiv" ).find( ".txtIpt" ).val() ) );
				var a_ask = Math.abs( parseFloat( $( a ).find( ".askSec" ).find( ".ctrlPipsDiv" ).find( ".txtIpt" ).val() ) );
				var a_spread = ( a_bid + a_ask ) / 2;

				var b_bid = Math.abs( parseFloat( $( b ).find( ".bidSec" ).find( ".ctrlPipsDiv" ).find( ".txtIpt" ).val() ) );
				var b_ask = Math.abs( parseFloat( $( b ).find( ".askSec" ).find( ".ctrlPipsDiv" ).find( ".txtIpt" ).val() ) );
				var b_spread = ( b_bid + b_ask ) / 2;

				checkSort( a , b , a_spread , b_spread );
			} );
		}

		for ( i = 0 ; i < $( ".macRow" ).length; i ++ ){ // sort row by symbol
			$( ".macRow" ).sort( function ( a , b ){
				var a_sym = $( a ).find( ".symDiv" ).find( ".current" ).html();
				a_sym = a_sym.replace( / /g , "" );

				var b_sym = $( b ).find( ".symDiv" ).find( ".current" ).html();
				b_sym = b_sym.replace( / /g , "" );

				checkSort( a , b , a_sym , b_sym );
			} );
		}

		for ( i = 0 ; i < $( ".macRow" ).length; i ++ ){ // sort row by ecn
			$( ".macRow" ).sort( function ( a , b ){
				var a_ecn = $( a ).find( ".ecnDiv" ).find( ".current" ).html();
				a_ecn = a_ecn.replace( / /g , "" );

				var b_ecn = $( b ).find( ".ecnDiv" ).find( ".current" ).html();
				b_ecn = b_ecn.replace( / /g , "" );

				checkSort( a , b , a_ecn , b_ecn );
			} );
		}

		function checkSort( a , b , aVal , bVal ){
			if ( mode == 1 ){
				if ( aVal <= bVal ){
					return;
				}
			}else if ( mode == 2 ){
				if ( aVal >= bVal ){
					return;
				}
			}
			$( a ).hide();
			$( a ).appendTo( ".panlSec" ).show();
		}
		saveRowSeq();
	}

	/////////////////// UI function ///////////////////

	function uiClickIfdSwitch(){ // click if done switch
		var ckedSw = $( this );
		var index = $( ckedSw ).index();
		var top = -122 + ( index * 27 );
		var ifdSwDiv = $( ckedSw ).closest( ".macRow" ).find( ".ifdSwDiv" );

		$( ifdSwDiv ).each( function (){ // change both bid side and ask side if done switch
			var ifdSw = $( this );
			$( ifdSw ).find( ".ifdSwLbl" ).removeClass( "ifdSwChked" , 300 );
			$( ifdSw ).find( ".ifdSwBg" ).css( "top" , top + "px" );
			$( ifdSw ).find( ".ifdSwLbl:eq( " + index + " )" ).addClass( "ifdSwChked" , 300 );
		} );
	}

	function uiInputTxtValue(){ // input number to input box
		var iptVal = this.value;
		var iptCtrl = $( this );
		var maxLimit = 50;

		if ( $( iptCtrl ).closest( ".ctrlDiv" ).hasClass( "ctrlPipsDiv" ) ){
			var iptClass = ".ctrlPipsDiv";
			var	iptValDef = gDefPipsMax;
			var iptValMax = gDefPipsMax * maxLimit;
			var	iptValMin = gDefPipsMin;
		}else if ( $( iptCtrl ).closest( ".ctrlDiv" ).hasClass( "ctrlQtyDiv" ) ){
			var iptClass = ".ctrlQtyDiv";
			var	iptValDef = gDefQtyMax;
			var iptValMax = gDefQtyMax * maxLimit;
			var	iptValMin = gDefQtyMin;
		}

		if ( ! $.isNumeric( iptVal ) ){ // input validation
			iptVal = iptValDef;
		}else{
			iptVal = Math.abs( iptVal );
		}

		if ( iptVal > ( iptValMax ) ){
			iptVal = iptValMax;
		}else if ( iptVal < iptValMin ){
			iptVal = iptValMin;
		}

		var iptGp = $( iptCtrl ).closest( ".macRow" ).find( iptClass );

		$( iptGp ).each( function (){ // change both bid side and ask side input value
			var iptDiv = $( this );
			var val = iptVal;
			$( iptDiv ).find( ".sldrBg" ).slider( "value" , val );

			if ( iptClass == ".ctrlPipsDiv" ){
				if ( $( iptDiv ).closest( ".sideSec" ).hasClass( "bidSec" ) ){
					val = -val;
				}else{
					val = "+" + val;
				}
			}

			$( iptDiv ).find( ".sldrHd" ).text( val );
			$( iptDiv ).find( ".txtIpt" ).val( val );
		} );
	}

	function uiClickCfgChkbox(){ // click config check box
		if ( $( this ).prop( "checked" ) ){
			$( this ).closest( ".cfgDiv" ).find( ".txtIpt" ).removeClass( "disableCtrl" , 300 );
			$( this ).closest( ".cfgDiv" ).find( ".txtIpt" ).prop( 'disabled' , false );
			$( this ).closest( ".cfgDiv" ).find( ".cfgCutChk" ).removeClass( "disableCtrl" , 300 );
			$( this ).closest( ".cfgDiv" ).find( ".cfgCutChk" ).prop( 'disabled' , false );

			if ( $( this ).closest( ".cfgDiv" ).hasClass( "cfgVolumeDiv" ) ){
				$( ".cfgResumeVolumeDiv" ).find( ".txtIpt" ).removeClass( "disableCtrl" , 300 );
				$( ".cfgResumeVolumeDiv" ).find( ".txtIpt" ).prop( 'disabled' , false );
				$( ".cfgResumeVolumeDiv" ).find( ".cfgOnChk" ).removeClass( "disableCtrl" , 300 );
				$( ".cfgResumeVolumeDiv" ).find( ".cfgOnChk" ).prop( 'disabled' , false );
			}
		}else{
			$( this ).closest( ".cfgDiv" ).find( ".txtIpt" ).addClass( "disableCtrl" , 300 );
			$( this ).closest( ".cfgDiv" ).find( ".txtIpt" ).prop( 'disabled' , true );
			$( this ).closest( ".cfgDiv" ).find( ".cfgCutChk" ).addClass( "disableCtrl" , 300 );
			$( this ).closest( ".cfgDiv" ).find( ".cfgCutChk" ).prop( 'disabled' , true );

			if ( $( this ).closest( ".cfgDiv" ).hasClass( "cfgVolumeDiv" ) ){
				$( ".cfgResumeVolumeDiv" ).find( ".txtIpt" ).addClass( "disableCtrl" , 300 );
				$( ".cfgResumeVolumeDiv" ).find( ".txtIpt" ).prop( 'disabled' , true );
				$( ".cfgResumeVolumeDiv" ).find( ".cfgOnChk" ).addClass( "disableCtrl" , 300 );
				$( ".cfgResumeVolumeDiv" ).find( ".cfgOnChk" ).prop( 'disabled' , true );
				$( ".cfgResumeVolumeDiv" ).find( ".cfgOnChk" ).prop( 'checked' , false );
			}
		}
	}

	function uiChgRowEcn(){ // change row ecn
		var ecn = $( this ).val();
		var sym = $( this ).closest( ".rowInfSec" ).find( ".symDiv" ).find( ".current" ).html();
		sym = sym.replace( / /g , "" );

		var symSlt = $( this ).closest( ".rowInfSec" ).find( ".symSlt" );
		$( symSlt ).empty(); // delete old symbol list

		var symECN = eval( "gSymOpt_" + ecn );
		var symList = "";
		$.each( symECN , function ( value ) {
			symList += "<option value = " + value + "> "+ value + "</option>";
		} );
		$( symSlt ).append( symList ); // append new symbol list
		var hasThisSym = $( symSlt ).find( 'option[ value = "' + sym + '" ]' ).length; // check new symbol list include old symbol or not

		if ( hasThisSym == 1 ){
			$( symSlt ).val( sym ); // select old symbol
		}else if ( ecn == "MREX" || ecn == "PATS" || ecn == "PATX" ){
			$( symSlt ).val( gDefSym ); // select default symbol
		}else{
			$( symSlt ).eq( 0 ).prop( 'selected' , true ); // select the first symbol
		}

		$( symSlt ).foundationCustomForms(); // call foundationCustomForms to apply symbol list change
		$( this ).closest( ".macRow" ).find( ".symDiv" ).find( ".current" ).html( $( symSlt ).val() );

		showSymSpread(); // show spread for new symbol
	}

	function uiChgRowSym(){ // change row symbol
		showSymSpread(); // show spread for new symbol
	}

	function showSlider(){ // show macro pip control slider
		$( ".sldrBg" ).stop( true , true ); // clear previous jquery animation
		$( ".sldrBg" ).hide();
		var sldrVal = Math.abs( $( this ).val() );
		$( this ).closest( ".ctrlDiv" ).find( ".sldrBg" ).slider( "value" , sldrVal );
		$( this ).closest( ".ctrlDiv" ).find( ".sldrBg" ).fadeIn();
	}

	function hideSlider( evt ){ // hide macro pip control slider
		if ( $( evt.target ).closest( ".ctrlSec" ).length > 0 ){
			return;
		}
		$( ".sldrBg" ).stop( true , true );
		$( ".sldrBg" ).fadeOut();
	}

	function showMacOnWarn(){ // show warnning if the macro is on
		var macSw = $( this ).closest( ".sideSec" ).find( ".macSw" );

		if ( $( macSw ).find( ".swOnLbl" ).hasClass( "swMacOn" ) ){
			$( macSw ).find( ".swOffLbl" ).stop( true , true );
			$( macSw ).find( ".swOffLbl" ).addClass( "swOnNotice" , 300 );
			$( macSw ).find( ".swOffLbl" ).delay( 600 ).removeClass( "swOnNotice" , 300 );
		}
	}

	function showRowOnWarn(){ // show warnning if bid or ask side macro is on
		$( this ).closest( ".macRow" ).find( ".swOnLbl" ).each( function (){
			if ( !$( this ).hasClass( "swMacOn" ) ){
				return;
			}

			$( this ).siblings( ".swOffLbl" ).stop( true , true );
			$( this ).siblings( ".swOffLbl" ).addClass( "swOnNotice" , 300 );
			$( this ).siblings( ".swOffLbl" ).delay( 600 ).removeClass( "swOnNotice" , 300 );
		} );
	}

	function showOffAllWarn(){ // show warnning if one of the macro is on
		$( ".panlSwDiv" ).find( ".swOnLbl" ).addClass( "swOnNotice" , 300 );
		$( ".panlSwDiv" ).find( ".swOnLbl" ).delay( 600 ).removeClass( "swOnNotice" , 300 );
	}

	function readPlacedPx( cmrAry ){ // show macro placed order price
		// CMR 300 MSG ALGO Mac_01_BUY O LO2 MREX CME/CMX_GLD/AUG17 30270720033500022 BUY 2 1217.7
		if ( cmrAry.length < 12 ){
			return;
		}
		var ecn = cmrAry[ 7 ];
		var sym = cmrAry[ 8 ];
		var px = parseFloat( cmrAry[ 12 ] );
		var symDip = getSymItem( ecn , sym ).dip; // get symbol decimal places

		if ( ! $.isNumeric( px ) ){
			return;
		}

		px = px.toFixed( symDip );
		var numStr = cmrAry[ 4 ].split( "_" )[ 1 ];
		var side = cmrAry[ 4 ].split( "_" )[ 2 ];

		if ( side == "BUY" ){
			var bidOrAsk = ".bidSec";
		}else{
			var bidOrAsk = ".askSec";
		}

		var rowID = "#macRow_" + numStr;
		var oPx = $( rowID ).find( bidOrAsk ).find( ".infPxDiv" ).find( ".txtIpt" ).val();

		if ( px == oPx ){
			return;
		}
		$( rowID ).find( bidOrAsk ).find( ".infPxDiv" ).find( ".txtIpt" ).val( px );
	}

	function readTotalFill( cmrAry ){ // read total fill quantity message
		// e.g. CMR 300 TOTAL_FILL MREX CME/CMX_GLD/AUG17 15 46 SELL 1 1246.7 OPX 1246.4
		if ( cmrAry.length < 7 ){
			return;
		}

		saveLastFillSide( cmrAry );
		showFilledQty( cmrAry );
		noTradeGuardian();

		clearTimeout( gReadTotalFillTimer );

		gReadTotalFillTimer = setTimeout( function (){ // wait 1 second to avoid reading too many partil fill msg
			$.when( checkIsLeader() ).then( function (){
				if ( gIsLeader == 0 ){
					return;
				}
				sendTradeMsg( cmrAry );
				filledQtyGuardian( cmrAry );
				slowFedGuardian( cmrAry );
				triggerOffRowGuardian();
			} );
		} , 1000 );
	}

	function showFilledQty( cmrAry ){
		if ( gOnAllMacro == 0 ){
			return;
		}

		var rEcn = cmrAry[ 3 ]; // new add
		var rSym = cmrAry[ 4 ];
		var rBuy = parseFloat( cmrAry[ 5 ] );
		var rSell = parseFloat( cmrAry[ 6 ] );
		var newSym = true;

		for ( i = 0 ; i < gTtlFillAry.length ; i ++ ){
			var oEcn = gTtlFillAry[ i ][ 0 ];
			var oSym = gTtlFillAry[ i ][ 1 ];
			if ( rEcn != oEcn ){
				continue;
			}
			if ( rSym != oSym ){
				continue;
			}

			newSym = false;

			var oBuy = gTtlFillAry[ i ][ 2 ];
			var oSell = gTtlFillAry[ i ][ 3 ];
			var netBuy = rBuy - oBuy;
			var netSell = rSell - oSell;

			$( ".macRow" ).each( function (){
				var mEcn = $( this ).find( ".ecnDiv" ).find( ".current" ).html();
				mEcn = mEcn.replace( / /g , "" );

				var mSym = $( this ).find( ".symDiv" ).find( ".current" ).html();
				mSym = mSym.replace( / /g , "" );
				if ( mEcn != rEcn ){
					return;
				}

				if ( mSym != rSym ){
					return;
				}

				var oNetBuy = $( this ).find( ".bidSec" ).find( ".infQtyDiv" ).find( ".txtIpt" ).val();
				var oNetSell = $( this ).find( ".askSec" ).find( ".infQtyDiv" ).find( ".txtIpt" ).val();
				if ( netBuy != oNetBuy ){
					$( this ).find( ".bidSec" ).find( ".infQtyDiv" ).find( ".txtIpt" ).val( netBuy );
				}
				if ( netSell != oNetSell ){
					$( this ).find( ".askSec" ).find( ".infQtyDiv" ).find( ".txtIpt" ).val( netSell );
				}
			} );
			break;
		}

		if ( newSym ){
			gTtlFillAry[ gTtlFillAry.length ] = [ rEcn , rSym , rBuy , rSell ]; // store total fill info to an array
		}
	}

	function saveLastFillSide( cmrAry ){
		gLastFillSide = cmrAry[ 7 ];
	}

	function sendTradeMsg( cmrAry ){
		var ecn = cmrAry[ 3 ];
		var sym = cmrAry[ 4 ];
		var side = cmrAry[ 7 ];
		var qty = parseFloat( cmrAry[ 8 ] );
		var px = parseFloat( cmrAry[ 9 ] );
		var ordPx = cmrAry[ 11 ];

		if ( ordPx == "UNKNOWN" || ordPx == "NIL" ){
			return;
		}

		var ts = parseFloat( getSymItem( ecn , sym ).ts ); // get tick size
		var dip = parseFloat( getSymItem( ecn , sym ).dip ); // get symbol decimal places


		if ( isNaN( qty ) || isNaN( px ) || isNaN( ts ) || isNaN( dip ) ){
			showLog( "sendTradeMsg" , "Error On Reading Trade Msg. qty : " + qty + " . trade px : " + px + " . tick size : " + ts + " . decimal places : " + dip );
			return;
		}

		px = px.toFixed( dip );

		var tradeMsg = side + " " + ecn + " " + sym + " at " + px + " .";

		var symMsg = "";

		var symInfo = "";

		symInfo = getSymNetPos( ecn , sym );

		var netPos = symInfo.netPos; // calulate total net position for a product
		var avgPx = symInfo.avgPx;

		if ( $.isNumeric( netPos ) && $.isNumeric( avgPx ) ){
			symMsg = " Net Position: " + netPos + ". Average Price: " + avgPx + ".";
		}

		var accMsg;
		accPnL = getAccPnL(); // get account Pnl
		if ( $.isNumeric( accPnL ) ){
			accMsg = " P&L: " + accPnL + ".";
		}

		var msg = tradeMsg + symMsg + accMsg;

		sendTabMsg( msg );
	}

	/////////////////////////// Other function ///////////////

	function getSeqNum( type ){ // get sequence number
		if ( gActNo >= 100 ){
			gActNo = 0;
		}

		var actNo = gActNo;

		if ( gActNo < 10 ){
			actNo = "0" + gActNo
		}

		gSeqNum = type.toString() + gSessID.toString() + actNo.toString() + gVersion; // e.g. SEQ 7378391a
		gActNo ++;
	}


	function getSymItem( ecn , sym ){ // get symbol details ( tick size , decimal place )
		if ( ecn == "PATX" ){
			ecn == "MREX";
		}

		var symObj = { };
		symObj.e = ecn;
		symObj.s = sym;
		var symItem = gInitSymItem( symObj );
		return symItem;
	}

	function getSymNetPos( ecn , sym ){ // get symbol net position
		sym = sym.replace( /\//g , "_" );

		var netPos = gInitNetPos( gInitUserID , ecn , sym )[ 0 ];
		var avgPx = gInitNetPos( gInitUserID , ecn , sym )[ 1 ];

		var netPosNum = parseFloat ( netPos );
		var avgPxNum = Math.round( parseFloat ( avgPx ) * 100 ) / 100;

		if ( ! validateParameter( "netPos" , netPosNum , netPos ) ){
			netPosNum = 0;
		}

		if ( ! validateParameter( "avgPx" , avgPxNum , avgPx ) ){
			avgPxNum = 0;
		}

		symInfo = { netPos: netPosNum , avgPx: avgPxNum };
		return symInfo;
	}

	function getFirstRowNetPos(){
		var symAry = getSymAry(); // get symbol array;

		var ecn = symAry[ 0 ][ 0 ]; // read first row net position
		var sym = symAry[ 0 ][ 1 ];
		var netPos = getSymNetPos( ecn , sym ).netPos;

		return netPos;
	}

	function getSymMarketPx( ecn , sym ){ // get symbol market price
		if( ecn == "PATX" ){
			ecn == "MREX";
		}

		sym = sym.replace( /\//g , "_" );

		var mktPx = gInitMktPx( ecn , sym );
		var mktPxNum = parseFloat ( mktPx );

		if ( ! validateParameter( "mktPx" , mktPxNum , mktPx ) ){
			mktPxNum = 0;
		}

		return mktPxNum;
	}

	function getSymBidAskPx( ecn , sym ){
		if( ecn == "PATX" ){
			ecn == "MREX";
		}

		var sym = sym.replace( /\//g , "_" );
		var str = ecn + "-" + sym;

		var bidPx = gInitBidAry[ str ];
		var bidPxNum = parseFloat ( bidPx );

		if ( ! validateParameter( "bidPx" , bidPxNum , bidPx ) ){
			bidPxNum = 0;
		}

		var askPx = gInitAskAry[ str ];
		var askPxNum = parseFloat ( askPx );

		if ( ! validateParameter( "askPx" , askPxNum , askPx ) ){
			askPxNum = 0;
		}

		var pxAry = [ bidPxNum , askPxNum ];
		return pxAry;
	}

	function getCutLossPx( ecn , sym , netPos , qty , bidPx , askPx ){
		var px = 0;
		var pnl = getAccPnL(); // get current PnL
		var maxLoss = gDefMaxLossBuffer;
		var buffer = pnl + maxLoss;

		var ts = getSymItem( ecn , sym ).ts; // get tick size
		var tc = getSymItem( ecn , sym ).tc; // get tick cent

		var pnlPerTick = ts * tc * Math.abs( netPos );

		var bufferTick = Math.abs( buffer / pnlPerTick );

		if ( bufferTick > gDefAutoFlatSlip ){
			bufferTick = gDefAutoFlatSlip;
		}

		var bufferPx = Math.round( bufferTick ) * ts;

		if ( qty > 0 ){
			px = askPx + bufferPx;
		}else{
			px = bidPx - bufferPx;
		}

		return px;
	}

	function getAccPnL(){ // get account PnL
		var pnl = gInitAccPnl[ gInitUserID ];

		if ( typeof pnl == "undefined" ){
			pnl = 0;
		}

		var pnlNum = Math.round( parseFloat( pnl ) * 10 ) / 10; // round number

		if ( ! validateParameter( "pnl" , pnlNum , pnl ) ){
			pnlNum = 0;
		}

		if ( pnlNum < -100000 || pnlNum > 100000 ){ // prevent reading wrong pnl number from GFA
			var msg = "Error on reading account PnL. PnL : " + pnl;
			sendLogMsg( msg );

			pnlNum = 0;
		}

		return pnlNum;
	}

	function getOpenOrderNum(){ // get open order number
		var openOrder = gInitOpenOrdCount();
		var orderNum = parseFloat( openOrder );

		if ( ! validateParameter( "orderNum" , orderNum , openOrder ) ){
			orderNum = 0;
		}

		return orderNum;
	}

	function getOnAlgoNum(){ // get on algo number
		var onAlgo = 0;
		$( ".macSw" ).each( function (){
			if ( $( this ).find( ".swOnLbl" ).hasClass( "swChked" ) && $( this ).find( ".swOnLbl" ).hasClass( "swMacOn" ) ){
				onAlgo ++;
			}
		} );
		return onAlgo;
	}

	function getRunningAlgoNum(){ // get total runnning algo number
		var num = gMacOnAry.length;
		return num;
	}

	function getSymAry(){
		var symAry = [];

		$( ".macRow" ).each( function (){
			var macRow = $( this );

			var rEcn = $( macRow ).find( ".ecnDiv" ).find( ".current" ).html();
			rEcn = rEcn.replace( / /g , "" );

			var rSym = $( macRow ).find( ".symDiv" ).find( ".current" ).html();
			rSym = rSym.replace( / /g , "" );

			var newSym = 1;

			for ( i = 0 ; i < symAry.length ; i ++ ){
				var ecn = symAry[ i ][ 0 ];
				var sym = symAry[ i ][ 1 ];

				if ( rEcn == ecn && rSym == sym ){
					newSym = 0;
					break;
				}
			}

			if ( newSym == 0 ){
				return;
			}
			symAry[ symAry.length ] = [ rEcn , rSym ];
		} );

		return symAry;
	}

	function getLocalStore(){ // load local conifg.
		var jShowLog = store.get( "jShowLog" );
		if ( $.isNumeric( jShowLog ) ){
			gShowLog = jShowLog;
		}

		var jAlertSound = store.get( "jAlertSound" );
		if ( $.isNumeric( jAlertSound ) ){
			gAlertSound = jAlertSound;
		}
	}

	function getRandomNumber(){ // 1 - 100
		var num = Math.floor( ( Math.random() * 100 ) + 1 );
		return num;
	}

	function uiClickReloadAll(){ // request every use to reload the page
		startMacro( "_RELOAD" );
	}

	function reloadAll(){ // reload page count down
		$( ".panlReldCvr" ).fadeIn(); // show a cover to cover all button ( not include STOPALL )
		$( ".panlReldNtcSec" ).fadeIn();

		var randomMSec = Math.random() * 10000 ;
		var randomSec = Math.round( randomMSec / 1000 );
		var reloadMSec = 5000;
		var reloadSec = reloadMSec / 1000;
		var totalSec = reloadSec + randomSec; // count down 5 - 15 seconds
		var totalMSec = randomMSec + reloadMSec; // add a random second to avoid everyone reload at the same time

		$( ".panlReldTimeSn" ).html( totalSec );

		showReload();
		var reloadInterval = setInterval( showReload , 1000 );

		setTimeout( reload , totalMSec );

		function showReload(){
			totalSec -= 1;
			$( ".panlReldTimeSn" ).html( totalSec );

			if ( totalSec <= 0 ){
				clearInterval( reloadInterval );
			}
		}

		function reload(){
			gReloaded = 1; // mark this user is reloaded. unable to send and receive any CMD and CMR

			clearInterval( gPanelShortInterval ); // stop panel timer
			gPanelShortInterval = false;
			clearInterval( gPanelLongInterval );
			gPanelLongInterval = false;

			showReloadNotice(); // show reload notice
		}
	}

	function showReloadNotice(){
		location.reload();
		$( ".panlReldNtcSn" ).hide();
		$( ".panlReldLdDiv" ).hide();
		$( ".panlReldWarnSn" ).fadeIn();
	}

	function flashMacroLight( rowID , side ){ // show a notice if the macro is triggered
		var infDiv = $( rowID ).find( side ).find( ".macInfDiv" );
		$( infDiv ).find( ".trigLgtDiv" ).addClass( "trigLgtOnDiv" ).removeClass( "trigLgtOnDiv" , 3000 );
	}

	function checkIptVal( ctrlDiv , maxVal , MinVal ){ // input validation
		var iptElem = $( ctrlDiv ).find( ".txtIpt" );
		var iptVal = parseFloat( $( iptElem ).val() );

		if ( iptVal <= maxVal && iptVal >= MinVal ){
			return iptVal;
		}else if ( iptVal > maxVal ){
			$( iptElem ).attr( "value" , maxVal );
		}else if ( iptVal < MinVal ){
			$( iptElem ).attr( "value" , MinVal );
		}

		$( iptElem ).addClass( "cfgWarnIpt" , 300 );
		$( iptElem ).delay( 600 ).removeClass( "cfgWarnIpt" , 300 );
		$( iptElem ).focus();
		return false;
	}

	function clearGlobalVal(){ // clear global variable
		gLastMmoNum = 0;
		gTtlFillAry = [];
		gMacOnAry = [];
		gSessIDAry = [];
		gSymSprdAry = [];
		gModeAry = [];
	}

	function showLog( name , content ){ // show con sole log
		if ( gShowLog != 1 ){
			return;
		}
		console.log( name + " : " + content );
	}

	function showDebugLog( name , content ){ // show con sole log
		if ( gShowLog != 1 ){
			return;
		}
		console.log( " DEBUG : " + name + " : " + content );
	}

	function ctrlKeyDown( e ){ // ctrl key down
		if ( e.keyCode != 17 ){
			return;
		}

		var $keyDelBtn = $( ".panlAddDiv" );
		if ( $keyDelBtn.is( ":hover" ) ){
			$( ".panlAddDiv" ).hide();
			$( ".panlDelAllDiv" ).show();
		}

		var $keyReldBtn = $( ".cfgPanlCfmDiv" );
		if ( $keyReldBtn.is( ":hover" ) ){
			$( ".cfgPanlCfmDiv" ).hide();
			$( ".cfgReldDiv" ).show();
		}

		var $modeDeleteBtn = $( ".cfgModeDeleteDiv" );
		if ( $modeDeleteBtn.is( ":hover" ) ){
			$( ".cfgModeDeleteDiv" ).hide();
			$( ".cfgModeDeleteAllDiv" ).show();
		}
	}

	function ctrlKeyUp( e ){ // ctrl key up
		if ( e.keyCode != 17 ){
			return;
		}
		$( ".panlDelAllDiv" ).hide();
		$( ".panlAddDiv" ).show();
		$( ".cfgReldDiv" ).hide();
		$( ".cfgPanlCfmDiv" ).show();
		$( ".cfgModeDeleteAllDiv" ).hide();
		$( ".cfgModeDeleteDiv" ).show();
	}

	function pushCMD( cmd ){
		if ( gDisconnected == 1 || gReloaded == 1 ){
			return;
		}

		gInitOrdSubscription.push( cmd );
	}

	function sendMsg( target , topic , msg ){ // send message
		getSeqNum( gByFunction );
		//var cmd = "CMD " + gInitUserID + " MSG " + target + " " + topic + " { " + msg + " }" + " SEQ " + gSeqNum;
		var cmd = "CMD " + gInitUserID + " MSG " + target + " " + topic + " { " + msg + " }";

		pushCMD( cmd );
		showLog( "sendMsg" , cmd );
	}

	function sendTabMsg( msg ){
		sendMsg( gInitUserID , "MMALGO" , msg );

		if ( gSendTelg == 0 ){
			return;
		}

		for ( i = 0 ; i < gTelgTarget.length ; i ++ ){
			sendMsg( gTelgTarget[ i ] , gInitUserID + ":" , msg );
		}
	}

	function sendLogMsg( msg ){ // send log message to server for debug
		getSeqNum( gByFunction );
		var topic = "LOGMSG";
		var cmd = "CMD " + gInitUserID + " MSG " + gInitUserID + " " + topic + " { " + msg + " . Seq No. : " + gSeqNum + " . IP : " + gIP + " }";

		pushCMD( cmd );
		showLog( "sendLogMsg" , cmd );
	}

	function readMsg( cmrAry ){ // read MSG cmr message
		// e.g. CMR 302 MSG MMALGO Stopped All Algo.
		if ( cmrAry.length < 6 ){
			return;
		}

		var cmrMsg = "";

		for ( i = 4 ; i < cmrAry.length ; i ++ ){
			if ( i > 4 ){
				cmrMsg += " ";
			}
			cmrMsg += cmrAry[ i ];
		}

		if ( cmrMsg == "Server Started." ){
			reloadAll(); // reload page if server is restarted.

		}else if ( cmrMsg.slice( 0 , 20 ) == "Updated Algo Setting" ){
			$( ".cfgPanlSec" ).fadeOut(); // hide panel config box if someone changed parameter

		}else if ( cmrMsg.slice( 0 , 24 ) == "Updated Guardian Setting" ){
			$( ".cfgGuarSec" ).fadeOut();
		}
	}

	function readNotice( cmrAry ){ // read notice msg
		// e.g. CMR 304 MSG MMNOTICE NOTICE2 Volume Update: CME/CRUDE/JAN18 994
		if ( cmrAry.length < 4 ){
			return;
		}

		notice = cmrAry[ 4 ];
		topic = notice.split( "_" )[ 0 ];
		type = notice.split( "_" )[ 1 ];

		var msg = "";
		for ( i = 5 ; i < cmrAry.length ; i ++ ){
			msg += cmrAry[ i ] + " ";
		}

		if ( topic == "GUARD" ){ // guardian msg
			showGuardianNotice( type , msg );

		}else if ( topic == "ALGOFUSE" ){ // hit algo fuse
			showAlgoFuseNotice( msg );

		}else if ( topic == "CHGMODE" ){ // change mode
			if ( type == "FAIL" ){
				showChgModeFailNotice( msg );
			}else{
				showChgModeNotice( msg );
			}

		}else if ( topic == "NOTICE1" ){ // roger's notice
			showRNotice( msg );
			twiceFillGuardian( msg );

		}else if ( topic == "NOTICE2" ){ // roger's notice
			volumeGuardian( msg );
		}
	}

	function sendTelgMsg( topic , msg ){ // show message to telegram
		if ( gSendTelg == 0 ){
			return;
		}

		for ( i = 0 ; i < gTelgTarget.length ; i ++ ){
			sendMsg( gTelgTarget[ i ] , topic , msg );
		}
	}

	function showBgAlgoCount(){ // show algo count on tab badge
		clearTimeout( gBgCountTimer ); // clear previous timer

		gBgCountTimer = setTimeout( function (){
			var onAlgoNum = getOnAlgoNum(); // get on algo number
			var badgeSp = $( gPanelID ).find( ".badgeSp" );
			var oOnAlgoNum = parseInt( $( badgeSp ).text() ); // get previous on algo number
			if ( onAlgoNum != oOnAlgoNum ){ // update the badge if the number is changed
				$( badgeSp ).text( onAlgoNum );
				$( badgeSp ).show();
			}
		} , 800 );
	}

	function hideBgAlgoCount(){ // hide algo count on tab badge
		clearTimeout( gBgCountTimer );

		gBgCountTimer = setTimeout( function (){
			var badgeSp = $( gPanelID ).find( ".badgeSp" );
			$( badgeSp ).text( "" );
			$( badgeSp ).hide();
		} , 800 );
	}

	function readAlgoRunning( cmrAry ){ // read ALGO-RUNNING message
		// e.g. CMR 302 ALGO-RUNNING 0
		gRunning = cmrAry[ 3 ];

		if ( gRunning == 0 ){
			hideOnAlgoGuardian(); // stop algo guardian if all algo are stopped
			$( gPanelID ).find( ".badgeSp" ).removeClass( "badgeOnSp" );
		}else if ( gRunning == 1 ){
			$( gPanelID ).find( ".badgeSp" ).addClass( "badgeOnSp" );
		}
	}

	function readDefaultParm( cmrAry ){ // read default algo parameter
		var name = cmrAry[ 3 ];
		var parm = parseFloat( cmrAry[ 4 ] );
	}

	function readGetParm( cmrAry ){ // read get parameter msg from server
		// e.g. CMR 302 GET _ONALL_MACRO 0
		var name = cmrAry[ 3 ];
		var parm = cmrAry[ 4 ];

		if ( parm == "NOT_FOUND" ){
			parm = 0;
		}

		parm = parseFloat( parm );

		gGotDefaultParm = 1;

		if ( name == "_ONALL_MACRO" ){
			trunPanelSwitch( parm );
		}else if ( name == "_PAUSE" ){
			gPause = parm;
			if ( gPause == 0 ){
				$( ".panlPauseNtcSec" ).hide();
			}
		}else if ( name == "CS_GUARDIAN_ALLOW_TRADE" ){
			gCSAllowTrade = parm;
			setCSAllowTradeIcon( parm );
		}
	}

	function validateParameter( parmName , parmNum , parm ){ // validate algo parameter
		if ( typeof parm == "undefined" || parmNum.toString() == "NaN" ){
			var msg = "parameter name: " + parmName + " is NaN or undefined. Parameter : " + parm;
			showDebugLog( "validateParameter" , msg );
			return false;
		}
		return true;
	}

	function showParmErrorNotice( type ){ // show algo parameter error ( NaN or undefined )
		var receivedType = 1;
		var sendType = 2;

		if ( type == receivedType ){
			$( ".cfgErrorSec" ).find( ".cfgMsgSp" ).html( "Received Invalid Algo Parameter From Server.<br>Please Review Algo Setting." );
		}else if ( type == sendType ){
			$( ".cfgErrorSec" ).find( ".cfgMsgSp" ).html( "Invalid Algo Parameter.<br>Please Review Algo Setting." );
		}

		$( ".cfgErrorSec" ).fadeIn();
	}

	function uiClickCloseErrorDiv(){ // close error notice box
		$( ".cfgErrorSec" ).fadeOut();
	}

	function showRNotice( msg ){ // notice msg from roger
		// e.g. BUY Twice in 1 minute , Same Algo FullyFilled > 1
		if ( gAlertSound == 1 ){
			soundManager.play( "order" );
		}

		showToastr( "warning" , msg , "ALGO Signal" );
	}

	function showAlgoFuseNotice( msg ){
		if ( gAlertSound == 1 ){
			soundManager.play( "order" );
		}

		showToastr( "warning" , msg , "ALGO Fuse" );
	}

	function getCurrentTime(){ // get time. format 2017_10_19_12_53_32 = 19/11/2017 12:53:32a.m.
		var date = new Date();

		var year = date.getFullYear().toString();
		var month = date.getMonth().toString();
		var day = date.getDate().toString();
		var hour = date.getHours().toString();
		var minute = date.getMinutes().toString();
		var second = date.getSeconds().toString();

		var time = year + "_" + month + "_" + day + "_" + hour + "_" + minute + "_" + second;

		return time;
	}

	function saveMaxProfit(){ // save daily maximum profit to server
		if ( gGuaLossFromTopOn == 0 ){ // check the guard maximum loss from top function is turnned on or not
			return;
		}

		var pnl = getAccPnL();
		if ( pnl <= gMaxProfit ){ // check if got new day high
			return;
		}

		gMaxProfit = pnl;
		showPnlStop();

		$.when( checkIsLeader() ).then( function (){
			if ( gIsLeader == 0 ){ // check if the user is leader or not
				return;
			}

			var time = getCurrentTime();
			var ary = [ time , gMaxProfit ];

			setMacro( "_MAX_PROFIT" , ary );
		} );
	}

	function showLossFromTop( macParm ){ // show loss from top information on algo tab
		var show = parseInt( macParm );
		if ( show == 0 ){
			$( ".panlMaxProfitDiv" ).fadeOut();
		}else if ( show == 1 ){
			$( ".panlMaxProfitDiv" ).fadeIn();
		}
	}

	function renewMaxProfit(){
		var time = getCurrentTime();
		var pnl = getAccPnL();
		var ary = [ time , pnl ];

		setMacro( "_MAX_PROFIT" , ary );
	}

	function readMaxProfit( cmrAry ){ // read maximum profit msg
		// e.g. CMR 302 MACRO _MAX_PROFIT NAME { 2017_10_19_12_53 2345 }
		var rHour = 5; // restart hour 5a.m.

		var mDateStr = cmrAry[ 6 ];
		var mYear = parseInt( mDateStr.split( "_" )[ 0 ] );
		var mMonth = parseInt( mDateStr.split( "_" )[ 1 ] );
		var mDay = parseInt( mDateStr.split( "_" )[ 2 ] );
		var mHour = parseInt( mDateStr.split( "_" )[ 3 ] );
		var mMinute = parseInt( mDateStr.split( "_" )[ 4 ] );
		var mSecond = parseInt( mDateStr.split( "_" )[ 5 ] );

		var mDate = new Date( mYear , mMonth , mDay , mHour , mMinute , mSecond ); // macro day

		var cDate = new Date(); // get current date

		var cDateStr = getCurrentTime();
		var cYear = parseInt( cDateStr.split( "_" )[ 0 ] );
		var cMonth = parseInt( cDateStr.split( "_" )[ 1 ] );
		var cDay = parseInt( cDateStr.split( "_" )[ 2 ] );
		var cHour = parseInt( cDateStr.split( "_" )[ 3 ] );

		var rDate = new Date( cYear , cMonth , cDay , rHour , 0 , 0 );
		if ( cHour < rHour ){
			rDate.setDate( rDate.getDate() - 1 ); // server restart time;
		}

		if ( mDate < rDate ){
			return; // macro date too old
		}

		if ( mDate > cDate ){
			return; // macro date too new
		}

		var maxProfit = parseInt( cmrAry[ 7 ] );
		gMaxProfit = maxProfit;

		showPnlStop();
	}

	function GFA_disconnected(){
		reloadAll(); // reload the page if GFA is disconnected.

		gDisconnected = 1; // disable panel react if gfa is disconnected
	}

	function showToastr( type , title , msg ){ // show GFA notice box
		var toastNum = $( ".toast" ).length;

		if ( toastNum >= 5 ){ // prevent showing too many toast box
			return;
		}

		if ( type == "success" ){
			var timeOut = 5000;
		}else if ( type == "info" ){
			var timeOut = 5000;
		}else if ( type == "warning" ){
			var timeOut = 600000;
		}else if ( type == "error" ){
			var timeOut = 600000;
		}

		var options = {
			"closeButton": true ,
			"debug": false ,
			"newestOnTop": false ,
			"progressBar": true ,
			"positionClass": "toast-bottom-left" ,
			"preventDuplicates": false ,
			"onclick": null ,
			"showDuration": "300" ,
			"hideDuration": "300" ,
			"timeOut": timeOut ,
			"extendedTimeOut": "1000" ,
			"showEasing": "swing" ,
			"hideEasing": "linear" ,
			"showMethod": "fadeIn" ,
			"hideMethod": "fadeOut"
		}

		gInitNotice( type , msg , title , options );
	}

	function getDefaultParameter( name ){
		var cmd = "CMD " + gInitUserID + " GET " + name;
		pushCMD( cmd );

		showLog( "getDefaultParameter" , cmd );
	}

	function sendStatus(){ // if-done example
		setTimeout( function (){
			getSeqNum( gByFunction );

			var cmd = "CMD " + gInitUserID + " IF { EQ _ONALL_MACRO 1 } { MSG " + gInitUserID + " MMALGO { ALGO is ON! } } { MSG " + gInitUserID + " MMALGO { ALGO is OFF! } }" + " SEQ " + gSeqNum;

			showLog( "Testing If Done" , cmd );
			pushCMD( cmd );
		} , 1000 );
	}

	function countDecimals( num ){ // count decimal places. e.g. 3.12 > 2
		if ( Math.floor( num ) === num ){
			return 0;
		}
		return num.toString().split( "." )[ 1 ].length || 0;
	}

	function getClientIP(){ // get user ip address
		var defer = $.Deferred();
		var retry = 0;

		gGetIPInterval = setInterval( getIP , 500 );
		return defer;

		function getIP(){
			if ( ( gIP != "" && typeof gIP != "undefined" ) || retry > 10 ){
				clearInterval( gGetIPInterval );
				defer.resolve( null );
				return defer;
			}

			window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
			var pc = new RTCPeerConnection( { iceServers:[] } ) , noop = function (){ };
			pc.createDataChannel( "" );
			pc.createOffer( pc.setLocalDescription.bind( pc ) , noop );

			pc.onicecandidate = function ( ice ){
				if ( !ice || !ice.candidate || !ice.candidate.candidate ) {
					return;
				}
				var regex = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/;

				gIP = regex.exec( ice.candidate.candidate )[ 1 ];
				$( "#userInfoIP" ).text( gIP );

				pc.onicecandidate = noop;
			};

			retry ++;
		}
	}

	function getMovingAvgPx( m ){
		if ( gOnAllMacro == 0 ){
			return;
		}

		var maBid = parseFloat( m.d[ 0 ].bid );
		var maAsk = parseFloat( m.d[ 0 ].ask );

		if ( isNaN( maBid ) || isNaN( maAsk ) ){
			return;
		}

		var ecn = m.e;
		var sym = m.s;
		var ts = parseFloat( getSymItem( ecn , sym ).ts ); // get tick size

		if ( isNaN( ts ) ){
			return;
		}

		var maSpread = ( maAsk - maBid ) / ts;
		var spreadLimit = 5;

		if ( maSpread <= 5 ){
			retun;
		}

		var newSym = 1;
		var sent = 0;
		for ( i = 0 ; i < gSpreadMsgAry.length ; i ++ ){
			var aECN = gSpreadMsgAry[ i ][ 0 ];
			var aSym = gSpreadMsgAry[ i ][ 1 ];
			var	aSent = gSpreadMsgAry[ i ][ 2 ];

			if ( aECN != ecn ){
				continue;
			}
			if ( aSym != sym ){
				continue;
			}

			sent = aSent;
			newSym = 0;

			if( aSent == 0 ){
				gSpreadMsgAry[ i ][ 2 ] = 1;
			}

			break;
		}

		if ( sent == 1 ){
			return;
		}

		if ( newSym == 1 ){
			gSpreadMsgAry[ gSpreadMsgAry.length ] = [ ecn , sym , 1 ];
		}

		gRecordAry = [];

		storeRecord();
		var recordInterval = setInterval( storeRecord , 1000 );

		function storeRecord(){
			if( gRecordAry.length >= 3 ){
				sendRecord();
				clearInterval( recordInterval );
			}

			var maBid = parseFloat( m.d[ 0 ].bid );
			var maAsk = parseFloat( m.d[ 0 ].ask );
			var bid = parseFloat( m.d[ 1 ].bid );
			var ask = parseFloat( m.d[ 1 ].ask );
			var ma15Vol = parseFloat( m.d[ 2 ].bidVol );
			var ma1Vol = parseFloat( m.d[ 2 ].ask );

			if ( isNaN( maBid ) || isNaN( maAsk ) || isNaN( bid ) || isNaN( ask ) ) {
				return;
			}

			if ( isNaN( ma15Vol ) ){
				ma15Vol = 0;
			}

			if ( isNaN( ma1Vol ) ){
				ma1Vol = 0;
			}

			gRecordAry[ gRecordAry.length ] = [ maBid , maAsk , bid , ask , ma15Vol , ma1Vol ];
		}

		function sendRecord(){
			var maBid = gRecordAry[ 0 ][ 0 ];
			var maAsk = gRecordAry[ 0 ][ 1 ];
			var bid = gRecordAry[ 0 ][ 2 ];
			var ask = gRecordAry[ 0 ][ 3 ];
			var ma15Vol = gRecordAry[ 0 ][ 4 ];
			var ma1Vol = gRecordAry[ 0 ][ 5 ];

			var msg = "Moving Average Spread Over 5 Pips on " + ecn + " " + sym + ". MA Bid: " + maBid + " . MA Ask: " + maAsk + " . Bid: " + bid + " . Ask: " + ask + " . 15 Minute Volume: " + ma15Vol + " . One Minute Volume: " + ma1Vol;

			sendTabMsg( msg );
		}
	}

	function getAccUIPnL(){
		var pnl = $( "#menu_bar_pl_grand_total" ).html();
		return pnl;
	}

	function showPnlStop(){
		var pnlStop = gMaxProfit + gGuaLossFromTopParm;
		pnlStop = pnlStop.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'); // 1000 > 1,000.00

		$( ".panlMaxProfitSn" ).text( pnlStop );
	}

	/////////////////// for chan shing ////////

	function uiClickCSPanlSwitch(){
		var swVal = $( this ).find( ".swIpt" ).val();
		if ( swVal == "Off" ){
			turnOffCSPanel();

		}else if ( swVal == "Flat" ){
			turnOffPanel();

			var squarePct = 100;
			var cutSlip = 10;

			clearTimeout( gAutoFlatTimer ); // clear previous timer
			gAutoFlatTimer = setTimeout( function (){
				squarePosition( gGuaAutoFlatQtyParm , gDefAutoFlatSlip );
			} , 1000 ); // wait 1 second to ensure _STOPALL is completed.

		}else if ( swVal == "On" ){
			if ( $( this ).hasClass( "panlSwChked" ) ){
				return;
			}

			turnOnCSPanel();
		}
	}

	function turnOnCSPanel(){
		try{
			updateCSParm();

			setDefaultParameter( "CS_GUARDIAN_ALLOW_TRADE" , 1 );

			var macName = "CS_START_ALL";
			startMacro( macName );

			getSeqNum( gByManual );
			var msg = "Trun On Algo. Seq No.: " + gSeqNum + ".";
			sendTabMsg( msg );

		}catch ( e ){
			showParmErrorNotice( 2 );
			showLog( "catch error" , e );
		}
	}

	function turnOffCSPanel(){
		var macName = "CS_STOP_ALL";
		startMacro( macName );

		getSeqNum( gByManual );
		var msg = "Trun Off Algo. Seq No.: " + gSeqNum + ".";
		sendTabMsg( msg );
	}

	function updateCSParm(){
		var macRow = $( ".macRow" ).first();

		var ecn = $( macRow ).find( ".ecnDiv" ).find( ".current" ).html();
		ecn = ecn.replace( / /g , "" );

		var sym = $( macRow ).find( ".symDiv" ).find( ".current" ).html();
		sym = sym.replace( / /g , "" );

		var enterSprd = parseFloat( $( macRow ).find( ".cfgEnterSpreadDiv" ).find( ".txtIpt" ).val() );
		var cutlossSprd = parseFloat( $( macRow ).find( ".cfgCutLossSpreadDiv" ).find( ".txtIpt" ).val() );
		var exitSprd = parseFloat( $( macRow ).find( ".cfgExitSpreadDiv" ).find( ".txtIpt" ).val() );
		var holdTime = parseFloat( $( macRow ).find( ".cfgHoldTimeDiv" ).find( ".txtIpt" ).val() );
		var restartTime = parseFloat( $( macRow ).find( ".cfgRestartTimeDiv" ).find( ".txtIpt" ).val() );
		var exitOrderSprd = parseFloat( $( macRow ).find( ".cfgExitOrderSpreadDiv" ).find( ".txtIpt" ).val() );
		var orderQty = parseFloat( $( macRow ).find( ".cfgOrderQtyDiv" ).find( ".txtIpt" ).val() );
		var commision = parseFloat( $( macRow ).find( ".cfgCommisionDiv" ).find( ".txtIpt" ).val() );

		if ( ! validateParameter( "enterSprd" , enterSprd , enterSprd ) || ! validateParameter( "cutlossSprd" , cutlossSprd , cutlossSprd ) || ! validateParameter( "exitSprd" , exitSprd , exitSprd ) || ! validateParameter( "holdTime" , holdTime , holdTime ) || ! validateParameter( "restartTime" , restartTime , restartTime )|| ! validateParameter( "exitOrderSprd" , exitOrderSprd , exitOrderSprd ) || !validateParameter( "orderQty" , orderQty , orderQty ) || !validateParameter( "commision" , commision , commision ) ){
			throw new Error( "Parameter is NaN or undefined." );
		}

		if( ecn != gCSParmObj.ECN || sym != gCSParmObj.SYM ){
			updateCSMacro( ecn , sym );
		}

		gCSParmObj.ECN = ecn;
		gCSParmObj.SYM = sym;
		gCSParmObj.CS_RESTART_TIME = restartTime;
		gCSParmObj.CS_EXIT_ORDER_SPREAD = exitOrderSprd;

		gCSParmObj.CS_BUY_ENTER_SPREAD = enterSprd;
		gCSParmObj.CS_BUY_EXIT_SPREAD = exitSprd;
		gCSParmObj.CS_BUY_CUTLOSS_SPREAD = cutlossSprd;
		gCSParmObj.CS_BUY_HOLD_TIME_SEC = holdTime;
		gCSParmObj.CS_BUY_QTY = orderQty;

		gCSParmObj.CS_SELL_ENTER_SPREAD = enterSprd;
		gCSParmObj.CS_SELL_EXIT_SPREAD = exitSprd;
		gCSParmObj.CS_SELL_CUTLOSS_SPREAD = cutlossSprd;
		gCSParmObj.CS_SELL_HOLD_TIME_SEC = holdTime;
		gCSParmObj.CS_SELL_QTY = orderQty;

		gCSParmObj.CS_COMMISION = commision;

		buildCSMarco( "BUY" );
		buildCSMarco( "SELL" );
		sendParmMsg();
	}

	function storeMacroCMD( cmrLine ){
		var cmrAry = cmrLine.split( " " );
		var macName = cmrAry[ 3 ];
		var cmrObj = { name: macName , cmr: cmrLine };

		var index = gCSMacroAry.findIndex( checkName );
		if ( index == -1 ){
			gCSMacroAry.push( cmrObj );
		}else{
			gCSMacroAry[ index ] = cmrObj;
		}

		function checkName( obj ){
			return obj.name == macName;
		}
	}

	function readCSParm( cmrAry ){
		for ( i = 4 ; i < cmrAry.length ; i = i + 3 ){
			var type = cmrAry[ i ];
			var name = cmrAry[ i + 1 ];
			var parm = cmrAry[ i + 2 ];

			if ( type != "DEFAULT" || parm == "DEFAULT" ){
				i = i - 2;
				continue;
			}

			gCSParmObj[ name ] = parm;
		}
	}

	function updateCSRow(){
		var ecn = gCSParmObj.ECN;
		var sym = gCSParmObj.SYM;
		var enterSprd = gCSParmObj.CS_BUY_ENTER_SPREAD;
		var exitSprd = gCSParmObj.CS_BUY_EXIT_SPREAD;
		var cutlossSprd = gCSParmObj.CS_BUY_CUTLOSS_SPREAD;
		var holdTime = gCSParmObj.CS_BUY_HOLD_TIME_SEC;
		var orderQty = gCSParmObj.CS_BUY_QTY;
		var restartTime = gCSParmObj.CS_RESTART_TIME;
		var exitOrderSprd = gCSParmObj.CS_EXIT_ORDER_SPREAD;
		var commision = gCSParmObj.CS_COMMISION;

		if ( gEcnList.indexOf( ecn ) == -1 ){
			ecn = gDefECN;
		}
		if ( isNaN( enterSprd ) ){
			enterSprd = gDefEnterSprd;
		}
		if ( isNaN( exitSprd ) ){
			exitSprd = gDefExitSpread;
		}
		if ( isNaN( cutlossSprd ) ){
			cutlossSprd = gDefCutLossSpread;
		}
		if ( isNaN( holdTime ) ){
			holdTime = gDefHoldTime;
		}
		if ( isNaN( restartTime ) ){
			restartTime = gDefRestartTime;
		}
		if ( isNaN( exitOrderSprd ) ){
			exitOrderSprd = gDefExitOrderSpread;
		}
		if ( isNaN( orderQty ) ){
			orderQty = gDefOrderQty;
		}
		if ( isNaN( commision ) ){
			commision = gDefCommision;
		}

		var symECN = eval( "gSymOpt_" + ecn );
		var symList = "";

		$.each( symECN , function ( value ) {
			symList += "<option value = " + value + "> "+ value + "</option>";
		} );

		var macRow = $( ".panlSec" ).find( ".macRow:last" );

		$( macRow ).find( ".symSlt" ).append( symList );
		$( macRow ).find( ".ecnSlt" ).val( ecn );
		$( macRow ).find( ".ecnSlt" ).trigger( "change" );
		$( macRow ).find( ".ecnSlt" ).foundationCustomForms();

		$( macRow ).find( ".symSlt" ).val( sym );
		$( macRow ).find( ".symSlt" ).trigger( "change" );
		$( macRow ).find( ".symSlt" ).foundationCustomForms();

		$( macRow ).find( ".cfgEnterSpreadDiv" ).find( ".txtIpt" ).val( enterSprd );
		$( macRow ).find( ".cfgCutLossSpreadDiv" ).find( ".txtIpt" ).val( cutlossSprd );
		$( macRow ).find( ".cfgExitSpreadDiv" ).find( ".txtIpt" ).val( exitSprd );
		$( macRow ).find( ".cfgHoldTimeDiv" ).find( ".txtIpt" ).val( holdTime );
		$( macRow ).find( ".cfgRestartTimeDiv" ).find( ".txtIpt" ).val( restartTime );
		$( macRow ).find( ".cfgExitOrderSpreadDiv" ).find( ".txtIpt" ).val( exitOrderSprd );
		$( macRow ).find( ".cfgOrderQtyDiv" ).find( ".txtIpt" ).val( orderQty );
		$( macRow ).find( ".cfgCommisionDiv" ).find( ".txtIpt" ).val( commision );
	}

	function showCSRow( ){ // add a new row if it is not exist
		var rowHtml = "<div class = 'macRow''>" + gRowHtml + "</div>";
		$( ".panlSec" ).append( rowHtml );

		var macRow = $( ".panlSec" ).find( ".macRow:last" );
		$( macRow ).attr( 'id' , "macRow_01" );

		$( macRow ).show();
	}

	function buildCSMarco( side ){
		var macName = "CS_INIT_" + side ;
		var cmd = "CMD " + gInitUserID + " MACRO " + macName;

		for ( key in gCSParmObj ){
			if ( !gCSParmObj.hasOwnProperty( key ) ){
				continue;
			}

			var kSide = key.split("_")[1];

			if( ( side == "BUY" && kSide == "SELL" ) || ( side == "SELL" && kSide == "BUY" ) ){		continue;
			}

			addParm( key , gCSParmObj[ key ] );
		}

		pushCMD( cmd );
		showLog( "buildCSMarco" , cmd );

		function addParm( name , parm ){
			cmd += " DEFAULT " + name + " " + parm;
		}
	}

	function printAlgoLog( cmrLine ){
		var text = $( ".rowStatusTextDiv" ).html();
		var length = 100;
		var text = text.substring(0, length);
		$( ".rowStatusTextDiv" ).html( cmrLine + "<br>" + text );
	}

	function updateCSMacro( ecn , sym ){
		var oldEcn = gCSParmObj.ECN;
		var oldSym = gCSParmObj.SYM;

		if ( typeof oldEcn == "undefined" || typeof oldSym == "undefined" ){
			var msg = "undefined ECN or SYM, ECN: " + oldEcn + " , Sym: " + oldSym;
			showDebugLog( "updateCSMacro" , msg );
			return;
		}

		for( i = 0 ; i < gCSMacroAry.length ; i++ ) {
			var regex = new RegExp( oldEcn , "g" );
			var cmd = gCSMacroAry[i].cmr;
			cmd = cmd.replace( regex , ecn );

			var regex = new RegExp( oldSym , "g" );
			cmd = cmd.replace( regex , sym );

			var regex = new RegExp( "CMR" , "g" );
			cmd = cmd.replace( regex , "CMD" );

			pushCMD( cmd );
			showLog( "updateCSMacro" , cmd );
		}
	}

	function receiveCSMsg( cmrAry ){
		var cmrMsg = "";

		for ( i = 4 ; i < cmrAry.length ; i ++ ){
			if ( i > 4 ){
				cmrMsg += " ";
			}
			cmrMsg += cmrAry[ i ];
		}

		if ( cmrMsg == "PnL Guardian NOT Allow Trade !" ){
			checkCSAllowTrade();
		}
	}

	function receiveHTTPCMD( cmrAry ){
		if ( cmrAry[4] != "CHG_PARAMS" ){
			return;
		}

		var cmrMsg = "";

		for ( i = 4 ; i < cmrAry.length ; i ++ ){
			if ( i > 4 ){
				cmrMsg += " ";
			}
			cmrMsg += cmrAry[ i ];
		}

		$.when( checkIsLeader() ).then( function (){ // check itself is leader
			if ( gIsLeader == 0 ){
				return;
			}
			changeCSParm( cmrMsg );
		} );
	}

	function checkCSAllowTrade(){
		$.when( checkIsLeader() ).then( function (){
			if ( gIsLeader == 0 ){
				return;
			}

			if ( gNotAllowTradeToday == 1 ){
				var msg = "Banned Client to Turn On Algo From Web.";
				sendTabMsg( msg );

			}else if ( gNotAllowTradeToday == 0 ){
				var msg = "PnL Reset. Allowed Client to Turn On Algo From Web.";
				sendTabMsg( msg );

				turnOnCSPanel();
			}
		});
	}

	function changeCSAllowTrade(){
		if( gCSAllowTrade == 0 ){
			gCSAllowTrade = 1;
		}else{
			gCSAllowTrade = 0;
			saveCSNotAllowTradeDay();
		}

		setDefaultParameter( "CS_GUARDIAN_ALLOW_TRADE" , gCSAllowTrade );
	}

	function setCSAllowTradeIcon( parm ){
		if( parm == 1 ){
			$( ".panlAllowTradeSn" ).text( "Web Trade Allowed" );
			$( ".panlAllowTradeDiv" ).removeClass( "banned" );
			$( ".panlAllowTradeDiv" ).addClass( "allowed" );
		}else if ( parm == 0 ){
			$( ".panlAllowTradeSn" ).text( "Web Trade Not Allowed" );
			$( ".panlAllowTradeDiv" ).removeClass( "allowed" );
			$( ".panlAllowTradeDiv" ).addClass( "banned" );
		}
	}

	function saveCSNotAllowTradeDay(){
		var time = getCurrentTime();
		setMacro( "_NOT_ALLOW_DAY" , time );
	}

	function readNotAllowDay( cmrAry ){
		var mDateStr = cmrAry[ 5 ];
		var mYear = parseInt( mDateStr.split( "_" )[ 0 ] );
		var mMonth = parseInt( mDateStr.split( "_" )[ 1 ] );
		var mDay = parseInt( mDateStr.split( "_" )[ 2 ] );

		var cDateStr = getCurrentTime();
		var cYear = parseInt( cDateStr.split( "_" )[ 0 ] );
		var cMonth = parseInt( cDateStr.split( "_" )[ 1 ] );
		var cDay = parseInt( cDateStr.split( "_" )[ 2 ] );

		if ( mYear != cYear || mMonth != cMonth || mDay != cDay ){
			gNotAllowTradeToday = 0;
		}else{
			gNotAllowTradeToday = 1;
		}
	}

	function sendParmMsg(){
		var macRow = $( ".macRow" ).first();

		var ecn = $( macRow ).find( ".ecnDiv" ).find( ".current" ).html();
		ecn = ecn.replace( / /g , "" );

		var sym = $( macRow ).find( ".symDiv" ).find( ".current" ).html();
		sym = sym.replace( / /g , "" );

		var enterSprd = parseFloat( $( macRow ).find( ".cfgEnterSpreadDiv" ).find( ".txtIpt" ).val() );
		var cutlossSprd = parseFloat( $( macRow ).find( ".cfgCutLossSpreadDiv" ).find( ".txtIpt" ).val() );
		var exitSprd = parseFloat( $( macRow ).find( ".cfgExitSpreadDiv" ).find( ".txtIpt" ).val() );
		var holdTime = parseFloat( $( macRow ).find( ".cfgHoldTimeDiv" ).find( ".txtIpt" ).val() );
		var restartTime = parseFloat( $( macRow ).find( ".cfgRestartTimeDiv" ).find( ".txtIpt" ).val() );
		var exitOrderSprd = parseFloat( $( macRow ).find( ".cfgExitOrderSpreadDiv" ).find( ".txtIpt" ).val() );
		var orderQty = parseFloat( $( macRow ).find( ".cfgOrderQtyDiv" ).find( ".txtIpt" ).val() );
		var commision = parseFloat( $( macRow ).find( ".cfgCommisionDiv" ).find( ".txtIpt" ).val() );

		var msg = "Parameter: ECN: " + ecn + ", Symbol: " + sym + ", Enter Spread: " + enterSprd + ", Cut Loss Spread: " + cutlossSprd + ", Exit Spread: " + exitSprd + ", Hold Time: " + holdTime + ", Restart Time: " + restartTime + ", Exit Order Spread: " + exitOrderSprd + ", Order Quantity: " + orderQty + ", Commision: " + commision;

		if ( gGuaMaxLossOn == 1 ){
			msg += ", Maximum Loss: " + gGuaMaxLossParm;
		}

		if ( gGuaLossFromTopOn == 1 ){
			msg += ", Maximum Loss From Top: " + gGuaMaxLossParm;
		}

		sendTabMsg( msg );
		sendTestMsg( msg );
	}

	function sendTestMsg( msg ){
		sendMsg( gInitUserID , "TEST" , msg );
	}

	function changeCSParm( cmrMsg ){
		var cmrAry = cmrMsg.split( " " );
		for ( var i = 1 ; i < cmrAry.length; i = i + 2 ){
			var name = cmrAry[ i ];
			var parm = parseInt( cmrAry[ i + 1 ] );

			if ( isNaN( parm ) ){
				i--;
				continue;
			}

			gCSParmObj[ name ] = parm;
		}

		buildCSMarco( "BUY" );
		buildCSMarco( "SELL" );

		callMacro( "CS_INIT" );

		setTimeout( function (){
			sendParmMsg();
		} , 500 );
	}

	function callMacro( name ){
		var cmd = "CMD " + gInitUserID + " CALL " + name;
		pushCMD( cmd );
	}
 }