// algo setting //
var gVersion = "1a";
var gPanelID = "#jpanel_algo";
var gDefECN = "MREX";
var gDefSym = "CME/CMX_GLD/AUG18";
var gDefOrdTIF = "DAY";
var gLOType = "LO2";
var gSOType = "SO2";
var gDefOrdType = gLOType;
var gDefPips = 1;
var gDefPipsMin = 0;
var gDefPipsMax = 10;
var gDefQty = 1;
var gDefQtyMin = 1;
var gDefQtyMax = 20;
var gDefCase = 9;

var gDefPlcOrdDly = 0; // Delay to place order when a order is canceled on non-stop algo
var gDefBufferMaxLoss = -1000;
var gDefBufferOpenOrd = 3;
var gDefAutoFlatSlip = 20;
var gDelayTurnOn = 15000;
var gFuseStopPxBuffer = 10;
var gLeaderLiveTime = 3000;
var gAlertVolume = 50;
var gFiveSecondTick = 10;

var gNearBuffer = 5;
var gLeaveBuffer = gNearBuffer + 2;
var gBreakBuffer = -10;

var gDealerUser = [ 311 , 312 , 313 , 314 , 315 , 316 , 317 , 318 , 319 , 321 , 322 , 323 , 324 , 325 , 326 , 327 , 328 , 329 ];

var gTelegramTargetAry = [];
gTelegramTargetAry.push( { userID: [ 300 , 310 , 314 , 317 , 318 , 319 , 320 , 324 , 326 , 327 , 328 , 329 ] , target: [ "joseph" ] } );
gTelegramTargetAry.push( { userID: [ 301 , 303 , 304 , 305 , 306 , 307 , 308 , 309 ] , target: [ "joseph" , "alfred" , "kellog" ] } );
gTelegramTargetAry.push( { userID: [ 302 ] , target: [ "joseph" , "alfred" , "kellog" , "yuri" ] } );
gTelegramTargetAry.push( { userID: [ 311 , 321 ] , target: [ "joseph" , "marky" ] } );
gTelegramTargetAry.push( { userID: [ 312 , 322 ] , target: [ "joseph" , "raymond" ] } );
gTelegramTargetAry.push( { userID: [ 313 , 323 ] , target: [ "joseph" , "kellog" ] } );
gTelegramTargetAry.push( { userID: [ 315 , 325 ] , target: [ "joseph" , "grand" ] } );
gTelegramTargetAry.push( { userID: [ 316 , 326 ] , target: [ "joseph" , "yuri" ] } );
gTelegramTargetAry.push( { userID: [ 319 , 329 ] , target: [ "joseph" , "kaho" ] } );
gTelegramTargetAry.push( { userID: [ 528 , 529 ] , target: [ "joseph" , "dicky" ] } );

var gAlertTarget = [ "gsRO" ];